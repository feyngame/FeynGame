//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Taskbar;
import java.awt.geom.Rectangle2D;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.print.attribute.standard.MediaSizeName;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.WindowConstants;

import amplitude.FeynmanDiagram;
import export.ExportFormat.ExportException;
import export.ExportFormat.ExportProgressUpdate;
import export.ExportFormat.ExportResult;
import export.ExportManager;
import game.FeynLine;
import game.InOutLevel;
import game.Presets;
import layout.FeynGameDiagramStructureException;
import layout.GraphLayoutException;
import qgraf.LoadedQgrafStyle;
import qgraf.ModelData;
import qgraf.QgrafDiagram;
import qgraf.QgrafImportException;
import qgraf.QgrafImportManager;
import qgraf.QgrafOutputFile;
import qgraf.QgrafStyleFile;
import resources.GetResources;
import ui.Frame;
import ui.InOutMode;
import ui.Pane;

/**
 * This is the first class that is run, it handles the initial frame to choose
 * the gamemode.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */

public class FeynGame implements Runnable {
  private static final Logger LOGGER = Frame.getLogger(FeynGame.class);
  
  static {
    String os = System.getProperty("os.name");
    boolean macOs = os.toLowerCase().startsWith("mac");

    JFrame.setDefaultLookAndFeelDecorated(true);
    try {
      if (!macOs)
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      if (macOs) {
        try {
          for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
              UIManager.setLookAndFeel(info.getClassName());
              break;
            }
          }
        } catch (Exception e) {
          UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      LOGGER.log(Level.WARNING, "Cannot set the desired Look and Feel", e);
    }
    LOGGER.log(Level.FINE, "Selected Look and Feel: " + UIManager.getLookAndFeel().getName());
  }
  
  /**
   * the command line arguments that are potentially given when starting
   * FeynGame from the console
   */
  private String[] args;
  private OptionParser options;
  private Frame frame;
  private QgrafStyleFile selectedStyle;
  /**
   * the constructor of this class that sets the args member variable
   * @param args the arguments that are potentially given
   */
  public FeynGame(String[] args) {
    this.args = args;
  }
  /**
   * this is the main class that constructs an instance of this class
   * @param args the arguments that are potentially given
   */
  public static void main(String[] args) {
    SwingUtilities.invokeLater(new FeynGame(args));
  }
  
  public void run() {
    printDisclaimer();
    
    if(GraphicsEnvironment.isHeadless()) {
      printHeadlessMessage();
      System.exit(OptionException.EC_HEADLESS);
    }
    
    if (this.args.length == 0) {
      Frame.configureParentLogger(false, null, null);
      runNoArgs();
    } else {
      try {
        options = OptionParser.parse(args);
        Frame.DEBUG_MODE = options.debug;
        Frame.configureParentLogger(options.debug, options.consoleLevel, options.fileLevel);
        
        //If ONLY debug is set, run as if called with no args
        if(options.debug && args.length == 1) {
          runNoArgs();
        } else {
          runWithArgs();
        }
      } catch (OptionException e) {
        LOGGER.log(Level.SEVERE, "", e);
        System.exit(e.getExitCode());
      }
    }
  }

  private static void printHeadlessMessage() {
    System.out.println("FeynGame requires a headful graphics environment that supports keyboard and mouse input and screen output.");
    System.err.println("Headless mode not supported");
  }

  private void runNoArgs() {
    /*show initial frame*/

    /* get version*/
    String res = Frame.readFeynGameVersion();
    /* initialize initial frame with version number*/
    JFrame newFrame = new JFrame("FeynGame v" + res);
    newFrame.setResizable(false);

    /* set background of the Window, there was one case where it did
    not do that automatically*/
    newFrame.getContentPane().setBackground(
        UIManager.getColor("Panel.background"));

    ToolTipManager.sharedInstance().setInitialDelay(0);

    newFrame.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    newFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    newFrame.setLayout(gridbag);
    c.weightx = 1;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = 0;
    c.insets = new Insets(50, 50, 0, 50);

    JButton freeDraw = new JButton();
    try {
      freeDraw.setIcon(GetResources.getIcon("freeIcon.png"));
    } catch (Exception ex) {
      LOGGER.log(Level.SEVERE, "Cannot load draw mode icon", ex);
    }
    freeDraw.addActionListener(e -> {
      new Frame(null, true); //show is true
      newFrame.dispose();
    });
    freeDraw.setPreferredSize(new Dimension(215, 117));
    freeDraw.setToolTipText("Draw Feynman diagrams freely");
    ToolTipManager.sharedInstance().registerComponent(freeDraw);
    newFrame.add(freeDraw, c);

    c.gridx++;

    JButton inOutMode = new JButton();
    try {
      inOutMode.setIcon(GetResources.getIcon("inOutIcon.png"));
    } catch (Exception ex) {
      LOGGER.log(Level.SEVERE, "Cannot load infin mode icon", ex);
    }
    inOutMode.addActionListener(e -> {
      new InOutMode();

      newFrame.dispose();
    });
    inOutMode.setPreferredSize(new Dimension(215, 117));
    inOutMode.setToolTipText("Draw a Feynman diagram with the initial and final states given");
    ToolTipManager.sharedInstance().registerComponent(inOutMode);
    newFrame.add(inOutMode, c);

    c.insets = new Insets(0, 50, 50, 50);

    c.gridx = 0;
    c.gridy++;

    JLabel freeLabel = new JLabel("Drawing mode");
    Font bigFont = freeLabel.getFont().deriveFont((float) (freeLabel.getFont().getSize2D()*2.));
    freeLabel.setFont(bigFont);
    newFrame.add(freeLabel, c);

    c.gridx++;

    JLabel inOutLabel = new JLabel("InFin mode");
    inOutLabel.setFont(bigFont);
    newFrame.add(inOutLabel, c);

    newFrame.pack();
    newFrame.setLocationRelativeTo(null);
    newFrame.setVisible(true);
  }

  private void runWithArgs() throws OptionException {
    boolean infin = options.loadIf != null;
    
    if(options.help) {
      System.out.println("Detected help option, ignoring all other options");
      printHelpText();
      System.exit(0); //explicit exit since help ignores everything
    } else if(options.version) {
      System.out.println("FeynGame v" + Frame.readFeynGameVersion());
      System.exit(0);
    } else if(infin) {
      createInFinFrameAndLoadLevel();
    } else { //Draw mode
      createDrawFrameAndLoadModel();
    }
    
    //load a qgraf style from file or select it from the loaded ones
    if(options.importStyle != null && !infin) {
      loadQgrafStyle();
    }
    
    if(options.mbbArea != null) {
      setBoundingBox();
    }
    
    //Get the diagram or level from the correct source
    boolean hasDiagram;
    if(infin) {
      //already loaded in createInFinFrame
      hasDiagram = true;
    } else if(options.loadFg != null) {
      loadDrawFile();
      hasDiagram = true;
    } else if(options.importQgraf != null) {
      loadQgrafFile();
      hasDiagram = true;
    } else {
      hasDiagram = false;
    }
    
    //apply labels, momenta etc
    if(!infin) {
      modifyDiagram();
    }
    
    //export or show frame
    if(hasDiagram && options.exportDest != null) {
      exportToFile();
    } else {
      showFrame();
    }
  }
  
  private void createInFinFrameAndLoadLevel() {
    String path = options.loadIf.toAbsolutePath().toString();
    InOutLevel level = new InOutLevel(path);
    int time = options.infinTime;
    frame = new Frame(level, time > 0, time);
  }
  
  private void createDrawFrameAndLoadModel() {
    String modelPath = options.loadModel == null ? null : options.loadModel.toAbsolutePath().toString();
    frame = new Frame(modelPath, false); //for now, don't show since we might want to export later.
  }
  
  private void loadQgrafStyle() throws OptionException {
    String styleNameOrPath = options.importStyle;
    for(LoadedQgrafStyle lqs : QgrafImportManager.getQgrafStyles()) {
      if(lqs.getName().equals(styleNameOrPath)) { //found one
        selectedStyle = lqs.getStyle();
        break;
      }
    }
    
    //not found under that name, so search as a path instead
    if(selectedStyle == null) {
      Path filePath = Paths.get(styleNameOrPath);
      LoadedQgrafStyle lqs;
      try {
        lqs = QgrafImportManager.doAutoStyleLoad(filePath);
      } catch (QgrafImportException e) {
        throw new OptionException("Cannot load style file at " + filePath.toAbsolutePath().toString() +
            ": not a valid qgraf style file (" + e.getMessage() + ")", e, OptionException.EC_INVALID_FILE_CONTENT);
      } catch (FileNotFoundException e) {
        throw new OptionException("Cannot load style file at " + filePath.toAbsolutePath().toString() +
            ": file not found (" + e.getMessage() + ")", e, OptionException.EC_FILE_NOT_FOUND);
      } catch (IOException e) {
        throw new OptionException("Cannot load style file at " + filePath.toAbsolutePath().toString() +
            ": cannot access file (" + e.getMessage() + ")", e, OptionException.EC_FILE_NOT_ACCESSIBLE);
      }
      selectedStyle = lqs.getStyle();
    }
  }

  private void setBoundingBox() {
    Rectangle2D area = options.mbbArea;
    Pane pane = frame.getDrawPane();
    pane.mBB = true;
    pane.xMBB = area.getMinX();
    pane.yMBB = area.getMinY();
    pane.wMBB = area.getWidth();
    pane.hMBB = area.getHeight();
  }
  
  private void loadDrawFile() {
    frame.loadDiagram(options.loadFg.toAbsolutePath().toString());
  }
  
  private void loadQgrafFile() throws OptionException {
    if(selectedStyle == null) throw new OptionException("Cannot import from qgraf output file without selecting a style file", OptionException.EC_MISSING_REQIRED_OPTION);
    Presets presets = frame.models.get(frame.tileTabs.getCurrentKey());
    ModelData model = new ModelData(presets);
    QgrafOutputFile qof;
    try {
       qof = QgrafOutputFile.fromFile(options.importQgraf, selectedStyle, options.qgrafDiagramIndex, model);
    } catch (FileNotFoundException e) {
      throw new OptionException("Cannot import qgraf output file at " + options.importQgraf.toAbsolutePath().toString() +
          ": file not found (" + e.getMessage() + ")", e, OptionException.EC_FILE_NOT_FOUND);
    } catch (IOException e) {
      throw new OptionException("Cannot import qgraf output file at " + options.importQgraf.toAbsolutePath().toString() +
          ": cannot access file (" + e.getMessage() + ")", e, OptionException.EC_FILE_NOT_ACCESSIBLE);
    } catch (QgrafImportException e) {
      throw new OptionException("Cannot import qgraf output file at " + options.importQgraf.toAbsolutePath().toString() +
          ": not a valid qgraf output file for the selected qgraf style (" + e.getMessage() + ")", e, OptionException.EC_INVALID_FILE_CONTENT);
    }
    
    boolean loadAll = options.qgrafDiagramIndex == QgrafOutputFile.LOAD_ALL_INDEX_VALUE;
    if(qof == null) {
      if(loadAll) {
        throw new OptionException("Cannot import qgraf output file at " + options.importQgraf.toAbsolutePath().toString(), OptionException.EC_INVALID_FILE_CONTENT);
      } else { //index not in file
        throw new OptionException("Cannot import qgraf output file at " + options.importQgraf.toAbsolutePath().toString() 
            + ": The requested diagram index " + options.qgrafDiagramIndex + " is not in the file", OptionException.EC_INVALID_OPTION_VALUE);
      }
    }
    
    //File is parsed. Now display the correct diagram
    if(qof.getDiagramCount() == 0) throw new OptionException("Output file contains no diagrams", OptionException.EC_INVALID_FILE_CONTENT);
    if(!loadAll) { //check that it's there if we selected one
      if(qof.isPartial() && options.qgrafDiagramIndex != qof.getOnlyLoadedIndex())
        throw new OptionException("Cannot import qgraf output file at " + options.importQgraf.toAbsolutePath().toString() 
            + ": The requested diagram index " + options.qgrafDiagramIndex + " is not in the file", OptionException.EC_INVALID_OPTION_VALUE);
      if(!qof.isPartial() && options.qgrafDiagramIndex > qof.getDiagramCount())
        throw new OptionException("Cannot import qgraf output file at " + options.importQgraf.toAbsolutePath().toString() 
            + ": The requested diagram index " + options.qgrafDiagramIndex + " is not in the file", OptionException.EC_INVALID_OPTION_VALUE);
    }
    
    frame.setDiagramAndMultiMode(qof, loadAll ? 0 : options.qgrafDiagramIndex, loadAll);
    if(frame.resetExportDestinationOnLoad) ExportManager.setNextGuiExportDestination(options.importQgraf, true);
    frame.repaint();
  }
  
  private void modifyDiagram() throws OptionException {
    if(options.applyModel) {
      frame.forceLineStyles(frame.getSelectedPresets().presets);
    }
    
    if(options.blackwhite) {
      frame.getDrawPane().bw();
    }
    
    if(options.applyLayout) {
      try {
        QgrafDiagram qgdg = new QgrafDiagram(frame.getDrawPane().vertices, frame.getDrawPane().lines, frame.getDrawPane().getDiagramLayoutArea());
        frame.setDiagramNoGui(qgdg, Frame.CLEARMODE_CLEAR_LINES);
      } catch (FeynGameDiagramStructureException | GraphLayoutException e) {
        throw new OptionException("Cannot apply layout algorithm to diagram (" + e.getMessage() + ")", e, OptionException.EC_INVALID_DIAGRAM);
      }
    }
    
    if(options.distributeMomenta) {
      FeynmanDiagram fd = new FeynmanDiagram(frame.getDrawPane().lines);
      fd.distributeMomenta();
    }
    
    if(options.enableLabels) {
      for(FeynLine l : frame.getDrawPane().lines) {
        l.showDesc = true;
      }
    } else if(options.disableLabels) {
      for(FeynLine l : frame.getDrawPane().lines) {
        l.showDesc = false;
      }
    }
  }
  
  private void exportToFile() throws OptionException {
    //First, handle all options
    ExportManager.configureBlackWhite(options.blackwhite);
    ExportManager.configureTransparent(options.transparent);
    ExportManager.configureRasterExport(1); //default value
    ExportManager.configureVectorExport(MediaSizeName.ISO_A4, false);
    ExportManager.configureDiagramsPerPage(1, 1);
//    ExportManager.configureBlackWhite(false);
//    ExportManager.configureTransparent(false);
    
    if(options.imageSize != null) {
      if(options.imageSize.length == 2) {
        ExportManager.configureRasterExport(options.imageSize[0], options.imageSize[1]);
      } else {
        ExportManager.configureRasterExport(options.imageSize[0]);
      }
    } else if(options.physicalSize != null) {
      if(options.physicalSize.length == 2) {
        ExportManager.configureVectorExport(options.physicalSize[0], options.physicalSize[1], options.physicalSizeUnit);
      } else {
        ExportManager.configureVectorExport(options.physicalSize[0], options.physicalSizeUnit);
      }
    } else if(options.paperName != null) {
      ExportManager.configureVectorExport(options.paperName, options.landscape);
    }
    
    if(options.multiRowCol != null) {
      ExportManager.configureDiagramsPerPage(options.multiRowCol[0], options.multiRowCol[1]);
    }
    ExportManager.configureLabels(options.enableLabels, options.disableLabels);
    ExportManager.configureMomenta(options.distributeMomenta);
    
    try {
      LOGGER.info("Exporting to \"" + options.exportDest.toString() + "\"");
      ExportResult result = options.exportFormat.doAutoExport(frame.getDrawPane(), options.exportDest, ExportProgressUpdate.PRINT_TO_LOGGER);
      LOGGER.info(result.isCanceled() ? "Export canceled" : "Export successful");
    } catch (FileNotFoundException e) {
      throw new OptionException("Cannot export to file at " + options.exportDest.toAbsolutePath().toString() + 
          ": file not found (" + e.getMessage() + ")", e, OptionException.EC_FILE_NOT_FOUND);
    } catch (IOException e) {
      throw new OptionException("Cannot export to file at " + options.exportDest.toAbsolutePath().toString() + 
          ": cannot access file (" + e.getMessage() + ")", e, OptionException.EC_FILE_NOT_ACCESSIBLE);
    } catch (ExportException e) { //display the exact message
      throw new OptionException(e.getMessage(), e.getCause(), OptionException.EC_EXPORT_ERROR);
    }
    
    //dispose and exit
    frame.dispose();
  }
  
  private void showFrame() {
    frame.setVisible(true);
  }
  
  private void printHelpText() {
    System.out.println("================================================================================");
    System.out.println("This is FeynGame, a graphical tool to create Feynman diagrams.");
    System.out.println("If launched with no options, a GUI will allow the user to choose between"
		       + System.getProperty("line.separator")
		       + "Draw mode and InFin mode."
		       + System.getProperty("line.separator")
		       );
    System.out.println("FeynGame can also be launched with the following command line arguments:"
		       + System.getProperty("line.separator"));
    System.out.println("$ <FeynGame> [filenames] [options]");
    System.out.println();
    System.out.println("- <FeynGame> is either the FeynGame executable or"
		       + System.getProperty("line.separator")
		       + "  'java -jar <path_to/>FeynGame.jar>'"
		       + System.getProperty("line.separator")
		       + "  if launching directly with Java.");
    System.out.println();
    System.out.println("- [filenames] is a list of filenames. The meaning of the filename will"
		       + System.getProperty("line.separator")
		       + "  depend on the file extension:");
    System.out.println("    *.fg    = --diagram <filename>");
    System.out.println("    *.if    = --level <filename>");
    System.out.println("    *.model = --model <filename>");
    System.out.println("    *.sty   = --qgraf-style <filename>");
    System.out.println("    *.out   = --qgraf-file <filename>");
    System.out.println("  Otherwise, if FeynGame knows how to export to a file format associated"
		       + System.getProperty("line.separator")
		       + "  with the extension, this is eqivalent to --export <filename>");
    System.out.println("  You can list more than one filename, but not all file types can go"
		       + System.getProperty("line.separator")
		       + "  together and each type can only be used once. See the respective options"
		       + System.getProperty("line.separator")
		       + "  for more details.");
    System.out.println();
    System.out.println("- [options] is a list of options and their corresponding values."
		       + System.getProperty("line.separator")
		       + "  Options have the format --<name> [values], where");
    System.out.println("  the number of expected values depends on the option. Some options also"
		       + System.getProperty("line.separator")
		       + "  have short names: -<shortname> [values].");
    System.out.println("  Every option may appear only once. Not all options are compatible"
       		       + System.getProperty("line.separator")
		       + "  with each other. FeynGame supports the following options:"
       		       + System.getProperty("line.separator")
		       );
    System.out.println("  Help options:");
    System.out.println("    --help (short -h): Shows this text and exits, discarding all other options."); 
    System.out.println("    --version (short -v): Shows the program version and exits,"
		       + System.getProperty("line.separator")
		       + "        discarding all other options."
		       + System.getProperty("line.separator"));
    System.out.println("  InFin mode options:");
    System.out.println("    Starting in InFin mode disables most other options related to import,"
       		       + System.getProperty("line.separator")
		       + "    export and diagram manipulation.");
    System.out.println("    --level <filename> (short -l): Load a level file and show the GUI"
		       + System.getProperty("line.separator")
		       + "      in InFin mode.");
    System.out.println("    --time-limit <number> (short -t): A limit that restricts the amount of"
		       + System.getProperty("line.separator")
		       + "      time a user has to solve a challenge in InFin mode."
		       + System.getProperty("line.separator")
		       );
    System.out.println("  Draw mode options:");
    System.out.println("    --diagram <filename> (short -d) Load a FeynGame save file and"
		       + System.getProperty("line.separator")
		       + "      show it in Draw mode.");
    System.out.println("    --diagram (short -d): Show the GUI in draw mode, but do not load"
		       + System.getProperty("line.separator")
		       + "      any diagram.");
    System.out.println("    --bounds <number> <number> (short -b): Enables and configures the"	
		       + System.getProperty("line.separator")
		       + "      manual bounding box; a rectangle that is used for layout"
		       + System.getProperty("line.separator")
		       + "      and export purposes. Numbers are width and height, the top left"
		       + System.getProperty("line.separator")
		       + "      corner will be at (0, 0).");
    System.out.println("    --bounds <number> <number> <number> <number> (short -b): Enables and"
		       + System.getProperty("line.separator")
		       + "      configures the manual bounding box; a rectangle that is used for layout"
		       + System.getProperty("line.separator")
		       + "      and export purposes. Numbers are top left x and y, width and height.");
    System.out.println("    --model <file> Load this FeynGame model instead of the default model.");
    System.out.println("");
    System.out.println("  Qgraf import options:");
    System.out.println("    These options control the interaction of FeynGame with the");
    System.out.println("      external program 'qgraf'.");
    System.out.println("    --qgraf-style <name or file> (short -s): Selects a qgraf style that");
    System.out.println("      will be used to import from qgraf and adds a qgraf style to the list"
		       + System.getProperty("line.separator")
		       + "      of loaded styles. If <name or file> matches the name of any style that"
		       + System.getProperty("line.separator")
		       + "      is loaded by default, this one is selected. Otherwise,"
		       + System.getProperty("line.separator")
		       + "      <name or file> is interpreted as a path and that file is imported"
		       + System.getProperty("line.separator")
		       + "      as a qgraf style file.");
    System.out.println("    --qgraf-file <file> (short -q): Imports a qgraf output file. This option"
		       + System.getProperty("line.separator")
		       + "      can only be used if a style has been selected.");
    System.out.println("      It cannot be used together with --diagram <file>. This output must have"
		       + System.getProperty("line.separator")
		       + "      been generated using the selected qgraf style file.");
    System.out.println("    --diagram-index <number>: Selects one diagram from the qgraf output file"
		       + System.getProperty("line.separator")
		       + "      by zero-based index. Can only be used with --qgraf-file.");
    System.out.println("      If this option is omitted, FeynGame will enter Multi-Diagram mode,"
		       + System.getProperty("line.separator")
		       + "      where all diagrams in the output file are loaded and can be cycled"
		       + System.getProperty("line.separator")
		       + "      through in the GUI. The available formats for exporting"
		       + System.getProperty("line.separator")
		       + "      may be limited in that mode."
		       + System.getProperty("line.separator"));
    System.out.println("  Diagram manipulation options:");
    System.out.println("    --labels: Enables the label for all lines in the diagram.");
    System.out.println("    --no-labels: Disables the label for all lines in the diagram.");
    System.out.println("    --momenta: Calculates and distributes momenta to all lines and displays"
		       + System.getProperty("line.separator")
		       + "      a momentum arrow on each line.");
    System.out.println("    --apply-layout: Applies the default layout algorithm to the diagram."
		       + System.getProperty("line.separator")
		       + "      This option is implied when importing from qgraf.");
    System.out.println("    --blackwhite: Converts all line colors to greyscale.");
    System.out.println("    --apply-model: This will change all lines in the loaded FeynGame save file"
		       + System.getProperty("line.separator")
		       + "      to match the style defined in the model file loaded by"
		       + System.getProperty("line.separator")
		       + "      the --model option, or the default model file if that option is missing."
		       + System.getProperty("line.separator"));
    System.out.println("  Export options:");
    System.out.println("    FeynGame distinguishes between exporting to a raster image"
		       + System.getProperty("line.separator")
		       + "    format (PNG, JPG, ...) and a vector image format (PS, PDF, ...).");
    System.out.println("    Some options only affect one of these two formats, and will be silently"
		       + System.getProperty("line.separator")
		       + "    ignored for the other.");
    System.out.println("    --export <file> (short -e): Enables export and sets the export"
		       + System.getProperty("line.separator")
		       + "      destination file. The export format will be determined from the");
    System.out.println("      extension, but can also be set explicitly with --export-format."
		       + System.getProperty("line.separator")
		       + "      If the file exists, it will be overwritten.");
    System.out.println("    --export-format <extension> (short -f): Sets the export format by"
		       + System.getProperty("line.separator")
		       + "      naming a file extension. Can be used if the extension");
    System.out.println("      of the export destination deviates from the desired format.");
    System.out.println("    --transparent: Affects raster image export. If the format supports"
		       + System.getProperty("line.separator")
		       + "      transparency, the background will be transparent instead of white.");
    System.out.println("    --image-size <number>: Affects raster image export. Set the width of"
		       + System.getProperty("line.separator")
		       + "      the image in pixels. The height will automatically be determined");
    System.out.println("      from the aspect ratio of the diagram (or of the bounding box"
		       + System.getProperty("line.separator")
		       + "      if it was enabled with --bounds).");
    System.out.println("    --image-size <number> <number>: Affects raster image export. Set width"
		       + System.getProperty("line.separator")
		       + "      and height of the image in pixels. The diagram will be centered");
    System.out.println("      on the image and as large as possible without cutting off anything.");
    System.out.println("    --paper-size <paper>: Affects vector image export. The exported vector"
		       + System.getProperty("line.separator")
		       + "      graphic will have exactly the size of a named paper format.");
    System.out.println("      Accepts some simple names like 'A4' or 'letter'"
		       + System.getProperty("line.separator")
		       + "      ('A0'-'A10'; 'B0'-'B10'; 'C0'-'C6'; 'letter'; 'legal' are"
		       + System.getProperty("line.separator")
		       + "      currently supported).");
    System.out.println("      You can also specify any name of a paper size defined in the java"
		       + System.getProperty("line.separator")
		       + "      class javax.print.attribute.standard.MediaSizeName"
		       + System.getProperty("line.separator")
		       + "      using the variable name.");
    System.out.println("    --paper-size <number> <unit>: Affects vector image export. The exported"
		       + System.getProperty("line.separator")
		       + "      vector graphic will have the specified width. The height will");
    System.out.println("      be calculated from the aspect ratio of the diagram"
		       + System.getProperty("line.separator")
		       + "      (or of the bounding box if it was enabled with --bounds).");
    System.out.println("      Supported units are 'cm', 'mm' and 'in'.");
    System.out.println("    --paper-size <number> <number> <unit>: Affects vector image export."
		       + System.getProperty("line.separator")
		       + "      The exported vector graphic will have the specified width and height.");
    System.out.println("      Supported units are 'cm', 'mm' and 'in'.");
    System.out.println("    --diagrams-per-page <number> <number>: Affects vector image export."
		       + System.getProperty("line.separator")
		       + "      Only relevant in Multi-Diagram mode (see --diagram-index).");
    System.out.println("      Configures the number of rows and columns of diagrams that"
		       + System.getProperty("line.separator")
		       + "      are on one page.");
    System.out.println("    If no diagram is exported, FeynGame will open a GUI and display the"
		       + System.getProperty("line.separator")
		       + "    loaded diagram in Draw mode.");
    System.out.println("    If a diagram is exported to a file, FeynGame will terminate afterwards"
		       + System.getProperty("line.separator")
		       + "    without showing any GUI.");
    System.out.println("================================================================================");
  }
  
  /*
   * Just some Console output with some disclaimers
   */
  private void printDisclaimer() {
    System.out.println("\n" +
		       "  ______                    _____                         \n" +
		       " |  ____|                  / ____|                        \n" +
		       " | |__  ___  _   _  _ __  | |  __   __ _  _ __ ___    ___ \n" +
		       " |  __|/ _ \\| | | || '_ \\ | | |_ | / _` || '_ ` _ \\  / _ \\\n" +
		       " | |  |  __/| |_| || | | || |__| || (_| || | | | | ||  __/\n" +
		       " |_|   \\___| \\__, ||_| |_| \\_____| \\__,_||_| |_| |_| \\___|\n" +
		       "              __/ |                                       \n" +
		       "             |___/                                        \n"
		       + System.getProperty("line.separator")
		       + "  Copyright (C) 2019-2024"
		       + System.getProperty("line.separator")
		       + "by Lars B\u00fcndgen, Robert Harlander, Sven Yannick Klein,"
		       + System.getProperty("line.separator")
		       + "   Maximilian Lipp, and Magnus Schaaf."
		       + System.getProperty("line.separator")
		       + "This program comes with ABSOLUTELY NO WARRANTY."
		       + System.getProperty("line.separator")
		       + "This is free software, and you are welcome "
		       + "to redistribute it "
		       + System.getProperty("line.separator")
		       + "under certain conditions. "
		       + "For details, see License."
		       );
  }
}
