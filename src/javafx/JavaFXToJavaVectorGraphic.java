//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package javafx;

import game.JavaVectorGraphic;
import game.Vertex;
import game.Shaped;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;

/**
 * this is a class that reads the information from the JavaFx (".fx") files
 * and stores them in a {@link game.JavaVectorGraphic} object.
 *
 * @author Sven Yannick Klein
 */
public class JavaFXToJavaVectorGraphic {
  /**
   * holds all the available paths to the fillings (stored as ".fx") inside the application
   */
  public static ArrayList<String> patternsIn = new ArrayList<>();
  /**
   * holds all the available paths to the fillings (stored as ".fx") from outside of the application
   */
  public static ArrayList<String> patternsOut = new ArrayList<>();
  /**
   * the current stoke
   */
  private static BasicStroke currStroke;
  /**
   * determines if there is a moving described in the lines currently read
   */
  private static boolean moveTo;
  /**
   * determines if there is a line described in the lines currently read
   */
  private static boolean lineTo;
  /**
   * determines if there is a curve described in the lines currently read
   */
  private static boolean curveTo;
  /**
   * the lastly read x value
   */
  private static double x;
  /**
   * the lastly read control x1 value
   */
  private static double contx1;
  /**
   * the lastly read control y1 value
   */
  private static double conty1;
  /**
   * the lastly read control x2 value
   */
  private static double contx2;
  /**
   * the lastly read control y2 value
   */
  private static double conty2;
  /**
   * current path
   */
  private static java.awt.geom.Path2D.Double path;
  /**
   * determines if the first "Path" keyword has been found already
   */
  private static boolean fPath;

  /**
   * initialises the list of pattern names
   */
  public static void initNames() {
    patternsIn.clear();
    patternsIn.add("Cross");
    patternsIn.add("Crosshatched");
    patternsIn.add("Hatched");
    patternsIn.add("Star");
  }

  /**
   * add path to {@link #patternsOut}
   *
   * @param path the path to the ".fx" file that is added
   */
  public static void addPath(String path) {
    JavaFXToJavaVectorGraphic.patternsOut.add(path);
  }

  /**
   * searches for file with name "name.fx" and tries to convert its content to a Line2D object
   *
   * @param name the name of the pattern
   * @return the JavaVectorGraphic Object that has been parsed from the file
   * @throws Exception if the file described by name could not be found
   */
  public static JavaVectorGraphic getJavaVectorGraphic(String name) throws Exception {
    if (!name.endsWith(".fx"))
      name = name + ".fx";

    boolean in = false;
    boolean found = false;
    for (String path : patternsIn) {
      path += ".fx";
      path = path.toLowerCase();
      if (path.equals(name)) {
        name = path;
        in = true;
        found = true;
        break;
      }
    }
    if (!found) {
      for (String path : patternsOut) {
        if (path.endsWith(name)) {
          name = path;
          in = false;
          found = true;
          break;
        }
      }
    }

    if (!found)
      throw new Exception("Could not find file.");

    InputStream inStream = JavaFXToJavaVectorGraphic.class.getResourceAsStream(name);
    if (in)
      inStream = JavaFXToJavaVectorGraphic.class.getResourceAsStream(name);
    if (!in)
      inStream = new FileInputStream(name);
    BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
    String line;
    JavaVectorGraphic panel = new JavaVectorGraphic();
    currStroke = new BasicStroke();
    path = new java.awt.geom.Path2D.Double();
    try {
      while ((line = reader.readLine()) != null) {
        JavaFXToJavaVectorGraphic.addToLine(line, panel);
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    if (path != null)
      panel.draw(path);
    fPath = false;
    return panel;
  }

  /**
   * converts a string to Line2D
   *
   * @param text  the line that is parsed
   * @param panel the JavaVectorGraphic object that the parsed information is written to
   */
  public static void addToLine(String text, JavaVectorGraphic panel) {

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("opacity:")) {
      String trans = text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("opacity:", "");
      Color old = panel.getColor();
      if (old != null) {
        Color newC = new Color(old.getRed(), old.getGreen(), old.getBlue(), (int) (255 * Float.parseFloat(trans)));
        panel.setColor(newC);
      }
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("fill:")) {
      String trans = text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("fill:Color.rgb", "");
      trans = trans.replaceAll("[)(]", "");
      String[] values = trans.split("[,;]");
      Color newC = new Color(Integer.decode(values[0]), Integer.decode(values[1]), Integer.decode(values[2]), (int) (255 * Float.parseFloat(values[3])));
      panel.setFillColor(newC);
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("stroke:")) {
      String trans = text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("stroke:Color.rgb", "");
      trans = trans.replaceAll("[)(]", "");
      String[] values = trans.split("[,;]");
      Color newC = new Color(Integer.decode(values[0]), Integer.decode(values[1]), Integer.decode(values[2]), (int) (255 * Float.parseFloat(values[3])));
      panel.setColor(newC);
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("strokeWidth:")) {
      String strokeWidth = text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("strokeWidth:", "");
      BasicStroke newS = new BasicStroke(Float.parseFloat(strokeWidth), currStroke.getEndCap(), currStroke.getLineJoin(), currStroke.getMiterLimit(), currStroke.getDashArray(), currStroke.getDashPhase());
      currStroke = newS;
      panel.setStroke(newS);
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("strokeLineCap:StrokeLineCap.")) {
      String stroke = text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("strokeLineCap:StrokeLineCap.", "");
      BasicStroke newS = new BasicStroke(currStroke.getLineWidth());
      float lineWidth = currStroke.getLineWidth();
      if (stroke.equals("BUTT"))
        newS = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, currStroke.getLineJoin(), currStroke.getMiterLimit(), currStroke.getDashArray(), currStroke.getDashPhase());
      if (stroke.equals("ROUND"))
        newS = new BasicStroke(lineWidth, BasicStroke.CAP_ROUND, currStroke.getLineJoin(), currStroke.getMiterLimit(), currStroke.getDashArray(), currStroke.getDashPhase());
      if (stroke.equals("SQUARE"))
        newS = new BasicStroke(lineWidth, BasicStroke.CAP_SQUARE, currStroke.getLineJoin(), currStroke.getMiterLimit(), currStroke.getDashArray(), currStroke.getDashPhase());
      currStroke = newS;
      panel.setStroke(newS);
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("strokeLineJoin:StrokeLineJoin.")) {
      String stroke = text.replaceAll("\t", "").replaceAll("\\s ", "").replaceAll("strokeLineCap:StrokeLineJoin.", "");
      BasicStroke newS = new BasicStroke(currStroke.getLineWidth());
      if (stroke.equals("MITER"))
        newS = new BasicStroke(currStroke.getLineWidth(), currStroke.getEndCap(), BasicStroke.JOIN_MITER, currStroke.getMiterLimit(), currStroke.getDashArray(), currStroke.getDashPhase());
      if (stroke.equals("ROUND"))
        newS = new BasicStroke(currStroke.getLineWidth(), currStroke.getEndCap(), BasicStroke.JOIN_ROUND, currStroke.getMiterLimit(), currStroke.getDashArray(), currStroke.getDashPhase());
      if (stroke.equals("BEVEL"))
        newS = new BasicStroke(currStroke.getLineWidth(), currStroke.getEndCap(), BasicStroke.JOIN_BEVEL, currStroke.getMiterLimit(), currStroke.getDashArray(), currStroke.getDashPhase());

      currStroke = newS;
      panel.setStroke(newS);
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("strokeMiterLimit:")) {
      String strokeWidth = text.replaceAll("\t", "").replaceAll(" ", "").replaceAll("strokeMiterLimit:", "");
      strokeWidth = strokeWidth.replaceAll("\\[", "");
      strokeWidth = strokeWidth.replaceAll("\\]", "");
      BasicStroke newS = new BasicStroke(currStroke.getLineWidth(), JavaFXToJavaVectorGraphic.currStroke.getEndCap(), currStroke.getLineJoin(), Float.parseFloat(strokeWidth), currStroke.getDashArray(), currStroke.getDashPhase());
      currStroke = newS;
      panel.setStroke(newS);
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("strokeDashArray:")) {
      String strokeWidth = text.replaceAll("\t", "").replaceAll(" ", "").replaceAll("strokeDashArray:", "");
      strokeWidth = strokeWidth.replaceAll("\\[", "");
      strokeWidth = strokeWidth.replaceAll("\\]", "");
      if (strokeWidth.equals(""))
        return;
      String[] array = strokeWidth.split("[;,]");
      float[] dashArray = new float[array.length];
      for (int i = 0; i < array.length; i++) {
        try {
          dashArray[i] = Float.parseFloat(array[i]);
        } catch (java.lang.NumberFormatException ex) {
          return;
        }
      }
      BasicStroke newS = new BasicStroke(currStroke.getLineWidth(), currStroke.getEndCap(), currStroke.getLineJoin(), currStroke.getMiterLimit(), dashArray, currStroke.getDashPhase());
      currStroke = newS;
      panel.setStroke(newS);
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("MoveTo{")) {
      moveTo = true;
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("LineTo{")) {
      lineTo = true;
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("CubicCurveTo{")) {
      curveTo = true;
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("x:")) {
      x = Float.parseFloat(text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("x:", ""));
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("controlX1:")) {
      contx1 = Float.parseFloat(text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("controlX1:", ""));
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("controlY1:")) {
      conty1 = Float.parseFloat(text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("controlY1:", ""));
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("controlX2:")) {
      contx2 = Float.parseFloat(text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("controlX2:", ""));
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("controlY2:")) {
      conty2 = Float.parseFloat(text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("controlY2:", ""));
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("y:")) {
      double y = Float.parseFloat(text.replaceAll("\t", "").replaceAll("\\s", "").replaceAll("y:", ""));
      if (moveTo) {
        path.moveTo(x, y);
        moveTo = false;
      }

      if (lineTo) {
        path.lineTo(x, y);
        lineTo = false;
      }

      if (curveTo) {
        path.curveTo(contx1, conty1, contx2, conty2, x, y);
        curveTo = false;
      }
      return;
    }

    if (text.replaceAll("\t", "").replaceAll("\\s", "").startsWith("Path{")) {

      if (path != null && fPath)
        panel.draw(path);
      path = new java.awt.geom.Path2D.Double();
      fPath = true;
    }
  }
  /**
   * gives the name of the pattern corresponding to the index indicated by number
   * @param number the index
   * @return the name of the pattern
   */
  public static String getPattern(int number) {
    int index = number;
    return patternsIn.get(index);
  }
  /**
   * gives the index of the pattern that of the filling of the Vertex + 1; if no pattern is selected, return 0
   * @param vertex of whom the fillings pattern has to be given
   * @return the index + 1 of the pattern, 0 if no pattern
   */
  public static int getIndexOfPattern(Vertex vertex) {
    if (vertex.filling.name != null) {
      String name = vertex.filling.name;
      for (int i = 0; i < patternsIn.size(); i++) {
        if (name.toLowerCase().equals(patternsIn.get(i).toLowerCase() + ".fx"))
          return i + 1;
      }
    }
    return 0;
  }
  /**
   * gives the index of the pattern that of the filling of the Shape + 1; if no pattern is selected, return 0
   * @param vertex shape of whom the fillings pattern has to be given
   * @return the index + 1 of the pattern, 0 if no pattern
   */
  public static int getIndexOfPatternS(Shaped vertex) {
    if (vertex.fill.name != null) {
      String name = vertex.fill.name;
      for (int i = 0; i < patternsIn.size(); i++) {
        if (name.toLowerCase().equals(patternsIn.get(i).toLowerCase() + ".fx"))
          return i + 1;
      }
    }
    return 0;
  }
  /**
   * checks if a pattern is already loaded
   * @param name the name of the pattern to check whether or not it is loaded
   * @return true if pattern exists and false otherwise
   */
  public static boolean checkIfExist(String name) {
    if (patternsIn.contains(name))
      return true;
    if (patternsOut.contains(name))
      return true;
    for (String pattern : patternsIn) {
      if (pattern.toLowerCase().equals(name.replace(".fx", "")))
        return true;
    }
    return false;
  }
}
