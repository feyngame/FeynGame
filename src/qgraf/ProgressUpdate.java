package qgraf;

/**
 * A callback interface that can be passed to 
 * {@link QgrafOutputReader#parseOutput(StyleNodeIterator, java.util.function.LongSupplier, ProgressUpdate, boolean, boolean)}
 * to receive updates about parsing progress.
 * 
 * @author Lars Bündgen
 */
public interface ProgressUpdate {
  
  /**
   * Initializes the parse progress meter (e.g. the maximum value of a progress bar).
   * @param fileLength The maximum value that the file progress will assume.
   */
  void init(long fileLength);
  
  /**
   * Updates the parsing progress.
   * @param filePointer The current value of the progress meter.
   * @param diagramCount The number of diagrams that have been completely parsed so far.
   * @param pathCount The current number of active paths.
   * @param peakPathCount The maximum number of paths that were ever active at the same time.
   * @param lineCount The number of lines in the line buffer.
   * @param peakLineCount The maximum number of lines that were ever in the line buffer at the same time.
   */
  void update(long filePointer, int diagramCount, int pathCount, int peakPathCount, int lineCount, int peakLineCount);
  
  /**
   * Called when file parsing was cancelled.
   */
  void cancel();
  
  /**
   * Called when the parser emits a log message. The default implementation will discard the message.
   * @param message The log message.
   */
  default void acceptLogMessage(String message) {}
  
  /**
   * A {@link ProgressUpdate} instance that ignores all calls.
   */
  public static final ProgressUpdate VOID = new ProgressUpdate() {
    @Override
    public void update(long filePointer, int diagramCount, int pathCount, int peakPathCount, int lineCount,
        int peakLineCount) {}
    @Override
    public void init(long fileLength) {}
    @Override
    public void cancel() {}
  };
}