package qgraf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.LongSupplier;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jgrapht.Graph;
import org.jgrapht.graph.DirectedPseudograph;

import qgraf.QgrafDiagram.EdgeType;
import qgraf.QgrafDiagram.GraphEdge;
import qgraf.QgrafDiagram.GraphVertex;
import qgraf.QgrafDiagram.VertexType;
import qgraf.QgrafStyleNode.KeywordName;
import qgraf.QgrafStyleNode.LoopName;
import qgraf.QgrafStyleNode.NodeContentType;
import qgraf.QgrafStyleNode.StyleNodeContent;
import qgraf.StyleNodeIterator.NodePath;
import resources.Java8Support;
import ui.Frame;

class QgrafOutputReader {
  private static final Logger LOGGER = Frame.getLogger(QgrafOutputReader.class);
  
  static class ProgressFileReader implements AutoCloseable, IOStringSupplier {
    private final RandomAccessFile file;
    private final BufferedReader reader;
    
    ProgressFileReader(File path) throws IOException {
      this.file = new RandomAccessFile(path, "r");
      this.reader = new BufferedReader(new FileReader(file.getFD()));
    }
    
    ProgressFileReader(Path path) throws IOException {
      this.file = new RandomAccessFile(path.toFile(), "r");
      this.reader = new BufferedReader(new FileReader(file.getFD()));
    }
    
    @Override
    public void close() throws IOException {
      file.close();
      reader.close();
    }
    
    public BufferedReader getReader() {
      return reader;
    }
    
    public long getPointer() {  //has to be a LongSupplier, so no IOException
      try {
        return file.getFilePointer();
      } catch (IOException e) {
        return -1;
      }
    }
    
    public long getSize() throws IOException {
      return file.length();
    }
    
    public FileDescriptor getFD() throws IOException {
      return file.getFD();
    }

    @Override
    public String nextLine() throws IOException {
      return reader.readLine();
    }
    
  }
  
  static QgrafOutputFile toOutputFile(String name, QgrafOutputBuilder builder, ModelData model) throws QgrafImportException {
    List<QgrafDiagram> diagrams = new ArrayList<>();
    
    if(builder.getLoadOnlyIndex() == -1) {
      for(QgrafOutputBuilder.DiagramData dd : builder.diagrams()) {
        try {
          diagrams.add(toGraph(dd, model));
        } catch (BuilderInvalidException e) {
          throw new QgrafImportException("Cannot convert builder to graph", e);
        }
      }
      return new QgrafOutputFile(name, diagrams, model);
    } else {
      QgrafOutputBuilder.DiagramData onlyData = builder.getDiagram(builder.getLoadOnlyIndex());
      if(onlyData == null) return null;
      QgrafDiagram onlyDiagram;
      try {
        onlyDiagram = toGraph(onlyData, model);
      } catch (BuilderInvalidException e) {
        throw new QgrafImportException("Cannot convert builder to graph", e);
      }
      return new QgrafOutputFile(name, onlyDiagram, builder.getLoadOnlyIndex(), model);
    }
  }
  
  
  static QgrafDiagram toGraph(QgrafOutputBuilder.DiagramData diagram, ModelData model) throws BuilderInvalidException {
    
    class FieldLinkage {
      
      public FieldLinkage(QgrafOutputBuilder.LegData data, boolean in) {
        this(data.vertexIndex, data.rayIndex, data.fieldIndex, data.field, data.dualField, in ? 1 : 2, data.momentum, data.dualMomentum);
      }
      
      public FieldLinkage(QgrafOutputBuilder.PropagatorOrRayData data, boolean prop, boolean dual, boolean swapFields) {
        this(dual ? data.dualVertexIndex : data.vertexIndex,
             dual ? data.dualRayIndex : data.rayIndex,
             dual ? data.dualFieldIndex : data.fieldIndex,
             dual ^ swapFields ? data.dualField : data.field,
             dual ^ swapFields ? data.field : data.dualField,
             prop ? 3 : 0,
             dual ^ swapFields ? data.dualMomentum : data.momentum,
             dual ^ swapFields ? data.momentum : data.dualMomentum); //TODO CHECK IF SWAPFIELDS IS NEEDED
      }
      
      public FieldLinkage(int vertexIndex, int rayIndex, int fieldIndex, String field, String dualField, int fieldType, String momentum, String dualMomentum) {
        this.vertexIndex = vertexIndex;
        this.rayIndex = rayIndex;
        this.fieldIndex = fieldIndex;
        this.field = field;
        this.dualField = dualField;
        this.fieldType = fieldType;
        this.momentum = momentum;
        this.dualMomentum = dualMomentum;
      }
      
      int vertexIndex;
      int rayIndex;
      int fieldIndex;
      String field;
      String dualField;
      String momentum;
      String dualMomentum;
      int fieldType;
      
      void merge(FieldLinkage other) throws BuilderInvalidException {
        this.vertexIndex = setInt(this.vertexIndex, other.vertexIndex);
        this.rayIndex = setInt(this.rayIndex, other.rayIndex);
        this.fieldIndex = setInt(this.fieldIndex, other.fieldIndex);
        this.fieldType = setInt(this.fieldType, other.fieldType);
        this.field = setString(this.field, other.field);
        this.dualField = setString(this.dualField, other.dualField);
        this.momentum = setString(this.momentum, other.momentum);
        this.dualMomentum = setString(this.dualMomentum, other.dualMomentum);
      }
      
      int setInt(int oldValue, int newValue) throws BuilderInvalidException {
        if(newValue == 0) return oldValue;
        if(oldValue == 0) return newValue;
        if(newValue != oldValue) throw new BuilderInvalidException("cannot set field value");
        return newValue;
      }
      
      String setString(String oldValue, String newValue) throws BuilderInvalidException {
        if(newValue == null) return oldValue;
        if(oldValue == null) return newValue;
        if(!Objects.equals(newValue, oldValue)) throw new BuilderInvalidException("cannot set field value");
        return newValue;
      }
    }

    List<FieldLinkage> unlinkedFields = new ArrayList<>();
    List<FieldLinkage> linkedFields = new ArrayList<>();
    Map<Integer, Boolean> propagatorIndexUsed = new HashMap<>();
    
    for(QgrafOutputBuilder.LegData inLeg : diagram.inLegs()) {
      model.registerDualPair(inLeg.field, inLeg.dualField);
      FieldLinkage fl = new FieldLinkage(inLeg, true);
      if(fl.vertexIndex != 0) {
        linkedFields.add(fl);
      } else {
        unlinkedFields.add(fl);
      }
    }
    
    for(QgrafOutputBuilder.LegData outLeg : diagram.outLegs()) {
      model.registerDualPair(outLeg.field, outLeg.dualField);
      FieldLinkage fl = new FieldLinkage(outLeg, false);
      if(fl.vertexIndex != 0) {
        linkedFields.add(fl);
      } else {
        unlinkedFields.add(fl);
      }
    }
    
    for(QgrafOutputBuilder.PropagatorOrRayData prop : diagram.propagators()) {
      model.registerDualPair(prop.field, prop.dualField);
      FieldLinkage fl1 = new FieldLinkage(prop, true, false, false);
      if(fl1.vertexIndex != 0) {
        linkedFields.add(fl1);
      } else {
        unlinkedFields.add(fl1);
      }
      FieldLinkage fl2 = new FieldLinkage(prop, true, true, false);
      if(fl2.vertexIndex != 0) {
        linkedFields.add(fl2);
      } else {
        unlinkedFields.add(fl2);
      }
    }
    
    for(QgrafOutputBuilder.VertexData vert : diagram.vertices()) {
      for(QgrafOutputBuilder.PropagatorOrRayData ray : vert.rays()) {
        model.registerDualPair(ray.field, ray.dualField);
        boolean isOutLine = (ray.fieldIndex < 0 && ray.fieldIndex % 2 == 0) 
            || (ray.propagatorIndex < 0 && ray.propagatorIndex % 2 == 0);
        if(ray.fieldIndex == 0 && ray.propagatorIndex == 0) {
          LOGGER.warning("cannot find prop/field index on ray data");
          continue; //TODO maybe just a warning, since this is not always a fatal error
        }
        FieldLinkage fl = new FieldLinkage(ray, false, false, isOutLine);
        if(fl.fieldIndex != 0) {
          linkedFields.add(fl);
        } else if(ray.propagatorIndex != 0) { //Special case. just assign the field index in any way
          if(diagram.isPropagatorDataEmpty()) {
            if(ray.propagatorIndex < 0) {
              fl.fieldIndex = ray.propagatorIndex;
            } else {
              if(propagatorIndexUsed.getOrDefault(ray.propagatorIndex, false)) { //Create second field
                fl.fieldIndex = 2*ray.propagatorIndex;
                linkedFields.add(fl);
              } else { //create first field
                fl.fieldIndex = 2*ray.propagatorIndex - 1;
                linkedFields.add(fl);
                propagatorIndexUsed.put(ray.propagatorIndex, true);
              }
            }
          } else { //compare with prop data
            FieldLinkage fl1 = null;
            FieldLinkage fl2 = null;
            if(ray.propagatorIndex < 0) {
              for(FieldLinkage f : unlinkedFields) { //just check these, if it is already linked we don't care
                if(f.fieldIndex == ray.propagatorIndex - 1) fl1 = f;
              }
              if(fl1 != null && Objects.equals(fl1.field, fl.field)) {
                fl.fieldIndex = fl1.fieldIndex;
                linkedFields.add(fl);
              }
            } else {
              for(FieldLinkage f : unlinkedFields) { //just check these, if it is already linked we don't care
                if(f.fieldIndex == 2*ray.propagatorIndex - 1) fl1 = f;
                if(f.fieldIndex == 2*ray.propagatorIndex) fl2 = f;
              }
              if(fl1 != null && Objects.equals(fl1.field, fl.field)) {
                fl.fieldIndex = fl1.fieldIndex;
                linkedFields.add(fl);
              } else if(fl2 != null && Objects.equals(fl2.field, fl.field)) {
                fl.fieldIndex = fl2.fieldIndex;
                linkedFields.add(fl);
              }
            }
          }
          
        } else {
          unlinkedFields.add(fl);
        }
      }
    }
    
    //Now, try to link up the unlinked information
    for(FieldLinkage unlinked : unlinkedFields) {
      if(unlinked.fieldIndex != 0) {
        //find a linked field with same field index and merge
        boolean success = false;
        for(FieldLinkage linked : linkedFields) {
          if(linked.fieldIndex == unlinked.fieldIndex) {
            linked.merge(unlinked);
            success = true;
            break;
          }
        }
        if(!success) LOGGER.warning("cannot find linked field 1");
        
      } else if(unlinked.vertexIndex != 0 && unlinked.rayIndex != 0) {
          //find a linked field with same field index and merge
          boolean success = false;
          for(FieldLinkage linked : linkedFields) {
            if(linked.vertexIndex == unlinked.vertexIndex && linked.rayIndex == unlinked.rayIndex) {
              linked.merge(unlinked);
              success = true;
              break;
            }
          }
          //It is okay to fail here
          if(!success) LOGGER.warning("cannot find linked field 2");
      } else {
        LOGGER.severe("unlinked fields with incomplete info");
      }
    }
    
    //Finally, fields my still appear twice in the list at this point
    //Use an iterator because the duplicates will be removed after merging
    Map<Integer, FieldLinkage> encounteredFields = new HashMap<>();
    Iterator<FieldLinkage> it = linkedFields.iterator();
    while(it.hasNext()) {
      FieldLinkage fl = it.next();
      if(encounteredFields.containsKey(fl.fieldIndex)) {
        FieldLinkage other = encounteredFields.get(fl.fieldIndex);
        other.merge(fl);
        it.remove();
      } else {
        encounteredFields.put(fl.fieldIndex, fl);
      }
    }
    
    //Then, create the graph structure
    
    Graph<GraphVertex, GraphEdge> graph = new DirectedPseudograph<>(GraphEdge.class);
    Map<Integer, GraphVertex> vertices = new HashMap<>();
    Map<Integer, FieldLinkage> incompletePropagators = new HashMap<>();
    
    for(FieldLinkage field : linkedFields) {
      if(field.fieldIndex > 0) { //part of propagator
        assert field.fieldType == 3 || field.fieldType == 0;
        int propagatorIndex = (field.fieldIndex + 1) / 2;
        if(incompletePropagators.containsKey(propagatorIndex)) {
          FieldLinkage other = incompletePropagators.get(propagatorIndex);
          FieldLinkage norm, dual;
          if(other.fieldIndex % 2 == 0) {
            assert field.fieldIndex % 2 != 0;
            norm = field;
            dual = other;
          } else {
            assert field.fieldIndex % 2 == 0;
            norm = other;
            dual = field;
          }
          
          if(norm.field != null & dual.dualField != null && !Objects.equals(norm.field, dual.dualField)) 
            throw new BuilderInvalidException("Incompatible identfiers"); 
          if(norm.dualField != null & dual.field != null  && !Objects.equals(norm.dualField, dual.field)) 
            throw new BuilderInvalidException("Incompatible identfiers");
          if(norm.field == null) norm.field = dual.dualField;
          if(dual.field == null) dual.field = norm.dualField;
          
          GraphVertex v1 = vertices.computeIfAbsent(norm.vertexIndex, i ->
            new GraphVertex(VertexType.INTERNAL, model.defaultVertexStyle(), 0));
          GraphVertex v2 = vertices.computeIfAbsent(dual.vertexIndex, i -> 
            new GraphVertex(VertexType.INTERNAL, model.defaultVertexStyle(), 0));
          GraphEdge e = new GraphEdge(EdgeType.PROPAGATOR, norm.field, dual.field,
              model.getLineStyle(norm.field, dual.field, norm.momentum, dual.momentum));
          graph.addVertex(v1);
          graph.addVertex(v2);
          graph.addEdge(v2, v1, e);
        } else {
          incompletePropagators.put(propagatorIndex, field);
        }
      } else if(field.fieldIndex < 0 && field.fieldIndex % 2 == 0) { //out leg
        assert field.fieldType == 2 || field.fieldType == 0;
        int outIndex = -(field.fieldIndex / 2);
        
        GraphVertex x = new GraphVertex(VertexType.EXTERNAL_OUT, model.defaultVertexStyle(), outIndex);
        GraphVertex v = vertices.computeIfAbsent(field.vertexIndex, i ->
          new GraphVertex(VertexType.INTERNAL, model.defaultVertexStyle(), 0));
        GraphEdge e = new GraphEdge(EdgeType.OUT_LEG, field.field, field.dualField,
            model.getLineStyle(field.field, field.dualField, field.momentum, field.dualMomentum));
        graph.addVertex(v);
        graph.addVertex(x);
        graph.addEdge(v, x, e);
      } else if(field.fieldIndex < 0 && field.fieldIndex % 2 != 0) { //In leg
        assert field.fieldType == 1 || field.fieldType == 0;
        int inIndex = -((field.fieldIndex-1) / 2);
        GraphVertex x = new GraphVertex(VertexType.EXTERNAL_IN, model.defaultVertexStyle(), inIndex);
        GraphVertex v = vertices.computeIfAbsent(field.vertexIndex, i ->
          new GraphVertex(VertexType.INTERNAL, model.defaultVertexStyle(), 0));
        GraphEdge e = new GraphEdge(EdgeType.IN_LEG, field.field, field.dualField,
            model.getLineStyle(field.field, field.dualField, field.momentum, field.dualMomentum));
        graph.addVertex(v);
        graph.addVertex(x);
        graph.addEdge(x, v, e);
      }
    }
    
    return new QgrafDiagram(graph, model, diagram.getSourceText());
  }
  
  static QgrafOutputBuilder parseOutputFromString(String file, QgrafStyleFile style, ProgressUpdate update,
      boolean allowIterruptions, boolean diagramMode)
      throws QgrafImportException, InterruptedException {
    BufferedReader br = new BufferedReader(new StringReader(file));
    StyleNodeIterator sni = style.iterator(br::readLine, QgrafOutputFile.LOAD_ALL_INDEX_VALUE);
    try {
      return parseOutput(sni, () -> -1, update, allowIterruptions, diagramMode);
    } catch (IOException e) {
      throw new RuntimeException("Should be impossible", e);
    }
  }
  
  static QgrafOutputBuilder parseOutputFromFile(Path file, QgrafStyleFile style, int loadOnlyBase0, ProgressUpdate update,
      boolean allowIterruptions, boolean diagramMode)
      throws IOException, QgrafImportException, InterruptedException {
    try (ProgressFileReader pfr = new ProgressFileReader(Objects.requireNonNull(file))) {
      StyleNodeIterator sni = style.iterator(pfr, loadOnlyBase0);
      update.init(Files.size(file));
      return parseOutput(sni, pfr::getPointer, update, allowIterruptions, diagramMode);
    }
  }
  
  static QgrafOutputBuilder parseOutput(StyleNodeIterator iter, LongSupplier filePointer, ProgressUpdate update, 
      boolean allowIterruptions, boolean diagramMode)
      throws IOException, QgrafImportException, InterruptedException {
    
    Set<QgrafOutputBuilder> finishedPaths = new HashSet<>();
    
    NodePath current;
    while((current = iter.pollNextPath()) != null) {
      if(iter.pathCount() >= 200) throw new QgrafImportException("Too many paths");
      
      if(Thread.interrupted()) {
        if(allowIterruptions) {
          throw new InterruptedException();
        } else {
          Thread.currentThread().interrupt();
        }
      }
      
      //Define all thze context variables for easier access
      final IOStringSupplier lines = current.contextLineSupplier();
      final QgrafOutputBuilder builder = current.contextOutputBuilder();
      final StringBuilder incompleteLine = current.contextIncompleteLine();
      
      update.update(filePointer.getAsLong(), builder.getDiagramCount(), iter.pathCount(),
          iter.maxPathCount(), iter.getLineSupplier().getBufferSize(), iter.getLineSupplier().getMaxBufferSize());
      
      int cachedBreaks = countLineBreaks(incompleteLine);
      int requiredBreaks = current.literalLinebreaks() + 1; //Always request an extra line?
      
      if(cachedBreaks < requiredBreaks) { //load additional breaks
        int delta = requiredBreaks - cachedBreaks;
        for(int i = 0; i < delta; i++) {
          String line = lines.nextLine();
          if(line == null) {
            update.acceptLogMessage("Not enough lines for path");
          } else {
            incompleteLine.append(line).append(QgrafStyleNode.LINEBREAK);
          }
        }
      }
      
      Pattern regex = current.getRegex("^", null); //match start but not end
      if(regex == null) {
        if(current.isEndOfStyle()) {
          regex = Pattern.compile("^"); //Matches anything
        } else {
          update.acceptLogMessage("Cannot create regex for empty path " + current.printContent());
          continue;
        }
      }
      
//      update.acceptLogMessage("\nMatching '" + regex + "'\nfor\n'" + current.printContent() + "'\nto\n'" + incompleteLine + "'\nstatus: "
//          + current.isEndOfStyle() + "\n");
      Matcher matcher = regex.matcher(incompleteLine);
      if(matcher.find()) {
        int afterEndOfMatch = matcher.end();
        
        boolean invalidFlag = false;
        int groupIndex = 1;
        for(StyleNodeContent snc : current) {
          try {
            if(snc instanceof LoopName) {
              builder.pushRepeatLoop((LoopName) snc);
            } else if(snc == StyleNodeContent.EXIT) {
              builder.pushExitCurrentLoop();
            } else if(snc.getType() == NodeContentType.DATA && snc instanceof KeywordName) {
              String value = matcher.group(groupIndex++);
              builder.pushKeywordValue((KeywordName) snc, value);
            } else if(snc.getType() == NodeContentType.DATA) {
              String value = matcher.group(groupIndex++);
              builder.pushFunctionValue(snc.getData(), value);
            }
          } catch (BuilderInvalidException e) {
            update.acceptLogMessage("invalid node path: " + e.getMessage());
            invalidFlag = true;
            break;
          }
        }
        
        builder.pushSourceTextLine(incompleteLine.substring(0, afterEndOfMatch));
        //Matcher reads through to the builder, only delete after all groups are read
        incompleteLine.delete(0, afterEndOfMatch);
        
        if(invalidFlag) {
          update.acceptLogMessage("Discarded branch: invalid builder");
        } else {
          if(builder.canExitEarly()) {
            update.acceptLogMessage("Exit early");
            return builder;
          }
          
          if(current.isEndOfStyle()) {
          //If it is finished AND there is no more content
            String nl = lines.nextLine();
            if(incompleteLine.length() == 0 && nl == null) {
              finishedPaths.add(builder);
              update.acceptLogMessage("Finished branch");
            } else { //Check if all the remaining content is whitespace
              String s = incompleteLine.toString();
              if(Java8Support.isBlank(s) && (nl == null || Java8Support.isBlank(nl)) 
                  && IOStringSupplier.isRemainingFileBlank(lines)) { //Then, check if the remaining lines are also withespace?
                //Actually, only check the next line to avoid loading more stuff
                finishedPaths.add(builder);
                update.acceptLogMessage("Finished branch with whitespace left");
              } else {
                update.acceptLogMessage("Discarded finish branch: more content");
              }
            }
          } else {
            iter.addAsNewPath(current);
            update.acceptLogMessage("Continued branch");
          }
        }
      } else {
        update.acceptLogMessage("Discarded branch: no match");
      }
      
    }
    
    if(finishedPaths.size() == 0) {
      update.acceptLogMessage("No branch remains");
      return null;
    } else if(finishedPaths.size() == 1) {
      update.acceptLogMessage("Found 1 path");
      return finishedPaths.iterator().next();
    } else {
      update.acceptLogMessage("Too many options " + finishedPaths.size());
      LOGGER.warning("Found more than one possible path, result might not be accurate");
      return finishedPaths.iterator().next(); //Just any path
    }
  }
  
  static int countLineBreaks(String s) {
    int index = -1;
    int count = 0;
    while((index = s.indexOf(QgrafStyleNode.LINEBREAK, index+1)) != -1) count++;
    return count;
  }
  
  static int countLineBreaks(StringBuilder s) {
    int index = 0;
    int count = 0;
    while((index = s.indexOf(QgrafStyleNode.LINEBREAK, index+1)) != -1) count++;
    return count;
  }
  
}
