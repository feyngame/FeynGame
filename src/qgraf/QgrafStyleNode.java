package qgraf;

import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import ui.Frame;

/**
 * Type of vertices in the graph representation of a style file.
 * The vertices represent the items that appear in the output, such as literals, keywords, etc;
 * or loop vertices.
 * 
 * @author Lars Bündgen
 */
public class QgrafStyleNode {
  private static final Logger LOGGER = Frame.getLogger(QgrafStyleNode.class);
  
  private static final String REGEX_PRINTABLE = "[ -~]*";
  private static final String REGEX_FUNCTION_CONTENT = REGEX_PRINTABLE;
  protected static final String REGEX_GENERATED = REGEX_PRINTABLE;
  private static final String REGEX_IDENTIFIER = "[a-zA-Z][A-Za-z0-9_]*";
  private static final String REGEX_NUMBER = "(?:\\+|-)?[0-9]+";
  private static final String REGEX_MINUS = "-?";
  private static final String REGEX_SIGN = "(?:\\+|-)";
  private static final String REGEX_ZEROONE = "[01]";
  private static final String REGEX_FRACTION = "(?:\\+|-)?[0-9]+(?:\\/[0-9]+)?";
  private static final String REGEX_MOMENTUM = "(?:(?:(?:\\+|-)?[a-zA-Z][A-Za-z0-9_]*)+|0)";
  
  protected static boolean canMatchSafeChars(String regex) {
    return REGEX_PRINTABLE.equals(regex);
  }
  
  /**
   * The string to be used when matching linebreaks.
   */
  public static final String LINEBREAK = "\n";
  
  /**
   * Common interface of all possible contents of a style node. These can be keywords, literals and functions
   * as well as some special generated instances.
   * 
   * @author Lars Bündgen
   */
  public static interface StyleNodeContent {
    
    /**
     * Returns the data stored in the content implementations, such as the keyword name or the literal value.
     * @return The data stored in the content implementations, such as the keyword name or the literal value.
     */
    public String getData();
    
    /**
     * Returns a regular experssion that will match the valid values for this style node.
     * @return A regular experssion that will match the valid values for this style node.
     */
    public String getRegex();
    
    /**
     * Returns the type of this content node.
     * @return The type of this content node.
     */
    public NodeContentType getType();

    /**
     * Creates a completely custom instance of {@link StyleNodeContent}. 
     * @param content The content of the style node.
     * @param regex The regular experssion that will match the content.
     * @param type The {@link NodeContentType} of the style node.
     * @return The custom {@link StyleNodeContent} instance.
     */
    public static StyleNodeContent custom(String content, String regex, NodeContentType type) {
      Objects.requireNonNull(content);
      Objects.requireNonNull(regex);
      Objects.requireNonNull(type);
      
      return new StyleNodeContent() {
        @Override public NodeContentType getType() {
          return type;
        }
        @Override public String getRegex() {
          return regex;
        }        
        @Override public String getData() {
          return content;
        }
        @Override public String toString() {
          return "SNC[type=" + type + ", content='" + content.replaceAll("\n", "\\\\n") + "']";
        }
      };
    }
    
    /**
     * Creates a {@link StyleNodeContent} instance that represents a user-defined function.
     * @param functionName The name of the function.
     * @return The style node instance for the function.
     */
    public static StyleNodeContent forFunction(String functionName) {
      return custom(functionName, REGEX_FUNCTION_CONTENT, NodeContentType.DATA);
    }
    
    /**
     * Creates a {@link StyleNodeContent} instance that represents a literal in the style file.
     * @param literal The literal string.
     * @return The style node instance for the literal.
     */
    public static StyleNodeContent forLiteral(String literal) {
      return custom(literal, literal, NodeContentType.LITERAL);
    }
    
    /**
     * Creates a {@link StyleNodeContent} instance for an entire loop that does not capture the loop content.
     * <p>
     * This is currently not implemented.
     * @param loop The name of the loop that is not captured.
     * @param content The nodes that are the content of the loop.
     * @return A style node instance that matches the loop an arbitrary amount of times, but contains no
     * capturing groops.
     * @throws UnsupportedOperationException Always.
     */
    public static StyleNodeContent forNonCapture(LoopName loop, List<StyleNodeContent> content) {
      throw new UnsupportedOperationException("not implemented");
//      return custom("<non-capture-loop>", REGEX_PRINTABLE, NodeContentType.GENERATED);
    }
    
    /**
     * A special {@link StyleNodeContent} instance that represents the {@code <back>} keyword.
     * It will not have data or a regex associated with it and must be processed before the regex for a node path
     * can be generated.
     */
    public static final StyleNodeContent BACK = new StyleNodeContent() {
      @Override public NodeContentType getType() {
        return NodeContentType.BACK;
      }
      @Override public String getRegex() {
        throw new UnsupportedOperationException();
      }        
      @Override public String getData() {
        throw new UnsupportedOperationException();
      }
      
      @Override public String toString() {
        return "[BACK]";
      }
    };
    
    /**
     * A special {@link StyleNodeContent} instance that represents the start of the style file.
     * It has {@code null} as data and has no regex associated with it.
     */
    public static final StyleNodeContent START = new StyleNodeContent() {
      @Override public NodeContentType getType() {
        return null; 
      }
      @Override public String getRegex() {
        throw new UnsupportedOperationException();
      }        
      @Override public String getData() {
        return null;
      }
      
      @Override public String toString() {
        return "[START]";
      }
    };
    
    /**
     * A special {@link StyleNodeContent} instance that represents the end of the style file.
     * It has {@code null} as data and has no regex associated with it.
     */
    public static final StyleNodeContent EXIT = new StyleNodeContent() {
      @Override public NodeContentType getType() {
        return null; 
      }
      @Override public String getRegex() {
        throw new UnsupportedOperationException();
      }        
      @Override public String getData() {
        return null;
      }
      
      @Override public String toString() {
        return "[EXIT]";
      }
    };
    
  }
  
  /**
   * The possible types of style node content.
   * 
   * @author Lars Bündgen
   */
  public static enum NodeContentType {
    /**
     * Used for style nodes that represent literals in the style file.
     */
    LITERAL,
    /**
     * Used for style nodes that represent data keywords.
     */
    DATA,
    /**
     * Used for style nodes that represent loop keywords.
     */
    LOOP,
    /**
     * Used for style nodes that represent the special {@code <back>} keyword.
     */
    BACK,
    /**
     * Used for style nodes that do not directly appear in the style file, but were generated to
     * alter the parsing process in some way.
     */
    GENERATED;
  }
  
  /**
   * Defines {@link StyleNodeContent} implementations for all data keywords available in qgraf 3.6.5 (or 3.6.6 ?)
   * 
   * @author Lars Bündgen
   */
  @SuppressWarnings("javadoc") //These are all pretty self-explanatory, they are named just like the keyword
  public static enum KeywordName implements StyleNodeContent {
    PROGRAM("program", REGEX_PRINTABLE),
    COMMAND_DATA("command_data", REGEX_PRINTABLE),
    DIAGRAM_INDEX("diagram_index", REGEX_NUMBER),
    LEGS("legs", REGEX_NUMBER),
    LEGS_IN("legs_in", REGEX_NUMBER),
    LEGS_OUT("legs_out", REGEX_NUMBER),
    LOOPS("loops", REGEX_NUMBER),
    MINUS("minus", REGEX_MINUS),
    PROPAGATORS("propagators", REGEX_NUMBER),
    SIGN("sign", REGEX_SIGN),
    SYMMETRY_FACTOR("symmetry_factor", REGEX_FRACTION),
    SYMMETRY_NUMBER("symmetry_number", REGEX_NUMBER),
    VERTICES("vertices", REGEX_NUMBER),
    DUAL_FIELD("dual-field", REGEX_IDENTIFIER),
    DUAL_FIELD_INDEX("dual-field_index", REGEX_NUMBER),
    DUAL_MOMENTUM("dual-momentum", REGEX_MOMENTUM),
    DUAL_RAY_INDEX("dual-ray_index", REGEX_NUMBER),
    DUAL_VERTEX_DEGREE("dual-vertex_degree", REGEX_NUMBER),
    DUAL_VERTEX_INDEX("dual-vertex_index", REGEX_NUMBER),
    FIELD("field", REGEX_IDENTIFIER),
    FIELD_INDEX("field_index", REGEX_NUMBER),
    FIELD_TYPE("field_type", REGEX_NUMBER),
    FIELD_SIGN("field_sign", REGEX_SIGN),
    MOMENTUM("momentum", REGEX_MOMENTUM),
    PROPAGATOR_INDEX("propagator_index", REGEX_NUMBER),
    RAY_INDEX("ray_index", REGEX_NUMBER),
    VERTEX_DEGREE("vertex_degree", REGEX_NUMBER),
    VERTEX_INDEX("vertex_index", REGEX_NUMBER),
    IN_INDEX("in_index", REGEX_NUMBER),
    OUT_INDEX("out_index", REGEX_NUMBER),
    LEG_INDEX("leg_index", REGEX_NUMBER),
    NEW_TOPOLOGY("new_topology", REGEX_ZEROONE),
    NEW_PARTITION("new_partition", REGEX_ZEROONE),
    NEW_LOOPS("new_loops", REGEX_ZEROONE),
    NEW_ELINKS("new_elinks", REGEX_ZEROONE),
    UNKNOWN("[unknown keyword]", REGEX_PRINTABLE);
    
    private final String name;
    private final String regex;
    
    private KeywordName(String name, String regex) {
      this.name = name;
      this.regex = regex;
    }
    
    @Override
    public String getData() {
      return name;
    }

    @Override
    public String getRegex() {
      return regex;
    }

    @Override
    public NodeContentType getType() {
      return NodeContentType.DATA;
    }
    
    @Override public String toString() {
      return "KWNAME[name=" + name + "]";
    }
    
    public static KeywordName ofName(String name) {
      Objects.requireNonNull(name);
      
      for(KeywordName option : KeywordName.values()) {
        if(option.name.equals(name)) return option;
      }
      
      return null;
    }
  }
  
  /**
   * Defines {@link StyleNodeContent} implementations for all loop keywords.
   * 
   * @author Lars Bündgen
   */
  @SuppressWarnings("javadoc")
  public static enum LoopName implements StyleNodeContent {
    COMMAND_LOOP("command_loop"),
    COMMAND_LINE_LOOP("command_line_loop"),
    DIAGRAM("diagram"), //does not end with _loop, this is correct
    IN_LOOP("in_loop"),
    OUT_LOOP("out_loop"),
    PROPAGATOR_LOOP("propagator_loop"),
    VERTEX_LOOP("vertex_loop"),
    RAY_LOOP("ray_loop");

    private final String name;
    
    private LoopName(String name) {
      this.name = name;
    }
    
    @Override
    public String getData() {
      return name;
    }

    @Override
    public String getRegex() {
      throw new UnsupportedOperationException();
    }

    @Override
    public NodeContentType getType() {
      return NodeContentType.LOOP;
    }
    
    @Override public String toString() {
      return "LOOP[name=" + name + "]";
    }
    
    public static LoopName ofName(String name) {
      Objects.requireNonNull(name);
      
      for(LoopName option : LoopName.values()) {
        if(option.name.equals(name)) return option;
      }
      
      return null;
    }
  }  

  private final StyleNodeContent content;
  
  /**
   * Creates a new style node.
   * @param content The style node content.
   * @throws NullPointerException If the parameter is {@code null}.
   * @throws IllegalArgumentException If the content type of the parameter is {@link NodeContentType#GENERATED}.
   */
  public QgrafStyleNode(StyleNodeContent content) {
    this.content = Objects.requireNonNull(content);
    if(content.getType() == NodeContentType.GENERATED) throw new IllegalArgumentException();
  }
  
  /**
   * Returns the content of this style node.
   * @return The content fo this style node.
   */
  public StyleNodeContent getContent() {
    return content;
  }
  
  /**
   * The data stored in this style node.<br>
   * Shortcut for {@code getContent().getData()}.
   * @return The data stored in this style node.
   */
  public String getData() {
    return content.getData();
  }
  
  /**
   * The type of the content stored in this style node.<br>
   * Shortcut for {@code getContent().getType()}.
   * @return The type of the content stored in this style node.
   */
  public NodeContentType getContentType() {
    return content.getType();
  }

  @Override
  public String toString() {
    return "QgrafStyleNode [content=" + content + "]";
  }
  
  /**
   * Creates a new style node for a literal in a style file.
   * @param literal The literal string.
   * @return The style node for the literal.
   */
  public static QgrafStyleNode ofLiteral(String literal) {
    return new QgrafStyleNode(StyleNodeContent.forLiteral(literal));
  }
  
  /**
   * Creates a new style node for a function in a style file.
   * @param funcName The function name.
   * @return The style node for the function.
   */
  public static QgrafStyleNode ofFunction(String funcName) {
    return new QgrafStyleNode(StyleNodeContent.forFunction(funcName));
  }
  
  /**
   * Creates a new style node for a loop or data keyword. For unknown keyword names,
   * a warning is emitted and the returned node has {@link KeywordName#UNKNOWN} as content.
   * The unknown keyword matches all characters except a linebreak.
   * @param kwName The name of the keyword.
   * @return The style node for this keyword.
   */
  public static QgrafStyleNode ofKeyword(String kwName) {
    KeywordName kw = KeywordName.ofName(kwName);
    if(kw != null) {
      return new QgrafStyleNode(kw);
    } else {
      LoopName lp = LoopName.ofName(kwName);
      if(lp != null) {
        return new QgrafStyleNode(lp);
      } else {
        if(kwName.equals("back")) {
          return new QgrafStyleNode(StyleNodeContent.BACK);
        } else {
          LOGGER.severe("Encountered unknown keyword " + kwName);
          return new QgrafStyleNode(KeywordName.UNKNOWN);
        }
      }
    }
  }
  
  /**
   * Creates a new style node for the start of the style graph.
   * @return The style node for the start of the style graph.
   */
  public static QgrafStyleNode start() {
    return new QgrafStyleNode(StyleNodeContent.START);
  }
  
}
