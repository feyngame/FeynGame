package qgraf;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.alg.drawing.RandomLayoutAlgorithm2D;
import org.jgrapht.alg.drawing.model.Box2D;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.MapLayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;
import org.jgrapht.graph.AsUnmodifiableGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;

import game.FeynLine;
import game.Vertex;
import layout.FeynGameDiagramStructureException;
import layout.GraphLayoutAlgorithm;
import layout.GraphLayoutException;
import qgraf.ModelData.LineStyle;
import qgraf.ModelData.VertexStyle;

/**
 * This class contains a single Feynman diagram represented as {@link DirectedPseudograph}. It also stores the model associated with
 * the diagram. It also stores the vertex positions in a {@link LayoutModel2D} if they are available.
 * 
 * @author Lars Bündgen
 */
public class QgrafDiagram {

  /**
   * The class that is used as the vertex type for the graph.
   * Contains information about the {@link VertexType} as well as a {@link VertexStyle}.
   */
  public static class GraphVertex {
    private final VertexType type;
    private final VertexStyle style;
    private final int externalIndex;
    
    /**
     * Creates a {@link GraphVertex} with a type and the default vertex style.
     * @param type The vertex type.
     */
    public GraphVertex(VertexType type, int externalIndex) {
      this.type = type;
      this.style = new VertexStyle(new game.Vertex(new game.Point2D()));
      this.externalIndex = externalIndex;
    }
    
    /**
     * Creates a {@link GraphVertex} with a type and a {@link VertexStyle}.
     * @param type The vertex type.
     * @param style The vertex style.
     */
    public GraphVertex(VertexType type, VertexStyle style, int externalIndex) {
      this.type = type;
      this.style = style;
      this.externalIndex = externalIndex;
    }
    
    /**
     * Returns the vertex type.
     * @return the vertex type.
     */
    public VertexType getType() {
      return type;
    }
    
    /**
     * Returns the vertex style.
     * @return the vertex style.
     */
    public VertexStyle getStyle() {
      return style;
    }
    
    public int getExternalIndex() {
      return externalIndex;
    }
  }
  
  /**
   * The class that is used as the edge type of the graph. Extends {@link DefaultEdge}.
   * Contains information about the edge type and the line style and the identifier of the edge.
   */
  public static class GraphEdge extends DefaultEdge implements Comparable<GraphEdge> {
    private static final long serialVersionUID = -8056540850700861584L;
    
    private final EdgeType type;
    private final String identifier;
    private final String dualIdentifier;
    private final LineStyle style;
    
    /**
     * Creates a new edge with a type, identifier and line style.
     * @param type The edge type.
     * @param identifier The identifier of the particle.
     * @param dualIdentifier The identifier of the corresponding antiparticle.
     * @param style The style of the line.
     */
    public GraphEdge(EdgeType type, String identifier, String dualIdentifier, LineStyle style) {
      this.type = type;
      this.identifier = identifier;
      this.dualIdentifier = dualIdentifier;
      this.style = style;
    }
    
    /**
     * Returns the type of the edge.
     * @return the type of the edge.
     */
    public EdgeType getType() {
      return type;
    }
    
    /**
     * Returns the style of the edge.
     * @return the style of the edge.
     */
    public LineStyle getStyle() {
      return style;
    }
    
    /**
     * Returns the identifier of the edge.
     * @return the identifier of the edge.
     */
    public String getIdentifier() {
      return identifier;
    }
    
    /**
     * Returns the dual identifier of the edge.
     * @return the dual identifier of the edge.
     */
    public String getDualIdentifier() {
      return dualIdentifier;
    }

    @Override
    public int compareTo(GraphEdge o) {
      String tKey = style.isReversed() ? dualIdentifier : identifier;
      String oKey = o.style.isReversed() ? o.dualIdentifier : o.identifier;
      
      //If one is null, use the other one
      if(tKey == null)  tKey = style.isReversed() ? identifier : dualIdentifier;
      if(oKey == null)  oKey = o.style.isReversed() ? o.dualIdentifier : o.identifier;
      
      //If it is still null, put the one with a name on top
      if(tKey == null && oKey == null) {
        return 0;
      } else if(tKey == null) {
        return -1;
      } else if(oKey == null) {
        return 1;
      } else {
        return tKey.compareTo(oKey);
      }
    }
  }
  
  /**
   * The possible edge types.
   */
  public static enum EdgeType {
    /**
     * An edge that represents a propagator (internal line) of the diagram.
     */
    PROPAGATOR(3),
    /**
     * An edge that represents an incoming external leg.
     */
    IN_LEG(1),
    /**
     * An edge that represents an outgoing external leg.
     */
    OUT_LEG(2);
    
    private final int fieldType;
    
    private EdgeType(int fieldType) {
      this.fieldType = fieldType;
    }
    
    /**
     * The integer field type that is used by qgraf: 1 for incoming legs, 2 for outgoing legs and 3 for propagators.
     * @return The integer field type used by qgraf.
     */
    public int getFieldType() {
      return fieldType;
    }
    
    /**
     * Decodes the integer field type used by qgraf int an enum instance.
     * @param fieldType The integer field type.
     * @return The corresponding {@link EdgeType}.
     * @throws IllegalArgumentException If {@code fieldType} is not 1, 2 or 3.
     */
    public static EdgeType ofFieldType(int fieldType) {
      switch (fieldType) {
      case 1:
        return IN_LEG;
      case 2:
        return OUT_LEG;
      case 3:
        return PROPAGATOR;
      default:
        throw new IllegalArgumentException();
      }
    }
  }
  
  /**
   * The possible vertex types.
   */
  public static enum VertexType {
    /**
     * A vertex that is internal to the diagram.
     */
    INTERNAL(false),
    /**
     * A vertex that represents the outer end of an incoming leg of the diagram.
     */
    EXTERNAL_IN(true),
    /**
     * A vertex that represents the outer end of an outgoing leg of the diagram.
     */
    EXTERNAL_OUT(true);
    
    
    private final boolean external;
    
    private VertexType(boolean external) {
      this.external = external;
    }
    
    /**
     * Returns {@code true} for {@link #EXTERNAL_IN} and {@link #EXTERNAL_OUT}, {@code false} for {@link #INTERNAL}.
     * @return {@code true} for {@link #EXTERNAL_IN} and {@link #EXTERNAL_OUT}, {@code false} for {@link #INTERNAL}.
     */
    public boolean isExternal() {
      return external;
    }
  }
  
  private static boolean isVertexIncoming(game.Point2D vertex, FeynLine l) {
    if(GraphLayoutAlgorithm.flipDiagramOrientation()) {
      if(vertex.equals(l.getStart())) {
        return l.getStartY() >= l.getEndY();
      } else if(vertex.equals(l.getEnd())) {
        return l.getEndY() >= l.getStartY();
      } else {
        throw new RuntimeException("line does not contain mapped vertex");
      }
    } else {
      if(vertex.equals(l.getStart())) {
        return l.getStartX() <= l.getEndX();
      } else if(vertex.equals(l.getEnd())) {
        return l.getEndX() <= l.getStartX();
      } else {
        throw new RuntimeException("line does not contain mapped vertex");
      }
    }
  }
  
  private final Graph<GraphVertex, GraphEdge> graph;
  private final ModelData model;
  private LayoutModel2D<GraphVertex> positionData;
  private final String sourceText;
  
  /**
   * Creates a new instance from an existing {@link Graph} that represents the Feynman diagram.
   * The created {@link QgrafDiagram} instance will not contain any position data for the vertices.
   * @param graph The graph that represents the diagram
   * @param model The model that was be used to create and look up line styles.
   * @param The qgraf source code of the diagram, or {@code null}
   * @throws NullPointerException If any parameter is null.
   */
  public QgrafDiagram(Graph<GraphVertex, GraphEdge> graph, ModelData model, String sourceText) {
    this.graph = graph.getType().isModifiable() ? new AsUnmodifiableGraph<>(graph) : graph;
    this.model = Objects.requireNonNull(model);
    this.positionData = null;
    this.sourceText = sourceText;
  }
  
  /**
   * Creates a new instance from a list of {@link FeynLine}s and a list of {@link Vertex}. The created {@link QgrafDiagram} will contain
   * the position data of the vertices. There will not be a model associated with this instance.
   * @param vertices The list of vertices. The far ends of external legs should not have a vertex in this list.
   * @param lines The list of lines.
   * @param drawAreaBounds The target area that the vertices should be translated and scaled into.
   * @throws FeynGameDiagramStructureException Lines and vertices produce a diagram that is empty, unconnected or otherwise invalid.
   * @throws NullPointerException If any parameter is null.
   */
  public QgrafDiagram(List<game.Vertex> vertices, List<game.FeynLine> lines, Box2D drawAreaBounds) throws FeynGameDiagramStructureException {
    this.sourceText = null;
    Objects.requireNonNull(vertices);
    Objects.requireNonNull(lines);
    Objects.requireNonNull(drawAreaBounds);
    
    final Graph<GraphVertex, GraphEdge> mutGraph = new DirectedPseudograph<>(GraphEdge.class);
    this.graph = new AsUnmodifiableGraph<>(mutGraph);
    this.model = null;
    
    Box2D withBorder = GraphLayoutAlgorithm.boxWithinBorder(drawAreaBounds, GraphLayoutAlgorithm.getLayoutAlgorithm().getBorderSize());
    this.positionData = new MapLayoutModel2D<>(withBorder);
    
    if(vertices.isEmpty() || lines.isEmpty())
      throw new FeynGameDiagramStructureException("Diagram is empty");
    
    final Map<FeynLine, Vertex> lineStarts = new HashMap<>();
    final Map<FeynLine, Vertex> lineEnds = new HashMap<>();
    final Map<Vertex, GraphVertex> vertMap = new HashMap<>();
    for(game.Vertex v : vertices) {
      GraphVertex vert = new GraphVertex(VertexType.INTERNAL, new VertexStyle(v), 0);
      
      Point2D p_vert = new Point2D(v.origin.x, v.origin.y);//intPrecisionPoint(v.origin);
      Point2D p_flipped = GraphLayoutAlgorithm.flipDiagramOrientation() ? new Point2D(p_vert.getY(), -p_vert.getX()) : p_vert; 
      
      positionData.put(vert, p_flipped);
      vertMap.put(v, vert);
      mutGraph.addVertex(vert);
      
      //Then find all attached lines
      for(FeynLine fl : lines) {
        if(fl.getStart().distance(v.origin) < VERTX_ATTACHED_DISTANCE) {
          lineStarts.put(fl, v);
        }
        if(fl.getEnd().distance(v.origin) < VERTX_ATTACHED_DISTANCE) {
          lineEnds.put(fl, v);
        }
      }
    }
    
    boolean hasExternal = false;
    for(FeynLine fl : lines) {
      Vertex start = lineStarts.get(fl);
      Vertex end   = lineEnds.get(fl);
      
      if(start == null && end == null) {
        throw new FeynGameDiagramStructureException("Unconnected single line");
      } else if(start == null) {
        hasExternal = true;
        //Start is an external vertex
        final GraphVertex extV;
        final GraphEdge edge;
        double di = GraphLayoutAlgorithm.flipDiagramOrientation() ? fl.getStartX() : fl.getStartY();
        int ii = (int) (100*di); //assuming that the vertices are not separated by more than 0.01 units
        if(isVertexIncoming(fl.getStart(), fl)) {//IN LEG
          extV = new GraphVertex(VertexType.EXTERNAL_IN, ii);
          edge = new GraphEdge(EdgeType.IN_LEG, fl.lineConfig.identifier.getKey(),
              fl.lineConfig.identifier.getValue(), new LineStyle(fl, false));
        } else { //OUT LEG
          extV = new GraphVertex(VertexType.EXTERNAL_OUT, ii);
          edge = new GraphEdge(EdgeType.OUT_LEG, fl.lineConfig.identifier.getKey(),
              fl.lineConfig.identifier.getValue(), new LineStyle(fl, false));
        }
        
        Point2D p_extV = new Point2D(fl.getStartX(), fl.getStartY());
        Point2D p_flipped = GraphLayoutAlgorithm.flipDiagramOrientation() ? new Point2D(p_extV.getY(), -p_extV.getX()) : p_extV; 
        positionData.put(extV, p_flipped);
        mutGraph.addVertex(extV);
        mutGraph.addEdge(extV, vertMap.get(end), edge);
      } else if(end == null) {
        hasExternal = true;
        //end is an external vertex
        final GraphVertex extV;
        final GraphEdge edge;
        double di = GraphLayoutAlgorithm.flipDiagramOrientation() ? fl.getEndX() : fl.getEndY();
        int ii = (int) (100*di); //assuming that the vertices are not separated by more than 0.01 units
        if(isVertexIncoming(fl.getEnd(), fl)) {//IN LEG
          extV = new GraphVertex(VertexType.EXTERNAL_IN, ii);
          edge = new GraphEdge(EdgeType.IN_LEG, fl.lineConfig.identifier.getKey(),
              fl.lineConfig.identifier.getValue(), new LineStyle(fl, false));
        } else { //OUT LEG
          extV = new GraphVertex(VertexType.EXTERNAL_OUT, ii);
          edge = new GraphEdge(EdgeType.OUT_LEG, fl.lineConfig.identifier.getKey(),
              fl.lineConfig.identifier.getValue(), new LineStyle(fl, false));
        }
        
        Point2D p_extV = new Point2D(fl.getEndX(), fl.getEndY());
        Point2D p_flipped = GraphLayoutAlgorithm.flipDiagramOrientation() ? new Point2D(p_extV.getY(), -p_extV.getX()) : p_extV; 
        positionData.put(extV, p_flipped);
        mutGraph.addVertex(extV);
        mutGraph.addEdge(vertMap.get(start), extV, edge);
      } else { //propagator
        GraphEdge edge = new GraphEdge(EdgeType.PROPAGATOR, fl.lineConfig.identifier.getKey(),
            fl.lineConfig.identifier.getValue(), new LineStyle(fl, false));
        mutGraph.addEdge(vertMap.get(start), vertMap.get(end), edge);
      }
    }
    
    GraphLayoutAlgorithm.moveVerticesToDrawArea(positionData);
    
    if(!hasExternal) {
      throw new FeynGameDiagramStructureException("No external legs found");
    }
    
    Set<GraphVertex> deg0verts = mutGraph.vertexSet().stream().filter(v -> mutGraph.degreeOf(v) == 0).collect(Collectors.toSet());
    deg0verts.forEach(mutGraph::removeVertex);
    
    ConnectivityInspector<GraphVertex, GraphEdge> ce = new ConnectivityInspector<>(graph);
    if(ce.connectedSets().size() != 1) {
      throw new FeynGameDiagramStructureException("Diagram is not connected");
    }
  }

  /**
   * Distance between line end and vertex at which the line is considered attached to the vertex.
   * The value of 1.5 is hard-coded into Frame
   */
  private static final double VERTX_ATTACHED_DISTANCE = 1.5;
  
  /**
   * Returns the model that was used to look up or generate the line styles in this diagram.
   * May be {@code null} if the diagram was created from existing lines and vertices.
   * @return The model that was used to create the line styles in this diagram.
   */
  public ModelData getModel() {
    return model;
  }
  
  /**
   * Returns the {@link Graph} representation of this Feynman diagram. The returned graph is unmodifiable.
   * @return the {@link Graph} representation of this Feynman diagram.
   */
  public Graph<GraphVertex, GraphEdge> getAsGraph() {
    return graph;
  }
  
  /**
   * Returns the vertex position data that is associated with this diagram. May be {@code null} if the diagram was created from a graph object.
   * @return the vertex position data that is associated with this diagram.
   */
  public LayoutModel2D<GraphVertex> getPositionData() {
//    if(positionData == null) throw new IllegalStateException();
    return positionData;
  }
  
  /**
   * Returns the drawing area that is associated with the position data for this diagram.
   * @return The drawing area that is associated with the position data for this diagram.
   * @throws IllegalStateException If this diagram does not have any associated position data.
   */
  public Box2D getGraphBounds() {
    if(positionData == null) throw new IllegalStateException();
    return positionData.getDrawableArea();
  }
  
  /**
   * Returns {@code true} if this diagram contains vertex position data, {@code false} otherwise.
   * Iff this method returns {@code false}, {@link #getPositionData()} returns {@code null}. 
   * @return {@code true} if this diagram contains vertex position data, {@code false} otherwise.
   */
  public boolean hasPositionData() {
    return positionData != null;
  }
  
  /**
   * Sets the vertex position data of this diagram. This method accepts any {@link LayoutModel2D}, it is not checked whether the model
   * contains positions for all vertices in the diagram.
   * @param positionData The new vertex position data. May be {@code null} to remove position data.
   */
  public void setPositionData(LayoutModel2D<GraphVertex> positionData) {
    this.positionData = positionData;
  }
  
  /**
   * Applies a graph layout algotithm to this diagram. If present, the vertex position data of this diagram is passed to the algorithm.
   * @param alg The graph layout algorithm.
   * @throws GraphLayoutException When the graph layout algorithm encounters an error.
   * @throws IllegalArgumentException If this diagram contains no position data and the algorithm reports that it requires an initial layout.
   */
  public void applyLayout(GraphLayoutAlgorithm alg) throws GraphLayoutException {
    if(positionData == null && alg.requiresInitialLayout()) {
      throw new IllegalArgumentException();
    }
    
    alg.layout(graph, positionData);
  }
  
  public String getSourceText() {
    return sourceText;
  }
  
  public boolean hasSourceText() {
    return sourceText != null;
  }
  
  public void applySelectedLayout(Box2D area) throws GraphLayoutException {
    GraphLayoutAlgorithm gla = GraphLayoutAlgorithm.getLayoutAlgorithm();
    GraphLayoutAlgorithm glx = GraphLayoutAlgorithm.getPreLayoutAlgorithm();
    GraphLayoutAlgorithm glp = GraphLayoutAlgorithm.getPostLayoutAlgorithm();
    Box2D bounds = area;
    bounds = GraphLayoutAlgorithm.boxWithinBorder(bounds, GraphLayoutAlgorithm.getLayoutAlgorithm().getBorderSize());
    
    if(!hasPositionData()) {
      LayoutModel2D<GraphVertex> model = new MapLayoutModel2D<>(bounds);
      new RandomLayoutAlgorithm2D<GraphVertex, GraphEdge>().layout(getAsGraph(), model);
      setPositionData(model);
    }
    
    if(glx != null) applyLayout(glx);
    applyLayout(gla);
    if(glp != null) applyLayout(glp);
    //applying the whole cycle twice helps with stability; this is different from just 
    //increasing the iterations as here we run all three phases twice
    if(glx != null) applyLayout(glx);
    applyLayout(gla);
    if(glp != null) applyLayout(glp);
  } 

  /**
   * Removes the vertex position data.
   */
  public void clearPositionData() {
    positionData = null;
  }
  
}
