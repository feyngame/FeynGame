package qgraf;

/**
 * This exception type is used whenever adding a keyword value or loop iteration to a {@link QgrafOutputBuilder}
 * leaves the builder in an inconsistent state (An example would be to pass a negative vertex index). This exception should
 * not be propagated as an error message to the user. Instead, it is a signal for 
 * {@link QgrafOutputReader#parseOutput(StyleNodeIterator, java.util.function.LongSupplier, ProgressUpdate, boolean, boolean)}
 * to discard the current branch.
 * @author Lars Bündgen
 */
public class BuilderInvalidException extends Exception {
  private static final long serialVersionUID = 4988829111285255893L;

  /**
   * Construct the exception with a message and a cause (such as {@link NumberFormatException}).
   * @param message The exception message. Not shown to the user.
   * @param cause The {@link Throwable} that caused the builder to become invalid.
   */
  public BuilderInvalidException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Construct the exception with a message.
   * @param message The exception message. Not shown to the user.
   */
  public BuilderInvalidException(String message) {
    super(message);
  }
  
}