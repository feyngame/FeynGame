package qgraf;

import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * An improved version of Java's {@link Cloneable} that overrides the {@code clone()} method to return the implementing type.
 * @param <T> The implementing type
 * 
 * @author Lars Bündgen
 */
public interface Cloneable2<T extends Cloneable2<T>> extends Cloneable {
  
    /**
     * Creates and returns a copy of this object. Exact behavior must be explained in the implementing method. 
     * @return The cloned object
     */
    public T clone();
    
    /**
     * A utility method to deep-clone a collection of items that implement {@link Cloneable2}.
     * @param <T> The type of the list's items. Must implement {@link Cloneable2}.
     * @param <C> The type of the collection implementation.
     * @param collection The collection to clone.
     * @param ctor A no-args constructor that creates an empty collection of te original collection's type
     * @return A new collection that contains the clones of the original list's items. 
     * For ordered collections the items will be in the same order. 
     */
    public static <T extends Cloneable2<? extends T>, C extends Collection<T>> C clone(C collection, Supplier<? extends C> ctor) {
      return collection.stream().map(Cloneable2::clone).collect(Collectors.toCollection(ctor));
    }
}
