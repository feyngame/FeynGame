package qgraf;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.IntFunction;

import qgraf.QgrafStyleNode.KeywordName;
import qgraf.QgrafStyleNode.LoopName;

/**
 * Mutable description of one qgraf output file.
 * 
 * @author Lars Bündgen
 */
class QgrafOutputBuilder implements Cloneable2<QgrafOutputBuilder> {
  
  private final Deque<LoopName> loops;
  private final DiagramLoopList diagramData;
  private ConstraintsImpl constraints;
  
  /**
   * A generic interface for any component that can accept keyword values. These types must also
   * implement {@link Cloneable2} to correctly interact with the {@link LoopList} class.
   * 
   * @author Lars Bündgen
   * @param <T> The implementing type, for {@link Cloneable2}.
   */
  static interface AcceptsKeywords<T extends AcceptsKeywords<T>> extends Cloneable2<T> {
    void acceptKeyword(KeywordName name, String data) throws BuilderInvalidException, QgrafImportException;
  }
  
  //Some validator methods.
  //The get... methods validate the string and return the result in the correct type or throw
  //The set... methods also compare to an existing value and throw if the new value is not compatible
  //The new value is returned and must still be assigned manually
  //Used like: value = set...(value, data, keyword);
  
  private static String setString(String value, String data, KeywordName kwname) throws BuilderInvalidException {
    if(value == null) {
      return data;
    } else {
      if(!value.equals(data)) throw new BuilderInvalidException("Cannot set " + kwname + " to " 
          + data + " because it is already " + value);
      return data;
    }
  }
  
  private static int getPosInt(String text) throws BuilderInvalidException {
    try {
      int i = Integer.parseInt(text);
      if(i <= 0) throw new BuilderInvalidException("Value " + i + " is not a positive integer");
      return i;
    } catch (NumberFormatException e) {
      throw new BuilderInvalidException("Cannot convert keyword value to integer", e);
    }
  }
  
  private static int setPosInt(int value, String data, KeywordName kwname) throws BuilderInvalidException {
    int i = getPosInt(data);
    if(value == 0) {
      return i;
    } else {
      if(value != i) throw new BuilderInvalidException("Cannot set " + kwname + " to " 
          + i + " because it is already " + value);
      return i;
    }
  }
  
  private static int getNegInt(String text) throws BuilderInvalidException {
    try {
      int i = Integer.parseInt(text);
      if(i >= 0) throw new BuilderInvalidException("Value " + i + " is not a negative integer");
      return i;
    } catch (NumberFormatException e) {
      throw new BuilderInvalidException("Cannot convert keyword value to integer", e);
    }
  }
  
  private static int setNegInt(int value, String data, KeywordName kwname) throws BuilderInvalidException {
    int i = getNegInt(data);
    if(value == 0) {
      return i;
    } else {
      if(value != i) throw new BuilderInvalidException("Cannot set " + kwname + " to " 
          + i + " because it is already " + value);
      return i;
    }
  }
  
  private static int getNonZeroInt(String text) throws BuilderInvalidException {
    try {
      int i = Integer.parseInt(text);
      if(i == 0) throw new BuilderInvalidException("Value " + i + " is not a nonzero integer");
      return i;
    } catch (NumberFormatException e) {
      throw new BuilderInvalidException("Cannot convert keyword value to integer", e);
    }
  }
  
  private static int setNonZeroInt(int value, String data, KeywordName kwname) throws BuilderInvalidException {
    int i = getNonZeroInt(data);
    if(value == 0) {
      return i;
    } else {
      if(value != i) throw new BuilderInvalidException("Cannot set " + kwname + " to " 
          + i + " because it is already " + value);
      return i;
    }
  }
  
  private static int getNonNegInt(String text) throws BuilderInvalidException {
    try {
      int i = Integer.parseInt(text);
      if(i < 0) throw new BuilderInvalidException("Value " + i + " is not a nonnegative integer");
      return i;
    } catch (NumberFormatException e) {
      throw new BuilderInvalidException("Cannot convert keyword value to integer", e);
    }
  }
  
  private static int setNonNegInt(int value, String data, KeywordName kwname) throws BuilderInvalidException {
    int i = getNonNegInt(data);
    if(value == 0) {
      return i;
    } else {
      if(value != i) throw new BuilderInvalidException("Cannot set " + kwname + " to " 
          + i + " because it is already " + value);
      return i;
    }
  }
  
  private static int getInt(String text) throws BuilderInvalidException {
    try {
      int i = Integer.parseInt(text);
      return i;
    } catch (NumberFormatException e) {
      throw new BuilderInvalidException("Cannot convert keyword value to integer", e);
    }
  }
  
  private static int setInt(int value, String data, KeywordName kwname) throws BuilderInvalidException {
    int i = getInt(data);
    if(value == 0) {
      return i;
    } else {
      if(value != i) throw new BuilderInvalidException("Cannot set " + kwname + " to " 
          + i + " because it is already " + value);
      return i;
    }
  }
  
  /**
   * Create an instance with no diagram data.
   * @param loadOnlyBase0 The index of the diagram to load, or {@link QgrafOutputFile#LOAD_ALL_INDEX_VALUE}
   * to load all diagrams.
   */
  protected QgrafOutputBuilder(int loadOnlyBase0) {
    this.loops = new ArrayDeque<>();
    this.diagramData = new DiagramLoopList(loadOnlyBase0);
    this.constraints = null;
  }
  
  /**
   * Copy constructor. Does not copy parameters. Does not null-check.
   * @param loops The currently entered loops.
   * @param diagramData The list of loaded diagram data.
   */
  private QgrafOutputBuilder(Deque<LoopName> loops, DiagramLoopList diagramData) {
    this.loops = loops;
    this.diagramData = diagramData;
    this.constraints = null;
  }
  
  private void initConstraints(Map<LoopName, Integer> mapEntries) {
    this.constraints = new ConstraintsImpl(mapEntries);
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(diagramData, loops);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof QgrafOutputBuilder)) {
      return false;
    }
    QgrafOutputBuilder other = (QgrafOutputBuilder) obj;
    return Objects.equals(diagramData, other.diagramData)
        && Objects.equals(loops, other.loops);
  }

  /**
   * Special {@link LoopList} implementation that can cause the parser to exit early if the diagram with the selected index has been loaded.
   * Data of a diagram that is not to be loaded will initially be read to support constraints, and deleted after the diagram is fully read.
   * Diagrams that are fully read are assumed to be immutable and will not be deep copied.
   */
  static class DiagramLoopList extends LoopList<DiagramData> {

    private final int loadOnlyBase0;
    private boolean canExitEarly;
    
    public DiagramLoopList(int loadOnlyBase0) {
      super(DiagramData::new, false);
      this.loadOnlyBase0 = loadOnlyBase0;
      this.canExitEarly = false;
    }
    
    
    public DiagramLoopList(int loadOnlyBase0, int currentLastIndex, boolean firstIterationCompleted, List<DiagramData> list,
        boolean repeatable, IntFunction<DiagramData> ctor, boolean canExitEarly, boolean forceEmptyEntry) {
      super(currentLastIndex, firstIterationCompleted, list, repeatable, ctor, forceEmptyEntry);
      this.loadOnlyBase0 = loadOnlyBase0;
      this.canExitEarly = canExitEarly;
    }
    
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + Objects.hash(canExitEarly, loadOnlyBase0);
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!super.equals(obj)) {
        return false;
      }
      if (!(obj instanceof DiagramLoopList)) {
        return false;
      }
      DiagramLoopList other = (DiagramLoopList) obj;
      return canExitEarly == other.canExitEarly && loadOnlyBase0 == other.loadOnlyBase0;
    }

    @Override
    void repeat() throws BuilderInvalidException {
      //Check the last one
      if(!list.isEmpty()) { //if not enter
        DiagramData last = list.get(list.size() - 1);
        last.validateDiagram();
      }
      super.repeat();
      if(loadOnlyBase0 >= 0 && currentLastIndex - 1 != loadOnlyBase0) { //last one was not the requested one
        assert currentLastIndex + 1 == list.size();
        list.set(currentLastIndex - 1, null); //delete the object for GC
//        System.gc();
      } else if(loadOnlyBase0 >= 0 && currentLastIndex - 2 == loadOnlyBase0) { //read 2 more, to ensure that last one is complete
        canExitEarly = true;
      }
    }
    
    public DiagramLoopList cloneCurrentOnly() {
      if(repeatable) {
        return clone();
      } else if(firstIterationCompleted) {
        //cannot change anymore -> clone object, but not the list items
        return new DiagramLoopList(loadOnlyBase0, currentLastIndex, firstIterationCompleted, 
            new ArrayList<>(list), repeatable, ctor, canExitEarly, forceEmptyEntry);
      } else if(currentLastIndex == -1) { 
        assert list.isEmpty();
        //not yet started? -> can clone because list is empty anyways
        return clone();
      } else {
        ArrayList<DiagramData> copy = new ArrayList<>(list);
        DiagramData last = copy.remove(copy.size() - 1);
        copy.add(last.clone());
        return new DiagramLoopList(loadOnlyBase0, currentLastIndex, firstIterationCompleted,
            copy, repeatable, ctor, canExitEarly, forceEmptyEntry);
      }
    }

    @Override
    public DiagramLoopList clone() {
      return new DiagramLoopList(loadOnlyBase0, currentLastIndex, firstIterationCompleted,
          Cloneable2.clone(list, ArrayList::new), repeatable, ctor, canExitEarly, forceEmptyEntry);
    }
  }
  
  /**
   * Data associated with an incoming or outgoing leg of the diagram.
   * Corresponds to the content of the {@code <in_loop>} or {@code <out_loop>}.
   */
  static class LegData implements AcceptsKeywords<LegData> {
    private final boolean isInLeg;
    
    public LegData(int loopIndexBase0, boolean isInLeg) {
      this.isInLeg = isInLeg;
      if(isInLeg) {
        this.fieldIndex = -2*(loopIndexBase0+1)+1;
      } else {
        this.fieldIndex = -2*(loopIndexBase0+1);
      }
      this.inOutIndex = loopIndexBase0+1;
    }
    
    public LegData(boolean isInLeg) {
      this.isInLeg = isInLeg;
    }
    
    String dualField;
    String field;
    int fieldIndex;
    int inOutIndex;
    int legIndex; //Listed only for outIndex, allow for both
    int rayIndex;
    int vertexIndex;
    String momentum;
    String dualMomentum;
    
    public LegData(boolean isInLeg, String dualField, String field, int fieldIndex, int inOutIndex, int legIndex,
        int rayIndex, int vertexIndex, String momentum, String dualMomentum) {
      this.isInLeg = isInLeg;
      this.dualField = dualField;
      this.field = field;
      this.fieldIndex = fieldIndex;
      this.inOutIndex = inOutIndex;
      this.legIndex = legIndex;
      this.rayIndex = rayIndex;
      this.vertexIndex = vertexIndex;
      this.momentum = momentum;
      this.dualMomentum = dualMomentum;
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(dualField, field, fieldIndex, inOutIndex, isInLeg, legIndex, rayIndex, vertexIndex, momentum, dualMomentum);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof LegData)) {
        return false;
      }
      LegData other = (LegData) obj;
      return Objects.equals(dualField, other.dualField) && Objects.equals(field, other.field)
          && fieldIndex == other.fieldIndex && inOutIndex == other.inOutIndex && isInLeg == other.isInLeg
          && legIndex == other.legIndex && rayIndex == other.rayIndex && vertexIndex == other.vertexIndex
          && Objects.equals(momentum, other.momentum) && Objects.equals(dualMomentum, other.dualMomentum);
    }

    @Override
    public LegData clone() {
      return new LegData(isInLeg, dualField, field, fieldIndex, inOutIndex, legIndex, rayIndex, vertexIndex, momentum, dualMomentum);
    }

    @Override
    public void acceptKeyword(KeywordName name, String data) throws QgrafImportException, BuilderInvalidException {
      switch (name) {
      case DUAL_FIELD:
        this.dualField = setString(this.dualField, data, name);
        break;
      case FIELD:
        this.field = setString(this.field, data, name);
        break;
      case FIELD_INDEX:
        this.fieldIndex = setNegInt(this.fieldIndex, data, name);
        if(this.fieldIndex >= 0) {
          throw new BuilderInvalidException("positive field index for external leg");
        } else if((this.fieldIndex % 2 == 0) == isInLeg) {
          throw new BuilderInvalidException("divisibility of field index for external leg");
        }
        break;
      case LEG_INDEX:
        this.legIndex = setPosInt(this.legIndex, data, name);
        break;
      case RAY_INDEX:
        this.rayIndex = setPosInt(this.rayIndex, data, name);
        break;
      case VERTEX_INDEX:
        this.vertexIndex = setPosInt(this.vertexIndex, data, name);
        break;
      case DUAL_MOMENTUM:
        this.dualMomentum = setString(this.dualMomentum, data, name);
        break;
      case FIELD_SIGN:
        break;
      case FIELD_TYPE:
        break;
      case MOMENTUM:
        this.momentum = setString(this.momentum, data, name);
        break;
      case VERTEX_DEGREE:
        break; //ignore
      case IN_INDEX:
        if(isInLeg) {
          this.inOutIndex = setPosInt(this.inOutIndex, data, name);
          break;
        } //else fall-through to error
      case OUT_INDEX:
        if(!isInLeg) {
          this.inOutIndex = setPosInt(this.inOutIndex, data, name);
          break;
        }
      default:
        throw new QgrafImportException("Cannot use keyword " + name + " in " + (isInLeg ? "in" : "out") + " loop");
      }
    }
  }

  /**
   * Data associated with a vertex. Corresponds to the content of the {@code <vertex_loop>}.
   * Ray data is stored in the separate class {@link PropagatorOrRayData}.
   */
  static class VertexData implements AcceptsKeywords<VertexData> {
    private int vertexIndex;
    private int vertexDegree;
    private final LoopList<PropagatorOrRayData> rayData;
    
    public VertexData() {
      this.vertexDegree = 0;
      this.vertexIndex = 0;
      this.rayData = new LoopList<>(i -> new PropagatorOrRayData(i, false), true);
    }
    
    public VertexData(int loopIndexBase0) {
      this.vertexDegree = 0;
      this.vertexIndex = loopIndexBase0+1;
      this.rayData = new LoopList<>(i -> new PropagatorOrRayData(i, loopIndexBase0+1, false), true);
    }
    
    public VertexData(int vertexIndex, int vertexDegree, LoopList<PropagatorOrRayData> rayData) {
      this.vertexIndex = vertexIndex;
      this.vertexDegree = vertexDegree;
      this.rayData = rayData;
    }
    
    public Iterable<PropagatorOrRayData> rays() {
      return rayData;
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(rayData, vertexDegree, vertexIndex);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof VertexData)) {
        return false;
      }
      VertexData other = (VertexData) obj;
      return Objects.equals(rayData, other.rayData) && vertexDegree == other.vertexDegree
          && vertexIndex == other.vertexIndex;
    }

    @Override
    public VertexData clone() {
      return new VertexData(vertexIndex, vertexDegree, rayData.clone());
    }

    @Override
    public void acceptKeyword(KeywordName name, String data) throws BuilderInvalidException, QgrafImportException {
      switch (name) {
      case VERTEX_INDEX:
        this.vertexIndex = setPosInt(this.vertexIndex, data, name);
        break;
      case VERTEX_DEGREE:
        this.vertexDegree = setPosInt(this.vertexDegree, data, name);
        break;
      default:
        throw new QgrafImportException("Cannot use keyword " + name + " in vertex loop");
      }
    }
  }
  
  /**
   * Data associated with a propagator or a ray. Corresponds to the content of the
   * {@code <propagator_loop>} or the {@code <ray_loop>}.
   */
  static class PropagatorOrRayData implements AcceptsKeywords<PropagatorOrRayData> {
    private final boolean isPropagator;
    
    String dualField;
    int dualFieldIndex;
    int dualRayIndex;
    int dualVertexIndex;
    String field;
    int fieldIndex;
    int propagatorIndex;
    int rayIndex;
    int vertexIndex;
    
    String momentum;
    String dualMomentum;
    
    public PropagatorOrRayData(int loopIndexBase0, int forceVertexIndex, boolean isPropagator) {
      this(loopIndexBase0, isPropagator);
      if(isPropagator) throw new IllegalArgumentException();
      this.vertexIndex = forceVertexIndex;
    }
    
    public PropagatorOrRayData(int loopIndexBase0, boolean isPropagator) {
      this.isPropagator = isPropagator;
      if(isPropagator) { //appears in prop loop
        this.propagatorIndex = loopIndexBase0 + 1;
        //see qgraf document
        this.fieldIndex = 2*(loopIndexBase0+1)-1;
        this.dualFieldIndex = 2*(loopIndexBase0+1);
      } else { //appears in ray loop
        this.rayIndex = loopIndexBase0+1;
      }
      //Rest is default value
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(dualField, dualFieldIndex, dualRayIndex, dualVertexIndex, field, fieldIndex, isPropagator,
          propagatorIndex, rayIndex, vertexIndex, momentum, dualMomentum);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof PropagatorOrRayData)) {
        return false;
      }
      PropagatorOrRayData other = (PropagatorOrRayData) obj;
      return Objects.equals(dualField, other.dualField) && dualFieldIndex == other.dualFieldIndex
          && dualRayIndex == other.dualRayIndex && dualVertexIndex == other.dualVertexIndex
          && Objects.equals(field, other.field) && fieldIndex == other.fieldIndex && isPropagator == other.isPropagator
          && propagatorIndex == other.propagatorIndex && rayIndex == other.rayIndex && vertexIndex == other.vertexIndex
          && Objects.equals(momentum, other.momentum) && Objects.equals(dualMomentum, other.dualMomentum);
    }

    public PropagatorOrRayData(boolean isPropagator) {
      this.isPropagator = isPropagator;
    }

    public PropagatorOrRayData(boolean isPropagator, String dualField, int dualFieldIndex, int dualRayIndex, int dualVertexIndex,
        String field, int fieldIndex, int propagatorIndex, int rayIndex, int vertexIndex, String momentum, String dualMomentum) {
      this.isPropagator = isPropagator;
      this.dualField = dualField;
      this.dualFieldIndex = dualFieldIndex;
      this.dualRayIndex = dualRayIndex;
      this.dualVertexIndex = dualVertexIndex;
      this.field = field;
      this.fieldIndex = fieldIndex;
      this.propagatorIndex = propagatorIndex;
      this.rayIndex = rayIndex;
      this.vertexIndex = vertexIndex;
      this.momentum = momentum;
      this.dualMomentum = dualMomentum;
    }

    @Override
    public PropagatorOrRayData clone() {
      return new PropagatorOrRayData(isPropagator, dualField, dualFieldIndex, dualRayIndex, dualVertexIndex,
          field, fieldIndex, propagatorIndex, rayIndex, vertexIndex, momentum, dualMomentum);
    }

    @Override
    public void acceptKeyword(KeywordName name, String data) throws QgrafImportException, BuilderInvalidException {
      switch (name) {
      case DUAL_FIELD:
        this.dualField = setString(this.dualField, data, name);
        break;
      case DUAL_FIELD_INDEX:
        this.dualFieldIndex = setInt(this.dualFieldIndex, data, name);
        if(isPropagator && this.dualFieldIndex <= 0) {
          throw new BuilderInvalidException("non positive dual field index for propagator");
        }
        break;
      case DUAL_RAY_INDEX:
        if(isPropagator) {
          this.dualRayIndex = setPosInt(this.dualRayIndex, data, name);
        } else {
          this.dualRayIndex = setNonNegInt(this.dualRayIndex, data, name); //allow 0 as dual vertex index
        }
        break;
      case DUAL_VERTEX_INDEX: 
        if(isPropagator) {
          this.dualVertexIndex = setPosInt(this.dualVertexIndex, data, name);
        } else {
          this.dualVertexIndex = setNonNegInt(this.dualVertexIndex, data, name); //allow 0 as dual vertex index
        }
        break;
      case FIELD:
        this.field = setString(this.field, data, name);
        break;
      case FIELD_INDEX:
        this.fieldIndex = setNonZeroInt(this.fieldIndex, data, name);
        if(isPropagator && this.fieldIndex <= 0) {
          throw new BuilderInvalidException("non positive field index for propagator");
        }
        break;
      case PROPAGATOR_INDEX:
        this.propagatorIndex = setNonZeroInt(this.propagatorIndex, data, name);
        if(isPropagator && this.propagatorIndex <= 0) {
          throw new BuilderInvalidException("non positive propagator index in propagator loop");
        }
        if(this.propagatorIndex < 0) { //if prop is negative, it is also the field index
          if(this.fieldIndex == 0) 
            this.fieldIndex = this.propagatorIndex;
          else if(this.fieldIndex != this.propagatorIndex) 
            throw new BuilderInvalidException("External legs must have the same field and prop index in ray loop");
        }
        break;
      case RAY_INDEX:
        this.rayIndex = setPosInt(this.rayIndex, data, name);
        break;
      case VERTEX_INDEX:
        this.vertexIndex = setPosInt(this.vertexIndex, data, name);
        break;
      case DUAL_MOMENTUM:
        this.dualMomentum = setString(this.dualMomentum, data, name);
        break;
      case DUAL_VERTEX_DEGREE:
        break;
      case FIELD_SIGN:
        break;
      case FIELD_TYPE:
        break;
      case MOMENTUM:
        this.momentum = setString(this.momentum, data, name);
        break;
      case VERTEX_DEGREE:
        break;
      default:
        throw new QgrafImportException("Cannot use keyword " + name + " in propagator loop");
      }
    }
  }
  
  /**
   * Content of one Feynman diagram. Corresponds to the content of the {@code <diagram>} loop.
   */
  static final class DiagramData implements AcceptsKeywords<DiagramData> {
    
    @Override
    public int hashCode() {
      return Objects.hash(diagramIndex, inLegData, legs, legsIn, legsOut, outLegData, propagatorData, propagators,
          trueDiagramIndex, vertexData, vertices, sourceText);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof DiagramData)) {
        return false;
      }
      DiagramData other = (DiagramData) obj;
      return diagramIndex == other.diagramIndex && Objects.equals(inLegData, other.inLegData) && legs == other.legs
          && legsIn == other.legsIn && legsOut == other.legsOut && Objects.equals(outLegData, other.outLegData)
          && Objects.equals(propagatorData, other.propagatorData) && propagators == other.propagators
          && trueDiagramIndex == other.trueDiagramIndex && Objects.equals(vertexData, other.vertexData)
          && vertices == other.vertices && Objects.equals(sourceText, other.sourceText);
    }

    public boolean isPropagatorDataEmpty() {
      return propagatorData.list.isEmpty();
    }
    
    public Iterable<PropagatorOrRayData> propagators() {
      return propagatorData;
    }
    
    public Iterable<LegData> inLegs() {
      return inLegData;
    }
    
    public Iterable<LegData> outLegs() {
      return outLegData;
    }
    
    public Iterable<VertexData> vertices() {
      return vertexData;
    }
    
    /**
     * Validates all data in this diagram for consitency (e.g number of vertices must be equal to the
     * {@code <vertices>} keyword value if present). Usually called at the end of one diagram loop iteration. 
     * @throws BuilderInvalidException If the diagram data is inconsistent.
     */
    public void validateDiagram() throws BuilderInvalidException {
      //vertex/prop/leg count
      if(vertices > 0) { //Count is known
        if(vertexData.getKnownSize() != -1) {
          if(vertices != vertexData.getKnownSize()) {
            throw new BuilderInvalidException("vertices != vertex loop size");
          }
        }
      }
      
      if(propagators > 0) { //Count is known
        if(propagatorData.getKnownSize() != -1) {
          if(propagators != propagatorData.getKnownSize()) {
            throw new BuilderInvalidException("propagators != propagator loop size");
          }
        }
      }
      
      if(legsIn > 0) { //Count is known
        if(inLegData.getKnownSize() != -1) {
          if(legsIn != inLegData.getKnownSize()) {
            throw new BuilderInvalidException("legsIn != in leg loop size");
          }
        }
      }
      
      if(legsOut > 0) { //Count is known
        if(outLegData.getKnownSize() != -1) {
          if(legsOut != outLegData.getKnownSize()) {
            throw new BuilderInvalidException("legsOut != out leg loop size");
          }
        }
      }
      
      //ray count for each vertex
      for(VertexData vd : vertexData) {
        if(vd.vertexDegree > 0) { //Count is known
          if(vd.rayData.getKnownSize() != -1) {
            if(vd.vertexDegree != vd.rayData.getKnownSize()) {
              throw new BuilderInvalidException("vertex degree != ray loop size");
            }
          }
        }
      }
      //TODO what else can be validated about a diagram
    }
    
    public DiagramData(int loopIndexBase0) {
      this.vertexData = new LoopList<>(VertexData::new, true);
      this.propagatorData = new LoopList<>(i -> new PropagatorOrRayData(i, true), true);
      this.inLegData = new LoopList<>(i -> new LegData(i, true), true);
      this.outLegData = new LoopList<>(i -> new LegData(i, false), true);
      this.sourceText = new StringBuilder();
      this.trueDiagramIndex = loopIndexBase0+1;
      //All other values have default 0
    }
    
    private DiagramData(LoopList<VertexData> vertexData, LoopList<PropagatorOrRayData> propagatorData,
        LoopList<LegData> inLegData, LoopList<LegData> outLegData, int diagramIndex, int legs, int legsIn, int legsOut,
        int propagators, int vertices, int trueDiagramIndex, StringBuilder sourceText) {
      this.vertexData = vertexData;
      this.propagatorData = propagatorData;
      this.inLegData = inLegData;
      this.outLegData = outLegData;
      this.diagramIndex = diagramIndex;
      this.legs = legs;
      this.legsIn = legsIn;
      this.legsOut = legsOut;
      this.propagators = propagators;
      this.vertices = vertices;
      this.trueDiagramIndex = trueDiagramIndex;
      this.sourceText = sourceText;
    }

    @Override
    public DiagramData clone() {
      return new DiagramData(vertexData.clone(), propagatorData.clone(), inLegData.clone(), outLegData.clone(),
          diagramIndex, legs, legsIn, legsOut, propagators, vertices, trueDiagramIndex, new StringBuilder(sourceText.length()).append(sourceText));
    }
    
    private final LoopList<VertexData> vertexData;
    private final LoopList<PropagatorOrRayData> propagatorData;
    private final LoopList<LegData> inLegData;
    private final LoopList<LegData> outLegData;
    
    private int trueDiagramIndex; //how it appears in the file
    private int diagramIndex;
    private int legs;
    private int legsIn;
    private int legsOut;
    private int propagators;
    private int vertices;
    //omit all properties that are not used by FeynGame (like loops),
    //these could be added later
    
    private final StringBuilder sourceText;
    
    public void acceptSourceLine(String line) {
      sourceText.append(line);
//      sourceText.append("\n");
    }
    
    public String getSourceText() {
      return sourceText.toString();
    }
    
    @Override
    public void acceptKeyword(KeywordName name, String data) throws QgrafImportException, BuilderInvalidException {
      switch (name) {
      case DIAGRAM_INDEX:
        this.diagramIndex = setPosInt(this.diagramIndex, data, name);
        break;
      case LEGS:
        this.legs = setInt(this.legs, data, name);
        break;
      case LEGS_IN:
        this.legsIn = setInt(this.legsIn, data, name);
        break;
      case LEGS_OUT:
        this.legsOut = setInt(this.legsOut, data, name);
        break;
      case PROPAGATORS:
        this.propagators = setInt(this.propagators, data, name);
        break;
      case VERTICES:
        this.vertices = setPosInt(this.vertices, data, name);
      case LOOPS:
      case MINUS:
      case SIGN:
      case SYMMETRY_FACTOR:
      case SYMMETRY_NUMBER:
      case NEW_ELINKS:
      case NEW_LOOPS:
      case NEW_PARTITION:
      case NEW_TOPOLOGY:
        break; //Ignore, but no error
      default:
        throw new QgrafImportException("Cannot use keyword " + name + " in diagram loop");
      }
    }
  }
  
  /**
   * Special list implementation that keeps track of a current element. Elements are automatically appended when iterating the first time.
   * @param <T> The element type. Must be cloneable and implement {@link AcceptsKeywords}.
   */
  static class LoopList<T extends AcceptsKeywords<T>> implements Cloneable2<LoopList<T>>, Iterable<T> {
    
    protected int currentLastIndex;
    protected boolean firstIterationCompleted;
    protected final List<T> list;
    protected final boolean repeatable;
    protected final IntFunction<T> ctor;
    protected boolean forceEmptyEntry;
    
    /**
     * Creates an empty {@link LoopList}.
     * @param ctor The constructor that creates an empty instance of the element type.
     * @param repeatable Whether the loop can be iterated more than once.
     */
    public LoopList(IntFunction<T> ctor, boolean repeatable) {
      this.list = new ArrayList<>();
      this.ctor = ctor;
      this.repeatable = repeatable;
      this.currentLastIndex = -1;
      this.firstIterationCompleted = false;
      this.forceEmptyEntry = false;
    }
    
    /**
     * Copy constructor. Does not copy parameters. Does not null check.
     * @param currentLastIndex Field value.
     * @param firstIterationCompleted  Field value.
     * @param list  Field value.
     * @param repeatable  Field value.
     * @param ctor Field value.
     * @param forceEmptyEntry Field value.
     */
    public LoopList(int currentLastIndex, boolean firstIterationCompleted, List<T> list, boolean repeatable,
        IntFunction<T> ctor, boolean forceEmptyEntry) {
      this.currentLastIndex = currentLastIndex;
      this.firstIterationCompleted = firstIterationCompleted;
      this.list = list;
      this.repeatable = repeatable;
      this.ctor = ctor;
      this.forceEmptyEntry = forceEmptyEntry;
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(currentLastIndex, firstIterationCompleted, forceEmptyEntry, list, repeatable);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof LoopList)) {
        return false;
      }
      LoopList<?> other = (LoopList<?>) obj;
      return currentLastIndex == other.currentLastIndex
          && firstIterationCompleted == other.firstIterationCompleted && forceEmptyEntry == other.forceEmptyEntry
          && Objects.equals(list, other.list) && repeatable == other.repeatable;
    }

    @Override
    public LoopList<T> clone() {
      return new LoopList<>(currentLastIndex, firstIterationCompleted, Cloneable2.clone(list, ArrayList::new), repeatable, ctor, forceEmptyEntry);
    }
    
    public T current() {
      if(currentLastIndex == -1) return null; //Not in loop
      return list.get(currentLastIndex);
    }
    
    public void currentAcceptData(KeywordName name, String data) throws QgrafImportException, BuilderInvalidException {
      if(forceEmptyEntry) throw new BuilderInvalidException("force empty entry");
      T current = current();
      if(current != null) current.acceptKeyword(name, data);
    }
    
    public int getActiveLoopCount() {
      if(currentLastIndex >= 0) return currentLastIndex + 1;
      return 0;
    }
    
    /**
     * @return size if known, -1 else
     */
    public int getKnownSize() {
      if(firstIterationCompleted) {
        return list.size();
      } else {
        return -1;
      }
    }
    
    void repeat() throws BuilderInvalidException {
      currentLastIndex++;
      if(firstIterationCompleted) {
        if(list.size() == currentLastIndex) {
          forceEmptyEntry = true;
          list.add(ctor.apply(currentLastIndex)); //TODO
        } else if(list.size() < currentLastIndex || forceEmptyEntry) {
          throw new BuilderInvalidException("cannot repeat on force-empty");
        }
        //current() now point to the next existing element
      } else {
        assert list.size() == currentLastIndex;
        list.add(ctor.apply(currentLastIndex));
      }
    }
    
    void enter() throws BuilderInvalidException {
      currentLastIndex = 0;
      if(firstIterationCompleted) {
        if(list.isEmpty())  {
          forceEmptyEntry = true;
          list.add(ctor.apply(0)); //TODO
        } else if(forceEmptyEntry) {
          throw new BuilderInvalidException("cannot enter force empty");
        }
        //now, current() will point to the existing element at 0
      } else {
        assert list.isEmpty();
        list.add(ctor.apply(0));
      }
    }
    
    void exit() throws BuilderInvalidException {
      firstIterationCompleted = true;
      currentLastIndex = -1;
      //Delete the last one, since it is empty
//      if(!firstIterationCompleted)
        list.remove(list.size() - 1);
    }

    @Override
    public Iterator<T> iterator() {
      return list.iterator();
    }
  }
  
  public int getDiagramCount() {
    return Math.max(0, diagramData.list.size());
  }
  
  DiagramData getDiagram(int index) {
    if(diagramData.list.size() <= index) return null;
    return diagramData.list.get(index);
  }
  
  int getLoadOnlyIndex() {
    return diagramData.loadOnlyBase0;
  }
  
  Iterable<DiagramData> diagrams() {
    return diagramData;
  }
  
  /**
   * Update the state of the builder. This method is called when the parser exits the innermost entered loop.
   * @throws BuilderInvalidException If the loop cannot be exited with the current builder state.
   */
  void pushExitCurrentLoop() throws BuilderInvalidException {
    LoopList<?> cl = getCurrentList();
    if(cl != null) cl.exit();
    loops.pollLast();
  }
  
  /**
   * Update the state of the builder. This method is called when the parser enters a loop or repeats a loop
   * iteration. This method should also be called immediately before {@link #pushExitCurrentLoop()}.
   * @param loop The loop to enter or repeat.
   * @throws QgrafImportException If the loop name is not valid at this parser position.
   * @throws BuilderInvalidException If the loop cannot be repeated with the current builder state.
   */
  void pushRepeatLoop(LoopName loop) throws QgrafImportException, BuilderInvalidException {
    Objects.requireNonNull(loop);
    
    if(loops.peekLast() == loop) { //repeat
      LoopList<?> cl = getCurrentList();
      if(cl != null) cl.repeat();
    } else { //enter
      if(loops.contains(loop)) throw new QgrafImportException("Cannot enter the same loop twice");
      loops.offerLast(loop);
      LoopList<?> cl = getCurrentList();
      if(cl != null) cl.enter();
    }
  }
  
  LoopList<?> getCurrentList() {
    return getList(loops.peekLast());
  }
  
  boolean canExitEarly() {
    return diagramData.canExitEarly;
  }
  
  LoopList<?> getList(LoopName l) {
    if(l == null) return null;
    
    DiagramData dd;
    switch (l) {
    case COMMAND_LINE_LOOP:
    case COMMAND_LOOP:
      return null; //ignore those two loops
    case DIAGRAM:
      return diagramData;
    case IN_LOOP:
      dd = diagramData.current();
      if(dd == null) return null;
      return dd.inLegData;
    case OUT_LOOP:
      dd = diagramData.current();
      if(dd == null) return null;
      return dd.outLegData;
    case PROPAGATOR_LOOP:
      dd = diagramData.current();
      if(dd == null) return null;
     return dd.propagatorData;
    case RAY_LOOP:
      dd = diagramData.current();
      if(dd == null) return null;
      qgraf.QgrafOutputBuilder.VertexData vd = dd.vertexData.current();
      return vd == null ? null : vd.rayData;
    case VERTEX_LOOP:
      dd = diagramData.current();
      if(dd == null) return null;
      return dd.vertexData;
    default:
      throw new IllegalStateException("current loop is null");
    }
  }
  
  /**
   * Update the state of the builder. This method is called when a data keyword is encountered. It will automatically 
   * pass the keyword value to the current active object ({@link DiagramData} or {@link LegData} or {@link PropagatorOrRayData}
   * or {@link VertexData}).
   * @param name The name of the keyword.
   * @param value The value for this keyword.
   * @throws QgrafImportException If the keyword is not legal at the current position.
   * @throws BuilderInvalidException If the keyword cannot be accepted with the current builder state.
   */
  void pushKeywordValue(KeywordName name, String value) throws QgrafImportException, BuilderInvalidException {
    if(name == KeywordName.UNKNOWN) return; //ignore unknown keywords
    
    LoopList<?> list = getCurrentList();
    if(list == null) {
      if(name == KeywordName.COMMAND_DATA 
      || name == KeywordName.DIAGRAM_INDEX 
      || name == KeywordName.PROGRAM) {
        return;
      } else {
        throw new QgrafImportException("Keyword '" + name.getData() + "' is not allowed outside of any loop");
      }
    }
    list.currentAcceptData(name, value);
  }
  
  void pushSourceTextLine(String line) {
    DiagramData current = diagramData.current();
    if(current != null) current.acceptSourceLine(line);
  }
  
  /**
   * Update the state of the builder. This method is called when a function is encountered. The current implementation
   * ignores all function data.
   * @param name The name of the function.
   * @param value The value for this function.
   * @throws QgrafImportException Not implemented.
   * @throws BuilderInvalidException Not implemented.
   */
  void pushFunctionValue(String functionName, String value) throws QgrafImportException, BuilderInvalidException {
    return; //Functions are ignored
  }
  
  @Override
  public QgrafOutputBuilder clone() {
    QgrafOutputBuilder qob = new QgrafOutputBuilder(new ArrayDeque<>(loops), diagramData.cloneCurrentOnly());
    if(constraints != null) qob.initConstraints(constraints.additionalIterations);
    return qob;
  }
  
  public DiagramConstraints constraints() {
    if(constraints == null) {
      constraints = new ConstraintsImpl();
    }
    return constraints;
  }

  //Called when a nodepath is done
  public void resetConstraints() {
    constraints = null;
  }
  
  /**
   * Implementation of {@link DiagramConstraints} that is linked to
   * this builder.
   */
  private class ConstraintsImpl implements DiagramConstraints {
    private final Map<LoopName, Integer> additionalIterations = new HashMap<>();
    
    private ConstraintsImpl() {}
    
    private ConstraintsImpl(Map<LoopName, Integer> add) {
      this.additionalIterations.putAll(add);
    }
    
    @Override
    public DiagramConstraints clone() {
      throw new UnsupportedOperationException("Cannot clone inner class");
    }
    
    @Override
    public boolean hasLoopLimit(LoopName name) {
      return getMaxLoopCount(name) != -1;
    }
    
    public int getMinLoopCount(LoopName name) {
      DiagramData dd = diagramData.current();
      switch (name) {
      case COMMAND_LINE_LOOP:
        return 1; //at least 1 line per command
      case COMMAND_LOOP:
      case DIAGRAM:
        return 0;
      case IN_LOOP:
        if(dd == null) return 0;
        if(dd.legsIn > 0) return dd.legsIn; //number of iteration is known
        if(dd.inLegData.getKnownSize() != -1) return dd.inLegData.getKnownSize();
        return 0;
      case OUT_LOOP:
        if(dd == null) return 0;
        if(dd.legsOut > 0) return dd.legsOut;
        if(dd.legsIn > 0 && dd.legs >= dd.legsIn) return dd.legs - dd.legsIn;
        if(dd.outLegData.getKnownSize() != -1) return dd.outLegData.getKnownSize();
        return 0;
      case PROPAGATOR_LOOP:
        if(dd == null) return 0;
        if(dd.propagators > 0) return dd.propagators;
        if(dd.propagatorData.getKnownSize() != -1) return dd.propagatorData.getKnownSize();
        return 0;
      case RAY_LOOP:
        if(dd == null) return 1;
        qgraf.QgrafOutputBuilder.VertexData vd = dd.vertexData.current();
        if(vd == null) return 1;
        if(vd.vertexDegree > 0) return vd.vertexDegree;
        if(vd.rayData.getKnownSize() != -1) return vd.rayData.getKnownSize();
        return 1;
      case VERTEX_LOOP:
        if(dd == null) return 0;
        if(dd.vertices > 0) return dd.vertices;
        if(dd.vertexData.getKnownSize() != -1) return dd.vertexData.getKnownSize();
        return 0;
      default:
        throw new IllegalArgumentException(); //Cannot occur, but compiler wants it
      }
    }
    
    public int getMaxLoopCount(LoopName name) {
      DiagramData dd = diagramData.current();
      switch (name) {
      case COMMAND_LINE_LOOP:
      case COMMAND_LOOP:
      case DIAGRAM:
        return -1;
      case IN_LOOP:
        if(dd == null) return -1;
        if(dd.legsIn > 0) return dd.legsIn; //number of iteration is known
        if(dd.inLegData.getKnownSize() != -1) return dd.inLegData.getKnownSize();
        return -1;
      case OUT_LOOP:
        if(dd == null) return -1;
        if(dd.legsOut > 0) return dd.legsOut;
        if(dd.legsIn > 0 && dd.legs >= dd.legsIn) return dd.legs - dd.legsIn;
        if(dd.outLegData.getKnownSize() != -1) return dd.outLegData.getKnownSize();
        return -1;
      case PROPAGATOR_LOOP:
        if(dd == null) return -1;
        if(dd.propagators > 0) return dd.propagators;
        if(dd.propagatorData.getKnownSize() != -1) return dd.propagatorData.getKnownSize();
        return -1;
      case RAY_LOOP:
        if(dd == null) return 1;
        qgraf.QgrafOutputBuilder.VertexData vd = dd.vertexData.current();
        if(vd == null) return 1;
        if(vd.vertexDegree > 0) return vd.vertexDegree;
        if(vd.rayData.getKnownSize() != -1) return vd.rayData.getKnownSize();
        return QgrafImportManager.unlimitedVertexDegree ? -1 : 6; //By default, qgraf is limited to 6, but can be recompiled to any number
      case VERTEX_LOOP:
        if(dd == null) return -1;
        if(dd.vertices > 0) return dd.vertices;
        if(dd.vertexData.getKnownSize() != -1) return dd.vertexData.getKnownSize();
        return -1;
      default:
        throw new IllegalArgumentException(); //Cannot occur, but compiler wants it
      }
    }
    
    @Override
    public boolean canExitLoop(LoopName name) {
      LoopList<?> list = getList(name);
      int done;
      if(list == null) {
        done = additionalIterations.getOrDefault(name, 0);
      } else {
        done = list.getActiveLoopCount() + additionalIterations.getOrDefault(name, 0);
      }
      int min = getMinLoopCount(name);
      
      if(min == -1) { //no min set -> can enter
        return true;
      } else {
        if(done < min) return false;
        else return true;
      }
    }
    
    @Override
    public boolean canEnterLoop(LoopName name) {
      
      
      LoopList<?> list = getList(name);
      if(list == null) return true;
      int done = list.getActiveLoopCount() + additionalIterations.getOrDefault(name, 0);
      int max = getMaxLoopCount(name);
      
      if(max == -1) { //no max set -> can enter
        return true;
      } else {
        if(done < max) return true;
        else return false;
      }
      
    }
    
    @Override
    public void assumeExited(LoopName name) {
      additionalIterations.put(name, 0);
    }
    
    @Override
    public void assumeEntered(LoopName name) {
      additionalIterations.compute(name, (n, v) -> v == null ? 1 : v + 1);
//      additionalIterations.merge(name, 1, (v1, v2) -> v1 + v2);
    }
  }

  Set<LoopName> getActiveLoops(boolean diagramMode) {
    Set<LoopName> s = new HashSet<>(loops);
    if(diagramMode) s.remove(LoopName.DIAGRAM);
    return s;
  }
}
