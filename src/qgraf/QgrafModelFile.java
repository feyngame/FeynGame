package qgraf;

import java.awt.Color;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import game.LineConfig;
import game.LineType;
import game.Presets;
import resources.Java8Support;

public class QgrafModelFile {

  public static class QgrafModelFormatException extends Exception {
    private static final long serialVersionUID = 4944928150404726983L;

    private QgrafModelFormatException(String message) {
      super(message);
    }

    private QgrafModelFormatException(String message, Throwable cause) {
      super(message, cause);
    }
  }
  
  public static enum PropagatorSign {
    PLUS, MINUS;
    
    public static boolean isSign(String s) {
      return "+".equals(s) || "-".equals(s);
    }
    
    public static PropagatorSign fromSign(String sign) {
      if(sign.equals("+")) return PLUS;
      else if(sign.equals("-")) return MINUS;
      else throw new IllegalArgumentException();
    }
  }
  
  public static class PropagatorDefinition {
    private final String identifier;
    private final String dualIdentifier;
    private final PropagatorSign sign;
    
    public PropagatorDefinition(String identifier, String dualIdentifier, PropagatorSign sign) {
      this.identifier = Objects.requireNonNull(identifier);
      this.dualIdentifier = Objects.requireNonNull(dualIdentifier);
      this.sign = Objects.requireNonNull(sign);
    }
    
    public boolean isSelfConjugate() {
      return Objects.equals(identifier, dualIdentifier);
    }
    
    public PropagatorSign getSign() {
      return sign;
    }
    
    public String getIdentifier() {
      return identifier;
    }
    
    public String getDualIdentifier() {
      return dualIdentifier;
    }
  }
  
  public static class VertexDefinition {
    private final List<String> identifiers;
    
    public VertexDefinition(String...identifiers) {
      this.identifiers = Arrays.asList(identifiers);
    }
    
    public int getVertexDegree() {
      return identifiers.size();
    }
    
    public List<String> getIdentifiers() {
      return identifiers;
    }
  }
  
  private static String requireVaildIdentifier(String string, int line, boolean more) throws QgrafModelFormatException {
    if(string.matches("[a-zA-Z0-9][a-zA-Z0-9_]*"))
      return string;
    else
      throw createModelException("String '" + string + "' is not a valid qgraf identifier", line, more);
  }
  
  private final List<VertexDefinition> vertices;
  private final List<PropagatorDefinition> propagators;
  private final Map<PropagatorDefinition, LineConfig> lineStyles;
  private final Map<VertexDefinition, Map.Entry<HashMap<String, Integer>, String>> vertexRules;
  private final String name;
  
  private boolean lineStylesCreated;
  private float colorOffset;
  private float lastHue;
  
  private static final float COLOR_INCREMENT = 4.0f / 9.0f;
  
  private QgrafModelFile(List<PropagatorDefinition> propagators, List<VertexDefinition> vertices, String name) {
    this.propagators = Objects.requireNonNull(propagators);
    this.vertices = Objects.requireNonNull(vertices);
    this.lineStyles = new HashMap<>();
    this.vertexRules = new HashMap<>();
    this.lineStylesCreated = false;
    this.colorOffset = 0.0f;
    this.lastHue = 0.0f;
    this.name = name;
  }
  
  private Color generateNextColor() {
    lastHue += COLOR_INCREMENT;
    float hue = (lastHue + colorOffset) % 1;
    Color c = Color.getHSBColor(hue, 0.9f, 0.9f);
    //check for back near 0
    if(hue < 0.001) {
      colorOffset += COLOR_INCREMENT*COLOR_INCREMENT;
      colorOffset = colorOffset % COLOR_INCREMENT;
    }
    return c;
  }
  
  public String getName() {
    return name;
  }
  
  private void fillInMaps() {
    if(lineStylesCreated) return;
    
    for(PropagatorDefinition pd : propagators) {
      boolean boson = pd.getSign() == PropagatorSign.PLUS;
      LineConfig lc = new LineConfig(boson ? LineType.PHOTON : LineType.FERMION, generateNextColor());
      lc.showArrow = !pd.isSelfConjugate();
      lc.identifier = new AbstractMap.SimpleEntry<>(pd.getIdentifier(), pd.getDualIdentifier());
      if(boson) { //Similar to photon
        lc.wiggleHeight = 10;
        lc.wiggleLength = 19;
        lc.wiggleOffset = 3;
      }
      //Create tex subscript if necessary
      String label;
      String id = pd.getIdentifier();
      int i = id.indexOf('_');
      if(i > 0 && i < id.length() - 1) {
        int e = id.indexOf(" ", i);
        if(e < 0) { //n space -> to end
          label = id.substring(0, i);
          label += "_{";
          label += id.substring(i+1);
          label += "}";
        } else {
          label = id.substring(0, i);
          label += "_{";
          label += id.substring(i+1, e);
          label += "}";
          if(e != id.length() - 1) {
            label += id.substring(e);
          }
        }
      } else {
        label = id;
      }
      lc.description = label;
      lineStyles.put(pd, lc);
    }
    int runningVertexId = 0;
    for(VertexDefinition vd : vertices) {
      HashMap<String, Integer> idCounts = vd.getIdentifiers().stream()
          .collect(Collectors.groupingBy(Function.identity(), HashMap::new, Collectors.summingInt(e -> 1)));
      String vertexName = String.valueOf(runningVertexId);
      runningVertexId += 1;
      Map.Entry<HashMap<String, Integer>, String> e = new AbstractMap.SimpleEntry<>(idCounts, vertexName);
      vertexRules.put(vd, e);
    }
    
    lineStylesCreated = true;
  }
  
  public Presets createPreset() {
    fillInMaps();
    Presets p = emptyPreset();
    for(LineConfig c : lineStyles.values()) {
      p.presets.add(c);
    }
    p.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
    
    for(Map.Entry<HashMap<String, Integer>, String> vd : vertexRules.values()) {
      p.rules.add(vd);
    }
    
    Presets pp = emptyPreset();
    pp.merge(p);
    return pp;
  }
  
  private static Presets emptyPreset() {
    Presets p = new Presets(); 
    //The default ctor creates a preset that is NOT empty!
    p.presets.clear();
    p.vertexPresets.clear();
    p.fIPresets.clear();
    p.feynmanRuleTemplates.clear();
    p.rules.clear();
    p.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
    
    return p;
  }
  
  private static QgrafModelFormatException createModelException(String text, int number, boolean more) {
     return new QgrafModelFormatException(text + (more ? " (line " + number + " and previous lines)" : " (line " + number + ")")); 
   }
  
  private static final int NOT_FOUND = Integer.MAX_VALUE;
  public static QgrafModelFile importFromFile(Path path) throws IOException, QgrafModelFormatException {
    int lineNumber = 1; //For output, start counting at 1
    boolean moreLines = false;
    
    Map<String, PropagatorDefinition> propagatorMap = new HashMap<>();
    List<PropagatorDefinition> propagators = new ArrayList<>();
    List<VertexDefinition> vertices = new ArrayList<>();
    
    Iterator<String> iter = Files.lines(path, StandardCharsets.UTF_8).iterator();
    while(iter.hasNext()) {
      String line = iter.next();
      moreLines = false;
      
      line = Java8Support.strip(line);
      if(line.startsWith("%") || line.startsWith("#") || line.startsWith("*") || Java8Support.isBlank(line)) 
        continue;
      if(!line.startsWith("[")) 
        throw createModelException("Non-comment lines must start with '[' unless they continue a previous statement", lineNumber, moreLines);
      
      while(!line.endsWith("]")) { //try to pull more lines
        if(!iter.hasNext())
          throw createModelException("Non-comment lines must end with ']' unless they are continued on the next line", lineNumber, moreLines);
        else 
          line += Java8Support.strip(iter.next()); //whitespace/comments are NOT allowed in the middle of a declaration!
          moreLines = true;
          lineNumber += 1;
      }
      
      int firstQuote     = line.indexOf("'");
      int firstSemicolon = line.indexOf(";");
      int firstComma     = line.indexOf(",");
      int firstEquals    = line.indexOf("=");
      //To simplify the conditions, DNE should be a large value here:
      if(firstQuote     < 0) firstQuote     = NOT_FOUND;
      if(firstSemicolon < 0) firstSemicolon = NOT_FOUND;
      if(firstComma     < 0) firstComma     = NOT_FOUND;
      if(firstEquals    < 0) firstEquals    = NOT_FOUND;
      
      if(firstEquals != NOT_FOUND && firstEquals < firstComma && firstEquals < firstQuote && firstEquals < firstSemicolon) {
        //model constant; can ignore this
        //[constname='constvalue']
      } else if( (firstSemicolon == NOT_FOUND && firstQuote == NOT_FOUND && firstEquals == NOT_FOUND) ||
          (firstSemicolon < firstQuote && firstSemicolon < firstEquals)) {
        //normal declaration without any custom data 
        //or declaration with custom data after the semicolon
        if(firstSemicolon == NOT_FOUND) {
          line = line.substring(1, line.length() - 1); //strip [ and ]
        } else {
          line = line.substring(1, firstSemicolon);
        }
        
        String[] parts = line.split(",");
        for(int i = 0; i < parts.length; i++) 
          parts[i] = Java8Support.strip(parts[i]);
        
        if(parts.length < 3) 
          throw createModelException("Definition needs at least three parts separated by ','; found " + parts.length, lineNumber, moreLines);
        
        if(PropagatorSign.isSign(parts[2])) { //propagator definition
          String p0 = requireVaildIdentifier(parts[0], lineNumber, moreLines);
          String p1 = requireVaildIdentifier(parts[1], lineNumber, moreLines);
          PropagatorDefinition pd = new PropagatorDefinition(p0, p1, PropagatorSign.fromSign(parts[2]));
          propagators.add(pd);
          propagatorMap.put(pd.getIdentifier(), pd);
          if(!pd.isSelfConjugate()) 
            propagatorMap.put(pd.getDualIdentifier(), pd);
        } else { //vertex definition
          VertexDefinition vd = new VertexDefinition(parts);
          for(String id : vd.getIdentifiers()) {
            requireVaildIdentifier(id, lineNumber, moreLines);
            if(!propagatorMap.containsKey(id))
              throw createModelException("Vertex definition contains unknown field '" + id + "'", lineNumber, moreLines);
          }
          vertices.add(vd);
        }
      } else {
        throw createModelException("Cannot interpret line as propagator or vertex definition", lineNumber, moreLines);
      }
      
      lineNumber += 1;
    }
    
    return new QgrafModelFile(propagators, vertices, path.getFileName().toString());
  }
}
