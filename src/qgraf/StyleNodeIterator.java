package qgraf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.jgrapht.Graph;

import qgraf.QgrafStyleAction.ActionType;
import qgraf.QgrafStyleNode.KeywordName;
import qgraf.QgrafStyleNode.LoopName;
import qgraf.QgrafStyleNode.NodeContentType;
import qgraf.QgrafStyleNode.StyleNodeContent;
import resources.Java8Support;
import ui.Frame;

/**
 * This class manages the iteration of elements in the style graph by maintaining a list of {@link NodePath}s.
 *  
 * @author Lars Bündgen
 */
class StyleNodeIterator {
  private static final Logger LOGGER = Frame.getLogger(StyleNodeIterator.class);
  
  private final Graph<QgrafStyleNode, QgrafStyleAction> graph;
  private final Queue<NodePath> activePaths;
  private final ForkableIOStringSupplier io;
  private final int maxBackStatements;
  private int maxPathCount;
  
  private final boolean allowLiteralLengthDetection = true;
  private final boolean diagramMode;
  
  StyleNodeIterator(Graph<QgrafStyleNode, QgrafStyleAction> graph, QgrafStyleNode startNode, IOStringSupplier forContext,
      int loadOnlyBase0, boolean diagramMode) {
    this.graph = Objects.requireNonNull(graph);
    this.activePaths = new PriorityQueue<>(Comparator.comparing(NodePath::getCompareOrder));
//    this.activePaths = new TreeSet<>(Comparator.comparing(NodePath::getCompareOrder));
    this.io = new ForkableIOStringSupplier(IOStringSupplier.skipInitialWhitespace(forContext));
    
    NodePath initial = new NodePath(startNode, io.initial(), loadOnlyBase0, true);
    
    if(diagramMode) {
      try {
        initial.builder.pushRepeatLoop(LoopName.DIAGRAM);
      } catch (QgrafImportException | BuilderInvalidException e) {
        throw new RuntimeException(e);
      }
    }
    this.diagramMode = diagramMode;
    
    activePaths.add(initial);
    this.maxPathCount = 1; 
    this.maxBackStatements = (int) graph.vertexSet().stream().filter(qsn -> qsn.getContentType() == NodeContentType.BACK).count();
  }
  
  public ForkableIOStringSupplier getLineSupplier() {
    return io;
  }
  
  public NodePath pollNextPath() throws QgrafImportException {
    if(activePaths.isEmpty()) return null; //No more paths
    
    NodePath active = activePaths.poll(); //least progressed path
    while(active.isIncomplete()) {
      NodePath branched = active.advance();
      if(branched != null) {
        activePaths.add(branched);
      }
    }
    assert active.isComplete();
    maxPathCount = Math.max(maxPathCount, activePaths.size());
    return active;
  }
  
  public void addAsNewPath(NodePath old) {
    assert old.isComplete();
    //This will reuse the builder, so that becomes invalid for the old path to use
    NodePath path = old.createNewIncomplete();
    activePaths.add(path);
    
    io.setLowestSupplier(activePaths.peek().reader);
  }
  
  public int pathCount() {
    return activePaths.size();
  }
  
  public int maxPathCount() {
    return maxPathCount;
  }
  
  static enum SafePointType {
    LINEBREAK_ONLY(Pattern.quote(QgrafStyleNode.LINEBREAK)), NON_IDENTIFIER("[^A-Za-z0-9\\\\+\\\\-_]");
    
    private final Pattern compiled;
    
    private SafePointType(String regex) {
      this.compiled = Pattern.compile(regex);
    }
    
    public Pattern getPattern() {
      return compiled;
    }
    
    public SafePointType upgrade(SafePointType newType) {
      Objects.requireNonNull(newType);
      if(newType == LINEBREAK_ONLY) return LINEBREAK_ONLY;
      return this;
    }
  }
  
  public class NodePath implements Iterable<StyleNodeContent> {
    private final List<StyleNodeContent> content;
    private final StashList<StyleNodeContent> readAheadContent;
    private QgrafStyleNode head;
    private final Set<QgrafStyleNode> enteredLoops; //Detects repeating one loop over and over again
    
    //Context stuff: This is conceptually not part of a nodepath object,
    //but one set of instances is required per nodepath, so they are stored here
    private final IOStringSupplier reader;
    private final QgrafOutputBuilder builder;
    private final StringBuilder incompleteLine;
    
    //Contract: if this is flipped to true, it will not change back AND the lists are not modified
    private boolean safePointFound;
    private SafePointType safePointType;
    
    private boolean isLowQuality;
    private boolean isEndOfStyle;
    private final boolean isFirstPath;
    
    private NodePath(Collection<StyleNodeContent> readAheadContent, QgrafStyleNode head,
        IOStringSupplier sup, QgrafOutputBuilder build, CharSequence builderContent, boolean first) {
      this.content = new ArrayList<>();
      this.readAheadContent = new StashList<>(readAheadContent);
      this.head = head;
      this.safePointFound = false;
      this.safePointType = SafePointType.NON_IDENTIFIER;
      this.enteredLoops = new HashSet<>();
//      this.readAheadContent.add(head.getContent());
      this.reader = sup;
      this.builder = build;
      this.incompleteLine = new StringBuilder(builderContent);
      this.isEndOfStyle = false;
      this.isLowQuality = false;
      this.isFirstPath = first;
    }
    
    private NodePath(QgrafStyleNode head, IOStringSupplier sup, int loadOnlyBase0, boolean first) {
      this.content = new ArrayList<>();
      this.readAheadContent = new StashList<>();
      this.head = head;
      this.safePointFound = false;
      this.safePointType = SafePointType.NON_IDENTIFIER;
      this.enteredLoops = new HashSet<>();
//      this.readAheadContent.add(head.getContent()); //Not for initial path
      this.reader = sup;
      this.builder = new QgrafOutputBuilder(loadOnlyBase0);
      this.incompleteLine = new StringBuilder();
      this.isEndOfStyle = false;
      this.isLowQuality = false;
      this.isFirstPath = first;
    }
    
    String printContent() {
      return content.stream().map(Object::toString).collect(Collectors.joining(" , "));
    }
    
    IOStringSupplier contextLineSupplier() {
      return reader;
    }
    
    QgrafOutputBuilder contextOutputBuilder() {
      return builder;
    }
    
    StringBuilder contextIncompleteLine() {
      return incompleteLine;
    }
    
    private NodePath(List<StyleNodeContent> content, StashList<StyleNodeContent> readAheadContent, QgrafStyleNode head,
        Set<QgrafStyleNode> enteredLoops, IOStringSupplier reader, QgrafOutputBuilder builder, boolean safePointFound,
        StringBuilder incompleteLine, boolean isEndOfStyle, SafePointType safePointType, boolean isLowQuality, boolean isFirstPath) {
      this.content = content;
      this.readAheadContent = readAheadContent;
      this.head = head;
      this.enteredLoops = enteredLoops;
      this.reader = reader;
      this.builder = builder;
      this.safePointFound = safePointFound;
      this.incompleteLine = incompleteLine;
      this.isEndOfStyle = isEndOfStyle;
      this.safePointType = safePointType;
      this.isLowQuality = isLowQuality;
      this.isFirstPath = isFirstPath;
    }

    boolean isIncomplete() {
      return !safePointFound;
    }
    
    boolean isLowQuality() {
      return isLowQuality;
    }
    
    boolean isComplete() {
      return safePointFound;
    }
    
    boolean isEndOfStyle() {
      return isEndOfStyle && safePointFound;
    }
    
    boolean isFirstPath() {
      return isFirstPath;
    }
    
    //Amount of linebreaks in all the literals
    int literalLinebreaks() {
      return content.stream()
          .filter(s -> s.getType() == NodeContentType.LITERAL)
          .mapToInt(s -> QgrafOutputReader.countLineBreaks(s.getData())) //For literals, data and regex are the same
          .sum();
    }
    
    private int getCompareOrder() {
      return reader.progress();
    }
    
    Pattern getRegex(String prefix, String suffix) {
      if(!safePointFound) throw new IllegalStateException("not completed");
      StringBuilder sb = new StringBuilder();
      
      if(isEndOfStyle) { //find the last literal and change it
        ListIterator<StyleNodeContent> iter = content.listIterator(content.size());
        while(iter.hasPrevious()) {
          StyleNodeContent c = iter.previous();
          if(c.getType() == NodeContentType.BACK || c.getType() == NodeContentType.LOOP || c == StyleNodeContent.EXIT) {
            continue;
          } else if(c.getType() == NodeContentType.LITERAL) { //Last literal found, edit
            String literal = c.getData();
            String newLiteral = Java8Support.stripTrailing(literal); //remove all trailing linebreaks
            if(newLiteral.isEmpty()) {
              iter.remove();
            } else {
              iter.set(StyleNodeContent.forLiteral(newLiteral));
            }
            break;
          } else { //data is last, can't do anything
            break;
          }
        }
      }
      
      //Now the same thing for start: remove leading whitespace
      if(isFirstPath) {
        ListIterator<StyleNodeContent> iter = content.listIterator(0);
        while(iter.hasNext()) {
          StyleNodeContent c = iter.next();
          if(c.getType() == NodeContentType.BACK || c.getType() == NodeContentType.LOOP || c == StyleNodeContent.EXIT) {
            continue;
          } else if(c.getType() == NodeContentType.LITERAL) { //Last literal found, edit
            String literal = c.getData();
            String newLiteral = Java8Support.stripLeading(literal); //remove all trailing linebreaks
            if(newLiteral.isEmpty()) {
              iter.remove();
            } else {
              iter.set(StyleNodeContent.forLiteral(newLiteral));
            }
            break;
          } else { //data is last, can't do anything
            break;
          }
        }
      }
      
      boolean hasMatchable = false;
      if(prefix != null) sb.append(prefix);
      for(StyleNodeContent c : content) {
        if(c.getType() == NodeContentType.BACK || c.getType() == NodeContentType.LOOP || c == StyleNodeContent.EXIT) {
          continue;
//          throw new IllegalStateException("invalid content type");
        } else if(c.getType() == NodeContentType.LITERAL) {//Literal: add quotation marks
          hasMatchable = true;
          sb.append(Pattern.quote(c.getRegex()));
        } else if(c.getType() == NodeContentType.DATA){ //Data: add a capturing group
          hasMatchable = true;
          sb.append("(").append(c.getRegex()).append(")");
        } else if(c.getType() == NodeContentType.GENERATED){//generated: this is the exact pattern
          hasMatchable = true;
          sb.append(c.getRegex());
        } else {
          throw new IllegalStateException();
        }
      }
      if(suffix != null) sb.append(suffix);
      return hasMatchable ? Pattern.compile(sb.toString()) : null; //null means branch cannot be matched
    }
    
    /**
     * @return {@code true} when the end of the style is reached. In that case, this path has no content.
     */
    boolean isEmpty() {
      if(!safePointFound) throw new IllegalStateException("not completed");
      return content.isEmpty();
    }

    private NodePath createNewIncomplete() {
      if(!safePointFound) throw new IllegalStateException("not completed");
      if(isEndOfStyle) throw new IllegalStateException("cannot create new branch from end of style");
      return new NodePath(new ArrayList<>(readAheadContent), head, reader, builder, incompleteLine, false);
    }
    
    private NodePath fork() {
      return new NodePath(new ArrayList<>(content), new StashList<>(readAheadContent),
          head, new HashSet<>(enteredLoops), reader.fork(), builder.clone(), safePointFound,
          new StringBuilder(incompleteLine), isEndOfStyle, safePointType, isLowQuality, isFirstPath);
    }
    
    /**
     * Called every time a new node is added to the read-ahead, detects safe points
     * @throws QgrafImportException
     */
    private void updateSafeState() throws QgrafImportException {
      //last added must be kw or func:
      if(safePointFound) throw new IllegalStateException("already completed");
      
      StyleNodeContent node = readAheadContent.getLast(); //Do not remove the node
      if(node.getType() == NodeContentType.BACK) {
        readAheadContent.removeLast(); //This removes <back>
        
        boolean found = false;
        while(!readAheadContent.isEmpty() && !found) {
          StyleNodeContent previous = readAheadContent.stashLast();
          
          if(previous.getType() == NodeContentType.BACK) {
            throw new IllegalStateException("<back> found in content queue");
          } else if(previous.getType() == NodeContentType.LITERAL) {
            String literalText = previous.getData();
            if(literalText.isEmpty()) throw new IllegalStateException("empty literal in content queue");
            String newLiteralText = literalText.substring(0, literalText.length() - 1);
            if(!newLiteralText.isEmpty()) {
              readAheadContent.addLast(StyleNodeContent.forLiteral(newLiteralText));
            }
            readAheadContent.removeStashTop(); //This deletes the old literal
            found = true;
          } else if(previous.getType() == NodeContentType.DATA) {
            LOGGER.warning("<back> statement deletes part of " + 
                ((previous instanceof KeywordName) ? " keyword <" + previous.getData() + ">" : "function [" + previous.getData() + "]"));
            found = true;
          } else if(previous.getType() == NodeContentType.GENERATED) {
            LOGGER.warning("<back> statement deletes part of generated loop matcher");
            found = true;
          }
        }
        
        if(!found) throw new QgrafImportException("<back> has no content to delete");
        
        readAheadContent.restoreStash();
      
      } else if(node.getType() == NodeContentType.DATA) { //safe point is possible
        //First, upgrade safe point type
        final SafePointType newType;
        if(node instanceof KeywordName) {
          if(QgrafStyleNode.canMatchSafeChars(node.getRegex())) {
            newType = SafePointType.LINEBREAK_ONLY;
          } else {
            newType = SafePointType.NON_IDENTIFIER;
          }
        } else { //function
          newType = SafePointType.LINEBREAK_ONLY; //TODO allow identifier-only functions?
        }
        safePointType = safePointType.upgrade(newType);
        
        //Go back unitl a literal is found
        readAheadContent.stashLast(); //stash current KW
        
        boolean found = false;
        while(!readAheadContent.isEmpty() && !found) {
          StyleNodeContent previous = readAheadContent.getLast();
          
          if(previous.getType() == NodeContentType.BACK) {
            throw new IllegalStateException("<back> found in content queue");
          } else if(previous.getType() == NodeContentType.LITERAL) {
            String literalText = previous.getData();
            Pattern safeChars = safePointType.getPattern();
            if(safeChars.matcher(literalText).find()) {
              
              readAheadContent.drainList(content::add);  //the read-ahead contains everything up to and including the literal
              readAheadContent.restoreStash(); //the remaining elements go to the read-ahead
              safePointFound = true;
              builder.resetConstraints();
              
              found = true;
            } else {
              //Try more literals, stash this one
              readAheadContent.stashLast();
            }
          } else if(previous.getType() == NodeContentType.DATA || previous.getType() == NodeContentType.GENERATED) {
            //This data was already checked to be not safe
            readAheadContent.restoreStash();
            found = true;
          } else {
            readAheadContent.stashLast(); //stash and try next element
          }
        }
        
        if(!found) { //if the readahead was empty before safe point was found
          readAheadContent.restoreStash();
        }
        
      } else if(node.getType() == NodeContentType.GENERATED) {
        safePointType = safePointType.upgrade(SafePointType.LINEBREAK_ONLY);
      } else if(node.getType() == NodeContentType.LITERAL) {
        //Inspect below
        StyleNodeContent below = readAheadContent.peekBelow();
        if(below != null && below.getType() == NodeContentType.LITERAL) {
          //remove current and below literal
          readAheadContent.removeLast(); 
          readAheadContent.removeLast();
          StyleNodeContent combined = StyleNodeContent.forLiteral(below.getData() + node.getData());
          readAheadContent.addLast(combined);
        }
        
        
        //BEGIN max back statement
        StyleNodeContent newNode = readAheadContent.getLast(); //might have been replaced by previous step
        String literalText = newNode.getData();
        if(literalText.length() > maxBackStatements && allowLiteralLengthDetection) {
          Set<LoopName> currentLoops = builder.getActiveLoops(diagramMode);
          
          //Apply all exit nodes, by reverse iterating
          readAheadContent.requireStashEmpty();
          ListIterator<StyleNodeContent> rev = readAheadContent.listIterator(readAheadContent.size());
          while(rev.hasPrevious()) {
            StyleNodeContent snc = rev.previous();
            if(snc == StyleNodeContent.EXIT) {
              LoopName ln = (LoopName) rev.previous(); //TODO unsafe cast?
              currentLoops.remove(ln);
            } else if(snc instanceof LoopName) {
              currentLoops.add((LoopName) snc);
            }
          }
                    
          if(!currentLoops.isEmpty()) { //allow this only inside an inner loop

            int split = literalText.length() - maxBackStatements;
            String reducedText = literalText.substring(0, split);
            Pattern safeChars = safePointType.getPattern();
            if(safeChars.matcher(reducedText).find()) {

              String remainingText = literalText.substring(split);
              StyleNodeContent first = StyleNodeContent.forLiteral(reducedText);
              StyleNodeContent second = StyleNodeContent.forLiteral(remainingText);

              readAheadContent.removeLast(); //The literal
              readAheadContent.addLast(first);
              readAheadContent.drainList(content::add);  //the read-ahead contains everything up to and including the literal
              readAheadContent.clear(); //the remaining elements go to the read-ahead
              if(!remainingText.isEmpty()) readAheadContent.addLast(second);
              safePointFound = true;
              builder.resetConstraints();
            }

          }
        }
        //END max back statements
      } else if(node.getType() == NodeContentType.LOOP || node == StyleNodeContent.EXIT) { //Bubble-down below literals
        readAheadContent.removeLast(); //loop node
        readAheadContent.stashWhile(c -> c.getType() == NodeContentType.LITERAL);
        readAheadContent.addLast(node);
        readAheadContent.restoreStash();
//      } else if(node == StyleNodeContent.EXIT) { //This adds loop and exit
//        readAheadContent.removeLast(); //exit node
//        StyleNodeContent loop = readAheadContent.removeLast();
//        if(loop.getType() != NodeContentType.LOOP) throw new IllegalStateException("Unexpected EXIT");
//        readAheadContent.stashWhile(c -> c.getType() == NodeContentType.LITERAL);
//        readAheadContent.addLast(loop);
//        readAheadContent.addLast(node);
//        readAheadContent.restoreStash();
//        
      }
    }
    
    private NodePath checkLoopRepeated(QgrafStyleNode loopNode) {
//      System.err.println("loop repeated");
      return null;
      
//      if(readAheadContent.removeLast() != loop) throw new IllegalStateException();
//      boolean everFound = false;
//      while(true) {
//        if(!readAheadContent.stashWhile(c -> c != loop))
//          break;
//        if(!readAheadContent.isEmpty())
//          readAheadContent.stashLast();
//        everFound = true;
//      }
//      //now list has everything up to and including loop, stash has loop content
//      
//      if(everFound) {
//        readAheadContent.restoreStashTop();
//        
//        NodePath copy = fork(); //This erases the stash in the copy
////        copy.readAheadContent.removeLast(); //removes the loop
//        assert copy.readAheadContent.getLast() == loop;
//        copy.readAheadContent.addLast(StyleNodeContent.EXIT);
//        copy.readAheadContent.addLast(StyleNodeContent.forNonCapture(loop, this.readAheadContent.getStash()));
//        copy.isLowQuality = true;
//        this.readAheadContent.restoreStash();
//        return copy;
//      } else {
//        readAheadContent.restoreStash();
//        return null;
//      }      
    }
    
    /**
     * Processes the next node. If there is a loop, {@code this} will be a path that skips/exits the loop
     * and the returned path will be the one that enters/repeats the loop. Otherwise, the return value is empty.
     * <p>
     * After advancing, the path is automatically checked for completeness.
     * @return A branching path if it exists
     * @throws QgrafImportException 
     */
    private NodePath advance() throws QgrafImportException {
      if(safePointFound) throw new IllegalStateException("already completed");
      assert content.size() == 0;
      
      Set<QgrafStyleAction> options = graph.outgoingEdgesOf(head);
      if(options.isEmpty()) { 
        //end of style -> add everything
        safePointFound = true;
        isEndOfStyle = true;
        content.addAll(readAheadContent);
        readAheadContent.clear();
        builder.resetConstraints();
        return null;
      } else if(options.size() == 1) {
        QgrafStyleAction option = options.stream().findAny().orElseThrow(NoSuchElementException::new);
        QgrafStyleNode next = graph.getEdgeTarget(option);
        
        if(head.getContentType() == NodeContentType.LOOP) {
          if(option.getType() != ActionType.REPEAT_LOOP) throw new QgrafImportException("Loop with one exit must be repeat");
          
          LoopName loopName = (LoopName) head.getContent();
          if(builder.constraints().canEnterLoop(loopName) && builder.constraints().canExitLoop(loopName)) {
            NodePath copy = fork();
            //This -> end of file, copy -> repeat
            
            this.readAheadContent.add(StyleNodeContent.EXIT);
            this.updateSafeState();
            
            safePointFound = true;
            isEndOfStyle = true;

            content.addAll(readAheadContent);
            readAheadContent.clear();
            builder.resetConstraints();
            
            copy.readAheadContent.add(next.getContent());
            copy.enteredLoops.add(head);
            copy.head = next;
            copy.builder.constraints().assumeEntered(loopName);
            copy.updateSafeState();
            return copy;
          } else if(builder.constraints().canEnterLoop(loopName)) {
            this.readAheadContent.add(next.getContent());
            this.enteredLoops.add(head);
            this.head = next;
            this.builder.constraints().assumeEntered(loopName);
            this.updateSafeState();
            return null;
          } else if(builder.constraints().canExitLoop(loopName)) {
            safePointFound = true;
            isEndOfStyle = true;

            content.addAll(readAheadContent);
            readAheadContent.clear();
            builder.resetConstraints();
            
            return null;
          } else {
            throw new IllegalStateException("constraints have no option for loop");
          }
          
        } else {
          if(option.getType() != ActionType.DEFAULT) throw new QgrafImportException("non-default edge from non-loop");
          readAheadContent.addLast(next.getContent());
          head = next;
          updateSafeState();
          return null;
        }
      } else if(options.size() == 2) {
        if(head.getContentType() != NodeContentType.LOOP) throw new QgrafImportException("two options for non loop");
        
        QgrafStyleAction repeatOption = options.stream().filter(e -> e.getType() == ActionType.REPEAT_LOOP).findAny().orElseThrow(NoSuchElementException::new); 
        QgrafStyleAction exitOption = options.stream().filter(e -> e.getType() == ActionType.EXIT_LOOP).findAny().orElseThrow(NoSuchElementException::new); 
        
        boolean exitOnly = false;
        NodePath generatedLoopPath = null;
        if(enteredLoops.contains(head)) {
          generatedLoopPath = checkLoopRepeated(head);
          exitOnly = true;
        }
        
        LoopName loopName = (LoopName) head.getContent();
        if(builder.constraints().canEnterLoop(loopName) && builder.constraints().canExitLoop(loopName) && !exitOnly) {
          NodePath copy = fork();
          QgrafStyleNode repeatNode = graph.getEdgeTarget(repeatOption);
          QgrafStyleNode exitNode = graph.getEdgeTarget(exitOption);
          this.readAheadContent.add(StyleNodeContent.EXIT);
          this.updateSafeState(); //needed for bubble-down
          this.readAheadContent.add(exitNode.getContent());
          copy.readAheadContent.add(repeatNode.getContent());
          copy.enteredLoops.add(head);
          this.head = exitNode;
          copy.head = repeatNode;
          this.builder.constraints().assumeExited(loopName);
          copy.builder.constraints().assumeEntered(loopName);
          this.updateSafeState();
          copy.updateSafeState();
          return copy;
        } else if(builder.constraints().canEnterLoop(loopName) && !exitOnly) {
          QgrafStyleNode repeatNode = graph.getEdgeTarget(repeatOption);
          this.readAheadContent.add(repeatNode.getContent());
          this.enteredLoops.add(head);
          this.head = repeatNode;
          this.builder.constraints().assumeEntered(loopName);
          this.updateSafeState();
          return generatedLoopPath;
        } else if(builder.constraints().canExitLoop(loopName) || exitOnly) {
          QgrafStyleNode exitNode = graph.getEdgeTarget(exitOption);
          this.readAheadContent.add(StyleNodeContent.EXIT);
          this.updateSafeState();
          this.readAheadContent.add(exitNode.getContent());
          this.head = exitNode;
          this.builder.constraints().assumeExited(loopName);
          this.updateSafeState();
          
          if(generatedLoopPath != null) {
            generatedLoopPath.readAheadContent.add(exitNode.getContent());
            generatedLoopPath.head = exitNode;
            generatedLoopPath.updateSafeState();
          }
          
          return generatedLoopPath;
        } else {
          throw new IllegalStateException("constraints have no option for loop");
        }
      } else {
        throw new IllegalStateException("more than two options");
      }
    }

    @Override
    public Iterator<StyleNodeContent> iterator() {
      if(isIncomplete()) throw new IllegalStateException();
      return content.iterator();
    }

  }


}
