package qgraf;

import java.awt.Color;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import game.FeynLine;
import game.Line;
import game.LineConfig;
import game.LineType;
import game.Presets;
import game.Vertex;
import resources.Java8Support;

/**
 * A representation of a FeynGame model. This class will map particle identifiers to {@link LineStyle}s, 
 * from which a {@link FeynLine} of the correct style can be created. It also manages the generation of line styles for
 * identifiers that are not known by FeynGame.
 * 
 * @author Lars Bündgen
 */
public class ModelData {

  private static boolean dashed = true;
  
  /**
   * Set a global configuration value.<br>
   * If {@code true}, all generated line styles will be dashed. If {@code false}, all generated line styles will be solid.
   * @param value The new configuration value
   */
  public static void setDashed(boolean value) {
    dashed = value;
  }
  
  /**
   * Get a global configuration value.<br>
   * If {@code true}, all generated line styles will be dashed. If {@code false}, all generated line styles will be solid.
   * @return The current configuration value
   */
  public static boolean getDashed() {
    return dashed;
  }
  
  /**
   * The {@link LineStyle} class stores all information about a {@link FeynLine} except the position of start and end.
   * The main difference to {@link LineConfig} is that it can also store the direction of the arrow and the curve radius and loop angle.
   * 
   * @author Lars Bündgen
   */
  public static final class LineStyle implements Cloneable2<LineStyle> {
    private final FeynLine lineTemplate;
    private final boolean reverse;
    
    /**
     * Creates a new {@link LineStyle} using a {@link FeynLine} as a template.
     * @param l The template line.
     * @param reverse If {@code false}, the arrow will point from start to end, if {@code true} the arrow will point
     * from end to start. 
     */
    public LineStyle(FeynLine l, boolean reverse) {
      this.lineTemplate = l.getCopy();
      this.reverse = reverse;
    }
    
    /**
     * Creates a new {@link LineStyle} using a {@link LineConfig} as a template.
     * @param lc The template line config.
     * @param reverse If {@code false}, the arrow will point from start to end, if {@code true} the arrow will point
     * from end to start. 
     */
    public LineStyle(LineConfig lc, boolean reverse) {
      this.lineTemplate = new FeynLine(new Line(), lc, 0);
      this.reverse = reverse;
    }

    /**
     * Returns a {@link FeynLine} that has this line style and the two points as start and end. The returned line instance is linked
     * to this {@code LineStyle}; modifications to the curve radius and some other properties will affect both the line style 
     * and the returned line.<br>
     * This version accepts jgrapht's {@link org.jgrapht.alg.drawing.model.Point2D} class
     * @param start The starting point of the line
     * @param end The endpoint of the line
     * @return The {@link FeynLine} with this style and the selected start and end point
     * @see #getFeynLine(game.Point2D, game.Point2D)
     */
    public FeynLine getFeynLine(org.jgrapht.alg.drawing.model.Point2D start, org.jgrapht.alg.drawing.model.Point2D end) {
      return getFeynLine(new game.Point2D(start.getX(), start.getY()), new game.Point2D(end.getX(), end.getY()));
    }
    
    /**
     * Returns a {@link FeynLine} that has this line style and the two points as start and end. The returned line instance is linked
     * to this {@code LineStyle}; modifications to the curve radius and some other properties will affect both the line style 
     * and the returned line.<br>
     * This version accepts FeynGame's {@link game.Point2D} class
     * @param start The starting point of the line
     * @param end The endpoint of the line
     * @return The {@link FeynLine} with this style and the selected start and end point
     * @see #getFeynLine(org.jgrapht.alg.drawing.model.Point2D, org.jgrapht.alg.drawing.model.Point2D)
     */
    public FeynLine getFeynLine(game.Point2D start, game.Point2D end) {
      FeynLine l = lineTemplate.getCopy();
      if(reverse) {
        l.setStart(end);
        l.setEnd(start);
      } else {
        l.setStart(start);
        l.setEnd(end);
      }
      return l;
    }
    
    /**
     * Set the curve radius of the {@link FeynLine} accessed with {@link #getFeynLine(game.Point2D, game.Point2D)}.
     * @param height The curve radius. Set to 0 for a straight line
     */
    public void setLineHeight(double height) {
      lineTemplate.setHeight(height);
    }
    
    /**
     * Set the loop angle of the {@link FeynLine} accessed with {@link #getFeynLine(game.Point2D, game.Point2D)}.
     * The loop angle affects the orientation of self-loops. It has no effect on other lines.
     * @param height The new loop angle in radians
     */
    public void setLoopAngle(double height) {
      lineTemplate.circAngle = height;
    }
    
    /**
     * Returns {@code true} if the line is reversed (arrow points end-to-start), {@code false} if not.
     * @return {@code true} if the line is reversed (arrow points end-to-start), {@code false} if not.
     */
    public boolean isReversed() {
      return reverse;
    }
    
    /**
     * Creates a deep copy of this {@link LineStyle}.
     * @return A deep copy of this {@link LineStyle}. 
     */
    @Override
    public LineStyle clone() {
      return new LineStyle(lineTemplate.getCopy(), reverse);
    }
  
    /**
     * Creates a deep copy of this {@link LineStyle} that has the opposite value for
     * the {@link #isReversed()} property. If this is called on the line style for a particle,
     * the result will have the opposite arrow direction and thus represent the corresponding antiparticle.
     * @return A deep copy of this {@link LineStyle} that has the opposite value for
     * the {@link #isReversed()} property.
     */
    public LineStyle cloneDual() {
      return new LineStyle(lineTemplate.getCopy(), !reverse);
    }
  }
  
  /**
   * The {@link VertexStyle} class contains all information about the appearence of a {@link Vertex} except
   * the vertex position.
   * 
   * @author Lars Bündgen
   */
  public static final class VertexStyle implements Cloneable2<VertexStyle> {
    private final Vertex vertexTemplate;
    
    /**
     * Creates a new {@link VertexStyle} using a vertex as a template.
     * @param v The template vertex.
     */
    public VertexStyle(Vertex v) {
      this.vertexTemplate = v;
    }
    
    /**
     * Returns a {@link Vertex} that has the appearance of this vertex style and is located at the given position.
     * The returned instance is linked to this vertex style.<br>
     * This version accepts FeynGame's {@link game.Point2D} class
     * @param position The position where the vertex should appear.
     * @return The {@link Vertex} with this style at the selected position.
     */
    public Vertex getVertex(game.Point2D position) {
      Vertex v = new Vertex(vertexTemplate);
      v.origin = position;
      return v;
    }
    
    /**
     * Returns a {@link Vertex} that has the appearance of this vertex style and is located at the given position.
     * The returned instance is linked to this vertex style.<br>
     * This version accepts jgrapht's {@link org.jgrapht.alg.drawing.model.Point2D} class
     * @param position The position where the vertex should appear.
     * @return The {@link Vertex} with this style at the selected position.
     */
    public Vertex getVertex(org.jgrapht.alg.drawing.model.Point2D position) {
      Vertex v = new Vertex(vertexTemplate);
      v.origin = new game.Point2D(position.getX(), position.getY());
      return v;
    }
    
    /**
     * Creates a deep copy of this {@link VertexStyle} instance.
     * @return A deep copy of this {@link VertexStyle} instance.
     */
    @Override
    public VertexStyle clone() {
      return new VertexStyle(new Vertex(vertexTemplate));
    }
  }
  
  private final Map<String, LineStyle> lineStyles;
  private final List<FieldAndDual> unusedUnknownFields;
  private final Set<LineConfig> generated;
  private final Presets preset;
  private final Set<LineConfig> usedConfigs;
  private final Vertex defaultVertex;
  
  private boolean alreadyAskedUnknownLines = false;
  
  //Simple container class for identifier and dual identifier
  private static class FieldAndDual {
    String field;
    String dual;
    
    FieldAndDual(String field, String dual) {
      this.field = field;
      this.dual = dual;
    }
  }
  
  //The colors that will be used for generated lines
  private static final List<Color> COLORS = 
      Java8Support.listOf(Color.BLACK, Color.RED, Color.BLUE, Color.GREEN, Color.ORANGE, Color.CYAN, Color.DARK_GRAY);
  private static final AtomicInteger COLOR_INDEX = new AtomicInteger();
  
  /**
   * Creates a new {@link ModelData} instance from a {@link Presets} object. The line configs in the {@link Presets#presets}
   * field will be added as available line styles.
   * @param preset The {@link Presets} object that contains the line styles
   */
  public ModelData(Presets preset) {
    this.lineStyles = new HashMap<>();
    this.unusedUnknownFields = new ArrayList<>();
    this.generated = new HashSet<>();
    this.preset = preset;
    this.usedConfigs = new HashSet<>();
    
    if(preset != null) {
      for(LineConfig lc : preset.presets) {
        if(lc.identifier == null) continue;
        String field = lc.identifier.getKey();
        String dual = lc.identifier.getValue();
        
        lineStyles.put(field, new LineStyle(lc, false));
        if(!field.equals(dual)) {
          lineStyles.put(dual, new LineStyle(lc, true));
        }
      }
      
      if(preset.vertexPresets.isEmpty()) {
        this.defaultVertex = new Vertex();
      } else {
        this.defaultVertex = preset.vertexPresets.get(0).getKey();
      }
    } else {
      this.defaultVertex = null;
    }
  }
  
  /**
   * Creates an empty {@link ModelData} instance that contains no line styles.
   */
  public ModelData() {
    this(null);
  }
  
  /**
   * Returns {@code true} if this model contains any line styles that were generated for a line with an unknown identifier.
   * @return {@code true} if this model contains any line styles that were generated for a line with an unknown identifier.
   */
  public boolean hasGeneratedLines() {
    return !generated.isEmpty();
  }
  
  /**
   * Returns all line configs that were generated for lines with unknown identifier.
   * @return all line configs that were generated for lines with unknown identifier.
   */
  public Iterable<LineConfig> generatedLines() {
    return generated;
  }
  
  /**
   * Utility method to assure that the user is asked about handling generated lines only once.
   * Will return {@code true} on the first invocation and {@code false} on all subsequent invocations. 
   * @return {@code true} on the first invocation and {@code false} on all subsequent invocations.
   */
  public boolean askUnknownLines() {
    boolean result = !alreadyAskedUnknownLines;
    alreadyAskedUnknownLines = true;
    return result;
  }
  
  //Helper method to create a Presets instance that behaves the way you would expect an empty instance to behave
  private Presets emptyPreset() {
    Presets p = new Presets(); 
    //The default ctor creates a preset that is NOT empty!
    p.presets.clear();
    p.vertexPresets.clear();
    p.fIPresets.clear();
    p.feynmanRuleTemplates.clear();
    p.rules.clear();
    p.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
    
    return p;
  }
  
  /**
   * Returns the {@link Presets} object that was used in the constructor of this model. May be null.
   * @return The {@link Presets} object that was used in the constructor of this model. May be null.
   */
  public Presets getBasePreset() {
    return preset;
  }
  
  /**
   * Creates a preset that contains all of the line styles that this model was constructed with and all line styles
   * that were generated for lines with unknown identifiers.<br>
   * This is equivalent to merging the {@link #getBasePreset()} and the {@link #getGeneratedPreset()} presets.
   * @return A {@link Presets} object containing all known and all generated lines.
   */
  public Presets getMergedPreset() {
    Presets p = emptyPreset();
    p.merge(getBasePreset());
    p.merge(getGeneratedPreset());
    return p;
  }
  
  public Presets getUsedPreset() {
    Presets p = emptyPreset();
    for(LineConfig c : usedConfigs) {
      p.presets.add(c);
    }
    p.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
    Presets rp = emptyPreset();
    rp.merge(p);
    return rp;
  }
  
  /**
   * Returns a {@link Presets} object that contains all line styles that were generated for lines with unknown identifiers.
   * @return a {@link Presets} object that contains all line styles that were generated for lines with unknown identifiers.
   */
  public Presets getGeneratedPreset() {
    Presets p = emptyPreset();
    for(LineConfig c : generated) {
      p.presets.add(c);
    }
    p.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
    return p;
  }
  
  /**
   * Creates a {@link VertexStyle} with the default appearance of a vertex.
   * @return a {@link VertexStyle} with the default appearance of a vertex.
   */
  public VertexStyle defaultVertexStyle() {
    return new VertexStyle(new Vertex(defaultVertex));
  }
  
  /**
   * Retrieves or generates a line style for a propagator with some identifier and dual identifier.
   * Both identifier and/or dual identifier may be {@code null} if they are not known. All known particle/antiparticle
   * pairs should be registered using {@link #registerDualPair(String, String)} before this method is called for the first time.
   * @param identifier The identifier of the particle
   * @param dualIdentifier The dual identifier of the particle; this is the identifier of the corresponding antiparticle. 
   * @return The {@link LineStyle} that this model contains or generated for the particle.
   */
  public LineStyle getLineStyle(String identifier, String dualIdentifier, String momentum, String dualMomentum) {
    LineStyle ls = identifier == null ? getDualStyle(dualIdentifier) : getLineStyle(identifier);
    //First, import the momentum, it can be deleted later if it is not desired
    if(momentum != null) {
      ls.lineTemplate.showMom();
      ls.lineTemplate.momArrow.description = momentum;
      ls.lineTemplate.momArrow.showDesc = true;
    } else if(dualMomentum != null) {
      ls.lineTemplate.showMom();
      ls.lineTemplate.momArrow.description = momentum;
      ls.lineTemplate.momArrow.showDesc = true;
      ls.lineTemplate.momArrow.invert = true;
    }
    return ls;
  }
  
  private LineStyle getDualStyle(String identifier) {
    LineStyle ls = getLineStyle(identifier);
    return ls.cloneDual();
  }
  
  private LineStyle getLineStyle(String identifier) {
    if(identifier == null) { //Generate style and name
      String id = "unknown";
      Color color = Color.BLACK;
      LineConfig cfg = new LineConfig(LineType.FERMION, color);
      cfg.identifier = new AbstractMap.SimpleEntry<>(id, id);
      cfg.description = "";
      cfg.dashed = dashed;
      cfg.showArrow = false;
      LineStyle ls = new LineStyle(cfg, false);
//      generated.add(cfg); //Do not remember the unknown lines for model creation
      return ls.clone();
    }
    
    LineStyle ls = lineStyles.get(identifier);
    if(ls != null) {
      usedConfigs.add(ls.lineTemplate.lineConfig);
      return ls.clone();
    } else { //generate
      Optional<FieldAndDual> fad = unusedUnknownFields.stream()
          .filter(f -> identifier.equals(f.field) || identifier.equals(f.dual))
          .findAny();
      if(fad.isPresent()) {
        FieldAndDual f = fad.get();
        String dualIdentifier = identifier.equals(f.field) ? f.dual : f.field;
        if(lineStyles.containsKey(dualIdentifier)) {
          LineStyle dualStyle = lineStyles.get(dualIdentifier);
          ls = dualStyle.cloneDual();
          lineStyles.put(identifier, ls);
          usedConfigs.add(ls.lineTemplate.lineConfig);
          return ls.clone();
        } else {
          //Create line, the cases are different in case they need to be changed later
          if(f.dual.equals(f.field)) { //boson
            Color color = COLORS.get(COLOR_INDEX.getAndIncrement() % COLORS.size());
            LineConfig cfg = new LineConfig(LineType.FERMION, color);
            cfg.identifier = new AbstractMap.SimpleEntry<>(identifier, dualIdentifier);
            cfg.description = createTexDescription(identifier);
            cfg.wiggleHeight = 14;
            cfg.wiggleLength = 28;
            cfg.wiggleOffset = 5;
            cfg.dashLength = 5;
            cfg.dashed = dashed;
            cfg.showArrow = false;
            ls = new LineStyle(cfg, false);
            LineStyle ds = new LineStyle(cfg, true);
            generated.add(cfg);
            lineStyles.put(identifier, ls);
            lineStyles.put(dualIdentifier, ds);
            usedConfigs.add(ls.lineTemplate.lineConfig);
            return ls.clone();
          } else { //fermion
            Color color = COLORS.get(COLOR_INDEX.getAndIncrement() % COLORS.size());
            LineConfig cfg = new LineConfig(LineType.FERMION, color);
            cfg.identifier = new AbstractMap.SimpleEntry<>(identifier, dualIdentifier);
            cfg.description = createTexDescription(identifier);
            cfg.wiggleHeight = 0;
            cfg.wiggleLength = 0;
            cfg.wiggleOffset = 0;
            cfg.dashLength = 5;
            cfg.dashed = dashed;
            cfg.showArrow = true;
            ls = new LineStyle(cfg, false);
            LineStyle ds = new LineStyle(cfg, true);
            generated.add(cfg);
            lineStyles.put(identifier, ls);
            lineStyles.put(dualIdentifier, ds);
            usedConfigs.add(ls.lineTemplate.lineConfig);
            return ls.clone();
          }
        }
      } else { //Just generate a single style
        Color color = COLORS.get(COLOR_INDEX.getAndIncrement() % COLORS.size());
        LineConfig cfg = new LineConfig(LineType.FERMION, color);
        cfg.identifier = new AbstractMap.SimpleEntry<>(identifier, identifier);
        cfg.description = createTexDescription(identifier);
        cfg.wiggleHeight = 14;
        cfg.wiggleLength = 28;
        cfg.wiggleOffset = 5;
        cfg.dashLength = 5;
        cfg.dashed = dashed;
        cfg.showArrow = false;
        ls = new LineStyle(cfg, false);
        generated.add(cfg);
        lineStyles.put(identifier, ls);
        usedConfigs.add(ls.lineTemplate.lineConfig);
        return ls.clone();
      }
    }
  }
  
  private static String createTexDescription(String identifier) {
    int i = identifier.indexOf('_');
    if(i == -1) {
      return identifier;
    } else {
      String first = identifier.substring(0, i);
      String last = identifier.substring(i + 1);
      return first + "_{" + last + "}";
    }
  }
  
  /**
   * Notify the model that two identifiers are a pair of particle and corresponding antiparticle.
   * This is important for the proper creation of line styles for unknown lines.<br>
   * One or both parameters may be null. In this case nothing happens.
   * @param identifier The identifier of the particle.
   * @param dualIdentifier The identifier of the corresponding antiparticle.
   */
  public void registerDualPair(String identifier, String dualIdentifier) {
    /*if(identifier == null && dualIdentifier == null) {
//      throw new NullPointerException();
      //Just ignore, in case of a style file without field names
    } else*/ if(identifier != null && dualIdentifier != null) {
      unusedUnknownFields.add(new FieldAndDual(identifier, dualIdentifier));
    }
  }
  
}
