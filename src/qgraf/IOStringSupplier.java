package qgraf;

import java.io.BufferedReader;
import java.io.IOException;

import resources.Java8Support;

/**
 * A method that supplies {@link String}s and can throw an {@link IOException}.
 * Intended to be used with {@link BufferedReader#readLine()}. It is expected that the supplier will provide a finite number
 * of lines before returning {@code null}. It is expected that once {@code null} is returned from {@link #nextLine()},
 * all further calls will also return {@code null}.
 * 
 * @author Lars Bündgen
 */
@FunctionalInterface interface IOStringSupplier {
  public String nextLine() throws IOException;
  
  /**
   * Creates a new supplier that starts at the same porition as the current one, but is independent of this one.
   * Not supported by all implementations. Not supported when the instance was created from a lamda expression.
   * @return A copy of this {@link IOStringSupplier}
   */
  public default IOStringSupplier fork() {
    throw new UnsupportedOperationException();
  }
  
  /**
   * Some relative measure of the amount of lines the supplier has read compared to the total number.
   * Should only be used to compare progress with other {@link IOStringSupplier}s that implement this method in the same way.
   * Not supported by all implementations. Not supported when the instance was created from a lamda expression.
   * @return The progress of the supplier
   */
  public default int progress() {
    throw new UnsupportedOperationException();
  }
  
  /**
   * An {@link IOStringSupplier} that skips over all whitespace lines at the beginning of the file.
   * @param io The delegate, the position of the delegate is considered the beginning of the file. 
   * @return A supplier that skips leading whitespace
   */
  public static IOStringSupplier skipInitialWhitespace(IOStringSupplier io) {
    return new IOStringSupplier() {
      boolean trigd = false;
      
      @Override
      public String nextLine() throws IOException {
        if(trigd) {
          return io.nextLine();
        } else {
          String nl;
          while((nl = io.nextLine()) != null) {
            if(!Java8Support.isBlank(nl)) {
              trigd = true;
              return Java8Support.stripLeading(nl);
            }
          }
          return null;
        }
      }

      @Override
      public IOStringSupplier fork() {
        return io.fork();
      }

      @Override
      public int progress() {
        return io.progress();
      }
    };
  }
  
  /**
   * A utility method that checks whether all lines remaining in an {@link IOStringSupplier} are blank.
   * The line pointer of the supplier is advanced to the end of the file.
   * @param io The {@link IOStringSupplier} to check.
   * @return {@code true} if all lines are blank, {@code false} otherwise
   * @throws IOException If a line cannot be read from the supplier
   * @see String#isBlank()
   * @see Character#isWhitespace(int) 
   */
  public static boolean isRemainingFileBlank(IOStringSupplier io) throws IOException {
    String nl;
    while((nl = io.nextLine()) != null) {
      if(!Java8Support.isBlank(nl)) {
        return false;
      }
    }
    return true;
  }
  
}