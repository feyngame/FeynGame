package qgraf;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import resources.Java8Support;

/**
 * This class represents a parsed qgraf output file. Depending on the parsing mode
 * it may contain one or all of the diagrams in the output file. 
 * 
 * @author Lars Bündgen
 */
public class QgrafOutputFile implements Iterable<QgrafDiagram> {

  /*
   * If positve, only read the diagram with that index and discard all others.
   * The diagram index is a qgraf index, which starts at 1.
   */
  private final int loadOnlyBase0;
  private final List<QgrafDiagram> diagrams;
  private final String name;
  private final ModelData sharedModel;
  
  QgrafOutputFile(String name, List<QgrafDiagram> trustedList, ModelData model) {
    this.diagrams = Objects.requireNonNull(trustedList);
    this.loadOnlyBase0 = QgrafOutputFile.LOAD_ALL_INDEX_VALUE;
    this.name = name;
    this.sharedModel = model;
  }
  
  QgrafOutputFile(String name, QgrafDiagram diagram, int loadOnlyBase0, ModelData model) {
    this.diagrams = Java8Support.listOf(diagram);
    this.loadOnlyBase0 = loadOnlyBase0;
    this.name = name;
    this.sharedModel = model;
  }
  
  /**
   * Returns {@code true} if this object only contains data of a single diagram, {@code false} if it contains data for all diagrams
   * in the output file.
   * @return {@code true} if this object only contains data of a single diagram, {@code false} otherwise.
   */
  public boolean isPartial() {
    return loadOnlyBase0 >= 0;
  }
  
  /**
   * Returns the {@link ModelData} instance that was used by all {@link QgrafDiagram} instances contained in this output file.
   * @return The shared model for all contained diagrams.
   */
  public ModelData getModel() {
    return sharedModel;
  }
  
  /**
   * Get one diagram with a specific index from the output file. 
   * @param index The zero-based index of the diagram.
   * @return The diagram with the selected index.
   * @throws IndexOutOfBoundsException If the index is smaller than zero or larger than or equal to the number
   * of diagrams; or when only a single diagram is loaded and it has not the selected index.
   * @see #isPartial()
   */
  public QgrafDiagram getDiagram(int index) {
    if(loadOnlyBase0 < 0) {
      return diagrams.get(index);
    } else { //ignore index if we only have one
      return diagrams.get(0);
    } 
  }
  
  /**
   * Returns the number of diagrams that are in the output file.
   * @return The number of diagrams that are in the output file.
   */
  public int getDiagramCount() {
    return diagrams.size();
  }
  
  @Override
  public Iterator<QgrafDiagram> iterator() {
    return diagrams.iterator();
  }
  
  /**
   * Returns the name of the output file.
   * @return The name of the output file.
   */
  public String getName() {
    return name;
  }
  
  /**
   * Parses a string into a {@link QgrafOutputFile} instance.
   * @param name The name that will be reported by {@link #getName()}.
   * @param file The entire content of a valid qgraf output file contained in a string.
   * @param style The style file that qgraf used to generate the output file.
   * @param model The model that contains all known line styles.
   * @return The parsed output file.
   * @throws QgrafImportException If the output file cannot successfully be parsed into an object for any reason.
   * @throws NullPointerException If any parameter is {@code null}.
   */
  public static QgrafOutputFile fromString(String name, String file, QgrafStyleFile style, ModelData model) 
      throws QgrafImportException {
    Objects.requireNonNull(name);
    Objects.requireNonNull(file);
    Objects.requireNonNull(style);
    Objects.requireNonNull(model);
    
    QgrafOutputBuilder diagrams;
    try {
      diagrams = QgrafOutputReader.parseOutputFromString(file, style, ProgressUpdate.VOID, false, false);
    } catch (InterruptedException e) {
      //should not happen since allowInterruptions if false
      throw new RuntimeException(e);
    }
    if(diagrams == null) throw new QgrafImportException("Incorrect style file");
    QgrafOutputFile outFile = QgrafOutputReader.toOutputFile(name, diagrams, model);
    return outFile;
  }
  
  /**
   * Parses a file into a {@link QgrafOutputFile} instance.
   * @param file The path to the output file.
   * @param style The style file that qgraf used to generate the output file.
   * @param loadOnlyBase0 If this is a nonnegative integer, only one diagram with that index will be parsed from the
   * file. Otherwise, pass {@link #LOAD_ALL_INDEX_VALUE} (or any negative number) to load all diagrams from the file.
   * @param model The model that contains all known line styles.
   * @return The parsed output file.
   * @throws IOException If there is a problem with accessing the file contents.
   * @throws QgrafImportException If the output file cannot successfully be parsed into an object for any reason.
   * @throws NullPointerException If any parameter is {@code null}.
   */
  public static QgrafOutputFile fromFile(Path file, QgrafStyleFile style, int loadOnlyBase0, ModelData model) 
      throws IOException, QgrafImportException {
    Objects.requireNonNull(file);
    Objects.requireNonNull(style);
    Objects.requireNonNull(model);
    
    QgrafOutputBuilder diagrams;
    try {
      diagrams = QgrafOutputReader.parseOutputFromFile(file, style, loadOnlyBase0, ProgressUpdate.VOID, false, false);
    } catch (InterruptedException e) {
      //should not happen since allowInterruptions if false
      throw new RuntimeException(e);
    }
    QgrafOutputFile outFile = QgrafOutputReader.toOutputFile(file.getFileName().toString(), diagrams, model);
    return outFile;
  }
  
  
  /**
   * Should be passed as the index of the diagram to load (often named {@code loadOnlyBase0}
   * if all diagrams should be loaded from the file.
   */
  public static final int LOAD_ALL_INDEX_VALUE = -1;
  
  /**
   * Parses a file into a {@link QgrafOutputFile} instance. This method should be used instead of
   * {@link #fromFile(File, QgrafStyleFile, int, ModelData)} if parsing is done on a separate thread.
   * @param file The path to the output file.
   * @param style The style file that qgraf used to generate the output file.
   * @param loadOnlyBase0 If this is a nonnegative integer, only one diagram with that index will be parsed from the
   * file. Otherwise, pass {@link #LOAD_ALL_INDEX_VALUE} (or any negative number) to load all diagrams from the file.
   * @param update The callback object that will be notified of parsing progress.
   * @param model The model that contains all known line styles.
   * @return The parsed output file.
   * @throws IOException If there is a problem with accessing the file contents.
   * @throws QgrafImportException If the output file cannot successfully be parsed into an object for any reason.
   * @throws InterruptedException If the current thread is interrupted while parsing is ongoing.
   * @throws NullPointerException If any parameter is {@code null}.
   */
  public static QgrafOutputFile fromFileParallel(Path file, QgrafStyleFile style, int loadOnlyBase0, ProgressUpdate update, ModelData model) 
      throws IOException, QgrafImportException, InterruptedException {
    Objects.requireNonNull(file);
    Objects.requireNonNull(style);
    Objects.requireNonNull(model);
    Objects.requireNonNull(update);
    
    QgrafOutputBuilder diagrams = QgrafOutputReader.parseOutputFromFile(file, style, loadOnlyBase0, update, true, false);
    if(diagrams == null) throw new QgrafImportException("Incorrect style file");
    QgrafOutputFile outFile = QgrafOutputReader.toOutputFile(file.getFileName().toString(), diagrams, model);
    return outFile;
  }

  /**
   * Parses an excerpt of an output file that represents a single Feynman diagram into a {@link QgrafOutputFile} instance. 
   * @param name The name that will be reported by {@link #getName()}.
   * @param file An excerpt from an output file that represents exactly one diagram (leading and trailing whitespace is ignored).
   * @param style The style file that qgraf used to generate the output file.
   * @param model The model that contains all known line styles.
   * @return The parsed output file. It will contain a single diagram at index 0.
   * @throws QgrafImportException If the output file cannot successfully be parsed into an object for any reason.
   * @throws NullPointerException If any parameter is {@code null}.
   */
  public static QgrafOutputFile singleFromString(String name, String file, QgrafStyleFile style, ModelData model)
      throws QgrafImportException {
    Objects.requireNonNull(name);
    Objects.requireNonNull(file);
    Objects.requireNonNull(style);
    Objects.requireNonNull(model);
    
    QgrafOutputBuilder diagrams;
    BufferedReader br = new BufferedReader(new StringReader(file));
    StyleNodeIterator sni = style.diagramIterator(br::readLine, QgrafOutputFile.LOAD_ALL_INDEX_VALUE);
    try {
      diagrams = QgrafOutputReader.parseOutput(sni, () -> -1, ProgressUpdate.VOID, false, true);
    } catch (InterruptedException | IOException e) {
      //should not happen since allowInterruptions if false
      throw new RuntimeException(e);
    }
    if(diagrams == null) throw new QgrafImportException("Incorrect style file");
    return QgrafOutputReader.toOutputFile(name, diagrams, model);
  }

  /**
   * Checks whether a diagram index is valid for this output file. Shows an error message dialog if the
   * index is not valid.
   * @param index The 0-based index to check.
   * @param parent The {@link JFrame} that is the parent of the error message dialog.
   * @return {@code true} if the index is valid, {@code false} otherwise.
   */
  public boolean checkIndex(int index, JFrame parent) {
    if(loadOnlyBase0 == LOAD_ALL_INDEX_VALUE) {
      return true; //Disable this check, always show the first one
    } else {
      if(index != loadOnlyBase0) {
        //This isn't even something the user is supposed to see, so it might as well be a confusing message
        JOptionPane.showMessageDialog(parent, "Invalid index: index was not selected for parsing");
        return false;
      }
    }
    return true;
  }
  
  
  public int getOnlyLoadedIndex() {
    if(!isPartial()) throw new IllegalStateException("Not a partial output file");
    return loadOnlyBase0;
  }
  
  public List<QgrafDiagram> diagrams() {
    return Collections.unmodifiableList(diagrams);
  }
  
}
