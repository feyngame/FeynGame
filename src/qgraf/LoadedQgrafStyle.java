package qgraf;

import java.nio.file.Path;
import java.util.Objects;

/**
 * Represents a style file loaded by FeynGame. This class combines a {@link QgrafStyleFile} object,
 * which represents the content of the style file, and some additional information about how the style was loaded.
 * 
 * @author Lars Bündgen
 */
public class LoadedQgrafStyle {

  private final QgrafStyleFile style;
  private final Path location;
  private final boolean isJarStyle;
  private final boolean isDefaultStyle;
 
  /**
   * Create a new instance.
   * @param style The {@link QgrafStyleFile} object that represents the content of the style file.
   * @param file The file path that the style file was loaded from. May be {@code null} for styles 
   * that are not loaded from the file system (e.g. builtin styles from the JAR file).
   * @param isBuiltin {@code true} iff the style file is one of the builtin styles that are loaded from the JAR file.
   * @param isDefault {@code true} iff the style file is one of the default styles that are always loaded
   * when FeynGame is started.
   */
  public LoadedQgrafStyle(QgrafStyleFile style, Path file, boolean isBuiltin, boolean isDefault) {
    this.style = Objects.requireNonNull(style);
    this.location = file;
    this.isJarStyle = isBuiltin;
    this.isDefaultStyle = isDefault;
  }
  
  @Override
  public String toString() {
    return style.getName();
  }
  
  /**
   * Returns the name of the style file.
   * @return The name of the style file.
   */
  public String getName() {
    return style.getName();
  }
  
  /**
   * Returns the style object that represents the content of the style file.
   * @return The style object that represents the content of the style file.
   */
  public QgrafStyleFile getStyle() {
    return style;
  }
  
  /**
   * Returns the file that this style was loaded from. May be {@code null}.
   * @return The file that this style was loaded from. May be {@code null}.
   */
  public Path getPath() {
    return location;
  }
  
  /**
   * Returns {@code true} if this file is a default style, {@code false} otherwise. 
   * @return {@code true} if this file is a default style, {@code false} otherwise.
   */
  public boolean isDefaultStyle() {
    return isDefaultStyle;
  }
  
  /**
   * Returns {@code true} if this file is a builtin style, {@code false otherwise}.
   * @return {@code true} if this file is a builtin style, {@code false otherwise}.
   */
  public boolean isBuiltinStyle() {
    return isJarStyle;
  }

  @Override
  public int hashCode() {
    return Objects.hash(isDefaultStyle, isJarStyle, location, style);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!(obj instanceof LoadedQgrafStyle))
      return false;
    LoadedQgrafStyle other = (LoadedQgrafStyle) obj;
    return isDefaultStyle == other.isDefaultStyle && isJarStyle == other.isJarStyle
        && Objects.equals(location, other.location) && Objects.equals(style.getName(), other.style.getName());
  }
  
}
