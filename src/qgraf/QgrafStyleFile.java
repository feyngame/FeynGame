package qgraf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.jgrapht.Graph;
import org.jgrapht.graph.AsUnmodifiableGraph;

import resources.Java8Support;

/**
 * This class represents a parsed qgraf style file.
 * 
 * @author Lars Bündgen
 */
public class QgrafStyleFile {

  private final Graph<QgrafStyleNode, QgrafStyleAction> graph;
  private final QgrafStyleNode start;
  private final String name;
  
  private final Graph<QgrafStyleNode, QgrafStyleAction> diagramSubGraph;
  private final QgrafStyleNode diagramStart;
  
  private QgrafStyleFile(Graph<QgrafStyleNode, QgrafStyleAction> graph, QgrafStyleNode start, String name) throws QgrafImportException {
    this.graph = graph.getType().isModifiable() ? new AsUnmodifiableGraph<>(graph) : graph;
    this.start = Objects.requireNonNull(start);
    this.name = Objects.requireNonNull(name);
    
    this.diagramStart = QgrafStyleNode.start();
    this.diagramSubGraph = QgrafStyleReader.extractDiagramLoop(graph, start, this.diagramStart);
  }
  
  /**
   * The graph representation of this style file. The returned graph is unmodifiable.
   * @return The graph representation of this style file.
   */
  public Graph<QgrafStyleNode, QgrafStyleAction> getAsGraph() {
    return graph;
  }

  /**
   * A displayable name for this style file.
   * @return The name of this style.
   */
  public String getName() {
    return name;
  }

  /**
   * Creates a new {@link StyleNodeIterator} that can be used to iterate over all possible paths
   * through the style.
   * @param file The {@link IOStringSupplier} that supplies lines of the file that should be parsed.
   * @param loadDiagramBase0 The base-0 index of the diagram to load.
   * @return A {@link StyleNodeIterator} for this style.
   */
  StyleNodeIterator iterator(IOStringSupplier file, int loadDiagramBase0) {
    return new StyleNodeIterator(graph, start, Objects.requireNonNull(file), loadDiagramBase0, false);
  }
  
  /**
   * Creates a new {@link StyleNodeIterator} that can be used to iterate over all possible paths
   * through the style.
   * <p>
   * This iterator will match only a single diagram, not an entire style file.
   * @param file The {@link IOStringSupplier} that supplies lines of the file that should be parsed.
   * @param loadDiagramBase0 The base-0 index of the diagram to load.
   * @return A {@link StyleNodeIterator} for this style that parses single diagrams.
   */
  StyleNodeIterator diagramIterator(IOStringSupplier file, int loadDiagramBase0) {
    return new StyleNodeIterator(diagramSubGraph, diagramStart, Objects.requireNonNull(file), loadDiagramBase0, true);
  }

  /**
   * Creates a {@link QgrafStyleFile} object from the contents of a file.
   * @param file The path to the style file
   * @return A {@link QgrafStyleFile} object representing that file
   * @throws QgrafImportException If the file is not a valid qgraf style
   * @throws IOException If the file could not be accessed
   */
  public static QgrafStyleFile fromFile(Path file) throws QgrafImportException, IOException {
    List<QgrafStyleReader.StyleItem> rawItems;
    try(BufferedReader br = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
      rawItems = QgrafStyleReader.readItems(br::readLine);
    }
    return fromItems(rawItems, file.getFileName().toString());
  }
  
  /**
   * Creates a {@link QgrafStyleFile} object from the contents of a file.
   * @param is The input stream to the style file
   * @param filename The name to use for this style
   * @return A {@link QgrafStyleFile} object representing that file
   * @throws QgrafImportException If the file is not a valid qgraf style
   * @throws IOException If the file could not be accessed
   */
  public static QgrafStyleFile fromStream(InputStream is, String filename) throws QgrafImportException, IOException {
    List<QgrafStyleReader.StyleItem> rawItems;
    try(BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
      rawItems = QgrafStyleReader.readItems(br::readLine);
    }
    return fromItems(rawItems, filename);
  }

  private static QgrafStyleFile fromItems(List<QgrafStyleReader.StyleItem> items, String name) throws QgrafImportException {
    Deque<QgrafStyleReader.StyleItem> fixedItems = QgrafStyleReader.collapseLiterals(items);
    QgrafStyleNode start = QgrafStyleNode.start();
    Graph<QgrafStyleNode, QgrafStyleAction> graph = QgrafStyleReader.createGraph(fixedItems, start);
    QgrafStyleReader.detectAmbigousLoops(graph, false);
    return new QgrafStyleFile(graph, start, name);
  }

  /**
   * Creates a {@link QgrafStyleFile} object from a string that is exactly equal to the content of a valid style file.
   * @param string A string that contains the entire style file content
   * @param name The name of this style
   * @return A {@link QgrafStyleFile} object representing that file
   * @throws QgrafImportException If the file is not a valid qgraf style
   */
  public static QgrafStyleFile fromString(String string, String name) throws QgrafImportException {
    Objects.requireNonNull(string);
    Objects.requireNonNull(name);

    final Iterator<String> lines = Java8Support.lines(string).iterator();
    List<QgrafStyleReader.StyleItem> rawItems;

    try {
      rawItems = QgrafStyleReader.readItems(() -> lines.hasNext() ? lines.next() : null);
    } catch (IOException e) {
      throw new RuntimeException("Should not happen", e);
    }

    return fromItems(rawItems, name);
  }

}
