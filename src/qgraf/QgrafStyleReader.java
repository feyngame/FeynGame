package qgraf;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.DirectedPseudograph;
import org.jgrapht.graph.SimpleDirectedGraph;

import qgraf.QgrafStyleAction.ActionType;
import qgraf.QgrafStyleNode.LoopName;
import qgraf.QgrafStyleNode.NodeContentType;
import qgraf.QgrafStyleNode.StyleNodeContent;
import ui.Frame;

/**
 * Contains utility methods for parsing a file into a {@link QgrafStyleFile} object.
 * 
 * @author Lars Bündgen
 */
class QgrafStyleReader {
  private static final Logger LOGGER = Frame.getLogger(QgrafStyleReader.class);

  /**
   * The possible types of a {@link StyleItem}.
   * 
   * @author Lars Bündgen
   */
  static enum StyleItemType {
    /**
     * The content of the item is a literal value that appears in the output.
     */
    LITERAL,
    /**
     * The content of the item is the name of a keyword that is replaced by its value in the output.
     */
    KEYWORD,
    /**
     * The content of the item is the name of a function that is replaced by its value in the output.
     */
    FUNCTION;
  }
  
  /**
   * Any qgraf style file can be represented as a sequence of {@link StyleItem}s.
   * @see StyleItemType
   * @author Lars Bündgen
   */
  static class StyleItem {
    final StyleItemType type;
    final String content;
    
    StyleItem(StyleItemType type, String content) {
      this.type = type;
      this.content = content;
    }

    @Override
    public String toString() {
      return "StyleItem [type=" + type + ", content=" + content + "]";
    }
    
  }
  
  static enum ParserState {
    PRE(1), PROLOGUE(2), DIAGRAM(3), EPILOGUE(4), POST(5);

    private final int index;
    
    private ParserState(int index) {
      this.index = index;
    }
    
    ParserState advanceTo(ParserState state, int lineNumberForMessage) throws QgrafImportException {
      if(state.index == this.index + 1) {
        return state;
      } else {
        throw new QgrafImportException("Style files must contain <prologue>, <diagram>, <epilogue>,"
            + " <exit> in that order (line " + lineNumberForMessage + ")");
      }
    }
  }
  
  static Graph<QgrafStyleNode, QgrafStyleAction> extractDiagramLoop(Graph<QgrafStyleNode, QgrafStyleAction> graph,
      QgrafStyleNode graphStart, QgrafStyleNode diagramStart) throws QgrafImportException {
    Graph<QgrafStyleNode, QgrafStyleAction> split = 
        DirectedPseudograph.<QgrafStyleNode, QgrafStyleAction>createBuilder(QgrafStyleAction.class).addGraph(graph).build();
    
    QgrafStyleNode diagramNode = graph.vertexSet().stream()
      .filter(v -> v.getContent() == LoopName.DIAGRAM)
      .findAny()
      .orElseThrow(() -> new QgrafImportException("Graph contains no <diagram> node"));
    
    split.removeVertex(diagramNode); //split in three
    
    QgrafStyleAction repeatDiagram = graph.outgoingEdgesOf(diagramNode).stream()
      .filter(e -> e.getType() == ActionType.REPEAT_LOOP)
      .findAny()
      .orElseThrow(() -> new QgrafImportException("<diagram> node has no REPEAT edge"));
    QgrafStyleNode startOfDiagramContent = Graphs.getOppositeVertex(graph, repeatDiagram, diagramNode);
    
    if(startOfDiagramContent == diagramNode) throw new QgrafImportException("Diagram loop has no content");
    
    ConnectivityInspector<QgrafStyleNode, QgrafStyleAction> ce = new ConnectivityInspector<>(split);
    Set<QgrafStyleNode> content = ce.connectedSetOf(startOfDiagramContent);
    
    if(content == null || content.isEmpty()) throw new QgrafImportException("Diagram has no content");
    
    //Now, construct the subgraph with only those vertices
    Set<QgrafStyleNode> toRemove = new HashSet<>(split.vertexSet());
    toRemove.removeAll(content); //retain content
    for(QgrafStyleNode tr : toRemove) {
      split.removeVertex(tr);
    }
    
    //add the new start vertex
    split.addVertex(diagramStart);
    split.addEdge(diagramStart, startOfDiagramContent, new QgrafStyleAction(ActionType.DEFAULT));
    
    return split;
  }
  
  static void detectAmbigousLoops(Graph<QgrafStyleNode, QgrafStyleAction> styleGraph, boolean errorForVertexLoop) throws QgrafImportException {

    Set<QgrafStyleNode> loopNodes = styleGraph.vertexSet().stream().filter(n -> styleGraph.degreeOf(n) == 4)
        .collect(Collectors.toCollection(HashSet::new));
    for(QgrafStyleNode node : loopNodes) {
      if(node.getContentType() != NodeContentType.LOOP) throw new IllegalStateException("deg 4 no loop");
      Set<QgrafStyleAction> in = styleGraph.incomingEdgesOf(node);

      if(in.stream().anyMatch(a -> a.getType() == ActionType.REPEAT_LOOP)) {
        Set<QgrafStyleAction> out = styleGraph.outgoingEdgesOf(node);
        QgrafStyleAction exit_to_outer = out.stream().filter(a -> a.getType() == ActionType.EXIT_LOOP).findAny().orElseThrow(NoSuchElementException::new);
        QgrafStyleAction enter_from_outer = in.stream().filter(a -> a.getType() == ActionType.REPEAT_LOOP).findAny().orElseThrow(NoSuchElementException::new);

        QgrafStyleNode outer1 = styleGraph.getEdgeSource(enter_from_outer);
        QgrafStyleNode outer2 = styleGraph.getEdgeTarget(exit_to_outer);

        if(outer1 == outer2) {
          if(node.getContent() == LoopName.COMMAND_LINE_LOOP
              && outer1.getContent() == LoopName.COMMAND_LOOP) {
            //Dissolve into a command loop
            QgrafStyleAction enterInner = out.stream().filter(a -> a.getType() == ActionType.REPEAT_LOOP).findAny().orElseThrow(NoSuchElementException::new);
            QgrafStyleAction exitInner = in.stream().filter(a -> a.getType() == ActionType.DEFAULT).findAny().orElseThrow(NoSuchElementException::new);

            QgrafStyleNode firstInner = styleGraph.getEdgeTarget(enterInner);
            QgrafStyleNode lastInner = styleGraph.getEdgeSource(exitInner);

            styleGraph.removeVertex(node);
            styleGraph.addEdge(outer1, firstInner, new QgrafStyleAction(ActionType.REPEAT_LOOP));
            styleGraph.addEdge(lastInner, outer1, new QgrafStyleAction(ActionType.DEFAULT));

          } else if(node.getContent() == LoopName.RAY_LOOP
              && outer1.getContent() == LoopName.VERTEX_LOOP
              && !errorForVertexLoop){
            LOGGER.severe("Ambiguous nested loop: " + node.getContent() + " in " + outer1.getContent());
          } else {
            throw new QgrafImportException("Ambiguous nested loop: " + node.getContent() + " in " + outer1.getContent());
          }
        }
      }
    }
  }
  
  
  /**
   * Parses a qgraf style file into a list of {@link StyleItem}s.<p>
   * The {@code <diagram>} keyword will be treated as a loop keyword and a corresponding {@code <end>}
   * item is inserted before the epilogue part. {@code <prologue>}, {@code <epilogue>} and {@code <exit>}
   * will not show up as items.
   * @param lines A function will supply the next line of the input file on demand.
   * Designed to work directly with {@link BufferedReader#readLine()}.
   * @return A list of {@link StyleItem}s in the order they appear in the style file
   * @throws QgrafImportException If the file content is not a valid style file
   * @throws IOException If there is an error retrieving data from the supplier
   * @implNote If the supplier never throws an {@link IOException}, this method will also
   * never throw one.
   */
  static List<StyleItem> readItems(IOStringSupplier lines) throws QgrafImportException, IOException {
    int lineNumber = 0; //For error messages
    
    ParserState state = ParserState.PRE;
    List<StyleItem> itemList = new ArrayList<>();
    StringBuilder currentSequence = new StringBuilder();
    String line;
    while((line = lines.nextLine()) != null) {
      lineNumber++;
      //First, check the four required keywords that appear on their own line
      if(line.equals("<prologue>")) {
        state = state.advanceTo(ParserState.PROLOGUE, lineNumber);
      } else if(line.equals("<diagram>")) {
        state = state.advanceTo(ParserState.DIAGRAM, lineNumber);
        itemList.add(new StyleItem(StyleItemType.KEYWORD, "diagram"));
      } else if(line.equals("<epilogue>")) {
        itemList.add(new StyleItem(StyleItemType.KEYWORD, "end"));
        state = state.advanceTo(ParserState.EPILOGUE, lineNumber);
      } else if(line.equals("<exit>")) {
        state = state.advanceTo(ParserState.POST, lineNumber);
      } else if(state == ParserState.PRE || state == ParserState.POST) {
        continue; //Skip all lines before <prologue>/after <exit>
      } else { //Otherwise, process lines normally
        char storedChar = 0; //Can ONLY be 0, '<', '[', ']', '>'. Stores last char to detect <<, >>, [[, ]] escape sequences
        StyleItemType mode = StyleItemType.LITERAL;
        currentSequence.setLength(0);
        
        //Step through all characters on this line //TODO can this be done with regex?
        for(int i = 0; i < line.length(); i++) {
          char c = line.charAt(i);
          
          if(mode == StyleItemType.LITERAL) {
            if(c == '<' || c == '[' || c == '>' || c == ']') {
              if(c == storedChar) { //repeated character is added as literal
                currentSequence.append(c);
                storedChar = 0;
              } else if(storedChar == 0) { //store and wait for next char
                storedChar = c;
              } else { //something else was stored. Remember: >,] that close a valid KW/func are never stored!
                //This will also be triggered for empty keyword/func (<> and [])
                throw new QgrafImportException("Unexpected '" + c + "' after '" + storedChar + "' (line " + lineNumber + ")");
              }
            } else { 
              if(storedChar == 0) { 
                currentSequence.append(c);
              } else if(storedChar == '<'){ //start of a keyword
                String literal = currentSequence.toString();
                currentSequence.setLength(0);
                if(!literal.isEmpty()) itemList.add(new StyleItem(StyleItemType.LITERAL, literal));
                mode = StyleItemType.KEYWORD;
                currentSequence.append(c); //c is already the first char of the keyword
                storedChar = 0;
              } else if(storedChar == '[') {
                String literal = currentSequence.toString();
                currentSequence.setLength(0);
                if(!literal.isEmpty()) itemList.add(new StyleItem(StyleItemType.LITERAL, literal));
                mode = StyleItemType.FUNCTION;
                currentSequence.append(c); //c is already the first char of the function
                storedChar = 0;
              } else { //stored is '>' or ']'
                throw new QgrafImportException("Unexpected single '" + storedChar + "' (line " + lineNumber + ")");
              }
            }
          /* the following code consumes single closing brackets as soon as they are available,
           * this means that an input like "literal <keyword>> literal" will produce the error:
           * "unexpected single >" (for the last >) instead of: "cannot have >> in keyword" plus
           * "unclosed keyword" as one might expect. Also, <keyword>>> will be accepted as vaild,
           * idk how qgraf treats that.
           * 
           * Other options: find+replace all occurences of <<, >>, [[, ]] with some special char,
           * parse, re-replace
           */
          } else if(mode == StyleItemType.KEYWORD) {
            if(c == '>') { //Keyword ends
              String kwName = currentSequence.toString();
              currentSequence.setLength(0);
              itemList.add(new StyleItem(StyleItemType.KEYWORD, kwName));
              //Command_data contains an implicit linebreak at the end
              if(kwName.equals("command_data")) itemList.add(new StyleItem(StyleItemType.LITERAL, QgrafStyleNode.LINEBREAK));
              mode = StyleItemType.LITERAL;
              storedChar = 0; //Unnecessary?
            } else if(c == '[' || c == ']' || c == '<') {
              throw new QgrafImportException("Character '" + c + "' can not appear inside keyword name (line " + lineNumber + ")");
            } else { //variable name continues
              currentSequence.append(c);
            }
          } else if(mode == StyleItemType.FUNCTION) {
            if(c == ']') { //Variable ends
              String funcName = currentSequence.toString();
              currentSequence.setLength(0);
              itemList.add(new StyleItem(StyleItemType.FUNCTION, funcName));
              mode = StyleItemType.LITERAL;
              storedChar = 0; //Unnecessary?
            } else if(c == '[' || c == '<' || c == '>') {
              throw new QgrafImportException("Character '" + c + "' can not appear inside function name (line " + lineNumber + ")");
            } else { //variable name continues
              currentSequence.append(c);
            }
          }
        }
        
        //This is the end of one line. If the last kw/func was closed, we are in literal mode
        if(mode != StyleItemType.LITERAL) {
          throw new QgrafImportException("Unclosed keyword/function (line " + lineNumber + ")");
        } else if(storedChar != 0){
          throw new QgrafImportException("Unexpected single '" + storedChar + "' at end of line (line " + lineNumber + ")");
        } else {
          currentSequence.append(QgrafStyleNode.LINEBREAK); //Append the linebreak
          String literal = currentSequence.toString();
          if(!literal.isEmpty()) itemList.add(new StyleItem(StyleItemType.LITERAL, literal));
          currentSequence.setLength(0);
        }
        
      } //end required KW if
    } //end line loop
    
    if(state != ParserState.POST) {
      throw new QgrafImportException("Style files must contain <prologue>, <diagram>, <epilogue>, <exit> in that order");
    }
    
    return itemList;
  }
  
  /**
   * Processes a list of {@link StyleItem}s, possibly reducing the number of items in the list without altering the
   * meaning of the items. There are two main steps that are applied here:
   * <ul><li>
   *   Consecutive literals are concatenated into a single literal
   * </li><li>
   *   back statements that immediately follow a literal are applied by removing the statement and the last character of the literal
   * </li></ul>
   * @param sourceList Any list of {@link StyleItem}s
   * @return An equivalent list of {@link StyleItem}s
   * @throws QgrafImportException If the list of items does not represent a valid style file
   */
  static Deque<StyleItem> collapseLiterals(List<StyleItem> sourceList) throws QgrafImportException {
    Deque<StyleItem> ll = new ArrayDeque<>();
    for(StyleItem item : sourceList) {
      if(item.type == StyleItemType.LITERAL && !ll.isEmpty()) {
        if(item.content.isEmpty()) continue; //ignore empty literals
        StyleItem top = ll.peekLast();
        if(top.type == StyleItemType.LITERAL) { //combine the literals
          ll.removeLast(); //remove top
          ll.offerLast(new StyleItem(StyleItemType.LITERAL, top.content + item.content));
        } else {
          ll.offerLast(item);
        }
      } else if(item.type == StyleItemType.KEYWORD && item.content.equals("back")) {
        if(ll.isEmpty()) {
          throw new QgrafImportException("<back> cannot be the first statement of a style file");
        } else {
          StyleItem top = ll.peekLast();
          if(top.type == StyleItemType.LITERAL && top.content.length() >= 1) {
            ll.removeLast();
            String newContent = top.content.substring(0, top.content.length() - 1); //Delete last char
            if(!newContent.isEmpty()) {
              ll.offerLast(new StyleItem(StyleItemType.LITERAL, newContent));
            }
            //Then don't add the back statement
          } else {
            //In this case, back follows a keyword. But this might be something with no content, like any loop or <end>, which is okay
            ll.offerLast(item);
          }
        }
      } else {
        ll.offerLast(item);
      }
    }
    return ll;
  }
  
  /**
   * Translates the list of style items into a graph structure.
   * @param items The items
   * @param start The starting node of the graph. Its content must be {@link StyleNodeContent#START}.
   * @return The generated graph 
   * @throws QgrafImportException If loops are unclosed or keywords unknown
   */
  static Graph<QgrafStyleNode, QgrafStyleAction> createGraph(Deque<StyleItem> items, QgrafStyleNode start) throws QgrafImportException {
    Objects.requireNonNull(items);
    Objects.requireNonNull(start);
    if(start.getContent() != QgrafStyleNode.StyleNodeContent.START) throw new IllegalArgumentException();
    
    Graph<QgrafStyleNode, QgrafStyleAction> graph = new SimpleDirectedGraph<>(QgrafStyleAction.class);
    graph.addVertex(start);
    createGraphImpl(graph, items, start);
    return graph;
  }
  
  /**
   * Recursive implementation method of {@link #createGraph(List, QgrafStyleNode)}.
   * @param graph The graph that is being edited
   * @param items The deque of remaining style items
   * @param startNode The node to start the recursive step at, this must be either the loop node
   * or the start node.
   * @return The last connected node, this is either the loop node or the node where the style ends.
   * @throws QgrafImportException If any loops are unclosed or keyword unknown
   */
  private static QgrafStyleNode createGraphImpl(Graph<QgrafStyleNode, QgrafStyleAction> graph,
      Deque<StyleItem> items, QgrafStyleNode startNode) throws QgrafImportException {
    final boolean loop = startNode.getContentType() == NodeContentType.LOOP;
    assert loop || startNode.getContent() == QgrafStyleNode.StyleNodeContent.START;
    
    StyleItem item;
    QgrafStyleNode lastNode = startNode;
    while((item = checkLoop(items.pollFirst(), !loop, loop ? startNode.getData() : null)) != null) {
      final ActionType type;
      if(lastNode == startNode && loop) { //first loop step is repeat
        type = ActionType.REPEAT_LOOP;
      } else if(lastNode.getContentType() == NodeContentType.LOOP) { //after any other loop is exit
        type = ActionType.EXIT_LOOP;
      } else {
        type = ActionType.DEFAULT;
      }
      
      final QgrafStyleNode newNode;
      if(item.type == StyleItemType.LITERAL) {
        newNode = QgrafStyleNode.ofLiteral(item.content);
      } else if(item.type == StyleItemType.KEYWORD) {
        newNode = QgrafStyleNode.ofKeyword(item.content);
        if(newNode == null) throw new QgrafImportException("Unknown keyword <" + item.content + ">");
      } else if(item.type == StyleItemType.FUNCTION) {
        newNode = QgrafStyleNode.ofFunction(item.content);
      } else {
        //Should never happen!
        throw new RuntimeException("invalid type " + item.type); 
      }
      
      graph.addVertex(newNode);
      graph.addEdge(lastNode, newNode, new QgrafStyleAction(type));
      lastNode = newNode;
      
      if(newNode.getContentType() == NodeContentType.LOOP) {
        QgrafStyleNode loopNode = createGraphImpl(graph, items, newNode);
        assert loopNode == newNode;
      }
    } 
    
    if(loop) { //After loop, link up to loopNode.
      //If the last node was a loop node, this is an EXIT action
      ActionType type = lastNode.getContentType() == NodeContentType.LOOP ? ActionType.EXIT_LOOP : ActionType.DEFAULT;
      graph.addEdge(lastNode, startNode, new QgrafStyleAction(type));
      return startNode;
    } else {
      return lastNode;
    }
    
    
  }
  
  /**
   * Loop condition for {@link #createGraphImpl(Graph, Deque, QgrafStyleNode)}.
   * @param top The current loop node, or the start node if {@code topLevel} is {@code true}
   * @param topLevel Whether this is not inside any loop
   * @param loopName The name of the innermost current loop. Should be {@code null}
   * iff {@code topLevel} ist {@code true}.
   * @return The next style item to add, or {@code null} if the loop is done
   * @throws QgrafImportException If loops are unclosed
   */
  private static StyleItem checkLoop(StyleItem top, boolean topLevel, String loopName) throws QgrafImportException {
    if(top == null && !topLevel) { //Deque is empty, but there is an unclosed loop
      throw new QgrafImportException("Loop " + loopName + " is not closed by <end> statement");
    } else if(top == null) {
      return null; //the last item in the style, abort, loop will be false
    }
    
    if(top.type == StyleItemType.KEYWORD && top.content.equals("end")) {
      return null; //end the current loop
    } else {
      return top;
    }
  }
  
}
