package qgraf;

import qgraf.QgrafStyleNode.LoopName;

/**
 * A {@link DiagramConstraints} instance provides information about the validity of certain node paths.
 * It is usually provided by the {@link QgrafOutputBuilder#constraints()} method.
 * It can be used to check whether a node path that takes a specific loop action (enter/exit) 
 * agrees with the information in the associated {@link QgrafOutputBuilder}.
 * The constraints can also be updated with the assumption of entering/exiting some loop without affecting the underlying builder.
 * 
 * @author Lars Bündgen
 */
public interface DiagramConstraints extends Cloneable2<DiagramConstraints> {

  /**
   * Returns {@code true} if a node path that enters/repeats the loop with the specified name is a valid path
   * with respect to the underlying builder, {@code false} otherwise.
   * @param name The name of the loop to check.
   * @return {@code true} if a valid path can enter the loop, {@code false} otherwise. 
   */
  public boolean canEnterLoop(LoopName name);
  
  /**
   * Returns {@code true} if a node path that exits/skips the loop with the specified name is a valid path
   * with respect to the underlying builder, {@code false} otherwise.
   * @param name The name of the loop to check.
   * @return {@code true} if a valid path can exit the loop, {@code false} otherwise. 
   */
  public boolean canExitLoop(LoopName name);
  
  /**
   * Returns {@code true} if the specified loop has a known upper limit for the number of times it can be repeated, {@code false}
   * if there is no upper limit for the iteration count. 
   * @param name The name of the loop to check.
   * @return {@code true} if there is an upper limit to the iteration count.
   */
  public boolean hasLoopLimit(LoopName name); //whether there is an upper limit for loop iterations
 
  /**
   * Update the state of this constraints object under the assumption that the specified loop was entered/repeated.
   * The builder object that created this constraints object will not be affected by this.
   * @param name The name of the loop to enter.
   */
  public void assumeEntered(LoopName name);
  
  /**
   * Update the state of this constraints object under the assumption that the specified loop was exited/skipped.
   * The builder object that created this constraints object will not be affected by this.
   * @param name The name of the loop to exit.
   */
  public void assumeExited(LoopName name);
  
}
