package qgraf;

/**
 * This exception type is thrown when a file can not be successfully parsed to a {@link QgrafStyleFile} or {@link QgrafOutputFile} object.
 * 
 * @author Lars Bündgen
 */
public class QgrafImportException extends Exception {
  private static final long serialVersionUID = -1192407923341521388L;

  /**
   * Creates a new instance with an exception message and a cause.
   * @param message The exception message. The message might be shown to the user.
   * @param cause The cause of this exception.
   */
  public QgrafImportException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Creates a new instance with an exception message.
   * @param message The exception message. The message might be shown to the user.
   */
  public QgrafImportException(String message) {
    super(message);
  }

}
