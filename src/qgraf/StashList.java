package qgraf;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * A custom data structure that consists of a list and a stack that can be used to stash list elements.
 * @author Lars Bündgen
 * @param <T> The content type
 */
public class StashList<T> implements Collection<T> {
  
  private final List<T> list;
  private final Deque<T> stash;
  
  /**
   * Creates a new instance with the list content.
   * @param toAdd The list content.
   */
  public StashList(Collection<? extends T> toAdd) {
    this();
    addAllLast(toAdd);
  }
  
  /**
   * Creates a new empty instance.
   */
  public StashList() {
    this.list = new ArrayList<>();
    this.stash = new ArrayDeque<>();
  }
  
  /**
   * Retrieves an item from the list based on its index.
   * @param index The index of the item
   * @return The item at that index.
   * @throws IndexOutOfBoundsException If the index is not valid for the list.
   */
  public T get(int index) {
    return list.get(index);
  }
  

  /**
   * Returns a list iterator for the list.
   * @param index The index of the first element in the iterator.
   * @return A list iterator for the list.
   * @see List#listIterator(int)
   */
  public ListIterator<T> listIterator(int index) {
    return list.listIterator(index);
  }
  
  /**
   * Returns the last (highes index) element in the list.
   * @return The last element in the list.
   * @throws NoSuchElementException If the list is empty.
   */
  public T getLast() {
    if(list.isEmpty()) throw new NoSuchElementException();
    return list.get(list.size() - 1);
  }
  
  /**
   * Returns the second last (second highest index) element in the list, which is 'below'
   * the last element.
   * @return The second last element, or {@code null} if the list has less than two elements.
   */
  public T peekBelow() {
    if(list.size() < 2) return null; 
    return list.get(list.size() - 2);
  }
  
  /**
   * Removes and returns the last (highest index) element in the list.
   * @return The removed last element.
   * @throws NoSuchElementException If the list is empty.
   */
  public T removeLast() {
    if(list.isEmpty()) throw new NoSuchElementException();
    return list.remove(list.size() - 1);
  }
  
  /**
   * Removes and returns the item on top of the stash.
   * @return The removed top element.
   * @throws NoSuchElementException If the stash is empty.
   */
  public T removeStashTop() {
    return stash.removeFirst();
  }
  
  /**
   * Takes the top element from the stash and appends it to the end of the list.
   * @return The moved element.
   * @throws NoSuchElementException If the stash is empty.
   */
  public T restoreStashTop() {
    T top = stash.removeFirst();
    list.add(top);
    return top;
  }
  
  /**
   * Appends an element to the end of the list.
   * @param element The element to add.
   */
  public void addLast(T element) {
    list.add(element);
  }
  
  /**
   * Appends all elements of a collection to the end of the list.
   * @param element The elements to add.
   * @throws NullPointerException If the parameter is {@code null}.
   */
  public void addAllLast(Collection<? extends T> element) {
    list.addAll(element);
  }
  
  /**
   * Asserts that the stash is empty.
   * @throws IllegalStateException If the stash is not empty.
   */
  public void requireStashEmpty() {
    if(!stash.isEmpty()) throw new IllegalStateException();
  }
  
  /**
   * Removes the last element from the list and puts it to the top of the stash.
   * @return The moved element.
   */
  public T stashLast() {
    T element = removeLast();
    stash.addFirst(element);
    return element;
  }
  
  /**
   * Takes all elements from the stash and moves them to the end of the list.
   * The top element of the stash is added first, so the bottom element of the stash will be at the end of the list.
   */
  public void restoreStash() {
    while(!stash.isEmpty()) {
      list.add(stash.removeFirst());
    }
  }
  
  /**
   * Clears all elements from the stash.
   */
  public void clearStash() {
    stash.clear();
  }
  
  /**
   * Clears all elements from the list. The stash is not modified.
   */
  @Override
  public void clear() {
    list.clear();
  }
  
  /**
   * Removes all elements from the stash. Each removed element is passed to the destination.
   * The top element from the stash is removed first.
   * @param destination The {@link Consumer} that accepts the removed items.
   */
  public void drainStash(Consumer<T> destination) {
    while(!stash.isEmpty()) {
      destination.accept(stash.removeFirst());
    }
  }
  
  /**
   * Removes all elements from the list. Each removed element is passed to the destination.
   * The first element from the list is removed first.
   * @param destination The {@link Consumer} that accepts the removed items.
   */
  public void drainList(Consumer<T> destination) {
    while(!list.isEmpty()) {
      destination.accept(list.remove(0));
    }
  }
  
  /**
   * Moves items from the end of the list to the top of the stash as long as
   * some predicate applies to the moved item. The first encountered element that does not match the predicate
   * will remain at the end of the list.
   * @param predicate The {@link Predicate} that tests the item to move.
   * @return {@code true} if stashing stopped because the predicate failed to match some element; 
   * {@code false} if stashing stopped because the list was empty.
   */
  public boolean stashWhile(Predicate<T> predicate) {
    while(!list.isEmpty() && predicate.test(getLast())) {
      stashLast();
    }
    return !list.isEmpty(); //false if empty
  }
  
  @Override
  public boolean isEmpty() {
    return list.isEmpty();
  }
  
  /**
   * Returns {@code true} if the stash is empty, {@code false} otherwise.
   * @return {@code true} if the stash is empty, {@code false} otherwise.
   */
  public boolean isStashEmpty() {
    return stash.isEmpty();
  }

  @Override
  public Iterator<T> iterator() {
    return list.iterator();
  }
  
  /**
   * Returns an iterator over the items in the stash from top to bottom.
   * @return An iterator over the items in the stash.
   */
  public Iterator<T> stashIterator() {
    return stash.iterator();
  }
  
  /**
   * The backing {@link List} that holds the data of the list part of this type. 
   * @return The backing list.
   */
  public List<T> getList() {
    return list;
  }
  
  /**
   * The backing {@link Deque} that holds the data of the stash part of this type. 
   * @return The backing stash.
   */
  public Deque<T> getStash() {
    return stash;
  }

  @Override
  public int size() {
    return list.size();
  }

  /**
   * Returns the number of elements in the stash.
   * @return The number of elements in the stash.
   */
  public int stashSize() {
    return stash.size();
  }
  
  @Override
  public boolean contains(Object o) {
    return list.contains(o);
  }

  @Override
  public Object[] toArray() {
    return list.toArray();
  }

  @Override
  public <E> E[] toArray(E[] a) {
    return list.toArray(a);
  }

  @Override
  public boolean add(T e) {
    return list.add(e);
  }

  @Override
  public boolean remove(Object o) {
    return list.remove(o);
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return list.containsAll(c);
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    return list.addAll(c);
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return list.removeAll(c);
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return list.retainAll(c);
  }
}
