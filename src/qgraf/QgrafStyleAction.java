package qgraf;

import java.util.Objects;

import org.jgrapht.graph.DefaultEdge;

/**
 * Type of edges in the graph representation of a style file. Does not modify {@link #hashCode()} or {@link #equals(Object)},
 * so two edges are equal by identity only. Extends {@link DefaultEdge} for better graph optimization.
 * <p>
 * Every edge represents a possible action of the parser, which is usually just to continue to the next style node 
 * (vertex in the graph representation). In case of loops, there can be different actions for repeating or exiting the loop.
 * 
 * @author Lars Bündgen
 */
public final class QgrafStyleAction extends DefaultEdge {
  private static final long serialVersionUID = -2507853124979736961L;

  /**
   * The possible types of an action.
   * 
   * @author Lars Bündgen
   */
  public enum ActionType {
    /**
     * The default action. Will always be chosen when available.
     * It should only be used when there is only one action available.
     */
    DEFAULT,
    /**
     * The action of repeating a loop (or entering it for the first time).
     * Usually used together with {@link #EXIT_LOOP}
     */
    REPEAT_LOOP,
    /**
     * The action of exiting a loop (or skipping it entirely).
     * Usually used together with {@link #REPEAT_LOOP}.
     */
    EXIT_LOOP;
  }
  
  private final ActionType type;
  
  /**
   * Creates a new style action (graph edge).
   * @param type The type of the edge.
   */
  public QgrafStyleAction(ActionType type) {
    this.type = Objects.requireNonNull(type);
  }
  
  /**
   * Returns the type of this action.
   * @return the type of this action.
   */
  public ActionType getType() {
    return type;
  }

  @Override
  public String toString() {
    return type.toString();
  }
  
}
