package qgraf;

import java.awt.Window;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import game.Presets;
import qgraf.QgrafModelFile.QgrafModelFormatException;
import resources.GetResources;
import ui.Frame;
import ui.ImportQgrafDiagramDialog;

/**
 * Collects settings and methods related to qgraf import;
 * Introduced to better decouple the import from the Frame class since 
 * it should be deactivated in InFin mode
 */
public class QgrafImportManager {
  private static final Logger LOGGER = Frame.getLogger(QgrafImportManager.class);
  
  private QgrafImportManager() {
    throw new UnsupportedOperationException();
  }
  
  private static final JFileChooser qgrafOutputChooser;
  private static final JFileChooser qgrafStyleChooser;
  private static final JFileChooser qgrafModelChooser;
  private static ImportQgrafDiagramDialog importDialog;
  
  private static final List<LoadedQgrafStyle> loadedStyles;
  
  static {
    qgrafOutputChooser = new JFileChooser();
    qgrafOutputChooser.setMultiSelectionEnabled(false);
    qgrafOutputChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    qgrafOutputChooser.setDialogType(JFileChooser.OPEN_DIALOG);
    qgrafOutputChooser.setDialogTitle("Select qgraf output file");
    qgrafOutputChooser.setAcceptAllFileFilterUsed(true);
    
    qgrafStyleChooser = new JFileChooser();
    qgrafStyleChooser.setMultiSelectionEnabled(true);
    qgrafStyleChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    qgrafStyleChooser.setDialogType(JFileChooser.OPEN_DIALOG);
    qgrafStyleChooser.setDialogTitle("Select qgraf style file(s)");
    qgrafStyleChooser.setAcceptAllFileFilterUsed(true);
    qgrafStyleChooser.addChoosableFileFilter(new FileNameExtensionFilter("qgraf style (*.sty)", "sty"));
    
    qgrafModelChooser = new JFileChooser();
    qgrafModelChooser.setMultiSelectionEnabled(false);
    qgrafModelChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    qgrafModelChooser.setDialogType(JFileChooser.OPEN_DIALOG);
    qgrafModelChooser.setDialogTitle("Select qgraf model file");
    qgrafModelChooser.setAcceptAllFileFilterUsed(true);
    
    loadedStyles = new ArrayList<>();
  }
  
  private static boolean loadMomentumArrows;
  static boolean unlimitedVertexDegree = false;
  
  public static boolean loadMomentumArrows() {
    return loadMomentumArrows;
  }
  
  public static void loadMomentumArrows(boolean value) {
    loadMomentumArrows = value;
  }
  
  public static boolean unlimitedVertexDegree() {
    return unlimitedVertexDegree;
  }
  
  public static void unlimitedVertexDegree(boolean value) {
    unlimitedVertexDegree = value;
  }
  
  public static void initFrame(Frame frame) {
    importDialog = new ImportQgrafDiagramDialog(frame); //TODO can this ever work in a reliable way?
  }
  
  public static void readPreferences(String line) {
    if(line.startsWith("Qgraf_Output_File")) {
      qgrafOutputChooser.setSelectedFile(readPrefsPath(line));
    } else if(line.startsWith("Qgraf_Style_File")) {
      qgrafStyleChooser.setSelectedFiles(new File[] {readPrefsPath(line)});
    } else if(line.startsWith("Qgraf_Model_File")) {
      qgrafModelChooser.setSelectedFile(readPrefsPath(line));
    } else if(line.startsWith("Qgraf_Import_All")) {
      importDialog.setImportAll(readBoolean(line, "Qgraf_Import_All", true));
    } else if(line.startsWith("Qgraf_Import_Index")) {
      importDialog.setSelectedDiagramIndex(readPosInt(line, "Qgraf_Import_Index", 1));
    } else if(line.startsWith("Qgraf_Import_Style")) {
      String s =line.substring(line.indexOf(':') + 1);
      loadedStyles.stream()
        .filter(l -> l.getName().equals(s))
        .findFirst()
        .ifPresent(importDialog::setSelectedStyle);
    } else if(line.startsWith("Qgraf_Load_Momenta")) {
      loadMomentumArrows = readBoolean(line, "Qgraf_Load_Momenta", false);
    } else if(line.startsWith("Qgraf_unlimited_vertex_degree")) {
      unlimitedVertexDegree = readBoolean(line, "Qgraf_unlimited_vertex_degree", false);
    }
  }
  
  private static int readPosInt(String line, String name, int defaultValue) {
    try {
      int i = Integer.parseInt(line.split(":")[1]);
      if(i <= 0) return defaultValue;
      else return i;
    } catch (NumberFormatException e) {
      LOGGER.warning("Config option " + name + " has invalid value");
      return defaultValue;
    } catch (ArrayIndexOutOfBoundsException e) {
      LOGGER.warning("Config option " + name + " has no value");
      return defaultValue;
    }
  }
  
  private static boolean readBoolean(String line, String name, boolean defaultValue) {
    String[] split = line.split(":");
    if(split.length >= 2) {
      return Boolean.parseBoolean(split[1]);
    } else {
      LOGGER.warning("Config option " + name +" has no value");
      return defaultValue;
    }
  }
  
  private static File readPrefsPath(String line) {
    return new File(line.substring(line.indexOf(':') + 1));
  }
  
  private static final String LF = System.getProperty("line.separator");
  public static void writePreferences(BufferedWriter bw) throws IOException {
    if(qgrafOutputChooser.getSelectedFile() != null)
      bw.write("Qgraf_Output_File:" + qgrafOutputChooser.getSelectedFile().getAbsolutePath() + LF);
    if(qgrafStyleChooser.getSelectedFile() != null)
      bw.write("Qgraf_Style_File:" + qgrafStyleChooser.getSelectedFile().getAbsolutePath() + LF);
    else if(qgrafStyleChooser.getSelectedFiles() != null && qgrafStyleChooser.getSelectedFiles().length != 0)
      bw.write("Qgraf_Style_File:" + qgrafStyleChooser.getSelectedFiles()[0].getAbsolutePath() + LF);
    if(qgrafModelChooser.getSelectedFile() != null)
      bw.write("Qgraf_Model_File:" + qgrafModelChooser.getSelectedFile().getAbsolutePath() + LF);
    bw.write("Qgraf_Import_All:" + Boolean.toString(importDialog.getImportAll()) + LF);
    bw.write("Qgraf_Import_Index:" + Integer.toString(importDialog.getSelectedDiagramIndex()) + LF);
    if(importDialog.getSelectedStyle() != null)
      bw.write("Qgraf_Import_Style:" + importDialog.getSelectedStyle().getName() + LF);
    bw.write("Qgraf_Load_Momenta:" + Boolean.toString(loadMomentumArrows) + LF);
    bw.write("Qgraf_unlimited_vertex_degree:" + Boolean.toString(unlimitedVertexDegree));
  }
  
  
  public static void doGuiImport(Frame frame, Path selectedPath) {
    assertDrawmode();
    LOGGER.info("Starting qgraf import from \"" + selectedPath.toString() + "\"");
    Frame.setStatusText("Importing from qgraf file " + selectedPath.toString());
    Presets currentModel = frame.getSelectedPresets();
    QgrafOutputFile out = importDialog.showDialog(selectedPath, new ModelData(currentModel));
    //    lastSelectedStyleIndex = dia.getSelectedStyleIndex();
    int index = importDialog.getSelectedDiagramIndex();
    if(out == null || !out.checkIndex(index, frame)) {
      if(importDialog.showCancelMessage()) {
        LOGGER.info("Canceled qgraf import from \"" + selectedPath.toString() + "\"");
        Frame.setStatusText("Import from qgraf canceled");
      } else {
        LOGGER.info("Failed qgraf import from \"" + selectedPath.toString() + "\"");
        Frame.setStatusText("Import from qgraf failed");
      }
    } else {
      boolean all = importDialog.getImportAll();
      frame.setDiagramAndMultiMode(out, all);
      int count = out.getDiagramCount();
      LOGGER.info("Successful qgraf import from \"" + selectedPath.toString() + "\": Imported " + count + " diagrams");
      Frame.setStatusText("Imported " + count + " diagrams from qgraf file " + selectedPath.toString());
    }
  }
  
  public static void doGuiImport(Frame frame) {
    assertDrawmode();
    int result = qgrafOutputChooser.showDialog(frame, "Import");
    if(result != JFileChooser.APPROVE_OPTION) return;
    File selectedFile = qgrafOutputChooser.getSelectedFile();
    if(selectedFile == null) return; //just to be sure, idk when selectedFile can be null
    doGuiImport(frame, selectedFile.toPath());
  }
  
  //return the most recent added style so it can be set as current selection elsewhere
  public static LoadedQgrafStyle doGuiStyleLoad(Window parent, boolean addAsDefault) {
    assertDrawmode();
    int result = qgrafStyleChooser.showDialog(parent, "Load");
    if(result != JFileChooser.APPROVE_OPTION) return null;
    File[] selectedFiles = qgrafStyleChooser.getSelectedFiles();
    if(selectedFiles == null) {
      File selectedFile = qgrafStyleChooser.getSelectedFile();
      if(selectedFile != null) {
        selectedFiles = new File[] {selectedFile};
      } else {
        return null; //Nothing selected
      }
    }
    
    LoadedQgrafStyle last = null;
    for(File f : selectedFiles) {
      Path p = f.toPath().toAbsolutePath();
      LOGGER.info("Starting style import from \"" + p.toString() + "\"");
      Frame.setStatusText("Importing qgraf style " + p.toString());
      try {
        if(addAsDefault) {
          addAsQgrafDefaultStyle(p); //This first copies it to the default folder
        } else {
          QgrafStyleFile qs = QgrafStyleFile.fromFile(p);
          LoadedQgrafStyle ls = new LoadedQgrafStyle(qs, p, false, false);
          loadedStyles.add(ls);
          last = ls;
        }
        LOGGER.info("Successful style import from \"" + p.toString() + "\"");
        Frame.setStatusText("Imported qgraf style " + p.toString());
      } catch (QgrafImportException e) {
        JOptionPane.showMessageDialog(parent, "Selected file is not a valid qgraf style file: " + p.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        LOGGER.log(Level.WARNING, "Failed style import from \"" + p.toString() + "\"", e);
        Frame.setStatusText("Import of qgraf style from " + p.toString() + " failed");
      } catch (IOException e) {
        JOptionPane.showMessageDialog(parent, "Cannot find or access selected file: " + p.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        LOGGER.log(Level.WARNING, "Failed style import from \"" + p.toString() + "\"", e);
        Frame.setStatusText("Import of qgraf style from " + p.toString() + " failed");
      }
    }
    return last;
  }
  
  public static void doGuiModelLoad(Frame frame) {
    int result = qgrafModelChooser.showDialog(frame, "Import");
    if(result != JFileChooser.APPROVE_OPTION || qgrafModelChooser.getSelectedFile() == null) return;
    Path p = qgrafModelChooser.getSelectedFile().toPath().toAbsolutePath();
    try {
      LOGGER.info("Starting qgraf model import from \"" + p.toString() + "\"");
      Frame.setStatusText("Importing qgraf model " + p.toString());
      QgrafModelFile model = QgrafModelFile.importFromFile(p);
      Presets presets = model.createPreset();
      presets.allowUnsaved = true;
      presets.unsaved = true;
      frame.tileTabs.addModel(presets, model.getName());
      frame.tileTabs.update();
      frame.tileTabs.setCurrUnsaved();
      LOGGER.info("Successful qgraf model import from \"" + p.toString() + "\"");
      Frame.setStatusText("Imported qgraf model " + p.toString());
    } catch (IOException e) {
      JOptionPane.showMessageDialog(frame, "An error occurred while importing the model file:\n" + e.getMessage(), "Import error", JOptionPane.ERROR_MESSAGE);
      LOGGER.log(Level.WARNING, "Failed model import from \"" + p.toString() + "\"", e);
      Frame.setStatusText("Import of qgraf model from " + p.toString() + " failed");
    } catch (QgrafModelFormatException e) {
      JOptionPane.showMessageDialog(frame, "This is not a valid qgraf model file:\n" + e.getMessage(), "Import error", JOptionPane.ERROR_MESSAGE);
      LOGGER.log(Level.WARNING, "Failed model import from \"" + p.toString() + "\"", e);
      Frame.setStatusText("Import of qgraf model from " + p.toString() + " failed");
    }
  }
  
  public static void doClipboardGuiImport(Frame frame, String diagram) {
    Objects.requireNonNull(diagram);
    QgrafOutputFile out = null;
    Presets currentModel = frame.getSelectedPresets();
    
    LOGGER.info("Clipboard: Attempting to parse pasted text as full qgraf output file...");
    for(LoadedQgrafStyle style : QgrafImportManager.getQgrafStyles()) {
      try {
        out = QgrafOutputFile.fromString("(from clipboard)", diagram, style.getStyle(), new ModelData(currentModel));
        LOGGER.info("Clipboard: ...successfully parsed pasted data with style '" + style.getName() + "'");
        break;
      } catch (QgrafImportException e) {
        LOGGER.info("Clipboard: ...qgraf style '" + style.getName() + "' does not match pasted data...");
        continue;
      }
    }

    if(out == null) {
      LOGGER.info("Clipboard ...no matching style file found");
      LOGGER.info("Clipboard: Attempting to parse pasted text as single diagram from qgraf output file...");
      for(LoadedQgrafStyle style : QgrafImportManager.getQgrafStyles()) {
        try {
          out = QgrafOutputFile.singleFromString("(diagram from clipboard)", diagram, style.getStyle(), new ModelData(currentModel));
          LOGGER.info("Clipboard: ...successfully parsed pasted data with style '" + style.getName() + "'");
          break;
        } catch (QgrafImportException e) {
          LOGGER.info("Clipboard: ...qgraf style '" + style.getName() + "' does not match pasted data...");
          continue;
        }
      }
    }

    if(out == null) {
      LOGGER.info("Clipboard: ...failed: none of the loaded qgraf styles match the output file");
    } else if(out.getDiagramCount() == 0) {
      LOGGER.info("Clipboard: Pasted output file does not contain any diagrams");
    } else {
      frame.setDiagramAndMultiMode(out, 0, out.getDiagramCount() != 1);
      Frame.setStatusText("Imported diagram from clipboard");
    }
  }
  
  public static void updateQgrafDefaultStyles() {
    List<LoadedQgrafStyle> nonDefaultStyles = loadedStyles.stream()
        .filter(l -> !l.isBuiltinStyle() && !l.isDefaultStyle())
        .collect(Collectors.toCollection(ArrayList::new));
    
    loadedStyles.clear();
    addQgrafDefaultJarStyles();
    Path folder = getQgrafDefaultStyleFolder();
    if(folder == null) return;
    try(Stream<Path> s = Files.list(folder)) {
      s.filter(Files::isRegularFile).map(Path::toAbsolutePath).forEach(p -> {
        try {
          QgrafStyleFile style = QgrafStyleFile.fromFile(p);
          LoadedQgrafStyle loaded = new LoadedQgrafStyle(style, p, false, true);
          loadedStyles.add(loaded);
        } catch (QgrafImportException | IOException e) {
          LOGGER.log(Level.WARNING, "Invalid file in style folder: " + p.toString(), e);
        }
        
      });
    } catch (NoSuchFileException e) {
      try {
        Files.createDirectories(folder);
      } catch (IOException e1) {
        LOGGER.log(Level.SEVERE, "Cannot create default style folder", e1);
      }
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Cannot access default style folder", e);
    }
    LOGGER.info("Loaded " + loadedStyles.size() + " default styles");
    loadedStyles.addAll(nonDefaultStyles);
  }

  private static void addQgrafDefaultJarStyles() {
    for(LoadedQgrafStyle ds : GetResources.getDefaultStyles()) {
      loadedStyles.add(ds);
    }
  }

  public static Path getQgrafDefaultStyleFolder() {
    final String os = System.getProperty("os.name").toLowerCase();
    final String home = System.getProperty("user.home");
    if (os.startsWith("mac")) {
      return Paths.get(home, "Library", "Preferences", "FeynGame", "DefaultQgrafStyles");
    } else if (os.startsWith("win")) {
      return Paths.get(home, "AppData", "Roaming", "FeynGame", "DefaultQgrafStyles");
    } else if (os.startsWith("linux")) {
      return Paths.get(home, ".config", "FeynGame", "DefaultQgrafStyles");
    } else {
      LOGGER.warning("Unknown operating system; saving default styles in working directory");
      return Paths.get(".");
    }
  }

  //Copy and add style file from external source
  public static void addAsQgrafDefaultStyle(Path path) {
    String styleName = path.getFileName().toString();
    if(styleName.endsWith(".sty")) styleName = styleName.substring(0, styleName.length() - 4);
    Path destination = getQgrafDefaultStyleFolder();

    //Find a unused file name
    Path uniquePath = destination.resolve(styleName + ".sty");
    int uniqueNamePart = 0;
    while(Files.exists(uniquePath)) {
      uniquePath = destination.resolve(styleName + "_" + uniqueNamePart + ".sty");
      uniqueNamePart++;
    }

    try {
      if(!Files.exists(uniquePath.getParent())) {
        Files.createDirectories(uniquePath.getParent());
      }
      Files.copy(path, uniquePath, StandardCopyOption.REPLACE_EXISTING);
      QgrafStyleFile qs = QgrafStyleFile.fromFile(uniquePath);
      LoadedQgrafStyle ls = new LoadedQgrafStyle(qs, uniquePath, false, true);
      loadedStyles.add(ls);
    } catch (IOException e) {
      LOGGER.log(Level.SEVERE, "Unable to copy default qgraf model file to " + uniquePath, e);
    } catch (QgrafImportException e) {
      LOGGER.log(Level.SEVERE, "Invalid style file " + uniquePath, e);
    }
  }
  
  public static LoadedQgrafStyle doAutoStyleLoad(Path stylePath) throws QgrafImportException, IOException {
    assertDrawmode();
    QgrafStyleFile qs = QgrafStyleFile.fromFile(stylePath);
    LoadedQgrafStyle ls = new LoadedQgrafStyle(qs, stylePath, false, false);
    loadedStyles.add(ls);
    return ls;
  }
  
  public static void doAutoImport(Frame frame, Path qgrafOutputFile, QgrafStyleFile style, ModelData model, int loadIndex) throws IOException, QgrafImportException {
    assertDrawmode();
    QgrafOutputFile qof = QgrafOutputFile.fromFile(qgrafOutputFile, style, loadIndex, model);
    if(qof == null) throw new QgrafImportException("Diagram with requested index not found");
    boolean loadAll = loadIndex == QgrafOutputFile.LOAD_ALL_INDEX_VALUE;
    frame.setDiagramAndMultiMode(qof, loadAll ? loadIndex : 0, loadAll);
  }
  
  public static List<LoadedQgrafStyle> getQgrafStyles() {
    return Collections.unmodifiableList(loadedStyles);
  }
  
  private static void assertDrawmode() {
    if(Frame.gameMode != 0) throw new IllegalStateException("Method should be inaccessible in InFin mode");
  }
}
