package export;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import export.ExportManager.PaperUnit;
import game.FeynLine;
import game.FloatingObject;
import game.Point2D;
import game.Shaped;
import game.Vertex;
import ui.BoundingBoxCalculator;
import ui.Frame;
import ui.Pane;

public class TexExportFormat extends ExportFormat {
  
  private final ExportFormat epsExport;
  
  protected TexExportFormat(String description, ExportFormat epsExport, String...allExtensions) {
    super(new FileNameExtensionFilter(description, allExtensions));
    this.epsExport = Objects.requireNonNull(epsExport);
    if(!epsExport.isExportFormatFor("eps")) throw new IllegalArgumentException();
  }

  @Override
  public ExportDialogTemplate createDialog(Frame parent, String displayDestination) {
    return new ExportDialog(parent, displayDestination);
  }

  @Override
  public ExportResult doAutoExport(Pane pane, Path destination, ExportProgressUpdate update) throws IOException, ExportException {
    String filenameWithoutExtension = ExportManager.getWithoutExtension(destination);
    Path epsDestination = destination.resolveSibling(filenameWithoutExtension + ".eps");
    Pane.drawLabel = false;
    ExportResult prevResult = epsExport.doAutoExport(pane, epsDestination, update);
    Pane.drawLabel = true;
    return doTexConversion(prevResult, pane, epsDestination, destination, update);
  }
  
  @Override
  public void doGuiExport(Frame parent, String displayDestination, Path destination, ExportContinuation continuation) {
    ExportContinuation ec = ExportContinuation.combine(continuation, (r, u) -> {
      double w = r.getCanvasSize().getWidth();
      double h = r.getCanvasSize().getHeight();
      PageFormat pf = new PageFormat();
      Paper p = new Paper();
      p.setSize(w, h);
      p.setImageableArea(0, 0, w, h);
      pf.setPaper(p);
      pf.setOrientation(PageFormat.PORTRAIT);
      SwingUtilities.invokeLater(() -> {
        if(ExportManager.showBBcalc.isSelected()) {
          BoundingBoxCalculator bbc = new BoundingBoxCalculator(w, h, pf, new Rectangle2D.Double(0, 0, w, h), false);
          bbc.showBB();
        }
      });
      return r;
    });
    
    //Pass continuation that shows BBCalc afterwards
    super.doGuiExport(parent, displayDestination, destination, ec);
  }
  
  private ExportResult doTexConversion(ExportResult prevResult, Pane original, Path epsDestination, Path destination, ExportProgressUpdate update) throws IOException {
    if(prevResult.isCanceled()) return ExportResult.CANCELED;
    /*
     * Here we make some assumptions about the eps page format:
     * The imageable area is the same as the full page area, so imageableX = imageableY = 0
     */
    
    try (BufferedWriter bw = Files.newBufferedWriter(destination, StandardCharsets.UTF_8)) {      
      double rel = prevResult.getCanvasSize().getWidth() / prevResult.getCanvasSize().getHeight();
      String viewportString =  "viewport = 0 0 " + (int) Math.round(prevResult.getCanvasSize().getWidth()) + " " + (int) Math.round(prevResult.getCanvasSize().getHeight());
      if(cancellationRequested()) return ExportResult.CANCELED;
      
      bw.write("% Created with FeynGame\n");
      bw.write("% Include with input{" + destination.getFileName().toString() + "}\n");
      bw.write("% Width can be set with \\def\\svgwidth{...} and a scaling with \\def\\svgscale{...}\n");
      bw.write("% These options have to be set before the import statement and only apply to one included file\n");
      bw.write("\\begingroup%\n");
      bw.write("  \\makeatletter%\n");
      bw.write("  \\providecommand\\rotatebox[2]{#2}%\n");
      bw.write("  \\ifx\\svgwidth\\undefined%\n");
      bw.write("    \\setlength{\\unitlength}{595.32000732bp}%\n");
      bw.write("    \\ifx\\svgscale\\undefined%\n");
      bw.write("      \\relax%\n");
      bw.write("    \\else%\n");
      bw.write("      \\setlength{\\unitlength}{\\unitlength * \\real{\\svgscale}}%\n");
      bw.write("    \\fi%\n");
      bw.write("  \\else%\n");
      bw.write("    \\setlength{\\unitlength}{\\svgwidth}%\n");
      bw.write("  \\fi%\n");
      bw.write("  \\global\\let\\svgwidth\\undefined%\n");
      bw.write("  \\global\\let\\svgscale\\undefined%\n");
      bw.write("  \\makeatother%\n");
      bw.write("  \\parbox[c][][c]{\\unitlength}{%\n");
      bw.write("    \\begin{picture}(1," + 1 / rel + ")%\n");
      bw.write("      \\put(0,0){\\includegraphics[width=\\unitlength," + viewportString + "]{" + epsDestination.getFileName().toString() + "}}%\n");
      
      Dimension2D pane = prevResult.getCroppedPaneSize();
      Rectangle2D offset = original.getDiagramBounds();
      for (FeynLine fl : original.lines) {
        if(cancellationRequested()) return ExportResult.CANCELED;
        double len = Math.sqrt((fl.getStartX() - fl.getEndX()) * (fl.getStartX() - fl.getEndX()) + (fl.getStartY() - fl.getEndY()) * (fl.getStartY() - fl.getEndY()));
        String description = fl.lineConfig.description;
        if (fl.direction == -1) description = fl.lineConfig.negDescription;
        if (fl.direction == +1) description = fl.lineConfig.posDescription;
        if ((len > 0.1 || Math.abs(fl.height) > .1) && fl.showDesc && description != null && !description.equals("") && fl.descScale != 0) {
          Point2D place = fl.getLabelPosition();

          place.x -= offset.getMinX();
          place.y -= offset.getMinY();
          place.y = pane.getHeight() - place.y;
          place.x = place.x / pane.getWidth();
          place.y = place.y / pane.getWidth();
          bw.write("      \\put(" + place.x + "," + place.y + "){\\raisebox{-0.5\\height}{\\rotatebox{" + fl.rotDesc * 180. / Math.PI + "}{\\scalebox{" + fl.descScale + "}{\\makebox(0,0)[]{{$" + description + "$}}}}}}%\n");
        }
        if (fl.isMom() && fl.momArrow.description != "" && fl.momArrow.description != null && fl.momArrow.descScale != 0) {
          Point2D place = fl.momArrow.getLabelPosition();

          place.x -= offset.getMinX();
          place.y -= offset.getMinY();
          place.y = pane.getHeight() - place.y;
          place.x = place.x / pane.getWidth();
          place.y = place.y / pane.getWidth();
          bw.write("      \\put(" + place.x + "," + place.y + "){\\raisebox{-0.5\\height}{\\rotatebox{" + fl.momArrow.rotDesc * 180. / Math.PI + "}{\\scalebox{" + fl.momArrow.descScale + "}{\\makebox(0,0)[]{{$" + fl.momArrow.description + "$}}}}}}%\n");

        }
      }

      for (Vertex v : original.vertices) {
        if(cancellationRequested()) return ExportResult.CANCELED;
        if (v.showDesc && v.description != null && v.description != "" && v.descScale != 0) {
          Point2D place = v.getLabelPosition();
          place.x -= offset.getMinX();
          place.y -= offset.getMinY();
          place.y = pane.getHeight() - place.y;
          place.x = place.x / pane.getWidth();
          place.y = place.y / pane.getWidth();
          bw.write("      \\put(" + place.x + "," + place.y + "){\\raisebox{-0.5\\height}{\\rotatebox{" + v.rotDesc * 180. / Math.PI + "}{\\scalebox{" + v.descScale + "}{\\makebox(0,0)[]{{$" + v.description + "$}}}}}}%\n");
        }
      }

      for (FloatingObject fO : original.fObjects) {
        if(cancellationRequested()) return ExportResult.CANCELED;
        if (fO.v) {
          if (fO.vertex.showDesc && fO.vertex.description != null && fO.vertex.description != "" && fO.vertex.descScale != 0) {
            Point2D place = fO.vertex.getLabelPosition();
            place.x -= offset.getMinX();
            place.y -= offset.getMinY();
            place.y = pane.getHeight() - place.y;
            place.x = place.x / pane.getWidth();
            place.y = place.y / pane.getWidth();
            bw.write("      \\put(" + place.x + "," + place.y + "){\\raisebox{-0.5\\height}{\\rotatebox{" + fO.vertex.rotDesc * 180. / Math.PI + "}{\\scalebox{" + fO.vertex.descScale + "}{\\makebox(0,0)[]{{$" + fO.vertex.description + "$}}}}}}%\n");
          }
        } else {
          if (fO.floatingImage.showDesc && fO.floatingImage.description != null && fO.floatingImage.description != "" && fO.floatingImage.descScale != 0) {
            Point2D place = fO.floatingImage.getLabelPosition();
            place.x -= offset.getMinX();
            place.y -= offset.getMinY();
            place.y = pane.getHeight() - place.y;
            place.x = place.x / pane.getWidth();
            place.y = place.y / pane.getWidth();
            bw.write("      \\put(" + place.x + "," + place.y + "){\\raisebox{-0.5\\height}{\\rotatebox{" + fO.floatingImage.rotDesc * 180. / Math.PI + "}{\\scalebox{" + fO.floatingImage.descScale + "}{\\makebox(0,0)[]{{$" + fO.floatingImage.description + "$}}}}}}%\n");
          }
        }
      }

      for (Shaped s : original.shapes) {
        if(cancellationRequested()) return ExportResult.CANCELED;
        if (s.showDesc && s.description != null && s.description != "" && s.descScale != 0) {
          Point2D place = s.getLabelPosition();
          place.x -= offset.getMinX();
          place.y -= offset.getMinY();
          place.y = pane.getHeight() - place.y;
          place.x = place.x / pane.getWidth();
          place.y = place.y / pane.getWidth();
          bw.write("      \\put(" + place.x + "," + place.y + "){\\raisebox{-0.5\\height}{\\rotatebox{" + s.rotDesc * 180. / Math.PI + "}{\\scalebox{" + s.descScale + "}{\\makebox(0,0)[]{{$" + s.description + "$}}}}}}%\n");
        }
      }
      bw.write("    \\end{picture}%\n");
      bw.write("  }%\n");
      bw.write("\\endgroup%\n");
    }
     return prevResult;
  }
  
  @Override
  public boolean isSecret() {
    return true;
  }

  @Override
  public String[] getAdditionalExportedExtensions() {
    return new String[] { "eps" };
  }
  
  private static class ExportDialog extends ExportDialogTemplate {
    private static final long serialVersionUID = 1L;

    protected ExportDialog(Frame owner, String destination) {
      super(owner, owner.getDrawPane(), destination);
    }

    @Override
    protected boolean addContentOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      JCheckBox blackWhite = new JCheckBox("Export in black/white");
      blackWhite.setModel(ExportManager.checkboxBlackWhite);
      addComponentToDisable(blackWhite);
      centerPanel.add(blackWhite, gbc);
      JCheckBox showBBcalc = new JCheckBox("Show viewbox parameters");
      showBBcalc.setModel(ExportManager.showBBcalc);
      addComponentToDisable(showBBcalc);
      centerPanel.add(showBBcalc, gbc);
      return true;
    }

    @Override
    protected boolean addSimpleOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      
      JPanel resolutionBox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      resolutionBox.add(new JLabel("Width:"), gbc2);
      gbc2.gridx = 1;
      gbc2.weightx = 1.0;
      JSpinner scaleCanvas = new JSpinner(ExportManager.spinnerPaperX);
      resolutionBox.add(scaleCanvas, gbc2);
      addComponentToDisable(scaleCanvas);
      gbc2.gridx = 2;
      gbc2.weightx = 0.0;
      JComboBox<PaperUnit> unit = new JComboBox<>(ExportManager.comboBoxPaperUnit);
      unit.setRenderer(ExportManager.rendererPaperUnit);
      resolutionBox.add(unit, gbc2);
      addComponentToDisable(unit);
      gbc2.gridx = 3;
      resolutionBox.add(Box.createHorizontalStrut(50), gbc);
      centerPanel.add(resolutionBox, gbc);
      scaleCanvas.addChangeListener(this::updateExportSizeText);
      unit.addActionListener(this::updateExportSizeText);
      return true;
    }

    @Override
    protected boolean addAdvancedOptions(JPanel centerPanel, GridBagConstraints gbc) {
      return false;
    }
    
  }

}
