package export;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import export.ExportManager.PaperUnit;
import ui.Frame;
import ui.Pane;

public class EpsExportFormat extends ExportFormat {
  static final FileFilter EPS_FILE_FILTER = new FileNameExtensionFilter("EPS (*.eps)", "eps");  
  
  private final StreamPrintExportFormat exportDelegate;
  
  EpsExportFormat(String description, StreamPrintExportFormat delegate, String...allExtensions) {
    super(new FileNameExtensionFilter(description, allExtensions));
    if(!delegate.isExportFormatFor("ps")) throw new IllegalArgumentException();
    this.exportDelegate = delegate;
  }


  @Override
    public ExportDialogTemplate createDialog(Frame parent, String displayDestination) {
      return new ExportDialog(parent, displayDestination);
    }

  @Override
  public ExportResult doAutoExport(Pane content, Path destination, ExportProgressUpdate update)
      throws IOException, ExportException {
    Path tempFile = ExportManager.getTempFile(exportDelegate.getPreferredExtension());
    ExportResult prevResult = exportDelegate.doAutoExport(content, tempFile, update);
    return doEpsConversion(prevResult, tempFile, destination, update, true);
  }
  
  private ExportResult doEpsConversion(ExportResult prevResult, Path source, Path destination, ExportProgressUpdate update, boolean deleteSourceFile)
      throws ExportException, IOException {
    if(prevResult.isCanceled()) return prevResult;
    if(prevResult.isMultipage()) throw new ExportException("Cannot convert PS documents with more than one page to EPS");
    Rectangle2D bb = prevResult.getBoundingBox();
    if(bb == null) throw new ExportException("Unexpected error: Previous export stage did not provide bounding box information");
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    try(BufferedReader br = Files.newBufferedReader(source, StandardCharsets.UTF_8);
        BufferedWriter bw = Files.newBufferedWriter(destination, StandardCharsets.UTF_8)) {
      
      String line = br.readLine();
      bw.write("%!PS-Adobe-3.0 EPSF-3.0\n");
      String boundingBoxStr = "%%BoundingBox: ";
      boundingBoxStr += (int) Math.round(bb.getMinX()) + " " + (int) Math.round(bb.getMinY()) + " " 
          + (int) Math.round(bb.getMaxX()) + " " + (int) Math.round(bb.getMaxY()) + "\n";
      bw.write(boundingBoxStr);
      while ((line = br.readLine()) != null) {
        if(cancellationRequested()) return ExportResult.CANCELED;
        if (!line.contains("%%EndSetup") && !line.contains("%%BeginSetup") && !line.contains("<< /PageSize")) {
          bw.write(line + "\n");
        }
      }
    } finally {
      if(deleteSourceFile) {
        Files.delete(source);
      }
    }
    return new ExportResult(bb.getWidth(), bb.getHeight(), 0, 0, bb.getWidth(), bb.getHeight(), prevResult.getCroppedPaneSize(), false);
  }
  
  @Override
  public boolean isSecret() {
    return false;
  };
  
  private class ExportDialog extends ExportDialogTemplate {
    private static final long serialVersionUID = 1L;

    protected ExportDialog(Frame owner, String destination) {
      super(owner, owner.getDrawPane(), destination);
    }
    
    @Override
    protected boolean addContentOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      JCheckBox blackWhite = new JCheckBox("Export in black/white");
      blackWhite.setModel(ExportManager.checkboxBlackWhite);
      addComponentToDisable(blackWhite);
      centerPanel.add(blackWhite, gbc);
      return true;
    }

    @Override
    protected boolean addSimpleOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      
      JPanel resolutionBox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      resolutionBox.add(new JLabel("Width:"), gbc2);
      gbc2.gridx = 1;
      gbc2.weightx = 1.0;
      JSpinner scaleCanvas = new JSpinner(ExportManager.spinnerPaperX);
      resolutionBox.add(scaleCanvas, gbc2);
      addComponentToDisable(scaleCanvas);
      gbc2.gridx = 2;
      gbc2.weightx = 0.0;
      JComboBox<PaperUnit> unit = new JComboBox<>(ExportManager.comboBoxPaperUnit);
      unit.setRenderer(ExportManager.rendererPaperUnit);
      resolutionBox.add(unit, gbc2);
      addComponentToDisable(unit);
      gbc2.gridx = 3;
      resolutionBox.add(Box.createHorizontalStrut(50), gbc);
      centerPanel.add(resolutionBox, gbc);
      scaleCanvas.addChangeListener(this::updateExportSizeText);
      unit.addActionListener(this::updateExportSizeText);
      return true;
    }
    
    @Override
    protected boolean addAdvancedOptions(JPanel centerPanel, GridBagConstraints gbc) {
      return false;
    }

    @Override
    protected void updateExportSizeText(Object anyEventOrNull) {
      Rectangle2D paneRect = getPaneCropRectangle();
      double w = ExportManager.spinnerPaperX.getNumber().doubleValue();
      double h = w * paneRect.getHeight() / paneRect.getWidth();
      PaperUnit u = (PaperUnit) ExportManager.comboBoxPaperUnit.getSelectedItem();
      String ws = NumberFormat.getNumberInstance().format(w);
      String hs = NumberFormat.getNumberInstance().format(h);
      setExportSizeText(ws + u.getDisplayName() + " by " + hs + u.getDisplayName());
    }
  }
 
}
