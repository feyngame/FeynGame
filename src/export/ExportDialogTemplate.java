package export;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.WindowConstants;

import export.ExportFormat.ExportContinuation;
import export.ExportFormat.ExportTask;
import ui.Frame;
import ui.Pane;

abstract class ExportDialogTemplate extends JDialog {
  private static final long serialVersionUID = 1L;

  protected static final Insets PRE_CATEGORY_HEAD_INSETS = new Insets(5, 5, 5, 5);
  protected static final Insets PRE_CATEGORY_ITEM_INSETS = new Insets(0, 5, 0, 5);
  protected static final Insets PRE_CATEGORY_SUBITEM_INSETS = new Insets(0, 30, 0, 5);
  protected static final Insets PRE_PROGRESSBAR_INSETS = new Insets(15, 5, 5, 5);
  protected static final Insets PRE_HEADER_INSETS = new Insets(5, 5, 0, 5);
  
  private boolean result;
  private final JProgressBar exportProgress;
  private final List<JComponent> componentsToDisable;
  private final JButton exportButton;
  private final JButton cancelButton;
  private final JToggleButton advancedButton;
  private final JLabel exportSizeSimple;
  private final JLabel exportSizeAdvanced;
  
  protected final Pane pane;
  
  private final JPanel optionsPanel;
  private final JPanel simplePanel;
  private final JPanel advancedPanel;
  
  private final boolean hasAdvancedOptions;
  
  private static final int MAX_PATH_WIDTH = 260;
  private static final int MIN_PROGRESS_HEIGHT = 20;
  
  //extra method is required because we cannot put code in the constructor before the super() call
  private static String title(String destinationOrText) {
    String ext = ExportManager.getFileExtension(destinationOrText);
    if(ext == null || ext.isEmpty()) return destinationOrText;
    else return ext;
  }
  
  protected ExportDialogTemplate(Frame owner, Pane pane, String destination) {
    super(owner, "Export Options for " + title(destination), ModalityType.APPLICATION_MODAL);
    this.componentsToDisable = new ArrayList<>();
    this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //single-use frame
    this.pane = pane;
    
    JPanel centerPanel = new JPanel(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridx = 0;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.weightx = 1.0;
    gbc.insets = PRE_HEADER_INSETS;
    
    JTextArea destArea = new JTextArea("Exporting to: " + destination);
    destArea.setBackground(centerPanel.getBackground());
    destArea.setFont(new JLabel("asdf").getFont());
    destArea.setEditable(false);
    destArea.setLineWrap(true);
    destArea.setWrapStyleWord(false);
    destArea.setFocusable(false);
    destArea.setMaximumSize(new Dimension(MAX_PATH_WIDTH, Integer.MAX_VALUE));
    centerPanel.add(destArea, gbc);
    
    optionsPanel = new JPanel(new GridBagLayout());
    optionsPanel.setBorder(BorderFactory.createTitledBorder("Export options"));
    GridBagConstraints optionsGbc = new GridBagConstraints();
    optionsGbc.gridx = 0;
    optionsGbc.fill = GridBagConstraints.HORIZONTAL;
    optionsGbc.weightx = 1.0;
    optionsGbc.insets = PRE_CATEGORY_ITEM_INSETS;
    boolean hasOptions = addContentOptions(optionsPanel, optionsGbc);
    if(hasOptions) {
      centerPanel.add(optionsPanel, gbc);
    }
    
    exportSizeSimple = new JLabel("Size:");
    simplePanel = new JPanel(new GridBagLayout());
    simplePanel.setBorder(BorderFactory.createTitledBorder("Export dimensions"));
    GridBagConstraints simpleGbc = new GridBagConstraints();
    simpleGbc.gridx = 0;
    simpleGbc.fill = GridBagConstraints.HORIZONTAL;
    simpleGbc.weightx = 1.0;
    simpleGbc.insets = PRE_CATEGORY_HEAD_INSETS;
    simplePanel.add(exportSizeSimple, simpleGbc);
    boolean hasSimple = addSimpleOptions(simplePanel, simpleGbc);
    if(hasSimple) {
      centerPanel.add(simplePanel, gbc);
    }
    
    exportSizeAdvanced = new JLabel("Size:");
    advancedPanel = new JPanel(new GridBagLayout());
    advancedPanel.setBorder(BorderFactory.createTitledBorder("Export dimensions (advanced)"));
    GridBagConstraints advancedGbc = new GridBagConstraints();
    advancedGbc.gridx = 0;
    advancedGbc.fill = GridBagConstraints.HORIZONTAL;
    advancedGbc.weightx = 1.0;
    advancedGbc.insets = PRE_CATEGORY_HEAD_INSETS;
    advancedPanel.add(exportSizeAdvanced, advancedGbc);
    boolean hasAdvanced = addAdvancedOptions(advancedPanel, advancedGbc);
    if(hasAdvanced) {
      centerPanel.add(advancedPanel, gbc);
      advancedPanel.setVisible(false);
    }
    
    
    if(hasAdvanced && !hasSimple) throw new IllegalStateException();
    hasAdvancedOptions = hasAdvanced;
    
    gbc.insets = PRE_PROGRESSBAR_INSETS;
    
    exportProgress = new JProgressBar();
    exportProgress.setValue(0);
    exportProgress.setMinimum(0);
    exportProgress.setMaximum(1);
    exportProgress.setStringPainted(true);
    exportProgress.setString("");
    exportProgress.setMinimumSize(new Dimension(exportProgress.getMinimumSize().width, MIN_PROGRESS_HEIGHT));
    centerPanel.add(exportProgress, gbc);
    
    Box buttonPanel = new Box(BoxLayout.X_AXIS);
    advancedButton = new JToggleButton("Show Advanced");
    advancedButton.setModel(ExportManager.advancedSettingsEnabled);
    advancedButton.addActionListener(this::toggleAdvanced);
    if(!hasAdvanced) advancedButton.setVisible(false);
    buttonPanel.add(advancedButton);
    buttonPanel.add(Box.createHorizontalStrut(40)); //min distance
    buttonPanel.add(Box.createHorizontalGlue());
    cancelButton = new JButton("Cancel");
    buttonPanel.add(cancelButton);
    exportButton = new JButton("Export");
    buttonPanel.add(exportButton);
    getRootPane().setDefaultButton(exportButton);
    componentsToDisable.add(exportButton);
    
    add(centerPanel, BorderLayout.CENTER);
    add(buttonPanel, BorderLayout.SOUTH);
    centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    toggleAdvanced(null);
    updateExportSizeText(null);
    setVisible(false);
    setResizable(false);
//    setMinimumSize(new Dimension(200, getMinimumSize().height));
    pack();
    setLocationRelativeTo(owner);
  }
  
  private void toggleAdvanced(ActionEvent e) {
    if(hasAdvancedOptions) {
      boolean switchToAdvanced = advancedButton.isSelected();
      advancedButton.setText(advancedButton.isSelected() ? "Show simple" : "Show advanced");
      simplePanel.setVisible(!switchToAdvanced);
      advancedPanel.setVisible(switchToAdvanced);
    } else {
      simplePanel.setVisible(true);
      advancedPanel.setVisible(false);
    }
    updateExportSizeText(null);
    pack();
  }
  
  protected void addComponentToDisable(JComponent component) {
    componentsToDisable.add(component);
  }
  
//  protected void addBoundsChangeListener(ActionListener l) {
//    autoBounds.addActionListener(l);
//    manualBounds.addActionListener(l);
//    fullBounds.addActionListener(l);
//  }
  
  protected void setExportSizeText(String text) {
    exportSizeSimple.setText("Size: " + text);
    exportSizeAdvanced.setText("Size: " + text);
  }
  
  protected Rectangle getPaneCropRectangle() {
    return pane.getDiagramBounds().getBounds();
  }
  
  protected abstract boolean addContentOptions(JPanel centerPanel, GridBagConstraints gbc);
  protected abstract boolean addSimpleOptions(JPanel centerPanel, GridBagConstraints gbc);
  protected abstract boolean addAdvancedOptions(JPanel centerPanel, GridBagConstraints gbc);
  protected void updateExportSizeText(Object anyEventOrNull) {};
  
  private void createWorker(ExportTask continuation) {
    ExportWorker worker = new ExportWorker(this) {
      
      @Override
      protected void onSuccess() {
        ExportDialogTemplate.this.hideDialog(true);
      }
      
      @Override
      protected void doWork() throws Exception {
        if(continuation != null) {
          continuation.doExport(this);
        }
      }
    };
    
    //clear the old listeners to prevent memory leak
    for(ActionListener l : exportButton.getActionListeners())
      exportButton.removeActionListener(l);
    for(ActionListener l : cancelButton.getActionListeners())
      cancelButton.removeActionListener(l);
    
    exportButton.addActionListener(e -> {
      for(JComponent c : componentsToDisable)
        c.setEnabled(false);
      worker.execute();
    });
    cancelButton.addActionListener(e -> {
      worker.cancel();
    });
  }
  
  public boolean showDialogAndWait(ExportTask task, ExportContinuation continuationSecond) {
    return showDialogAndWait(ExportTask.continueWith(task, continuationSecond));
  }
  
  public boolean showDialogAndWait(ExportTask task) {
    createWorker(task);
    result = false;
    setVisible(true);
    return result;
  }
  
  protected void hideDialog(boolean value) {
    result = value;
    setVisible(false);
  }
  
  public void setProgressBarValues(int progress, int total, String message) {
    exportProgress.setMinimum(0);
    exportProgress.setMaximum(total);
    exportProgress.setValue(progress);
    if(message == null) 
      exportProgress.setString(progress + " / " + total);
    else 
      exportProgress.setString(String.format(message, progress, total));
  }

  public void setProgressBarText(String message) {
    exportProgress.setString(message);
  }
}
