package export;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.print.AttributeException;
import javax.print.CancelablePrintJob;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.FlavorException;
import javax.print.PrintException;
import javax.print.SimpleDoc;
import javax.print.StreamPrintService;
import javax.print.StreamPrintServiceFactory;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jgrapht.alg.drawing.model.Box2D;

import amplitude.FeynmanDiagram;
import export.ExportManager.PaperUnit;
import game.FeynLine;
import game.LineConfig;
import layout.GraphLayoutException;
import qgraf.QgrafDiagram;
import qgraf.QgrafImportManager;
import resources.Java8Support;
import ui.Frame;
import ui.Pane;

public class MultiDiagramStreamPrintExportFormat extends ExportFormat {
  private static final Logger LOGGER = Frame.getLogger(MultiDiagramStreamPrintExportFormat.class);
  
  private final DocFlavor outputFlavor;
  
  protected MultiDiagramStreamPrintExportFormat(String description, DocFlavor outputFlavor, String...allExtensions) {
    super(new FileNameExtensionFilter(description, allExtensions));
    if(allExtensions.length == 0) throw new IllegalArgumentException();
    if(!outputFlavor.getRepresentationClassName().equals(byte[].class.getName())) throw new IllegalArgumentException();
    this.outputFlavor = outputFlavor;
  }
  
  @Override
  public ExportDialogTemplate createDialog(Frame parent, String displayDestination) {
    return new ExportDialog(parent, displayDestination);
  }

  @Override
  public ExportResult doAutoExport(Pane pane, Path destination, ExportProgressUpdate update) throws IOException, ExportException {
    boolean printCustomSize =
        !ExportManager.advancedSettingsEnabled.isSelected() ||
        !ExportManager.radioButtonPaperSizeV.isSelected();
    update.displayExportProgress(0, 2, "Prepearing");
    
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    StreamPrintServiceFactory[] spsfs = StreamPrintServiceFactory.lookupStreamPrintServiceFactories(
        printCustomSize ? DocFlavor.SERVICE_FORMATTED.PAGEABLE : DocFlavor.SERVICE_FORMATTED.PRINTABLE, outputFlavor.getMimeType());
    if(spsfs.length == 0) throw new ExportException("Unexpected error: cannot find stream print service for output type " + outputFlavor.getMimeType());
    StreamPrintServiceFactory spsf = spsfs[0];
    
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    try(OutputStream os = Files.newOutputStream(destination)) {
      update.displayExportProgress(1, 2, "Printing to file");
      StreamPrintService sps = spsf.getPrintService(os);
      DocPrintJob dpj = sps.createPrintJob();
      ExportPrintable ep = new ExportPrintable(pane, update);
      Doc doc;
      if(printCustomSize) {
        PageFormat pf = StreamPrintExportFormat.createPaper(pane.getDiagramBounds());
        doc = ep.createDocCustomPage(pf, null);
      } else {
        MediaSizeName paper = (MediaSizeName) ExportManager.comboBoxPaperSize.getSelectedItem();
        OrientationRequested orientation = ExportManager.checkboxLandscape.isSelected() ?
            OrientationRequested.LANDSCAPE : OrientationRequested.PORTRAIT;
        doc = ep.createDocPresetPage(paper, orientation, null);
      } 
      
      if(cancellationRequested()) return ExportResult.CANCELED;
            
      //Then wait for the job to be completed
      try {
        AwaitablePrintJobListener.printAndWait(dpj, doc, null, 1, TimeUnit.MINUTES);
      } catch (PrintException e) {
        if(e instanceof FlavorException) {
          throw new ExportException("Unexpected error: Stream print service does not support the selected output type");
        } else if(e instanceof AttributeException) {
          throw new ExportException("Unexpected error: Stream print service does not support a required attribute");
        } else {
          throw new ExportException("Failed to submit job to stream print service");
        }
      } catch (InterruptedException e) {
        //This is a cancellation request
        if(dpj instanceof CancelablePrintJob) {
          try {
            ((CancelablePrintJob) dpj).cancel();
          } catch (PrintException e1) {
            throw new ExportException("Failed to cancel.");
          }
        } else {
          //Nothing we can do about it
        }
      } catch (TimeoutException e) {
        //The user will probably cancel the job long before this happens, but we should not wait forever
        throw new ExportException("Export took too long (more than one minute). The process was not explicitly aborted any may still complete in the background.");
      }
      update.displayExportProgress(2, 2, "Finished");
      
      //no content that could be converted by further stages (cancelled is not accurate, but null is not allowed)
      if(ep.result == null) return ExportResult.CANCELED;
      return ep.result;
    }
  }
  
  @Override
  public boolean isSecret() {
    return false;
  }
  
  private static class ExportPrintable implements Printable {
    private final int colsPerPage;
    private final int rowsPerPage;
    protected final int pageCount;
    private final ExportProgressUpdate update;
    private final Pane original;
    private ExportResult result;
    private final String formatString;
    private final int totalDc;
    
    public ExportPrintable(Pane pane, ExportProgressUpdate update) {
      this.original = Objects.requireNonNull(pane);
      this.update = Objects.requireNonNull(update);
      this.colsPerPage = ExportManager.multiColCount.getNumber().intValue();
      this.rowsPerPage = ExportManager.multiRowCount.getNumber().intValue();
      if(colsPerPage <= 0 || rowsPerPage <= 0) throw new IllegalStateException();
      
      this.totalDc = pane.getCurretQgrafFile().diagrams().size();
      int dpp = ExportManager.multiRowCount.getNumber().intValue() * ExportManager.multiColCount.getNumber().intValue();
      this.pageCount = Java8Support.ceilDiv(totalDc, dpp);
      this.formatString = ExportManager.multiEnableFormatString.isSelected() ? ExportManager.formatString : null;
    }
    
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
      if(pageIndex >= pageCount) return NO_SUCH_PAGE;
      
      Pane pane = ExportManager.copyAndCropPane(original);
      Dimension paneSize = pane.getSize();
      Graphics2D g2d = (Graphics2D) graphics.create();
      
      double pWidth = pageFormat.getImageableWidth() / colsPerPage;
      double pHeight = pageFormat.getImageableHeight() / rowsPerPage;
      
      /*
       * The diagram will fit into the area; but the scale will be wrong: The wavy part of the line will be too large.
       * Instead; the natural scale of the diagram is the bounding box provided in the constructor.
       */
      double xScale = paneSize.width / pWidth;
      double yScale = paneSize.height / pHeight;
      double xyScale = Math.min(xScale, yScale); //Find a common scale to keep aspect ratio
      
      g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY()); //pt translations
      g2d.scale(1.0 / xyScale, 1.0 / xyScale);
      
      double dWidth = pWidth * xyScale;
      double dHeight = pHeight * xyScale;
      
      int dc = pageIndex * rowsPerPage * colsPerPage;
      for(int i = 0; i < colsPerPage; i++) {
        for(int j = 0; j < rowsPerPage; j++) {
          Graphics2D g2d2 = (Graphics2D) g2d.create(); //Independent offset for each diagram
          g2d2.translate(dWidth*i, dHeight*j);
          if(!selectDiagram(pane, dc, dWidth, dHeight)) continue;
          pane.print(g2d2);
          if(formatString != null) {
            String toPrint = String.format(formatString, dc + 1, totalDc);
            float fs = (float)(72.0 * 0.05 * pane.getHeight() / Toolkit.getDefaultToolkit().getScreenResolution());
            g2d2.setFont(g2d2.getFont().deriveFont(fs));
            Rectangle2D b = g2d2.getFont().getStringBounds(toPrint, g2d2.getFontRenderContext());
            g2d2.drawString(toPrint, 0, (int)b.getHeight());
          }
          g2d2.dispose();
          dc++;
        }
      }
      
      update.displayExportProgress(pageIndex, pageCount, "Rendering Page " + (pageIndex+1) + "/" + pageCount);
      
      g2d.dispose();
      if(result == null) { //The layout for every page is the same, so just calculate it for the first one
        result = new ExportResult(pageFormat.getWidth(), pageFormat.getHeight(), pageFormat.getImageableX(),
            pageFormat.getImageableY(), pageFormat.getImageableWidth(), pageFormat.getImageableHeight(), pane.getSize(), true);
      }
      return PAGE_EXISTS;
    }

    private boolean selectDiagram(Pane pane, int index, double width, double height) {
      pane.clearPane(Frame.CLEARMODE_CLEAR_ALL);
      pane.setSize((int) Math.ceil(width), (int) Math.ceil(height));
      pane.mBB = false; //use natural bounds for layout and export
      if(index < 0 || index >= pane.getCurretQgrafFile().diagrams().size()) return false;
      QgrafDiagram current = pane.getCurretQgrafFile().diagrams().get(index);
      List<LineConfig> forceModel = pane.getCurretForceModel();
      try {
        current.clearPositionData();
        current.applySelectedLayout(new Box2D(width, height));
      } catch (GraphLayoutException e) {
        LOGGER.log(Level.SEVERE, "Cannot print diagram with index " + index, e);
        return false; //Will simply leave this spot blank in the file
      }
      
      pane.putDiagramData(current, true, true);
      
      Map<Map.Entry<String, String>, LineConfig> idToStyle = new HashMap<>();
      if(forceModel != null) {
        for(LineConfig lc : forceModel) {
          if(lc.identifier != null) {
            idToStyle.put(lc.identifier, lc);
          }
        }
      }
      
      boolean printMom = ExportManager.multiEnableMomenta.isSelected();
      boolean deleteMom = !QgrafImportManager.loadMomentumArrows();
      boolean labelsOn = ExportManager.multiEnableLabels.isSelected();
      boolean labelsOff = ExportManager.multiDisableLabels.isSelected();
      boolean hasAllMom = true;
      for(FeynLine fl : pane.lines) {
        if(deleteMom && !printMom) fl.momArrow = null;
        
        Map.Entry<String, String> id = fl.lineConfig.identifier;
        LineConfig newStyle = idToStyle.get(id);
        if(newStyle != null) {
          fl.changeConfigTo(newStyle);
        }  
        
        if(labelsOn) {
          fl.showDesc = true;
        } else if(labelsOff) {
          fl.showDesc = false;
        }
        hasAllMom &= fl.momArrow != null;
      }
      
      if(printMom) {
        if(hasAllMom) {
          for(FeynLine fl : pane.lines) {
            fl.momArrow.drawMomentum = true;
          }
        } else {
          FeynmanDiagram fd = new FeynmanDiagram(pane.lines);
          fd.distributeMomenta();
        }
      }
      
      
      if(ExportManager.checkboxBlackWhite.isSelected()) pane.bw();
      return true;
    }
    
    public Doc createDocPresetPage(MediaSizeName msn, OrientationRequested orq, DocAttributeSet otherAttrs) {
      DocAttributeSet das = new HashDocAttributeSet(otherAttrs); //ensure mutable set
      das.add(msn);
      das.add(orq);
      return new SimpleDoc(this, DocFlavor.SERVICE_FORMATTED.PRINTABLE, das);
    }
    
    public Doc createDocCustomPage(PageFormat format, DocAttributeSet otherAttrs) {
      Pageable p = new Pageable() {
        @Override
        public Printable getPrintable(int pageIndex) throws IndexOutOfBoundsException {
          if(pageIndex < 0 || pageIndex >= pageCount) throw Java8Support.newIndexOutOfBoundsException(pageIndex);
          return ExportPrintable.this;
        }
        @Override
        public PageFormat getPageFormat(int pageIndex) throws IndexOutOfBoundsException {
          if(pageIndex < 0 || pageIndex >= pageCount) throw Java8Support.newIndexOutOfBoundsException(pageIndex);
          return format;
        }
        @Override
        public int getNumberOfPages() {
          return pageCount;
        }
      };
      return new SimpleDoc(p, DocFlavor.SERVICE_FORMATTED.PAGEABLE, otherAttrs);
    }
  }
  
  private static class ExportDialog extends StreamPrintExportFormat.ExportDialog {
    private static final long serialVersionUID = 1L;

    protected ExportDialog(Frame owner, String destination) {
      super(owner, destination);
    }

    @Override
    protected boolean addContentOptions(JPanel centerPanel, GridBagConstraints gbc) {
      super.addContentOptions(centerPanel, gbc);
      //add page boxes
      JSpinner rows = new JSpinner(ExportManager.multiRowCount);
      JSpinner cols = new JSpinner(ExportManager.multiColCount);
      
      JPanel rcbox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      gbc2.weightx = 0.0;
      rcbox.add(new JLabel("Rows per page", JLabel.LEFT), gbc2);
      gbc2.gridy = 1;
      rcbox.add(new JLabel("Columns per page", JLabel.LEFT), gbc2);
      gbc2.gridy = 0;
      gbc2.gridx = 1;
      gbc2.weightx = 1.0;
      rcbox.add(rows, gbc2);
      gbc2.gridy = 1;
      rcbox.add(cols, gbc2);
      addComponentToDisable(rows);
      addComponentToDisable(cols);
      rows.addChangeListener(this::updateExportSizeText);
      cols.addChangeListener(this::updateExportSizeText);
      centerPanel.add(rcbox, gbc);
      
      JCheckBox labels = new JCheckBox("Enable line labels");
      labels.setModel(ExportManager.multiEnableLabels);
      addComponentToDisable(labels);
      centerPanel.add(labels, gbc);
      JCheckBox momenta = new JCheckBox("Enable momenta");
      momenta.setModel(ExportManager.multiEnableMomenta);
      addComponentToDisable(momenta);
      centerPanel.add(momenta, gbc);
      
      JTextField formatString = new JTextField(ExportManager.formatString);
      JCheckBox labelDiagrams = new JCheckBox("Print diagram labels");
      labelDiagrams.setModel(ExportManager.multiEnableFormatString);
      JPanel fbox = new JPanel(new GridBagLayout());
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.weightx = 0.0;
      fbox.add(new JLabel("Format string:"), gbc2);
      gbc2.gridx = 1;
      gbc2.weightx = 1.0;
      fbox.add(formatString, gbc2);
      
      formatString.setEnabled(labelDiagrams.isSelected());
      labelDiagrams.addActionListener(e -> {
        formatString.setEnabled(labelDiagrams.isSelected());
      });
      formatString.getDocument().addDocumentListener((DocumentChangeListener) e -> {
        ExportManager.formatString = formatString.getText();
      });
      addComponentToDisable(labelDiagrams);
      addComponentToDisable(formatString);
      
      centerPanel.add(labelDiagrams, gbc);
      centerPanel.add(fbox, gbc);
      
      return true;
    }

    @Override
    protected void updateExportSizeText(Object anyEventOrNull) {
      double[] wh = StreamPrintExportFormat.getSelectedPaperSize(getPaneCropRectangle());
      PaperUnit u = (PaperUnit) ExportManager.comboBoxPaperUnit.getSelectedItem();
      String ws = NumberFormat.getNumberInstance().format(wh[0]);
      String hs = NumberFormat.getNumberInstance().format(wh[1]);
      
      int diagrams = pane.getCurretQgrafFile().diagrams().size();
      int dpp = ExportManager.multiRowCount.getNumber().intValue() * ExportManager.multiColCount.getNumber().intValue();
      int pages = Java8Support.ceilDiv(diagrams, dpp);
      
      setExportSizeText(ws + u.getDisplayName() + " by " + hs + u.getDisplayName() + "; " + pages + " pages");
    }
  }
  
  private static interface DocumentChangeListener extends DocumentListener {
    public void handleAnyChange(DocumentEvent e);

    @Override
    public default void insertUpdate(DocumentEvent e) {
      handleAnyChange(e);
    }

    @Override
    public default void removeUpdate(DocumentEvent e) {
      handleAnyChange(e);
    }

    @Override
    public default void changedUpdate(DocumentEvent e) {
      handleAnyChange(e);
    }
    
    
  }
}
