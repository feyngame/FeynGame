package export;

import java.awt.geom.Dimension2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import ui.Frame;
import ui.Pane;

/**
 * Contains code to export a draw pane to a specific format
 */
public abstract class ExportFormat {
  private static final Logger LOGGER = Frame.getLogger(ExportFormat.class);
  
  public static interface ExportProgressUpdate {
    public void displayExportProgress(int progress, int total, String message);
    public void displayTextMessage(String message);
    
    public default void displayExportProgress(int progress, int total) {
      displayExportProgress(progress, total, null);
    }
    
    public static final ExportProgressUpdate PRINT_TO_LOGGER = new PrintStreamExportProgressUpdate(LOGGER, Level.INFO);
  }
  
  public static final class PrintStreamExportProgressUpdate implements ExportProgressUpdate {

    private final Consumer<String> outstream;
    public PrintStreamExportProgressUpdate(PrintStream outstream) {
      this(outstream::println);
    }

    public PrintStreamExportProgressUpdate(Logger logger, Level level) {
      this(s -> logger.log(level, s));
    }
    
    public PrintStreamExportProgressUpdate(Consumer<String> outstream) {
      this.outstream = Objects.requireNonNull(outstream);
    }

    @Override
    public void displayExportProgress(int progress, int total, String message) {
      if(message == null)
        outstream.accept("Export progress: " + progress + " / " + total);
      else
        outstream.accept("Export progress: " + String.format(message, progress, total));
    }

    @Override
    public void displayTextMessage(String message) {
      outstream.accept("Export: " + message);
    }
  }
  
  @FunctionalInterface
  public static interface ExportTask {
    public ExportResult doExport(ExportProgressUpdate update) throws IOException, InterruptedException, ExportException;
    
    public static ExportTask continueWith(ExportTask a, ExportContinuation b) {
      if(a == null) {
        throw new NullPointerException();
      } else if(b == null) {
        return a;
      } else {
        return (u) -> {
          ExportResult n = a.doExport(u);
          if(n.isCanceled()) {
            return n;
          } else { 
            return b.continueExport(n, u);
          }
        };
      }
    }
  }
  
  @FunctionalInterface
  public static interface ExportContinuation {
    public ExportResult continueExport(ExportResult previousStageResult, ExportProgressUpdate update) throws IOException, InterruptedException, ExportException;
    
    public static ExportContinuation combine(ExportContinuation a, ExportContinuation b) {
      if(a == null && b == null) {
        return null;
      } else if(a == null) {
        return b;
      } else if(b == null) {
        return a;
      } else {
        return (p, u) -> {
          ExportResult n = a.continueExport(p, u);
          if(n.isCanceled()) {
            return n;
          } else { 
            return b.continueExport(n, u);
          }
        };
      }
    }
  }

  public static class ExportResult {
    private final boolean cancelled;
    private final Dimension2D fullCanvasSize; //full canvas/paper size of the exported file
    private final Rectangle2D diagramArea; //the part of the canvas that is occupied by the diagram
    private final Dimension2D croppedPaneSize;
    private final boolean multipage;
    
    public static final ExportResult CANCELED = new ExportResult();
    
    protected ExportResult(Dimension2D bbox, Dimension2D croppedPaneSize, boolean multipage) {
      LOGGER.finer("Export Result: bbox=" + bbox + ", pane=" + croppedPaneSize + ", multipage=" + multipage);
      this.fullCanvasSize = bbox;
      this.diagramArea = new Rectangle2D.Double(0, 0, bbox.getWidth(), bbox.getHeight());
      this.cancelled = false;
      this.multipage = multipage;
      this.croppedPaneSize = croppedPaneSize;
    }
    
    protected ExportResult(Dimension2D bbox, Rectangle2D area, Dimension2D croppedPaneSize, boolean multipage) {
      checkRectWithTolerance(bbox, area);
      this.fullCanvasSize = bbox;
      this.diagramArea = area;
      this.cancelled = false;
      this.multipage = multipage;
      this.croppedPaneSize = croppedPaneSize;
    }
    
    protected ExportResult(double canvasWidth, double canvasHeight, double offsetX, double offsetY,
        double diagramAreaWidth, double diagramAreaHeight, Dimension2D croppedPaneSize, boolean multipage) {
      this(new Dimension2DDouble(canvasWidth, canvasHeight),
          new Rectangle2D.Double(offsetX, offsetY, diagramAreaWidth, diagramAreaHeight), croppedPaneSize, multipage);
    }
    
    private static final double TOLERANCE = 0.01;
    private void checkRectWithTolerance(Dimension2D bounds, Rectangle2D rect) {
      double x = rect.getX();
      double y = rect.getY();
      double w = rect.getWidth();
      double h = rect.getHeight();
      double bw = bounds.getWidth();
      double bh = bounds.getHeight();
      
      if((x < 0 && -x > TOLERANCE) ||
         (y < 0 && -y > TOLERANCE) ||
         (x + w > bw && x + w - bw > TOLERANCE) ||
         (y + h > bh && y + h - bh > TOLERANCE)){
        LOGGER.warning("Export area out of bounds (bounding box [" + bw + "," + bh + 
            "] does not contain the area covered by the diagram [" + x + "," + y + "," + w + "," + h + "])");
      }
    }
    
    private ExportResult() {
      this.fullCanvasSize = null;
      this.diagramArea = null;
      this.cancelled = true;
      this.multipage = false;
      this.croppedPaneSize = null;
    }
    
    public Dimension2D getCroppedPaneSize() {
      return croppedPaneSize;
    }
    
    public Dimension2D getCanvasSize() {
      return fullCanvasSize;
    }
    
    public Rectangle2D getBoundingBox() {
      return diagramArea;
    }
    
    public boolean isCanceled() {
      return cancelled;
    }
    
    public boolean isMultipage() {
      return multipage;
    }

    @Override
    public String toString() {
      return "ExportResult [cancelled=" + cancelled + ", fullCanvasSize=" + fullCanvasSize + ", diagramArea="
          + diagramArea + ", croppedPaneSize=" + croppedPaneSize + ", multipage=" + multipage + "]";
    }
  }
  
  //Why is this class not part of java???
  private static final class Dimension2DDouble extends Dimension2D {
    private double w, h;

    public Dimension2DDouble(double width, double height) {
      w = width;
      h = height;
    }

    @Override
    public double getWidth() {
      return w;
    }

    @Override
    public double getHeight() {
      return h;
    }

    @Override
    public void setSize(double width, double height) {
      w = width;
      h = height;
    }

    @Override
    public String toString() {
      return "Dimension2DDouble [w=" + w + ", h=" + h + "]";
    }
  }
  
  public static final class ExportException extends Exception {
    private static final long serialVersionUID = 1L;
    
    public ExportException(String message) {
      super(message);
    }
  }
  
  /////////////////////////////// START MEMBERS //////////////////////////////////////
  
  private final FileNameExtensionFilter fnef;

  protected ExportFormat(FileNameExtensionFilter fnef) {
    this.fnef = Objects.requireNonNull(fnef);
  }
  
  public final FileNameExtensionFilter getFileFilter() {
    return fnef;
  }

  public final boolean isExportFormatFor(String extension) {
    for(String acc : fnef.getExtensions())
      if(extension.equalsIgnoreCase(acc)) return true;
    return false;
  }
  
  public String getPreferredExtension() {
    return fnef.getExtensions()[0];
  }
  
  //Support for showing the overwrite prompt for files that are not selected in the file chooser (e.g. the eps file for tex export)
  public String[] getAdditionalExportedExtensions() {
    return new String[0];
  }
  
  public final void doGuiExport(Frame parent, Path destination, ExportContinuation continuation) {
    LOGGER.info("Starting diagram export to \"" + destination.toString() + "\" in format \"" + getPreferredExtension() + "\"");
    Frame.setStatusText("Exporting diagram to " + destination.toString());
    doGuiExport(parent, destination.toAbsolutePath().toString(), destination, continuation);
  }
  
  public void doGuiExport(Frame parent, String displayDestination, Path destination, ExportContinuation continuation) {
    if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException();
    LOGGER.fine("Starting diagram export to \"" + destination.toString() + "\" (possibly a temporary file) in format \"" + getPreferredExtension() + "\"");
    ExportDialogTemplate dialog = createDialog(parent, displayDestination);
    dialog.showDialogAndWait(u -> doAutoExport(parent.getDrawPane(), destination, u), continuation);
  }
  
  public abstract ExportDialogTemplate createDialog(Frame parent, String displayDestination);
  
  public final ExportResult doAutoExport(Pane pane, Path destination) throws IOException, ExportException {
    return doAutoExport(pane, destination, ExportProgressUpdate.PRINT_TO_LOGGER);
  }
  
  public abstract ExportResult doAutoExport(Pane pane, Path destination, ExportProgressUpdate update) throws IOException, ExportException;
  
  public abstract boolean isSecret();
  
  protected static boolean cancellationRequested() {
    return Thread.interrupted();
  }
}
