package export;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import resources.Java8Support;
import ui.Frame;
import ui.Pane;

public class ExternalConverterExportFormat extends ExportFormat {
  private static final Logger LOGGER = Frame.getLogger(ExternalConverterExportFormat.class);
  
  @FunctionalInterface
  public static interface ExternalConverter {
    
    public ProcessBuilder buildExternalProcess(Path sourcePath, Path destPath);
    
    public default Future<Integer> startExternalProcess(Path sourcePath, Path destPath) throws IOException {
      ProcessBuilder pb = buildExternalProcess(sourcePath, destPath);
      String name = pb.command().get(0);
      //TODO redirect somewhere else when the logger is implemented
      pb.inheritIO();
      LOGGER.info("Starting '" + name + "' process ...");
      Process p = pb.start();
      CompletableFuture<Process> f = Java8Support.onExit(p);
      f.thenRun(() -> {
        LOGGER.info("... '" + name + "' process completed");
      });
      
      return new Future<Integer>() { //override cancel method to destroy the process
        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
          f.cancel(true);
          LOGGER.info("... Destroying '" + name + "' process (export cancelled)");
          p.destroyForcibly();
          return true;
        }

        @Override
        public boolean isCancelled() {
          return f.isCancelled();
        }

        @Override
        public boolean isDone() {
          return f.isDone();
        }

        @Override
        public Integer get() throws InterruptedException, ExecutionException {
          try {
            return f.get().exitValue();
          } catch (InterruptedException e) {
            //Interruptedexception occurs if the swingworker is cancelled
            cancel(true);
            throw e;
          }
        }

        @Override
        public Integer get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
          try {
            return f.get(timeout, unit).exitValue();
          } catch (InterruptedException e) {
            cancel(true);
            throw e;
          }
        }
      };
    }
  }
  
  /**
   * Constructs a new converter for an executable program that accepts the input and output paths
   * immediately after the executable name, with no further parameters. 
   * @param executableName The name of the executable
   * @return An {@link ExternalConverter} that will run that executable.
   */
  public static ExternalConverter converterForPathExecutable(String executableName) {
    return (in, out) -> new ProcessBuilder(executableName, in.toAbsolutePath().toString(), out.toAbsolutePath().toString());
  }
  
  private final ExportFormat exportDelegate;
  private final ExternalConverter converter;
  
  ExternalConverterExportFormat(String description, ExportFormat delegate, ExternalConverter converter, String...allExtensions) {
    super(new FileNameExtensionFilter(description, allExtensions));
    this.exportDelegate = Objects.requireNonNull(delegate);
    this.converter = Objects.requireNonNull(converter);
  }

  @Override
  public void doGuiExport(Frame parent, String displayDestination, Path destination, ExportFormat.ExportContinuation continuation) {
    Path tempFile;
    try {
      tempFile = ExportManager.getTempFile(exportDelegate.getPreferredExtension());
    } catch (IOException e) {
      JOptionPane.showMessageDialog(parent, "Error: cannot create temporary file: " + e.getMessage(), "Export error", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    exportDelegate.doGuiExport(parent, displayDestination, tempFile, ExportContinuation.combine(
        (r, u) -> doPdfConversion(r, tempFile, destination, u, true), continuation));
  }
  
  @Override
  public ExportDialogTemplate createDialog(Frame parent, String displayDestination) {
    throw new UnsupportedOperationException(); //Don't need this since doGuiExport() is implemented differently
  }
  
  @Override
  public ExportResult doAutoExport(Pane content, Path destination, ExportProgressUpdate update)
      throws IOException, ExportException {
    Path tempFile = ExportManager.getTempFile(exportDelegate.getPreferredExtension());
    ExportResult prevResult = exportDelegate.doAutoExport(content, tempFile, update);
    return doPdfConversion(prevResult, tempFile, destination, update, true);
  }
  
  private ExportResult doPdfConversion(ExportResult prevResult, Path source, Path destination, ExportProgressUpdate update, boolean deleteSourceFile) throws IOException, ExportException {
    if(prevResult.isCanceled()) return ExportResult.CANCELED; //Cancel immediately

    update.displayExportProgress(1, 1);
    update.displayTextMessage("Converting to " + getPreferredExtension());

    Future<Integer> f = converter.startExternalProcess(source, destination);
    try {
      int i = f.get(); //wait for the thread to complete, either success or cancelled
      if(i != 0) {
        throw new ExportException("Error: ps2pdf failed with exit code " + i);
      }
    } catch (CancellationException e) {
      //Probably will not happen, since cancellation goes through interrupts
      throw new ExportException("Unexpectd error: Canceled by external source");
    } catch (ExecutionException e) {
      Throwable t = e.getCause();
      if(t instanceof IOException) throw (IOException) t;
      else if(t instanceof ExportException) throw (ExportException) t;
      else if(t instanceof RuntimeException) throw (RuntimeException) t;
      else if(t instanceof Error) throw (Error) t;
      else throw new RuntimeException(t);
    } catch (InterruptedException e) {
      f.cancel(true); //Cancel requested by user button press -> will terminate process
      return ExportResult.CANCELED;
    } finally {
      if(deleteSourceFile) {
        Files.delete(source);
      }
    }
    return prevResult; //BBox will not change by EPS conversion    
  }
  
  @Override
  public boolean isSecret() {
    return exportDelegate.isSecret();
  };
  
}
