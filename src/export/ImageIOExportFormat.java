package export;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import export.ExportManager.DpiUnit;
import ui.ChainToggleButton;
import ui.Frame;
import ui.Pane;

public class ImageIOExportFormat extends ExportFormat {
  
  private final boolean supportsTransparency;

  ImageIOExportFormat(String description, boolean transparent, String...allExtensions) {
    super(new FileNameExtensionFilter(description, allExtensions));
    this.supportsTransparency = transparent;
   }

  @Override
  public ExportDialogTemplate createDialog(Frame parent, String displayDestination) {
    return new ExportDialog(parent, displayDestination);
  }
  
  @Override
  public ExportResult doAutoExport(Pane original, Path destination, ExportProgressUpdate update) throws IOException, ExportException {
    if(cancellationRequested()) return ExportResult.CANCELED;
    update.displayExportProgress(0, 2, "Rendering image");
    
    boolean transparent = supportsTransparency && ExportManager.checkboxTransparent.isSelected();
    Pane pane = ExportManager.copyAndCropPane(original);
    pane.noAA = transparent;
    pane.setOpaque(!transparent);
    
    if(cancellationRequested()) return ExportResult.CANCELED;

    Dimension canvasSize = pane.getSize();
    Dimension imageSize = getOutputPixelSize(canvasSize);
    double xScale =  imageSize.getWidth() / canvasSize.getWidth();
    double yScale =  imageSize.getHeight() / canvasSize.getHeight();
    
    double scale = Math.min(xScale, yScale);
    
    double drawnX = canvasSize.getWidth() * scale;
    double drawnY = canvasSize.getHeight() * scale;
    double shiftX = (imageSize.getWidth() - drawnX) / 2.0;
    double shiftY = (imageSize.getHeight() - drawnY) / 2.0;
    
    AffineTransform at = new AffineTransform();
    at.translate(shiftX, shiftY);
    at.scale(scale, scale);
    
    if(imageSize.width <= 0) throw new ExportException("Invalid image width: " + imageSize.width + "px: Cannot be zero or negative.");
    if(imageSize.height <= 0) throw new ExportException("Invalid image height: " + imageSize.height + "px: Cannot be zero or negative.");
    
    BufferedImage bi = new BufferedImage(imageSize.width, imageSize.height, 
        transparent ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);
    Graphics2D g = bi.createGraphics();
    
    if(!transparent) {
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, imageSize.width, imageSize.height);
    }
    
    g.transform(at);
    pane.print(g);
    g.dispose();
    
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    Image image = bi;
    
    if (transparent) {
      image = Frame.makeColorTransparent(bi);
    }
    
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    g = bi.createGraphics();
    g.setComposite(AlphaComposite.Src);
    g.drawImage(image, 0, 0, null);
    g.dispose();
    
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    update.displayExportProgress(1, 2, "Writing file");
    String formatExtension = getPreferredExtension();
    boolean success = ImageIO.write(bi, formatExtension, destination.toFile());
    if(!success) throw new IOException("No writer for this image format: " + formatExtension);
    update.displayExportProgress(2, 2, "Done");
    
    return new ExportResult(imageSize.getWidth(), imageSize.getHeight(), shiftX, shiftY, drawnX, drawnY, pane.getSize(), false);
  }
  
  @Override
  public boolean isSecret() {
    return false;
  }
  
  public class ExportDialog extends ExportDialogTemplate { 
    private static final long serialVersionUID = 1L;
    
    public ExportDialog(Frame owner, String destination) {
      super(owner, owner.getDrawPane(), destination);
    }

    @Override
    protected boolean addContentOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      JCheckBox blackWhite = new JCheckBox("Export in black/white");
      blackWhite.setModel(ExportManager.checkboxBlackWhite);
      addComponentToDisable(blackWhite);
      centerPanel.add(blackWhite, gbc);
      if(supportsTransparency) {
        JCheckBox transparent = new JCheckBox("Transparent background");
        transparent.setModel(ExportManager.checkboxTransparent);
        addComponentToDisable(transparent);
        centerPanel.add(transparent, gbc);
      }
      return true;
    }

    @Override
    protected boolean addSimpleOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      
      JPanel resolutionBox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      resolutionBox.add(new JLabel("Resolution scale:"), gbc2);
      gbc2.gridx = 1;
      gbc2.weightx = 1.0;
      JSpinner scaleCanvas = new JSpinner(ExportManager.spinnerResolutionScale);
      resolutionBox.add(scaleCanvas, gbc2);
      addComponentToDisable(scaleCanvas);
      gbc2.gridx = 2;
      gbc2.weightx = 0.0;
      resolutionBox.add(Box.createHorizontalStrut(50), gbc);
      centerPanel.add(resolutionBox, gbc);
      scaleCanvas.addChangeListener(this::updateExportSizeText);
      return true;
    }
    
    @Override
    protected boolean addAdvancedOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      JRadioButton pixelSize = new JRadioButton("Exact size");
      pixelSize.setModel(ExportManager.radioButtonPixelSize);
      addComponentToDisable(pixelSize);
      centerPanel.add(pixelSize, gbc);
      //TODO pixel size
      gbc.insets = PRE_CATEGORY_SUBITEM_INSETS;
      JPanel pixelBox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      gbc2.fill = GridBagConstraints.NONE;
      gbc2.insets = new Insets(1, 1, 1, 1);
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      pixelBox.add(new JLabel("Width:"), gbc2);
      gbc2.gridy = 1;
      pixelBox.add(new JLabel("Height:"), gbc2);
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.weightx = 1;
      gbc2.gridx = 1;
      gbc2.gridy = 0;
      JSpinner pixelX = new JSpinner(ExportManager.spinnerPixelX);
      addComponentToDisable(pixelX);
      pixelBox.add(pixelX, gbc2);
      gbc2.gridy = 1;
      JSpinner pixelY = new JSpinner(ExportManager.spinnerPixelY);
      addComponentToDisable(pixelY);
      pixelBox.add(pixelY, gbc2);
      gbc2.fill = GridBagConstraints.NONE;
      gbc2.weightx = 0;
      gbc2.gridx = 2;
      gbc2.gridy = 0;
      pixelBox.add(new JLabel("px"), gbc2);
      gbc2.gridy = 1;
      pixelBox.add(new JLabel("px"), gbc2);
      gbc2.gridx = 3;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.BOTH;
      gbc2.gridheight = 2;
      JToggleButton chainDimensions = new ChainToggleButton(ExportManager.chainExportRatio);
      addComponentToDisable(chainDimensions);
      pixelBox.add(chainDimensions, gbc2);
      gbc2.fill = GridBagConstraints.NONE;
      gbc2.gridx = 4;
      pixelX.addChangeListener(e -> {
        if(ExportManager.chainExportRatio.isSelected()) {
          Dimension size = getPaneCropRectangle().getSize();
          double ratio = size.getWidth() / size.getHeight();
          //X was typed, so adjust y
          double y = ExportManager.spinnerPixelX.getNumber().doubleValue() / ratio;
          ExportManager.spinnerPixelY.setValue((int) Math.round(y));
        }
      });
      pixelY.addChangeListener(e -> {
        if(ExportManager.chainExportRatio.isSelected()) {
          Dimension size = getPaneCropRectangle().getSize();
          double ratio = size.getWidth() / size.getHeight();
          double x = ExportManager.spinnerPixelY.getNumber().doubleValue() * ratio;
          ExportManager.spinnerPixelX.setValue((int) Math.round(x));
        }
      });
      chainDimensions.addActionListener(e -> {
        if(ExportManager.chainExportRatio.isSelected()) {
          //TODO decide whether X or Y should change when enabling the chain
          Dimension size = getPaneCropRectangle().getSize();
          double ratio = size.getWidth() / size.getHeight();
          double y = ExportManager.spinnerPixelX.getNumber().doubleValue() / ratio;
          ExportManager.spinnerPixelY.setValue((int) Math.round(y));
        }
      });
      centerPanel.add(pixelBox, gbc);
      
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      JRadioButton paperSize = new JRadioButton("From Paper size");
      paperSize.setModel(ExportManager.radioButtonPaperSizeR);
      addComponentToDisable(paperSize);
      centerPanel.add(paperSize, gbc);
      
      gbc.insets = PRE_CATEGORY_SUBITEM_INSETS;
      JPanel paperBox = new JPanel(new GridBagLayout());
      gbc2 = new GridBagConstraints();
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      JLabel paperSizeLabel = new JLabel("Paper:");
      paperSizeLabel.setHorizontalAlignment(SwingConstants.LEFT);
      paperBox.add(paperSizeLabel, gbc2);
      gbc2.gridy = 1;
      JLabel pixelDensityLabel = new JLabel("Pixel density:");
      pixelDensityLabel.setHorizontalAlignment(SwingConstants.LEFT);
      paperBox.add(pixelDensityLabel, gbc2);
      gbc2.gridx = 1;
      gbc2.gridy = 0;
      gbc2.weightx = 1.0;
      gbc2.gridwidth = 2;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      JComboBox<MediaSizeName> paper = new JComboBox<>(ExportManager.comboBoxPaperSize);
      paper.setRenderer(ExportManager.rendererPaperSize);
      addComponentToDisable(paper);
      paperBox.add(paper, gbc2);
      gbc2.gridwidth = 1;
      gbc2.gridy = 1;
      JSpinner dpiNum = new JSpinner(ExportManager.spinnerDpi);
      addComponentToDisable(dpiNum);
      dpiNum.setMinimumSize(new Dimension(30, 10));
      dpiNum.setPreferredSize(new Dimension(60, 20));
      dpiNum.setMaximumSize(new Dimension(60, Integer.MAX_VALUE));
      paperBox.add(dpiNum, gbc2);
      gbc2.gridx = 2;
      gbc2.fill = GridBagConstraints.NONE;
      gbc2.weightx = 0.0;
      JComboBox<DpiUnit> dpi = new JComboBox<>(ExportManager.comboBoxDpiUnit);
      dpi.setRenderer(ExportManager.rendererDpiUnit);
      addComponentToDisable(dpi);
      paperBox.add(dpi, gbc2);
      gbc2.gridx = 3;
      gbc2.gridy = 0;
      gbc2.gridheight = 2;
//      paperBox.add(Box.createHorizontalStrut(50), gbc2);
      centerPanel.add(paperBox, gbc);
      
      JCheckBox landscape = new JCheckBox("Landscape");
      landscape.setModel(ExportManager.checkboxLandscape);
      addComponentToDisable(landscape);
      centerPanel.add(landscape, gbc);
      
      paper.addActionListener(this::updateExportSizeText);
      dpi.addActionListener(this::updateExportSizeText);
      dpiNum.addChangeListener(this::updateExportSizeText);
      landscape.addActionListener(this::updateExportSizeText);
      paperSize.addActionListener(this::updateExportSizeText);
      pixelSize.addActionListener(this::updateExportSizeText);
      pixelX.addChangeListener(this::updateExportSizeText);
      pixelY.addChangeListener(this::updateExportSizeText);
      
      ActionListener l = e -> {
        boolean px = pixelSize.isSelected();
        pixelX.setEnabled(px);
        pixelY.setEnabled(px);
        chainDimensions.setEnabled(px);
        paper.setEnabled(!px);
        dpi.setEnabled(!px);
        dpiNum.setEnabled(!px);
        landscape.setEnabled(!px);
      };
      pixelSize.addActionListener(l);
      paperSize.addActionListener(l);
      l.actionPerformed(null);
      
      return true;
    }
    
    @Override
    protected void updateExportSizeText(Object anyEventOrNull) {
      Rectangle r = getPaneCropRectangle();
      Dimension d = getOutputPixelSize(r.getSize());
      setExportSizeText(d.width + "px by " + d.height + " px");
    }
  }
  
  private static Dimension getOutputPixelSize(Dimension boundsSize) {
    if(ExportManager.secretRasterExportMode) {
      int width = ExportManager.spinnerPixelX.getNumber().intValue();
      double ratio = boundsSize.getHeight() / boundsSize.getWidth();
      int height = (int)Math.ceil(ratio * (double)width);
      return new Dimension(width, height);
    }
    
    if(!ExportManager.advancedSettingsEnabled.isSelected()) {
      double scale = ExportManager.spinnerResolutionScale.getNumber().doubleValue();
      return new Dimension((int)Math.ceil((double)boundsSize.width * scale), (int)Math.ceil((double)boundsSize.height * scale));
    } else if(ExportManager.radioButtonPixelSize.isSelected()) {
      return new Dimension(ExportManager.spinnerPixelX.getNumber().intValue(), ExportManager.spinnerPixelY.getNumber().intValue());
    } else if(ExportManager.radioButtonPaperSizeR.isSelected()) {
      MediaSizeName msn = (MediaSizeName) ExportManager.comboBoxPaperSize.getSelectedItem();
      MediaSize ms = MediaSize.getMediaSizeForName(msn);
      DpiUnit du = (DpiUnit) ExportManager.comboBoxDpiUnit.getSelectedItem();
      float dpiVal = ExportManager.spinnerDpi.getNumber().floatValue();
      float w = ms.getX(du.getMediaSizeType());
      float h = ms.getY(du.getMediaSizeType());
      Dimension d = new Dimension((int) (w * dpiVal), (int) (h * dpiVal));
      
      if(ExportManager.checkboxLandscape.isSelected()) {
        return new Dimension(d.height, d.width);
      } else {
        return d;
      }
    } else {
      throw new IllegalStateException();
    }
  }

}