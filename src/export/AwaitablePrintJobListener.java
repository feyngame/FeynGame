package export;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.print.Doc;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

import export.ExportFormat.ExportException;

public class AwaitablePrintJobListener extends PrintJobAdapter {
  
  public static void printAndWait(DocPrintJob dpj, Doc doc, PrintRequestAttributeSet pras, long timeout, TimeUnit unit)
    throws PrintException, InterruptedException, TimeoutException, ExportException {
    AwaitablePrintJobListener apjl = new AwaitablePrintJobListener();
    dpj.addPrintJobListener(apjl);
    dpj.print(doc, pras);
    apjl.awaitCompletion(timeout, unit);
  }
  
  private final CompletableFuture<PrintJobEvent> completion;
  
  public AwaitablePrintJobListener() {
    this.completion = new CompletableFuture<>();
  }
  
  //make sure there is not a deadlock by also handling this event
  @Override
  public void printJobNoMoreEvents(PrintJobEvent pje) {
    completion.complete(pje); 
  }
  
  @Override
  public void printJobCompleted(PrintJobEvent pje) {
    completion.complete(pje);
  }

  @Override
  public void printJobFailed(PrintJobEvent pje) {
    completion.completeExceptionally(new ExportException("Stream print service failed to write document"));
  }

  @Override
  public void printJobCanceled(PrintJobEvent pje) {
    completion.cancel(true);
  }
  
  public void awaitCompletion(long timeout, TimeUnit unit) throws InterruptedException, ExportException, TimeoutException {
    try {
      completion.get(timeout, unit);
    } catch (ExecutionException e) {
      Throwable t = e.getCause();
      if(t instanceof ExportException) {
        throw (ExportException) t;
      } else {
        throw new IllegalStateException();
      }
    } catch (CancellationException e) {
      //If this is user-cancelled, completion.get will be interrupted
      throw new ExportException("Stream print job cancelled by external source");
    }
  }
  
  
}