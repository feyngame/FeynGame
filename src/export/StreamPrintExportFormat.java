package export;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.print.AttributeException;
import javax.print.CancelablePrintJob;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.FlavorException;
import javax.print.PrintException;
import javax.print.SimpleDoc;
import javax.print.StreamPrintService;
import javax.print.StreamPrintServiceFactory;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import export.ExportManager.PaperUnit;
import ui.ChainToggleButton;
import ui.Frame;
import ui.Pane;

public class StreamPrintExportFormat extends ExportFormat {
  
  static final boolean PDF_SUPPORTED;
  static final boolean PS_SUPPORTED;
  
  static {
    StreamPrintServiceFactory[] printable2pdf = StreamPrintServiceFactory.lookupStreamPrintServiceFactories(
        DocFlavor.SERVICE_FORMATTED.PAGEABLE, DocFlavor.BYTE_ARRAY.PDF.getMimeType());
    StreamPrintServiceFactory[] pageable2pdf = StreamPrintServiceFactory.lookupStreamPrintServiceFactories(
        DocFlavor.SERVICE_FORMATTED.PAGEABLE, DocFlavor.BYTE_ARRAY.PDF.getMimeType());
    PDF_SUPPORTED = pageable2pdf.length != 0 && printable2pdf.length != 0;

    StreamPrintServiceFactory[] printable2ps = StreamPrintServiceFactory.lookupStreamPrintServiceFactories(
        DocFlavor.SERVICE_FORMATTED.PRINTABLE, DocFlavor.BYTE_ARRAY.POSTSCRIPT.getMimeType());
    StreamPrintServiceFactory[] pageable2ps = StreamPrintServiceFactory.lookupStreamPrintServiceFactories(
        DocFlavor.SERVICE_FORMATTED.PAGEABLE, DocFlavor.BYTE_ARRAY.POSTSCRIPT.getMimeType());
    PS_SUPPORTED = pageable2ps.length != 0 && printable2ps.length != 0;
  }
  
  private final DocFlavor outputFlavor;
  
  StreamPrintExportFormat(String description, DocFlavor outputFlavor, String...allExtensions) {
    super(new FileNameExtensionFilter(description, allExtensions));
    if(allExtensions.length == 0) throw new IllegalArgumentException();
    if(!outputFlavor.getRepresentationClassName().equals(byte[].class.getName())) throw new IllegalArgumentException();
    
    this.outputFlavor = outputFlavor;
  }

  @Override
  public ExportDialogTemplate createDialog(Frame parent, String displayDestination) {
    return new ExportDialog(parent, displayDestination);
  }
  
  @Override
  public ExportResult doAutoExport(Pane content, Path destination, ExportProgressUpdate update) throws IOException, ExportException {
    boolean printCustomSize =
        !ExportManager.advancedSettingsEnabled.isSelected() ||
        !ExportManager.radioButtonPaperSizeV.isSelected();
    update.displayExportProgress(0, 2, "Prepearing");
    
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    StreamPrintServiceFactory[] spsfs = StreamPrintServiceFactory.lookupStreamPrintServiceFactories(
        printCustomSize ? DocFlavor.SERVICE_FORMATTED.PAGEABLE : DocFlavor.SERVICE_FORMATTED.PRINTABLE, outputFlavor.getMimeType());
    if(spsfs.length == 0) throw new ExportException("Unexpected error: cannot find stream print service for output type " + outputFlavor.getMimeType());
    StreamPrintServiceFactory spsf = spsfs[0];
    
    if(cancellationRequested()) return ExportResult.CANCELED;
    
    try(OutputStream os = Files.newOutputStream(destination)) {
      update.displayExportProgress(1, 2, "Printing to file");
      StreamPrintService sps = spsf.getPrintService(os);
      DocPrintJob dpj = sps.createPrintJob();
      ExportPrintable ep = new ExportPrintable(content);
      Doc doc;
      if(printCustomSize) {
        PageFormat pf = createPaper(content.getDiagramBounds());
        doc = ep.createDocCustomPage(pf, null);
      } else {
        MediaSizeName paper = (MediaSizeName) ExportManager.comboBoxPaperSize.getSelectedItem();
        OrientationRequested orientation = ExportManager.checkboxLandscape.isSelected() ?
            OrientationRequested.LANDSCAPE : OrientationRequested.PORTRAIT;
        doc = ep.createDocPresetPage(paper, orientation, null);
      } 
      
      if(cancellationRequested()) return ExportResult.CANCELED;
            
      //Then wait for the job to be completed
      try {
        AwaitablePrintJobListener.printAndWait(dpj, doc, null, 1, TimeUnit.MINUTES);
      } catch (PrintException e) {
        if(e instanceof FlavorException) {
          throw new ExportException("Unexpected error: Stream print service does not support the selected output type");
        } else if(e instanceof AttributeException) {
          throw new ExportException("Unexpected error: Stream print service does not support a required attribute");
        } else {
          throw new ExportException("Failed to submit job to stream print service");
        }
      } catch (InterruptedException e) {
        //This is a cancellation request
        if(dpj instanceof CancelablePrintJob) {
          try {
            ((CancelablePrintJob) dpj).cancel();
          } catch (PrintException e1) {
            throw new ExportException("Failed to cancel.");
          }
        } else {
          //Nothing we can do about it
        }
      } catch (TimeoutException e) {
        //The user will probably cancel the job long before this happens, but we should not wait forever
        throw new ExportException("Export took too long (more than one minute). The process was not explicitly aborted any may still complete in the background.");
      }
      update.displayExportProgress(2, 2, "Finished");

      return ep.result;
    }
  }
  
  @Override
  public boolean isSecret() {
    return false;
  }
  
  private static class ExportPrintable implements Printable {

    private final Pane original;
    private ExportResult result;
    
    private ExportPrintable(Pane pane) {
      this.original = pane;
      this.result = null;
    }
    
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
      if(pageIndex > 0) return NO_SUCH_PAGE;
      
      Pane pane = ExportManager.copyAndCropPane(original);
      Dimension paneSize = pane.getSize();
      Graphics2D g2d = (Graphics2D) graphics.create();
      
      double xScale =  pageFormat.getImageableWidth() / paneSize.getWidth();
      double yScale =  pageFormat.getImageableHeight() / paneSize.getHeight();
      
      double scale = Math.min(xScale, yScale);
      
      double drawnX = paneSize.getWidth() * scale;
      double drawnY = paneSize.getHeight() * scale;
      double shiftX = Math.max((pageFormat.getImageableWidth() - drawnX) / 2.0 + pageFormat.getImageableX(), 0);
      double shiftY = Math.max((pageFormat.getImageableHeight() - drawnY) / 2.0 + pageFormat.getImageableY(), 0);
      
      g2d.translate(shiftX, shiftY);
      g2d.scale(scale, scale);
      g2d.clipRect(0, 0, paneSize.width, paneSize.height);
      
      pane.print(g2d);
      g2d.dispose();
      
      if(result == null) {
        result = new ExportResult(pageFormat.getWidth(), pageFormat.getHeight(), shiftX, shiftY, drawnX, drawnY, pane.getSize(), false);
      }
      
      return PAGE_EXISTS;
    }
    
    public Doc createDocPresetPage(MediaSizeName msn, OrientationRequested orq, DocAttributeSet otherAttrs) {
      DocAttributeSet das = new HashDocAttributeSet(otherAttrs); //ensure mutable set
      das.add(msn);
      das.add(orq);
      return new SimpleDoc(this, DocFlavor.SERVICE_FORMATTED.PRINTABLE, das);
    }
    
    public Doc createDocCustomPage(PageFormat format, DocAttributeSet otherAttrs) {
      Book book = new Book();
      book.append(this, format);
      return new SimpleDoc(book, DocFlavor.SERVICE_FORMATTED.PAGEABLE, otherAttrs);
    }
  }

  //Reused for MultiExport
  protected static class ExportDialog extends ExportDialogTemplate {
    private static final long serialVersionUID = 1L;
    
    private boolean paperXlastEdited = true;
    
    protected ExportDialog(Frame owner, String destination) {
      super(owner, owner.getDrawPane(), destination);
    }
    
    @Override
    protected boolean addContentOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      JCheckBox blackWhite = new JCheckBox("Export in black/white");
      blackWhite.setModel(ExportManager.checkboxBlackWhite);
      addComponentToDisable(blackWhite);
      centerPanel.add(blackWhite, gbc);
      return true;
    }

    @Override
    protected boolean addSimpleOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      
      JPanel resolutionBox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      resolutionBox.add(new JLabel("Width:"), gbc2);
      gbc2.gridx = 1;
      gbc2.weightx = 1.0;
      JSpinner scaleCanvas = new JSpinner(ExportManager.spinnerPaperX);
      resolutionBox.add(scaleCanvas, gbc2);
      addComponentToDisable(scaleCanvas);
      gbc2.gridx = 2;
      gbc2.weightx = 0.0;
      JComboBox<PaperUnit> unit = new JComboBox<>(ExportManager.comboBoxPaperUnit);
      unit.setRenderer(ExportManager.rendererPaperUnit);
      resolutionBox.add(unit, gbc2);
      addComponentToDisable(unit);
      gbc2.gridx = 3;
      resolutionBox.add(Box.createHorizontalStrut(50), gbc);
      centerPanel.add(resolutionBox, gbc);
      scaleCanvas.addChangeListener(this::updateExportSizeText);
      unit.addActionListener(this::updateExportSizeText);
      return true;
    }

    @Override
    protected boolean addAdvancedOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      
      JRadioButton physicalSize = new JRadioButton("Physical size");
      physicalSize.setModel(ExportManager.radioButtonPhysicalSize);
      addComponentToDisable(physicalSize);
      centerPanel.add(physicalSize, gbc);
      
      gbc.insets = PRE_CATEGORY_SUBITEM_INSETS;
      JPanel physicalBox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      
      JLabel widthLabel = new JLabel("Width:");
      widthLabel.setHorizontalAlignment(SwingConstants.LEFT);
      physicalBox.add(widthLabel, gbc2);
      gbc2.gridy = 1;
      JLabel heightLabel = new JLabel("Height:");
      heightLabel.setHorizontalAlignment(SwingConstants.LEFT);
      physicalBox.add(heightLabel, gbc2);
      
      gbc2.gridx = 1;
      gbc2.gridy = 0;
      gbc2.weightx = 1.0;
      
      JSpinner paperSizeX = new JSpinner(ExportManager.spinnerPaperX);
      physicalBox.add(paperSizeX, gbc2);
      addComponentToDisable(paperSizeX);
      gbc2.gridy = 1;
      JSpinner paperSizeY = new JSpinner(ExportManager.spinnerPaperY);
      physicalBox.add(paperSizeY, gbc2);
      addComponentToDisable(paperSizeY);
      
      gbc2.gridx = 2;
      gbc2.gridy = 0;
      gbc2.weightx = 0.0;
      gbc2.gridheight = 2;
      gbc2.weighty = 1.0;
      gbc2.fill = GridBagConstraints.BOTH;
      
      ChainToggleButton chainDimensions = new ChainToggleButton(ExportManager.chainExportRatio);
      physicalBox.add(chainDimensions, gbc2);
      addComponentToDisable(chainDimensions);
      
      gbc2.gridx = 3;
      gbc2.gridy = 0;
      gbc2.weightx = 0.0;
      gbc2.gridheight = 2;
      gbc2.weighty = 0.0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      
      paperSizeX.addChangeListener(e -> {
        if(ExportManager.chainExportRatio.isSelected()) {
          Dimension size = getPaneCropRectangle().getSize();
          double ratio = size.getWidth() / size.getHeight();
          //X was typed, so adjust y
          double y = ExportManager.spinnerPaperX.getNumber().doubleValue() / ratio;
          ExportManager.spinnerPaperY.setValue(y);
        }
        paperXlastEdited = true;
      });
      paperSizeY.addChangeListener(e -> {
        if(ExportManager.chainExportRatio.isSelected()) {
          Dimension size = getPaneCropRectangle().getSize();
          double ratio = size.getWidth() / size.getHeight();
          double x = ExportManager.spinnerPaperY.getNumber().doubleValue() * ratio;
          ExportManager.spinnerPaperX.setValue(x);
        }
        paperXlastEdited = false;
      });
      chainDimensions.addActionListener(e -> {
        if(ExportManager.chainExportRatio.isSelected()) {
          //TODO decide whether X or Y should change when enabling the chainas
          Dimension size = getPaneCropRectangle().getSize();
          double ratio = size.getWidth() / size.getHeight();
          if(paperXlastEdited) { //change y
            double y = ExportManager.spinnerPaperX.getNumber().doubleValue() / ratio;
            ExportManager.spinnerPaperY.setValue(y);
          } else { //change x
            double x = ExportManager.spinnerPaperY.getNumber().doubleValue() * ratio;
            ExportManager.spinnerPaperX.setValue(x);
          }
        }
      });
      
      JComboBox<PaperUnit> paperSizeUnits = new JComboBox<>(ExportManager.comboBoxPaperUnit);
      paperSizeUnits.setRenderer(ExportManager.rendererPaperUnit);
      physicalBox.add(paperSizeUnits, gbc2);
      addComponentToDisable(paperSizeUnits);
      
      centerPanel.add(physicalBox, gbc);
      
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      
      JRadioButton paperSize = new JRadioButton("Paper size");
      paperSize.setModel(ExportManager.radioButtonPaperSizeV);
      addComponentToDisable(paperSize);
      centerPanel.add(paperSize, gbc);
      
      gbc.insets = PRE_CATEGORY_SUBITEM_INSETS;
      
      JPanel paperBox = new JPanel(new GridBagLayout());
      gbc2 = new GridBagConstraints();
      
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);

      JLabel paperSizeLabel = new JLabel("Paper:");
      paperSizeLabel.setHorizontalTextPosition(SwingConstants.LEFT);
      paperBox.add(paperSizeLabel, gbc2);

      gbc2.gridx = 1;
      gbc2.gridy = 0;
      gbc2.weightx = 1.0;
      JComboBox<MediaSizeName> paper = new JComboBox<>(ExportManager.comboBoxPaperSize);
      paper.setRenderer(ExportManager.rendererPaperSize);
      addComponentToDisable(paper);
      paperBox.add(paper, gbc2);
      
      gbc2.gridx = 2;
      gbc2.weightx = 0.0;
      paperBox.add(Box.createHorizontalStrut(50), gbc);
      
      centerPanel.add(paperBox, gbc);

      JCheckBox landscape = new JCheckBox("Landscape");
      landscape.setModel(ExportManager.checkboxLandscape);
      addComponentToDisable(landscape);
      centerPanel.add(landscape, gbc);
      

      paper.addActionListener(this::updateExportSizeText);
      paperSizeUnits.addActionListener(this::updateExportSizeText);
      paperSizeX.addChangeListener(this::updateExportSizeText);
      paperSizeY.addChangeListener(this::updateExportSizeText);
      landscape.addActionListener(this::updateExportSizeText);
      paperSize.addActionListener(this::updateExportSizeText);
      physicalSize.addActionListener(this::updateExportSizeText);
      
      ActionListener l = e -> {
        boolean phys = physicalSize.isSelected();
        paperSizeX.setEnabled(phys);
        paperSizeY.setEnabled(phys);
        paperSizeUnits.setEnabled(phys);
        chainDimensions.setEnabled(phys);
        
        paper.setEnabled(!phys);
        landscape.setEnabled(!phys);
      };
      
      physicalSize.addActionListener(l);
      paperSize.addActionListener(l);
      l.actionPerformed(null); //initialize
      
      return true;
    }
    
    @Override
    protected void updateExportSizeText(Object anyEventOrNull) {
      double[] wh = getSelectedPaperSize(getPaneCropRectangle());
      PaperUnit u = (PaperUnit) ExportManager.comboBoxPaperUnit.getSelectedItem();
      String ws = NumberFormat.getNumberInstance().format(wh[0]);
      String hs = NumberFormat.getNumberInstance().format(wh[1]);
      setExportSizeText(ws + u.getDisplayName() + " by " + hs + u.getDisplayName());
    }
  }
  
  //return type is double[] because Dimension has integer precision
  protected static double[] getSelectedPaperSize(Rectangle2D paneRect) {
    if(!ExportManager.advancedSettingsEnabled.isSelected()) {
      //Handle multi export here better
      double w = ExportManager.spinnerPaperX.getNumber().doubleValue();
      double ratio = paneRect.getHeight() / paneRect.getWidth();
      if(ExportManager.multiDiagramExportActive) {
        int cc = ExportManager.multiColCount.getNumber().intValue();
        int rc = ExportManager.multiRowCount.getNumber().intValue();
        return new double[] {w * cc, w * ratio * rc};
      } else {
        return new double[] {w, w * ratio};
      }
    } else if(ExportManager.radioButtonPaperSizeV.isSelected()) {
      MediaSizeName msn = (MediaSizeName) ExportManager.comboBoxPaperSize.getSelectedItem();
      MediaSize ms = MediaSize.getMediaSizeForName(msn);
      PaperUnit pu = (PaperUnit) ExportManager.comboBoxPaperUnit.getSelectedItem();
      float w = ms.getX(pu.getMediaSizeType());
      float h = ms.getY(pu.getMediaSizeType());
      boolean land = ExportManager.checkboxLandscape.isSelected();
      return land ? new double[] {h, w} : new double[] {w, h};
    } else if(ExportManager.radioButtonPhysicalSize.isSelected()) {
      double w = ExportManager.spinnerPaperX.getNumber().doubleValue();
      double h = ExportManager.spinnerPaperY.getNumber().doubleValue();
      return new double[] {w, h};
    } else {
      throw new IllegalStateException();
    }
  }
  
  protected static PageFormat createPaper(Rectangle2D paneRect) throws ExportException {
    PaperUnit pu = (PaperUnit) ExportManager.comboBoxPaperUnit.getSelectedItem();
    double toPaperSize = 72.0 * ((double) pu.getMediaSizeType()) / ((double) MediaSize.INCH);
    if(ExportManager.advancedSettingsEnabled.isSelected() &&
        ExportManager.radioButtonPaperSizeV.isSelected()) throw new IllegalStateException(); //use MSN instead in this case
    
    double[] dims = getSelectedPaperSize(paneRect);
    if(Double.isNaN(dims[0])) throw new ExportException("Invalid paper width: " + dims[0] + pu.getDisplayName());
    if(Double.isNaN(dims[1])) throw new ExportException("Invalid paper height: " + dims[1] + pu.getDisplayName());
    if(dims[0] <= 0) throw new ExportException("Invalid paper width: " + dims[0] + pu.getDisplayName() + ": Cannot be zero or negative.");
    if(dims[1] <= 0) throw new ExportException("Invalid paper height: " + dims[1] + pu.getDisplayName() + ": Cannot be zero or negative.");
    double w = dims[0] * toPaperSize;
    double h = dims[1] * toPaperSize;
    
    Paper p = new Paper();
    p.setSize(w, h);
    p.setImageableArea(0, 0, w, h);
    
    PageFormat pf = new PageFormat();
    pf.setPaper(p);
    pf.setOrientation(PageFormat.PORTRAIT);
    return pf;
  }
}
