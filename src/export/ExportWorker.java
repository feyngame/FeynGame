package export;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.SwingWorker.StateValue;

import export.ExportFormat.ExportException;
import export.ExportFormat.ExportProgressUpdate;


abstract class ExportWorker implements ExportProgressUpdate {

  private final ExportDialogTemplate dialog;
  private final InnerWorker worker;
  
  protected ExportWorker(ExportDialogTemplate update) {
    this.dialog = Objects.requireNonNull(update);
    this.worker = new InnerWorker();
  }
  
  @Override
  public void displayExportProgress(int progress, int total, String message) {
    worker.publishImpl(new UpdateData(progress, total, message));
  }

  @Override
  public void displayTextMessage(String message) {
    worker.publishImpl(new UpdateData(message));
  }

  protected abstract void doWork() throws Exception; //called on backgroun thread
  protected abstract void onSuccess();
  
  private void onException(Exception exception) {
    System.out.println("OnException");
    String msg;
    if(exception instanceof IOException) { 
      msg = "Error while accessing or writing file: " + exception.getMessage();
    } else if(exception instanceof ExportException) {
      msg = exception.getMessage();
    } else {
      msg = "Unknown error: " + exception.getMessage();
    }
    JOptionPane.showMessageDialog(dialog, msg, "Export error", JOptionPane.ERROR_MESSAGE);
    dialog.hideDialog(false);
  } 
  
  private void onCancelled() {
    dialog.hideDialog(false);
  } 
  
  public void execute() {
    worker.execute();
  }
  
  public void cancel() {
    boolean notStarted = worker.getState() == StateValue.PENDING;
    worker.cancel(true);
    if(notStarted) onCancelled(); 
  }
  
  private final class UpdateData {
    private final String message;
    private final int progress;
    private final int total;
    private final boolean messageOnly;
    
    private UpdateData(int progress, int total, String message) {
      this.message = message;
      this.progress = progress;
      this.total = total;
      this.messageOnly = false;
    }
    
    private UpdateData(String message) {
      this.message = message;
      this.progress = 0;
      this.total = 0;
      this.messageOnly = true;
    }
  }
  
  //Moved to inner class to provide an unambiguous API on ExportWorkerTemplate
  private final class InnerWorker extends SwingWorker<Void, UpdateData> {

    @Override
    protected Void doInBackground() throws Exception {
      ExportWorker.this.doWork();
      return null;
    }
    
    protected void publishImpl(UpdateData data) {
      publish(data);
    }

    @Override
    protected void process(List<UpdateData> chunks) {
      int s = chunks.size();
      if(s == 0) throw new IllegalArgumentException();
      UpdateData d = chunks.get(s - 1);
      if(d.messageOnly) {
        dialog.setProgressBarText(d.message);
      } else {
        dialog.setProgressBarValues(d.progress, d.total, d.message);
      }
    }

    @Override
    protected void done() {
      try {
        get();
        onSuccess(); //Only called if get() does not throw
      } catch(CancellationException e) {
        onCancelled();
      } catch (ExecutionException e) {
        Throwable cause = e.getCause();
        if(cause instanceof RuntimeException)
          throw (RuntimeException) cause;
        else if(cause instanceof Error)
          throw (Error) cause;
        else if(cause instanceof Exception) 
          onException((Exception) cause);
        else
          throw new RuntimeException("Encountered unknown throwable", cause);
      } catch (InterruptedException e) {
        throw new RuntimeException("Unexpected EDT interrupt");
      }
    }
  }
}
