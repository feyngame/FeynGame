package export;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.Vector;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicFileChooserUI;

import export.ExportFormat.ExportException;
import export.ExportFormat.ExportProgressUpdate;
import export.ExportFormat.ExportResult;
import game.JavaVectorGraphic;
import resources.Java8Support;
import ui.Frame;
import ui.Pane;
import ui.Print;

/**
 * Manages export of a {@link Pane} to various file formats and the clipboard.
 * There are two main ways to export: GUI export and automatic export:
 * <ul><li><b>GUI export</b> ({@link #doGuiExport(Frame, boolean)}) will present a GUI with all options relevant to the selected file.
 * It will also display the file selection dialog and an overwrite prompt if the file already exists.</li>
 * <li><b>Auto export</b> ({@link #doAutoExport(Pane, Path, boolean)}) Exports to a destination path without showing any user prompts,
 * overwriting the file if it exists. You can also choose the format using {@link #getFormatForDestination(Path, boolean)} 
 * and then call {@link ExportFormat#doAutoExport(Pane, Path)} to manually select the format.</li></ul>
 * When doing automatic exports, the current export settings are used.
 * You can change the current settings with the various {@code configure...} methods. 
 */
public final class ExportManager {
  private static final Logger LOGGER = Frame.getLogger(ExportManager.class);
  
  public static final boolean ALLOW_PS2PDF = true;
  
  private static final List<ExportFormat> formats;
  private static final List<ExportFormat> multiFormats;
  private static final JFileChooser fileChooser;
//  private static FileFilter lastSelectedFileFilter;
  
  private ExportManager() {
    throw new UnsupportedOperationException();
  }
  
  private static final class TransferableImage implements Transferable {
    
    private final Image image;
    
    private TransferableImage(Image image) {
      this.image = image;
    }
    
    @Override
    public DataFlavor[] getTransferDataFlavors() {
      return new DataFlavor[] { DataFlavor.imageFlavor };
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
      return flavor.equals(DataFlavor.imageFlavor);
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
      if(!flavor.equals(DataFlavor.imageFlavor)) throw new UnsupportedFlavorException(flavor);
      return image;
    }
  }
  
  /**
   * Holds possible units associated with the value entered in the DPI selector for raster export with paper size
   */
  public static enum DpiUnit {
    DPI("dpi (ppi)", MediaSize.INCH), PPCM("px/cm", MediaSize.MM*10), PPMM("px/mm", MediaSize.MM);
    
    private final String displayName;
    private final int mediaSizeType;
    
    private DpiUnit(String displayName, int mediaSizeType) {
      this.displayName = displayName;
      this.mediaSizeType = mediaSizeType;
    }
    
    public String getDisplayName() {
      return displayName;
    }
    
    public int getMediaSizeType() {
      return mediaSizeType;
    }
  }
  
  /**
   * Holds possible units associated with the pyhsical dimension values of a custom paper size
   */
  public static enum PaperUnit {
    CM("cm", MediaSize.MM*10), MM("mm", MediaSize.MM), INCH("inch", MediaSize.INCH);
    
    private final String displayName;
    private final int mediaSizeType;
    
    private PaperUnit(String displayName, int mediaSizeType) {
      this.displayName = displayName;
      this.mediaSizeType = mediaSizeType;
    }
    
    public String getDisplayName() {
      return displayName;
    }
    
    public int getMediaSizeType() {
      return mediaSizeType;
    }
    
    public static Optional<PaperUnit> fromString(String s) {
      if(s.equalsIgnoreCase("cm") || s.equalsIgnoreCase("centimeter") || s.equalsIgnoreCase("centimeters")) {
        return Optional.of(CM);
      } else if(s.equalsIgnoreCase("mm") || s.equalsIgnoreCase("millimeter") || s.equalsIgnoreCase("millimeters")) {
        return Optional.of(MM);
      } else if(s.equalsIgnoreCase("in") || s.equalsIgnoreCase("inch") || s.equalsIgnoreCase("inches")) {
        return Optional.of(INCH);
      } else {
        return Optional.empty();
      }
    }
  }
  
  private static final Map<MediaSizeName, String> paperSizeDisplayNames = new TreeMap<>(Comparator.comparing(MediaSizeName::toString));
  public static final ButtonModel advancedSettingsEnabled = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel checkboxBlackWhite = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel checkboxLandscape = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel checkboxTransparent = new JToggleButton.ToggleButtonModel();
  public static final ComboBoxModel<MediaSizeName> comboBoxPaperSize;
  public static final ListCellRenderer<MediaSizeName> rendererPaperSize = customRenderer(msn -> paperSizeDisplayNames.getOrDefault(msn, msn.toString()));
  public static final ButtonModel radioButtonPaperSizeV = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel radioButtonPaperSizeR = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel radioButtonPhysicalSize = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel radioButtonPixelSize = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel chainExportRatio = new JToggleButton.ToggleButtonModel();
  private static final ButtonGroup sizeButtonGroupR = new ButtonGroup();
  private static final ButtonGroup sizeButtonGroupV = new ButtonGroup();
  
  //For raster export
  public static final SpinnerNumberModel spinnerDpi = new SpinnerNumberModel(100.0, 1.0, Double.MAX_VALUE, 1.0);
  public static final ComboBoxModel<DpiUnit> comboBoxDpiUnit = new DefaultComboBoxModel<>(DpiUnit.values());
  public static final ListCellRenderer<DpiUnit> rendererDpiUnit = customRenderer(DpiUnit::getDisplayName);
  public static final SpinnerNumberModel spinnerResolutionScale = new SpinnerNumberModel(1.0, 0.001, 1000.0, 0.1);
  public static final SpinnerNumberModel spinnerPixelX = new SpinnerNumberModel(200, 1, 10000, 1);
  public static final SpinnerNumberModel spinnerPixelY = new SpinnerNumberModel(200, 1, 10000, 1);
  
  //For vector export
  public static final SpinnerNumberModel spinnerPaperX = new SpinnerNumberModel(20.0, 0.01, 10000.0, 0.1);
  public static final SpinnerNumberModel spinnerPaperY = new SpinnerNumberModel(20.0, 0.01, 10000.0, 0.1);
  public static final ComboBoxModel<PaperUnit> comboBoxPaperUnit = new DefaultComboBoxModel<>(PaperUnit.values());
  public static final ListCellRenderer<PaperUnit> rendererPaperUnit = customRenderer(PaperUnit::getDisplayName);
  
  //For export all
  public static final SpinnerNumberModel multiRowCount = new SpinnerNumberModel(1, 1, 10000, 1);
  public static final SpinnerNumberModel multiColCount = new SpinnerNumberModel(1, 1, 10000, 1);
  public static final ButtonModel multiEnableLabels = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel multiDisableLabels = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel multiEnableMomenta = new JToggleButton.ToggleButtonModel();
  public static final ButtonModel multiEnableFormatString = new JToggleButton.ToggleButtonModel();
  
  public static final ButtonModel showBBcalc = new JToggleButton.ToggleButtonModel();
  protected static String formatString = "Diagram %1$d/%2$d";
  
  protected static boolean secretRasterExportMode = false;
  protected static boolean multiDiagramExportActive = false;
  
  static {
    formats = new ArrayList<>();
    multiFormats = new ArrayList<>();
    
    if(StreamPrintExportFormat.PDF_SUPPORTED) {
      //In theory Java could support PDF export directly, but on all tested systems it does not. Check this first, and then fall back to ps2pdf
      formats.add(new StreamPrintExportFormat("PDF (*.pdf)", DocFlavor.BYTE_ARRAY.PDF, "pdf"));
      multiFormats.add(new MultiDiagramStreamPrintExportFormat("PDF (*.pdf)", DocFlavor.BYTE_ARRAY.PDF, "pdf"));
    }
    
    if(StreamPrintExportFormat.PS_SUPPORTED) {  
      StreamPrintExportFormat psFormat = new StreamPrintExportFormat("Postscript (*.ps)", DocFlavor.BYTE_ARRAY.POSTSCRIPT, "ps");
      EpsExportFormat epsFormat = new EpsExportFormat("Embedded Postscript (*.eps)", psFormat, "eps");
      formats.add(psFormat);
      formats.add(epsFormat);
      formats.add(new TexExportFormat("TeX (*.tex)", epsFormat, "tex")); //secret!
      
      MultiDiagramStreamPrintExportFormat mdspef = new MultiDiagramStreamPrintExportFormat("PS (*.ps)", DocFlavor.BYTE_ARRAY.POSTSCRIPT, "ps"); 
      multiFormats.add(mdspef);
      
      if(ALLOW_PS2PDF && !StreamPrintExportFormat.PDF_SUPPORTED && detectPs2Pdf()) { //Only add ps2pdf if there is no native PDF support
        formats.add(new ExternalConverterExportFormat("PDF (*.pdf)", psFormat,
            ExternalConverterExportFormat.converterForPathExecutable("ps2pdf"), "pdf"));
        multiFormats.add(new ExternalConverterExportFormat("PDF (*.pdf)", mdspef,
            ExternalConverterExportFormat.converterForPathExecutable("ps2pdf"), "pdf"));
      }
    }
    
    List<String> javaSupportedExtensions = Stream.of(ImageIO.getWriterFileSuffixes())
        .map(String::toLowerCase).collect(Collectors.toCollection(ArrayList::new));
    if(javaSupportedExtensions.contains("png")) 
      formats.add(new ImageIOExportFormat("PNG (*.png)", true, "png"));
    if(javaSupportedExtensions.contains("jpg"))  
      formats.add(new ImageIOExportFormat("JPEG (*.jpg, *.jpeg)", false, "jpg", "jpeg"));
    if(javaSupportedExtensions.contains("bmp")) 
      formats.add(new ImageIOExportFormat("BMP (*.bmp)", false, "bmp"));
    if(javaSupportedExtensions.contains("gif"))
      formats.add(new ImageIOExportFormat("GIF (*.gif)", true, "gif"));
    
    fileChooser = new JFileChooser(); 
    fileChooser.setSelectedFile(new File("Untitled.png")); //default value before config read
    fileChooser.setDialogTitle("Select export location and format");
    fileChooser.setMultiSelectionEnabled(false);    
    fileChooser.addPropertyChangeListener(JFileChooser.FILE_FILTER_CHANGED_PROPERTY, new PropertyChangeListener()
    {
        @Override
        public void propertyChange(PropertyChangeEvent e) {
          try {
            String currentName = ((BasicFileChooserUI)fileChooser.getUI()).getFileName();
            if(e.getNewValue() == null || e.getNewValue().equals(fileChooser.getAcceptAllFileFilter())) return;
            FileNameExtensionFilter filter = (FileNameExtensionFilter) e.getNewValue();
            String currentNameNoExt = getWithoutExtension(currentName);
            String newExt = filter.getExtensions()[0];
            String newName = currentNameNoExt + "." + newExt;
            Path newFile = fileChooser.getCurrentDirectory().toPath().resolve(newName);
            fileChooser.setSelectedFile(newFile.toFile());
          } catch(ClassCastException ex) {
            //Can't do it if the UI is for some reason not the basic type
          }
        }
    });
    
    checkboxBlackWhite.setSelected(false);
    checkboxLandscape.setSelected(false);
    checkboxTransparent.setSelected(true);
    
    paperSizeDisplayNames.put(MediaSizeName.ISO_A0, "A0");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A1, "A1");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A2, "A2");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A3, "A3");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A4, "A4");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A5, "A5");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A6, "A6");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A7, "A7");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A8, "A8");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A9, "A9");
    paperSizeDisplayNames.put(MediaSizeName.ISO_A10, "A10");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B0, "B0");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B1, "B1");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B2, "B2");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B3, "B3");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B4, "B4");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B5, "B5");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B6, "B6");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B7, "B7");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B8, "B8");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B9, "B9");
    paperSizeDisplayNames.put(MediaSizeName.ISO_B10, "B10");
    paperSizeDisplayNames.put(MediaSizeName.NA_LEGAL, "Legal");
    paperSizeDisplayNames.put(MediaSizeName.NA_LETTER, "Letter");
//    putAllPaperSizes(paperSizeDisplayNames);
    comboBoxPaperSize = new DefaultComboBoxModel<>(new Vector<>(paperSizeDisplayNames.keySet()));
    
    radioButtonPaperSizeV.setGroup(sizeButtonGroupV);
    radioButtonPhysicalSize.setGroup(sizeButtonGroupV);
    radioButtonPixelSize.setGroup(sizeButtonGroupR);
    radioButtonPaperSizeR.setGroup(sizeButtonGroupR);
    radioButtonPhysicalSize.setSelected(true);
    radioButtonPaperSizeR.setSelected(false);
    radioButtonPaperSizeV.setSelected(false);
    radioButtonPixelSize.setSelected(true);

    //set some defaults in case none are in the config
    spinnerDpi.setValue(100);
    spinnerPaperX.setValue(30);
    spinnerPaperY.setValue(30);
    comboBoxDpiUnit.setSelectedItem(DpiUnit.DPI);
    comboBoxPaperUnit.setSelectedItem(PaperUnit.CM);
  }
  
  @SuppressWarnings("unused")
  private static void putAllPaperSizes(Map<MediaSizeName, String> map) {
    //reflectively find all MediaSizeName constants
    Arrays.stream(MediaSizeName.class.getFields())
      .filter(f -> {
        int m = f.getModifiers();
        return Modifier.isStatic(m) && Modifier.isFinal(m) && Modifier.isPublic(m);
      })
      .filter(f -> f.getType().equals(MediaSizeName.class))
      .flatMap(f -> { //Flatmap to allow empty return in case of an error
        try {
          MediaSizeName msn = (MediaSizeName) f.get(null);
          return Stream.of(msn);
        } catch(IllegalAccessException | ClassCastException e) {
          return Stream.empty();
        }
      })
      .forEach(msn -> map.putIfAbsent(msn, msn.toString()));
  }
  
  private static void setFileFilters(boolean multiExport) {
    List<ExportFormat> f = multiExport ? multiFormats : formats;
    fileChooser.resetChoosableFileFilters();
    fileChooser.setAcceptAllFileFilterUsed(true);
    f.stream()
      .filter(Java8Support.not(ExportFormat::isSecret))
      .map(ExportFormat::getFileFilter)
      .forEach(fileChooser::addChoosableFileFilter);
    if(fileChooser.getSelectedFile() != null) { //try to find the first one that matches the file
      for(FileFilter ff : fileChooser.getChoosableFileFilters()) {
        if(ff.accept(fileChooser.getSelectedFile()) && !ff.equals(fileChooser.getAcceptAllFileFilter())) {
          fileChooser.setFileFilter(ff);
          break;
        }
      }
    }
  }
  
  static Pane copyAndCropPane(Pane original) {
    Pane copy = new Pane(original, false);
    copy.grid = false;
    copy.helperLines = false;
    copy.showCurrentSelectedLine = false;
    if(ExportManager.checkboxBlackWhite.isSelected()) copy.bw();
    Rectangle2D bounds = original.getDiagramBounds();
    copy.crop(bounds);
    return copy;
  }

  
  public static void doClipboardGuiExport(Frame frame) {
    secretRasterExportMode = false;
    ClipboardExportDialog ced = new ClipboardExportDialog(frame);
    prepareGuiExport(frame);
    ced.showDialogAndWait(u -> {
      LOGGER.info("Starting diagram export to system clipboard");
      Frame.setStatusText("Exporting diagram to clipboard");
      ExportResult res = doClipboardAutoExport(frame.getDrawPane(), u);
      if(res.isCanceled()) {
        LOGGER.info("Export to system clipboard was canceled");
        Frame.setStatusText("Export to clipboard canceled");
      } else {
        LOGGER.info("Export to system clipboard was successful");
        Frame.setStatusText("Exported diagram to clipboard");
      }
      return res;
    });
    cleanupGuiExport(frame);
  }
  
  public static ExportResult doClipboardAutoExport(Pane original, ExportProgressUpdate update) {
    if(Thread.interrupted()) return ExportResult.CANCELED;
    update.displayExportProgress(0, 2, "Rendering image");
    boolean transparent = ExportManager.checkboxTransparent.isSelected();
    
    Pane pane = copyAndCropPane(original);
    pane.setBackground(Color.WHITE);
    pane.noAA = transparent;
    pane.setOpaque(!transparent);
    pane.repaint();
    
    if(Thread.interrupted()) return ExportResult.CANCELED;

    Dimension canvasSize = pane.getSize(); //pane is cropped to correct size. Can be changed later
    Dimension imageSize = ClipboardExportDialog.getOutputPixelSize(canvasSize);
    double xScale =  imageSize.getWidth() / canvasSize.getWidth();
    double yScale =  imageSize.getHeight() / canvasSize.getHeight();
    
    double scale = Math.min(xScale, yScale);
    
    double drawnX = canvasSize.getWidth() * scale;
    double drawnY = canvasSize.getHeight() * scale;
    double shiftX = (imageSize.getWidth() - drawnX) / 2.0;
    double shiftY = (imageSize.getHeight() - drawnY) / 2.0;
    
    AffineTransform at = new AffineTransform();
    at.translate(shiftX, shiftY);
    at.scale(scale, scale);
    
    BufferedImage bi = new BufferedImage(imageSize.width, imageSize.height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g = bi.createGraphics();
    
    g.transform(at);
    pane.print(g);
    g.dispose();
    
    if(Thread.interrupted()) return ExportResult.CANCELED;
    
    Image image = transparent ? Frame.makeColorTransparent(bi) : bi;
    
    if(Thread.interrupted()) return ExportResult.CANCELED;
    
    update.displayExportProgress(1, 2, "Writing to Clipboard");
    Transferable t = new TransferableImage(image);
    //Apparently this can print a stacktrace to stderr, but this is not actually a thrown
    //exception and there is nothing that can be done about it (JRE code calls e.printStackTrace().
    //Export should still work despite the error message. It is related to transparency support in the clipboard.
    //Related: https://stackoverflow.com/a/64598247
    LOGGER.finer("Potential exception with message \"Registered service providers failed to encode BufferedImage\" is expected and not an error");
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(t, null);
    update.displayExportProgress(2, 2, "Done");
    
    return new ExportResult(canvasSize, pane.getSize(), false); 
  }
  
  public static void doGuiExport(Frame frame, boolean multiExport) {
    secretRasterExportMode = false; //required for cmd line export with width only
    multiDiagramExportActive = multiExport;
    if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException();
    
    setFileFilters(multiExport);
    int result = fileChooser.showDialog(frame, "Export");
    if(result != JFileChooser.APPROVE_OPTION) { 
      return;
    }
    Path destination = fileChooser.getSelectedFile().toPath().toAbsolutePath();
    if(destination == null) {
      return; 
    }
    String ext = getFileExtension(destination).toLowerCase();
    if(ext.isEmpty()) {
      JOptionPane.showMessageDialog(frame, wrapMessage("The selected file does not have a file extension. Cannot determine export format."),
          "Unknown format", JOptionPane.ERROR_MESSAGE);
      return;
    }
    Optional<ExportFormat> selectedFormat = (multiExport ? multiFormats : formats)
      .stream()
      .filter(e -> e.isExportFormatFor(ext))
      .findFirst();
    
    if(!selectedFormat.isPresent()) {
      JOptionPane.showMessageDialog(frame, wrapMessage("The export format for the file extension '." + ext + "' is unknown."),
          "Unknown format", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    if(Files.exists(destination)) {
      int r = JOptionPane.showConfirmDialog(frame, wrapMessage("The selected file already exists. Do you want to overwrite the existing file?"),
          "Overwrite file", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
      if(r != JOptionPane.YES_OPTION) return;
    }
    
    ExportFormat format = selectedFormat.orElseThrow(NoSuchElementException::new);
    String destinationNoExt = getWithoutExtension(destination);
    for(String extraExtension : format.getAdditionalExportedExtensions()) {
      Path extraDestination = destination.resolveSibling(destinationNoExt + "." + extraExtension);
      if(Files.exists(extraDestination)) {
        int r = JOptionPane.showConfirmDialog(frame, wrapMessage("This export format will also write to a file named " 
            + extraDestination.getFileName().toString() + ". This file already exists. Do you want to overwrite the file?"),
            "Overwrite file", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if(r != JOptionPane.YES_OPTION) return;
      }
    }
    
    prepareGuiExport(frame);
    format.doGuiExport(frame, destination, (r, u) -> {
      if(r.isCanceled()) {
        LOGGER.info("Canceled diagram export to \"" + destination.toString() + "\"");
        Frame.setStatusText("Exported diagram to " + destination.toString());
      } else {
        LOGGER.info("Exported diagram to \"" + destination.toString() + "\" in format \"" + format.getPreferredExtension() + "\"");
        Frame.setStatusText("Exported diagram to " + destination.toString());
      }
      return r;
    });
    cleanupGuiExport(frame);
    JavaVectorGraphic.drawVector = false; //Not sure if necessary, but the old code had this
  }
  
  public static ExportResult doAutoExport(Pane pane, Path destination, boolean multiExport)
      throws IOException, InterruptedException, ExportException {
    multiDiagramExportActive = multiExport;
    Optional<ExportFormat> selectedFormat = getFormatForDestination(destination, multiExport);
    
    if(!selectedFormat.isPresent()) 
      throw new IOException("Unknown export format with file extension '*." + getFileExtension(destination) + "'");
    
    ExportFormat format = selectedFormat.orElseThrow(NoSuchElementException::new);
    ExportResult er = format.doAutoExport(pane, destination);
    JavaVectorGraphic.drawVector = false;
    multiDiagramExportActive = false;
    return er;
  }
  
  public static File getLastGuiExportDestination() {
    return fileChooser.getSelectedFile();
  }
  
  public static Optional<ExportFormat> getFormatForDestination(Path destination, boolean multiExport) {
    return getFormatForExtension(getFileExtension(destination), multiExport);
  }
  
  public static Optional<ExportFormat> getFormatForExtension(String fileExtension, boolean multiExport) {
    String fe = fileExtension.toLowerCase();
    List<ExportFormat> l = multiExport ? multiFormats : formats;
    Optional<ExportFormat> selectedFormat = l.stream()
      .filter(e -> e.isExportFormatFor(fe))
      .findFirst();
    return selectedFormat;
  }
  
  
  private static final String LF = System.getProperty("line.separator");
  
  static void prepareGuiExport(Frame frame) {
    enableAll();
    Print.SHOW_AUTO_BOUNDS = !frame.getDrawPane().mBB;
    Frame.frame.repaint();
  }
  
  public static void setNextGuiExportDestination(Path path, boolean keepPreviousFormat) {
    if(keepPreviousFormat) {
      String filename = getWithoutExtension(path);
      String lastext = getFileExtension(fileChooser.getSelectedFile().getPath());
      Path full = path.toAbsolutePath().getParent().resolve(filename + "." + lastext);
      fileChooser.setSelectedFile(full.toFile());
    } else {
      fileChooser.setSelectedFile(path.toFile());
    }
    
  }
  
  public static void setNextGuiExportDestination(File file, boolean keepPreviousFormat) {
    setNextGuiExportDestination(file.toPath(), keepPreviousFormat);
  }
  
  public static void configureRasterExport(double scaleFactor) {
    advancedSettingsEnabled.setSelected(false);
    spinnerResolutionScale.setValue(Double.valueOf(scaleFactor));
  }
  
  public static void configureRasterExport(int pixelWidth) {
    secretRasterExportMode = true; //Can leave it at true, since this is only ever triggered by a CLI auto export
    spinnerPixelX.setValue(Integer.valueOf(pixelWidth));
  }
  
  public static void configureRasterExport(int pixelWidth, int pixelHeight) {
    advancedSettingsEnabled.setSelected(true);
    radioButtonPixelSize.setSelected(true);
    radioButtonPaperSizeR.setSelected(false);
    chainExportRatio.setSelected(false);
    spinnerPixelX.setValue(Integer.valueOf(pixelWidth));
    spinnerPixelY.setValue(Integer.valueOf(pixelHeight));
  }
  
  public static void configureRasterExport(MediaSizeName paper, boolean landscape, double dpi, DpiUnit unit) {
    advancedSettingsEnabled.setSelected(true);
    radioButtonPixelSize.setSelected(false);
    radioButtonPaperSizeR.setSelected(true);
    comboBoxPaperSize.setSelectedItem(paper);
    spinnerDpi.setValue(Double.valueOf(dpi));
    comboBoxDpiUnit.setSelectedItem(unit);
    checkboxLandscape.setSelected(landscape);
  }
  
  public static void configureVectorExport(double paperWidth, PaperUnit unit) {
    advancedSettingsEnabled.setSelected(false);
    spinnerPaperX.setValue(Double.valueOf(paperWidth));
    comboBoxPaperUnit.setSelectedItem(unit);
  }
  
  public static void configureVectorExport(MediaSizeName paper, boolean landscape) {
    advancedSettingsEnabled.setSelected(true);
    radioButtonPaperSizeV.setSelected(true);
    radioButtonPhysicalSize.setSelected(false);
    comboBoxPaperSize.setSelectedItem(paper);
    checkboxLandscape.setSelected(landscape);
  }
  
  public static void configureVectorExport(double paperWidth, double paperHeight, PaperUnit unit) {
    advancedSettingsEnabled.setSelected(true);
    radioButtonPaperSizeV.setSelected(false);
    radioButtonPhysicalSize.setSelected(true);
    chainExportRatio.setSelected(false);
    spinnerPaperX.setValue(Double.valueOf(paperWidth));
    spinnerPaperY.setValue(Double.valueOf(paperHeight));
    comboBoxPaperUnit.setSelectedItem(unit);
  }
  
  public static void configureBlackWhite(boolean blackWhite) {
    checkboxBlackWhite.setSelected(blackWhite);
  }
  
  public static void configureTransparent(boolean transparent) {
    checkboxTransparent.setSelected(transparent);
  }
  
  public static void configureDiagramsPerPage(int rows, int cols) {
    multiRowCount.setValue(Integer.valueOf(rows));
    multiColCount.setValue(Integer.valueOf(cols));
  }
  
  public static void configureLabels(boolean enable, boolean disable) {
    if(enable && disable) throw new IllegalArgumentException();
    multiEnableLabels.setSelected(enable);
    multiDisableLabels.setSelected(disable);
  }
  
  public static void configureMomenta(boolean enable) {
    multiEnableMomenta.setSelected(enable);
  }
  
  //Null to disable, any to enable
  public static void configureFormatString(String string) {
    
  }
  
  @Deprecated
  static Rectangle getBoundsForPane(Pane pane) {
    if(pane.mBB) {
      return new Rectangle((int)Math.floor(pane.xMBB), (int)Math.floor(pane.yMBB), (int)Math.ceil(pane.wMBB), (int)Math.ceil(pane.hMBB));
    } else {
      return pane.getBoundsPainted();
    }
  }
  
  static void cleanupGuiExport(Frame frame) {
    Print.SHOW_AUTO_BOUNDS = false;
    secretRasterExportMode = false;
    multiDiagramExportActive = false;
  }
  
  private static void enableAll() {
    checkboxBlackWhite.setEnabled(true);
    checkboxTransparent.setEnabled(true);
    checkboxLandscape.setEnabled(true);
    radioButtonPaperSizeR.setEnabled(true);
    radioButtonPaperSizeV.setEnabled(true);
    radioButtonPhysicalSize.setEnabled(true);
    radioButtonPixelSize.setEnabled(true);
    chainExportRatio.setEnabled(true);
    multiEnableLabels.setEnabled(true);
    multiDisableLabels.setEnabled(true);
    multiEnableMomenta.setEnabled(true);
    multiEnableFormatString.setEnabled(true);
  }

  private static <T> ListCellRenderer<T> customRenderer(Function<T, Object> f) {
    Objects.requireNonNull(f);
    return new ListCellRenderer<T>() {
      private final ListCellRenderer<Object> delegate = new DefaultListCellRenderer();
      public Component getListCellRendererComponent(JList<? extends T> list, T value, int index, boolean isSelected, boolean cellHasFocus) {
        return delegate.getListCellRendererComponent(list, f.apply(value), index, isSelected, cellHasFocus);
      }
    };
  }
  
  public static void writePreferences(BufferedWriter bw) throws IOException {
    if(fileChooser.getSelectedFile() != null) {
      String file = fileChooser.getSelectedFile().getAbsolutePath();
      bw.write("Export_Last_FileChooser_File:" + file + LF);
    }
    bw.write("Export_Landscape:" + Boolean.toString(checkboxLandscape.isSelected()) + LF);
    bw.write("Export_BlackWhite:" + Boolean.toString(checkboxBlackWhite.isSelected()) + LF);
    bw.write("Export_Transparent:" + Boolean.toString(checkboxTransparent.isSelected()) + LF);
    MediaSizeName selected = (MediaSizeName) comboBoxPaperSize.getSelectedItem();
    bw.write("Export_PaperSize_Index:" + selected.getValue() + LF);
    int selectedSizeV = radioButtonPaperSizeV.isSelected() ? 0 :
        radioButtonPhysicalSize.isSelected() ? 1 :
          -1;
    if(selectedSizeV != -1) bw.write("Export_Selected_Size_Vector:" + selectedSizeV + LF);
    int selectedSizeR = radioButtonPaperSizeR.isSelected() ? 0 :
      radioButtonPixelSize.isSelected() ? 1 :
        -1;
    if(selectedSizeR != -1) bw.write("Export_Selected_Size_Raster:" + selectedSizeR + LF);
    bw.write("Export_Dpi_Value:" + Double.toString(spinnerDpi.getNumber().doubleValue()) + LF);
    bw.write("Export_Dpi_Index:" + Integer.toString(((DpiUnit) comboBoxDpiUnit.getSelectedItem()).ordinal()) + LF);
    bw.write("Export_Paper_X:" + Double.toString(spinnerPaperX.getNumber().doubleValue()) + LF);
    bw.write("Export_Paper_Y:" + Double.toString(spinnerPaperY.getNumber().doubleValue()) + LF);
    bw.write("Export_PaperUnit_Index:" + Integer.toString(((PaperUnit) comboBoxPaperUnit.getSelectedItem()).ordinal()) + LF);
    bw.write("Export_ResolutionScale:" + Double.toString(spinnerResolutionScale.getNumber().doubleValue()) + LF);
    bw.write("Export_UseAdvancedSettings:" + Boolean.toString(advancedSettingsEnabled.isSelected()) + LF);
    bw.write("Export_Pixel_X:" + Integer.toString(spinnerPixelX.getNumber().intValue()) + LF);
    bw.write("Export_Pixel_Y:" + Integer.toString(spinnerPixelY.getNumber().intValue()) + LF);
    bw.write("Export_Chain:" + Boolean.toString(chainExportRatio.isSelected()) + LF);
    bw.write("Export_Multi_Row_Count:" + Integer.toString(multiRowCount.getNumber().intValue()) + LF);
    bw.write("Export_Multi_Col_Count:" + Integer.toString(multiColCount.getNumber().intValue()) + LF);
    bw.write("Export_ShowBBcalc:" + Boolean.toString(showBBcalc.isSelected()) + LF);
    bw.write("Export_Format_String_Enabled:" + Boolean.toString(multiEnableFormatString.isSelected()) + LF);
    bw.write("Export_Format_String:" + formatString + LF);
  }
  
  public static void readPreferences(String line) {
    if(line.startsWith("Export_Last_FileChooser_File")) {
      File file = new File(readPrefsString(line));
      fileChooser.setSelectedFile(file);
    } else if(line.startsWith("Export_Landscape")) {
      checkboxLandscape.setSelected(readBoolean(line, "Export_Landscape", false));
    } else if(line.startsWith("Export_BlackWhite")) {
      checkboxBlackWhite.setSelected(readBoolean(line, "Export_BlackWhite", false));
    } else if(line.startsWith("Export_PaperSize_Index")) {
      int selectedIndex = readInt(line, "Export_PaperSize_Index", MediaSizeName.ISO_A4.getValue());
      for(MediaSizeName msn : paperSizeDisplayNames.keySet()) {
        if(msn.getValue() == selectedIndex) {
          comboBoxPaperSize.setSelectedItem(msn);
          break;
        }
      }
    } else if(line.startsWith("Export_Transparent")) {
      checkboxTransparent.setSelected(readBoolean(line, "Export_Transparent", true));
    } else if(line.startsWith("Export_Selected_Size_Raster")) {
      readRadioButton(line, "Export_Selected_Size_Raster", 0, radioButtonPaperSizeR, radioButtonPixelSize);
    } else if(line.startsWith("Export_Selected_Size_Vector")) {
      readRadioButton(line, "Export_Selected_Size_Vector", 0, radioButtonPaperSizeV, radioButtonPhysicalSize);
    } else if(line.startsWith("Export_Dpi_Value")) {
      spinnerDpi.setValue(readDouble(line, "Export_Dpi_Value", 300));
    } else if(line.startsWith("Export_Dpi_Index")) {
      int i = readInt(line, "Export_Dpi_Index", DpiUnit.DPI.ordinal());
      if(i < 0 || i >= DpiUnit.values().length) {
        LOGGER.warning("Config option Export_Dpi_Index has invalid value");
        i = DpiUnit.DPI.ordinal();
      } 
      comboBoxDpiUnit.setSelectedItem(DpiUnit.values()[i]);
    } else if(line.startsWith("Export_Paper_X")) {
      spinnerPaperX.setValue(readDouble(line, "Export_Paper_X", 30));
    } else if(line.startsWith("Export_Paper_Y")) {
      spinnerPaperY.setValue(readDouble(line, "Export_Paper_Y", 30));
    } else if(line.startsWith("Export_PaperUnit_Index")) {
      int i = readInt(line, "Export_Dpi_Index", PaperUnit.CM.ordinal());
      if(i < 0 || i >= PaperUnit.values().length) {
        LOGGER.warning("Config option Export_PaperUnit_Index has invalid value");
        i = PaperUnit.CM.ordinal();
      } 
      comboBoxPaperUnit.setSelectedItem(PaperUnit.values()[i]);
    } else if(line.startsWith("Export_ResolutionScale")) {
      double d = readDouble(line, "Export_ResolutionScale", 1.0);
      if(d <= 0) {
        LOGGER.warning("Config option Export_ResolutionScale has invalid value");
        d = 1.0;
      }
      spinnerResolutionScale.setValue(Double.valueOf(d));
    } else if(line.startsWith("Export_UseAdvancedSettings")) {
      advancedSettingsEnabled.setSelected(readBoolean(line, "Export_UseAdvancedSettings", false));
    } else if(line.startsWith("Export_Pixel_X")) {
      spinnerPixelX.setValue(readInt(line, "Export_Pixel_X", 200));
    } else if(line.startsWith("Export_Pixel_Y")) {
      spinnerPixelY.setValue(readInt(line, "Export_Pixel_Y", 200));
    } else if(line.startsWith("Export_Chain")) {
      chainExportRatio.setSelected(readBoolean(line, "Export_Chain", false));
    } else if(line.startsWith("Export_Multi_Row_Count")) {
      multiRowCount.setValue(readInt(line, "Export_Multi_Row_Count", 3));
    } else if(line.startsWith("Export_Multi_Col_Count")) {
      multiColCount.setValue(readInt(line, "Export_Multi_Col_Count", 3));
    } else if(line.startsWith("Export_ShowBBcalc")) {
      showBBcalc.setSelected(readBoolean(line, "Export_ShowBBcalc", false));
    } else if(line.startsWith("Export_Format_String_Enabled")) {
      multiEnableFormatString.setSelected(readBoolean(line, "Export_Format_String_Enabled", false));
    } else if(line.startsWith("Export_Format_String")) {
      formatString = readPrefsString(line);
    }
  }
  
  private static String readPrefsString(String line) {
    return line.substring(line.indexOf(':') + 1);
  }
  
  private static int readInt(String line, String name, int defaultValue) {
    try {
      return Integer.parseInt(line.split(":")[1]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Config option " + name + " has invalid value");
      return defaultValue;
    } catch (ArrayIndexOutOfBoundsException e) {
      LOGGER.warning("Config option " + name + " has no value");
      return defaultValue;
    }
  }
  
  private static double readDouble(String line, String name, double defaultValue) {
    try {
      return Double.parseDouble(line.split(":")[1]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Config option " + name + " has invalid value");
      return defaultValue;
    } catch (ArrayIndexOutOfBoundsException e) {
      LOGGER.warning("Config option " + name + " has no value");
      return defaultValue;
    }
  }
  
  private static boolean readBoolean(String line, String name, boolean defaultValue) {
    String[] split = line.split(":");
    if(split.length >= 2) {
      return Boolean.parseBoolean(split[1]);
    } else {
      LOGGER.warning("Config option " + name +" has no value");
      return defaultValue;
    }
  }
  
  private static void readRadioButton(String line, String name, int defaultValue, ButtonModel...models) {
    int selectedBounds; 
    try {
      selectedBounds = Integer.parseInt(line.split(":")[1]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Config option " + name + " has invalid value");
      selectedBounds = defaultValue;
    } catch (ArrayIndexOutOfBoundsException e) {
      LOGGER.warning("Config option " + name + " has no value");
      selectedBounds = defaultValue;
    }
    if(selectedBounds >= models.length) {
      LOGGER.warning("Config option " + name + " has invalid value");
      selectedBounds = defaultValue;
    } 
    
    models[selectedBounds].setSelected(true);
  } 
  
  
  private static boolean detectPs2Pdf() {
    boolean found = detectCommandLineProgram("ps2pdf");
    LOGGER.info(found ? "Detected external program 'ps2pdf'; enabling PDF export" :
      "External program 'ps2pdf' not found on system path; PDF export disabled");
    return found;
  }
  
  public static boolean detectCommandLineProgram(String name) {
    Objects.requireNonNull(name);
    /*
     * Modified version of answer at
     * https://stackoverflow.com/a/23539220
     * (Changed to also check PATHEXT if it exists)
     */
    String path = System.getenv("PATH");
    String pathext = System.getenv("PATHEXT");
    if(path == null) return false;
    String[] paths = path.split(Pattern.quote(File.pathSeparator));
    String[] pathexts;
    
    if(pathext == null || pathext.isEmpty()) {
      pathexts = new String[] { "" };
    } else {
      String[] p = pathext.split(Pattern.quote(File.pathSeparator));
      pathexts = Arrays.copyOf(p, p.length + 1);
      pathexts[p.length] = ""; //always also look for the file without any extension
    }
    
    return Stream.of(paths)
      .map(Paths::get)
      .flatMap(p -> Stream.of(pathexts).map(e -> p.resolve(name + e)))
      .anyMatch(Files::exists);
  }
  
  private static String wrapMessage(String message) {
    return message; //TODO implement width limit for dialog boxes
//    return "<html><body><p style=\"max-width: " + percentWidth + "%;\">" + message + "</p></body></html>";
  }
  
  public static String getFileExtension(String path) {
    return getFileExtension(new File(path).toPath());
  }
  
  public static String getFileExtension(Path path) {
    String name = path.getFileName().toString();
    int i = name.lastIndexOf('.');
    if(i == -1) {
      return "";
    } else {
      return name.substring(i+1);
    }
  }
  
  public static String getWithoutExtension(String path) {
    return getWithoutExtension(new File(path).toPath());
  }
  
  public static String getWithoutExtension(Path path) {
    String name = path.getFileName().toString();
    int i = name.lastIndexOf('.');
    if(i == -1) {
      return name;
    } else {
      return name.substring(0, i);
    }
  }
  
  public static Path getTempFile() throws IOException {
    return Files.createTempFile("feyngame-export-", ".tmp");
  }
  
  public static Path getTempFile(String trueExtension) throws IOException {
    return Files.createTempFile("feyngame-export-", ".tmp." + trueExtension);
  }
  
  public static final class ClipboardExportDialog extends ExportDialogTemplate {
    private static final long serialVersionUID = 1L;

    public ClipboardExportDialog(Frame owner) {
      super(owner, owner.getDrawPane(), "Clipboard");
    }
    
    @Override
    protected boolean addContentOptions(JPanel centerPanel, GridBagConstraints gbc) {
      gbc.insets = PRE_CATEGORY_ITEM_INSETS;
      JCheckBox transparent = new JCheckBox("Transparent Background");
      transparent.setModel(ExportManager.checkboxTransparent);
      centerPanel.add(transparent, gbc);
      addComponentToDisable(transparent);
      return true;
    }

    @Override
    protected boolean addSimpleOptions(JPanel centerPanel, GridBagConstraints gbc) {
      JPanel resolutionBox = new JPanel(new GridBagLayout());
      GridBagConstraints gbc2 = new GridBagConstraints();
      
      gbc2.gridx = 0;
      gbc2.gridy = 0;
      gbc2.fill = GridBagConstraints.HORIZONTAL;
      gbc2.insets = new Insets(1, 1, 1, 1);
      
      resolutionBox.add(new JLabel("Resolution scale:"), gbc2);
      
      gbc2.gridx = 1;
      gbc2.weightx = 1.0;
      
      JSpinner scaleCanvas = new JSpinner(ExportManager.spinnerResolutionScale);
      resolutionBox.add(scaleCanvas, gbc2);
      
      gbc2.gridx = 2;
      gbc2.weightx = 0.0;
      resolutionBox.add(Box.createHorizontalStrut(50), gbc);
      
      centerPanel.add(resolutionBox, gbc);
      
      scaleCanvas.addChangeListener(this::updateExportSizeText);
      return true;
    }
    
    @Override
    protected void updateExportSizeText(Object anyEventOrNull) {
      Dimension d = getOutputPixelSize(getPaneCropRectangle().getBounds().getSize());
      setExportSizeText(d.width + "px by " + d.height + " px");
    }

    @Override
    protected boolean addAdvancedOptions(JPanel centerPanel, GridBagConstraints gbc) {
      return false;
    }

    protected static Dimension getOutputPixelSize(Dimension boundsSize) {
      double scale = ExportManager.spinnerResolutionScale.getNumber().doubleValue();
      return new Dimension((int)Math.ceil((double)boundsSize.getWidth() * scale),
                           (int)Math.ceil((double)boundsSize.getHeight() * scale));
    }
    
  }
}
