package resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Contains methods that are introduced in later Java version, but not yet available in java 8.
 * Collecting these workarounds here makes it easier to replace them by their proper counterpart when
 * FeynGame is upgraded to a newer version of Java.
 */
@SuppressWarnings("javadoc")
public final class Java8Support {
  
  private Java8Support() {
    throw new UnsupportedOperationException();
  }
  
  //Java 9
  public static IndexOutOfBoundsException newIndexOutOfBoundsException(int index) {
    return new IndexOutOfBoundsException("");
  }
  
  //Java 18
  public static int ceilDiv(int a, int b) {
    int c = a / b;
    return (a % b == 0) ? c : c + 1;
  }
  
  //Java 11
  public static boolean isBlank(String string) {
    for(int i = 0; i < string.length(); i++) {
      char c = string.charAt(i);
      if(!Character.isWhitespace(c)) return false;
    }
    return true;
  }
  
  //Java 11
  public static String stripLeading(String string) {
    for(int i = 0; i < string.length(); i++) {
      char c = string.charAt(i);
      if(!Character.isWhitespace(c)) return string.substring(i); 
    }
    return "";
  }
  
  //Java 11
  public static String stripTrailing(String string) {
    for(int i = string.length() - 1; i >= 0; i--) {
      char c = string.charAt(i);
      if(!Character.isWhitespace(c)) return string.substring(0, i + 1);
    }
    return "";
  }
  
  //Java 11
  public static String strip(String string) {
    //qgraf probably only uses ascii, so the difference between strip and trim should not matter too much
    return string.trim();
  }
  
  //Java 11
  public static Stream<String> lines(String string) {
    BufferedReader br = new BufferedReader(new StringReader(string));
    return br.lines().onClose(() -> {
      try {
        br.close();
      } catch (IOException e) {}
    });
  }
  
  //Java 9
  public static <T> Iterator<T> toIterator(Enumeration<T> enumeration) {
    return new Iterator<T>() {
      @Override
      public boolean hasNext() {
        return enumeration.hasMoreElements();
      }

      @Override
      public T next() {
        return enumeration.nextElement();
      }
    };
  }
  
  //Java 11
  public static <T> Predicate<T> not(Predicate<T> p) {
    return t -> !p.test(t);
  }
  
  //Java 9
  public static CompletableFuture<Process> onExit(Process process) {
    //This is just the implementation from openJDK 11
    return CompletableFuture.supplyAsync(() -> {
      boolean interrupted = false;
      while (true) {
          try {
              ForkJoinPool.managedBlock(new ForkJoinPool.ManagedBlocker() {
                  @Override
                  public boolean block() throws InterruptedException {
                      process.waitFor();
                      return true;
                  }
                  @Override
                  public boolean isReleasable() {
                      return !process.isAlive();
                  }
              });
              break;
          } catch (InterruptedException x) {
              interrupted = true;
          }
      }
      if (interrupted) {
          Thread.currentThread().interrupt();
      }
      return process;
    });
  }
  
  //Java 10
  public static <T> List<T> listCopyOf(Collection<? extends T> list) {
    List<T> l = new ArrayList<>();
    l.addAll(list);
    return Collections.unmodifiableList(l);
  }
  
  //Java 9
  @SuppressWarnings("unchecked")
  public static <T> List<T> listOf(T...e) {
    List<T> l = new ArrayList<>();
    for(T t : e) l.add(t);
    return Collections.unmodifiableList(l);
  }
  
}
