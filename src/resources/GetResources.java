//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package resources;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import qgraf.LoadedQgrafStyle;
import qgraf.QgrafImportException;
import qgraf.QgrafStyleFile;
import ui.Frame;
/**
 * this reads most of the ressources and converts it to objects that are needed
 * at many points of the program
 * @author Sven Yannick Klein
 */
public class GetResources {
  private static final Logger LOGGER = Frame.getLogger(GetResources.class);
  /**
   * parses the "KeyAssignmentSheet.html" and returns its content
   * @return the content of the key assignment sheet
   */
  public static String getKeyAssignmentSheet() {
    InputStream in = GetResources.class.getResourceAsStream("KeyAssignmentSheet.html");
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    StringBuilder htmlText = new StringBuilder();
    String line;
    try {
      while ((line = reader.readLine()) != null) {
        htmlText.append(line);
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return htmlText.toString();
  }
  /**
   * this class loads the icons for the startup window and returns it as an
   * Icon object
   * @param name the name of the file to be loaded
   * @param width the width of the resulting icon
   * @param height the height of the resulting icon
   * @return the icon
   */
  public static Icon getIcon(String name, int width, int height) {
    try {
      java.net.URL imageURL = GetResources.class.getResource(name);
      ImageIcon icon = new ImageIcon(imageURL);

      Image im = icon.getImage();

      Image newimg = im.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
      icon = new ImageIcon(newimg);
      return icon;

    } catch (Exception ex) {
      LOGGER.log(Level.SEVERE, "Error while loading icon '" + name + "'", ex);
    }
    return null;
  }
  /**
   * this class loads images and converts them to icon objects while
   * respecting the aspect ratio
   * @param name the filename of the image
   * @param height the height of the target icon
   * @return the icon
   */
  public static Icon getIcon(String name, int height) {
    try {
      java.net.URL imageURL = GetResources.class.getResource(name);
      ImageIcon icon = new ImageIcon(imageURL);

      Image im = icon.getImage();

      Image newimg = im.getScaledInstance(height * icon.getIconWidth() / icon.getIconHeight(), height, java.awt.Image.SCALE_SMOOTH);
      icon = new ImageIcon(newimg);
      return icon;

    } catch (Exception ex) {
      LOGGER.log(Level.SEVERE, "Cannot get Icon", ex);
    }
    return null;
  }
  /**
   * loads an icon with dimension (219, 112)
   * @param name the filename of the image
   * @return the Icon object
   */
  public static Icon getIcon(String name) {
    return getIcon(name, 219, 112);
  }
  /**
   * reads the "License.txt" file, converts characters that are occupied by
   * HTML
   * @return the converted content as a string
   */
  public static String getLicenseText() {
    InputStream in = GetResources.class.getResourceAsStream("License.txt");
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    StringBuilder htmlText = new StringBuilder();
    String line;
    htmlText.append("<p>");
    try {
      while ((line = reader.readLine()) != null) {
        htmlText.append("<br>");
        line = line.replaceAll("<", "&lt;");
        htmlText.append(line);
        htmlText.append("</br>");
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    htmlText.append("</p>");
    return htmlText.toString();
  }
  /**
   * reads the preview images for the patterns
   * @return an array of Icons that are the previews of the patterns
   */
  public static ImageIcon[] getPatternIcons() {
    ImageIcon images[] = new ImageIcon[] {
      getImageIcon("NoPattern.png", 0),
      getImageIcon("Cross.png", 20),
      getImageIcon("Crosshatched.png", 20),
      getImageIcon("Hatched.png", 20),
      getImageIcon("Star.png", 20),
    };
    return images;
  }
  /**
   * reads a preview image for the patterns and converts them to icons
   * @param name the filename of the preview image
   * @param height the height of the resulting icon
   * @return the icon
   */
   public static ImageIcon getImageIcon(String name, int height) {
    try {
      java.net.URL imageURL = GetResources.class.getResource(name);
      ImageIcon icon = new ImageIcon(imageURL);

      Image im = icon.getImage();

      Image newimg = im.getScaledInstance(height * icon.getIconWidth() / icon.getIconHeight(), height, java.awt.Image.SCALE_SMOOTH);
      icon = new ImageIcon(newimg);
      return icon;

    } catch (Exception ex) {
      // System.err.println("Error: " + ex);
    }
    return null;
  }
  /**
   * loads the feyngame icon
   * @return an image of the feyngame icon
   */
  public static Image getAppIcon() {
    try {
      java.net.URL imageURL = GetResources.class.getResource("fglogo.png");

      Image img =  Toolkit.getDefaultToolkit().createImage(imageURL);
      return img;

    } catch (Exception ex) {
      // System.err.println("Error: " + ex);
    }
    return null;
  }
  
  /**
   * The filenames of all builtin default style files. These files must be located in the
   * {@code /resources/qgrafstyles/} package.
   */
  private static final String[] DEFAULT_STYLE_NAMES = new String[] {
      "form.sty",
      "qgraf-tapir.sty",
      "q2e.sty"
  };
  
  /**
   * Loads all builtin default style files that are contained in the jar file of FeynGame.
   * @return A list of builtin default style files.
   */
  public static Iterable<LoadedQgrafStyle> getDefaultStyles() {
    List<LoadedQgrafStyle> list = new ArrayList<>();
    for(String name : DEFAULT_STYLE_NAMES) {
      try(InputStream is = GetResources.class.getResourceAsStream("/resources/qgrafstyles/" + name)) {
        QgrafStyleFile style = QgrafStyleFile.fromStream(is, name);
        LoadedQgrafStyle loaded = new LoadedQgrafStyle(style, null, true, true);
        list.add(loaded);
      } catch (QgrafImportException | IOException e) {
        LOGGER.log(Level.SEVERE, "Found invalid style file in JAR", e);
      }
    }
    return list;
  }
}
