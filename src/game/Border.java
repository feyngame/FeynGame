//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.awt.*;
import java.io.Serializable;

/**
 * Contains all information about the border of a specific vertex/shaped
 *
 * @author Sven Yannick Klein
 */
@SuppressWarnings("overrides")
public class Border implements Serializable {
  /**
   * Color of the border
   */
  public Color color = Color.BLACK;
  /**
   * indicates, whether or not the border is drawn dashed
   */
  public boolean dashed;
  /**
   * the length of the possible dashes
   */
  public float dashLength;
  /**
   * the stroke of the Border
   */
  public float stroke;
  /**
   * This is not configurable by config, but used to highlight the currently selected line.
   */
  public float dashOffset;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 45L;
  /**
   * basic constructor (black, not dashed, stroke =1)
   */
  public Border() {
    this.color = Color.BLACK;
    this.dashed = false;
    this.dashLength = 5;
    this.stroke = 1;
    this.dashOffset = 0;
  }
  /**
   * constructor just like basic one, just the color is modifiable
   * @param color the color
   */
  public Border(Color color) {
    this.color = color;
    this.dashed = false;
    this.dashLength = 0;
    this.stroke = 1;
    this.dashOffset = 0;
  }
  /**
   * copy constructor
   * @param b the object to be copied
   */
  public Border(Border b) {
    this.color = new Color(b.color.getRed(), b.color.getGreen(), b.color.getBlue());
    this.dashed = b.dashed;
    this.dashLength = b.dashLength;
    this.stroke = b.stroke;
    this.dashOffset = 0;
  }
  /**
   * constructor from various properties
   * @param color the color
   * @param dashed wether or not the border is dashed
   * @param dashLength if dash is true, this gives the length of the dashes
   * @param stroke the thickness of the border
   */
  public Border(Color color, boolean dashed, float dashLength, float stroke) {
    this.color = color;
    this.dashed = dashed;
    this.dashLength = dashLength;
    this.stroke = stroke;
    this.dashOffset = 0;
  }
  /**
   * checks wether or not this is equal to another object
   * @param obj the object to compare to
   */
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    Border other = (Border) obj;

    if (this.dashed != other.dashed)
      return false;
    if (this.dashLength != other.dashLength)
      return false;
    if(this.stroke != other.stroke)
      return false;
    if (!this.color.equals(other.color))
      return false;

    return true;
  }

}
