//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;


import java.awt.*;
import java.io.Serializable;

/**
 * Contains all information about a "FloatingObject" (Image or free floating Vertex)
 *
 * @author Sven Yannick Klein
 */
public class FloatingObject implements Serializable {
  /**
   * decides wether an Image or a Vertex is used (true for Vertex)
   */
  public boolean v;
  /**
   * the vertex that is shown if {@link #v} is true
   */
  public Vertex vertex;
  /**
   * the floating Image that is shown if {@link #v} is false
   */
  public FloatingImage floatingImage;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 46L;
  /**
   * copy constructor where center can be set independantly
   * @param fO the object to be copied
   * @param center the center of the new object
   */
  public FloatingObject(FloatingObject fO, Point2D center) {
    this.v = fO.v;
    if (this.v) {
      this.vertex = new Vertex(fO.vertex, center);
    } else {
      this.floatingImage = new FloatingImage(fO.floatingImage, center);
    }
  }
  /**
   * copy constructor
   * @param fO the object to be copied
   */
  public FloatingObject(FloatingObject fO) {
    this.v = fO.v;
    if (this.v) {
      this.vertex = new Vertex(fO.vertex);
    } else {
      this.floatingImage = new FloatingImage(fO.floatingImage);
    }
  }
  /**
   * constructs a floatingImage with its content set by an image from the
   * given file at a given point
   * @param path the path to the image file
   * @param center the center of the new object
   */
  public FloatingObject(String path, Point2D center) {
    this.v = false;
    this.floatingImage = new FloatingImage(path, center);
  }
  /**
   * constructor with a vertex as content at a given point
   * @param v the vertex to put as content of the FloatingObject
   * @param center the center of the FloatingObject
   */
  public FloatingObject(Vertex v, Point2D center) {
    this.v = true;
    this.vertex = new Vertex(v);
    this.vertex.origin = center;
  }
  /**
   * constructor with an image as content at a given point
   * @param fI the floatingImage to put as content of the FloatingObject
   * @param center the center of the FloatingObject
   */
  public FloatingObject(FloatingImage fI, Point2D center) {
    this.v = false;
    this.floatingImage = new FloatingImage(fI);
    this.floatingImage.center = center;
  }
  /**
   * Draws the specific vertex. Additionally enables antialiasing.
   *
   * @param g               Graphics variable
   * @param currentSelected whether the vertex should be drawn highlighted
   */
  public void draw(Graphics2D g, boolean currentSelected) {
    this.draw(g, currentSelected, false);
  }
  /**
   * Draws the specific vertex. Additionally enables antialiasing.
   *
   * @param g              Graphics variable
   * @param currentSelected whether the vertex should be drawn highlighted
   * @param clipTo10        if the drawing is only done in a circle with radius 10
   */
  public void draw(Graphics2D g, boolean currentSelected, boolean clipTo10) {
    if (this.v) {
      this.vertex.draw(g, currentSelected, clipTo10);
    } else {
      this.floatingImage.draw(g, clipTo10);
    }
  }
  /**
   * outputs a string that can be put in the config file to generate this exact vertex
   *
   * @return the string for the config file
   */
  public String toFile() {
    if (this.v) {
      return this.vertex.toFile();
    } else {
      return this.floatingImage.toFile();
    }
  }
  /**
   * outputs the center of the FloatingImage or the origin of the Vertex
   *
   * @return the center of the Object
   */
  public Point2D getCenter() {
    if (this.v) {
      return this.vertex.origin;
    } else {
      return this.floatingImage.center;
    }
  }
}
