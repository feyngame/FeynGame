//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;
import java.util.*;
import game.*;
import java.awt.*;
import ui.Pane;

import amplitude.FeynmanRuleTemplates;

public class Presets {
  /**
   * All lines that were parsed from config or generated by default.
   */
  @SuppressWarnings("CanBeFinal")
  public ArrayList<LineConfig> presets = new ArrayList<>();
  /**
   * The presets for the FloatingImage
   */
  public ArrayList<FloatingImage> fIPresets = new ArrayList<>();
  /**
   * The presets for the vertices
   */
  public ArrayList<Map.Entry<Vertex, String>> vertexPresets = new ArrayList<>();
  /**
   * the vertex rules
   */
  public ArrayList<Map.Entry<HashMap<String, Integer>, String>> rules = new ArrayList<>();
  /**
   * object containg the templates for the amplitude
   */
  public FeynmanRuleTemplates feynmanRuleTemplates = new FeynmanRuleTemplates();
  /**
   * wether or not this preset should be allowed to have an unsaved state
   */
  public boolean allowUnsaved = true;
  /**
   * wether or not this model is unsaved (if unsaved and ine tries to close this model file
   * one gets prompted wether or not to save this model to disk)
   */
  public boolean unsaved = true;
  /**
   * constructs a preset from the preset that is currently used
   */
  public Presets() {

    this.presets = new ArrayList<>();

    for (LineConfig lc : LineConfig.presets) {
      this.presets.add(new LineConfig(lc));
    }

    this.fIPresets = new ArrayList<>();

    for (FloatingImage fI : FloatingImage.fIPresets) {
      this.fIPresets.add(new FloatingImage(fI));
    }

    this.vertexPresets = new ArrayList<>();

    for (Map.Entry<Vertex, String> v : Vertex.vertexPresets) {
      this.vertexPresets.add(new AbstractMap.SimpleEntry<>(v.getKey(), v.getValue()));
    }

    this.rules = new ArrayList<>();

    for (Map.Entry<HashMap<String, Integer>, String> rule : LineConfig.rules) {
      HashMap<String, Integer> temp = new HashMap<>();
      for (Map.Entry<String, Integer> mapEntry : rule.getKey().entrySet()) {
        temp.put(mapEntry.getKey(), mapEntry.getValue());
      }
      this.rules.add(new AbstractMap.SimpleEntry<>(temp, rule.getValue()));
    }

    this.feynmanRuleTemplates = new FeynmanRuleTemplates(LineConfig.feynmanRuleTemplates);
  }
  /**
   * copy constructor
   * @param copy the preset to copy
   */
  public Presets(Presets copy) {

    this.presets = new ArrayList<>();

    for (LineConfig lc : copy.presets) {
      this.presets.add(new LineConfig(lc));
    }

    this.fIPresets = new ArrayList<>();

    for (FloatingImage fI : copy.fIPresets) {
      this.fIPresets.add(new FloatingImage(fI));
    }

    this.vertexPresets = new ArrayList<>();

    for (Map.Entry<Vertex, String> v : copy.vertexPresets) {
      this.vertexPresets.add(new AbstractMap.SimpleEntry<>(v.getKey(), v.getValue()));
    }

    this.rules = new ArrayList<>();

    for (Map.Entry<HashMap<String, Integer>, String> rule : copy.rules) {
      HashMap<String, Integer> temp = new HashMap<>();
      for (Map.Entry<String, Integer> mapEntry : rule.getKey().entrySet()) {
        temp.put(mapEntry.getKey(), mapEntry.getValue());
      }
      this.rules.add(new AbstractMap.SimpleEntry<>(temp, rule.getValue()));
    }

    this.feynmanRuleTemplates = new FeynmanRuleTemplates(copy.feynmanRuleTemplates);
  }
  /**
   * applies this preset
   */
  public void applyPresets() {
    LineConfig.presets = new ArrayList<>();
    for (LineConfig lc : this.presets) {
      LineConfig.presets.add(new LineConfig(lc));
    }

    FloatingImage.fIPresets = new ArrayList<>();
    for (FloatingImage fI : this.fIPresets) {
      FloatingImage.fIPresets.add(new FloatingImage(fI));
    }

    Vertex.vertexPresets = new ArrayList<>();
    for (Map.Entry<Vertex, String> v : this.vertexPresets) {
      Vertex.vertexPresets.add(new AbstractMap.SimpleEntry<>(v.getKey(), v.getValue()));
    }

    LineConfig.rules = new ArrayList<>();

    for (Map.Entry<HashMap<String, Integer>, String> rule : this.rules) {
      HashMap<String, Integer> temp = new HashMap<>();
      for (Map.Entry<String, Integer> mapEntry : rule.getKey().entrySet()) {
        temp.put(mapEntry.getKey(), mapEntry.getValue());
      }
      LineConfig.rules.add(new AbstractMap.SimpleEntry<>(temp, rule.getValue()));
    }

    LineConfig.feynmanRuleTemplates = new FeynmanRuleTemplates(this.feynmanRuleTemplates);
  }
  /**
   * merges this preset with another
   * @param p2 the other preset
   */
  public void merge(Presets p2) {
    for (FloatingImage fI : p2.fIPresets) {
      this.fIPresets.add(new FloatingImage(fI));
    }

    /* Maps the old vertex identifier to the new vertex identifier */
    HashMap<String, String> changedVIdents = new HashMap<>();
    /* contains all the used new identifiers of this preset*/
    ArrayList<String> usedVIdents = new ArrayList<>();
    /* contains all the used new identifiers of the other preset*/
    ArrayList<String> usedVIdents2 = new ArrayList<>();

    for (Map.Entry<Vertex, String> v : this.vertexPresets) {
      usedVIdents.add(v.getValue());
    }

    for (Map.Entry<Vertex, String> v : p2.vertexPresets) {
      usedVIdents2.add(v.getValue());
    }

    for (Map.Entry<Vertex, String> v2 : p2.vertexPresets) {
      Map.Entry<Vertex, String> v = new AbstractMap.SimpleEntry<Vertex, String>(new Vertex(v2.getKey()), v2.getValue());
      if (usedVIdents.contains(v.getValue())) {
        String ident = v.getValue();
        int i = 1;
        while (usedVIdents.contains(ident) || usedVIdents2.contains(ident)) {
          ident = v.getValue() + String.valueOf(i);
          i++;
        }
        changedVIdents.put(v.getValue(), ident);
        this.vertexPresets.add(new AbstractMap.SimpleEntry<>(v.getKey(), ident));
      } else {
        usedVIdents.add(v.getValue());
        this.vertexPresets.add(v);
      }
    }
    usedVIdents.clear();
    usedVIdents2.clear();

    /* Maps the old line identifier to the new line identifier */
    HashMap<String, String> changedLIdents = new HashMap<>();
    /* contains all the used new identifiers of this preset*/
    ArrayList<String> usedLIdents = new ArrayList<>();
    /* contains all the used new identifiers of the other preset*/
    ArrayList<String> usedLIdents2 = new ArrayList<>();

    this.presets.remove(this.presets.size() - 1);

    for (LineConfig lc : this.presets) {
      if (!lc.lineType.equals(LineType.GRAB)) {
        usedLIdents.add(lc.identifier.getKey());
        usedLIdents.add(lc.identifier.getValue());
      }
    }

    for (LineConfig lc : p2.presets) {
      if (!lc.lineType.equals(LineType.GRAB)) {
        usedLIdents2.add(lc.identifier.getKey());
        usedLIdents2.add(lc.identifier.getValue());
      }
    }

    for (LineConfig lc1 : p2.presets) {
      if (!lc1.lineType.equals(LineType.GRAB)) {
        LineConfig lc = new LineConfig(lc1);
        if (usedLIdents.contains(lc.identifier.getKey()) && usedLIdents.contains(lc.identifier.getValue())) {
          String ident = lc.identifier.getKey();
          int i = 1;
          while (usedLIdents.contains(ident) || usedLIdents2.contains(ident)) {
            ident = lc.identifier.getKey() + String.valueOf(i);
            i++;
          }
          changedLIdents.put(lc.identifier.getKey(), ident);

          String ident2 = lc.identifier.getValue();
          int j = 1;
          while (usedLIdents.contains(ident2) || usedLIdents2.contains(ident2)) {
            ident2 = lc.identifier.getValue() + String.valueOf(j);
            j++;
          }
          changedLIdents.put(lc.identifier.getValue(), ident2);

          lc.identifier = new AbstractMap.SimpleEntry<>(ident, ident2);
          this.presets.add(lc);
        } else if (usedLIdents.contains(lc.identifier.getKey())) {
          String ident = lc.identifier.getKey();
          int i = 1;
          while (usedLIdents.contains(ident) || usedLIdents2.contains(ident)) {
            ident = lc.identifier.getKey() + String.valueOf(i);
            i++;
          }
          changedLIdents.put(lc.identifier.getKey(), ident);

          lc.identifier = new AbstractMap.SimpleEntry<>(ident, lc.identifier.getValue());
          this.presets.add(lc);
        } else if (usedLIdents.contains(lc.identifier.getValue())) {
          String ident2 = lc.identifier.getValue();
          int j = 1;
          while (usedLIdents.contains(ident2) || usedLIdents2.contains(ident2)) {
            ident2 = lc.identifier.getValue() + String.valueOf(j);
            j++;
          }
          changedLIdents.put(lc.identifier.getValue(), ident2);

          lc.identifier = new AbstractMap.SimpleEntry<>(lc.identifier.getKey(), ident2);
          this.presets.add(lc);
        } else {
          this.presets.add(lc);
        }
      }
    }
    usedLIdents.clear();
    usedLIdents2.clear();

    this.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));

    for (Map.Entry<HashMap<String, Integer>, String> rule : p2.rules) {
      HashMap<String, Integer> temp = new HashMap<>();
      for (Map.Entry<String, Integer> mapEntry : rule.getKey().entrySet()) {
        String lIdent = mapEntry.getKey();
        if (changedLIdents.containsKey(lIdent))
          lIdent = changedLIdents.get(lIdent);
        temp.put(lIdent, mapEntry.getValue());
      }
      String vIdent = rule.getValue();
      if (changedVIdents.containsKey(vIdent))
          vIdent = changedVIdents.get(vIdent);
      this.rules.add(new AbstractMap.SimpleEntry<>(temp, vIdent));
    }
  }
  /**
   * merges two presets
   * @param p1 the first preset
   * @param p2 the second preset
   * @return the merge
   */
  public static Presets merge(Presets p1, Presets p2) {
    Presets r = new Presets(p1);
    r.merge(p2);
    return r;
  }
  /**
   * constructs a preset/model from the content of the canvas
   * @param drawPane the canvas
   */
  public Presets(Pane drawPane) {

    this.allowUnsaved = false;
    this.unsaved = false;

    ArrayList<String> lIDs = new ArrayList<>();

    HashMap<LineConfig, Map.Entry<String, String>> lcToIdent = new HashMap<>();

    for (FeynLine fl : drawPane.lines) {
      LineConfig lc = new LineConfig(fl.lineConfig);
      lc.identifier = new AbstractMap.SimpleEntry<String, String>(fl.lineConfig.identifier.getKey(), fl.lineConfig.identifier.getValue());

      boolean n = true;
      for (LineConfig lc2 : presets) {
        if (lc.equals(lc2)) {
          n = false;
          break;
        }
      }
      if (n) {
        if (lc.identifier == null || lIDs.contains(lc.identifier.getKey()) || lIDs.contains(lc.identifier.getValue())) {
          String id1 = lc.description.toLowerCase();
          int i = 1;
          while (lIDs.contains(id1)) {
            id1 = lc.description + String.valueOf(i);
            i++;
          }
          lIDs.add(id1);
          if (lc.showArrow) {
            String id2 = id1.toUpperCase();
            int j = 1;
            while (lIDs.contains(id2)) {
              id2 = lc.description + String.valueOf(j);
              j++;
            }
            lIDs.add(id2);
            lcToIdent.put(lc, new AbstractMap.SimpleEntry<String, String>(id1, id2));
            lc.identifier = new AbstractMap.SimpleEntry<String, String>(id1, id2);
          } else {
            lcToIdent.put(lc, new AbstractMap.SimpleEntry<String, String>(id1, id1));
            lc.identifier = new AbstractMap.SimpleEntry<String, String>(id1, id1);
          }
        } else {
          lIDs.add(lc.identifier.getKey());
          lIDs.add(lc.identifier.getValue());
        }
        presets.add(lc);
      }
    }

    this.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));

    int numberOfVertices = 0;

    for (Vertex v : drawPane.vertices) {

      HashMap<String, Integer> attLines = new HashMap<>();
      for (FeynLine fl : drawPane.lines) {
        if (v.origin.distance(fl.getStart()) < 1) {
          String id = fl.lineConfig.identifier.getKey();
          for (Map.Entry<LineConfig, Map.Entry<String, String>> entry : lcToIdent.entrySet()) {
            if (fl.lineConfig.equals(entry.getKey())) {
              id = entry.getValue().getKey();
            }
          }
          if (attLines.containsKey(id)) {
            attLines.put(id, attLines.get(id) + 1);
          } else {
            attLines.put(id, 1);
          }
        }
        if (v.origin.distance(fl.getEnd()) < 1) {
          String id = fl.lineConfig.identifier.getValue();
          for (Map.Entry<LineConfig, Map.Entry<String, String>> entry : lcToIdent.entrySet()) {
            if (fl.lineConfig.equals(entry.getKey())) {
              id = entry.getValue().getValue();
            }
          }
          if (attLines.containsKey(id)) {
            attLines.put(id, attLines.get(id) + 1);
          } else {
            attLines.put(id, 1);
          }
        }
      }

      String vId = String.valueOf(numberOfVertices);
      boolean ne = true;
      for (Map.Entry<Vertex, String> entry : vertexPresets) {
        if (entry.getKey().equals(v)) {
          ne = false;
          vId = entry.getValue();
          break;
        }
      }
      boolean rN = true;

      for (Map.Entry<HashMap<String, Integer>, String> entry : rules) {
        if (attLines.equals(entry.getKey())) {
          rN = false;
        }
      }
      if (rN)
        rules.add(new AbstractMap.SimpleEntry<HashMap<String, Integer>, String>(attLines, vId));
      if (ne) {
        vertexPresets.add(new AbstractMap.SimpleEntry<Vertex, String>(v, vId));
      }

    }


    for (FloatingObject fO : drawPane.fObjects) {
      if (fO.v) {
        boolean n = true;
        for (Map.Entry<Vertex, String> entry : this.vertexPresets) {
          if (fO.vertex.equals(entry.getKey())) {
            n = false;
            break;
          }
        }
        if (n)
          this.vertexPresets.add(new AbstractMap.SimpleEntry<Vertex, String>(fO.vertex, ""));
      } else {
        fIPresets.add(fO.floatingImage);
      }
    }

  }
}
