//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.awt.*;
import java.io.Serializable;

/**
 * Used in {@link game.Line} and only saves two {@link Double} values. The class only contains some constructors, nothing special.
 * I only implemented this class by myself, because I needed it to be {@link Serializable}.
 *
 * @author Maximilian Lipp
 */
public class Point2D implements Serializable {
  /**
   * the x and y component of the point
   */
  public double x, y;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 49L;
  /**
   * basic constructor for (0, 0)
   */
  public Point2D(Point p) {
    this.x = (double) p.x;
    this.y = (double) p.y;
  }
  public Point2D() {
  }
  /**
   * constuctor based on coordinates given in floating point precision
   * @param x the x component
   * @param y the y component
   */
  public Point2D(float x, float y) {
    this.x = x;
    this.y = y;
  }
  /**
   * constuctor based on coordinates given in double precision
   * @param x the x component
   * @param y the y component
   */
  public Point2D(double x, double y) {
    this.x = x;
    this.y = y;
  }
  /**
   * copy constructor
   * @param p the old point
   */
  public Point2D(Point2D p) {
    this.x = p.x;
    this.y = p.y;
  }
  /**
   * constuctor based on java Point.Double
   * @param p the Point.Double
   */
  public Point2D(Point.Double p) {
    this(p.x, p.y);
  }
  /**
   * gives the distance between this point and antother one (in euclidean
   * metric of course)
   * @param p the point to calculate the distance of this point to
   * @return the distance of this point to the other point
   */
  public double distance(Point2D p) {
    double dx = p.x - this.x;
    double dy = p.y - this.y;
    return Math.sqrt(dx * dx + dy * dy);
  }
  /**
   * gives the distance of this point to a point given by components
   * @param x the x component of the point to calculate the distance to
   * @param y the y component of the point to calculate the distance to
   * @return the distance
   */
  public double distance(double x, double y) {
    return this.distance(new Point2D(x, y));
  }
  /**
   * returns a translated point of this point by the vector from a given
   * point to the origin
   * @param dist the given point
   * @return the translated point
   */
  public Point2D translate(Point2D dist) {
    x += dist.x;
    y += dist.y;
    return this;
  }
  /**
   * gives a point that is this point but with coordinates being multiplied
   * by a factor
   * @param factor the factor
   * @return the new point
   */
  public Point2D scale(double factor) {
    x *= factor;
    y *= factor;
    return this;
  }
  /**
   * a string representaion of the point
   * @return a string representaion of the point
   */
  public String toString() {
    return x + ", " + y;
  }
  /**
   * checks if this is equal to another object
   * @param o the object to compare to
   * @return wether or not the object is equal to this object
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Point2D point2D = (Point2D) o;

    if (Double.compare(point2D.x, x) != 0) return false;
    return Double.compare(point2D.y, y) == 0;
  }
  /**
   * returns the hashCode
   * @return the hash Code
   */
  @Override
  public int hashCode() {
    int result;
    long temp;
    temp = Double.doubleToLongBits(x);
    result = Math.round(temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(y);
    result = 31 * result + Math.round(temp ^ (temp >>> 32));
    return result;
  }
  /**
   * Checks if this is (strictly) left of another Point2D
   * @param point2D the point to compare to
   * @return whether or not this object is to the left of the point
   */
  public boolean isLeftOf(Point2D point2D) {
    return Double.compare(point2D.x, x) > 0;
  }
  /**
   * rotates this point around a center point
   * @param angle the angle with which is rotated
   * @param centerX the x component of the center of rotation
   * @param centerY the y component of the center of rotation
   */
  public void rotate(double angle, double centerX, double centerY) {
    this.x -= centerX;
    this.y -= centerY;
    double tmpX = this.x;
    this.x = Math.cos(angle) * tmpX + Math.sin(angle) * this.y;
    this.y = - Math.sin(angle) * tmpX + Math.cos(angle) * this.y;
    this.x += centerX;
    this.y += centerY;
  }
  /**
   * calculates the nearest point on ellipse with height height
   * and width width to point query. ellipse is centered around origin
   * @param width the width of the ellipse
   * @param height the height of the ellipse
   * @param query the point of which the nearest point on the ellipse
   * is searched
   * @return the nearest point on the ellipse
   */
  public static Point2D nOnEllipse(double width, double height, Point2D query) {
    if (width >= height) {
      Point2D absQuery = new Point2D(Math.abs(query.x), Math.abs(query.y));
      Point2D point = new Point2D(nearestOnEllipse(width, height, absQuery));
      if (query.x < 0) {
        point.x *= -1;
      }
      if (query.y < 0) {
        point.y *= -1;
      }
      return point;
    } else {
      Point2D absQuery = new Point2D(Math.abs(query.y), Math.abs(query.x));
      Point2D point = new Point2D(nearestOnEllipse(height, width, absQuery));
      double tmpx = point.x;
      point.x = point.y;
      point.y = tmpx;
      if (query.x < 0) {
        point.x *= -1;
      }
      if (query.y < 0) {
        point.y *= -1;
      }
      return point;
    }
  }
  /**
   * calculates the nearest point on ellipse with height height
   * and width width to point query. ellipse is centered around origin
   * only works for:
   *      width {@literal >}= height {@literal >} 0
   *      query.x {@literal >}= 0
   *      query.y {@literal >}= 0
   * due to : https://www.geometrictools.com/Documentation/DistancePointEllipseEllipsoid.pdf
   * @param width the width of the ellipse
   * @param height the height of the ellipse
   * @param query the point of which the nearest point on the ellipse
   * is searched
   * @return the nearest point on the ellipse
   */
  private static Point2D nearestOnEllipse(double width, double height, Point2D query) {
    width *= 1d/2d;
    height *= 1d/2d;
    Point2D nPoint = new Point2D(0, 0);
    if (query.y > 0) {
      if (query.x > 0) {
        double z0 = query.x/width;
        double z1 = query.y/height;
        double g = z0*z0 + z1*z1 - 1;
        if (g != 0) {
          double r0 = width*width/height/height;
          double sbar = Point2D.getRoot(r0, z0, z1, g);
          nPoint.x = r0*query.x/(sbar + r0);
          nPoint.y = query.y/(sbar + 1);
        } else {
          nPoint.x = query.x;
          nPoint.y = query.y;
        }
      } else {
        nPoint.x = 0;
        nPoint.y = height;
      }
    } else {
      double numer0 = width * query.x;
      double denom0 = width * width - height * height;
      if (numer0 < denom0) {
        double xde0 = numer0/denom0;
        nPoint.x = width * xde0;
        nPoint.y = height * Math.sqrt(1 - xde0 * xde0);
      } else {
        nPoint.x = width;
        nPoint.y = 0;
      }
    }
    return nPoint;
  }
  /**
   * helper function for nearestOnEllipse
   * calculates root
   * due to : https://www.geometrictools.com/Documentation/DistancePointEllipseEllipsoid.pdf
   * @param r0 some parameter to define the polynomial
   * @param z0 some parameter to define the polynomial
   * @param z1 some parameter to define the polynomial
   * @param g some parameter to define the polynomial
   * @return the root of the polynomial
   */
  private static double getRoot(double r0, double z0, double z1, double g) {
    double n0 = r0 * z0;
    double s0 = z1 - 1;
    double s1 = (g < 0 ? 0 : Point2D.robustLength(new Point2D(n0, z1)) - 1);
    double s = 0;
    for (int i = 0; i < 100; i++) {
      s = (s0 + s1) / 2;
      if (s == s1 || s == s1)
        break;
      double ratio0 = n0/(s + r0);
      double ratio1 = z1/(s + 1);
      g = ratio0*ratio0 + ratio1*ratio1 - 1;
      if (g > 0) {
        s0 = s;
      } else if (g < 0){
        s1 = s;
      } else {
        break;
      }
    }
    return s;
  }
  /**
   * calculates the distance from point to origin
   * due to due to : https://www.geometrictools.com/Documentation/DistancePointEllipseEllipsoid.pdf
   * @param point the point
   * @return the length
   */
  private static double robustLength(Point2D point) {
    double max = Math.max(Math.abs(point.x), Math.abs(point.y));
    return max * Math.sqrt(point.x * point.x / max / max + point.y * point.y / max / max);
  }
  /**
   * calculates the determinant of (p1, p2)
   * @param p1 the vector to use as the first column of the matrix
   * @param p2 the vector to use as the second column of the matrix
   * @return the value of the determinant
   */
  public static double det(Point2D p1, Point2D p2) {
    return p1.x * p2.y - p1.y * p2.x;
  }

  public Point2D add(Point2D other) {
    return new Point2D(this.x+other.x,this.y+other.y);
  }

  public Point2D mulScalar(double scalar) {
    return new Point2D(this.x*scalar, this.y*scalar);
  }
}
