//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import ui.TileSelect;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

/**
 * This Defines the different operating modi. All of them are feynman lines except {@link #GRAB} which is used to alter existing lines. Each mode has an index which represents the order in which they are iterated through.
 * {@link #PHOTON},
 * {@link #FERMION},
 * {@link #GLUON},
 * {@link #GRAB}
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
public enum LineType {
  /**
   * PHOTON represents a wiggled line with index 0.
   */
  PHOTON(0),
  /**
   * FERMION represents a straight line and index 1.
   */
  FERMION(1),
  /**
   * GLUON represents a line that looks like pigtail with index 2.
   */
  GLUON(2),
  /**
   * GRAB does not represent a line but rather a operating mode for altering existing lines with index 4.
   */
  GRAB(3),
  /**
   * this lineType was omitted because of redundancy, it is stated here to load older .fg files with newer versions of feyngame
   */
  SCALAR(4),
  SWAVE(5),
  SSPIRAL(6)
  ;

  /**
   * All possible names of lineTypes
   * these are only used for display in the editFrame and therefore the
   * external names are used
   * Wave   -- Photon
   * Plain  -- Fermion
   * Spiral -- Gluon
   */
  public static final java.util.List<String> types = Arrays.asList("Wave", "Plain", "Spiral", "Swave", "Sspiral");
  /**
   * index is a positiv, consecutive number which uniquely identifies any LineType.
   */
  private final int index; // better be consecutive, starting at 0 (look at Pane.java:checkLines())

  /**
   * img is only set for the {@link #GRAB} where it contains the icon representing this action. All other modi have draw methods, which do not require an image.
   * because the grab has no icon in the tiles anymore, this is not used
   * anymore
   */
  public BufferedImage img;

  /**
   * Standard Constructor.
   *
   * @param pIndex is set to be the {@link #index}
   */
  LineType(int pIndex) {
    this(pIndex, "");
  }

  /**
   * Only used for {@link #GRAB}.
   *
   * @param i    is set to be the {@link #index}
   * @param path is passed to {@link #loadImage(String)} to load the {@link #img}
   */
  LineType(int i, String path) {
    index = i;
    if (!path.equals(""))
      loadImage(path);
  }

  /**
   * Loads {@link #img} from path.
   *
   * @param path path to image file, uses {@link javax.imageio.ImageIO#read(URL)}
   */
  private void loadImage(String path) {
    try {
      img = ImageIO.read(this.getClass().getResource(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
    Image imgT = img.getScaledInstance((int) (img.getWidth() * (TileSelect.uiHeight * 0.9 / img.getHeight())), (int) (TileSelect.uiHeight * 0.9), Image.SCALE_SMOOTH);
    if (imgT instanceof BufferedImage)
      img = (BufferedImage) imgT;
    else {
      img = new BufferedImage(imgT.getWidth(null), imgT.getHeight(null), BufferedImage.TYPE_INT_ARGB);
      Graphics2D bGr = img.createGraphics();
      bGr.drawImage(imgT, 0, 0, null);
      bGr.dispose();
    }
  }

  /**
   * @return returns next LineType according to the index.
   */
  @SuppressWarnings("unused")
  private LineType next() {
    return LineType.values()[this.index + 1 >= LineType.values().length-1 ? 0 : this.index + 1];
  }

  /**
   * @return returns previous LineType according to the index.
   */
  @SuppressWarnings("unused")
  private LineType prev() {
    return LineType.values()[this.index - 1 >= 0 ? this.index - 1 : LineType.values().length-1 - 1];
  }

  public String getName() {
    if (index == 0) {
      return "WAVE";
    } else if (index == 1 || index == 4) {
      return "PLAIN";
    } else if (index == 2) {
      return "SPIRAL";
    } else if (index == 5) {
      return "SWAVE";
    } else if (index == 6) {
      return "SSPIRAL";
    } else {
      return "GRAB";
    }
  }
}
