//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import ui.Frame;
import ui.Pane;
/**
 * Contains all information about a specific line on the screen.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
public class FeynLine implements Serializable {
  public static int latexSize = 20;
  /**
   * wether to allow for automatic creation of blanks or let the user create blanks
   */
  public static boolean autoBlanks = false;
  /**
   * wether or not the defaults for the label were set line type dependant
   */
  public static Boolean LINE_TYPE_DEP_DEF = false;
  /**
   * if the description is shown per default
   */
  public static Boolean showDescDef = false;
  /**
   * default value for descDis
   */
  public static double descDisDef = 10;
  /**
   * default for partPlaceDesc
   */
  public static double partPlaceDescDef = .5;
  /**
   * default for rotDesc;
   */
  public static double rotDescDef = 0;
  /**
   * default for descScale
   */
  public static double descScaleDef = 1;
  /**
   * if the description is shown per default for fermions/plain
   */
  public static Boolean showDescDefFERM = false;
  /**
   * default value for descDis for fermions/plain
   */
  public static double descDisDefFERM = 10;
  /**
   * default for partPlaceDesc for fermions/plain
   */
  public static double partPlaceDescDefFERM = .5;
  /**
   * default for rotDesc for fermions/plain
   */
  public static double rotDescDefFERM = 0;
  /**
   * default for descScale for fermions/plain
   */
  public static double descScaleDefFERM = 1;
  /**
   * if the description is shown per default for gluons/spiral
   */
  public static Boolean showDescDefGLUON = false;
  /**
   * default value for descDis for gluons/spiral
   */
  public static double descDisDefGLUON = 10;
  /**
   * default for partPlaceDesc for gluons/spiral
   */
  public static double partPlaceDescDefGLUON = .5;
  /**
   * default for rotDesc for gluons/spiral
   */
  public static double rotDescDefGLUON = 0;
  /**
   * default for descScale for gluons/spiral
   */
  public static double descScaleDefGLUON = 1;
  /**
   * if the description is shown per default for photons/wave
   */
  public static Boolean showDescDefPHOT = false;
  /**
   * default value for descDis for photons/wave
   */
  public static double descDisDefPHOT = 10;
  /**
   * default for partPlaceDesc for photons/wave
   */
  public static double partPlaceDescDefPHOT = .5;
  /**
   * default for rotDesc for photons/wave
   */
  public static double rotDescDefPHOT = 0;
  /**
   * default for descScale for photons/wave
   */
  public static double descScaleDefPHOT = 1;
  /**
   * If {@link game.LineType#GRAB} is selected, this safes the mode:
   * <ul><li><B>-1</B> means changing both the start and the end point of the {@link game.FeynLine#line}</li>
   * <li><B>0</B> means changing the start point of the {@link game.FeynLine#line}</li>
   * <li><B>1</B> means changing the end point of the {@link game.FeynLine#line}</li></ul>
   */
  public int draggedMode = 2; // -1 drag, 0,1 move
  /**
   * Angle in radians of the end point of the {@link #line} relative to the {@link #center}.
   */
  public double archEnd;
  /**
   * Angle in radians of the start point of the {@link #line} relative to the {@link #center}.
   */
  public double archStart;
  /**
   * Angle in radians of the vector connecting the start and the end point of the {@link #line}. If {@link #height} is 0, this is used a slope of the line.
   */
  public double angle;
  /**
   * If the {@link #height} is not 0, this gives radius of circle, which this line is a part of.
   */
  public double radius;
  /**
   * If the {@link #height} is not 0, this is the center of the circle, which this line is a part of.
   */
  public Point2D center;
  /**
   * If the {@link #height} is not 0, this is the center of the circle, which this line is a part of.
   */
  public Point origin; // deprecated: only here so that old .fg files can be loaded
  /**
   * This gives all information about how to draw the line.
   */
  @SuppressWarnings("CanBeFinal")
  public LineConfig lineConfig;
  /**
   * +1 or -1 to change wether the first node is high or low.
   */
  public int phase = 1;
  /**
   * This is responsible for the curvature of the line. It can only be changed by the getter and setter function.
   */
  public double height;
  /**
   * Angle in radians from {@link #archStart} to {@link #archEnd}.
   */
  public double deltaArch;
  /**
   * if the description is displayed or not
   */
  public Boolean showDesc;
  /**
   * the amount of pixels the description is displayed above(/below) the line
   */
  public int descDis;
  /**
   * the place of the discription relative on the path of the line
   */
  public double partPlaceDesc;
  /**
   * the rotation of the description
   */
  public double rotDesc;
  /**
   * describes the scaling of the description
   */
  public double descScale;
  /**
   * if the start of the line should be fixed or not
   */
  public boolean fixStart = false;
  /**
   * if the end of the line should be fixed or not
   */
  public boolean fixEnd = false;
  /**
   * determines the direction of the circle when the line is a closed circle
   */
  public double circAngle;
  /**
   * This is used to save the start and end point of this line.
   */
  @SuppressWarnings("CanBeFinal")
  private game.Line line;
  /**
   * This is a auxiliary variable used in {@link #draw(Graphics2D, boolean)} to draw the line.
   */
  private Shape path;
  /**
   * Serial Version UID
   */
  // static final long serialVersionUID = 474567L;
  static final long serialVersionUID = 43L;
  /**
   * the momentum arrow
   */
  public Momentum momArrow;
  /**
   * the blank spots (unsorted)
   */
  public ArrayList<Blank> blanksUnsorted = new ArrayList<>();
  /**
   * the blank spots
   */
  public ArrayList<Blank> blanks = new ArrayList<>();
  /**
   * wether or not to draw the blanks
   */
  public boolean drawBlanks = true;
  /*
   * whether or not to clip the arrow for Gluon/Photon
   */
  private static boolean ARROW_CLIP = false;
  /**
   * if the label is unsigned (0), positive (1), or negative (-1)
   */
  public int direction = 0;
  /**
   * sorts the blanks
   */
  public void sortBlanks() {
    blanks.clear();

    for (Blank bl : blanksUnsorted) {
      blanks.add(new Blank(bl));
    }

    /* combining combinable blanks */

    boolean allCombined = false;

    while (!allCombined) {
      allCombined = true;
      int in1 = 0;
      while (in1 < blanks.size() - 1) {
        boolean toBreak = false;
        int in2 = in1 + 1;
        if (in2 < blanks.size()) {
          while (in2 < blanks.size()) {
            Map.Entry<Boolean, Blank> entry = blanks.get(in1).combine(blanks.get(in2));
            toBreak = entry.getKey();
            allCombined = !entry.getKey();
            if (toBreak) {
              blanks.remove(in1);
              blanks.remove(in2 - 1);
              blanks.add(entry.getValue());
              break;
            }
            in2 += 1;
          }
        }
        if (toBreak)
          break;
        in1 += 1;
      }
    }

    /* sorting the blanks */

    FeynLine.quicksort(blanks, 0, blanks.size() - 1);
  }
  public Point2D getLabelPosition() {
    double len = Math.sqrt((this.getStartX() - this.getEndX()) * (this.getStartX() - this.getEndX()) + (this.getStartY() - this.getEndY()) * (this.getStartY() - this.getEndY()));

    String description = this.lineConfig.description;
    if (this.direction == -1) description = this.lineConfig.negDescription;
    if (this.direction == +1) description = this.lineConfig.posDescription;
    Point2D place = new Point2D(0,0);
    if ((len > 0.1 || Math.abs(this.height) > .1) && this.showDesc && description != null && !description.equals("") && this.descScale != 0) {
      if (Math.abs(this.height) < .1) {

        double normtanx = this.getStartX() - this.getEndX();
        normtanx /= len;
        double normtany = this.getStartY() - this.getEndY();
        normtany /= len;
        place = new Point2D(this.getStartX() - (len * this.partPlaceDesc) * normtanx + (this.descDis) * normtany, this.getStartY()- (len * this.partPlaceDesc) * normtany - (this.descDis) * normtanx);
      } else {
        double partArch = this.deltaArch * this.partPlaceDesc;
        double placeArch = this.archStart + partArch;
        double radius = this.radius + this.descDis;
        double x;
        double y;
        if (this.height > 0) {
          y = this.origin.y + radius * Math.sin(placeArch);
          x = this.origin.x + radius * Math.cos(placeArch);
        } else {
          y = this.origin.y - radius * Math.sin(placeArch);
          x = this.origin.x - radius * Math.cos(placeArch);
        }
        place = new Point2D(x, y);
      }
      if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
        place.rotate(-circAngle, this.getStart().x, this.getStart().y);
      }
    }
    return place;
  }
  /**
   * sorts the list defined by list[lowIndex:uppIndex] with quicksort
   * @param list the list of which to sort the part of
   * @param lowIndex the index of the first element of the elements to be sorted
   * @param uppIndex the index of the last element of the elements to be sorted
   */
  private static void quicksort(ArrayList<Blank> list, int lowIndex, int uppIndex) {
    if (lowIndex < uppIndex) {
      int p = FeynLine.partition(list, lowIndex, uppIndex);
      FeynLine.quicksort(list, lowIndex, p - 1);
      FeynLine.quicksort(list, p + 1, uppIndex);
    }
  }
  /**
   * partitions the list defined by list[lowIndex:uppIndex] for quicksort
   * @param list the list of which to partition the part of
   * @param lowIndex the index of the first element of the elements to be partitioned
   * @param uppIndex the index of the last element of the elements to be partitioned
   * @return the new index of the previously last element
   */
  private static int partition(ArrayList<Blank> list, int lowIndex, int uppIndex) {
    Blank pivot = list.get(uppIndex);
    int i = lowIndex;
    for (int j = lowIndex; j <= uppIndex; j+= 1) {
      if (list.get(j).startsBefore(pivot)) {
        Blank tmpBlank = list.get(i);
        list.set(i, list.get(j));
        list.set(j, tmpBlank);
        i += 1;
      }
    }
    Blank tmpBlank = list.get(i);
    list.set(i, list.get(uppIndex));
    list.set(uppIndex, tmpBlank);
    return i;
  }
  /**
   * show Momentum arrow
   */
  public void showMom() {
    if (this.momArrow == null) {
      this.momArrow = new Momentum(this);
    } else {
      this.momArrow.drawMomentum = true;
    }
  }
  /**
   * hide momentum arrow
   */
  public void hideMom() {
    if (this.momArrow != null) {
      this.momArrow.drawMomentum = false;
    }
  }
  /**
   * whether or not momentum arrow is shown
   * @return whether or not the momentum arrow is shown for this line
   */
  public boolean isMom() {
    if (this.momArrow == null)
      return false;
    return momArrow.drawMomentum;
  }
  /**
   * toggle momentum arrow
   */
  public void toggleMom() {
    if (this.isMom()) {
      this.hideMom();
    } else {
      this.showMom();
    }
  }
  /**
   * sets the default for momentum Arrow for this line type
   */
  public void setDefMom() {
    if (this.momArrow != null) {
      this.momArrow.setDefault();
    } else {
      if (this.lineConfig.lineType == LineType.FERMION)
        Momentum.drawMomentumFERM = false;
      else if (this.lineConfig.lineType == LineType.GLUON)
        Momentum.drawMomentumGLUON = false;
      else
        Momentum.drawMomentumPHOT = false;
    }
  }
  /**
   * Only call in initialization of line!!!!!!!!
   */
  private void initDefMom() {
    if (this.lineConfig.lineType == LineType.FERMION) {
      if (Momentum.drawMomentumFERM) {
        this.showMom();
      }
      return;
    } else if (this.lineConfig.lineType == LineType.GLUON || this.lineConfig.lineType == LineType.SSPIRAL) {
      if (Momentum.drawMomentumGLUON) {
        this.showMom();
      }
      return;
    } else if (this.lineConfig.lineType == LineType.PHOTON || this.lineConfig.lineType == LineType.SWAVE) {
      if (Momentum.drawMomentumPHOT) {
        this.showMom();
      }
    }
  }
  /**
   * constructor based on a Line object
   * @param l the Line object giving start and end
   * @param lp the lineConfig
   * @param h the height
   */
  public FeynLine(game.Line l, LineConfig lp, double h) {
    super();
    line = l;
    lineConfig = new LineConfig(lp);
    setHeight(h);
    this.getDefaultDesc();
    this.initDefMom();
    if (Frame.gameMode != 0) {
      checkMom();
    }
  }
  /**
   * constructor for explicitly given start and end
   * @param start the start
   * @param end the end
   * @param lp the lineConfig
   * @param h the height
   */
  public FeynLine(Point2D start, Point2D end, LineConfig lp, double h) {
    this(new game.Line(start.x, start.y, end.x, end.y), lp, h);
    this.getDefaultDesc();
    this.initDefMom();
    if (Frame.gameMode != 0) {
      checkMom();
    }
  }
  /**
   * constructor of straight line based on a Line object
   * @param l the line object giving the start and end
   * @param lp the lineConfig
   */
  public FeynLine(game.Line l, LineConfig lp) {
    this(l, lp, 0);
    this.getDefaultDesc();
    this.initDefMom();
    if (Frame.gameMode != 0) {
      checkMom();
    }
  }
  /**
   * copy constructor; momentum arrow is determined by the default setting
   * @param fl the FeynLine to copy
   */
  public FeynLine(FeynLine fl) { /* TODO:does not copy fixStart, fixEnd */
    super();
    archEnd = fl.archEnd;
    archStart = fl.archStart;
    angle = fl.angle;
    radius = fl.radius;
    center = fl.center;
    lineConfig = new LineConfig(fl.lineConfig);
    line = new game.Line(fl.line);
    path = fl.path;
    height = fl.height;
    deltaArch = fl.deltaArch;
    phase = fl.phase;
    circAngle = fl.circAngle;
    fixStart = fl.fixStart;
    fixEnd = fl.fixEnd;
    this.getDefaultDesc();
    this.initDefMom();
    if (Frame.gameMode != 0) {
      checkMom();
    }
  }
  /**
   * returns an exact copy of "old" including momentum arrow
   * @return an exact copy of this line
   */
  public FeynLine getCopy() {
    FeynLine fl = new FeynLine(this);
    fl.showDesc = this.showDesc;
    fl.descDis = this.descDis;
    fl.rotDesc = this.rotDesc;
    fl.descScale = this.descScale;
    fl.partPlaceDesc = this.partPlaceDesc;
    if (this.blanks != null) {
      for (Blank bl : this.blanks) {
        fl.blanks.add(new Blank(bl));
      }
    }
    if (this.momArrow == null) {
      fl.momArrow = null;
    } else {
      fl.momArrow = this.momArrow.getCopy(fl);
    }
    return fl;
  }
  /**
   * basic constructor
   */
  public FeynLine() {
    line = new Line();
    lineConfig = new LineConfig();
    this.getDefaultDesc();
    this.initDefMom();
    if (Frame.gameMode != 0) {
      checkMom();
    }
  }
  /**
   * checks whether to draw a momentum arrow indicating a charge flow
   * for bosons in game mode
   */
  private void checkMom() {
    if (!LineConfig.feynmanRuleTemplates.isAnticommuting(lineConfig.identifier) && !lineConfig.identifier.getValue().equals(lineConfig.identifier.getKey()) && !lineConfig.showArrow) {
      showMom();
    } else {
      hideMom();
    }
  }
  /**
   * Transforms angle to correct interval between 0 and 2PI.
   *
   * @param angle  to transform
   * @param height needed to find right quadrant
   * @return new angle
   */
  private static double clipAngle(double angle, double height) {
    double retVal = angle;
    if (Math.signum(height) < 0) {
      while (retVal > 2 * Math.PI)
        retVal -= 2 * Math.PI;
      while (retVal <= 0)
        retVal += 2 * Math.PI;
    } else {
      while (retVal >= 0)
        retVal -= 2 * Math.PI;
      while (retVal < -2 * Math.PI)
        retVal += 2 * Math.PI;
    }
    return retVal;
  }

  /**
   * A little bit stupid name^^ This has nothing to do with {@link #getHeight()}!!!! This function calculates the height of the line given a radius and an center.
   *
   * @param rad    radius of the line
   * @param p1     start point of the line
   * @param p2     end point of the line
   * @param center center of the line
   * @return height of the line
   */
  public static double getHeight(double rad, Point2D p1, Point2D p2, Point2D center) {
    boolean debug = false;

    if (p1.x - center.x == center.x - p2.x && p1.y - center.y == center.y - p2.y && p1.distance(new Point2D(center.x, center.y)) == Math.abs(rad)) {
      if (debug) System.out.println("Half circle with radius: " + rad);
      return rad;
    }

    if (p1.x - center.x == center.x - p2.x && p1.y - center.y == center.y - p2.y && p1.distance(new Point2D(center.x, center.y)) == Math.abs(rad)) {
      return rad;
    }

    double tendon = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
    double radius = Math.abs(rad);

    double archEnd = Math.atan2(p2.y - center.y, p2.x - center.x) + Math.PI;
    double archStart = Math.atan2(p1.y - center.y, p1.x - center.x) + Math.PI;
    double deltaArch = (-archStart + archEnd) * -Math.signum(rad);

    while (deltaArch >= 2 * Math.PI) deltaArch -= 2 * Math.PI;
    while (deltaArch < 0) deltaArch += 2 * Math.PI;

    double root = radius * radius - tendon * tendon / 4.0;
    double height1 = radius - Math.sqrt(root);
    double height2 = radius + Math.sqrt(root);

    double height;
    if (deltaArch > Math.PI)
      height = height2;
    else
      height = height1;

    if (Double.isNaN(height)) {
      if (debug) System.err.println("Height could not be calculated, returning radius: " + rad);
      return rad;
    }

    if (Math.signum(rad) == 1) {
      if (debug) System.out.println("New height: " + height);
      return height;
    } else {
      if (debug) System.out.println("New height: " + -height);
      return -height;
    }
  }
  /**
   * sets the paramteters of the description of this line from the default
   */
  private void getDefaultDesc() {
    if (FeynLine.LINE_TYPE_DEP_DEF) {
      if (this.lineConfig.lineType == LineType.FERMION) {
        this.showDesc = FeynLine.showDescDefFERM;
        this.descDis = (int) FeynLine.descDisDefFERM;
        this.rotDesc = FeynLine.rotDescDefFERM;
        this.descScale = FeynLine.descScaleDefFERM;
        this.partPlaceDesc = FeynLine.partPlaceDescDefFERM;
      } else if (this.lineConfig.lineType == LineType.GLUON || this.lineConfig.lineType == LineType.SSPIRAL) {
        this.showDesc = FeynLine.showDescDefGLUON;
        this.descDis = (int) FeynLine.descDisDefGLUON;
        this.rotDesc = FeynLine.rotDescDefGLUON;
        this.descScale = FeynLine.descScaleDefGLUON;
        this.partPlaceDesc = FeynLine.partPlaceDescDefGLUON;
      } else if (this.lineConfig.lineType == LineType.PHOTON || this.lineConfig.lineType == LineType.SWAVE) {
        this.showDesc = FeynLine.showDescDefPHOT;
        this.descDis = (int) FeynLine.descDisDefPHOT;
        this.rotDesc = FeynLine.rotDescDefPHOT;
        this.descScale = FeynLine.descScaleDefPHOT;
        this.partPlaceDesc = FeynLine.partPlaceDescDefPHOT;
      }
    } else {
      this.showDesc = FeynLine.showDescDef;
      this.descDis = (int) FeynLine.descDisDef;
      this.rotDesc = FeynLine.rotDescDef;
      this.descScale = FeynLine.descScaleDef;
      this.partPlaceDesc = FeynLine.partPlaceDescDef;
      double descDisOff = 0;
      if (lineConfig.lineType == LineType.PHOTON || this.lineConfig.lineType == LineType.SWAVE) {
        descDisOff =  Math.signum(FeynLine.descDisDef) * lineConfig.wiggleHeight * 19/25;
      } else if (lineConfig.lineType == LineType.GLUON || this.lineConfig.lineType == LineType.SSPIRAL) {
        if (FeynLine.descDisDef > 0) {
          descDisOff = Math.signum(FeynLine.descDisDef) * lineConfig.wiggleHeight * 2/5;
        } else {
          descDisOff = Math.signum(FeynLine.descDisDef) * lineConfig.wiggleHeight * 3/4;
        }
      }
      this.descDis += descDisOff;
    }
  }
  /**
   * sets the parameters for the description of this line to the default
   */
  public void setDefault() {
    FeynLine.LINE_TYPE_DEP_DEF = true;
    if (this.lineConfig.lineType == LineType.FERMION) {
      FeynLine.showDescDefFERM = this.showDesc;
      FeynLine.descDisDefFERM = this.descDis;
      FeynLine.rotDescDefFERM = this.rotDesc;
      FeynLine.descScaleDefFERM = this.descScale;
      FeynLine.partPlaceDescDefFERM = this.partPlaceDesc;
    } else if (this.lineConfig.lineType == LineType.GLUON || this.lineConfig.lineType == LineType.SSPIRAL) {
      FeynLine.showDescDefGLUON = this.showDesc;
      FeynLine.descDisDefGLUON = this.descDis;
      FeynLine.rotDescDefGLUON = this.rotDesc;
      FeynLine.descScaleDefGLUON = this.descScale;
      FeynLine.partPlaceDescDefGLUON = this.partPlaceDesc;
    } else if (this.lineConfig.lineType == LineType.PHOTON || this.lineConfig.lineType == LineType.SWAVE) {
      FeynLine.showDescDefPHOT = this.showDesc;
      FeynLine.descDisDefPHOT = this.descDis;
      FeynLine.rotDescDefPHOT = this.rotDesc;
      FeynLine.descScaleDefPHOT = this.descScale;
      FeynLine.partPlaceDescDefPHOT = this.partPlaceDesc;
    }
  }
  /**
   * Getter function for {@link #height}
   *
   * @return {@link #height}
   */
  public double getHeight() {
    return height;
  }
  /**
   * Setter function for {@link #height}
   *
   * @param h new height
   */
  public void setHeight(double h) {
    if (h != h) return;
    height = h;
    if (center == null)
      calculateArch(this.getStart(), this.getEnd());
  }
  /**
   * Increments the {@link #height} by h
   *
   * @param h height difference
   */
  public void addHeight(double h) {
    setHeight(height + h);
  }
  /**
   * getter for x comp. of the start of the line
   * @return the x comp. of the start
   */
  public double getStartX() {
    return line.getStartX();
  }
  /**
   * setter for x comp. of the start of the line
   * @param d the x comp. of the start
   */
  public void setStartX(double d) {
    line.setStartX(d);
  }
  /**
   * getter for y comp. of the start of the line
   * @return the y comp. of the start
   */
  public double getStartY() {
    return line.getStartY();
  }
  /**
   * setter for y comp. of the start of the line
   * @param d the y comp. of the start
   */
  public void setStartY(double d) {
    line.setStartY(d);
  }
  /**
   * getter for x comp. of the end of the line
   * @return the x comp. of the end
   */
  public double getEndX() {
    return line.getEndX();
  }
  /**
   * setter for x comp. of the end of the line
   * @param d the x comp. of the end
   */
  public void setEndX(double d) {
    line.setEndX(d);
  }
  /**
   * getter for y comp. of the end of the line
   * @return the y comp. of the end
   */
  public double getEndY() {
    return line.getEndY();
  }
  /**
   * setter for y comp. of the end of the line
   * @param d the y comp. of the end
   */
  public void setEndY(double d) {
    line.setEndY(d);
  }
  /**
   * getter function for the line member object
   * @return the line member
   */
  public Line getLine() {
    return line;
  }
  /**
   * getter for the start of the line as a Point2D object
   * @return the start
   */
  public Point2D getStart() {
    return new Point2D((double) line.getStartX(), (double) line.getStartY());
  }
  /**
   * setter for the start of the line as a Point2D object
   * @param p the start
   */
  @SuppressWarnings("unused")
  public void setStart(Point2D p) {
    this.setStartX(p.x);
    this.setStartY(p.y);
  }
  /**
   * getter for the end of the line as a Point2D object
   * @return the end
   */
  public Point2D getEnd() {
    return new Point2D((double) line.getEndX(), (double) line.getEndY());
  }
  /**
   * setter for the end of the line as a Point2D object
   * @param p the end
   */
  @SuppressWarnings("unused")
  public void setEnd(Point2D p) {
    this.setEndX(p.x);
    this.setEndY(p.y);
  }
  /**
   * draws the line without clipping to being straight when the height is
   * below 10 absolotely
   * @param g the graphics object to draw to
   * @param currentSelected wether or not the line should be highlighted
   * because it is currently selected
   */
  public void draw(Graphics2D g, boolean currentSelected) {
    this.draw(g, currentSelected, false);
  }

  /**
   * draws the arrow if showArrow
   * @param g the graphics object that is drawn on
   * @param point the point that the arrow is drawn
   */
  public void drawArrow(Graphics2D g, Point2D point) {

    Point2D p1 = new Point2D((double) line.getStartX(), (double) line.getStartY());
    Point2D p2 = new Point2D((double) line.getEndX(), (double) line.getEndY());

    java.awt.geom.Path2D.Double arrow = new java.awt.geom.Path2D.Double();
    double rotAngle = angle;
    // arrow.moveTo((lineConfig.arrowSize * Math.cos(rotAngle + 3.0 / 4.0 * Math.PI)), (lineConfig.arrowSize * Math.sin(rotAngle + 3.0 / 4.0 * Math.PI)));
    // arrow.lineTo((lineConfig.arrowSize * Math.cos(rotAngle + 5.0 / 4.0 * Math.PI)), (lineConfig.arrowSize * Math.sin(rotAngle + 5.0 / 4.0 * Math.PI)));
    // arrow.lineTo((lineConfig.arrowSize * Math.cos(rotAngle)), (lineConfig.arrowSize * Math.sin(rotAngle)));

    Point2D upper = new Point2D(- ((double) lineConfig.arrowSize)/2d, ((double) lineConfig.arrowHeight)/2d);
    Point2D lower = new Point2D(- ((double) lineConfig.arrowSize)/2d, - ((double) lineConfig.arrowHeight)/2d);
    Point2D tip = new Point2D((double) lineConfig.arrowSize / 2d, 0d);
    Point2D mid = new Point2D(- ((double) lineConfig.arrowSize)/2d + lineConfig.arrowDent, 0d);
    upper.rotate(- rotAngle, 0, 0);
    lower.rotate(- rotAngle, 0, 0);
    tip.rotate(- rotAngle, 0, 0);
    mid.rotate(- rotAngle, 0, 0);

    arrow.moveTo(upper.x, upper.y);
    arrow.lineTo(mid.x, mid.y);
    arrow.lineTo(lower.x, lower.y);
    arrow.lineTo(tip.x, tip.y);

    if (Math.abs(height) < .1) {

      AffineTransform tf = new AffineTransform();
      tf.translate(point.x, point.y);
      arrow.transform(tf);
      g.fill(arrow);



    } else {
      // double archEndGes = Math.atan2(this.line.end.y - this.center.y, this.line.end.x - this.center.x);
      // double archStartGes = Math.atan2(this.line.start.y - this.center.y, this.line.start.x - this.center.x);
      // double deltaArchGes = (-archStartGes + archEndGes) * -Math.signum(this.height);
      //
      // while (deltaArchGes > 2 * Math.PI) {
      //  deltaArchGes -= 2 * Math.PI;
      // }
      // while (deltaArchGes <= 0) {
      //  deltaArchGes += 2 * Math.PI;
      // }

      AffineTransform tf = new AffineTransform();
      tf.translate(point.x, point.y);
      tf.rotate(this.deltaArch * (this.lineConfig.arrowPlace - .5));
      arrow.transform(tf);

      if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
        g.rotate(circAngle, this.getStart().x, this.getStart().y);
        g.fill(arrow);
        g.rotate(-circAngle, this.getStart().x, this.getStart().y);
      } else {
        g.fill(arrow);
      }

    }
  }

  /**
   * Draws the specific line using {@link #drawFermion(Graphics2D)}, {@link #drawGluon(Graphics2D, boolean)} or {@link #drawPhoton(Graphics2D)}. Additionally enables anti aliasing.
   *
   * @param g                 Graphics variable
   * @param currentSelected    whether the line should be drawn highlighted
   * @param noStraightClipping whether the line should be drawn curved if the {@link #height} is smaller than 10
   */
  public void draw(Graphics2D g, boolean currentSelected, boolean noStraightClipping) {

    calculateArch();

    double len = Math.sqrt((this.getStartX() - this.getEndX()) * (this.getStartX() - this.getEndX()) + (this.getStartY() - this.getEndY()) * (this.getStartY() - this.getEndY()));

    String description = "";
    if (this.direction == -1) description = this.lineConfig.negDescription;
    if (this.direction == +1) description = this.lineConfig.posDescription;
    if (description == "") description = this.lineConfig.description;

    if (Pane.drawLabel && (len > 0.1 || Math.abs(height) > .1) && this.showDesc && description != null && !description.equals("") && this.descScale != 0) {

      if (lineConfig.description.contains("<html>")) {
        Point2D place;
        JLabel label = new JLabel(description, JLabel.CENTER);

        label.setForeground(Color.BLACK);
        label.setSize(label.getPreferredSize());
        double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.cos(this.rotDesc) + label.getPreferredSize().height * Math.sin(this.rotDesc);
        double labelHeight = /*label.getPreferredSize().height*/ -label.getPreferredSize().width * Math.sin(this.rotDesc) + label.getPreferredSize().height * Math.cos(this.rotDesc);
        labelHeight *= this.descScale;
        labelWidth *= this.descScale;
        if (Math.abs(height) < .1) {

          double normtanx = this.getStartX() - this.getEndX();
          normtanx /= len;
          double normtany = this.getStartY() - this.getEndY();
          normtany /= len;
          place = new Point2D(this.getStartX() - labelWidth / 2 - (len * this.partPlaceDesc) * normtanx + (this.descDis) * normtany, this.getStartY() - labelHeight / 2 - (len * this.partPlaceDesc) * normtany - (this.descDis) * normtanx);
        } else {
          double partArch = this.deltaArch * this.partPlaceDesc;
          double placeArch = this.archStart + partArch;
          double radius = this.radius + this.descDis;
          double x;
          double y;
          if (this.height > 0) {
            y = this.center.y + radius * Math.sin(placeArch);
            x = this.center.x + radius * Math.cos(placeArch);
          } else {
            y = this.center.y - radius * Math.sin(placeArch);
            x = this.center.x - radius * Math.cos(placeArch);
          }
          place = new Point2D(x - labelWidth / 2, y - labelHeight / 2);
        }


        if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
          place.x += labelWidth / 2;
          place.y += labelHeight / 2;
          place.rotate(-circAngle, this.getStart().x, this.getStart().y);
          place.x -= labelWidth / 2;
          place.y -= labelHeight / 2;
          g.translate(place.x, place.y);
          g.scale(this.descScale, this.descScale);
          g.rotate( - this.rotDesc);
          label.paint(g);
          g.rotate(this.rotDesc);
          g.scale(1/this.descScale, 1/this.descScale);
          g.translate( - place.x, - place.y);
        } else {
          g.translate(place.x, place.y);
          g.scale(this.descScale, this.descScale);
          g.rotate( - this.rotDesc);
          label.paint(g);
          g.rotate(this.rotDesc);
          g.scale(1/this.descScale, 1/this.descScale);
          g.translate( - place.x, - place.y);
        }
      } else {
        Point2D place;
        TeXFormula formula = new TeXFormula(description);

        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
        icon.setForeground(Color.BLACK);
        double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.cos(this.rotDesc) + icon.getIconHeight() * Math.sin(this.rotDesc);
        double labelHeight = /*label.getPreferredSize().height*/ -icon.getIconWidth() * Math.sin(this.rotDesc) + icon.getIconHeight() * Math.cos(this.rotDesc);

        labelHeight *= this.descScale;
        labelWidth *= this.descScale;
        if (Math.abs(height) < .1) {

          double normtanx = this.getStartX() - this.getEndX();
          normtanx /= len;
          double normtany = this.getStartY() - this.getEndY();
          normtany /= len;
          place = new Point2D(this.getStartX() - labelWidth / 2 - (len * this.partPlaceDesc) * normtanx + (this.descDis) * normtany, this.getStartY() - labelHeight / 2 - (len * this.partPlaceDesc) * normtany - (this.descDis) * normtanx);
        } else {
          double partArch = this.deltaArch * this.partPlaceDesc;
          double placeArch = this.archStart + partArch;
          double radius = this.radius + this.descDis;
          double x;
          double y;
          if (this.height > 0) {
            y = this.center.y + radius * Math.sin(placeArch);
            x = this.center.x + radius * Math.cos(placeArch);
          } else {
            y = this.center.y - radius * Math.sin(placeArch);
            x = this.center.x - radius * Math.cos(placeArch);
          }
          place = new Point2D(x - labelWidth / 2, y - labelHeight / 2);
        }


        if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
          place.x += labelWidth / 2;
          place.y += labelHeight / 2;
          place.rotate(-circAngle, this.getStart().x, this.getStart().y);
          place.x -= labelWidth / 2;
          place.y -= labelHeight / 2;
          g.translate(place.x, place.y);
          g.scale(this.descScale, this.descScale);
          g.rotate( - this.rotDesc);
          icon.paintIcon(new JPanel(), g, 0, 0);
          g.rotate(this.rotDesc);
          g.scale(1/this.descScale, 1/this.descScale);
          g.translate( - place.x, - place.y);
        } else {
          g.translate(place.x, place.y);
          g.scale(this.descScale, this.descScale);
          g.rotate( - this.rotDesc);
          icon.paintIcon(new JPanel(), g, 0, 0);
          g.rotate(this.rotDesc);
          g.scale(1/this.descScale, 1/this.descScale);
          g.translate( - place.x, - place.y);
        }
      }
    }

    /* the blanks */
    if (this.blanks.size() != 0 && this.drawBlanks) {

      this.drawBlanks = false;

      double width = this.lineConfig.stroke;
      if (this.lineConfig.showArrow) {
        width = Math.max(width, this.lineConfig.arrowSize);
      }

      if (!this.lineConfig.lineType.equals(LineType.FERMION)) {
        width = Math.max(width, this.lineConfig.wiggleHeight);
      }

      if (Math.abs(this.height) < 0.1) {

        Point2D upperRight = new Point2D();
        Point2D lowerRight = new Point2D();
        Point2D upperLeft = new Point2D();
        Point2D lowerLeft = new Point2D();

        double normtanx = this.getStartX() - this.getEndX();
        normtanx /= len;
        double normtany = this.getStartY() - this.getEndY();
        normtany /= len;

        upperLeft.x = getStartX() + normtany * width;
        upperLeft.y = getStartY() - normtanx * width;
        lowerLeft.x = getStartX() - normtany * width;
        lowerLeft.y = getStartY() + normtanx * width;

        int i = 0;

        for (Blank bl : this.blanks) {
          double startLen = (bl.getPartOfPath() - bl.getLength() / 2d) * len;
          double endLen = (bl.getPartOfPath() + bl.getLength() / 2d) * len;

          upperRight.x = getStartX() + normtany * width - normtanx * startLen;
          upperRight.y = getStartY() - normtanx * width - normtany * startLen;
          lowerRight.x = getStartX() - normtany * width - normtanx * startLen;
          lowerRight.y = getStartY() + normtanx * width - normtany * startLen;

          Shape clipRec = new java.awt.geom.Path2D.Double();
          ((java.awt.geom.Path2D.Double) clipRec).moveTo(upperLeft.x, upperLeft.y);
          ((java.awt.geom.Path2D.Double) clipRec).lineTo(lowerLeft.x, lowerLeft.y);
          ((java.awt.geom.Path2D.Double) clipRec).lineTo(lowerRight.x, lowerRight.y);
          ((java.awt.geom.Path2D.Double) clipRec).lineTo(upperRight.x, upperRight.y);
          ((java.awt.geom.Path2D.Double) clipRec).lineTo(upperLeft.x, upperLeft.y);

          Rectangle clRec = g.getClipBounds();

          g.setClip(clipRec);
          g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);

          this.draw(g, currentSelected, noStraightClipping);

          g.setClip(clRec);

          upperLeft.x = getStartX() + normtany * width - normtanx * endLen;
          upperLeft.y = getStartY() - normtanx * width - normtany * endLen;
          lowerLeft.x = getStartX() - normtany * width - normtanx * endLen;
          lowerLeft.y = getStartY() + normtanx * width - normtany * endLen;

          i+=1;
        }

        upperRight.x = getEndX() + normtany * width;
        upperRight.y = getEndY() - normtanx * width;
        lowerRight.x = getEndX() - normtany * width;
        lowerRight.y = getEndY() + normtanx * width;

        Shape clipRec = new java.awt.geom.Path2D.Double();
        ((java.awt.geom.Path2D.Double) clipRec).moveTo(upperLeft.x, upperLeft.y);
        ((java.awt.geom.Path2D.Double) clipRec).lineTo(lowerLeft.x, lowerLeft.y);
        ((java.awt.geom.Path2D.Double) clipRec).lineTo(lowerRight.x, lowerRight.y);
        ((java.awt.geom.Path2D.Double) clipRec).lineTo(upperRight.x, upperRight.y);
        ((java.awt.geom.Path2D.Double) clipRec).lineTo(upperLeft.x, upperLeft.y);

        Rectangle clRec = g.getClipBounds();

        g.setClip(clipRec);
        g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);

        this.draw(g, currentSelected, noStraightClipping);

        g.setClip(clRec);

      } else {
        double startAngle;
        double endAngle;

        double arcLen = Math.PI * 2 * this.radius;

        startAngle = this.archStart;

        for (Blank bl : this.blanks) {
          double endPart = bl.getPartOfPath() - bl.getLength() / 2d;
          double startPart = bl.getPartOfPath() + bl.getLength() / 2d;

          endAngle = this.archStart + this.deltaArch * endPart;

          Shape clip = new Arc2D.Double(Arc2D.Double.PIE);

          ((Arc2D.Double) clip).width = (Math.abs(radius) + Math.abs(width)) * 2;
          ((Arc2D.Double) clip).height = (Math.abs(radius) + Math.abs(width)) * 2;

          ((Arc2D.Double) clip).x = center.x - Math.abs(radius) - Math.abs(width);
          ((Arc2D.Double) clip).y = center.y - Math.abs(radius) - Math.abs(width);

          ((Arc2D.Double) clip).start = startAngle * 180 / Math.PI - angle / Math.PI * 360 - 180;
          ((Arc2D.Double) clip).extent = (endAngle - startAngle) * 180 / Math.PI;

          if (this.getStart().equals(this.getEnd())) {
            AffineTransform rotate = AffineTransform.getRotateInstance(circAngle, getStartX(), getStartY());
              clip = rotate.createTransformedShape(clip);
          }

          Rectangle clRec = g.getClipBounds();

          g.setClip(clip);
          g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);

          this.draw(g, currentSelected, noStraightClipping);

          g.setClip(clRec);

          startAngle = this.archStart + this.deltaArch * startPart;
        }

        endAngle = this.archStart + deltaArch;

        Shape clip = new Arc2D.Double(Arc2D.Double.PIE);

        ((Arc2D.Double) clip).width = (Math.abs(radius) + Math.abs(width)) * 2;
        ((Arc2D.Double) clip).height = (Math.abs(radius) + Math.abs(width)) * 2;

        ((Arc2D.Double) clip).x = center.x - Math.abs(radius) - Math.abs(width);
        ((Arc2D.Double) clip).y = center.y - Math.abs(radius) - Math.abs(width);

        ((Arc2D.Double) clip).start = startAngle * 180 / Math.PI - angle / Math.PI * 360 - 180;
        ((Arc2D.Double) clip).extent = (endAngle - startAngle) * 180 / Math.PI;

        if (this.getStart().equals(this.getEnd())) {
          AffineTransform rotate = AffineTransform.getRotateInstance(circAngle, getStartX(), getStartY());
            clip = rotate.createTransformedShape(clip);
        }

        Rectangle clRec = g.getClipBounds();

        g.setClip(clip);
        g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);

        this.draw(g, currentSelected, noStraightClipping);

        g.setClip(clRec);
      }

      this.drawBlanks = true;

      return;

    }

    currentSelected = (currentSelected || this.lineConfig.multiEdit);

    // this.calculateArch();

    Stroke oldStroke = g.getStroke();
    Color oldColor = g.getColor();

    if (!this.lineConfig.doubleLine) {

      double stroke = lineConfig.stroke;

      boolean thick = false;

      if ((this.draggedMode == 1 || this.draggedMode == 0 || this.draggedMode == -1) && stroke < 20) {
        stroke *= (double) 1.5;
        thick = true;
      } else if (this.draggedMode == 1 || this.draggedMode == 0 || this.draggedMode == -1) {
        stroke += 5;
      }

      if (lineConfig.dashed) {
        final float[] dash1 = {(float) lineConfig.dashLength};
        g.setStroke(new BasicStroke((float) stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
      } else
        g.setStroke(new BasicStroke((float) stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

      g.setColor(lineConfig.color);

      if (currentSelected) {
        float[] dash1 = {30f, 5};
        if (lineConfig.dashed)
          dash1 = new float[]{(float)lineConfig.dashLength};
        g.setStroke(new BasicStroke((float) stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, (float) lineConfig.dashOffset));
      }

      if (/*line.getStartX() > 0 && line.getStartY() > 0 && line.getEndX() > 0 && line.getEndY() > 0*/true) {
        if (lineConfig.lineType == LineType.PHOTON)
          drawNotPlain(g, LineType.PHOTON, noStraightClipping, thick);
        else if (lineConfig.lineType == LineType.FERMION || lineConfig.lineType == LineType.SCALAR)
          drawFermion(g);
        else if (lineConfig.lineType == LineType.GLUON)
          drawNotPlain(g, LineType.GLUON, noStraightClipping, thick);
        else if (lineConfig.lineType == LineType.SWAVE) {
          drawNotPlain(g, LineType.PHOTON, noStraightClipping, thick);
          drawFermion(g);
        } else if (lineConfig.lineType == LineType.SSPIRAL) {
          drawNotPlain(g, LineType.GLUON, noStraightClipping, thick);
          drawFermion(g);
        }
      }
      if (lineConfig.showArrow) {
        drawArrowAtPlace(g);
      }
    } else {
      this.lineConfig.doubleLine = false;
      this.draw(g, currentSelected, noStraightClipping);
      FeynLine copy = new FeynLine(this);
      copy.lineConfig.stroke = this.lineConfig.stroke / 3;
      copy.lineConfig.color = Color.WHITE;
      copy.lineConfig.doubleLine = false;
      copy.circAngle = this.circAngle;
      copy.lineConfig.arrowHeight = 0;
      copy.showDesc = false;
      // try {
      //   copy.lineConfig.arrowSize = 0;
      // } catch (NullPointerException ex) {
      //   ex.printStackTrace();
      // }
      copy.draw(g, currentSelected, noStraightClipping);
      this.lineConfig.doubleLine = true;
    }

    g.setStroke(oldStroke);
    g.setColor(oldColor);
  }

  private void drawNotPlain(Graphics2D g, LineType lt, boolean noStraightClipping, boolean thick) {

    double strokeSize = lineConfig.stroke;
    if (thick)
      strokeSize = lineConfig.stroke * 1.5;

    if (Math.abs(height) < .1) { // straight line

      if (lineConfig.showArrow) { // with arrow

        // we need five temporary lines: one is the notplain left and right to the arrow , and then we have two plains at the start and end, and a plain in the middle behind the arrow
        FeynLine start = new FeynLine(this);
        FeynLine end = new FeynLine(this);
        FeynLine middle = new FeynLine(this);
        FeynLine left = new FeynLine(this);
        FeynLine right = new FeynLine(this);

        start.lineConfig.lineType = LineType.FERMION;
        end.lineConfig.lineType = LineType.FERMION;
        middle.lineConfig.lineType = LineType.FERMION;

        Point2D p1 = new Point2D((double) line.getStartX(), (double) line.getStartY());
        Point2D p2 = new Point2D((double) line.getEndX(), (double) line.getEndY());

        // determine the 'breakpoints'
        double vecX = p1.x - p2.x;
        double vecY = p1.y - p2.y;

        double len = Math.sqrt(vecX*vecX+vecY*vecY);
        vecX /= len;
        vecY /= len;

        Point2D bP1 = new Point2D(p1.x - lineConfig.straight_len*vecX, p1.y - lineConfig.straight_len*vecY);
        Point2D bP2 = new Point2D(p1.x - (lineConfig.arrowPlace*len-lineConfig.straight_len_arrow-lineConfig.arrowSize/2.)*vecX, p1.y - (lineConfig.arrowPlace*len-lineConfig.straight_len_arrow-lineConfig.arrowSize/2.)*vecY);
        Point2D bP3 = new Point2D(p1.x - (lineConfig.arrowPlace*len+lineConfig.straight_len_arrow+lineConfig.arrowSize/2.)*vecX, p1.y - (lineConfig.arrowPlace*len+lineConfig.straight_len_arrow+lineConfig.arrowSize/2.)*vecY);
        Point2D bP4 = new Point2D(p2.x + lineConfig.straight_len*vecX, p2.y + lineConfig.straight_len*vecY);

        start.setEnd(bP1);
        left.setStart(bP1);
        left.setEnd(bP2);
        middle.setStart(bP2);
        middle.setEnd(bP3);
        right.setStart(bP3);
        right.setEnd(bP4);
        end.setStart(bP4);

        if (lt == LineType.PHOTON) {
          left.drawPhoton(g);
          right.drawPhoton(g);
        } else if (lt == LineType.GLUON) {
          left.drawGluon(g, noStraightClipping);
          right.drawGluon(g, noStraightClipping);
        }
        start.drawFermion(g);
        middle.drawFermion(g);
        end.drawFermion(g);

        g.setColor(lineConfig.color);
        Shape sp = new Ellipse2D.Double((bP1.x - strokeSize/2.),(bP1.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((bP2.x - strokeSize/2.),(bP2.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((bP3.x - strokeSize/2.),(bP3.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((bP4.x - strokeSize/2.),(bP4.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        // g.fillOval((int) (bP1.x - strokeSize/2.),(int) (bP1.y - strokeSize/2.),(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) (bP2.x - strokeSize/2.),(int) (bP2.y - strokeSize/2.),(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) (bP3.x - strokeSize/2.),(int) (bP3.y - strokeSize/2.),(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) (bP4.x - strokeSize/2.),(int) (bP4.y - strokeSize/2.),(int) strokeSize,(int) strokeSize);

      } else { // without arrow

        // we need three temporary lines: one is the notplain line in the middle, and then we have two plains at the start and end
        FeynLine start = new FeynLine(this);
        FeynLine end = new FeynLine(this);
        FeynLine middle = new FeynLine(this);

        start.lineConfig.lineType = LineType.FERMION;
        end.lineConfig.lineType = LineType.FERMION;

        Point2D p1 = new Point2D((double) line.getStartX(), (double) line.getStartY());
        Point2D p2 = new Point2D((double) line.getEndX(), (double) line.getEndY());

        // determine the 'breakpoints'
        double vecX = p1.x - p2.x;
        double vecY = p1.y - p2.y;

        double len = Math.sqrt(vecX*vecX+vecY*vecY);
        vecX /= len;
        vecY /= len;

        Point2D bP1 = new Point2D(p1.x - lineConfig.straight_len*vecX, p1.y - lineConfig.straight_len*vecY);
        Point2D bP2 = new Point2D(p2.x + lineConfig.straight_len*vecX, p2.y + lineConfig.straight_len*vecY);

        start.setEnd(bP1);
        middle.setStart(bP1);
        middle.setEnd(bP2);
        end.setStart(bP2);

        g.setColor(lineConfig.color);
        Shape sp = new Ellipse2D.Double((bP1.x - strokeSize/2.),(bP1.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((bP2.x - strokeSize/2.),(bP2.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);

        // g.setColor(lineConfig.color);
        // g.fillOval((int) bP1.x,(int) bP1.y,(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) bP2.x,(int) bP2.y,(int) strokeSize,(int) strokeSize);

        start.drawFermion(g);
        if (lt == LineType.PHOTON) {
          middle.drawPhoton(g);
        } else if (lt == LineType.GLUON) {
          middle.drawGluon(g, noStraightClipping);
        }
        end.drawFermion(g);
      }


    } else { // curved line

      calculateArch();
      if (getStart().equals(getEnd())) {
        g.rotate(circAngle, getStartX(), getStartY());
      }

      if (lineConfig.showArrow) { // with arrow
        double archEndGes = Math.atan2(getEnd().y - center.y, getEnd().x - center.x);
        double archStartGes = Math.atan2(getStart().y - center.y, getStart().x - center.x);
        double deltaArchGes = (-archStartGes + archEndGes) * -Math.signum(height);

        while (deltaArchGes > 2 * Math.PI) {
          deltaArchGes -= 2 * Math.PI;
        }
        while (deltaArchGes <= 0) {
          deltaArchGes += 2 * Math.PI;
        }

        double len = - deltaArch * radius;

        double arch1 = -deltaArchGes * lineConfig.straight_len/len;
        double arch2 = -deltaArchGes * (lineConfig.arrowPlace-lineConfig.straight_len_arrow/len-lineConfig.arrowSize/len/2.);
        double arch3 = -deltaArchGes * (lineConfig.arrowPlace+lineConfig.straight_len_arrow/len+lineConfig.arrowSize/len/2.);
        double arch4 = -deltaArchGes * (1.-lineConfig.straight_len/len);

        double partArch1 = this.deltaArch * (lineConfig.arrowPlace-lineConfig.straight_len_arrow/len-lineConfig.arrowSize/len/2.);
        double partArch2 = this.deltaArch * (lineConfig.arrowPlace+lineConfig.straight_len_arrow/len+lineConfig.arrowSize/len/2.);
        double placeArch1 = this.archStart + partArch1;
        double placeArch2 = this.archStart + partArch2;
        double x1;
        double x2;
        double y1;
        double y2;
        if (this.height > 0) {
          y1 = this.center.y + radius * Math.sin(placeArch1);
          x1 = this.center.x + radius * Math.cos(placeArch1);
          y2 = this.center.y + radius * Math.sin(placeArch2);
          x2 = this.center.x + radius * Math.cos(placeArch2);
        } else {
          y1 = this.center.y - radius * Math.sin(placeArch1);
          x1 = this.center.x - radius * Math.cos(placeArch1);
          y2 = this.center.y - radius * Math.sin(placeArch2);
          x2 = this.center.x - radius * Math.cos(placeArch2);
        }

        // System.out.println(String.valueOf(deltaArchGes) + " " + String.valueOf(deltaArch));

        if (height < 0.) {
          arch1 -= archStartGes;
          arch1 *= -1;
          arch1 += Math.PI / 2;
          // double tmp = arch2;
          // arch2 = arch3;
          // arch3 = tmp;
          arch2 -= archStartGes;
          // arch2 *= -1;
          // arch2 += Math.PI / 2;
          arch3 -= archStartGes;
          // arch3 -= Math.PI / 2;
          // arch3 *= -1;
          arch4 -= archStartGes;
          arch4 *= -1;
        } else {
          arch1 += archStartGes;
          arch1 += Math.PI / 2;
          arch2 += archStartGes;
          arch2 += Math.PI / 2;
          arch3 += archStartGes;
          arch3 += Math.PI / 2;
          arch4 += archStartGes;
        }
        arch4 += Math.PI / 2;

        double pointX1 = +center.x - Math.signum(height) * radius * Math.sin(-arch1);
        double pointY1 = +center.y - Math.signum(height) * radius * Math.cos(-arch1);
        double pointX2 = +center.x - Math.signum(height) * radius * Math.sin(-arch2);
        double pointY2 = +center.y - Math.signum(height) * radius * Math.cos(-arch2);
        double pointX3 = +center.x - Math.signum(height) * radius * Math.sin(-arch3);
        double pointY3 = +center.y - Math.signum(height) * radius * Math.cos(-arch3);
        double pointX4 = +center.x - Math.signum(height) * radius * Math.sin(-arch4);
        double pointY4 = +center.y - Math.signum(height) * radius * Math.cos(-arch4);

        Point2D ankerPoint1 = new Point2D(pointX1, pointY1);
        Point2D ankerPoint2 = new Point2D(x1, y1);
        Point2D ankerPoint3 = new Point2D(x2, y2);
        Point2D ankerPoint4 = new Point2D(pointX4, pointY4);

        LineConfig fermioned = new LineConfig(lineConfig);
        fermioned.lineType = LineType.FERMION;

        FeynLine helperLine1 = new FeynLine(getStart(), ankerPoint1, fermioned, FeynLine.getHeight(radius, getStart(), ankerPoint1, center));
        FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, lineConfig, FeynLine.getHeight(radius, ankerPoint1, ankerPoint2, center));
        FeynLine helperLine3 = new FeynLine(ankerPoint2, ankerPoint3, fermioned, FeynLine.getHeight(radius, ankerPoint2, ankerPoint3, center));
        FeynLine helperLine4 = new FeynLine(ankerPoint3, ankerPoint4, lineConfig, FeynLine.getHeight(radius, ankerPoint3, ankerPoint4, center));
        FeynLine helperLine5 = new FeynLine(ankerPoint4, getEnd(), fermioned, FeynLine.getHeight(radius, ankerPoint4, getEnd(), center));
        helperLine1.showDesc = false;
        helperLine2.showDesc = false;
        helperLine3.showDesc = false;
        helperLine4.showDesc = false;
        helperLine5.showDesc = false;

        if (lineConfig.straight_len > 0.)
          helperLine1.drawFermion(g);
        if (lt == LineType.PHOTON) {
          helperLine2.drawPhoton(g);
        } else if (lt == LineType.GLUON) {
          helperLine2.drawGluon(g, noStraightClipping);
        }
        // if (lineConfig.straight_len_arrow > 0.)
          helperLine3.drawFermion(g);
        if (lt == LineType.PHOTON) {
          helperLine4.drawPhoton(g);
        } else if (lt == LineType.GLUON) {
          helperLine4.drawGluon(g, noStraightClipping);
        }
        if (lineConfig.straight_len > 0.)
          helperLine5.drawFermion(g);

        g.setColor(lineConfig.color);
        Shape sp = new Ellipse2D.Double((ankerPoint1.x - strokeSize/2.),(ankerPoint1.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((ankerPoint2.x - strokeSize/2.),(ankerPoint2.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((ankerPoint3.x - strokeSize/2.),(ankerPoint3.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((ankerPoint4.x - strokeSize/2.),(ankerPoint4.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);

        // g.setColor(lineConfig.color);
        // g.fillOval((int) ankerPoint1.x,(int) ankerPoint1.y,(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) ankerPoint2.x,(int) ankerPoint2.y,(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) ankerPoint3.x,(int) ankerPoint3.y,(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) ankerPoint4.x,(int) ankerPoint4.y,(int) strokeSize,(int) strokeSize);

      } else { // without arrow
        double archEndGes = Math.atan2(getEnd().y - center.y, getEnd().x - center.x);
        double archStartGes = Math.atan2(getStart().y - center.y, getStart().x - center.x);
        double deltaArchGes = (-archStartGes + archEndGes) * -Math.signum(height);

        while (deltaArchGes > 2 * Math.PI) {
          deltaArchGes -= 2 * Math.PI;
        }
        while (deltaArchGes <= 0) {
          deltaArchGes += 2 * Math.PI;
        }

        double len = deltaArch*Math.abs(radius);
        double arch1 = -deltaArchGes * lineConfig.straight_len/len;
        double arch2 = -deltaArchGes * (1.-lineConfig.straight_len/len);

        if (height < 0.) {
          arch1 -= archStartGes;
          arch1 *= -1;
          arch1 += Math.PI / 2;
          arch2 -= archStartGes;
          arch2 *= -1;
        } else {
          arch1 += archStartGes;
          arch1 += Math.PI / 2;
          arch2 += archStartGes;
        }
        arch2 += Math.PI / 2;

        double pointX1 = +center.x - Math.signum(height) * radius * Math.sin(-arch1);
        double pointY1 = +center.y - Math.signum(height) * radius * Math.cos(-arch1);
        double pointX2 = +center.x - Math.signum(height) * radius * Math.sin(-arch2);
        double pointY2 = +center.y - Math.signum(height) * radius * Math.cos(-arch2);

        Point2D ankerPoint1 = new Point2D(pointX1, pointY1);
        Point2D ankerPoint2 = new Point2D(pointX2, pointY2);

        LineConfig fermioned = new LineConfig(lineConfig);
        fermioned.lineType = LineType.FERMION;

        FeynLine helperLine1 = new FeynLine(getStart(), ankerPoint1, fermioned, FeynLine.getHeight(radius, getStart(), ankerPoint1, center));
        double he = FeynLine.getHeight(radius, ankerPoint1, ankerPoint2, center);
        if (lineConfig.straight_len == 0. && Math.abs(he) == 0. && getStart().equals(getEnd())) he = this.height;
        FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, lineConfig, he);
        FeynLine helperLine3 = new FeynLine(ankerPoint2, getEnd(), fermioned, FeynLine.getHeight(radius, ankerPoint2, getEnd(), center));
        helperLine1.showDesc = false;
        helperLine2.showDesc = false;
        helperLine3.showDesc = false;
        if (lineConfig.straight_len > 0.)
          helperLine1.drawFermion(g);
        if (lt == LineType.PHOTON) {
          helperLine2.drawPhoton(g);
        } else if (lt == LineType.GLUON) {
          helperLine2.drawGluon(g, noStraightClipping);
        }
        if (lineConfig.straight_len > 0.)
          helperLine3.drawFermion(g);

        g.setColor(lineConfig.color);
        Shape sp = new Ellipse2D.Double((ankerPoint1.x - strokeSize/2.),(ankerPoint1.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);
        sp = new Ellipse2D.Double((ankerPoint2.x - strokeSize/2.),(ankerPoint2.y - strokeSize/2.),strokeSize,strokeSize);
        g.fill(sp);

        // g.setColor(lineConfig.color);
        // g.fillOval((int) ankerPoint1.x,(int) ankerPoint1.y,(int) strokeSize,(int) strokeSize);
        // g.fillOval((int) ankerPoint2.x,(int) ankerPoint2.y,(int) strokeSize,(int) strokeSize);
      }
      if (getStart().equals(getEnd())) {
        g.rotate(-circAngle, getStartX(), getStartY());
      }
    }
  }

  private void drawArrowAtPlace(Graphics2D g) {
    Point2D p1 = new Point2D((double) line.getStartX(), (double) line.getStartY());
    Point2D p2 = new Point2D((double) line.getEndX(), (double) line.getEndY());

    if (Math.abs(height) < .1) {

      Point2D arrowPoint = new Point2D(p1.x + (p2.x - p1.x) * this.lineConfig.arrowPlace, p1.y + (p2.y - p1.y) * this.lineConfig.arrowPlace);
      this.drawArrow(g, arrowPoint);

    } else {

      double arch1 = -deltaArch * this.lineConfig.arrowPlace;
      arch1 -= archStart;
      arch1 *= -1;
      arch1 += Math.PI / 2;

      Point2D topPoint = new Point2D(
          (this.center.x - Math.signum(this.height) * this.radius * Math.sin(-arch1)),
          (this.center.y - Math.signum(this.height) * this.radius * Math.cos(-arch1))
      );

      this.drawArrow(g, topPoint);
    }
  }
  /**
   * draws FERMION/PLAIN lines
   * @param g the graphics object to draw to
   */
  private void drawFermion(Graphics2D g) {
    Point2D p1 = new Point2D((double) line.getStartX(), (double) line.getStartY());
    Point2D p2 = new Point2D((double) line.getEndX(), (double) line.getEndY());

    // this.calculateArch(p1, p2);

    if (Math.abs(height) < .1) {
      path = new Line2D.Double(p1.x, p1.y, p2.x, p2.y);

      if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
        g.rotate(circAngle, this.getStart().x, this.getStart().y);
        g.draw(path);
        g.rotate(-circAngle, this.getStart().x, this.getStart().y);
      } else {
        g.draw(path);
      }


    } else {

      path = new Arc2D.Double();

      double r = Math.abs(radius);

      ((Arc2D.Double) path).width = Math.abs(radius) * 2;
      ((Arc2D.Double) path).height = Math.abs(radius) * 2;

      ((Arc2D.Double) path).x = center.x - r;
      ((Arc2D.Double) path).y = center.y - r;

      ((Arc2D.Double) path).start = this.archStart * 180 / Math.PI - 180 - angle / Math.PI * 360;
      ((Arc2D.Double) path).extent = this.deltaArch * 180 / Math.PI;

      if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
        g.rotate(circAngle, this.getStart().x, this.getStart().y);
        g.draw(path);
        g.rotate(-circAngle, this.getStart().x, this.getStart().y);
      } else {
        g.draw(path);
      }

    }
  }
  /**
   * draws PHOTON/WAVE lines
   * @param g the graphics object to draw to
   */
  private void drawPhoton(Graphics2D g) {
    Point2D p1 = new Point2D((double) line.getStartX(), (double) line.getStartY());
    Point2D p2 = new Point2D((double) line.getEndX(), (double) line.getEndY());

    path = new java.awt.geom.Path2D.Double();
    ((java.awt.geom.Path2D.Double) path).moveTo(p1.x, p1.y);

    int wiggleNum;
    if (Math.abs(height) > .1) {
      wiggleNum = Math.max((int) Math.round(2 * (Math.abs(radius) * Math.abs(deltaArch) / lineConfig.wiggleLength)), 2);
    } else {
      wiggleNum = Math.max((int) Math.round(2 * (Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y)) / lineConfig.wiggleLength)), 2);
    }

    if (Math.abs(height) < .1) {
      boolean even = false;

      double nY = (p2.x - p1.x) / (Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)));
      double nX = -(p2.y - p1.y) / (Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)));

      Point2D normal = new Point2D(nX, nY);
      Point2D tangent = new Point2D(nY, -nX);

      for (int i = 0; i < wiggleNum; i++) {

        Point2D pt1 = new Point2D(p1.x + (p2.x - p1.x) * 1.0 / wiggleNum * (i), p1.y + (p2.y - p1.y) * 1.0 / wiggleNum * (i));
        Point2D pt2 = new Point2D(p1.x + (p2.x - p1.x) * 1.0 / wiggleNum * (i + 1), p1.y + (p2.y - p1.y) * 1.0 / wiggleNum * (i + 1));

        double wiggleLen = pt1.distance(pt2);
        double sharpness = lineConfig.wiggleOffset;
        double invSharpness = wiggleLen/2.-sharpness;

        Point2D pt3 = pt1.add(tangent.mulScalar(wiggleLen/2.)).add(normal.mulScalar(lineConfig.wiggleHeight/2.));
        Point2D ctrl1 = pt3.add(tangent.mulScalar(-invSharpness));
        Point2D ctrl2 = pt3.add(tangent.mulScalar(invSharpness));

        Point2D ctrl3 = pt1.add(tangent.mulScalar(sharpness/2.)).add(normal.mulScalar(lineConfig.wiggleHeight/4.));
        Point2D ctrl4 = pt2.add(tangent.mulScalar(-sharpness/2.)).add(normal.mulScalar(lineConfig.wiggleHeight/4.));

        if (even) {
          pt3 = pt1.add(tangent.mulScalar(wiggleLen/2.)).add(normal.mulScalar(-lineConfig.wiggleHeight/2.));
          ctrl1 = pt3.add(tangent.mulScalar(-invSharpness));
          ctrl2 = pt3.add(tangent.mulScalar(invSharpness));
          ctrl3 = pt1.add(tangent.mulScalar(sharpness/2.)).add(normal.mulScalar(-lineConfig.wiggleHeight/4.));
          ctrl4 = pt2.add(tangent.mulScalar(-sharpness/2.)).add(normal.mulScalar(-lineConfig.wiggleHeight/4.));
        }

        ((java.awt.geom.Path2D.Double) path).curveTo(ctrl3.x, ctrl3.y, ctrl1.x, ctrl1.y, pt3.x, pt3.y);
        ((java.awt.geom.Path2D.Double) path).curveTo(ctrl2.x, ctrl2.y, ctrl4.x, ctrl4.y, pt2.x, pt2.y);

        even = !even;
      }
    } else {
      boolean even = true;
      for (int i = 0; i < wiggleNum; i++) {

        double archFrom = deltaArch / wiggleNum * (i) + archStart;
        double archTo = deltaArch / wiggleNum * (i + 1.) + archStart;
        double archMid = deltaArch / wiggleNum * (i + 1./2.) + archStart;

        double r = Math.abs(radius);
        double sgn = -1.;
        double sgn2 = 1.;
        if (even) {
          sgn *= -1.;
        }
        if (height<0.) {
          sgn *= -1.;
          sgn2 *= -1.;
        }

        double wiggleLen = Math.abs(r*deltaArch)/wiggleNum;
        double sharpness = lineConfig.wiggleOffset;
        double invSharpness = wiggleLen/2.-sharpness;

        Point2D pt1 = new Point2D(r * Math.cos(archFrom) + center.x, r * Math.sin(archFrom) + center.y);
        Point2D pt2 = new Point2D(r * Math.cos(archTo) + center.x, r * Math.sin(archTo) + center.y);
        Point2D pt3 = new Point2D((r + sgn*lineConfig.wiggleHeight/2.) * Math.cos(archMid) + center.x, (r + sgn*lineConfig.wiggleHeight/2.) * Math.sin(archMid) + center.y);

        Point2D normal = new Point2D(sgn*Math.cos(archMid), sgn*Math.sin(archMid));
        Point2D tangent = new Point2D(sgn2*Math.sin(archMid), -sgn2*Math.cos(archMid));

        Point2D ctrl1 = pt3.add(tangent.mulScalar(-invSharpness));
        Point2D ctrl2 = pt3.add(tangent.mulScalar(invSharpness));

        normal = new Point2D(sgn*Math.cos(archFrom), sgn*Math.sin(archFrom));
        tangent = new Point2D(sgn2*Math.sin(archFrom), -sgn2*Math.cos(archFrom));
        Point2D ctrl3 = pt1.add(tangent.mulScalar(sharpness/2.)).add(normal.mulScalar(lineConfig.wiggleHeight/4.));
        normal = new Point2D(sgn*Math.cos(archTo), sgn*Math.sin(archTo));
        tangent = new Point2D(sgn2*Math.sin(archTo), -sgn2*Math.cos(archTo));
        Point2D ctrl4 = pt2.add(tangent.mulScalar(-sharpness/2.)).add(normal.mulScalar(lineConfig.wiggleHeight/4.));


        ((java.awt.geom.Path2D.Double) path).curveTo(ctrl3.x, ctrl3.y, ctrl1.x, ctrl1.y, pt3.x, pt3.y);
        ((java.awt.geom.Path2D.Double) path).curveTo(ctrl2.x, ctrl2.y, ctrl4.x, ctrl4.y, pt2.x, pt2.y);
        even = !even;
      }

    }

    if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
      g.rotate(circAngle, this.getStart().x, this.getStart().y);
      g.draw(path);
      g.rotate(-circAngle, this.getStart().x, this.getStart().y);
    } else {
      g.draw(path);
    }


  }
  /**
   * draws GLUON/SPIRAL lines
   * @param g the graphics object to draw to
   * @param noStraightClipping whether or not the line clips to being straight
   * when the absolute of the height is below 10
   */
  private void drawGluon(Graphics2D g, boolean noStraightClipping) {
    Point2D p1 = new Point2D((double) line.getStartX(), (double) line.getStartY());
    Point2D p2 = new Point2D((double) line.getEndX(), (double) line.getEndY());
    double offset = lineConfig.wiggleOffset;

    path = new java.awt.geom.Path2D.Double();
    ((java.awt.geom.Path2D.Double) path).moveTo(p1.x, p1.y);

    int wiggleNum;
    if (Math.abs(height) > .1) {
      wiggleNum = Math.max((int) Math.round((Math.abs(radius) * Math.abs(deltaArch)/*-lineConfig.wiggleOffset*/) / lineConfig.wiggleLength) - 1, ((int) Math.round(Math.abs(deltaArch / (Math.PI / 4)))));
    } else {
      wiggleNum = Math.max((int) Math.round((Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y))/*-lineConfig.wiggleOffset*/) / lineConfig.wiggleLength) - 1, 1);
    }

    if (Math.abs(height) < .1) {

      double nY = (p2.x - p1.x) / (Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)));
      double nX = -(p2.y - p1.y) / (Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)));

      Point2D normal = new Point2D(-nX, -nY);
      Point2D tangent = new Point2D(nY, -nX);

      p1 = p1.add(tangent.mulScalar(-offset/2.));
      p2 = p2.add(tangent.mulScalar(offset/2.));

      for (int i = 0; i < wiggleNum; i++) {

        Point2D pt1 = new Point2D(p1.x + (p2.x - p1.x - offset*tangent.x) * 1.0 / wiggleNum * (i), p1.y + (p2.y - p1.y - offset*tangent.y) * 1.0 / wiggleNum * (i));
        Point2D pt2 = new Point2D(p1.x + (p2.x - p1.x - offset*tangent.x) * 1.0 / wiggleNum * (i + 1), p1.y + (p2.y - p1.y - offset*tangent.y) * 1.0 / wiggleNum * (i + 1));

        double wiggleLen = pt1.distance(pt2);

        Point2D pt3 = pt1.add(normal.mulScalar(lineConfig.wiggleHeight/2.)).add(tangent.mulScalar((wiggleLen+offset)/2.));
        Point2D pt4 = pt2.add(tangent.mulScalar(offset));
        Point2D pt5 = pt4.add(tangent.mulScalar(-offset/2.)).add(normal.mulScalar(-lineConfig.wiggleHeight/2.+lineConfig.wiggleAsymmetry));

        if (i == 0) {
          pt1 = pt1.add(tangent.mulScalar(offset/2.));
        }
        if (i == wiggleNum-1) {
          pt4 = pt4.add(tangent.mulScalar(-offset/2.));
        }

        double glue = 5./8.;
        double ratio = lineConfig.wiggleHeight/2./(lineConfig.wiggleHeight/2.-lineConfig.wiggleAsymmetry);

        Point2D ct1 = pt1.add(normal.mulScalar(lineConfig.wiggleHeight/2.*glue)).add(tangent.mulScalar(wiggleLen/16.));
        Point2D ct2 = pt3.add(tangent.mulScalar(-(wiggleLen+offset)/2.*glue));
        Point2D ct3 = pt3.add(tangent.mulScalar((wiggleLen+offset)/2.*glue));
        Point2D ct4 = pt4.add(normal.mulScalar(lineConfig.wiggleHeight/2.*glue)).add(tangent.mulScalar(-wiggleLen/16.));
        Point2D ct5 = pt4.add(normal.mulScalar((-lineConfig.wiggleHeight/2.+lineConfig.wiggleAsymmetry)*glue)).add(tangent.mulScalar(wiggleLen/16./ratio));
        Point2D ct6 = pt5.add(tangent.mulScalar(offset*glue*glue));
        Point2D ct7 = pt5.add(tangent.mulScalar(-offset*glue*glue));
        Point2D ct8 = pt2.add(normal.mulScalar((-lineConfig.wiggleHeight/2.+lineConfig.wiggleAsymmetry)*glue)).add(tangent.mulScalar(-wiggleLen/16./ratio));

        if (i == 0) {
          ct2 = ct2.add(tangent.mulScalar(offset/2.));
          pt3 = pt3.add(tangent.mulScalar(offset/4.));
        }
        if (i == wiggleNum -1) {
          pt3 = pt3.add(tangent.mulScalar(-offset/4.));
          ct3 = ct3.add(tangent.mulScalar(-offset/2.));
        }

        ((java.awt.geom.Path2D.Double) path).curveTo(ct1.x, ct1.y, ct2.x, ct2.y, pt3.x, pt3.y);
        ((java.awt.geom.Path2D.Double) path).curveTo(ct3.x, ct3.y, ct4.x, ct4.y, pt4.x, pt4.y);
        if (i != wiggleNum-1) {
          ((java.awt.geom.Path2D.Double) path).curveTo(ct5.x, ct5.y, ct6.x, ct6.y, pt5.x, pt5.y);
          ((java.awt.geom.Path2D.Double) path).curveTo(ct7.x, ct7.y, ct8.x, ct8.y, pt2.x, pt2.y);
        }
      }

    } else {

      if (height>0.) offset *= -1;

      double offsetAng = offset/Math.abs(radius);

      for (int i = 0; i < wiggleNum; i++) {
        double archFrom = (deltaArch) / wiggleNum * (i) + archStart - offsetAng/2.;
        double archTo = (deltaArch) / wiggleNum * (i + 1.) + archStart - offsetAng/2.;
        double archMid = (deltaArch) / wiggleNum * (i + 1./2.) + archStart;

        double r = Math.abs(radius);
        double sgn = 1.;
        double sgn2 = -1.;
        if (height>0.) {
          sgn *= -1.;
          sgn2 *= -1.;
        }

        if (i == 0) {
          archFrom = archFrom + offsetAng/2.;
        }
        if (i == wiggleNum-1) {
          archTo = archTo - offsetAng/2.;
        }

        double wiggleLen = Math.abs(r*(deltaArch+offsetAng))/wiggleNum;
        double archOffset = archTo + offsetAng;
        double arch5 = archTo + offsetAng/2.;

        Point2D pt1 = new Point2D(r * Math.cos(archFrom) + center.x, r * Math.sin(archFrom) + center.y);
        Point2D pt2 = new Point2D(r * Math.cos(archTo) + center.x, r * Math.sin(archTo) + center.y);
        Point2D pt3 = new Point2D((r + sgn*lineConfig.wiggleHeight/2.) * Math.cos(archMid) + center.x, (r + sgn*lineConfig.wiggleHeight/2.) * Math.sin(archMid) + center.y);
        Point2D pt4 = new Point2D((r) * Math.cos(archOffset) + center.x, (r) * Math.sin(archOffset) + center.y);
        Point2D pt5 = new Point2D((r - sgn*lineConfig.wiggleHeight/2. + sgn*lineConfig.wiggleAsymmetry) * Math.cos(arch5) + center.x, (r - sgn*lineConfig.wiggleHeight/2. + sgn*lineConfig.wiggleAsymmetry) * Math.sin(arch5) + center.y);

        double glue = 5./8.;
        double ratio = lineConfig.wiggleHeight/2./(lineConfig.wiggleHeight/2.-lineConfig.wiggleAsymmetry);

        Point2D normal = new Point2D(sgn*Math.cos(archFrom), sgn*Math.sin(archFrom));
        Point2D tangent = new Point2D(sgn2*Math.sin(archFrom), -sgn2*Math.cos(archFrom));
        Point2D ct1 = pt1.add(normal.mulScalar(lineConfig.wiggleHeight/2.*glue)).add(tangent.mulScalar(wiggleLen/16.));

        normal = new Point2D(sgn*Math.cos(archMid), sgn*Math.sin(archMid));
        tangent = new Point2D(sgn2*Math.sin(archMid), -sgn2*Math.cos(archMid));
        Point2D ct2 = pt3.add(tangent.mulScalar(-(wiggleLen+sgn*offset)/2.*glue));
        Point2D ct3 = pt3.add(tangent.mulScalar((wiggleLen+sgn*offset)/2.*glue));

        if (i == 0) {
          ct2 = ct2.add(tangent.mulScalar(sgn*offset/2.));
          pt3 = pt3.add(tangent.mulScalar(sgn*offset/4.));
        }
        if (i == wiggleNum -1) {
          pt3 = pt3.add(tangent.mulScalar(-sgn*offset/4.));
          ct3 = ct3.add(tangent.mulScalar(-sgn*offset/2.));
        }

        normal = new Point2D(sgn*Math.cos(archOffset), sgn*Math.sin(archOffset));
        tangent = new Point2D(sgn2*Math.sin(archOffset), -sgn2*Math.cos(archOffset));
        Point2D ct4 = pt4.add(normal.mulScalar(lineConfig.wiggleHeight/2.*glue)).add(tangent.mulScalar(-wiggleLen/16.));;
        Point2D ct5 = pt4.add(normal.mulScalar((-lineConfig.wiggleHeight/2.+lineConfig.wiggleAsymmetry)*glue)).add(tangent.mulScalar(wiggleLen/16./ratio));

        normal = new Point2D(sgn*Math.cos(arch5), sgn*Math.sin(arch5));
        tangent = new Point2D(-Math.sin(arch5), Math.cos(arch5));
        Point2D ct6 = pt5.add(tangent.mulScalar(offset*glue*glue));
        Point2D ct7 = pt5.add(tangent.mulScalar(-offset*glue*glue));

        normal = new Point2D(sgn*Math.cos(archTo), sgn*Math.sin(archTo));
        tangent = new Point2D(sgn2*Math.sin(archTo), -sgn2*Math.cos(archTo));
        Point2D ct8 = pt2.add(normal.mulScalar((-lineConfig.wiggleHeight/2.+lineConfig.wiggleAsymmetry)*glue)).add(tangent.mulScalar(-wiggleLen/16./ratio));

        ((java.awt.geom.Path2D.Double) path).curveTo(ct1.x, ct1.y, ct2.x, ct2.y, pt3.x, pt3.y);
        ((java.awt.geom.Path2D.Double) path).curveTo(ct3.x, ct3.y, ct4.x, ct4.y, pt4.x, pt4.y);
        if (i != wiggleNum-1) {
          ((java.awt.geom.Path2D.Double) path).curveTo(ct5.x, ct5.y, ct6.x, ct6.y, pt5.x, pt5.y);
          ((java.awt.geom.Path2D.Double) path).curveTo(ct7.x, ct7.y, ct8.x, ct8.y, pt2.x, pt2.y);
        }
      }

    }
    if (this.getStart().equals(this.getEnd()) && Math.abs(this.height) > .1) {
      g.rotate(circAngle, this.getStart().x, this.getStart().y);
      g.draw(path);
      g.rotate(-circAngle, this.getStart().x, this.getStart().y);
    } else {
      g.draw(path);
    }

  }

  private void calculateHeight(int sign) {

      double csquared = (getStart().x-getEnd().x)*(getStart().x-getEnd().x)/4.+(getStart().y-getEnd().y)*(getStart().y-getEnd().y)/4.;
      double a = Math.sqrt(radius*radius-csquared);
      height = radius - a;
      height = height * sign;
      calculateArch();
  }

  /**
   * same as calculateArch(Point2D p1, Point2D p2) just with p1, p2 replaced by the start and end of the line
   */
  public void calculateArch() {
    this.calculateArch(this.getStart(), this.getEnd());
  }

  /**
   * Calculates all other parameter from height, start and end point. Calculates {@link #archStart}, {@link #archEnd}, {@link #deltaArch}, {@link #angle}, {@link #center} and {@link #radius}.
   *
   * @param p1 start point
   * @param p2 end point
   */
  public void calculateArch(Point2D p1, Point2D p2) {
    double y = p2.y - p1.y;
    double x = p2.x - p1.x;
    angle = Math.atan2(y, x);

    if (Math.abs(height) > 0) {

      double tendon = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));

      radius = (tendon * tendon / (8.0 * height) + height / 2.0);

      center = new Point2D(
          p1.x + (p2.x - p1.x) / 2d + (radius - height) * Math.sin(angle),
          p1.y + (p2.y - p1.y) / 2d - (radius - height) * Math.cos(angle)
      );

      archStart = Math.atan2(p1.y - center.y, p1.x - center.x);
      archEnd = Math.atan2(p2.y - center.y, p2.x - center.x);

      while (archStart < 0)
        archStart += Math.PI * 2d;
      while(archStart > 2d*Math.PI)
        archStart -= Math.PI * 2d;

      while (archEnd < 0)
        archEnd += Math.PI * 2d;
      while(archEnd > 2d*Math.PI)
        archEnd -= Math.PI * 2d;

      deltaArch = -archStart + archEnd;

      deltaArch = clipAngle(deltaArch, height);
    }
  }
  /**
   * Converts point p to polar coordinates using the {@link #center} of this {@link game.FeynLine}.
   *
   * @param p point in cartesian space (x, y)
   * @return point in polar coordinates (radius, phi)
   */
  public Point2D toPolar(Point2D p) {
    double dx = -center.x + p.x;
    double dy = -center.y + p.y;
    Point2D returnVal = new Point2D(Math.sqrt(dx * dx + dy * dy), (Math.atan2(p.y - center.y, p.x - center.x)));
    return returnVal;
  }
  /**
   * Converts point p from polar to cartesian coordinates using the {@link #center} of this {@link game.FeynLine}.
   *
   * @param p point in polar coordinates (radius, phi)
   * @return point in cartesian coordinates (x, y)
   */
  public Point2D toCart(Point2D p) {
    Point2D returnVal = new Point2D(p.x * Math.cos(p.y) + center.x, p.x * Math.sin(p.y) + center.y);
    return returnVal;
  }

  /**
   * function to calculate and get the length of the line
   * @return length of the line
   */
  public double length() {

    if (Math.abs(height) < .1) {
      double dx = line.getEndX() - line.getStartX();
      double dy = line.getEndY() - line.getStartY();

      return Math.sqrt(dx * dx + dy * dy);
    }

    return Math.abs(2 * Math.PI * radius * deltaArch / (2 * Math.PI));
  }
  /**
   * Iterates to a random color new color.
   */
  public void nextColor() {
    lineConfig.color = new Color((int) Math.random(), (int) Math.random(), (int) Math.random());
  }
  /**
   * generates a string representation of the object
   * @return the string representation
   */
  public String toString() {
    return this.getStart() + " to " + this.getEnd() + " as " + lineConfig.lineType;
  }
  
  /**
   * checks wether end or start is fixed
   *
   * @return false if one end is not fixed, true otherwise
   */
  public Boolean isFixed() {
    return fixEnd || fixStart;
  }
  /**
   * checks wether or not this intersects other and gives the place of intersection if possible
   * @param other the line to compare to
   * @return a list of intersections between these lines the first entry of each of the entries in the list
   * gives the position relative to the line and the second the length of the intersection relative to the length
   * of the line
   */
  public ArrayList<Map.Entry<Double, Double>> intersects(FeynLine other) {
    ArrayList<Map.Entry<Double, Double>> list = new ArrayList<>();

    if (this.getStart().equals(other.getStart()) || this.getEnd().equals(other.getStart()) || this.getEnd().equals(other.getEnd()) || this.getStart().equals(other.getEnd()))
      return list;

    if (Math.abs(this.height) < .1 && Math.abs(other.height) < .1) {

      double partThis = Point2D.det(
        new Point2D(
          this.getStartX() - other.getStartX(),
          this.getStartY() - other.getStartY()
        ),
        new Point2D(
          other.getStartX() - other.getEndX(),
          other.getStartY() - other.getEndY()
        )
      );
      partThis /= Point2D.det(
        new Point2D(
          this.getStartX() - this.getEndX(),
          this.getStartY() - this.getEndY()
        ),
        new Point2D(
          other.getStartX() - other.getEndX(),
          other.getStartY() - other.getEndY()
        )
      );

      double partOther = Point2D.det(
        new Point2D(
          other.getStartX() - this.getStartX(),
          other.getStartY() - this.getStartY()
        ),
        new Point2D(
          this.getStartX() - this.getEndX(),
          this.getStartY() - this.getEndY()
        )
      );
      partOther /= Point2D.det(
        new Point2D(
          other.getStartX() - other.getEndX(),
          other.getStartY() - other.getEndY()
        ),
        new Point2D(
          this.getStartX() - this.getEndX(),
          this.getStartY() - this.getEndY()
        )
      );

      if (partOther <= 1 && partThis <= 1 && partOther >= 0 && partThis >= 0) {

        double len = this.getStart().distance(this.getEnd());
        double width = other.lineConfig.getWidth();
        double m1 = (this.getStartY() - this.getEndY())/(this.getStartX() - this.getEndX());
        double m2 = (other.getStartY() - other.getEndY())/(other.getStartX() - other.getEndX());
        double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
        double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
        list.add(new AbstractMap.SimpleEntry<Double, Double>(partThis, (20 + width/Math.sin(Math.atan(tanAlph1)) + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
        return list;
      } else {
        return list;
      }

    } else if (Math.abs(this.height) < .1 && Math.abs(other.height) >= .1) {
      other.calculateArch();

      Point2D center = new Point2D(other.center.x, other.center.y);
      if (other.getStart().equals(other.getEnd()))
        center.rotate(- other.circAngle, other.getStartX(), other.getStartY());

      double a = - (this.getStartX()-this.getEndX())*(this.getStartX()-this.getEndX()) - (this.getStartY()-this.getEndY())*(this.getStartY()-this.getEndY());

      double b = 2d*this.getStartX()*this.getStartX() - 2d*this.getStartX()*this.getEndX() - 2d*this.getStartX()*center.x + 2d*this.getEndX()*center.x + 2d*this.getStartY()*this.getStartY() - 2d*this.getStartY()*this.getEndY() - 2d*this.getStartY()*center.y + 2d*this.getEndY()*center.y;

      double c = - this.getStartX()*this.getStartX() + 2d*this.getStartX()*center.x - center.x*center.x + other.radius*other.radius - this.getStartY()*this.getStartY() + 2d*this.getStartY()*center.y - center.y*center.y;

      double dis = b*b - 4d*a*c;

      if (dis < 0) {
        return list;
      } else if (dis == 0) {
        double u = -b/2d/a;

        if (u < 0 || u > 1) {
          return list;
        }

        Point2D point = new Point2D(this.getStartX() + u*(this.getEndY()-this.getStartY()), this.getStartY() + u*(this.getEndY()-this.getStartY()));

        if (other.getStart().equals(other.getEnd()))
          point.rotate(- other.circAngle, other.getStartX(), other.getStartY());

        double arch = Math.atan2(point.y - center.y, point.x - center.x);
        while (arch < 0)
          arch += Math.PI * 2d;
        while(arch > 2d*Math.PI)
          arch -= Math.PI * 2d;

        if (other.height < 0) {

          if (other.getEnd().equals(other.getStart()) || (other.archStart < arch && arch < other.archEnd && other.archStart < other.archEnd) || ((other.archStart < arch || arch < other.archEnd) && other.archStart > other.archEnd)) {

            double h = this.lineConfig.getWidth() + other.lineConfig.getWidth();
            double blankLen = 2d*Math.sqrt(2d*other.radius*h - h*h);
            double len = Math.sqrt((this.getEndX() - this.getStartX())*(this.getEndX() - this.getStartX()) + (this.getEndY() - this.getStartY())*(this.getEndY() - this.getStartY()));
            list.add(new AbstractMap.SimpleEntry<Double, Double>(u, 20 + blankLen/len));
          }
        } else {
          if (other.getEnd().equals(other.getStart()) || (other.archStart > arch && arch > other.archEnd && other.archStart > other.archEnd) || ((other.archStart > arch || arch > other.archEnd) && other.archStart < other.archEnd)) {

            double h = this.lineConfig.getWidth() + other.lineConfig.getWidth();
            double blankLen = 2d*Math.sqrt(2d*other.radius*h - h*h);
            double len = Math.sqrt((this.getEndX() - this.getStartX())*(this.getEndX() - this.getStartX()) + (this.getEndY() - this.getStartY())*(this.getEndY() - this.getStartY()));
            list.add(new AbstractMap.SimpleEntry<Double, Double>(u, 20 + blankLen/len));
          }
        }

      } else {
        double u = -b/2d/a + Math.sqrt(dis)/2d/a;

        if (u > 0 && u < 1) {
          Point2D point = new Point2D(this.getStartX() + u*(this.getEndY()-this.getStartY()), this.getStartY() + u*(this.getEndY()-this.getStartY()));

          if (other.getStart().equals(other.getEnd()))
            point.rotate(other.circAngle, other.getStartX(), other.getStartY());

          double arch = Math.atan2(point.y - center.y, point.x - center.x);

          while (arch < 0)
            arch += Math.PI * 2d;
          while(arch > 2d*Math.PI)
            arch -= Math.PI * 2d;

//          System.out.println("arch: " + arch);
//          System.out.println("archStart: " + other.archStart);
//          System.out.println("archEnd: " + other.archEnd);

          if (other.height < 0) {

            if (other.getEnd().equals(other.getStart()) || (other.archStart < arch && arch < other.archEnd && other.archStart < other.archEnd) || ((other.archStart < arch || arch < other.archEnd) && other.archStart > other.archEnd)) {

              double m1 = (this.getStartY() - this.getEndY())/(this.getStartX() - this.getEndX());
              if (other.getStart().equals(other.getEnd()))
                point.rotate(- other.circAngle, other.getStartX(), other.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((this.getEndX() - this.getStartX())*(this.getEndX() - this.getStartX()) + (this.getEndY() - this.getStartY())*(this.getEndY() - this.getStartY()));
              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }

          } else {

            if (other.getEnd().equals(other.getStart()) || (other.archStart > arch && arch > other.archEnd && other.archStart > other.archEnd) || ((other.archStart > arch || arch > other.archEnd) && other.archStart < other.archEnd)) {

              double m1 = (this.getStartY() - this.getEndY())/(this.getStartX() - this.getEndX());
              if (other.getStart().equals(other.getEnd()))
                point.rotate(- other.circAngle, other.getStartX(), other.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((this.getEndX() - this.getStartX())*(this.getEndX() - this.getStartX()) + (this.getEndY() - this.getStartY())*(this.getEndY() - this.getStartY()));
              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }

          }
        }

        u = -b/2d/a - Math.sqrt(dis)/2d/a;

        if (u > 0 && u < 1) {
          Point2D point = new Point2D(this.getStartX() + u*(this.getEndY()-this.getStartY()), this.getStartY() + u*(this.getEndY()-this.getStartY()));

          if (other.getStart().equals(other.getEnd()))
            point.rotate(other.circAngle, other.getStartX(), other.getStartY());

          double arch = Math.atan2(point.y - center.y, point.x - center.x);
          while (arch < 0)
            arch += Math.PI * 2d;
          while(arch > 2d*Math.PI)
            arch -= Math.PI * 2d;

//          System.out.println("arch: " + arch);
//          System.out.println("archStart: " + other.archStart);
//          System.out.println("archEnd: " + other.archEnd);

          if (other.height < 0) {

            if (other.getEnd().equals(other.getStart()) || (other.archStart < arch && arch < other.archEnd && other.archStart < other.archEnd) || ((other.archStart < arch || arch < other.archEnd) && other.archStart > other.archEnd)) {

              double m1 = (this.getStartY() - this.getEndY())/(this.getStartX() - this.getEndX());
              if (other.getStart().equals(other.getEnd()))
                point.rotate(- other.circAngle, other.getStartX(), other.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((this.getEndX() - this.getStartX())*(this.getEndX() - this.getStartX()) + (this.getEndY() - this.getStartY())*(this.getEndY() - this.getStartY()));
              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }

          } else {

            if (other.getEnd().equals(other.getStart()) || (other.archStart > arch && arch > other.archEnd && other.archStart > other.archEnd) || ((other.archStart > arch || arch > other.archEnd) && other.archStart < other.archEnd)) {

              double m1 = (this.getStartY() - this.getEndY())/(this.getStartX() - this.getEndX());
              if (other.getStart().equals(other.getEnd()))
                point.rotate(- other.circAngle, other.getStartX(), other.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((this.getEndX() - this.getStartX())*(this.getEndX() - this.getStartX()) + (this.getEndY() - this.getStartY())*(this.getEndY() - this.getStartY()));
              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }

          }
        }

      }

    } else if (Math.abs(this.height) >= .1 && Math.abs(other.height) < .1) {
      this.calculateArch();

      Point2D center = new Point2D(this.center.x, this.center.y);
      if (this.getStart().equals(this.getEnd()))
        center.rotate(- this.circAngle, this.getStartX(), this.getStartY());

      double a = - (other.getStartX()-other.getEndX())*(other.getStartX()-other.getEndX()) - (other.getStartY()-other.getEndY())*(other.getStartY()-other.getEndY());

      double b = 2d*other.getStartX()*other.getStartX() - 2d*other.getStartX()*other.getEndX() - 2d*other.getStartX()*center.x + 2d*other.getEndX()*center.x + 2d*other.getStartY()*other.getStartY() - 2d*other.getStartY()*other.getEndY() - 2d*other.getStartY()*center.y + 2d*other.getEndY()*center.y;

      double c = - other.getStartX()*other.getStartX() + 2d*other.getStartX()*center.x - center.x*center.x + this.radius*this.radius - other.getStartY()*other.getStartY() + 2d*other.getStartY()*center.y - center.y*center.y;

      double dis = b*b - 4d*a*c;

      if (dis < 0) {
        return list;
      } else if (dis == 0) {
        double u = -b/2d/a;

        if (u < 0 || u > 1) {
          return list;
        }

        Point2D point = new Point2D(other.getStartX() + u*(other.getEndY()-other.getStartY()), other.getStartY() + u*(other.getEndY()-other.getStartY()));

        if (this.getStart().equals(this.getEnd()))
          point.rotate(- this.circAngle, this.getStartX(), this.getStartY());

        double arch = Math.atan2(point.y - center.y, point.x - center.x);
        while (arch < 0)
          arch += Math.PI * 2d;
        while(arch > 2d*Math.PI)
          arch -= Math.PI * 2d;

        if (this.height < 0) {

          if (this.getEnd().equals(this.getStart()) || (this.archStart < arch && arch < this.archEnd && this.archStart < this.archEnd) || ((this.archStart < arch || arch < this.archEnd) && this.archStart > this.archEnd)) {

            double h = other.lineConfig.getWidth() + this.lineConfig.getWidth();
            double blankLen = 2d*Math.sqrt(2d*this.radius*h - h*h);

            if (this.archStart < arch)
              u = Math.abs((arch - this.archStart)/this.deltaArch);
            else
              u = 1 - Math.abs((arch - this.archEnd)/this.deltaArch);

            double len = Math.sqrt((other.getEndX() - other.getStartX())*(other.getEndX() - other.getStartX()) + (other.getEndY() - other.getStartY())*(other.getEndY() - other.getStartY()));
            list.add(new AbstractMap.SimpleEntry<Double, Double>(u, 20 + blankLen/len));
          }

        } else {

          if (this.getEnd().equals(this.getStart()) || (this.archStart > arch && arch > this.archEnd && this.archStart > this.archEnd) || ((this.archStart > arch || arch > this.archEnd) && this.archStart < this.archEnd)) {

            double h = other.lineConfig.getWidth() + this.lineConfig.getWidth();
            double blankLen = 2d*Math.sqrt(2d*this.radius*h - h*h);

            if (this.archStart < arch)
              u = Math.abs((arch - this.archStart)/this.deltaArch);
            else
              u = 1 - Math.abs((arch - this.archEnd)/this.deltaArch);

            double len = Math.sqrt((other.getEndX() - other.getStartX())*(other.getEndX() - other.getStartX()) + (other.getEndY() - other.getStartY())*(other.getEndY() - other.getStartY()));
            list.add(new AbstractMap.SimpleEntry<Double, Double>(u, 20 + blankLen/len));
          }

        }

      } else {
        double u = -b/2d/a + Math.sqrt(dis)/2d/a;

        if (u > 0 && u < 1) {
          Point2D point = new Point2D(other.getStartX() + u*(other.getEndY()-other.getStartY()), other.getStartY() + u*(other.getEndY()-other.getStartY()));

          if (this.getStart().equals(this.getEnd()))
            point.rotate(this.circAngle, this.getStartX(), this.getStartY());

          double arch = Math.atan2(point.y - center.y, point.x - center.x);

          while (arch < 0)
            arch += Math.PI * 2d;
          while(arch > 2d*Math.PI)
            arch -= Math.PI * 2d;

          if (this.height < 0) {

            if (this.getEnd().equals(this.getStart()) || (this.archStart < arch && arch < this.archEnd && this.archStart < this.archEnd) || ((this.archStart < arch || arch < this.archEnd) && this.archStart > this.archEnd)) {

              double m1 = (other.getStartY() - other.getEndY())/(other.getStartX() - other.getEndX());
              if (this.getStart().equals(this.getEnd()))
                point.rotate(- this.circAngle, this.getStartX(), this.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((other.getEndX() - other.getStartX())*(other.getEndX() - other.getStartX()) + (other.getEndY() - other.getStartY())*(other.getEndY() - other.getStartY()));
              if (this.archStart < arch)
                u = Math.abs((arch - this.archStart)/this.deltaArch);
              else
                u = 1 - Math.abs((arch - this.archEnd)/this.deltaArch);

              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }
          } else {
            if (this.getEnd().equals(this.getStart()) || (this.archStart > arch && arch > this.archEnd && this.archStart > this.archEnd) || ((this.archStart > arch || arch > this.archEnd) && this.archStart < this.archEnd)) {

              double m1 = (other.getStartY() - other.getEndY())/(other.getStartX() - other.getEndX());
              if (this.getStart().equals(this.getEnd()))
                point.rotate(- this.circAngle, this.getStartX(), this.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((other.getEndX() - other.getStartX())*(other.getEndX() - other.getStartX()) + (other.getEndY() - other.getStartY())*(other.getEndY() - other.getStartY()));
              if (this.archStart < arch)
                u = Math.abs((arch - this.archStart)/this.deltaArch);
              else
                u = 1 - Math.abs((arch - this.archEnd)/this.deltaArch);

              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }
          }
        }

        u = -b/2d/a - Math.sqrt(dis)/2d/a;

        if (u > 0 && u < 1) {
          Point2D point = new Point2D(other.getStartX() + u*(other.getEndY()-other.getStartY()), other.getStartY() + u*(other.getEndY()-other.getStartY()));

          if (this.getStart().equals(this.getEnd()))
            point.rotate(this.circAngle, this.getStartX(), this.getStartY());

          double arch = Math.atan2(point.y - center.y, point.x - center.x);
          while (arch < 0)
            arch += Math.PI * 2d;
          while(arch > 2d*Math.PI)
            arch -= Math.PI * 2d;

          if (this.height < 0) {

            if (this.getEnd().equals(this.getStart()) || (this.archStart < arch && arch < this.archEnd && this.archStart < this.archEnd) || ((this.archStart < arch || arch < this.archEnd) && this.archStart > this.archEnd)) {

              double m1 = (other.getStartY() - other.getEndY())/(other.getStartX() - other.getEndX());
              if (this.getStart().equals(this.getEnd()))
                point.rotate(- this.circAngle, this.getStartX(), this.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((other.getEndX() - other.getStartX())*(other.getEndX() - other.getStartX()) + (other.getEndY() - other.getStartY())*(other.getEndY() - other.getStartY()));
              if (this.archStart < arch)
                u = Math.abs((arch - this.archStart)/this.deltaArch);
              else
                u = 1 - Math.abs((arch - this.archEnd)/this.deltaArch);

              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }
          } else {
            if (this.getEnd().equals(this.getStart()) || (this.archStart > arch && arch > this.archEnd && this.archStart > this.archEnd) || ((this.archStart > arch || arch > this.archEnd) && this.archStart < this.archEnd)) {

              double m1 = (other.getStartY() - other.getEndY())/(other.getStartX() - other.getEndX());
              if (this.getStart().equals(this.getEnd()))
                point.rotate(- this.circAngle, this.getStartX(), this.getStartY());
              double m2 = (center.x - point.x)/(center.y - point.y);
              double tanAlph1 = Math.abs((m2-m1)/(1+m1*m2));
              double tanAlph2 = Math.abs((m1-m2)/(1+m1*m2));
              double len = Math.sqrt((other.getEndX() - other.getStartX())*(other.getEndX() - other.getStartX()) + (other.getEndY() - other.getStartY())*(other.getEndY() - other.getStartY()));
              if (this.archStart < arch)
                u = Math.abs((arch - this.archStart)/this.deltaArch);
              else
                u = 1 - Math.abs((arch - this.archEnd)/this.deltaArch);

              list.add(new AbstractMap.SimpleEntry<Double, Double>(u, (20 + this.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph1)) + other.lineConfig.getWidth()/Math.sin(Math.atan(tanAlph2)))/len));
            }
          }
        }

      }

    } else {
      this.calculateArch();
      other.calculateArch();
    }
    return list;
  }

  private boolean descIsDef() {
    if (this.lineConfig.lineType == LineType.GLUON || this.lineConfig.lineType == LineType.SSPIRAL) {
      if (this.descDis != descDisDefGLUON) return false;
      if (this.descScale != descScaleDefGLUON) return false;
      if (this.partPlaceDesc != partPlaceDescDefGLUON) return false;
      if (this.rotDesc != rotDescDefGLUON) return false;
      // if (this.showDesc != showDescDefGLUON) return false;
    } else if (this.lineConfig.lineType == LineType.FERMION) {
      if (this.descDis != descDisDefFERM) return false;
      if (this.descScale != descScaleDefFERM) return false;
      if (this.partPlaceDesc != partPlaceDescDefFERM) return false;
      if (this.rotDesc != rotDescDefFERM) return false;
      // if (this.showDesc != showDescDefFERM) return false;
    } else if (this.lineConfig.lineType == LineType.PHOTON || this.lineConfig.lineType == LineType.SWAVE) {
      if (this.descDis != descDisDefPHOT) return false;
      if (this.descScale != descScaleDefPHOT) return false;
      if (this.partPlaceDesc != partPlaceDescDefPHOT) return false;
      if (this.rotDesc != rotDescDefPHOT) return false;
      // if (this.showDesc != showDescDefPHOT) return false;
    }
    return true;
  }

  private void descToDef(LineConfig lc) {
    if (lc.lineType == LineType.GLUON || lc.lineType == LineType.SSPIRAL) {
      this.descDis = (int) descDisDefGLUON;
      this.descScale = descScaleDefGLUON;
      this.partPlaceDesc = partPlaceDescDefGLUON;
      this.rotDesc = rotDescDefGLUON;
      // this.showDesc = showDescDefGLUON;
    } else if (lc.lineType == LineType.PHOTON || lc.lineType == LineType.SWAVE) {
      this.descDis = (int) descDisDefPHOT;
      this.descScale = descScaleDefPHOT;
      this.partPlaceDesc = partPlaceDescDefPHOT;
      this.rotDesc = rotDescDefPHOT;
      // this.showDesc = showDescDefPHOT;
    } else if (lc.lineType == LineType.FERMION) {
      this.descDis = (int) descDisDefFERM;
      this.descScale = descScaleDefFERM;
      this.partPlaceDesc = partPlaceDescDefFERM;
      this.rotDesc = rotDescDefFERM;
      // this.showDesc = showDescDefFERM;
    }
  }

  public void changeConfigTo(LineConfig newLc) {

    if (newLc.lineType != this.lineConfig.lineType) {
      if (this.descIsDef()) {
        descToDef(newLc);
      }
    }
    this.lineConfig = new LineConfig(newLc);
  }
}
