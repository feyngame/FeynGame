//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import javax.swing.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;
/**
 * this class handles the parsing and storing of the information of level (.if)
 * files and therefore stores the "challenges" of level file
 */
public class InOutLevel {
  /**
   * A list of the easy challenges consisting of two lists of the identifiers for the particles that the initial and final states consist of
   */
  public ArrayList<Map.Entry<ArrayList<String>, ArrayList<String>>> challengesEasy;
  /**
   * A list of the medium challenges consisting of two lists of the identifiers for the particles that the initial and final states consist of
   */
  public ArrayList<Map.Entry<ArrayList<String>, ArrayList<String>>> challengesMedium;
  /**
   * A list of the hard challenges consisting of two lists of the identifiers for the particles that the initial and final states consist of
   */
  public ArrayList<Map.Entry<ArrayList<String>, ArrayList<String>>> challengesHard;
  /**
   * the absolute path to the config/model file given in the level file
   */
  public String configPath;
  /**
   * gives what difficulty currently is parsed, 0 : easy; 1 : madium; 2 : hard
   */
  int difficulty;
  /**
   * The absolute path to the level file
   */
  private String path;
  /**
   * the list of identifiers of the particles in the initial state that is parsed right now
   */
  private ArrayList<String> currentIn;
  /**
   * constructor for default level file
   */
  public InOutLevel() {

    challengesEasy = new ArrayList<>();
    challengesHard = new ArrayList<>();
    challengesMedium = new ArrayList<>();

    difficulty = 0;

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(LineConfig.class.getClassLoader().getResourceAsStream("resources/defaults/default.if")));

      String line;
      while ((line = br.readLine()) != null) {
        this.readLine(line);
      }
    } catch (Exception e) {
      JOptionPane.showMessageDialog(new JFrame(), "Failed to read the default level file: " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
  /**
   * constructor based on a path to a level file
   * checks if file exists and if so parses it
   * @param path the path to the level file
   */
  public InOutLevel(String path) {

    File level = new File(path);

    if (!level.isFile() || !level.exists()) {
      JOptionPane.showMessageDialog(new JFrame(), "The given level file does not exist:\n" + path, "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    this.path = level.getAbsolutePath();

    challengesEasy = new ArrayList<>();
    challengesHard = new ArrayList<>();
    challengesMedium = new ArrayList<>();

    difficulty = 0;

    if (!level.canRead()) {
      JOptionPane.showMessageDialog(new JFrame(), "Failed to read the given level file.", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    try {
      BufferedReader br = new BufferedReader(new FileReader(level));

      String line;
      while ((line = br.readLine()) != null) {
        this.readLine(line);
      }
    } catch (Exception e) {
      JOptionPane.showMessageDialog(new JFrame(), "Failed to read the given level file: " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
  /**
   * funtion to parse each line of the level file
   * @param line the line to be parsed
   */
  private void readLine(String line) {
    /* parse model file if line starts with "mod"*/
    if (line.toLowerCase().startsWith("mod")) {
      String[] params = line.split("[:=]");
      if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
        String confPath = params[params.length - 1].replaceAll("\\s", "");
        if (Character.toString(confPath.charAt(1)).equals(":")) {
          this.configPath = confPath;
        } else {
          this.configPath = this.path.substring(0, this.path.lastIndexOf(System.getProperty("file.separator")));
          this.configPath += System.getProperty("file.separator");
          this.configPath += confPath;
        }
      } else {
        String confPath = params[params.length - 1].replaceAll("\\s", "");
        if (confPath.startsWith("/")) {
          this.configPath = confPath;
        } else if (confPath.startsWith("~")) {
          this.configPath = System.getProperty("user.home");
          confPath = confPath.substring(1);
          this.configPath += confPath;
        } else {
          this.configPath = this.path.substring(0, this.path.lastIndexOf(System.getProperty("file.separator")));
          this.configPath += System.getProperty("file.separator");
          this.configPath += confPath;
        }
        String[] directoryStructure = this.configPath.split("/");
        ArrayList<Integer> toDelete = new ArrayList<>();
        for (int i = 1; i < directoryStructure.length; i++){
          if (directoryStructure[i].equals("..")) {
            toDelete.add(i-1);
            toDelete.add(i);
          }
        }

        this.configPath = "";
        for (int i = 0; i < directoryStructure.length; i++) {
          if (!toDelete.contains(i)) {
            if (i != 0)
              this.configPath += "/";
            this.configPath += directoryStructure[i];
          }
        }

      }
      return;
    }
    /* set difficulty if line says so*/
    if (line.toLowerCase().startsWith("easy")) {
      difficulty = 0;
    }

    if (line.toLowerCase().startsWith("medium")) {
      difficulty = 1;
    }

    if (line.toLowerCase().startsWith("hard")) {
      difficulty = 2;
    }
    /* parse incoming particles if line starts with "in"*/
    if (line.toLowerCase().startsWith("in") || line.toLowerCase().startsWith("st")) {
      currentIn = new ArrayList<>();
      String[] entries = line.split("[:=]");
      if (!(entries.length > 1)) {
        JOptionPane.showMessageDialog(new JFrame(), "Line is not a valid definiton of initial states " + line, "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
      String parts = entries[1];
      entries = parts.split("[,;.]");
      for (String entry : entries) {
        currentIn.add(entry.replaceAll("\\s", ""));
      }
    }
    /* parse for outgoing particles if line starts with "out", adds
    challenge to the object at the end*/
    if (line.toLowerCase().startsWith("out") || line.toLowerCase().startsWith("fin") || line.toLowerCase().startsWith("end")) {
      ArrayList<String> currentFin = new ArrayList<>();
      String[] entries = line.split("[:=]");
      if (!(entries.length > 1)) {
        JOptionPane.showMessageDialog(new JFrame(), "Line is not a valid definiton of final states " + line, "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
      String parts = entries[1];
      entries = parts.split("[,;.]");
      for (String entry : entries) {
        currentFin.add(entry.replaceAll("\\s", ""));
      }
      if (difficulty == 0)
        challengesEasy.add(new AbstractMap.SimpleEntry<>(currentIn, currentFin));
      if (difficulty == 1)
        challengesMedium.add(new AbstractMap.SimpleEntry<>(currentIn, currentFin));
      if (difficulty == 2)
        challengesHard.add(new AbstractMap.SimpleEntry<>(currentIn, currentFin));
    }
  }

}
