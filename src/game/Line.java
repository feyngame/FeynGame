//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.io.Serializable;

/**
 * My own Line class which just saves two {@link game.Point2D}. Nothing special here. I do not wirte comments on all functions here, because there are only getter and setter functions plus some constructors.
 * I implemented this class by myself because I needed the {@link Serializable} interface, which the standard library version did not.
 *
 * @author Maximilian Lipp
 */
public class Line implements Serializable {
  public final Point2D start, end;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 48L;
  /**
   * contructor for a line from (0, 0) to (0 ,0)
   */
  public Line() {
    start = new Point2D();
    end = new Point2D();
  }
  /**
   * constructs a line from components of the points in floating point
   * precision
   * @param x1 the x-comp of the start
   * @param y1 the y-comp of the start
   * @param x2 the x.comp of the end
   * @param y2 the y-comp of the end
   */
  public Line(float x1, float y1, float x2, float y2) {
    start = new Point2D(x1, y1);
    end = new Point2D(x2, y2);
  }
  /**
   * constructs a line from components of the points in double precision
   * @param x1 the x-comp of the start
   * @param y1 the y-comp of the start
   * @param x2 the x.comp of the end
   * @param y2 the y-comp of the end
   */
  public Line(double x1, double y1, double x2, double y2) {
    start = new Point2D(x1, y1);
    end = new Point2D(x2, y2);
  }
  /**
   * copy constructor
   * @param l the line to be copied
   */
  public Line(Line l) {
    this.start = new Point2D(l.start);
    this.end = new Point2D(l.end);
  }
  /**
   * getter for the x-comp of the start
   * @return the x-comp of the start
   */
  public double getStartX() {
    return start.x;
  }
  /**
   * setter for the x-comp of the start
   * @param x the x-comp of the start
   */
  void setStartX(double x) {
    start.x = x;
  }
  /**
   * getter for the y-comp of the start
   * @return the y-comp of the start
   */
  public double getStartY() {
    return start.y;
  }
  /**
   * setter for the y-comp of the start
   * @param y the y-comp of the start
   */
  void setStartY(double y) {
    start.y = y;
  }
  /**
   * getter for the x-comp of the end
   * @return the x-comp of the end
   */
  public double getEndX() {
    return end.x;
  }
  /**
   * setter for the x-comp of the end
   * @param x the x-comp of the end
   */
  void setEndX(double x) {
    end.x = x;
  }
  /**
   * getter for the y-comp of the end
   * @return the y-comp of the end
   */
  public double getEndY() {
    return end.y;
  }
  /**
   * setter for the y-comp of the end
   * @param y the y-comp of the end
   */
  void setEndY(double y) {
    end.y = y;
  }
  /**
   * checks if "this" is equal to another object
   * @param o the object to compare "this" to
   * @return wether or not "this" is equal to o
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Line line = (Line) o;

    if (!start.equals(line.start)) return false;
    return end.equals(line.end);
  }
  /**
   * returns the hashCode of "this"
   * @return the hashCode
   */
  @Override
  public int hashCode() {
    int result = start.hashCode();
    result = 31 * result + end.hashCode();
    return result;
  }
}
