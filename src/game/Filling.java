//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import javafx.JavaFXToJavaVectorGraphic;
import ui.Frame;

/**
 * Contains all information about the filling of a specific vertex/shaped
 *
 * @author Sven Yannick Klein
 */
@SuppressWarnings("overrides")
public class Filling implements Serializable {
  private static final Logger LOGGER = Frame.getLogger(Filling.class);
  /**
   * connects every {@link #imageNumber} to a BufferedImage
   */
  public static HashMap<Integer, BufferedImage> imageNumberToImage = new HashMap<>();
  /**
   * connects the {@link #imageNumber}s to the source paths of the connected images
   */
  public static HashMap<String, Integer> pathToImageNumber = new HashMap<>();
  /**
   * connects the {@link #name}s to of the filling pattern to the connected {@link game.JavaVectorGraphic}
   */
  public static HashMap<String, JavaVectorGraphic> nameToJVG = new HashMap<>();
  /**
   * the next {@link #imageNumber} that is generated for {@link #pathToImageNumber} and {@link #imageNumberToImage}
   */
  public static int newImageNumber = 0;
  /**
   * The color of the filling if no image is displayed
   */
  public Color color = Color.BLACK;
  /**
   * if true the image indicated by {@link #imageNumber} is displayed. otherwise the {@link #color} is shown
   */
  public Boolean graphic;
  /**
   * Indicates the image that could be shown
   */
  public int imageNumber;
  /**
   * gives the name of the pattern that is the filling
   */
  public String name;
  /**
   * defines the scaling of the filling with respect to the object itself
   */
  public double scale;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 44L;
  /**
   * basic constructor of plain black filling
   */
  public Filling() {
    this.color = Color.BLACK;
    this.graphic = false;
    this.scale = 1;
  }
  /**
   * constructor for filling with solid color
   * @param color the color of the filling
   */
  public Filling(Color color) {
    this.color = color;
    this.graphic = false;
    this.scale = 1;
  }
  /**
   * copy constructor
   * @param f the filling to copy
   */
  public Filling(Filling f) {
    this.graphic = f.graphic;
    this.scale = f.scale;
    if (this.graphic) {
      this.imageNumber = f.imageNumber;
    } else {
      this.color = f.color;
    }
    if (f.name != null)
      this.name = f.name;
  }
  /**
   * constructor of filling with image
   * @param path the path to the file of the image
   */
  public Filling(String path) {
    this.scale = 1;
    this.graphic = true;
    this.imageNumber = getImageNumber(path);
  }
  /**
   * checks if the same image file has been loaded. If it is already been
   * loaded, gives back the imageNumber of the image. If not it loads the
   * image and sets a new imageNumber for this image and gives back the
   * imageNumber
   * @param path the path to the image file
   * @return the imageNumber of the image
   */
  public static int getImageNumber(String path) {
    int returnValue = 0;
    if (!pathToImageNumber.containsKey(path)) {
      try {
        if (path.toLowerCase().endsWith("jpg") || path.toLowerCase().endsWith("png") || path.toLowerCase().endsWith("gif") || path.toLowerCase().endsWith("bmp") || path.toLowerCase().endsWith("jpeg")) {
          imageNumberToImage.put(newImageNumber, ImageIO.read(new File(path)));
        }
        pathToImageNumber.put(path, newImageNumber);
        returnValue = newImageNumber;
        newImageNumber++;
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, "Cannot get image number", e);
      }
    } else {
      returnValue = pathToImageNumber.get(path);
    }
    return returnValue;
  }
  /**
   * checks if the same javafx file has been loaded. If it is already been
   * loaded, gives back the name idenentifier of the image. If not it loads
   * the JavaFX and sets a new name for this javafx and gives back the
   * name
   * @param path the path to the image file
   * @return the identifier (mostly the path to the image) of the image
   */
  public static String addPattern(String path) {
    if (!path.endsWith(".fx"))
      path = path + ".fx";
    if (JavaFXToJavaVectorGraphic.patternsIn.contains(path)) {
      try {
        Filling.nameToJVG.put(path, JavaFXToJavaVectorGraphic.getJavaVectorGraphic(path));
      } catch (Exception ex) {
        // System.err.println("Error: " + ex);
      }
      return path;
    }
    if (!JavaFXToJavaVectorGraphic.patternsOut.contains(path)) {
      JavaFXToJavaVectorGraphic.addPath(path);
      try {
        Filling.nameToJVG.put(path, JavaFXToJavaVectorGraphic.getJavaVectorGraphic(path));
      } catch (Exception ex) {
        // System.err.println("Error: " + ex);
      }
    }
    return path;
  }
  /**
   * returns the JVG Object (from JavaFX file) corresponding to the name
   * @param name the name identifier of the javafx
   * @return the JavaVectorGraphic (JVG) object from the javafx file
   * corresponding to the name
   */
  public static JavaVectorGraphic getJavaVectorGraphic(String name) {
    JavaVectorGraphic jvg = nameToJVG.get(name);
    if (jvg == null) {
      try {
        nameToJVG.put(name, JavaFXToJavaVectorGraphic.getJavaVectorGraphic(name));
      } catch (Exception ex) {
        // System.err.println(name + ": " + ex);
      }
    }
    return jvg;
  }
  /**
   * checks wether or not this is equal to the object
   * @param obj the object to compare to
   */
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    Filling other = (Filling) obj;

    if (this.name != null && other.name != null) {
      if (this.name != other.name)
        return false;
    } else if (other.name != name || this.name != null) {
      return false;
    }

    if (this.graphic && other.graphic) {
      if (this.imageNumber != other.imageNumber)
        return false;
    } else if(this.graphic || other.graphic)
      return false;

    if (!this.color.equals(other.color))
      return false;

    return true;
  }
}
