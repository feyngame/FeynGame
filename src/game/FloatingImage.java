//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import ui.Frame;
import ui.Pane;

/**
 * Contains all information about a "FloatingImage"
 *
 * @author Sven Yannick Klein
 */
@SuppressWarnings("overrides")
public class FloatingImage implements Serializable {
  private static final Logger LOGGER = Frame.getLogger(FloatingImage.class);
  
  public static int latexSize = 20;
  /**
   * All possible keys that can be used in the config file.
   */
  private static final java.util.List<String> mapKeys = Arrays.asList("scale", "imagepath", "rotation", "fxpath", "label"); // lower case!!!!
  /**
   * The presets for the FloatingImage
   */
  public static ArrayList<FloatingImage> fIPresets = new ArrayList<>();
  /**
   * if the description is shown per default
   */
  public static Boolean showDescDef = false;
  /**
   * default value for descPosX
   */
  public static int descPosXDef = 0;
  /**
   * default for partPlaceDesc
   */
  public static double descPosYDef = 20;
  /**
   * default for rotDesc;
   */
  public static double rotDescDef = 0;
  /**
   * default for descScale
   */
  public static double descScaleDef = 1;
  /**
   * determines where the center of the image is on the {@link ui.Pane}
   */
  public Point2D center;
  /**
   * determines the scale of the image
   */
  public float scale;
  /**
   * determines the rotation of the image
   */
  public float rotation;
  /**
   * determines which image is shown
   */
  public int imageNumber;
  /**
   * the name of the JVG that is drawn instead of the image
   */
  public String name;
  /**
   * a description/name of the image shown in a tooltip when hovering over a tile containing this image
   */
  public String description;
  /**
   * if label/description is shown
   */
  public boolean showDesc;
  /**
   * the amount of pixels the description is displayed left/right of the image
   */
  public int descPosX;
  /**
   * the amount of pixels the description is displayed above/below of the image
   */
  public double descPosY;
  /**
   * the rotation of the description
   */
  public double rotDesc;
  /**
   * describes the scaling of the description
   */
  public double descScale;
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 47L;
    /**
     * wether or not this is part if a multiedit object
     */
    public boolean multiEdit = false;
  /**
   * copy constructor with different place than the original image
   * @param fI the floatingImage to be copied
   * @param center the new center of the object constructed
   */
  public FloatingImage(FloatingImage fI, Point2D center) {
    this.center = center;
    this.scale = fI.scale;
    this.rotation = fI.rotation;
    this.imageNumber = fI.imageNumber;
    this.name = fI.name;
    this.description = fI.description;

    this.showDesc = fI.showDesc;
    this.descPosX = fI.descPosX;
    this.rotDesc = fI.rotDesc;
    this.descScale = fI.descScale;
    this.descPosY = fI.descPosY;
  }
  /**
   * basic constructor, has no content
   */
  public FloatingImage() {
    this.scale = 1;
    this.rotation = 0;

    this.showDesc = FloatingImage.showDescDef;
    this.descPosX = FloatingImage.descPosXDef;
    this.rotDesc = FloatingImage.rotDescDef;
    this.descScale = FloatingImage.descScaleDef;
    this.descPosY = FloatingImage.descPosYDef;
  }
  /**
   * copy constructor
   * @param fI the object to be copied
   */
  public FloatingImage(FloatingImage fI) {
    try {
      this.center = new Point2D(fI.center);
    } catch (NullPointerException ex) {
      this.center = fI.center;
    }
    this.scale = fI.scale;
    this.rotation = fI.rotation;
    this.imageNumber = fI.imageNumber;
    this.name = fI.name;
    this.description = fI.description;

    this.showDesc = fI.showDesc;
    this.descPosX = fI.descPosX;
    this.rotDesc = fI.rotDesc;
    this.descScale = fI.descScale;
    this.descPosY = fI.descPosY;
  }
  /**
   * constructor based on a path to an image file: content of object is
   * the image of the image file, does not set the center
   * @param path the path to the image file
   */
  public FloatingImage(String path) {
    this.scale = 1;
    this.rotation = 0;
    if (!path.endsWith(".fx"))
      this.imageNumber = Filling.getImageNumber(path);
    if (path.endsWith(".fx"))
      this.name = Filling.addPattern(path);
  }
  /**
   * constructor based on path to an image to be the content of the object
   * with a given center
   * @param path the path to the image file
   * @param center the center of the consrructed object
   */
  public FloatingImage(String path, Point2D center) {
    this.center = center;
    this.scale = 1;
    this.rotation = 0;

    this.showDesc = FloatingImage.showDescDef;
    this.descPosX = FloatingImage.descPosXDef;
    this.rotDesc = FloatingImage.rotDescDef;
    this.descScale = FloatingImage.descScaleDef;
    this.descPosY = FloatingImage.descPosYDef;

    if (!path.endsWith(".fx"))
      this.imageNumber = Filling.getImageNumber(path);
    if (path.endsWith(".fx"))
      this.name = Filling.addPattern(path);
  }

  /**
   * converts a string to a FloatingImage preset
   *
   * @param s the string to be converted
   * @return the converted FloatingImage
   * @throws Exception if config entry is not valid
   */
  public static FloatingImage toConf(String[] s) throws Exception {
    FloatingImage fI = new FloatingImage();
    for (int i = 0; i < Math.min(s.length, mapKeys.size()); i++) {

      int paramNum = i;
      String paramVal = s[i];

      if (s[i].contains("=") || s[i].contains("-")) {
        String[] map = s[i].split("[=(\\-)]+");
        map[0] = map[0].replaceAll("_", "");
        paramNum = mapKeys.indexOf(map[0].toLowerCase());
        paramVal = map[1];
      }

      if (paramNum >= mapKeys.size()) {
        throw new Exception("Unrecognized additional parameter: " + s[i] + "\nAvailable params are " + mapKeys);
      }

      switch (paramNum) {
        case 0: {
          fI.scale = Float.parseFloat(paramVal);
          break;
        }
        case 1: {
          fI.imageNumber = Filling.getImageNumber(paramVal);
          break;
        }
        case 2: {
          fI.rotation = Float.parseFloat(paramVal);
          break;
        }
        case 3: {
          fI.name = Filling.addPattern(paramVal);
          break;
        }
        case 4: {
          fI.description = paramVal.replaceAll("hashpound", "#");
          break;
        }
      }
    }
    return fI;
  }

  /**
   * Loads {@link #fIPresets} from the specified config file.
   *
   * @param path name or path to config file
   */
  public static void initPresets(String path) {
    File file = new File(path);

    if (!file.canRead())
      return;

    fIPresets.clear();
    try {
      BufferedReader br = new BufferedReader(new FileReader(file));
      ArrayList<String[]> images = new ArrayList<>();

      /*String st;
      Pattern pt = Pattern.compile("^(?:\\s*?)\\|(.[^#\\|\\|]*+)\\|(?:\\s*?)#*"); // Vertex definition
      while ((st = br.readLine()) != null) {
        Matcher matcher = pt.matcher(st);
        if (matcher.find()) {
          images.add(matcher.group(1).replaceAll("\\s+", "").split("[;,]"));
        }
      }*/

      String str;
            while ((str = br.readLine()) != null) {
                if (str.replaceAll("\\s", "").startsWith("|") && str.replaceAll("\\s", "").endsWith("|")) {
                    String stripped = str.replaceAll("\\s", "");
                    stripped = stripped.substring(stripped.indexOf("|") + 1, stripped.lastIndexOf("|"));
                    images.add(stripped.split("[;,]"));
                }
            }

      if (images.size() == 0) {
        return;
        // throw new Exception("No image defintions found.");
      }

      LOGGER.info("Found " + images.size() + " image definitions");

      for (String[] image : images) {
        fIPresets.add(toConf(image));
      }


    } catch (Exception e) {
      // System.err.println("Did not find any image definitions");
    }

  }

  /**
   * deletes the image preset with index from the presets
   *
   * @param index the index of the config to be deleted
   */
  public static void delete(int index) {
    fIPresets.remove(index);
  }

  /**
   * Draws the specific Image. Additionally enables antialiasing.
   *
   * @param g Graphics variable
   */
  public void draw(Graphics2D g) {
    this.draw(g, false);
  }

  /**
   * sets the parameters for the description of this line to the default
   */
  public void setDefault() {
    FloatingImage.showDescDef = this.showDesc;
    FloatingImage.descPosXDef = this.descPosX;
    FloatingImage.rotDescDef = this.rotDesc;
    FloatingImage.descScaleDef = this.descScale;
    FloatingImage.descPosYDef = this.descPosY;
  }

  public Point2D getLabelPosition() {
    Point2D place = new Point2D(this.center.x + this.descPosX, this.center.y + this.descPosY);
    return place;
  }

  /**
   * Draws the specific Image. Additionally enables antialiasing.
   *
   * @param g        Graphics variable
   * @param clipTo10 if the drawing is only done in a circle with radius 10
   */
  public void draw(Graphics2D g, boolean clipTo10) {

    if (scale == 0)
      return;

    Stroke oldStroke = g.getStroke();
    Color oldColor = g.getColor();

    AffineTransform backup = g.getTransform();
    AffineTransform a = AffineTransform.getRotateInstance(rotation / 180 * Math.PI, center.x, center.y);
    g.transform(a);
    if (name != null) {
      try {
        Filling.getJavaVectorGraphic(this.name).strokeSize = -1;
        Filling.getJavaVectorGraphic(this.name).draw(g, this.scale, this.center, clipTo10);
      } catch (NullPointerException nex) {
        this.name = Filling.addPattern(this.name);
        try {
          Filling.getJavaVectorGraphic(this.name).strokeSize = -1;
          Filling.getJavaVectorGraphic(this.name).draw(g, this.scale, this.center, clipTo10);
        } catch (NullPointerException nnex) {
          // System.err.println("Could not find JVG: " + nnex);
        }
      }

    } else {

      Rectangle clRec = g.getClipBounds();
      try {
        BufferedImage img = Filling.imageNumberToImage.get(this.imageNumber);
        double height = img.getWidth();
        double width = img.getHeight();

        if (clipTo10) {
          Shape shape = new Ellipse2D.Float((float)this.center.x - 10f, (float)this.center.y - 10f, 2 * 10, 2 * 10);
          g.setClip(shape);
          g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
        }
        g.drawImage(img, (int) Math.round(center.x - height / 2 * scale), (int) Math.round(center.y - width / 2 * scale), (int) Math.round(height * scale), (int) Math.round(width * scale), null);
      } catch (java.lang.NullPointerException ign) {
      }

      if (clipTo10)
        g.setClip(clRec);

    }

    g.setTransform(backup);

    g.setStroke(oldStroke);
    g.setColor(oldColor);



    if (Pane.drawLabel && !clipTo10 && this.showDesc && this.description != null && !this.description.equals("") && this.descScale != 0) {
      if (description.contains("<html>")) {
        JLabel label = new JLabel(this.description, JLabel.CENTER);
        label.setForeground(Color.BLACK);
        label.setSize(label.getPreferredSize());
        double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.cos(this.rotDesc) + label.getPreferredSize().height * Math.sin(this.rotDesc);
        double labelHeight = /*label.getPreferredSize().height*/ -label.getPreferredSize().width * Math.sin(this.rotDesc) + label.getPreferredSize().height * Math.cos(this.rotDesc);
        labelHeight *= this.descScale;
        labelWidth *= this.descScale;
        Point2D place = new Point2D(this.center.x + this.descPosX - labelWidth / 2, this.center.y + this.descPosY - labelHeight / 2);
        g.translate(place.x, place.y);
        g.scale(this.descScale, this.descScale);
        g.rotate(-this.rotDesc);
        label.paint(g);
        g.rotate(this.rotDesc);
        g.scale(1 / this.descScale, 1 / this.descScale);
        g.translate(-place.x, -place.y);
      } else {
        TeXFormula formula = new TeXFormula(description);
            TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
          icon.setForeground(Color.BLACK);
          double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.cos(this.rotDesc) + icon.getIconHeight() * Math.sin(this.rotDesc);
          double labelHeight = /*label.getPreferredSize().height*/ -icon.getIconWidth() * Math.sin(this.rotDesc) + icon.getIconHeight() * Math.cos(this.rotDesc);
          labelHeight *= this.descScale;
          labelWidth *= this.descScale;
        Point2D place = new Point2D(this.center.x + this.descPosX - labelWidth / 2, this.center.y + this.descPosY - labelHeight / 2);
        g.translate(place.x, place.y);
        g.scale(this.descScale, this.descScale);
        g.rotate(-this.rotDesc);
        icon.paintIcon(new JPanel(), g, 0, 0);
        g.rotate(this.rotDesc);
        g.scale(1 / this.descScale, 1 / this.descScale);
        g.translate(-place.x, -place.y);
      }
    }
  }
  /**
   * outputs a string that can be put in the config file to generate this exact vertex
   *
   * @return the string for the config file
   */
  public String toFile() {
    if (this.name != null) {
      return "|scale = " + this.scale + ", fxPath = " + name + ", rotation = " + this.rotation + "|";
    }
    try {
      String path = "";
      for (String s : Filling.pathToImageNumber.keySet()) {
        if (this.imageNumber == Filling.pathToImageNumber.get(s)) {
          path = s;
          break;
        }
      }
      if (description == null) {
        return "|scale = " + this.scale + ", imagePath = " + path + ", rotation = " + this.rotation + "|";
      } else {
        return "|scale = " + this.scale + ", imagePath = " + path + ", rotation = " + this.rotation + ", description = " + this.description.replaceAll("#", "hashpound") + "|";
      }
    } catch (Exception ex) {
      LOGGER.log(Level.SEVERE, "Path of Image could not be found", ex);
    }
    return "";
  }
  /**
   * sets the properties of this object to be the ones of the preset
   * excluding the center
   * @param fI the preset whose properties get copied to this object
   */
  public void turnTo(FloatingImage fI) {
    this.scale = fI.scale;
    this.rotation = fI.rotation;
    this.imageNumber = fI.imageNumber;
    this.name = fI.name;
  }
  /**
   * moves the object
   * @param offset the x-comp is the translation in positive x direction
   * and the y-comp is the translation in positive y direction
   */
  public void translate(Point2D offset) {
    center.x += offset.x;
    center.y += offset.y;
  }
  /**
     * Compares two {@link game.FloatingImage} and returns True if both have the same config values
     *
     * @param obj must be an {@link game.LineConfig} in order or be True.
     * @return True or False depending on some class variables.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FloatingImage other = (FloatingImage) obj;

        if (this.scale != other.scale)
          return false;
        if (this.rotation != other.rotation)
          return false;
        if (this.name != null && other.name != null && !other.name.equals(this.name))
          return false;
        if (this.name != null || other.name != null)
          return false;
        if (this.name == null && other.name == null)
          return true;
        if (this.imageNumber != other.imageNumber)
          return false;

        return true;
    }
}
