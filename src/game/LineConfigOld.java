//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================
package game;

import javax.swing.*;
import java.awt.*;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import amplitude.FeynmanRuleTemplates;

@SuppressWarnings("overrides")
public class LineConfigOld implements Serializable {
  @SuppressWarnings("CanBeFinal")
  public LineType lineType;
  public float stroke;
  public float wiggleLength;
  public float wiggleHeight;
  public float wiggleOffset;
  public float dashLength;
  public boolean dashed;
  public float dashOffset;
  public Map.Entry<String, String> identifier = null;
  public boolean showArrow;
  public int arrowSize;
  public int arrowDent = 0;
  public int arrowHeight = -1;
  public double arrowPlace;
  public Color color;
  public boolean doubleLine = false;
  public String description;
  public String posDescription = "";
  public String negDescription = "";
  // static final long serialVersionUID = 42L;
  static final long serialVersionUID = 23452345L;
  public boolean multiEdit = false;
  public double straight_len = 0.05;
  public double straight_len_arrow = 0.01;
}