//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.io.Serializable;
import java.awt.BasicStroke;

/**
 * this is a class that stores the information from the strokes of JavaFx (".fx") files.
 *
 * @author Sven Yannick Klein
 */
 public class SerialStroke implements Serializable {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 143L;
  /**
   * the properites that a basicStroke can have
   */
  float[] dashArray;
  float dashPhase;
  int endCap;
  int lineJoin;
  float lineWidth;
  float miterLimit;
  /**
   * constructs an objects that upon conversion to a basic stroke gives
   * the same as the basic constructor for BasicStroke without parameters
   */
  public SerialStroke() {
    BasicStroke bs = new BasicStroke();
    this.dashArray = bs.getDashArray();
    this.dashPhase = bs.getDashPhase();
    this.endCap = bs.getEndCap();
    this.lineJoin = bs.getLineJoin();
    this.lineWidth = bs.getLineWidth();
    this.miterLimit = bs.getMiterLimit();
  }
  /**
   * contrucor based on a basicsrtoke
   * @param bs the basicStroke
   */
  public SerialStroke(BasicStroke bs) {
    this.dashArray = bs.getDashArray();
    this.dashPhase = bs.getDashPhase();
    this.endCap = bs.getEndCap();
    this.lineJoin = bs.getLineJoin();
    this.lineWidth = bs.getLineWidth();
    this.miterLimit = bs.getMiterLimit();
  }
  /**
   * converts this object to a basicstroke
   * @return the converted basic stroke
   */
  public BasicStroke toStroke() {
    return new BasicStroke(lineWidth, endCap, lineJoin, miterLimit, dashArray, dashPhase);
  }
}
