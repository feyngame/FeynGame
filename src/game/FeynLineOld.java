//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.util.*;
import java.io.Serializable;
import java.awt.geom.*;

import ui.Frame;
import ui.Pane;

import org.scilab.forge.jlatexmath.*;
public class FeynLineOld implements Serializable {
  public int draggedMode = 2; // -1 drag, 0,1 move
  public double archEnd;
  public double archStart;
  public double angle;
  public double radius;
  public Point2D center;
  public Point origin; // deprecated: only here so that old .fg files can be loaded
  @SuppressWarnings("CanBeFinal")
  public LineConfigOld lineConfig;
  public int phase = 1;
  public double height;
  public double deltaArch;
  public Boolean showDesc;
  public int descDis;
  public double partPlaceDesc;
  public double rotDesc;
  public double descScale;
  public boolean fixStart = false;
  public boolean fixEnd = false;
  public double circAngle;
  @SuppressWarnings("CanBeFinal")
  private game.Line line;
  private Shape path;
  static final long serialVersionUID = 544563L;
  // static final long serialVersionUID = 43L;
  public Momentum momArrow;
  public ArrayList<Blank> blanksUnsorted = new ArrayList<>();
  public ArrayList<Blank> blanks = new ArrayList<>();
  public boolean drawBlanks = true;
  public int direction = 0;
}