//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.io.Serializable;
import java.util.*;
/**
 * Contains all information about a blank spot in a line (i.e. a section of the line which is not drawn)
 *
 * @author Sven Yannick Klein
 */
public class Blank implements Serializable {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 4134L;
  /**
   * the length of the blank spot relative to the length of the line
   */
  private double length = .1d;
  /**
   * the part of the path of the line where the middle of the bank section is
   */
  private double partOfPath = .5d;
  /**
   * basic constructor
   */
  public Blank() {}
  /**
   * copy constructor
   * @param old the blank object to copy
   */
  public Blank(Blank old) {
    this.length = old.length;
    this.partOfPath = old.partOfPath;
  }
  /**
   * constructor
   * @param length the lenght
   * @param partOfPath the part of the path
   */
  public Blank(double length, double partOfPath) {
    this.length = length;
    this.partOfPath = partOfPath;
  }
  /**
   * get the length
   * @return the length
   */
  public double getLength() {
    return length;
  }
  /**
   * set the length
   * @param length the new length
   */
  public void setLength(double length) {
    this.length = length;
  }
  /**
   * add to the length
   * @param add how much to add
   */
  public void addLength(double add) {
    this.length += add;
  }
  /**
   * get the partOfPath
   * @return the partOfPath
   */
  public double getPartOfPath() {
    return partOfPath;
  }
  /**
   * set the partOfPath
   * @param partOfPath the new partOfPath
   */
  public void setPartOfPath(double partOfPath) {
    this.partOfPath = partOfPath;
  }
  /**
   * add to the partOfPath
   * @param add how much to add
   */
  public void addPartOfPath(double add) {
    this.partOfPath += add;
  }
  /**
   * checks if two blanks are equal
   * @param other the blank to compare to
   * @return wether or not this equals other
   */
  public boolean equals(Blank other) {
    if (this.length != other.length)
      return false;
    if (this.partOfPath != other.partOfPath)
      return false;
    return true;
  }
  /**
   * checks if this can be combined with the given blank object
   * @param other the object to potentially combine with
   * @return a pair consisting of: 1.: wether or not these blanks could be combined
   * 2.: the combined blank object if combined worked, else this object
   */
  public Map.Entry<Boolean, Blank> combine(Blank other) {
    if (this.equals(other))
      return new AbstractMap.SimpleEntry<Boolean, Blank>(true, new Blank(this));

    double thisStart = this.partOfPath - this.length / 2;
    double thisEnd = this.partOfPath + this.length / 2;
    double otherStart = other.partOfPath - other.length / 2;
    double otherEnd = other.partOfPath + other.length / 2;

    double newStart = thisStart;
    double newEnd = thisEnd;
    boolean combined = false;

    if (thisStart <= otherStart && thisEnd >= otherEnd) {
      combined = true;
    } else if (thisStart >= otherStart && thisEnd <= otherEnd) {
      combined = true;
      newStart = otherStart;
      newEnd = otherEnd;
    } else if (thisStart <= otherStart && thisEnd >= otherStart) {
      combined = true;
      newEnd = otherEnd;
    } else if (otherStart <= thisStart && otherEnd >= thisStart) {
      combined = true;
      newStart = otherStart;
    }

    Blank ret = new Blank();
    ret.partOfPath = (newStart + newEnd) / 2;
    ret.length = newEnd - newStart;
    return new AbstractMap.SimpleEntry<Boolean, Blank>(combined, ret);
  }

  /**
   * checks wether this starts before the other blank
   * @param other the blank to compare to
   * @return true if this starts before the other blank
   */
  public boolean startsBefore(Blank other) {
    return (this.partOfPath - this.length / 2 < other.partOfPath - other.length / 2);
  }
}
