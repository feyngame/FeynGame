//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.io.Serializable;
import java.awt.geom.*;
import java.awt.event.*;
import ui.Pane;

import org.scilab.forge.jlatexmath.*;

/**
 * Contains all information about a Shape object
 *
 * @author Sven Yannick Klein
 */
public class Shaped implements Serializable {
  public static int latexSize = 20;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 1L;
  /**
   * the actual shape (Rectangle, Oval ...)
   */
  public String shape;
  /**
   * the possible shapes
   */
  public static java.util.List<String> SHAPES = Arrays.asList("oval", "rectangle");
  /**
   * the lineConfig (Only LineTypy Fermion for now)
   */
  public LineConfig lineConfig;
  /**
   * the width of the shape
   */
  public int width;
  /**
   * the height of the shape
   */
  public int height;
  /**
   * the rotation
   */
  public double rotation;
  /**
   * the filling of the shape
   */
  public Filling fill;
  /**
   * the middle of the shape
   */
  public Point2D origin;
  /**
   * this gives the hovering mode:
   * 0: not hovering
   * 1: hovering over center (moving center)
   * 2: hovering over top, bottom (stretching)
   * 3: hovering over left, right (stretching)
   * 4: hovering over rotation lines (rotating)
   * 5: hovering over description
   */
  public int hoverMode;
  /**
   * a description/name of the vertex shown in a tooltip when hovering over a tile containing this shape
   */
  public String description;
  /**
   * if label/description is shown
   */
  public boolean showDesc;
  /**
   * the amount of pixels the description is displayed left/right of the shape
   */
  public int descPosX;
  /**
   * the amount of pixels the description is displayed above/below of the shape
   */
  public double descPosY = 20;
  /**
   * the rotation of the description
   */
  public double rotDesc;
  /**
   * describes the scaling of the description
   */
  public double descScale;
  /**
   * if the description is shown per default
   */
  public static Boolean showDescDef = false;
  /**
   * default value for descPosX
   */
  public static int descPosXDef = 0;
  /**
   * default for partPlaceDesc
   */
  public static double descPosYDef = 0;
  /**
   * default for rotDesc;
   */
  public static double rotDescDef = 0;
  /**
   * default for descScale
   */
  public static double descScaleDef = 1;
  /**
   * when reshaping a shape this gives the vertices that have to be moved
   */
  private ArrayList<Integer> attV;
  /**
   * when reshaping a shape this gives the lines that have to be moved
   */
  private ArrayList<Integer> attL;
  /**
   * The timer that resets the connected lines and vertices
   */
  private javax.swing.Timer resTimer;
  /**
   * basic constructor for an oval shape at a given point
   * @param origin the center of the created shape
   */
  public Shaped(Point2D origin) {
    this.shape = "oval";
    this.lineConfig = new LineConfig(LineType.FERMION, Color.BLACK);
    this.width = 300;
    this.height = 100;
    this.rotation = 0;
    this.fill = new Filling();
    this.fill.color = new Color(0, 0, 0, 0);
    this.origin = new Point2D(origin.x, origin.y);
    this.showDesc = Shaped.showDescDef;
    this.descPosX = Shaped.descPosXDef;
    this.rotDesc = Shaped.rotDescDef;
    this.descScale = Shaped.descScaleDef;
    this.descPosY = Shaped.descPosYDef;
  }
  /**
   * copy construcor
   * @param old the object to copy
   */
  public Shaped(Shaped old) {
    this.shape = old.shape;
    this.lineConfig = new LineConfig(old.lineConfig);
    this.width = old.width;
    this.height = old.height;
    this.rotation = old.rotation;
    this.fill = new Filling(old.fill);
    this.origin = new Point2D(old.origin.x, old.origin.y);
    this.showDesc = old.showDesc;
    this.descPosX = old.descPosX;
    this.rotDesc = old.rotDesc;
    this.descScale = old.descScale;
    this.descPosY = old.descPosY;
  }
    
  /**
   * returns an exact copy of "old", including description
   * @return the exact copy
   */
  public Shaped getCopy() {
    Shaped sh = new Shaped(this);
    sh.description = this.description;
    return sh;
  }
  /**
   * sets the parameters for the description of this shape to the default
   */
  public void setDefault() {
    Shaped.showDescDef = this.showDesc;
    Shaped.descPosXDef = this.descPosX;
    Shaped.rotDescDef = this.rotDesc;
    Shaped.descScaleDef = this.descScale;
    Shaped.descPosYDef = this.descPosY;
  }

  public Point2D getLabelPosition() {
    Point2D place = new Point2D(this.origin.x + this.descPosX, this.origin.y + this.descPosY);
    return place;
  }
  /**
   * draws the shape
   * @param g the graphics object to draw to
   * @param currentSelected whether or not this object is currently selected
   * @param helperLines whether or not helperLines (@link
   * ui.Pane#helperLines) are active
   */
  public void draw(Graphics2D g, boolean currentSelected, boolean helperLines) {

    Shape sp;

    if (shape.equals("oval")) {
      sp = new Ellipse2D.Double(origin.x - width / 2, origin.y - height / 2, width, height);
    } else {
      sp = new Rectangle2D.Double(origin.x - width / 2, origin.y - height / 2, width, height);
    }

    g.rotate(rotation, origin.x, origin.y);


    Rectangle clRec = g.getClipBounds();
    g.setClip(sp);
    g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);

    if (fill.scale < 1) {
      fill.scale = 1;
    }
    if (fill.name != null) {
      int size = Math.max(height, width);
      try {
        JavaVectorGraphic jvg = Filling.getJavaVectorGraphic(this.fill.name);
        jvg.strokeSize = this.lineConfig.stroke;
        float scale = (float) Math.max(height/(jvg.maxY - jvg.minY), width/(jvg.maxX - jvg.minX));
        JavaVectorGraphic.bgColor = this.fill.color;
        JavaVectorGraphic.fgColor = this.lineConfig.color;
        jvg.draw(g, scale* (float) fill.scale, origin, false);
        JavaVectorGraphic.bgColor = Color.WHITE;
        JavaVectorGraphic.fgColor = Color.BLACK;
      } catch (NullPointerException nex) {
        this.fill.name = Filling.addPattern(this.fill.name);
        try {
          JavaVectorGraphic jvg = Filling.getJavaVectorGraphic(this.fill.name);
          jvg.strokeSize = this.lineConfig.stroke;
          float scale = (float) Math.max(height/(jvg.maxY - jvg.minY), width/(jvg.maxX - jvg.minX));
          JavaVectorGraphic.bgColor = this.fill.color;
          JavaVectorGraphic.fgColor = this.lineConfig.color;
          jvg.draw(g, scale*(float) fill.scale, origin, false);
          JavaVectorGraphic.bgColor = Color.WHITE;
          JavaVectorGraphic.fgColor = Color.BLACK;
        } catch (NullPointerException nnex) {
          // System.err.println("Could not find JVG: " + nnex);
        }
      }
    } else {
      if (!fill.graphic) {
        g.setColor(fill.color);
        g.fill(sp);
      } else {
        try {
          g.drawImage(Filling.imageNumberToImage.get(fill.imageNumber), (int) Math.round(origin.x - (double) width/2 * fill.scale), (int) Math.round(origin.y - (double) height* fill.scale/2), (int) Math.round(width*fill.scale), (int) Math.round(height*fill.scale), null);
        } catch (NullPointerException ign) {}
      }
    }

    g.rotate(- rotation, origin.x, origin.y);

    g.setClip(clRec);

    if (!this.lineConfig.doubleLine) {
      if (!helperLines) {

        Stroke oldStroke = g.getStroke();
        Color oldColor = g.getColor();

        float stroke = (float) lineConfig.stroke;

        if ((hoverMode != 0) && stroke < 20) {
          stroke *= (float) 1.5;
        } else if (hoverMode != 0) {
          stroke += 5;
        }

        if (lineConfig.dashed) {
          final float[] dash1 = {(float) lineConfig.dashLength};
          g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
        } else
          g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

        if (currentSelected) {
          float[] dash1 = {30f, 5};
          if (lineConfig.dashed)
            dash1 = new float[]{(float) lineConfig.dashLength};
          g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, (float) lineConfig.dashOffset));
        }

        g.setClip(clRec);
        g.setColor(lineConfig.color);
        g.rotate(rotation, origin.x, origin.y);
        g.draw(sp);
        g.rotate(-rotation, origin.x, origin.y);

      } else {
        if (this.hoverMode == 0 || this.hoverMode == 1 || this.hoverMode == 4 || this.hoverMode == 5) {
          this.draw(g, currentSelected, false);
        }
        if (this.hoverMode != 1) {
          g.setColor(Color.BLACK);
          g.setStroke(new BasicStroke(1f));
          g.drawOval((int) Math.round(origin.x) - 10, (int) Math.round(origin.y) - 10, 2 * 10, 2 * 10);
        } else {
          g.setColor(Color.BLACK);
          g.setStroke(new BasicStroke(2f));
          g.drawOval((int) Math.round(origin.x) - 10, (int) Math.round(origin.y) - 10, 2 * 10, 2 * 10);
        }
        //g.rotate(rotation, origin.x, origin.y);
        if (this.hoverMode == 4) {
          g.setStroke(new BasicStroke(2f));
          g.setColor(Color.BLUE);
        } else {
          g.setStroke(new BasicStroke(1f));
          g.setColor(Color.RED);
        }
        g.rotate(rotation, origin.x, origin.y);
        g.draw(new Line2D.Double(origin.x, origin.y + (double) height / 4, origin.x, origin.y + 3 * height / 4));
        g.draw(new Line2D.Double(origin.x, origin.y - (double) height / 4, origin.x, origin.y - 3 * height / 4));
        g.rotate(-rotation, origin.x, origin.y);

        if (hoverMode == 2 || hoverMode == 3) {

          Shape top;
          Shape left;
          Shape bottom;
          Shape right;

          if (shape.equals("oval")) {
            top = new Arc2D.Double(
              origin.x - width / 2,
              origin.y - height / 2,
              width,
              height,
              45,
              90,
              Arc2D.OPEN
            );
            left = new Arc2D.Double(
              origin.x - width / 2,
              origin.y - height / 2,
              width,
              height,
              135,
              90,
              Arc2D.OPEN
            );
            bottom = new Arc2D.Double(
              origin.x - width / 2,
              origin.y - height / 2,
              width,
              height,
              225,
              90,
              Arc2D.OPEN
            );
            right = new Arc2D.Double(
              origin.x - width / 2,
              origin.y - height / 2,
              width,
              height,
              315,
              90,
              Arc2D.OPEN
            );
          } else {
            top = new Line2D.Double(
              origin.x - (double) width / 2,
              origin.y + (double) height / 2,
              origin.x + (double) width / 2,
              origin.y + (double) height / 2
            );
            left = new Line2D.Double(
              origin.x - (double) width / 2,
              origin.y + (double) height / 2,
              origin.x - (double) width / 2,
              origin.y - (double) height / 2
            );
            bottom = new Line2D.Double(
              origin.x - (double) width / 2,
              origin.y - (double) height / 2,
              origin.x + (double) width / 2,
              origin.y - (double) height / 2
            );
            right = new Line2D.Double(
              origin.x + (double) width / 2,
              origin.y + (double) height / 2,
              origin.x + (double) width / 2,
              origin.y - (double) height / 2
            );
          }

          float stroke1 = (float) lineConfig.stroke;

          if (lineConfig.dashed) {
            final float[] dash11 = {(float) lineConfig.dashLength};
            g.setStroke(new BasicStroke(stroke1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash11, 0));
          } else
            g.setStroke(new BasicStroke(stroke1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

          if (currentSelected) {
            float[] dash12 = {30f, 5};
            if (lineConfig.dashed)
              dash12 = new float[]{(float) lineConfig.dashLength};
            g.setStroke(new BasicStroke(stroke1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash12, (float) lineConfig.dashOffset));
          }

          g.setColor(lineConfig.color);

          g.rotate(rotation, origin.x, origin.y);
          if (hoverMode == 2) {
            g.draw(left);
            g.draw(right);
          } else {
            g.draw(top);
            g.draw(bottom);
          }
          g.rotate(-rotation, origin.x, origin.y);

          float stroke2 = (float) lineConfig.stroke;

          if ((hoverMode != 0) && stroke2 < 20) {
            stroke2 *= (float) 1.5;
          } else if (hoverMode != 0) {
            stroke2 += 5;
          }

          if (lineConfig.dashed) {
            final float[] dash13 = {(float) lineConfig.dashLength};
            g.setStroke(new BasicStroke(stroke2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash13, 0));
          } else
            g.setStroke(new BasicStroke(stroke2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

          if (currentSelected) {
            float[] dash14 = {30f, 5};
            if (lineConfig.dashed)
              dash14 = new float[]{(float) lineConfig.dashLength};
            g.setStroke(new BasicStroke(stroke2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash14, (float) lineConfig.dashOffset));
          }

          Color color = (lineConfig.color);

          if (color.getRGB() == Color.BLACK.getRGB()) {
            color = Color.GRAY;
          }

          g.setColor(new Color(255 - color.getRed(), 255 - color.getBlue(), 255 - color.getGreen()));
          g.rotate(rotation, origin.x, origin.y);
          if (hoverMode != 2) {
            g.draw(left);
            g.draw(right);
          } else {
            g.draw(top);
            g.draw(bottom);
          }
          g.rotate(-rotation, origin.x, origin.y);
        }
      }
    } else {
      this.lineConfig.doubleLine = false;
      this.draw(g, currentSelected, helperLines);
      Shaped copy = new Shaped(this);
      copy.lineConfig.stroke = this.lineConfig.stroke / 3;
      copy.lineConfig.color = Color.WHITE;
      copy.lineConfig.doubleLine = false;
      copy.fill.name = null;
      copy.fill.graphic = false;
      copy.fill.color = new Color(0, 0, 0, 0);
      copy.showDesc = false;
      try {
        copy.lineConfig.arrowSize = 0;
      } catch (NullPointerException ex) {
        ex.printStackTrace();
      }
      copy.draw(g, currentSelected, false);
      this.lineConfig.doubleLine = true;
    }

    if (Pane.drawLabel && this.showDesc && this.description != null && !this.description.equals("") && this.descScale != 0) {

      if (helperLines) {

        if (this.hoverMode != 5) {
          g.setColor(Color.BLACK);
          g.setStroke(new BasicStroke(1f));
          g.drawOval((int) Math.round(origin.x + descPosX) - 10, (int) Math.round(origin.y + descPosY) - 10, 2 * 10, 2 * 10);
        } else {
          g.setColor(Color.BLACK);
          g.setStroke(new BasicStroke(2f));
          g.drawOval((int) Math.round(origin.x + descPosX) - 10, (int) Math.round(origin.y + descPosY) - 10, 2 * 10, 2 * 10);
        }
        g.setColor(Color.GRAY);
        g.setStroke(new BasicStroke(1f));
        g.drawLine((int) Math.round(origin.x + descPosX), (int) Math.round(origin.y + descPosY), (int) Math.round(origin.x), (int) Math.round(origin.y));
      }

      if (description.contains("<html>")) {
        JLabel label = new JLabel(this.description, JLabel.CENTER);
        label.setForeground(Color.BLACK);
        label.setSize(label.getPreferredSize());
        double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.cos(this.rotDesc) + label.getPreferredSize().height * Math.sin(this.rotDesc);
        double labelHeight = /*label.getPreferredSize().height*/ -label.getPreferredSize().width * Math.sin(this.rotDesc) + label.getPreferredSize().height * Math.cos(this.rotDesc);
        labelHeight *= this.descScale;
        labelWidth *= this.descScale;
        Point2D place = new Point2D(this.origin.x + this.descPosX - labelWidth / 2, this.origin.y + this.descPosY - labelHeight / 2);
        g.translate(place.x, place.y);
        g.scale(this.descScale, this.descScale);
        g.rotate(-this.rotDesc);
        label.paint(g);
        g.rotate(this.rotDesc);
        g.scale(1 / this.descScale, 1 / this.descScale);
        g.translate(-place.x, -place.y);
      } else {
        TeXFormula formula = new TeXFormula(description);
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
        icon.setForeground(Color.BLACK);
        double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.cos(this.rotDesc) + icon.getIconHeight() * Math.sin(this.rotDesc);
        double labelHeight = /*label.getPreferredSize().height*/ -icon.getIconWidth() * Math.sin(this.rotDesc) + icon.getIconHeight() * Math.cos(this.rotDesc);
        labelHeight *= this.descScale;
        labelWidth *= this.descScale;
        Point2D place = new Point2D(this.origin.x + this.descPosX - labelWidth / 2, this.origin.y + this.descPosY - labelHeight / 2);
        g.translate(place.x, place.y);
        g.scale(this.descScale, this.descScale);
        g.rotate(-this.rotDesc);
        icon.paintIcon(new JPanel(), g, 0, 0);
        g.rotate(this.rotDesc);
        g.scale(1 / this.descScale, 1 / this.descScale);
        g.translate(-place.x, -place.y);
      }
    }
  }
  /**
   * translates the shape and all connected lines and vertices
   * @param mainFrame the main window (@link ui.Frame)
   * @param delX the translation in x direction
   * @param delY the translation in y direction
   */
  public void translate(ui.Frame mainFrame, double delX, double delY) {

    this.updateAtt(mainFrame);

    for (Integer i : attL) {
      int index = (int) Math.floor(((double) i) / 2);
      FeynLine l = mainFrame.getDrawPane().lines.get(index);
      if (2 * index == i) {
        l.setStartX(l.getStartX() + delX);
        l.setStartY(l.getStartY() + delY);
      } else {
        l.setEndX(l.getEndX() + delX);
        l.setEndY(l.getEndY() + delY);
      }
    }
    for (Integer i : attV) {
      Vertex v = mainFrame.getDrawPane().vertices.get(i);
      v.origin.x += delX;
      v.origin.y += delY;
    }
    this.origin.x += delX;
    this.origin.y += delY;
  }
  /**
   * rotates the shape and moves the connected lines/vertices
   * @param mainFrame the main Window (@link ui.Frame)
   * @param delRot the angle of the rotation
   */
  public void rotate(ui.Frame mainFrame, double delRot) {

    this.updateAtt(mainFrame);

    for (Integer i : attL) {
      int index = (int) Math.floor(((double) i) / 2);
      FeynLine l = mainFrame.getDrawPane().lines.get(index);
      if (2 * index == i) {
        Point2D st = l.getStart();
        st.rotate( - delRot, this.origin.x, this.origin.y);
        l.setStart(st);
      } else {
        Point2D st = l.getEnd();
        st.rotate( - delRot, this.origin.x, this.origin.y);
        l.setEnd(st);
      }
    }
    for (Integer i : attV) {
      Vertex v = mainFrame.getDrawPane().vertices.get(i);
      Point2D pp = v.origin;
      pp.rotate( - delRot, this.origin.x, this.origin.y);
      v.origin.x = pp.x;
      v.origin.y = pp.y;
    }
    this.rotation += delRot;
    while (this.rotation >= Math.PI) {
      this.rotation -= 2 * Math.PI;
    }
    while (this.rotation < - Math.PI) {
      this.rotation += 2 * Math.PI;
    }
  }
  /**
   * changes height of the shape and moves the connected lines/vertices
   * @param mainFrame the main Window (@link ui.Frame)
   * @param delH the change in height
   */
  public void chHeight(ui.Frame mainFrame, double delH) {

    if (this.height + delH < 10)
      return;
    this.updateAtt(mainFrame);

    this.height += delH;

    for (Integer i : attL) {
      int index = (int) Math.floor(((double) i) / 2);
      FeynLine l = mainFrame.getDrawPane().lines.get(index);
      if (2 * index == i) {
        Point2D st = l.getStart();
        st.x -= this.origin.x;
        st.y -= this.origin.y;
        st.rotate(this.rotation, 0, 0);
        st.y *= (this.height)/(this.height - delH);
        st.rotate(-this.rotation, 0, 0);
        st.x += this.origin.x;
        st.y += this.origin.y;

        Map.Entry<Point2D, Boolean> start = this.nPoint(st, 1);
        if (start.getValue())
          l.setStart(start.getKey());
      } else {
        Point2D st = l.getEnd();
        st.x -= this.origin.x;
        st.y -= this.origin.y;
        st.rotate(this.rotation, 0, 0);
        st.y *= (this.height)/(this.height - delH);
        st.rotate(-this.rotation, 0, 0);
        st.x += this.origin.x;
        st.y += this.origin.y;

        Map.Entry<Point2D, Boolean> end = this.nPoint(st, 1);
        if (end.getValue())
          l.setEnd(end.getKey());
      }
    }
    for (Integer i : attV) {
      Vertex v = mainFrame.getDrawPane().vertices.get(i);
      Point2D pp = new Point2D(v.origin);
      pp.x -= this.origin.x;
      pp.y -= this.origin.y;
      pp.rotate(this.rotation, 0, 0);
      pp.y *= (this.height)/(this.height - delH);
      pp.rotate(-this.rotation, 0, 0);
      pp.x += this.origin.x;
      pp.y += this.origin.y;

      Map.Entry<Point2D, Boolean> po = this.nPoint(pp, 1);
      if (po.getValue()) {
        v.origin.x = po.getKey().x;
        v.origin.y = po.getKey().y;
      }
    }
  }
  /**
   * changes width of the shape and moves the connected lines/vertices
   * @param mainFrame the main Window (@link ui.Frame)
   * @param delW the change in width
   */
  public void chWidth(ui.Frame mainFrame, double delW) {

    if (this.width + delW < 10)
      return;
    this.updateAtt(mainFrame);

    this.width += delW;

    for (Integer i : attL) {
      int index = (int) Math.floor(((double) i) / 2);
      FeynLine l = mainFrame.getDrawPane().lines.get(index);
      if (2 * index == i) {
        Point2D st = l.getStart();
        st.x -= this.origin.x;
        st.y -= this.origin.y;
        st.rotate(this.rotation, 0, 0);
        st.x *= (this.width)/(this.width - delW);
        st.rotate(-this.rotation, 0, 0);
        st.x += this.origin.x;
        st.y += this.origin.y;

        Map.Entry<Point2D, Boolean> start = this.nPoint(st, 1);
        if (start.getValue())
          l.setStart(start.getKey());
      } else {
        Point2D st = l.getEnd();
        st.x -= this.origin.x;
        st.y -= this.origin.y;
        st.rotate(this.rotation, 0, 0);
        st.x *= (this.width)/(this.width - delW);
        st.rotate(-this.rotation, 0, 0);
        st.x += this.origin.x;
        st.y += this.origin.y;

        Map.Entry<Point2D, Boolean> end = this.nPoint(st, 1);
        if (end.getValue())
          l.setEnd(end.getKey());
      }
    }
    for (Integer i : attV) {
      Vertex v = mainFrame.getDrawPane().vertices.get(i);
      Point2D pp = new Point2D(v.origin);
      pp.x -= this.origin.x;
      pp.y -= this.origin.y;
      pp.rotate(this.rotation, 0, 0);
      pp.x *= (this.width)/(this.width - delW);
      pp.rotate(-this.rotation, 0, 0);
      pp.x += this.origin.x;
      pp.y += this.origin.y;

      Map.Entry<Point2D, Boolean> po = this.nPoint(pp, 1);
      if (po.getValue()) {
        v.origin.x = po.getKey().x;
        v.origin.y = po.getKey().y;
      }
    }
  }
  public Map.Entry<Point2D, Boolean> nPoint(Point2D p, double clipDis) {
    return this.nPoint(p, clipDis, 0);
  }
  /**
   * checks whether or not the point is near the shape and gives back
   * the clipping point
   * @param p the pointthat is checked
   * @param clipDis how far away the point can be from the shape to be
   * consider near the shape
   * @param gridSize the gridSize (@link ui.Pane#gridSize): if it is bigger
   * than 0 it clips only to points that are roughly the gridsize away
   * from each other
   * @return A map entry conisting of the input point and the value false
   * when the input point is not near the shape and the clipping point and
   * the value true if the input point is near the shape
   */
  public Map.Entry<Point2D, Boolean> nPoint(Point2D p, double clipDis, int gridSize) {
    Point2D point = new Point2D(p.x - this.origin.x, p.y - this.origin.y);
    point.rotate(this.rotation, 0, 0);
    if (this.shape.equals("rectangle")) {
      if (Math.abs(point.x - (double) this.width / 2) < clipDis && Math.abs(point.y) < (double) this.height / 2) {
        point.x = (double) this.width / 2;
        if (gridSize != 0) {
          int number = (int) (2f * Math.round(this.height / 2f / gridSize));
          double part = this.height / (number);
          double len = ((int) Math.round(point.y / part)) * part;
          point.y = len;
        }
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      } else if (Math.abs(point.x + (double) this.width / 2) < clipDis && Math.abs(point.y) < (double) this.height / 2) {
        point.x = - (double) this.width / 2;
        if (gridSize != 0) {
          int number = (int) (2f * Math.round(this.height / 2f / gridSize));
          double part = this.height / (number);
          double len = ((int) Math.round(point.y / part)) * part;
          point.y = len;
        }
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      } else if (Math.abs(point.y - (double) this.height / 2) < clipDis && Math.abs(point.x) < (double) this.width / 2) {
        point.y = (double) this.height / 2;
        if (gridSize != 0) {
          int number = (int) (2f * Math.round(this.width / 2f / gridSize));
          double part = this.width / (number);
          double len = ((int) Math.round(point.x / part)) * part;
          point.x = len;
        }
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      } else if (Math.abs(point.y + (double) this.height / 2) < clipDis && Math.abs(point.x) < (double) this.width / 2) {
        point.y =  - (double) this.height / 2;
        if (gridSize != 0) {
          int number = (int) (2f * Math.round(this.width / 2f / gridSize));
          double part = this.width / (number);
          double len = ((int) Math.round(point.x / part)) * part;
          point.x = len;
        }
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      } else if (point.distance(this.width / 2, this.height / 2) < clipDis) {
        point = new Point2D(this.width / 2, this.height / 2);
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      } else if (point.distance(this.width / 2, - this.height / 2) < clipDis) {
        point = new Point2D(this.width / 2, - this.height / 2);
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      } else if (point.distance( - this.width / 2, this.height / 2) < clipDis) {
        point = new Point2D( - this.width / 2, this.height / 2);
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      } else if (point.distance( - this.width / 2, - this.height / 2) < clipDis) {
        point = new Point2D( - this.width / 2, - this.height / 2);
        point.rotate( - this.rotation, 0, 0);
        point.x += this.origin.x;
        point.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(point, true);
      }
    } else if (this.shape.equals("oval")) {
      // double angle = Math.atan2(point.y / this.height * 2, point.x / this.width * 2);
      // Point2D nearestPoint = new Point2D(this.width / 2 * Math.cos(angle), this.height / 2 * Math.sin(angle));
      Point2D nearestPoint = Point2D.nOnEllipse(this.width, this.height, point);
      if (nearestPoint.distance(point) < clipDis) {
        if (gridSize != 0) {
          if (gridSize != 0) {
            double angle = Math.atan2(point.y / this.height * 2, point.x / this.width * 2);
            int number = (int) (4f * Math.round(Math.PI * (this.width + this.height) / 4f / gridSize));
            double part = Math.PI * 2 / (number);
            double deg = ((int) Math.round(angle / part)) * part;
            nearestPoint = new Point2D(this.width / 2 * Math.cos(deg), this.height / 2 * Math.sin(deg));
          }
        }
        nearestPoint.rotate( - this.rotation, 0, 0);
        nearestPoint.x += this.origin.x;
        nearestPoint.y += this.origin.y;
        return new AbstractMap.SimpleEntry<>(nearestPoint, true);
      }
    }
    return new AbstractMap.SimpleEntry<>(p, false);
  }
  /**
   * updates the attached lines when reshaping/moving of Shape starts
   * @param mainFrame the main´Window (@link ui.Frame)
   */
  private void updateAtt(ui.Frame mainFrame) {
    if (resTimer == null || attL == null || attV == null || !resTimer.isRunning()) {
      getAtt(mainFrame);
      if (resTimer == null) {
        resTimer = new javax.swing.Timer(500, new TimeListener());
      } else {
        resTimer.restart();
      }
    } else {
      resTimer.restart();
    }
  }
  /**
   * looks for all attached lines/vertices
   * @param mainFrame the main´Window (@link ui.Frame)
   */
  public void getAtt(ui.Frame mainFrame) {
    attL = new ArrayList<>();
    attV = new ArrayList<>();
    
    for (int i = 0; i < mainFrame.getDrawPane().lines.size(); i++) {
      FeynLine l = mainFrame.getDrawPane().lines.get(i);
      Map.Entry<Point2D, Boolean> start = this.nPoint(l.getStart(), 1);
      if (start.getValue()) {
        attL.add(2*i);
      }
      Map.Entry<Point2D, Boolean> end = this.nPoint(l.getEnd(), 1);
      if (end.getValue()) {
        attL.add(2*i+1);
      }
    }
    for (int i = 0; i < mainFrame.getDrawPane().vertices.size(); i++) {
      Vertex v = mainFrame.getDrawPane().vertices.get(i);
      Map.Entry<Point2D, Boolean> p = this.nPoint(v.origin, 1);
      if (p.getValue()) {
        attV.add(i);
      }
    }
  }
  /**
   * resets the attached lines vertices
   */
  public void resetAtt() {
    attL = null;
    attV = null;
  }
  /**
   * this class handles that the attached lines and vertices get deattached
   * when this object has not been changed for some time
   */
  private class TimeListener implements ActionListener {
    /**
     * gets called when the shaped has not been edited for a specific time
     * deatteches all vertices and lines
     * @param e the event that triggers this function
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      resetAtt();
    }
  }
}
