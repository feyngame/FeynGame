//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.io.Serializable;
import java.awt.geom.AffineTransform;
import java.awt.geom.*;

import org.scilab.forge.jlatexmath.*;
import ui.Pane;

/**
 * this class handles the momentum arrows that can be added to lines.
 * @author Sven Yannick Klein
 */
public class Momentum implements Serializable {
  public static int latexSize = 20;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 29L;
  /**
   * the line of which this is the momentum arrow of
   */
  public FeynLine line;
  /**
   * the length of the momentum arrow
   */
  public int length;
  /**
   * defaults for {@link length}
   */
  public static int lengthFERM = 50;
  public static int lengthGLUON = 50;
  public static int lengthPHOT = 50;
  /**
   * the stroke of the momentum arrow
   */
  public int stroke;
  /**
   * defaults for {@link stroke}
   */
  public static int strokeFERM = 2;
  public static int strokeGLUON = 2;
  public static int strokePHOT = 2;
  /**
   * the tipSize (size of the triangle) of the momentum arrow
   */
  public int tipSize;
  /**
   * defaults for {@link tipSize}
   */
  public static int tipSizeFERM = 15;
  public static int tipSizeGLUON = 15;
  public static int tipSizePHOT = 15;
  /**
   * the tipDent (dent of the triangle) of the momentum arrow
   */
  public int tipDent;
  /**
   * defaults for {@link tipDent}
   */
  public static int tipDentFERM = 0;
  public static int tipDentGLUON = 0;
  public static int tipDentPHOT = 0;
  /**
   * the tipHeight (height of the triangle) of the momentum arrow
   */
  public int tipHeight;
  /**
   * defaults for {@link tipHeight}
   */
  public static int tipHeightFERM = 15;
  public static int tipHeightGLUON = 15;
  public static int tipHeightPHOT = 15;
  /**
   * whether or not the momentum arrow is inverted with respect to the particle line
   */
  public boolean invert;
  /**
   * defaults for {@link invert}
   */
  public static boolean invertFERM = false;
  public static boolean invertGLUON = false;
  public static boolean invertPHOT = false;
  /**
   * the position of the momentum arrow
   */
  public double position;
  /**
   * defaults for {@link position}
   */
  public static double positionFERM = .5;
  public static double positionGLUON = .5;
  public static double positionPHOT = .5;
  /**
   * the distance of the momentum arrow to the line
   */
  public int distance;
  /**
   * defaults for {@link distance}
   */
  public static int distanceFERM = 20;
  public static double distanceGLUON = 19d/25d + 1d;
  public static double distancePHOT = 2d/5d + 1d;
  /**
   * whether or not a momentum is drawn
   */
  public boolean drawMomentum;
  /**
   * defaults for {@link drawMomentum}
   */
  public static boolean drawMomentumFERM = false;
  public static boolean drawMomentumGLUON = false;
  public static boolean drawMomentumPHOT = false;
  /**
   * whether or not the line color is used
   */
  public boolean lineColor;
  /**
   * defaults for {@link lineColor}
   */
  public static boolean lineColorFERM = false;
  public static boolean lineColorGLUON = false;
  public static boolean lineColorPHOT = false;
  /**
   * if {@link lineColor} == false this gives the color
   */
  public Color color;
  /**
   * defaults for {@link color}
   */
  public static Color colorFERM = Color.BLACK;
  public static Color colorGLUON = Color.BLACK;
  public static Color colorPHOT = Color.BLACK;
  /**
   * if the description is displayed or not
   */
  public Boolean showDesc;
  /**
   * defaults for {@link showDesc}
   */
  public static Boolean showDescFERM = false;
  public static Boolean showDescGLUON = false;
  public static Boolean showDescPHOT = false;
  /**
   * the amount of pixels the description is displayed above(/below) the line
   */
  public int descDis;
  /**
   * defaults for {@link descDis}
   */
  public static int descDisFERM = 20;
  public static int descDisGLUON = 20;
  public static int descDisPHOT = 20;
  /**
   * the place of the discription relative on the path of the line
   */
  public double partPlaceDesc;
  /**
   * defaults for {@link partPlaceDesc}
   */
  public static double partPlaceDescFERM = .5;
  public static double partPlaceDescGLUON = .5;
  public static double partPlaceDescPHOT = .5;
  /**
   * the rotation of the description
   */
  public double rotDesc;
  /**
   * defaults for {@link rotDesc}
   */
  public static double rotDescFERM = 0;
  public static double rotDescGLUON = 0;
  public static double rotDescPHOT = 0;
  /**
   * describes the scaling of the description
   */
  public double descScale;
  /**
   * defaults for {@link descScale}
   */
  public static double descScaleFERM = 1;
  public static double descScaleGLUON = 1;
  public static double descScalePHOT = 1;
  /**
   * whether or not the arrow tail is dashed
   */
  public boolean dashed;
  /**
   * defaults for {@link dashed}
   */
  public static boolean dashedFERM = false;
  public static boolean dashedGLUON = false;
  public static boolean dashedPHOT = false;
  /**
   * the length of the dashes of the tail of the arrow if dashed is true
   */
  public int dashLength;
  /**
   * defaults for {@link dashLength}
   */
  public static int dashLengthFERM = 10;
  public static int dashLengthGLUON = 10;
  public static int dashLengthPHOT = 10;
  /**
   * wether or not to draw the tail double
   */
  public boolean doubleLine;
  /**
   * defaults for {@link doubleLine}
   */
  public static boolean doubleLineFERM = false;
  public static boolean doubleLineGLUON = false;
  public static boolean doubleLinePHOT = false;
  /**
   * the string displayed as label/description
   */
  public String description = "p";
  public Point2D getLabelPosition() {
    Point2D place = new Point2D(0,0);
    if(Math.abs(line.height) < .1) {
      double len = Math.sqrt((line.getStartX() - line.getEndX()) * (line.getStartX() - line.getEndX()) + (line.getStartY() - line.getEndY()) * (line.getStartY() - line.getEndY()));

      double normtanx = - line.getStartX() + line.getEndX();
      normtanx /= len;
      double normtany = - line.getStartY() + line.getEndY();
      normtany /= len;

      double startPos;
      double endPos;
      if(!invert) {
        startPos = position - length / len / 2d;
        endPos = position + (length - tipSize) / len / 2d;
      } else {
        startPos = position - (length - tipSize) / len / 2d;
        endPos = position + length / len / 2d;
      }

      Point2D start = new Point2D(
        line.getStartX() +
        normtanx * len * startPos + normtany * distance,
        line.getStartY() +
        normtany * len * startPos - normtanx * distance
      );
      Point2D end = new Point2D(
        line.getStartX() +
        normtanx * len * endPos + normtany * distance,
        line.getStartY() +
        normtany * len * endPos - normtanx * distance
      );

      place = new Point2D(start.x  + (length * this.partPlaceDesc) * normtanx + (this.descDis) * normtany, start.y + (length * this.partPlaceDesc) * normtany - (this.descDis) * normtanx);
    } else{
      double len = line.radius * line.deltaArch;

      double radius = Math.abs(line.radius) - Math.signum(line.height) * this.distance;

      double lenn = Math.signum(line.height) * radius * line.deltaArch;

      double startPos;
      double endPos;
      if(invert) {
        startPos = position - length / lenn / 2d;
        endPos = position + (length - tipSize) / lenn / 2d;
      } else {
        startPos = position - (length - tipSize) / lenn / 2d;
        endPos = position + length / lenn / 2d;
      }

      Point2D start;
      Point2D end;

      double partArchStart = -line.deltaArch * startPos;
      double partArchEnd = -line.deltaArch * endPos;
      double placeArchStart = line.archStart - partArchStart + Math.PI/2;
      double placeArchEnd = line.archStart - partArchEnd + Math.PI/2;

      double deltaArch = placeArchEnd - placeArchStart;
      double partArch = deltaArch * this.partPlaceDesc;
      double placeArch = placeArchStart + partArch - Math.signum(line.height) * Math.PI/2;
      double lradius = radius - Math.signum(line.height) * this.descDis;
      double x;
      double y;
      if (line.height > 0) {
        y = line.origin.y + lradius * Math.sin(placeArch);
        x = line.origin.x + lradius * Math.cos(placeArch);
      } else {
        y = line.origin.y - lradius * Math.sin(placeArch);
        x = line.origin.x - lradius * Math.cos(placeArch);
      }
      place = new Point2D(x, y);
      if (line.getStart().equals(line.getEnd())) {
        place.rotate(-line.circAngle, line.getStart().x, line.getStart().y);
      }
    }
    return place;

  }
  /**
   * as the name suggests, this mehtod draws the arrow on the canvas
   * @param g the graphics object that is drawn on
   */
  public void draw(Graphics2D g) {

    if (!this.drawMomentum)
      return;

    if (showDesc == null)
      this.initDesc();

    if (this.lineColor)
      g.setColor(line.lineConfig.color);
    else
      g.setColor(this.color);

    if (this.dashed) {
      final float[] dash1 = {this.dashLength};
      g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
    } else
      g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

    if(Math.abs(line.height) < .1) {

      double len = Math.sqrt((line.getStartX() - line.getEndX()) * (line.getStartX() - line.getEndX()) + (line.getStartY() - line.getEndY()) * (line.getStartY() - line.getEndY()));

      double normtanx = - line.getStartX() + line.getEndX();
      normtanx /= len;
      double normtany = - line.getStartY() + line.getEndY();
      normtany /= len;

      double startPos;
      double endPos;
      double startPosReal = (double) position - (double) length / (double) len / 2d;
      double endPosReal = (double) position + (double) length / (double) len / 2d;;
      if(!invert) {
        startPos = (double) position - (double) length / (double) len / 2d;
        endPos = (double) position + ((double) length - (double) tipSize + (double) tipDent) / (double) len / 2d;
      } else {
        startPos = (double) position - ((double) length - (double) tipSize + (double) tipDent) / (double) len / 2d;
        endPos = (double) position + (double) length / (double) len / 2d;
      }

      Point2D start = new Point2D(
        line.getStartX() +
        normtanx * len * startPos + normtany * distance,
        line.getStartY() +
        normtany * len * startPos - normtanx * distance
      );
      Point2D end = new Point2D(
        line.getStartX() +
        normtanx * len * endPos + normtany * distance,
        line.getStartY() +
        normtany * len * endPos - normtanx * distance
      );

      Point2D startReal = new Point2D(
        line.getStartX() +
        normtanx * len * startPosReal + normtany * distance,
        line.getStartY() +
        normtany * len * startPosReal - normtanx * distance
      );
      Point2D endReal = new Point2D(
        line.getStartX() +
        normtanx * len * endPosReal + normtany * distance,
        line.getStartY() +
        normtany * len * endPosReal - normtanx * distance
      );

      g.draw(new Line2D.Double(start.x, start.y, end.x, end.y));

      if (this.doubleLine) {

        g.setColor(Color.WHITE);

        if (this.dashed) {
          final float[] dash1 = {this.dashLength};
          g.setStroke(new BasicStroke(stroke / 3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
        } else
          g.setStroke(new BasicStroke(stroke / 3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

        g.drawLine((int) start.x, (int) start.y,
        (int) end.x, (int) end.y);

         if (this.lineColor)
          g.setColor(line.lineConfig.color);
        else
          g.setColor(this.color);

        if (this.dashed) {
          final float[] dash1 = {this.dashLength};
          g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
        } else
          g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));
      }

      java.awt.geom.Path2D.Double arrow = new java.awt.geom.Path2D.Double();
      double rotAngle = line.angle;

      if (invert)
        rotAngle += Math.PI;

      Point2D upper = new Point2D((double) -this.tipSize, ((double) this.tipHeight)/2d);
      Point2D lower = new Point2D((double) -this.tipSize, - ((double) this.tipHeight)/2d);
      Point2D tip = new Point2D(0d, 0d);
      Point2D mid = new Point2D((double) - this.tipSize + (double) this.tipDent, 0d);
      upper.rotate(- rotAngle, 0, 0);
      lower.rotate(- rotAngle, 0, 0);
      tip.rotate(- rotAngle, 0, 0);
      mid.rotate(- rotAngle, 0, 0);

      arrow.moveTo(upper.x, upper.y);
      arrow.lineTo(mid.x, mid.y);
      arrow.lineTo(lower.x, lower.y);
      arrow.lineTo(tip.x, tip.y);

      AffineTransform tf = new AffineTransform();
      if (!invert)
        tf.translate(endReal.x, endReal.y);
      else
        tf.translate(startReal.x, startReal.y);
      arrow.transform(tf);
      g.fill(arrow);

      if (Pane.drawLabel && this.showDesc && this.description != null && !this.description.equals("") && this.descScale != 0) {
        if (description.contains("<html>")) {
          Point2D place;
          JLabel label = new JLabel(this.description, JLabel.CENTER);

          label.setForeground(Color.BLACK);
          label.setSize(label.getPreferredSize());
          double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.cos(this.rotDesc) + label.getPreferredSize().height * Math.sin(this.rotDesc);
          double labelHeight = /*label.getPreferredSize().height*/ -label.getPreferredSize().width * Math.sin(this.rotDesc) + label.getPreferredSize().height * Math.cos(this.rotDesc);
          labelHeight *= this.descScale;
          labelWidth *= this.descScale;

          place = new Point2D(start.x - labelWidth / 2 + (length * this.partPlaceDesc) * normtanx + (this.descDis) * normtany, start.y - labelHeight / 2 + (length * this.partPlaceDesc) * normtany - (this.descDis) * normtanx);

          g.translate(place.x, place.y);
          g.scale(this.descScale, this.descScale);
          g.rotate( - this.rotDesc);
          label.paint(g);
          g.rotate(this.rotDesc);
          g.scale(1/this.descScale, 1/this.descScale);
          g.translate( - place.x, - place.y);
        } else {
          Point2D place;
          TeXFormula formula = new TeXFormula(description);
          TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
          icon.setForeground(Color.BLACK);
          double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.cos(this.rotDesc) + icon.getIconHeight() * Math.sin(this.rotDesc);
          double labelHeight = /*label.getPreferredSize().height*/ -icon.getIconWidth() * Math.sin(this.rotDesc) + icon.getIconHeight() * Math.cos(this.rotDesc);
          labelHeight *= this.descScale;
          labelWidth *= this.descScale;

          place = new Point2D(start.x - labelWidth / 2 + (length * this.partPlaceDesc) * normtanx + (this.descDis) * normtany, start.y - labelHeight / 2 + (length * this.partPlaceDesc) * normtany - (this.descDis) * normtanx);

          g.translate(place.x, place.y);
          g.scale(this.descScale, this.descScale);
          g.rotate( - this.rotDesc);
          icon.paintIcon(new JPanel(), g, 0, 0);
          g.rotate(this.rotDesc);
          g.scale(1/this.descScale, 1/this.descScale);
          g.translate( - place.x, - place.y);
        }
      }

    } else {

      double len = line.radius * line.deltaArch;

      double radius = Math.abs(line.radius) - Math.signum(line.height) * this.distance;

      double lenn = Math.signum(line.height) * radius * line.deltaArch;

      double startPos;
      double endPos;
      double startPosReal = startPos = (double) position - (double) length / (double) lenn / 2d;
      double endPosReal = (double) position + (double) length / (double) lenn / 2d;
      if(invert) {
        startPos = (double) position - (double) length / (double) lenn / 2d;
        endPos = (double) position + ((double) length - (double) tipSize + (double) tipDent) / (double) lenn / 2d;
      } else {
        startPos = (double) position - ((double) length - (double) tipSize + (double) tipDent) / (double) lenn / 2d;
        endPos = (double) position + (double) length / (double) lenn / 2d;
      }

      Point2D start;
      Point2D end;

      double partArchStart = -line.deltaArch * startPos;
      double partArchEnd = -line.deltaArch * endPos;
      double placeArchStart = line.archStart - partArchStart + Math.PI/2;
      double placeArchEnd = line.archStart - partArchEnd + Math.PI/2;
      Arc2D arc = new Arc2D.Double(
        (line.center.x - radius),
        (line.center.y - radius),
        (2d * radius),
        (2d * radius),
        ( - (placeArchStart) * 180d / Math.PI + 90),
        ( - (placeArchEnd - placeArchStart) * 180d / Math.PI),
        Arc2D.OPEN
      );

      if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .1) {
        g.rotate(line.circAngle, line.getStart().x, line.getStart().y);
        g.draw(arc);
        g.rotate(-line.circAngle, line.getStart().x, line.getStart().y);
      } else {
        g.draw(arc);
      }

      if (this.doubleLine) {

        g.setColor(Color.WHITE);

        if (this.dashed) {
          final float[] dash1 = {this.dashLength};
          g.setStroke(new BasicStroke(stroke / 3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
        } else
          g.setStroke(new BasicStroke(stroke / 3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

        if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .1) {
          g.rotate(line.circAngle, line.getStart().x, line.getStart().y);
          g.draw(arc);
          g.rotate(-line.circAngle, line.getStart().x, line.getStart().y);
        } else {
          g.draw(arc);
        }

         if (this.lineColor)
          g.setColor(line.lineConfig.color);
        else
          g.setColor(this.color);

        if (this.dashed) {
          final float[] dash1 = {this.dashLength};
          g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
        } else
          g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));
      }

      java.awt.geom.Path2D.Double arrow = new java.awt.geom.Path2D.Double();

      double rotAngle = 0;
      if (invert) {
        rotAngle -= line.deltaArch * ( - endPos + .5);
      } else {
        rotAngle -= line.deltaArch * (- startPos + .5);
      }

      if (invert)
        rotAngle += Math.PI;

      Point2D upper = new Point2D((double) -this.tipSize, ((double) this.tipHeight)/2d);
      Point2D lower = new Point2D((double) -this.tipSize, - ((double) this.tipHeight)/2d);
      Point2D tip = new Point2D(0d, 0d);
      Point2D mid = new Point2D((double) - this.tipSize + (double) this.tipDent, 0d);
      upper.rotate( - line.angle, 0, 0);
      lower.rotate( - line.angle, 0, 0);
      tip.rotate( - line.angle, 0, 0);
      mid.rotate( - line.angle, 0, 0);

      arrow.moveTo(upper.x, upper.y);
      arrow.lineTo(mid.x, mid.y);
      arrow.lineTo(lower.x, lower.y);
      arrow.lineTo(tip.x, tip.y);

      partArchStart = -line.deltaArch * startPosReal;
      partArchEnd = -line.deltaArch * endPosReal;
      placeArchStart = line.archStart - partArchStart + Math.PI/2;
      placeArchEnd = line.archStart - partArchEnd + Math.PI/2;
      arc = new Arc2D.Double(
        (line.center.x - radius),
        (line.center.y - radius),
        (2d * radius),
        (2d * radius),
        ( - (placeArchStart) * 180d / Math.PI + 90),
        ( - (placeArchEnd - placeArchStart) * 180d / Math.PI),
        Arc2D.OPEN
      );

      AffineTransform tf = new AffineTransform();
      if (invert) {
        tf.translate(
          arc.getEndPoint().getX(),
          arc.getEndPoint().getY()
        );
      } else {
        tf.translate(
          arc.getStartPoint().getX(),
          arc.getStartPoint().getY()
        );
      }
      tf.rotate(rotAngle);
      arrow.transform(tf);
      if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .1) {
        g.rotate(line.circAngle, line.getStart().x, line.getStart().y);
        g.fill(arrow);
        g.rotate(-line.circAngle, line.getStart().x, line.getStart().y);
      } else {
        g.fill(arrow);
      }

      if (Pane.drawLabel && this.showDesc && this.description != null && !this.description.equals("") && this.descScale != 0) {

        if (description.contains("<html>")) {

          Point2D place;
          JLabel label = new JLabel(this.description, JLabel.CENTER);

          label.setForeground(Color.BLACK);
          label.setSize(label.getPreferredSize());
          double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.cos(this.rotDesc) + label.getPreferredSize().height * Math.sin(this.rotDesc);
          double labelHeight = /*label.getPreferredSize().height*/ -label.getPreferredSize().width * Math.sin(this.rotDesc) + label.getPreferredSize().height * Math.cos(this.rotDesc);
          labelHeight *= this.descScale;
          labelWidth *= this.descScale;

          double deltaArch = placeArchEnd - placeArchStart;
          double partArch = deltaArch * this.partPlaceDesc;
          double placeArch = placeArchStart + partArch - Math.signum(line.height) * Math.PI/2;
          double lradius = radius - Math.signum(line.height) * this.descDis;
          double x;
          double y;
          if (line.height > 0) {
            y = line.center.y + lradius * Math.sin(placeArch);
            x = line.center.x + lradius * Math.cos(placeArch);
          } else {
            y = line.center.y - lradius * Math.sin(placeArch);
            x = line.center.x - lradius * Math.cos(placeArch);
          }
          place = new Point2D(x, y);

          if (line.getStart().equals(line.getEnd())) {
            place.rotate(-line.circAngle, line.getStart().x, line.getStart().y);
            place.x -= labelWidth / 2;
            place.y -= labelHeight / 2;
            g.translate(place.x, place.y);
            g.scale(this.descScale, this.descScale);
            g.rotate( - this.rotDesc);
            label.paint(g);
            g.rotate(this.rotDesc);
            g.scale(1/this.descScale, 1/this.descScale);
            g.translate( - place.x, - place.y);
          } else {
            place.x -= labelWidth / 2;
            place.y -= labelHeight / 2;
            g.translate(place.x, place.y);
            g.scale(this.descScale, this.descScale);
            g.rotate( - this.rotDesc);
            label.paint(g);
            g.rotate(this.rotDesc);
            g.scale(1/this.descScale, 1/this.descScale);
            g.translate( - place.x, - place.y);
          }
        } else {
          Point2D place;
          TeXFormula formula = new TeXFormula(description);
          TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
          icon.setForeground(Color.BLACK);
          double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.cos(this.rotDesc) + icon.getIconHeight() * Math.sin(this.rotDesc);
          double labelHeight = /*label.getPreferredSize().height*/ -icon.getIconWidth() * Math.sin(this.rotDesc) + icon.getIconHeight() * Math.cos(this.rotDesc);
          labelHeight *= this.descScale;
          labelWidth *= this.descScale;

          double deltaArch = placeArchEnd - placeArchStart;
          double partArch = deltaArch * this.partPlaceDesc;
          double placeArch = placeArchStart + partArch - Math.signum(line.height) * Math.PI/2;
          double lradius = radius - Math.signum(line.height) * this.descDis;
          double x;
          double y;
          if (line.height > 0) {
            y = line.center.y + lradius * Math.sin(placeArch);
            x = line.center.x + lradius * Math.cos(placeArch);
          } else {
            y = line.center.y - lradius * Math.sin(placeArch);
            x = line.center.x - lradius * Math.cos(placeArch);
          }
          place = new Point2D(x, y);

          if (line.getStart().equals(line.getEnd())) {
            place.rotate(-line.circAngle, line.getStart().x, line.getStart().y);
            place.x -= labelWidth / 2;
            place.y -= labelHeight / 2;
            g.translate(place.x, place.y);
            g.scale(this.descScale, this.descScale);
            g.rotate( - this.rotDesc);
            icon.paintIcon(new JPanel(), g, 0, 0);
            g.rotate(this.rotDesc);
            g.scale(1/this.descScale, 1/this.descScale);
            g.translate( - place.x, - place.y);
          } else {
            place.x -= labelWidth / 2;
            place.y -= labelHeight / 2;
            g.translate(place.x, place.y);
            g.scale(this.descScale, this.descScale);
            g.rotate( - this.rotDesc);
            icon.paintIcon(new JPanel(), g, 0, 0);
            g.rotate(this.rotDesc);
            g.scale(1/this.descScale, 1/this.descScale);
            g.translate( - place.x, - place.y);
          }
        }
      }
    }
  }
  /**
   * sets the options used for this line to the default for the linetype of
   * the line
   */
  public void setDefault() {
    if (line.lineConfig.lineType == LineType.FERMION) {
      lengthFERM = length;
      strokeFERM = stroke;
      tipSizeFERM = tipSize;
      tipDentFERM = tipDent;
      tipHeightFERM = tipHeight;
      invertFERM = invert;
      positionFERM = position;
      distanceFERM = distance;
      drawMomentumFERM = drawMomentum;
      lineColorFERM = lineColor;
      colorFERM = color;
      showDescFERM = showDesc;
      descDisFERM = descDis;
      partPlaceDescFERM = partPlaceDesc;
      rotDescFERM = rotDesc;
      descScaleFERM = descScale;
      doubleLineFERM = doubleLine;
      dashedFERM = dashed;
      dashLengthFERM = dashLength;
    } else if (line.lineConfig.lineType == LineType.GLUON || line.lineConfig.lineType == LineType.SSPIRAL) {
      lengthGLUON = length;
      strokeGLUON = stroke;
      tipSizeGLUON = tipSize;
      tipDentGLUON = tipDent;
      tipHeightGLUON = tipHeight;
      invertGLUON = invert;
      positionGLUON = position;
      distanceGLUON = ((double) distance) / line.lineConfig.wiggleHeight;
      drawMomentumGLUON = drawMomentum;
      lineColorGLUON = lineColor;
      colorGLUON = color;
      showDescGLUON = showDesc;
      descDisGLUON = descDis;
      partPlaceDescGLUON = partPlaceDesc;
      rotDescGLUON = rotDesc;
      descScaleGLUON = descScale;
      doubleLineGLUON = doubleLine;
      dashedGLUON = dashed;
      dashLengthGLUON = dashLength;
    } else if (line.lineConfig.lineType == LineType.PHOTON || line.lineConfig.lineType == LineType.SWAVE) {
      lengthPHOT = length;
      strokePHOT = stroke;
      tipSizePHOT = tipSize;
      tipDentPHOT = tipDent;
      tipHeightPHOT = tipHeight;
      invertPHOT = invert;
      positionPHOT = position;
      distancePHOT = ((double) distance) / line.lineConfig.wiggleHeight;
      drawMomentumPHOT = drawMomentum;
      lineColorPHOT = lineColor;
      colorPHOT = color;
      showDescPHOT = showDesc;
      descDisPHOT = descDis;
      partPlaceDescPHOT = partPlaceDesc;
      rotDescPHOT = rotDesc;
      descScalePHOT = descScale;
      doubleLinePHOT = doubleLine;
      dashedPHOT = dashed;
      dashLengthPHOT = dashLength;
    }
  }
  /**
   * constructs a Momentum object based on the line this momentum arrow is
   * attached to and the default options for the linetype of this line
   * @param line the {@link game.FeynLine} this momentum arrow will be
   * connected to
   */
  public Momentum(FeynLine line) {
    this.line = line;

    if (line.lineConfig.lineType == LineType.FERMION) {
      length = lengthFERM;
      stroke = strokeFERM;
      tipSize = tipSizeFERM;
      tipDent = tipDentFERM;
      tipHeight = tipHeightFERM;
      invert = invertFERM;
      position = positionFERM;
      distance = distanceFERM;
      this.lineColor = lineColorFERM;
      this.color = colorFERM;
      showDesc = showDescFERM;
      descDis = descDisFERM;
      partPlaceDesc = partPlaceDescFERM;
      rotDesc = rotDescFERM;
      descScale = descScaleFERM;
      doubleLine = doubleLineFERM;
      dashed = dashedFERM;
      dashLength = dashLengthFERM;
    } else if (line.lineConfig.lineType == LineType.GLUON || line.lineConfig.lineType == LineType.SSPIRAL) {
      length = lengthGLUON;
      stroke = strokeGLUON;
      tipSize = tipSizeGLUON;
      tipDent = tipDentGLUON;
      tipHeight = tipHeightGLUON;
      invert = invertGLUON;
      position = positionGLUON;
      distance = (int) Math.round(distanceGLUON * line.lineConfig.wiggleHeight);
      this.lineColor = lineColorGLUON;
      this.color = colorGLUON;
      showDesc = showDescGLUON;
      descDis = descDisGLUON;
      partPlaceDesc = partPlaceDescGLUON;
      rotDesc = rotDescGLUON;
      descScale = descScaleGLUON;
      doubleLine = doubleLineGLUON;
      dashed = dashedGLUON;
      dashLength = dashLengthGLUON;
    } else if (line.lineConfig.lineType == LineType.PHOTON || line.lineConfig.lineType == LineType.SWAVE) {
      length = lengthPHOT;
      stroke = strokePHOT;
      tipSize = tipSizePHOT;
      tipDent = tipDentPHOT;
      tipHeight = tipHeightPHOT;
      invert = invertPHOT;
      position = positionPHOT;
      distance = (int) Math.round(distancePHOT * line.lineConfig.wiggleHeight);
      this.lineColor = lineColorPHOT;
      this.color = colorPHOT;
      showDesc = showDescPHOT;
      descDis = descDisPHOT;
      partPlaceDesc = partPlaceDescPHOT;
      rotDesc = rotDescPHOT;
      descScale = descScalePHOT;
      doubleLine = doubleLinePHOT;
      dashed = dashedPHOT;
      dashLength = dashLengthPHOT;
    }
    drawMomentum = true;
  }
  /**
   * copy constructor
   * @param old the momentum arrow to be copied
   */
  public Momentum(Momentum old) {
    this.length = old.length;
    this.stroke = old.stroke;
    this.tipSize = old.tipSize;
    this.tipHeight = old.tipHeight;
    this.tipDent = old.tipDent;
    this.invert = old.invert;
    this.position = old.position;
    this.distance = old.distance;
    this.drawMomentum = old.drawMomentum;
    this.line = old.line;
    this.lineColor = old.lineColor;
    this.color = old.color;
    this.showDesc = old.showDesc;
    this.descDis = old.descDis;
    this.partPlaceDesc = old.partPlaceDesc;
    this.rotDesc = old.rotDesc;
    this.descScale = old.descScale;
    this.dashed = old.dashed;
    this.dashLength = old.dashLength;
    this.doubleLine = old.doubleLine;
  }
  /**
   * this makes a momentum arrow use the preferences of another momentum
   * arrow while not changing the line the momentum arrow is attached to
   * @param old the momentum arrow to copy the options from
   */
  public void copy(Momentum old) {
    this.length = old.length;
    this.stroke = old.stroke;
    this.tipSize = old.tipSize;
    this.tipHeight = old.tipHeight;
    this.tipDent = old.tipDent;
    this.invert = old.invert;
    this.position = old.position;
    this.distance = old.distance;
    this.lineColor = old.lineColor;
    this.color = old.color;
    this.drawMomentum = old.drawMomentum;
    this.showDesc = old.showDesc;
    this.descDis = old.descDis;
    this.partPlaceDesc = old.partPlaceDesc;
    this.rotDesc = old.rotDesc;
    this.descScale = old.descScale;
    this.dashed = old.dashed;
    this.dashLength = old.dashLength;
    this.doubleLine = old.doubleLine;
  }
  /**
   * returns exact copy including description;
   * @param fl the line of the new momentum arrow
   * @return the copy of the momentum arrow
   */
  public Momentum getCopy(FeynLine fl) {
    Momentum m = new Momentum(fl);
    m.copy(this);
    m.description = this.description;
    return m;
  }
  /**
   * combnines the options for two momentum arrow to be the options for this
   * momentum arrow, used when lines get combined
   * @param old1 one of the other momentum arrows
   * @param old2 the other of the other momentum arrows
   */
  public void combine(Momentum old1, Momentum old2) {
    this.length = (int) Math.round(((double) (old1.length + old2.length)) / 2);
    this.stroke = (int) Math.round(((double) (old1.stroke + old2.stroke)) / 2);
    this.tipSize = (int) Math.round(((double) (old1.tipSize + old2.tipSize)) / 2);
    this.tipHeight = (int) Math.round(((double) (old1.tipHeight + old2.tipHeight)) / 2);
    this.tipDent = (int) Math.round(((double) (old1.tipDent + old2.tipDent)) / 2);
    this.invert = old1.invert;
    this.position = (((old1.position + old2.position)) / 2);
    this.distance = (int) Math.round(((double) (old1.distance + old2.distance)) / 2);
    this.lineColor = old1.lineColor;
    this.color = blend(old1.color, old2.color, 0.5f);
    if (old1.doubleLine && old2.doubleLine)
      this.doubleLine = true;
    else
      this.doubleLine = false;

    if (old1.dashed && old2.dashed) {
      this.dashed = true;
      this.dashLength = (int) Math.round(((double) (old1.dashLength + old2.dashLength)) / 2);
    } else {
      this.dashed = false;
    }
    if (old1.showDesc && old2.showDesc && old1.description != null && old2.description != null) {
      this.showDesc = old1.showDesc;
      this.descDis = (int) Math.round((double) (old1.descDis + old2.descDis) / 2);
      this.partPlaceDesc = (((old1.partPlaceDesc + old2.partPlaceDesc)) / 2);
      this.rotDesc = (((old1.rotDesc + old2.rotDesc)) / 2);
      this.descScale = (((old1.descScale + old2.descScale)) / 2);
      String desc1 = old1.description;
      String desc2 = old2.description;
      boolean hasHTML = desc1.contains("<html>") || desc2.contains("<html>");
      desc1 = desc1.replaceAll("<html>", "");
      desc2 = desc2.replaceAll("<html>", "");
      this.description = "";
      if (hasHTML)
        this.description += "<html>";
      this.description += desc1 + "+" + desc2;
    } else if ((old1.showDesc && old1.description != null) && !(old2.showDesc && old2.description != null)) {
      this.showDesc = old1.showDesc;
      this.descDis = old1.descDis;
      this.partPlaceDesc = old1.partPlaceDesc;
      this.rotDesc = old1.rotDesc;
      this.descScale = old1.descScale;
      this.description = old1.description;
    } else if (!(old1.showDesc && old1.description != null) && (old2.showDesc && old2.description != null)) {
      this.showDesc = old2.showDesc;
      this.descDis = old2.descDis;
      this.partPlaceDesc = old2.partPlaceDesc;
      this.rotDesc = old2.rotDesc;
      this.descScale = old2.descScale;
      this.description = old2.description;
    } else {
      this.showDesc = false;
    }
  }
  /**
   * this mixes the two colors with a ratio
   * @param c1 the first color
   * @param c2 the second color
   * @param ratio the mixing ratio between these two colors
   * @return the mixed color
   */
  private Color blend( Color c1, Color c2, float ratio ) {
    if ( ratio > 1f ) ratio = 1f;
    else if ( ratio < 0f ) ratio = 0f;
    float iRatio = 1.0f - ratio;

    int i1 = c1.getRGB();
    int i2 = c2.getRGB();

    int a1 = (i1 >> 24 & 0xff);
    int r1 = ((i1 & 0xff0000) >> 16);
    int g1 = ((i1 & 0xff00) >> 8);
    int b1 = (i1 & 0xff);

    int a2 = (i2 >> 24 & 0xff);
    int r2 = ((i2 & 0xff0000) >> 16);
    int g2 = ((i2 & 0xff00) >> 8);
    int b2 = (i2 & 0xff);

    int a = (int)((a1 * iRatio) + (a2 * ratio));
    int r = (int)((r1 * iRatio) + (r2 * ratio));
    int g = (int)((g1 * iRatio) + (g2 * ratio));
    int b = (int)((b1 * iRatio) + (b2 * ratio));

    return new Color( a << 24 | r << 16 | g << 8 | b );
  }
  /**
   * initializes the description of the momentum arrow based on the defaults
   */
  private void initDesc() {
    if (line.lineConfig.lineType == LineType.FERMION) {
      showDesc = showDescFERM;
      descDis = descDisFERM;
      partPlaceDesc = partPlaceDescFERM;
      rotDesc = rotDescFERM;
      descScale = descScaleFERM;
    } else if (line.lineConfig.lineType == LineType.GLUON) {
      showDesc = showDescGLUON;
      descDis = descDisGLUON;
      partPlaceDesc = partPlaceDescGLUON;
      rotDesc = rotDescGLUON;
      descScale = descScaleGLUON;
    } else {
      showDesc = showDescPHOT;
      descDis = descDisPHOT;
      partPlaceDesc = partPlaceDescPHOT;
      rotDesc = rotDescPHOT;
      descScale = descScalePHOT;
    }
  }
}
