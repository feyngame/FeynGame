//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import amplitude.FeynmanRuleTemplates;
import ui.Frame;

/**
 * LineConfig defines a specific line layout loaded from config file. {@link game.LineConfig} is part of {@link game.FeynLine} and used mainly in {@link game.FeynLine#draw(Graphics2D, boolean)}.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
@SuppressWarnings("overrides")
public class LineConfig implements Serializable {
  private static final Logger LOGGER = Frame.getLogger(LineConfig.class);
  /**
   * All possible keys that can be used in the config file.            #1         #2      #3        #4              #5          #6                #7            #8          #9           #10       #11          #12            #13          #14         #15           #16          #17          #18
   */
  private static final java.util.List<String> mapKeys = Arrays.asList("color", "stroke", "dash", "wigglesize", "wiggleheight", "wiggleoffset", "dashlength", "arrowsize", "arrowplace", "double", "label", "wigglesharpness", "showarrow", "indices", "propagator", "external", "external*", "anticommuting", "arrowdent", "arrowheight", "wiggleasymmetry", "straightlength", "straightlengtharrow"); // lower case!!!!
  /**
   * All lines that were parsed from config or generated by default.
   */
  @SuppressWarnings("CanBeFinal")
  public static ArrayList<LineConfig> presets = new ArrayList<>();
  /**
   * All vertices that are allowed by config or default.
   */
  @SuppressWarnings("CanBeFinal")
  public static ArrayList<Map.Entry<HashMap<String, Integer>, String>> rules = new ArrayList<>();
  /**
   * pattern to match index definitions
   */
  public static Pattern ptIndices = Pattern.compile("^(?:\\s*?)<\\d+(\\|\\d+)*>(?:\\s*?)");
  /**
   * object containg the templates for the amplitude
   */
  public static FeynmanRuleTemplates feynmanRuleTemplates = new FeynmanRuleTemplates();
  /**
   * Visual representation of the line which is used to draw it to screen. Loaded from config (mandatory).
   */
  @SuppressWarnings("CanBeFinal")
  public LineType lineType;
  /**
   * The line width width drawing. Loaded from config (optionally).
   */
  public float stroke;
  /**
   * If the {@link #lineType} is {@link game.LineType#PHOTON} or {@link game.LineType#GLUON}, this is used to influence the size of the wiggles. Loaded from config (optionally).
   */
  public float wiggleLength;
  /**
   * If the {@link #lineType} is {@link game.LineType#PHOTON} or {@link game.LineType#GLUON}, this is used to influence the amplitude of the wiggles. Loaded from config (optionally).
   */
  public float wiggleHeight;
  /**
   * If the {@link #lineType} is {@link game.LineType#GLUON}, this is used to influence how far back the wiggle reach. Loaded from config (optionally).
   */
  public float wiggleOffset;
  /**
   * If {@link #dashed} is True, this gives the length of the dash. For small values it is similar to dotted lines. Loaded from config (optionally).
   */
  public float dashLength;
  /**
   * This makes the line dashed or dotted depending of the {@link #dashLength}. Loaded from config (optionally).
   */
  public boolean dashed;
  /**
   * This is not configurable by config, but used to highlight the currently selected line.
   */
  public float dashOffset;
  /**
   * These identifiers are used with {@link #rules} to identify valid and invalid vertices. Loaded from config (mandatory).
   */
  public Map.Entry<String, String> identifier = null;
  /**
   * Whether or not the arrow is shown
   */
  public boolean showArrow;
  /**
   * If the {@link #lineType} if {@link game.LineType#FERMION}, this gives the size of the arrow. Loaded from config (optionally).
   */
  public int arrowSize;
  /**
   * the dent in the arrow
   */
  public int arrowDent = 0;
  /**
   * the height of the arrow
   */
  public int arrowHeight = -1;
  /**
   * If the {@link #lineType} if {@link game.LineType#FERMION}, this gives the place of the arrow. Loaded from config (optionally).
   */
  public double arrowPlace;
  /**
   * This gives the color of the line (and potentially of the arrow). Loaded from config (optionally).
   */
  public Color color;
  /**
   * if the line is drawn as a double line
   */
  public boolean doubleLine = false;
  /**
   * a description/name/label of the lineConfig shown in a tooltip when hovering over a tile containing this lineConfig
   */
  public String description;
  /**
   * description/name/label for incoming particle or outgoing antiparticle in gameMode
   */
  public String posDescription = "";
  /**
   * description/name/label for outgoing particle or incoming antiparticle in gameMode
   */
  public String negDescription = "";
  /**
   * Serial Version UID
   */
  // static final long serialVersionUID = 23654356L;
  static final long serialVersionUID = 42L;
  /**
   * wether or not this is part if a multiedit object
   */
  public boolean multiEdit = false;
  /**
   * the part of the gluon/photon line that is to be drawn without wiggles at the start and end
   */
  public double straight_len = 10;
  /**
   * the part of the gluon/photon line that is to be drawn without wiggles around an arrow
   */
  public double straight_len_arrow = 10;
  /**
   * If the {@link #lineType} is {@link game.LineType#GLUON}, this is used to influence how close to the middle relative to the wiggleheight the lower wiggle comes. Loaded from config (optionally).
   */
  public double wiggleAsymmetry;
  /**
   * basic constructor
   */
  public LineConfig() {
  }
  /**
   * constructor with all parameters given
   * @param arrowSize the size of the arrow
   * @param lineType the lineType
   * @param stroke the thickness of the line
   * @param wiggleLength the length of the waves/spirals for wave and spiral
   * line types
   * @param wiggleHeight the amplitude of the waves/spirals if applicable
   * for the lineType
   * @param wiggleOffset for spiral line type this is how much the area is
   * enclosed by the line at each repetition and for waves this is a measure
   * for the angle at which the line intersects the straight line/circle
   * section which connect start and end point
   * @param color the color of the line
   * @param dashed whether or not the line is drawn dashed
   * @param dashOffset how far the first dash is from the start of the line
   */
  public LineConfig(double arrowSize, LineType lineType, double stroke, double wiggleLength, double wiggleHeight, double wiggleOffset, double wiggleAsymmetry, Color color, boolean dashed, double dashOffset, double arrowDent, double arrowHeight) {
    this.arrowSize = (int) arrowSize;
    this.arrowDent = (int) arrowDent;
    this.arrowHeight = (int) arrowHeight;
    if (this.arrowHeight<0) this.arrowHeight = this.arrowSize;
    this.lineType = lineType;
    this.stroke = (float) stroke;
    this.wiggleLength = (float) wiggleLength;
    this.wiggleHeight = (float) wiggleHeight;
    this.wiggleOffset = (float) wiggleOffset;
    this.wiggleAsymmetry = wiggleAsymmetry;
    this.color = color;
    this.dashed = dashed;
    this.dashOffset = (float) dashOffset;
    this.dashLength = 10;
    this.doubleLine = false;
    this.arrowPlace = .5;
    this.showArrow = false;
    this.straight_len = 0.;
    this.straight_len_arrow = 0.;
  }
  /**
   * constructs a lineType from an old lineConfig and a color
   * @param lt the lineType
   * @param c the color
   */
  public LineConfig(LineType lt, Color c) {
    this(15, lt, 2, 50, 20, 15, 0, c, false, 0, 0, 15);
    if (this.arrowHeight<0) this.arrowHeight = this.arrowSize;
    this.doubleLine = false;
    if (lt.equals(LineType.FERMION) || lt.equals(LineType.SWAVE))
      this.arrowPlace = .5;
    if (lt.equals(LineType.GLUON) || lt.equals(LineType.SSPIRAL))
      this.wiggleLength = 27;
    this.wiggleHeight = 20;
    this.wiggleOffset = 15;
    this.wiggleAsymmetry = 0;
    this.showArrow = false;
    this.straight_len = 0.;
    this.straight_len_arrow = 0.;
  }
  /**
   * Copy constructor: needs to be modified if any additional class variables are added.
   *
   * @param lc {@link game.LineConfig} to copy
   */
  public LineConfig(LineConfig lc) {
    this(lc.arrowSize, lc.lineType, lc.stroke, lc.wiggleLength, lc.wiggleHeight, lc.wiggleOffset, lc.wiggleAsymmetry, lc.color, lc.dashed, lc.dashOffset, lc.arrowDent, lc.arrowHeight);
    this.identifier = lc.identifier;
    if (this.arrowHeight<0) this.arrowHeight = this.arrowSize;
    this.dashLength = lc.dashLength;
    this.doubleLine = lc.doubleLine;
    this.description = lc.description;
    this.arrowPlace = lc.arrowPlace;
    this.showArrow = lc.showArrow;
    this.arrowSize = lc.arrowSize;
    this.arrowPlace = lc.arrowPlace;
    this.arrowDent = lc.arrowDent;
    this.straight_len = lc.straight_len;
    this.straight_len_arrow = lc.straight_len_arrow;
  }

  /**
   * turns this to a copy of the argument
   * @param lc the lineConfig to copy
   */
  public void turnTo(LineConfig lc) {
    this.arrowSize = lc.arrowSize;
    this.arrowDent = lc.arrowDent;
    this.arrowHeight = lc.arrowHeight;
    if (this.arrowHeight<0) this.arrowHeight = this.arrowSize;
    this.lineType = lc.lineType;
    this.stroke = lc.stroke;
    this.wiggleLength = lc.wiggleLength;
    this.wiggleHeight = lc.wiggleHeight;
    this.wiggleOffset = lc.wiggleOffset;
    this.wiggleAsymmetry = lc.wiggleAsymmetry;
    this.color = lc.color;
    this.dashed = lc.dashed;
    this.dashOffset = lc.dashOffset;
    this.identifier = lc.identifier;
    this.dashLength = lc.dashLength;
    this.doubleLine = lc.doubleLine;
    this.description = lc.description;
    this.arrowPlace = lc.arrowPlace;
    this.showArrow = lc.showArrow;
    this.arrowSize = lc.arrowSize;
    this.arrowPlace = lc.arrowPlace;
    this.straight_len = lc.straight_len;
    this.straight_len_arrow = lc.straight_len_arrow;
  }

  /**
   * Adds some default lines if config could not be loaded or no config was given.
   */
  public static void initPresets() {
    rules.clear();
    feynmanRuleTemplates.clear();
    presets.clear();
    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(LineConfig.class.getClassLoader().getResourceAsStream("resources/defaults/default.model")));
      initPresets(br);
    } catch (Exception e) {
      e.printStackTrace();
      JOptionPane.showMessageDialog(null,
              "The fallback config file could not be properly processed!\n\nExited with:\n" + e.getMessage(),
              "Model File Error",
              JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Loads {@link #rules} and {@link #presets} from the specified config file.
   *
   * @param path name or path to config file
   */
  public static void initPresets(String path) {
    File file = new File(path);

    if (!file.isFile() || !file.exists()) {
      if (path != null && !path.equals(""))
        JOptionPane.showMessageDialog(new JFrame(), "The given model file does not exist:\n" + path, "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (!file.canRead()) {
      if (path != null && !path.equals(""))
        JOptionPane.showMessageDialog(new JFrame(), "Failed to read the given model file:\n" + path, "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (!file.canRead())
      return;

    rules.clear();
    feynmanRuleTemplates.clear();
    presets.clear();
    try(BufferedReader br = new BufferedReader(new FileReader(file))) {
      initPresets(br);
    } catch (Exception e) {
      e.printStackTrace();
      LineConfig.initPresets();
      JOptionPane.showMessageDialog(null,
          "The given config file could not be properly processed! Falling back to hardcoded model.\n\nExited with:\n" + e.getMessage(),
          "Model File Error",
          JOptionPane.ERROR_MESSAGE);
    }
  }


  /**
   * Loads {@link #rules} and {@link #presets}
   *
   * @param br BufferedReader containing the config file
   */
  public static void initPresets(BufferedReader br) throws Exception {
    ArrayList<String[]> commands = new ArrayList<>();
    ArrayList<String[]> vertices = new ArrayList<>();

      /*String st;
      Pattern pt = Pattern.compile("^(?:\\s*?)\\[(.[^#\\]\\[]*+)](?:\\s*?)#*"); // Line definition
      Pattern pt2 = Pattern.compile("^(?:\\s*?)\\{(.[^#}{]*+)}(?:\\s*?)#*"); // vertex definition
      while ((st = br.readLine()) != null) {
        Matcher matcher = pt.matcher(st);
        Matcher matcher2 = pt2.matcher(st);
        if (matcher.find())
          commands.add(matcher.group(1).replaceAll("\\s+", "").split("[;,]"));
        else if (matcher2.find())
          vertices.add(matcher2.group(1).replaceAll("\\s+", "").split("[;,]"));

      }*/

    String str;
    while ((str = br.readLine()) != null) {
      if (str.replaceAll("\\s", "").startsWith("[") && str.replaceAll("\\s", "").endsWith("]")) {
        String stripped = str.replaceAll("\\s", "");
        stripped = stripped.substring(stripped.indexOf("[") + 1, stripped.lastIndexOf("]"));
        commands.add(stripped.split("(?<!\\\\)[;,]"));
      } else if (str.replaceAll("\\s", "").startsWith("{") && str.replaceAll("\\s", "").endsWith("}")) {
        String stripped = str.replaceAll("\\s", "");
        stripped = stripped.substring(stripped.indexOf("{") + 1, stripped.lastIndexOf("}"));
        vertices.add(stripped.split("(?<!\\\\)[;,]"));
      }
    }

    if (commands.size() == 0) {
      throw new Exception("No line definitions found.");
    }

    /* check for invalid commands/vertices */
    for (int i = 0; i < commands.size(); i++) {
      String a = commands.get(i)[0];
      String b = commands.get(i)[1];
      // vertices.add(new String[] {a, b});
      for (int j = 0; j < commands.size(); j++) {
        if (i == j) continue;
        if (commands.get(j)[0].equals(a) || commands.get(j)[1].equals(b) || commands.get(j)[1].equals(a) || commands.get(j)[0].equals(b))
          throw new Exception("Invalid line start/end naming.");
      }
    }
    for (String[] rule : vertices) {
      if (rule.length < 2)
        throw new Exception("Invalid vertex rule found with just one identifier.");
      for (String s : rule) {
        if (!s.toLowerCase().startsWith("type") && !s.toLowerCase().startsWith("vertex") && !s.toLowerCase().startsWith("factor")) {
          boolean found = false;
          for (String[] command : commands) {
            if (command[0].split("\\|")[0].equals(s.split("\\|")[0]) || command[1].split("\\|")[0].equals(s.split("\\|")[0])) {
              found = true;
              break;
            }
          }
          if (!found)
            throw new Exception("Invalid vertex rule found.");
        }
      }
    }

    /* reformat rules */
    for (String[] arr : vertices) {
      HashMap<String, Integer> temp = new HashMap<>();
      int i = 0;
      String ident = "";
      String vertexTemplate = "";
      String vertexFactor = "";
      for (String ele : arr) {
        if (ele.toLowerCase().startsWith("vertex")) {
          vertexTemplate = ele.split("=+")[1].replaceAll("\\s", "");
        } else if (ele.toLowerCase().startsWith("factor")) {
          vertexFactor = ele.split("=+")[1].replaceAll("\\s", "");
        } else if (ele.toLowerCase().startsWith("type")) {
          ident = ele.split("[=(\\-)]+")[1].replaceAll("\\s", "");
        } else {
          String[] map = ele.split("\\|");
          ele = map[0];
          if (map.length > 1) {
            ident = map[1];
          }
          temp.put(ele, (temp.get(ele) == null ? 0 : temp.get(ele)) + 1);
          i++;
        }
      }
      rules.add(new AbstractMap.SimpleEntry<>(temp, ident));
      feynmanRuleTemplates.addVertexTemplate(temp, vertexTemplate);
      feynmanRuleTemplates.addVertexFactor(temp, vertexFactor);
    }

    LOGGER.info("Found " + commands.size() + " line configurations");
    LOGGER.info("Found " + (vertices.size()) + " vertex rules");


    for (String[] a : commands)
      presets.add(toConf(a));


    presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
  }


  /**
   * Helper function for {@link #initPresets(String)}, parsing one config line entry to actual {@link game.LineConfig}.
   *
   * @param s one entry from config, specifying a {@link game.LineConfig}
   * @return parsed {@link game.LineConfig}
   * @throws Exception if config entry is not valid
   */
  public static LineConfig toConf(String[] s) throws Exception {
    /* array shape:
      0,1: from,to          separated by comma
      2: LineType         (FERMION,PHOTON)
      3: Color
      4: Stroke
      5: dash
      6: wiggleSize
      7: wiggleHeight
      8: wiggleOffset
      9: wiggleAsymmetry
      10: dashLength*/

    if (s.length < 3)
      throw new Exception("not enough arguments: " + String.join(",", s));

    if (s[2].toUpperCase().equals("PLAIN"))
      s[2] = "FERMION";

    if (s[2].toUpperCase().equals("SPIRAL"))
      s[2] = "GLUON";

    if (s[2].toUpperCase().equals("WAVE"))
      s[2] = "PHOTON";

    LineType lt = LineType.valueOf(s[2].toUpperCase());
    if (s[2].toUpperCase().equals("SCALAR")) {
      lt = LineType.FERMION;
    }
    LineConfig lf = new LineConfig(lt, Color.BLACK);
    lf.identifier = new AbstractMap.SimpleEntry<>(s[0], s[1]);

    feynmanRuleTemplates.setAnticommuting(lf.identifier, false);
    if (!s[0].equals(s[1])) {
      lf.showArrow = true;
      feynmanRuleTemplates.setAnticommuting(lf.identifier, true);
    }

    boolean aH = false;

    /* parsing additional args*/
    for (int i = 3; i < Math.min(s.length, mapKeys.size() + 3); i++) {

      int paramNum = i;
      String paramVal = s[i];

      if (s[i].contains("=") || s[i].contains("-")) {
        String[] map = s[i].split("=+");
        map[0] = map[0].replaceAll("_", "");
        paramNum = mapKeys.indexOf(map[0].toLowerCase()) + 3;
        paramVal = map[1];
      }

      if (paramNum < 3 || paramNum >= mapKeys.size() + 3)
        throw new Exception("Unrecognized additional parameter: " + s[i] + "\nAvailable params are " + mapKeys);

      switch (paramNum) {
        case 3: {
          Color c = null;
          try {
            c = (Color) Color.class.getField(paramVal.toUpperCase()).get(null);
          } catch (Exception ignore) {
          }

          if (c == null && paramVal.length() == 8) {
            try {
              int b = Integer.parseInt(paramVal.substring(6, 8), 16);
              int g = Integer.parseInt(paramVal.substring(4, 6), 16);
              int r = Integer.parseInt(paramVal.substring(2, 4), 16);
              int a = Integer.parseInt(paramVal.substring(0, 2), 16);
              c = new Color(r, g, b, a);
            } catch (Exception ignore) {
            }
          }

          if (c == null && paramVal.length() == 6) {
            try {
              int r = Integer.parseInt(paramVal.substring(0, 2), 16);
              int g = Integer.parseInt(paramVal.substring(2, 4), 16);
              int b = Integer.parseInt(paramVal.substring(4, 6), 16);
              c = new Color(r, g, b);
            } catch (Exception ignore) {
            }
          }
          if (c != null)
            lf.color = c;
        }
        break;
        case 4:
          lf.stroke = (float) Double.parseDouble(paramVal);
          break;
        case 5:
          lf.dashed = paramVal.toLowerCase().equals("true");
          if (lf.dashLength <= 0)
            lf.dashLength = 10;
          break;
        case 6:
          lf.wiggleLength = (float) Double.parseDouble(paramVal);
          break;
        case 7:
          lf.wiggleHeight = (float) Double.parseDouble(paramVal);
          break;
        case 8:
          lf.wiggleOffset = (float) Double.parseDouble(paramVal);
          break;
        case 9:
          lf.dashLength = (float) Double.parseDouble(paramVal);
          lf.dashed = !(lf.dashLength <= 0);
          if (!lf.dashed)
            lf.dashLength = 1;
          break;
        case 10:
          lf.showArrow = true;
          lf.arrowSize = (int) Double.parseDouble(paramVal);
          break;
        case 11:
          lf.showArrow = true;
          lf.arrowPlace = Double.parseDouble(paramVal);
          break;
        case 12:
          lf.doubleLine = paramVal.toLowerCase().equals("true");
          break;
        case 13:
          String[] split = paramVal.split("\\|");
          if (split.length == 1) {
            lf.description = split[0].replaceAll("hashpound", "#");
            lf.posDescription = lf.description;
            lf.negDescription = lf.description;
          } else if (split.length == 2) {
            lf.description = split[0].replaceAll("hashpound", "#");
            lf.posDescription = split[1].replaceAll("hashpound", "#");
            lf.negDescription = lf.posDescription;
          } else {
            lf.description = split[0].replaceAll("hashpound", "#");
            lf.posDescription = split[1].replaceAll("hashpound", "#");
            lf.negDescription = split[2].replaceAll("hashpound", "#");
          }
          break;
        case 14:
          lf.wiggleOffset = (float) Double.parseDouble(paramVal);
          break;
        case 15:
          lf.showArrow = paramVal.toLowerCase().equals("true");
          break;
        case 16:
          Matcher mtList = ptIndices.matcher(paramVal);
          if (mtList.matches()) {
            java.util.List<Integer> indexRepresentations = new ArrayList<>();
            for (String index : mtList.group().replaceAll("(\\s+|<|>)", "").split("\\|")) {
              try {
                indexRepresentations.add(Integer.valueOf(index));
              } catch (NumberFormatException e) {
                System.err.println("this should not happen");
                indexRepresentations.add(0);
              }
            }
            feynmanRuleTemplates.addIndexRepresentation(lf.identifier, indexRepresentations);
          } else {
            throw new Exception("invalid index definition: " + paramVal);
          }
          break;
        case 17:
          String propagatorTemplate = paramVal;
          feynmanRuleTemplates.addPropagatorTemplate(lf.identifier, propagatorTemplate);
          break;
        case 18:
          String[] externalParticleTemplate = feynmanRuleTemplates.getExternalParticleTemplate(lf.identifier);
          String[] externalTemplate = paramVal.split("\\|");
          if (externalTemplate.length == 1) {
            externalParticleTemplate[0] = externalTemplate[0];
            externalParticleTemplate[1] = externalTemplate[0];
          } else if (externalTemplate.length == 2) {
            externalParticleTemplate[0] = externalTemplate[0];
            externalParticleTemplate[1] = externalTemplate[1];
          } else {
            throw new Exception("invalid external definition: " + paramVal);
          }
          feynmanRuleTemplates.addExternalParticleTemplate(lf.identifier, externalParticleTemplate);
          break;
        case 19:
          String[] externalAntiParticleTemplate = feynmanRuleTemplates.getExternalParticleTemplate(lf.identifier);
          String[] externalAntiTemplate = paramVal.split("\\|");
          if (externalAntiTemplate.length == 1) {
            externalAntiParticleTemplate[2] = externalAntiTemplate[0];
            externalAntiParticleTemplate[3] = externalAntiTemplate[0];
          } else if (externalAntiTemplate.length == 2) {
            externalAntiParticleTemplate[2] = externalAntiTemplate[0];
            externalAntiParticleTemplate[3] = externalAntiTemplate[1];
          } else {
            throw new Exception("invalid external* definition: " + paramVal);
          }
          feynmanRuleTemplates.addExternalParticleTemplate(lf.identifier, externalAntiParticleTemplate);
          break;
        case 20:
          boolean isAnticommuting = paramVal.toLowerCase().equals("true");
          feynmanRuleTemplates.setAnticommuting(lf.identifier, isAnticommuting);
          break;
        case 21:
          lf.arrowDent = (int) Double.parseDouble(paramVal);
          break;
        case 22:
          aH = true;
          lf.arrowHeight = (int) Double.parseDouble(paramVal);
          break;
        case 23:
          lf.wiggleAsymmetry = Double.parseDouble(paramVal);
          break;
        case 24:
          lf.straight_len = Double.parseDouble(paramVal);
          break;
        case 25:
          lf.straight_len_arrow = Double.parseDouble(paramVal);
          break;
      }

      if (!aH) lf.arrowHeight = lf.arrowSize;

    }
    return lf;
  }

  /**
   * deletes the line preset with index from the presets
   *
   * @param index the index of the config to be deleted
   */
  public static void delete(int index) {
    presets.remove(presets.size() - 1);
    boolean askRemove = true; // if it still needs to be asked which parts to remove if propagator is connected to Feynman rule
    boolean removeRules = false; // whether or not to remove also the rules
    if (presets.get(index).identifier != null) {
      for (int index_rule = 0; index_rule < rules.size(); index_rule ++) {
        boolean remove = false; // whether or not to remove the rule
        if (rules.get(index_rule).getKey().containsKey(presets.get(index).identifier.getKey())) {
          remove = true;
        } else if (rules.get(index_rule).getKey().containsKey(presets.get(index).identifier.getKey())) {
          remove = true;
        } // if remove then rule contains the propagator
        if (remove && askRemove) { // rule can be deleted and choice is not yet made whether to keep propagator/rules or not
          Object[] options = {"Remove propagator and Feynman rules", "Remove only propagator", "Keep all"};
          Object selectedValue = JOptionPane.showOptionDialog(null,
             "There are Feynman rules in the model that describe the interaction of this\npropagators particle with other particles. You can remove these rules alongside\nwith the propagator or only the propagator and keep the rules. It is also\npossible to abort the deletion of the propagator.", "Remove propagator/rules?",
             JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
             options, options[0]);
          if (selectedValue.equals(2)) { // rules and propagator rules get kept
            presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
            return;
          } else if (selectedValue.equals(0)) { // rules and propagator get deleted
            askRemove = false;
            removeRules = true;
          } else if (selectedValue.equals(1)) { // only propagator gets removed
            askRemove = false;
            break;
          }
        }
        if (removeRules && remove) { // remove the rule
          if (rules.get(index_rule).getValue() != "" && rules.get(index_rule).getValue() != null) {
            Vertex.removeVName(rules.get(index_rule).getValue());
          }
          rules.remove(index_rule);
          index_rule --;
        }
      }
    }
    presets.remove(index); // remove the propagator
    presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
  }

  /**
   * Returns the {@link game.LineConfig} information as a config entry. If the identifier of this config is {@code null},
   * the identifier will always be generated.
   * @param generateIdentifier If {@code true}, will generate with a random {@link #identifier} (such that there is no overlap in the config file).
   * Otherwise it uses the exisiting identifier
   * @return {@link String} in config layout
   */
  public String toFile(boolean generateIdentifier) {
    String identStart, identEnd;
    if(generateIdentifier || identifier == null) {
      Random r = new Random();
      String a = "";
      a += (char) (r.nextInt(26) + 'a');
      a += (char) (r.nextInt(26) + 'a');
      a += (char) (r.nextInt(26) + 'a');
//      String a = r.ints(3, 'a', 'a' + 26).mapToObj(Character::toString).collect(Collectors.joining());
      identStart = a;
      identEnd = a.toUpperCase();
    } else {
      identStart = identifier.getKey();
      identEnd = identifier.getValue();
    }
    return "[" + identStart + ", " + identEnd + toFileWOBracket() + "]";
  }

  /**
   * Returns the {@link game.LineConfig} information as a config entry without the brackets and identifiers.
   *
   * @return {@link String} in config layout
   */
  private String toFileWOBracket() {
    String returnVal = ", " + lineType.getName() + ", color=" + Integer.toHexString(color.getRGB()) + ", stroke=" + stroke + ", dash=" + dashed;
    if (dashed) {
      returnVal += ", dashLength=" + dashLength;
    }
    returnVal += ", double=" + doubleLine;
    if (showArrow) {
      returnVal += ", showArrow = true" + ", arrowSize=" + arrowSize + ", arrowPlace=" + arrowPlace + ", arrowDent= " + arrowDent + ", arrowHeight= " + arrowHeight;
    } else {
      returnVal += ", showArrow = false";
    }
    if (lineType == LineType.GLUON || lineType == LineType.SSPIRAL) {
      returnVal += ", wiggleSize=" + Float.toString(wiggleLength) + ", wiggleHeight=" + Float.toString(wiggleHeight) + ", wiggleOffset=" + Float.toString(wiggleOffset) + ", wiggleAsymmetry=" + Double.toString(wiggleAsymmetry) + ", straightLength=" + Double.toString(straight_len);
      if (showArrow) {
        returnVal += ", straightLengthArrow=" + Double.toString(straight_len_arrow);
      }
    } else if (lineType == LineType.PHOTON || lineType == LineType.SWAVE) {
      returnVal += ", wiggleSize=" + Float.toString(wiggleLength) + ", wiggleHeight=" + Float.toString(wiggleHeight) + ", wiggleSharpness=" + Float.toString(wiggleOffset) + ", straightLength=" + Double.toString(straight_len);
      if (showArrow) {
        returnVal += ", straightLengthArrow=" + Double.toString(straight_len_arrow);
      }
    }
    if (this.description != null && !this.description.equals("")) {
      returnVal += ", label = " + this.description.replaceAll("#", "hashpound");
    }
    if (this.identifier != null) {
      java.util.List<Integer> indexRepresentations = LineConfig.feynmanRuleTemplates.getIndexRepresentation(this.identifier);
      if (!indexRepresentations.isEmpty()) {
	String indices = "<" + indexRepresentations.stream().map(Object::toString).collect(Collectors.joining("|")) + ">";
	returnVal += ", indices = " + indices;
      }
      String propagatorTemplate = LineConfig.feynmanRuleTemplates.getPropagatorTemplate(this.identifier);
      if (propagatorTemplate != null && !propagatorTemplate.chars().allMatch(Character::isWhitespace)) {
	returnVal += ", propagator = " + propagatorTemplate;
      }
      String[] externalParticleTemplate = LineConfig.feynmanRuleTemplates.getExternalParticleTemplate(this.identifier);
      if (externalParticleTemplate.length == 4) {
        if (externalParticleTemplate[0] != null && !externalParticleTemplate[0].chars().allMatch(Character::isWhitespace)) {
          if (externalParticleTemplate[0].equals(externalParticleTemplate[1])) {
	    returnVal += ", external = " + externalParticleTemplate[0];
	  } else if (externalParticleTemplate[1] != null && !externalParticleTemplate[1].chars().allMatch(Character::isWhitespace)) {
	    returnVal += ", external = " + externalParticleTemplate[0] + " | " + externalParticleTemplate[1];
	  }
	}
	if (externalParticleTemplate[2] != null && !externalParticleTemplate[2].chars().allMatch(Character::isWhitespace)) {
          if (externalParticleTemplate[2].equals(externalParticleTemplate[3])) {
            returnVal += ", external* = " + externalParticleTemplate[2];
          } else if (externalParticleTemplate[3] != null && !externalParticleTemplate[3].chars().allMatch(Character::isWhitespace)) {
            returnVal += ", external* = " + externalParticleTemplate[2] + " | " + externalParticleTemplate[3];
          }
        }
      }
      if (LineConfig.feynmanRuleTemplates.isAnticommuting(this.identifier)) {
        returnVal += ", anticommuting = true";
      }
    }
    return returnVal;

  }

  /**
   * not used so far! {@link String} is unique.
   *
   * @return {@link String} characterizing the {@link game.LineConfig}.
   */
  public String toString() {
    return lineType + "," + stroke + "," + wiggleLength + "," + wiggleHeight + "," + wiggleOffset + "," + dashLength + "," + dashed + "," + dashOffset + "," + identifier.getKey() + "," + identifier.getValue() + "," + arrowSize + "," + color.getRGB() + "," + wiggleAsymmetry + "," + straight_len + "," + straight_len_arrow;
  }

  /**
   * not used so far! Counterpart of {@link #toString()}.
   *
   * @param data {@link String} to parse
   */
  @SuppressWarnings("unused")
  public void fromString(String data) {
    String[] param = data.split(",");
    lineType = LineType.valueOf(param[0]);
    stroke = (float) Double.parseDouble(param[1]);
    wiggleLength = (float) Double.parseDouble(param[2]);
    wiggleHeight = (float) Double.parseDouble(param[3]);
    wiggleOffset = (float) Double.parseDouble(param[4]);
    dashLength = (float) Double.parseDouble(param[5]);
    dashed = Boolean.parseBoolean(param[6]);
    dashOffset = (float) Double.parseDouble(param[7]);
    identifier = new AbstractMap.SimpleEntry<>(param[8], param[9]);
    arrowSize = (int) Double.parseDouble(param[10]);
    color = new Color(Integer.parseInt(param[11]));
    wiggleAsymmetry = Double.parseDouble(param[12]);
    straight_len = Double.parseDouble(param[13]);
    straight_len_arrow = Double.parseDouble(param[14]);
  }

  /**
   * Compares two {@link game.LineConfig} and returns True if both have the same config values (including same {@link #identifier}). {@link #dashOffset} does not need to be the same.
   *
   * @param obj must be an {@link game.LineConfig} in order ot be True.
   * @return True or False depending on some class variables.
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    LineConfig other = (LineConfig) obj;
    if (stroke != other.stroke)
      return false;
    if (!color.equals(other.color))
      return false;
    if (wiggleLength != other.wiggleLength)
      return false;
    if (wiggleHeight != other.wiggleHeight)
      return false;
    if (wiggleOffset != other.wiggleOffset)
      return false;
    if (wiggleAsymmetry != other.wiggleAsymmetry)
      return false;
    if (dashLength != other.dashLength)
      return false;
    if (dashed != other.dashed)
      return false;
    if (doubleLine != other.doubleLine)
      return false;
    if (showArrow != other.showArrow)
      return false;
    if (showArrow) {
      if (arrowSize != other.arrowSize)
        return false;
      if (arrowPlace != other.arrowPlace)
        return false;
      if (arrowDent != other.arrowDent)
        return false;
      if (arrowHeight != other.arrowHeight)
        return false;
    }
    if (straight_len != other.straight_len)
      return false;
    if (straight_len_arrow != other.straight_len_arrow)
      return false;
    return lineType.equals(other.lineType);
  }
  
  public int hashCode() {
    if(showArrow) {
      return Objects.hash(stroke, color, wiggleLength, wiggleHeight, wiggleOffset, dashLength, dashed, 
          doubleLine, showArrow, arrowSize, arrowPlace, arrowHeight, arrowDent, lineType);
    } else {
      return Objects.hash(stroke, color, wiggleLength, wiggleHeight, wiggleOffset, dashLength, dashed,
          doubleLine, showArrow, lineType);
    }
  }

  /**
   * Gives an identifier for the line Type that is displayed in the {@link ui.EditFrame}
   *
   * @return the identifier
   */
  public String lineType() {
    if (this.lineType.equals(LineType.PHOTON)) {
      return "Photon";
    } else if (this.lineType.equals(LineType.GLUON)) {
      return "Gluon";
    } else if (this.lineType.equals(LineType.FERMION)) {
      return "Fermion";
    } else if (this.lineType.equals(LineType.SWAVE)) {
      return "Photino";
    } else if (this.lineType.equals(LineType.SSPIRAL)) {
      return "Gluino";
    }
    return "";
  }

  /**
   * changes the {@link game.LineType} to the type identified by the param
   *
   * @param newType the identifier for the new type
   */
  public void changeType(String newType) {
    switch (newType.toLowerCase()) {
      case "photon":
        this.lineType = LineType.PHOTON;
        break;
      case "gluon":
        this.lineType = LineType.GLUON;
        break;
      case "fermion":
        this.lineType = LineType.FERMION;
        break;
      case "photino":
        this.lineType = LineType.SWAVE;
        break;
      case "gluino":
        this.lineType = LineType.SSPIRAL;
        break;
    }
  }
  /**
   * returns the width of the line
   * @return the width
   */
  public double getWidth() {
    if (this.lineType.equals(LineType.FERMION)) {
      return this.stroke;
    } else {
      return this.stroke/2. + this.wiggleHeight;
    }
  }
}
