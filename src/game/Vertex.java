//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import ui.Frame;
import ui.Pane;

/**
 * Contains all information about a specific Vertex on the screen.
 *
 * @author Sven Yannick Klein
 */
@SuppressWarnings("overrides")
public class Vertex implements Serializable {
  private static final Logger LOGGER = Frame.getLogger(Vertex.class);
  
  public static int latexSize = 20;
  /**
   * All possible shapes of a vertex
   */
  public static final java.util.List<String> shapes = Arrays.asList("Circle", "Square", "Triangle");
  /**
   * All possible keys that can be used in the config file.
   */
  private static final java.util.List<String> mapKeys = Arrays.asList("size", "bordercolor", "borderdashed", "borderdashlength", "borderstroke", "imagepath", "shape", "fillingcolor", "rotation", "fxpath", "label", "vname", "fillscale"); // lower case!!!!
  /**
   * The presets for the vertices
   */
  public static ArrayList<Map.Entry<Vertex, String>> vertexPresets = new ArrayList<>();
  /**
   * if the description is shown per default
   */
  public static Boolean showDescDef = false;
  /**
   * default value for descPosX
   */
  public static int descPosXDef = 0;
  /**
   * default for partPlaceDesc
   */
  public static double descPosYDef = 20;
  /**
   * default for rotDesc;
   */
  public static double rotDescDef = 0;
  /**
   * default for descScale
   */
  public static double descScaleDef = 1;
  /**
   * The middle of the vertex / the point where the {@link FeynLine}s meet
   */
  public Point2D origin;
  /**
   * The size of the Vertex
   */
  public int size;
  /**
   * Describes how the border is drawn
   */
  public Border border;
  /**
   * describes how the vertex is filled
   */
  public Filling filling;
  /**
   * describes the shape of the vertex
   */
  public String shape;
  /**
   * shows if one hovers over the vertex
   */
  public boolean hover;
  /**
   * determines the rotation of the image
   */
  public float rotation;
  /**
   * a description/name of the vertex shown in a tooltip when hovering over a tile containing this vertex
   */
  public String description;
  /**
   * if label/description is shown
   */
  public boolean showDesc;
  /**
   * the amount of pixels the description is displayed left/right of the vertex
   */
  public int descPosX;
  /**
   * the amount of pixels the description is displayed above/below of the vertex
   */
  public double descPosY = 20;
  /**
   * the rotation of the description
   */
  public double rotDesc;
  /**
   * describes the scaling of the description
   */
  public double descScale;
  /**
   * wether or not the vertex can be removed by automatic vertex detection
   */
  public boolean autoremove = true;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 50L;
  /**
   * wether or not this is part if a multiedit object
   */
  public boolean multiEdit = false;
  /**
   * constructor for an all black, circular vertex at a given position
   * @param origin the center of the vertex
   */
  public Vertex(Point2D origin) {
    this(new Vertex());
    // if (vertexPresets.size() > 0) {
    //  this.turnTo(vertexPresets.get(0).getKey());
    // }
    this.origin = origin;
    this.showDesc = Vertex.showDescDef;
    this.descPosX = Vertex.descPosXDef;
    this.rotDesc = Vertex.rotDescDef;
    this.descScale = Vertex.descScaleDef;
    this.descPosY = Vertex.descPosYDef;
  }
  /**
   * constructor for a vertex from an identifier for the presets and a given
   * position
   * @param origin the center
   * @param identifier the identifier of the vertex preset. if no preset with
   * this identifier exists, the standard is used
   */
  public Vertex(Point2D origin, String identifier) {
    this(new Vertex());
    // if (vertexPresets.size() > 0) {
    //  this.turnTo(vertexPresets.get(0).getKey());
    // }
    for (Map.Entry<Vertex, String> v : vertexPresets) {
      if (identifier.equals(v.getValue())) {
        this.turnTo(v.getKey());
        break;
      }
    }
    this.origin = origin;
    this.showDesc = Vertex.showDescDef;
    this.descPosX = Vertex.descPosXDef;
    this.rotDesc = Vertex.rotDescDef;
    this.descScale = Vertex.descScaleDef;
    this.descPosY = Vertex.descPosYDef;
  }
  /**
   * basic constructor: does not set the center
   */
  public Vertex() {
    this.filling = new Filling();
    this.border = new Border();
    this.size = 5;
    this.shape = "circle";
    this.hover = false;
    this.rotation = 0;

    this.showDesc = Vertex.showDescDef;
    this.descPosX = Vertex.descPosXDef;
    this.rotDesc = Vertex.rotDescDef;
    this.descScale = Vertex.descScaleDef;
    this.descPosY = Vertex.descPosYDef;
  }
  /**
   * copy constructor
   * @param v the vertex to be copied
   */
  public Vertex(Vertex v) {
    try {
      this.origin = new Point2D(v.origin);
    } catch (NullPointerException ex) {
      this.origin = v.origin;
    }
    this.size = v.size;
    this.border = new Border(v.border);
    this.filling = new Filling(v.filling);
    this.shape = v.shape;
    this.hover = false;
    this.rotation = v.rotation;
    this.description = v.description;
    this.showDesc = v.showDesc;
    this.descPosX = v.descPosX;
    this.rotDesc = v.rotDesc;
    this.descScale = v.descScale;
    this.descPosY = v.descPosY;
  }
  /**
   * constructs a copy of a given vertex at a given point
   * @param v the vertex to be copied
   * @param origin the new venter of the constructed vertex
   */
  public Vertex(Vertex v, Point2D origin) {
    this.origin = origin;
    this.size = v.size;
    this.border = new Border(v.border);
    this.filling = new Filling(v.filling);
    this.shape = v.shape;
    this.hover = false;
    this.rotation = v.rotation;
    this.description = v.description;
    this.showDesc = v.showDesc;
    this.descPosX = v.descPosX;
    this.rotDesc = v.rotDesc;
    this.descScale = v.descScale;
    this.descPosY = v.descPosY;
  }
  /**
   * constructor based on various parameters, filling is an image
   * @param size the size of the vertex
   * @param borderColor the color of the border
   * @param borderDashed wether or not the border is drawn dashed
   * @param borderDashLength the length of the dashes when borderDashed is
   * true
   * @param borderStroke the thickness of the border
   * @param imagePath the path to the image file that the filling of the
   * vertex is going to be
   * @param shape the shape of the vertex
   */
  public Vertex(int size, Color borderColor, boolean borderDashed, float borderDashLength, float borderStroke, String imagePath, String shape) {
    Filling filling = new Filling(imagePath);
    Border border = new Border(borderColor, borderDashed, borderDashLength, borderStroke);
    this.filling = filling;
    this.border = border;
    this.shape = shape;
    this.hover = false;
    this.size = size;
    this.showDesc = Vertex.showDescDef;
    this.descPosX = Vertex.descPosXDef;
    this.rotDesc = Vertex.rotDescDef;
    this.descScale = Vertex.descScaleDef;
    this.descPosY = Vertex.descPosYDef;
  }
  /**
   * constructor based on various parameters, filling is a solid color
   * @param size the size of the vertex
   * @param borderColor the color of the border
   * @param borderDashed wether or not the border is drawn dashed
   * @param borderDashLength the length of the dashes when borderDashed is
   * true
   * @param borderStroke the thickness of the border
   * @param fillingColor the color of the filling
   * @param shape the shape of the vertex
   */
  public Vertex(int size, Color borderColor, boolean borderDashed, float borderDashLength, float borderStroke, Color fillingColor, String shape) {
    Filling filling = new Filling(fillingColor);
    Border border = new Border(borderColor, borderDashed, borderDashLength, borderStroke);
    this.filling = filling;
    this.border = border;
    this.shape = shape;
    this.hover = false;
    this.size = size;

    this.showDesc = Vertex.showDescDef;
    this.descPosX = Vertex.descPosXDef;
    this.rotDesc = Vertex.rotDescDef;
    this.descScale = Vertex.descScaleDef;
    this.descPosY = Vertex.descPosYDef;
  }

  public static void removeVName(String vname) {
    for (Map.Entry<Vertex, String> me : vertexPresets) {
      if (me.getValue().equals(vname)) {
        me.setValue("default");
      }
    }
  }
  /**
   * Loads {@link #vertexPresets} from the specified config file.
   *
   * @param path name or path to config file
   */
  public static void initPresets(String path) {
    javafx.JavaFXToJavaVectorGraphic.initNames();
    File file = new File(path);
    if (!file.canRead())
      return;

    vertexPresets.clear();
    try(BufferedReader br = new BufferedReader(new FileReader(file))) {
      ArrayList<String[]> vertices = new ArrayList<>();

      /*String st;
      Pattern pt = Pattern.compile("^(?:\\s*?)\\((.[^#\\)\\(]*+)\\)(?:\\s*?)#*"); // Vertex definition
      while ((st = br.readLine()) != null) {
        Matcher matcher = pt.matcher(st);
        if (matcher.find()) {
          vertices.add(matcher.group(1).replaceAll("\\s+", "").split("[;,]"));
        }
      }*/

      String str;
      while ((str = br.readLine()) != null) {
        if (str.replaceAll("\\s", "").startsWith("(") && str.replaceAll("\\s", "").endsWith(")")) {
          String stripped = str.replaceAll("\\s", "");
          stripped = stripped.substring(stripped.indexOf("(") + 1, stripped.lastIndexOf(")"));
          vertices.add(stripped.split("[;,]"));
        }
      }

      LOGGER.info("Found " + vertices.size() + " vertex style definitions");
      
      if (vertices.size() == 0) {
        return;
        // throw new Exception("No vertex style definitions found.");
      }

      for (String[] vertex : vertices) {
        vertexPresets.add(toConf(vertex));
      }


    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, "Exception in Vertex parsing", e);
      // System.out.println("Did not find any vertex style definitons");
      Vertex.initPresets(); // this function was empty so I deleted it
    }

  }

  public static void initPresets() {
//    if (vertexPresets.size() == 0) {
    vertexPresets.clear();
    Vertex v = new Vertex();
    String ident = "default";
    Map.Entry<Vertex, String> defaultVertex = new AbstractMap.SimpleEntry<>(v, ident);
    vertexPresets.add(defaultVertex);
//    }
    javafx.JavaFXToJavaVectorGraphic.initNames();
  }

  /**
   * converts a string to a vertex preset
   *
   * @param s the string to be converted
   * @return a Mapentry consisting of the identifier for the Vertex (if there is) and the Vertex
   * @throws Exception if config entry is not valid
   */
  public static Map.Entry<Vertex, String> toConf(String[] s) throws Exception {
    Vertex v = new Vertex();
    String ident = "default";
    for (int i = 0; i < Math.min(s.length, mapKeys.size()); i++) {

      int paramNum = i;
      String paramVal = s[i];

      if (i == s.length - 1) {
        String[] map = s[i].split("\\|");
        s[i] = map[0];
        if (map.length > 1) {
          ident = map[1];
        }
      }

      if (s[i].contains("=") || s[i].contains("-")) {
        String[] map = s[i].split("[=(\\-)]+");
        map[0] = map[0].replaceAll("_", "");
        paramNum = mapKeys.indexOf(map[0].toLowerCase());
        paramVal = map[1];
      }

      if (paramNum >= mapKeys.size()) {
        throw new Exception("Unrecognized additional parameter: " + s[i] + "\nAvailable params are " + mapKeys);
      }

      switch (paramNum) {
        case 0: {
          v.size = Integer.parseInt(paramVal);
          break;
        }
        case 1: {
          Color c = null;
          try {
            c = (Color) Color.class.getField(paramVal.toUpperCase()).get(null);
          } catch (Exception ignore) {
          }

          if (c == null && paramVal.length() == 8) {
            try {
              int b = Integer.parseInt(paramVal.substring(6, 8), 16);
              int g = Integer.parseInt(paramVal.substring(4, 6), 16);
              int r = Integer.parseInt(paramVal.substring(2, 4), 16);
              int a = Integer.parseInt(paramVal.substring(0, 2), 16);
              c = new Color(r, g, b, a);
            } catch (Exception ignore) {
            }
          }

          if (c == null && paramVal.length() == 6) {
            try {
              int r = Integer.parseInt(paramVal.substring(0, 2), 16);
              int g = Integer.parseInt(paramVal.substring(2, 4), 16);
              int b = Integer.parseInt(paramVal.substring(4, 6), 16);
              c = new Color(r, g, b);
            } catch (Exception ignore) {
            }
          }
          if (c != null)
            v.border.color = c;
          break;
        }
        case 2: {
          v.border.dashed = paramVal.toLowerCase().equals("true");
          if (v.border.dashLength <= 0)
            v.border.dashLength = 10;
          break;
        }
        case 3: {
          v.border.dashLength = Float.parseFloat(paramVal);
          if (!v.border.dashed)
            v.border.dashLength = 1;
          break;
        }
        case 4: {
          v.border.stroke = Float.parseFloat(paramVal);
          break;
        }
        case 5: {
          v.filling = new Filling(paramVal);
          break;
        }
        case 6: {
          v.shape = paramVal;
          break;
        }
        case 7: {
          Color c = null;
          try {
            c = (Color) Color.class.getField(paramVal.toUpperCase()).get(null);
          } catch (Exception ignore) {
          }

          if (c == null && paramVal.length() == 8) {
            try {
              int b = Integer.parseInt(paramVal.substring(6, 8), 16);
              int g = Integer.parseInt(paramVal.substring(4, 6), 16);
              int r = Integer.parseInt(paramVal.substring(2, 4), 16);
              int a = Integer.parseInt(paramVal.substring(0, 2), 16);
              c = new Color(r, g, b, a);
            } catch (Exception ignore) {
            }
          }

          if (c == null && paramVal.length() == 6) {
            try {
              int r = Integer.parseInt(paramVal.substring(0, 2), 16);
              int g = Integer.parseInt(paramVal.substring(2, 4), 16);
              int b = Integer.parseInt(paramVal.substring(4, 6), 16);
              c = new Color(r, g, b);
            } catch (Exception ignore) {
            }
          }
          if (c != null)
            v.filling.color = c;
          break;
        }
        case 8:
          v.rotation = Float.parseFloat(paramVal);
          break;
        case 9:
          v.filling.name = Filling.addPattern(paramVal);
          if (v.filling.color.equals(v.border.color) && v.filling.color.equals(Color.BLACK))
            v.filling.color = Color.WHITE;
          break;
        case 10:
          v.description = paramVal.replaceAll("hashpound", "#");
          break;
        case 11:
          ident = paramVal;
          break;
        case 12:
          v.filling.scale = Double.parseDouble(paramVal);
      }
    }
    return new AbstractMap.SimpleEntry<>(v, ident);
  }
  /**
   * deletes the vertex preset with index from the presets
   *
   * @param index the index of the config to be deleted
   */
  public static void delete(int index) {
    String ident = vertexPresets.get(index).getValue();
    vertexPresets.remove(index);
    if (ident != "" && ident != null && ident != "default") {
      for (Map.Entry<HashMap<String, Integer>, String> me : LineConfig.rules) {
        if (me.getValue() == ident) {
          me.setValue("");
        }
      }
    }
  }
  /**
   * takes the options for the look of a given vertex and applies them to this
   * object
   * @param v the vertex of which the options get applied to this vertex
   */
  public void turnTo(Vertex v) {
    this.filling = new Filling(v.filling);
    this.border = new Border(v.border);
    this.size = v.size;
    this.shape = v.shape;
    this.hover = v.hover;
    this.rotation = v.rotation;
    //this.description = v.description;

    //this.showDesc = v.showDesc;
    //this.descPosX = v.descPosX;
    //this.rotDesc = v.rotDesc;
    //this.descScale = v.descScale;
    //this.descPosY = v.descPosY;
  }

  /**
   * Draws the specific vertex. Additionally enables antialiasing.
   *
   * @param g               Graphics variable
   * @param currentSelected whether the vertex should be drawn highlighted
   */
  public void draw(Graphics2D g, boolean currentSelected) {
    this.draw(g, currentSelected, false);
  }

  /**
   * sets the parameters for the description of this shape to the default
   */
  public void setDefault() {
    Vertex.showDescDef = this.showDesc;
    Vertex.descPosXDef = this.descPosX;
    Vertex.rotDescDef = this.rotDesc;
    Vertex.descScaleDef = this.descScale;
    Vertex.descPosYDef = this.descPosY;
  }

  public Point2D getLabelPosition() {
    Point2D place = new Point2D(this.origin.x + this.descPosX, this.origin.y + this.descPosY);
    return place;
  }

  /**
   * Draws the specific vertex. Additionally enables antialiasing.
   *
   * @param g               Graphics variable
   * @param currentSelected whether the vertex should be drawn highlighted
   * @param clipTo10        if the drawing is only done in a circle with radius 10
   */
  public void draw(Graphics2D g, boolean currentSelected, boolean clipTo10) {

    currentSelected = (currentSelected || this.multiEdit);

    Stroke oldStroke = g.getStroke();
    Color oldColor = g.getColor();

    AffineTransform backup = g.getTransform();

    AffineTransform a = AffineTransform.getRotateInstance(this.rotation / 180 * Math.PI, origin.x, origin.y);
    g.transform(a);

    Shape sp = new Ellipse2D.Float((float) origin.x - size, (float) origin.y - size, 2 * size, 2 * size);

    if (this.shape.toLowerCase().equals("square")) {
      sp = new  Rectangle2D.Double((float) origin.x - size, (float) origin.y - size, 2 * size, 2 * size);
    } else if (this.shape.toLowerCase().equals("triangle")) {
      Polygon p = new Polygon();
      double sizee = ((double) this.size);
      p.addPoint(
        (int) Math.round(origin.x),
        (int) Math.round(origin.y - sizee)
      );
      p.addPoint(
        (int) Math.round(origin.x - sizee * Math.sqrt(3) / 2),
        (int) Math.round(origin.y + sizee / 2)
      );
      p.addPoint(
        (int) Math.round(origin.x + sizee * Math.sqrt(3) / 2),
        (int) Math.round(origin.y + sizee / 2)
      );
      sp = (Shape) p;
    }

    Rectangle clRec = g.getClipBounds();

    g.setClip(sp);
    g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
    if (filling.scale < 1) {
      filling.scale = 1;
    }

    if (filling.name != null) {
      int size2 = size;
      if (clipTo10)
        size2 = Math.min(10, size);

      try {
        JavaVectorGraphic.bgColor = this.filling.color;
        JavaVectorGraphic.fgColor = this.border.color;
        Filling.getJavaVectorGraphic(this.filling.name).strokeSize = this.border.stroke;
        Filling.getJavaVectorGraphic(this.filling.name).draw(g, (int) Math.round(size*filling.scale), origin, size2);
        JavaVectorGraphic.bgColor = Color.WHITE;
        JavaVectorGraphic.fgColor = Color.BLACK;
      } catch (NullPointerException nex) {
        this.filling.name = Filling.addPattern(this.filling.name);
        try {
          JavaVectorGraphic.bgColor = this.filling.color;
          JavaVectorGraphic.fgColor = this.border.color;
          Filling.getJavaVectorGraphic(this.filling.name).strokeSize = this.border.stroke;
          Filling.getJavaVectorGraphic(this.filling.name).draw(g, (int) Math.round(size*filling.scale), origin, size2);
          JavaVectorGraphic.bgColor = Color.WHITE;
          JavaVectorGraphic.fgColor = Color.BLACK;
        } catch (NullPointerException nnex) {
          // System.err.println("Could not find JVG: " + nnex);
        }
      }

      g.setTransform(backup);
      g.transform(a);
    } else {
      if (!filling.graphic) {
        g.setColor(filling.color);
        g.fill(sp);
      } else {
        int size2 = size;
        Shape shape = sp;
        if (clipTo10) {
          size2 = Math.min(10, size);
          shape = new Ellipse2D.Float((float) origin.x - size2, (float) origin.y - size2, 2 * size2, 2 * size2);
        }
        g.setClip(shape);
        g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
        try {
          g.drawImage(Filling.imageNumberToImage.get(filling.imageNumber), (int) Math.round(origin.x - size * filling.scale), (int) Math.round(origin.y - size * filling.scale), (int) Math.round(2 * size * filling.scale), (int) Math.round(2 * size * filling.scale), null);
        } catch (NullPointerException ign) {}
      }
    }
    g.setClip(clRec);

    g.setColor(border.color);

    float stroke = border.stroke;

    if (!autoremove) {
      stroke += Math.min(stroke/2, 5) * Math.sin(this.border.dashOffset);
    }
    if (stroke < 0)
      stroke = 0;

    if (this.hover)
      stroke += 5;
    if ((this.hover) && stroke < 20) {
      stroke *= (float) 1.5;
    }

    if (border.dashed) {
      final float[] dash1 = {border.dashLength};
      g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, 0));
    } else
      g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));

    if (stroke > 0) {

      if (currentSelected) {
        float[] dash1 = {30f, 5};
        if (border.dashed)
          dash1 = new float[]{border.dashLength};
        g.setStroke(new BasicStroke(stroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10f, dash1, this.border.dashOffset));
      }

      g.draw(sp);
    }

    g.setTransform(backup);

    g.setStroke(oldStroke);
    g.setColor(oldColor);

    if (Pane.drawLabel && this.showDesc && this.description != null && !this.description.equals("") && this.descScale != 0) {
      if (description.contains("<html>")) {
        JLabel label = new JLabel(this.description, JLabel.CENTER);
        label.setForeground(Color.BLACK);
        label.setSize(label.getPreferredSize());
        double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.cos(this.rotDesc) + label.getPreferredSize().height * Math.sin(this.rotDesc);
        double labelHeight = /*label.getPreferredSize().height*/ -label.getPreferredSize().width * Math.sin(this.rotDesc) + label.getPreferredSize().height * Math.cos(this.rotDesc);
        labelHeight *= this.descScale;
        labelWidth *= this.descScale;
        Point2D place = new Point2D(this.origin.x + this.descPosX - labelWidth / 2, this.origin.y + this.descPosY - labelHeight / 2);
        g.translate(place.x, place.y);
        g.scale(this.descScale, this.descScale);
        g.rotate(-this.rotDesc);
        label.paint(g);
        g.rotate(this.rotDesc);
        g.scale(1 / this.descScale, 1 / this.descScale);
        g.translate(-place.x, -place.y);
      } else {
        TeXFormula formula = new TeXFormula(description);
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
        icon.setForeground(Color.BLACK);
        double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.cos(this.rotDesc) + icon.getIconHeight() * Math.sin(this.rotDesc);
        double labelHeight = /*label.getPreferredSize().height*/ -icon.getIconWidth() * Math.sin(this.rotDesc) + icon.getIconHeight() * Math.cos(this.rotDesc);
        labelHeight *= this.descScale;
        labelWidth *= this.descScale;
        Point2D place = new Point2D(this.origin.x + this.descPosX - labelWidth / 2, this.origin.y + this.descPosY - labelHeight / 2);
        g.translate(place.x, place.y);
        g.scale(this.descScale, this.descScale);
        g.rotate(-this.rotDesc);
        icon.paintIcon(new JPanel(), g, 0, 0);
        g.rotate(this.rotDesc);
        g.scale(1 / this.descScale, 1 / this.descScale);
        g.translate(-place.x, -place.y);
      }
    }
  }
  /**
   * turns the border to a new random color
   */
  public void nextColorBorder() {
    border.color = new Color((float) Math.random(), (float) Math.random(), (float) Math.random());
  }
  /**
   * turns the filling color to a new random color
   */
  public void nextColorFilling() {
    filling.color = new Color((float) Math.random(), (float) Math.random(), (float) Math.random());
  }
  /**
   * outputs a string that can be put in the config file to generate this exact vertex
   *
   * @return the string for the config file
   */
  public String toFile() {
    return "(" + this.toFileWOBracket() + ")";
  }
  /**
   * outputs a string that can be put in the config/model file to generate this exact vertex but without the brackets
   *
   * @return the string for the config file
   */
  public String toFileWOBracket() {
    String output = "shape=" + this.shape;
    output += ", size=" + Integer.toString(this.size);
    output += ", borderColor=" + Integer.toHexString(this.border.color.getRGB());
    output += ", borderDashed=" + this.border.dashed;
    if (this.border.dashed)
      output += ", borderDashLength=" + Integer.toString((int) border.dashLength);
    output += ", borderStroke=" + Integer.toString((int) border.stroke);
    output += ", rotation=" + Integer.toString((int) rotation);

    if (this.filling.graphic && this.filling.name == null) {
      try {
        String path = "";
        for (String s : Filling.pathToImageNumber.keySet()) {
          if (this.filling.imageNumber == Filling.pathToImageNumber.get(s)) {
            path = s;
            break;
          }
        }
        output += ", imagePath=" + path;
      } catch (Exception ex) {
        LOGGER.log(Level.SEVERE, "Path of Image could not be found", ex);
      }
    } else if(this.filling.name == null) {
      output += ", fillingColor = " + Integer.toHexString(this.filling.color.getRGB());
    } else {
      output += ", fxpath = " + filling.name;
    }

    if (this.filling.graphic || this.filling.name != null) {
      if (filling.scale != 1) {
        output += ", fillScale = " + filling.scale;
      }
    }

    if (this.description != null && this.description.trim() != "")
      output += ", label = " + this.description.replaceAll("#", "hashpound");

    return output;
  }
  /**
   * checks wether or not this is equal to the object; only checks filling, border, shape
   * @param obj the object to compare to
   */
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    Vertex other = (Vertex) obj;

    if (!this.filling.equals(other.filling))
      return false;
    if (!this.shape.equals(other.shape))
      return false;
    if (!this.border.equals(other.border))
      return false;
    // if (this.description == null && (other.description != null && !other.description.equals("")))
    //  return false;
    // if ((this.description != null && !this.description.equals("")) && other.description == null)
    //  return false;
    // if (this.description != null && other.description != null && !this.description.equals(other.description))
    //  return false;

    return true;
  }
}
