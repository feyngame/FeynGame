//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;
import java.io.Serializable;

/**
 * this is a class that stores the information from the JavaFx (".fx") files. It also handles drawing the objects on the screen
 *
 * @author Sven Yannick Klein
 */
public class JavaVectorGraphic implements Serializable {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 142L;
  /**
   * determines if the image is drawn or the lines are drawn
   */
  public static boolean drawVector = false;
  /**
   * maximum x value
   */
  public double maxX;
  /**
   * maximum y value
   */
  public double maxY;
  /**
   * minimun x value
   */
  public double minX;
  /**
   * minimum x value
   */
  public double minY;
  /**
   * the stroke size, if negative, the one from the jvg is used
   */
  public double strokeSize = -1;
  /**
   * the BufferedImage
   */
  private BufferedImage bImage;
  /**
   * the color white gets replaced with
   */
  public static Color bgColor = Color.WHITE;
  /**
   * the color the foreground gets replaced with
   */
  public static Color fgColor = Color.BLACK;
  /**
   * the paths with the corresponding BasicStrokes and Colors
   */
  private ArrayList<Map.Entry<Map.Entry<Map.Entry<SerialStroke, Color>, Color>, Shape>> pathShape = new ArrayList<>();
  /**
   * constructor: since this object is only constructed by the
   * {@link javafx.JavaFXToJavaVectorGraphic} class, this initializes only
   * the first path and color and stroke corresponding to this path later on,
   * also intializes the values to determine the size (dimensions) of the
   * obeject
   */
  public JavaVectorGraphic() {
    Map.Entry<SerialStroke, Color> fill = new AbstractMap.SimpleEntry<>(new SerialStroke(), null);
    Map.Entry<Map.Entry<SerialStroke, Color>, Color> style = new AbstractMap.SimpleEntry<>(fill, null);
    pathShape.add(new AbstractMap.SimpleEntry<>(style, new java.awt.geom.Path2D.Double()));
    maxX = -Double.MAX_VALUE;
    maxY = -Double.MAX_VALUE;
    minY = Double.MAX_VALUE;
    minX = Double.MAX_VALUE;
  }
  /**
   * returns the color of the last path
   * @return the color
   */
  public Color getColor() {
    return this.pathShape.get(pathShape.size() - 1).getKey().getValue();
  }
  /**
   * sets the color for the last path
   * @param newColor the color that the last path should have
   */
  public void setColor(Color newColor) {
    this.pathShape.get(pathShape.size() - 1).getKey().setValue(newColor);
  }
  /**
   * sets the color that the last path is filled with
   * @param newColor the color to set as the filling color
   */
  public void setFillColor(Color newColor) {
    this.pathShape.get(pathShape.size() - 1).getKey().getKey().setValue(newColor);
  }
  /**
   * set the stroke of the last path
   * @param newStroke the stroke to set the stroke of the last path to
   */
  public void setStroke(BasicStroke newStroke) {
    Map.Entry<SerialStroke, Color> fill = new AbstractMap.SimpleEntry<>(new SerialStroke(newStroke), this.pathShape.get(pathShape.size() - 1).getKey().getKey().getValue());
    Map.Entry<Map.Entry<SerialStroke, Color>, Color> style = new AbstractMap.SimpleEntry<>(fill, this.getColor());
    Shape tmpShape = this.pathShape.get(pathShape.size() - 1).getValue();
    this.pathShape.remove(pathShape.size() - 1);
    this.pathShape.add(new AbstractMap.SimpleEntry<>(style, tmpShape));
  }
  /**
   * finally constructs the path and creates an object holding all the
   * information of the path, its color, stroke etc and ads it to the paths
   * of this object
   * @param newPath the path to add
   */
  public void draw(java.awt.geom.Path2D.Double newPath) {
    this.pathShape.get(pathShape.size() - 1).setValue(newPath);


    Rectangle2D rec = newPath.getBounds2D();

    maxX = Math.max(maxX, rec.getMaxX());

    maxY = Math.max(maxY, rec.getMaxY());

    minX = Math.min(minX, rec.getMinX());

    minY = Math.min(minY, rec.getMinY());


    if (false) {
      JPanel panel = new JPanel() {
        public void paintComponent(Graphics gr) {
          super.paintComponent(gr);
          Graphics2D g = (Graphics2D) gr;
          AffineTransform af = new AffineTransform();
          af.translate(-minX, -minY);
          g.transform(af);
          for (int i = 0; i < pathShape.size(); i++) {
            if (pathShape.get(i).getKey().getKey().getValue() != null) {
              g.setPaint(pathShape.get(i).getKey().getKey().getValue());
              g.fill(pathShape.get(i).getValue());
            } else if (pathShape.get(i).getKey().getValue() != null) {
              g.setColor(pathShape.get(i).getKey().getValue());
              if (strokeSize < 0)
                g.setStroke(pathShape.get(i).getKey().getKey().getKey().toStroke());
              else
                g.setStroke(new BasicStroke((float) strokeSize));
              g.draw(pathShape.get(i).getValue());
            }

          }
          gr.dispose();
        }
      };
      panel.setSize(new Dimension((int) Math.round(maxX - minX), (int) Math.round(maxY - minY)));
      panel.setBackground(Color.WHITE);
      panel.setOpaque(true);
      BufferedImage image = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_ARGB);
      Graphics bGr = image.getGraphics();
      panel.print(bGr);
      this.bImage = image;
    }
    Map.Entry<SerialStroke, Color> fill = new AbstractMap.SimpleEntry<>(new SerialStroke(), null);
    Map.Entry<Map.Entry<SerialStroke, Color>, Color> style = new AbstractMap.SimpleEntry<>(fill, null);
    pathShape.add(new AbstractMap.SimpleEntry<>(style, new java.awt.geom.Path2D.Double()));
  }
  /**
   * draws the object as the content of the filling of a vertex or shape
   * @param g the graphics object to draw to
   * @param size the size that this object should have when drawn
   * @param origin where to draw
   * @param size2 was used to clip the drawing to a circle with this diameter
   * is not used anymore because this clipping is now handled in the
   * {@link game.Vertex} class itself
   */
  public void draw(Graphics2D g, int size, Point2D origin, int size2) {
    if (!JavaVectorGraphic.drawVector && false) {
      // Shape shape = new Rectangle2D.Float((int) Math.round(origin.x) - size2, (int) Math.round(origin.y) - size2, 2 * size2, 2 * size2);
      // g.setClip(shape);
      g.drawImage(this.bImage, (int) Math.round(origin.x - size), (int) Math.round(origin.y - size), 2 * size, 2 * size, null);
      // g.setClip(null);
    } else {
      // Shape shape = new Rectangle2D.Float((int) Math.round(origin.x) - size2, (int) Math.round(origin.y) - size2, 2 * size2, 2 * size2);
      // g.setClip(shape);
      AffineTransform backup = g.getTransform();
      AffineTransform af = new AffineTransform();
      double width = maxX - minX;
      double height = maxY - minY;
      af.translate(+origin.x, +origin.y);
      double scaleX = size / width;
      double scaleY = size / height;
      af.scale(2 * size / width, 2 * size / height);
      af.translate(-maxX + width / 2, -maxY + height / 2);
      g.transform(af);
      // Shape shape = new Rectangle2D.Double(+maxX - width, +maxY - height, size2 / scaleX, size2 / scaleY);
      // g.setClip(shape);

      for (int i = 0; i < pathShape.size(); i++) {

        if (pathShape.get(i).getKey().getKey().getValue() != null) {
          g.setPaint(pathShape.get(i).getKey().getKey().getValue());
          if (g.getColor().equals(Color.BLACK)) {
            g.setColor(JavaVectorGraphic.fgColor);
          }
          if (g.getColor().equals(Color.WHITE)) {
            g.setColor(JavaVectorGraphic.bgColor);
          }
          g.fill(pathShape.get(i).getValue());
        } else if (pathShape.get(i).getKey().getValue() != null) {
          g.setColor(pathShape.get(i).getKey().getValue());
          if (this.strokeSize < 0) {
            g.setStroke(pathShape.get(i).getKey().getKey().getKey().toStroke());
          } else {
            g.setStroke(new BasicStroke((float)this.strokeSize/(float)(2*size / width)));
          }
          if (g.getColor().equals(Color.BLACK)) {
            g.setColor(JavaVectorGraphic.fgColor);
          }
          if (g.getColor().equals(Color.WHITE)) {
            g.setColor(JavaVectorGraphic.bgColor);
          }
          g.draw(pathShape.get(i).getValue());
        }

      }
      // g.setClip(null);
      g.setTransform(backup);
    }
  }
  /**
   * draws the object as the content of the filling of a floatingImage
   * @param g the graphics object to draw to
   * @param scale the scale with which is drawn
   * @param origin where to draw
   * @param clipTo10 whether or not to only draw inside a circle with radius
   * 10
   */
  public void draw(Graphics2D g, float scale, Point2D origin, boolean clipTo10) {
    if (!JavaVectorGraphic.drawVector && false) {

      Rectangle clRec = g.getClipBounds();
      if (clipTo10) {
        Shape shape = new Ellipse2D.Float((float)origin.x - 10, (float)origin.y - 10, 2 * 10, 2 * 10);
        g.setClip(shape);
        g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
      }
      g.drawImage(this.bImage, (int) Math.round(origin.x - this.bImage.getWidth() * scale / 2), (int) Math.round(origin.y - this.bImage.getHeight() * scale / 2), Math.round(this.bImage.getWidth() * scale), Math.round(this.bImage.getHeight() * scale), null);
      g.setClip(clRec);
    } else {
      AffineTransform backup = g.getTransform();
      AffineTransform af = new AffineTransform();
      double width = maxX - minX;
      double height = maxY - minY;
      af.translate(+origin.x, +origin.y);
      af.scale(scale, scale);
      af.translate(-maxX + width / 2, -maxY + height / 2);
      g.transform(af);


      Rectangle clRec = g.getClipBounds();

      if (clipTo10) {
        Shape shape = new Ellipse2D.Float(-10 * scale, -10 * scale, 2 * 10 * scale, 2 * 10 * scale);
        g.setClip(shape);
        g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
      }

      for (int i = 0; i < pathShape.size(); i++) {

        if (pathShape.get(i).getKey().getKey().getValue() != null) {
          g.setPaint(pathShape.get(i).getKey().getKey().getValue());
          if (g.getColor().equals(Color.BLACK)) {
            g.setColor(JavaVectorGraphic.fgColor);
          }
          if (g.getColor().equals(Color.WHITE)) {
            g.setColor(JavaVectorGraphic.bgColor);
          }
          g.fill(pathShape.get(i).getValue());
        } else if (pathShape.get(i).getKey().getValue() != null) {
          g.setColor(pathShape.get(i).getKey().getValue());
          if (g.getColor().equals(Color.BLACK)) {
            g.setColor(JavaVectorGraphic.fgColor);
          }
          if (g.getColor().equals(Color.WHITE)) {
            g.setColor(JavaVectorGraphic.bgColor);
          }
          if (this.strokeSize < 0) {
            g.setStroke(pathShape.get(i).getKey().getKey().getKey().toStroke());
          } else {
            g.setStroke(new BasicStroke((float)this.strokeSize/(float)(scale)));
          }
          g.draw(pathShape.get(i).getValue());
        }

      }
      g.setClip(clRec);
      g.setTransform(backup);
    }
  }

}
