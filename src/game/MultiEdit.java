//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package game;

import ui.*;
import java.util.*;
import java.awt.Color;

/**
 * when a tile is selected all edits are done on the object in the tile and all objects on
 * the canvas that are "equal" to that object on the tile; this class deals with that
 *
 * @author Sven Yannick Klein
 */
public class MultiEdit {
  /**
   * decides whether a line, vertex or image is in the selected tile
   * 0 : line
   * 1 : vertex
   * 2 : image
   * 3 : empty
   */
  public int type;
  /**
   * a list containing pointers to all the lineconfigs of lines on the canvas that are the same
   * as the selected tile and a pointer to the lineconfig of the selected tile
   */
  private ArrayList<LineConfig> lineConfigs = new ArrayList<>();
  /**
   * a list containing pointers to all the vertices on the canvas that are the same
   * as the selected tile and a pointer to the vertex of the selected tile
   */
  private ArrayList<Vertex> vertices = new ArrayList<>();
  /**
   * a list containing pointers to all the floatingimages on the canvas that are the same
   * as the selected tile and a pointer to the floatingimage of the selected tile
   */
  private ArrayList<FloatingImage> images = new ArrayList<>();
  /**
   * whether or not this MultiEdit includes an object from a tile
   */
  private boolean withTile = false;
  /**
   * the main Frame
   */
  private Frame mainFrame;
  /**
   * whether or not finalize function does anything
   */
  public boolean finalize = true;
  /**
   * constructs the object with a single lineConfig
   * @param lc the lineConfig to add
   */
  public MultiEdit(LineConfig lc, Frame mainFrame) {
    type = 0;
    lineConfigs.add(lc);
    lc.multiEdit = true;
    this.mainFrame = mainFrame;
  }
  /**
   * constructs the object with a single vertex
   * @param v the vertex to add
   */
  public MultiEdit(Vertex v, Frame mainFrame) {
    type = 1;
    vertices.add(v);
    v.multiEdit = true;
    this.mainFrame = mainFrame;
  }
  /**
   * constructs the object with a single image
   * @param im the image to add
   */
  public MultiEdit(FloatingImage im, Frame mainFrame) {
    type = 2;
    images.add(im);
    im.multiEdit = true;
    this.mainFrame = mainFrame;
  }
  /**
   * copy constructor
   * @param old the MultiEdit to copy
   */
  public MultiEdit(MultiEdit old) {
    this.type = old.type;
    if (old.type == 0) {
      for (LineConfig lc : old.lineConfigs) {
        this.lineConfigs.add(lc);
        lc.multiEdit = true;
      }
    } else if (old.type == 1) {
      for (Vertex lc : old.vertices) {
        this.vertices.add(lc);
        lc.multiEdit = true;
      }
    } else if (old.type == 2) {
      for (FloatingImage lc : old.images) {
        this.images.add(lc);
        lc.multiEdit = true;
      }
    }
    this.mainFrame = old.mainFrame;
  }
  /**
   * add a lineConfig to the multiEdit
   * @param lc the lineConfig to add
   */
  public void add(LineConfig lc) {

    if (type == 3) {
      type = 0;
      return;
    }

    if (type != 0)
      return;

    lc.multiEdit = true;
    lineConfigs.add(lc);
  }
  /**
   * add a vertex to the multiEdit
   * @param v the vertex to add
   */
  public void add(Vertex v) {

    if (type == 3) {
      type = 1;
      return;
    }

    if (type != 1)
      return;

    v.multiEdit = true;
    vertices.add(v);
  }
  /**
   * add an image to the multiEdit
   * @param im the image to add
   */
  public void add(FloatingImage im) {

    if (type == 3) {
      type = 2;
      return;
    }

    if (type != 2)
      return;

    im.multiEdit = true;
    images.add(im);
  }
  /**
   * constructs the object when a tile with a line is selcted
   * @param mainFrame the mainFrame
   * @param lineConfig the lineConfig
   */
  public MultiEdit(Frame mainFrame, LineConfig lineConfig) {
    this.mainFrame = mainFrame;
    this.withTile = true;
    type = 0;
    lineConfigs.add(lineConfig);
    for (FeynLine line : mainFrame.getDrawPane().lines) {
      if (line.lineConfig.equals(lineConfig)) {
        line.lineConfig.multiEdit = true;
        lineConfigs.add(line.lineConfig);
      }
    }
  }
  /**
   * constructs the object when a tile with a vertex is selcted
   * @param mainFrame the mainFrame
   * @param vertex the vertex
   */
  public MultiEdit(Frame mainFrame, Vertex vertex) {
    this.mainFrame = mainFrame;
    this.withTile = true;
    type = 1;
    vertices.add(vertex);
    for (Vertex v : mainFrame.getDrawPane().vertices) {
      if (v.equals(vertex)) {
        v.multiEdit = true;
        vertices.add(v);
      }
    }
    for (FloatingObject fO : mainFrame.getDrawPane().fObjects) {
      if (fO.v) {
        if (fO.vertex.equals(vertex)) {
          fO.vertex.multiEdit = true;
          vertices.add(fO.vertex);
        }
      }
    }
  } /**
   * constructs the object when a tile with an image is selcted
   * @param mainFrame the mainFrame
   * @param image the image
   */
  public MultiEdit(Frame mainFrame, FloatingImage image) {
    this.mainFrame = mainFrame;
    this.withTile = true;
    type = 2;
    images.add(image);
    for (FloatingObject fO : mainFrame.getDrawPane().fObjects) {
      if (!fO.v) {
        if (fO.floatingImage.equals(image)) {
          fO.floatingImage.multiEdit = true;
          images.add(fO.floatingImage);
        }
      }
    }
  }
  /**
   * removes all objects from the multiEdit and sets them as not selected
   */
  public void fin() {

    if (!finalize)
      return;

    for (LineConfig lc : lineConfigs)
      lc.multiEdit = false;
    for (Vertex v : vertices)
      v.multiEdit = false;
    for (FloatingImage im : images)
      im.multiEdit = false;

    lineConfigs.clear();
    vertices.clear();
    images.clear();

    type = 3;
  }
  /**
   * change the line type
   * @param typeString the string corresponding to the new linetype
   */
  public void changeType(String typeString) {
    if (this.type != 0) {
      return;
    }
    if (withTile && !typeString.toLowerCase().equals(lineType().toLowerCase()))
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.changeType(typeString);
    }
  }
  /**
   * get the line types
   * @return the lineType
   */
  public String lineType() {
    if (this.type != 0) {
      return "";
    }
    if (lineConfigs.get(0).lineType.equals(LineType.PHOTON)) {
      return "Photon";
    } else if (lineConfigs.get(0).lineType.equals(LineType.GLUON)) {
      return "Gluon";
    } else if (lineConfigs.get(0).lineType.equals(LineType.FERMION)) {
      return "Fermion";
    } else if (lineConfigs.get(0).lineType.equals(LineType.SSPIRAL)) {
      return "Gluino";
    } else if (lineConfigs.get(0).lineType.equals(LineType.SWAVE)) {
      return "Photino";
    }
    return "";
  }
  /**
   * change the line types
   * @param lineType the line Type to change to
   */
  public void setLineType(LineType lineType) {
    if (this.type != 0) {
      return;
    }
    if (withTile && !lineType.equals(getLineType()))
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.lineType = lineType;
    }
  }
  /**
   * get the line types
   * @return the lineType
   */
  public LineType getLineType() {
    if (this.type != 0) {
      return LineType.GRAB;
    }
    return lineConfigs.get(0).lineType;
  }
  /**
   * change the stroke
   * @param stroke the stoke to change to
   */
  public void setStroke(double stroke) {
    if (this.type == 2) {
      return;
    }

    if (withTile && stroke != getStroke())
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 0) {
      for (LineConfig lc : lineConfigs) {
        lc.stroke = (float) stroke;
      }
    }
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.border.stroke = (float) stroke;
      }
    }
  }
  /**
   * get the stroke
   * @return the stroke
   */
  public double getStroke() {
    if (this.type == 2) {
      return 0;
    }
    if (this.type == 0)
      return lineConfigs.get(0).stroke;
    if (this.type == 1)
      return vertices.get(0).border.stroke;
    return 0;
  }
  /**
   * change the wiggleLength
   * @param wiggleLength the stoke to change to
   */
  public void setWiggleLength(double wiggleLength) {
    if (this.type != 0) {
      return;
    }

    if (withTile && wiggleLength != getWiggleLength())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.wiggleLength = (float) wiggleLength;
    }
  }
  /**
   * get the wiggleLength
   * @return the wiggleLength
   */
  public double getWiggleLength() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).wiggleLength;
  }
  /**
   * change the wiggleHeight
   * @param wiggleHeight the stoke to change to
   */
  public void setWiggleHeight(double wiggleHeight) {
    if (this.type != 0) {
      return;
    }
    if (withTile && wiggleHeight != getWiggleHeight())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.wiggleHeight = (float) wiggleHeight;
    }
  }
  /**
   * get the wiggleHeight
   * @return the wiggleHeight
   */
  public double getWiggleHeight() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).wiggleHeight;
  }
  /**
   * change the straight_len
   * @param straight_len the stoke to change to
   */
  public void setStraightLength(double straight_len) {
    if (this.type != 0) {
      return;
    }
    if (withTile && straight_len != getWiggleOffset())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.straight_len = straight_len;
    }
  }
  /**
   * get the straight_len
   * @return the straight_len
   */
  public double getStraightLength() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).straight_len;
  }
  /**
   * change the straight_len_arrow
   * @param straight_len_arrow the stoke to change to
   */
  public void setStraightLengthArrow(double straight_len_arrow) {
    if (this.type != 0) {
      return;
    }
    if (withTile && straight_len_arrow != getWiggleOffset())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.straight_len_arrow = straight_len_arrow;
    }
  }
  /**
   * get the straight_len_arrow
   * @return the straight_len_arrow
   */
  public double getStraightLengthArrow() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).straight_len_arrow;
  }
  /**
   * change the wiggleOffset
   * @param wiggleOffset the stoke to change to
   */
  public void setWiggleOffset(double wiggleOffset) {
    if (this.type != 0) {
      return;
    }
    if (withTile && wiggleOffset != getWiggleOffset())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.wiggleOffset = (float) wiggleOffset;
    }
  }
  /**
   * get the wiggleOffset
   * @return the wiggleOffset
   */
  public double getWiggleOffset() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).wiggleOffset;
  }
  /**
   * change the wiggleAsymmetry
   * @param wiggleAsymmetry the stoke to change to
   */
  public void setWiggleAsymmetry(double wiggleAsymmetry) {
    if (this.type != 0) {
      return;
    }
    if (withTile && wiggleAsymmetry != getWiggleAsymmetry())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.wiggleAsymmetry = wiggleAsymmetry;
    }
  }
  /**
   * get the wiggleAsymmetry
   * @return the wiggleAsymmetry
   */
  public double getWiggleAsymmetry() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).wiggleAsymmetry;
  }
  /**
   * change the dashLength
   * @param dashLength the stoke to change to
   */
  public void setDashLength(double dashLength) {
    if (this.type == 2) {
      return;
    }
    if (withTile && dashLength != getDashLength())
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 0) {
      for (LineConfig lc : lineConfigs) {
        lc.dashLength = (float) dashLength;
      }
    }
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.border.dashLength = (float) dashLength;
      }
    }
  }
  /**
   * get the dashLength
   * @return the dashLength
   */
  public double getDashLength() {
    if (this.type == 2) {
      return 0;
    }
    if (this.type == 0)
      return lineConfigs.get(0).dashLength;
    if (this.type == 1)
      return vertices.get(0).border.dashLength;

    return 0;
  }
  /**
   * change the dashOffset
   * @param dashOffset the stoke to change to
   */
  public void setDashOffset(double dashOffset) {
    if (this.type == 2) {
      return;
    }
    if (withTile && dashOffset != getDashOffset())
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 0) {
      for (LineConfig lc : lineConfigs) {
        lc.dashOffset = (float) dashOffset;
      }
    }
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.border.dashOffset = (float) dashOffset;
      }
    }
  }
  /**
   * get the dashOffset
   * @return the dashOffset
   */
  public double getDashOffset() {
    if (this.type == 2) {
      return 0;
    }
    if (this.type == 0)
      return lineConfigs.get(0).dashOffset;
    if (this.type == 1)
      return vertices.get(0).border.dashOffset;

    return 0;
  }
  /**
   * change the dashed
   * @param dashed the stoke to change to
   */
  public void setDashed(boolean dashed) {
    if (this.type == 2) {
      return;
    }
    if (withTile && dashed != getDashed())
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 0) {
      for (LineConfig lc : lineConfigs) {
        lc.dashed = dashed;
      }
    }
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.border.dashed = dashed;
      }
    }
  }
  /**
   * get the dashed
   * @return the dashed
   */
  public boolean getDashed() {
    if (this.type == 2) {
      return false;
    }
    if (this.type == 0)
      return lineConfigs.get(0).dashed;
    if (this.type == 1)
      return vertices.get(0).border.dashed;

    return false;
  }
  /**
   * change the showArrow
   * @param showArrow the stoke to change to
   */
  public void setShowArrow(boolean showArrow) {
    if (this.type != 0) {
      return;
    }
    if (withTile && showArrow != getShowArrow())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.showArrow = showArrow;
    }
  }
  /**
   * get the showArrow
   * @return the showArrow
   */
  public boolean getShowArrow() {
    if (this.type != 0) {
      return false;
    }
    return lineConfigs.get(0).showArrow;
  }
  /**
   * change the arrowSize
   * @param arrowSize the stoke to change to
   */
  public void setArrowSize(double arrowSize) {
    if (this.type != 0) {
      return;
    }
    if (withTile && arrowSize != getArrowSize())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.arrowSize = (int) arrowSize;
    }
  }
  /**
   * get the arrowSize
   * @return the arrowSize
   */
  public double getArrowSize() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).arrowSize;
  }
  /**
   * change the arrowDent
   * @param arrowDent the stoke to change to
   */
  public void setArrowDent(double arrowDent) {
    if (this.type != 0) {
      return;
    }
    if (withTile && arrowDent != getArrowDent())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.arrowDent = (int) arrowDent;
    }
  }
  /**
   * get the arrowDent
   * @return the arrowDent
   */
  public double getArrowDent() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).arrowDent;
  }
  /**
   * change the arrowHeight
   * @param arrowHeight the stoke to change to
   */
  public void setArrowHeight(double arrowHeight) {
    if (this.type != 0) {
      return;
    }
    if (withTile && arrowHeight != getArrowHeight())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.arrowHeight = (int) arrowHeight;
    }
  }
  /**
   * get the arrowHeight
   * @return the arrowHeight
   */
  public double getArrowHeight() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).arrowHeight;
  }
  /**
   * change the arrowPlace
   * @param arrowPlace the stoke to change to
   */
  public void setArrowPlace(double arrowPlace) {
    if (this.type != 0) {
      return;
    }
    if (withTile && arrowPlace != getArrowPlace())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.arrowPlace = arrowPlace;
    }
  }
  /**
   * get the arrowPlace
   * @return the arrowPlace
   */
  public double getArrowPlace() {
    if (this.type != 0) {
      return 0;
    }
    return lineConfigs.get(0).arrowPlace;
  }
  /**
   * change the doubleLine
   * @param doubleLine the stoke to change to
   */
  public void setDoubleLine(boolean doubleLine) {
    if (this.type != 0) {
      return;
    }
    if (withTile && doubleLine != getDoubleLine())
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.doubleLine = doubleLine;
    }
  }
  /**
   * get the doubleLine
   * @return the doubleLine
   */
  public boolean getDoubleLine() {
    if (this.type != 0) {
      return false;
    }
    return lineConfigs.get(0).doubleLine;
  }
  /**
   * change the color
   * @param color the stoke to change to
   */
  public void setColor(Color color) {
    if (this.type != 0) {
      return;
    }
    if (withTile && !color.equals(getColor()))
      mainFrame.tileTabs.setCurrUnsaved();
    for (LineConfig lc : lineConfigs) {
      lc.color = new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
  }
  /**
   * get the color
   * @return the color
   */
  public Color getColor() {
    if (this.type != 0) {
      return Color.WHITE;
    }
    Color color = lineConfigs.get(0).color;
    return new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
  }
  /**
   * set the rotation
   * @param rotation the rotation to change to
   */
  public void setRotation(double rotation) {
    if (this.type == 0)
      return;


    if (withTile && rotation != getRotation())
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.rotation = (float) rotation;
      }
    }
    if (this.type == 2) {
      for (FloatingImage im : images) {
        im.rotation = (float) rotation;
      }
    }
  }
  /**
   * get the rotation
   * @return the rotation
   */
  public double getRotation() {
    if (this.type == 0)
      return 0;
    if (this.type == 1)
      return vertices.get(0).rotation;
    if (this.type == 2)
      return images.get(0).rotation;

    return 0;
  }
  /**
   * set the scale
   * @param scale the scale to change to
   */
  public void setScale(double scale) {
    if (this.type == 0)
      return;

    if (withTile && scale!= getScale())
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.filling.scale = scale;
      }
    }
    if (this.type == 2) {
      for (FloatingImage im : images) {
        im.scale = (float) scale;
      }
    }
  }
  /**
   * get the scale
   * @return the scale
   */
  public double getScale() {
    if (this.type == 0)
      return 0;
    if (this.type == 1)
      return vertices.get(0).filling.scale;
    if (this.type == 2)
      return (double) images.get(0).scale;

    return 0;
  }
  /**
   * set the imageNumber
   * @param imageNumber the imageNumber to change to
   */
  public void setImageNumber(int imageNumber) {
    if (this.type == 0)
      return;

    if (withTile && imageNumber != getImageNumber())
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.filling.imageNumber = imageNumber;
        v.filling.graphic = true;
        v.filling.name = null;
      }
    }
    if (this.type == 2) {
      for (FloatingImage im : images) {
        im.imageNumber = imageNumber;
        im.name = null;
      }
    }
  }
  /**
   * get the imageNumber
   * @return the imageNumber
   */
  public int getImageNumber() {
    if (this.type == 0)
      return 0;
    if (this.type == 1)
      return vertices.get(0).filling.imageNumber;
    if (this.type == 2)
      return images.get(0).imageNumber;

    return 0;
  }
  /**
   * set the name
   * @param name the name to change to
   */
  public void setName(String name) {
    if (this.type == 0)
      return;

    if (withTile && !name.equals(getName()))
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.filling.name = new String(name);
      }
    }
    if (this.type == 2) {
      for (FloatingImage im : images) {
        im.name = new String(name);
      }
    }
  }
  /**
   * set the name to null
   */
  public void purgeName() {
    if (this.type == 0)
      return;
    if (this.type == 1) {
      for (Vertex v : vertices) {
        v.filling.name = null;
      }
    }
    if (this.type == 2) {
      for (FloatingImage im : images) {
        im.name = null;
      }
    }
  }
  /**
   * get the name
   * @return the name
   */
  public String getName() {
    if (this.type == 0)
      return "";
    if (this.type == 1)
      return vertices.get(0).filling.name;
    if (this.type == 2)
      return images.get(0).name;

    return "";
  }
  /**
   * set the filling from a path to a file
   * @param path the path to the file
   */
  public void setFromPath(String path) {
    if (this.type == 0)
      return;

    if (withTile)
      mainFrame.tileTabs.setCurrUnsaved();

    if (!path.endsWith(".fx"))
      setImageNumber(Filling.getImageNumber(path));
    if (path.endsWith(".fx"))
      setName(Filling.addPattern(path));
  }
  /**
   * change the size
   * @param size the stoke to change to
   */
  public void setSize(int size) {
    if (this.type != 1) {
      return;
    }

    if (withTile && size != getSize())
      mainFrame.tileTabs.setCurrUnsaved();
    for (Vertex v : vertices) {
      v.size = size;
    }
  }
  /**
   * get the size
   * @return the size
   */
  public int getSize() {
    if (this.type != 1) {
      return 0;
    }
    return vertices.get(0).size;
  }
  /**
   * change the shape
   * @param shape the stoke to change to
   */
  public void setShape(String shape) {
    if (this.type != 1) {
      return;
    }
    if (withTile && !shape.toLowerCase().equals(getShape().toLowerCase()))
      mainFrame.tileTabs.setCurrUnsaved();
    for (Vertex v : vertices) {
      v.shape = new String(shape);
    }
  }
  /**
   * get the shape
   * @return the shape
   */
  public String getShape() {
    if (this.type != 1) {
      return "";
    }
    return vertices.get(0).shape;
  }
  /**
   * change the filling color
   * @param color the stoke to change to
   */
  public void setFillColor(Color color) {
    if (this.type != 1) {
      return;
    }
    if (withTile && !color.equals(getFillColor()))
      mainFrame.tileTabs.setCurrUnsaved();
    for (Vertex v : vertices) {
      v.filling.color = new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
      v.filling.graphic = false;
      v.filling.name = null;
    }
  }
  /**
   * get the filling color
   * @return the color
   */
  public Color getFillColor() {
    if (this.type != 1) {
      return Color.WHITE;
    }
    Color color = vertices.get(0).filling.color;
    return new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
  }
  /**
   * change the border color
   * @param color the stoke to change to
   */
  public void setBorderColor(Color color) {
    if (this.type != 1) {
      return;
    }
    if (withTile && !color.equals(getBorderColor()))
      mainFrame.tileTabs.setCurrUnsaved();
    for (Vertex v : vertices) {
      v.border.color = new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
  }
  /**
   * get the border color
   * @return the color
   */
  public Color getBorderColor() {
    if (this.type != 1) {
      return Color.WHITE;
    }
    Color color = vertices.get(0).border.color;
    return new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
  }
  /**
   * returns the graphic variable of the vertices
   * @return graphic
   */
  public boolean getGraphic() {
    if (this.type != 1)
      return false;

    return vertices.get(0).filling.graphic;
  }
  /**
   * returns whether of not the label/description of the first selected object
   * is visible
   * @return showDesc
   */
  public boolean getShowDesc() {
    if (this.type == 0) {
      for (FeynLine fl : mainFrame.getDrawPane().lines) {
        for (LineConfig lc : this.lineConfigs) {
          if (fl.lineConfig == lc) {
            return fl.showDesc;
          }
        }
      }
    } else if (this.type == 1) {
      return vertices.get(0).showDesc;
    } else if (this.type == 2) {
      return images.get(0).showDesc;
    }
    return false;
  }
  /**
   * Sets whether or not the labels/descriptions are shown
   * @param showDesc whether or not to show the label
   */
  public void setShowDesc(boolean showDesc) {
    if (this.type == 0) {
      for (LineConfig lc : this.lineConfigs) {
        for (FeynLine fl : mainFrame.getDrawPane().lines) {
          if (fl.lineConfig == lc) {
            fl.showDesc = showDesc;
            break;
          }
        }
      }
    } else if (this.type == 1) {
      for (Vertex v : vertices) {
        v.showDesc = showDesc;
      }
    } else if (this.type == 2) {
      for (FloatingImage fi : images) {
        fi.showDesc = showDesc;
      }
    }
  }
  /**
   * returns the description/label of the leading object
   * @return the description/label
   */
  public String getDescription() {
    if (this.type == 0)
      return lineConfigs.get(0).description;
    else if (this.type ==1)
      return vertices.get(0).description;
    else if (this.type == 2)
      return images.get(0).description;
    return "";
  }
  /**
   * set the description/label
   * @param desc the description/label
   */
  public void setDescription(String desc) {

    if (withTile && !desc.equals(getDescription()))
      mainFrame.tileTabs.setCurrUnsaved();
    if (this.type == 0) {
      for (LineConfig lc : lineConfigs)
        lc.description = desc;
    }
    else if (this.type == 1) {
      for (Vertex v : vertices)
        v.description = desc;
    }
    else if (this.type == 2) {
      for (FloatingImage im : images)
        im.description = desc;
    }
  }
  /**
   * returns the first vertex if this is vertices and else simply a default vertex
   * @return what is described above
   */
  public Vertex getVertex() {
    if (this.type == 1) {
      return vertices.get(0);
    }
    return new Vertex();
  }
  /**
   * turns all selected vertices to the same style as the given vertex
   * @param to the vertex from which to copy the style
   */
  public void turnTo(Vertex to) {
    if (type != 1)
      return;
    for (Vertex v : vertices) {
      v.turnTo(v);
    }
  }
  /**
   * turns all selected lines to the same style as the given lineconfig
   * @param to the lineconfig from which to copy the style
   */
  public void turnTo(LineConfig to) {
    if (type != 0)
      return;
    if (!this.withTile) {
      for (LineConfig lc : lineConfigs) {
        lc.turnTo(to);
      }
    } else {
      Map.Entry<String,String> id = new AbstractMap.SimpleEntry<>(this.lineConfigs.get(0).identifier.getKey(), this.lineConfigs.get(0).identifier.getKey());
      for (LineConfig lc : lineConfigs) {
        lc.turnTo(to);
        lc.identifier = new AbstractMap.SimpleEntry<>(id.getKey(), id.getValue());
      }
    }
  }
  /**
   * turns all selected images to the same style as the given image
   * @param to the image from which to copy the style
   */
  public void turnTo(FloatingImage to) {
    if (type != 2)
      return;
    for (FloatingImage im : images) {
      im.turnTo(to);
    }
  }

}
