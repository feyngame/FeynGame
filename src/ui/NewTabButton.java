//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import javax.swing.*;
import java.awt.*;
import javax.swing.UIManager.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class NewTabButton extends CustomTabButton {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 34L;
  private Color hoverBackgroundColor;
  private Color pressedBackgroundColor;

  public NewTabButton() {

    Color norColor = UIManager.getColor("MenuItem.background");
    Color selColor = UIManager.getColor("MenuItem.selectionBackground");

    if (norColor == null) {
      norColor = this.getBackground();
    }
    if (selColor == null) {
      selColor = new Color((norColor.getRed() + 128) % 256, (norColor.getGreen() + 128) % 256, (norColor.getBlue() + 128) % 256);
    }

    this.setOpaque(true);
    this.setBackground(norColor);

    if (norColor.getGreen() + norColor.getBlue() + norColor.getRed() > 383) {
      this.setHoverBackgroundColor(norColor.darker());
    } else {
      this.setHoverBackgroundColor(norColor.brighter());
    }
    this.setPressedBackgroundColor(selColor);

    this.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 0));

    this.setMargin(new Insets(0, -5, 0, -5));
    if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      this.setMargin(new Insets(5, 5, 5, 5));
    }

    JLabel label = new JLabel("<html>&#10010");
    label.setSize(label.getPreferredSize());

    this.add(label);

    this.setSize(this.getPreferredSize());

    this.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseEntered(java.awt.event.MouseEvent evt) {
        thisHovered = true;
      }

      public void mouseExited(java.awt.event.MouseEvent evt) {
        thisHovered = false;
      }
    });

    this.setAlignmentX(LEFT_ALIGNMENT);
  }
}
