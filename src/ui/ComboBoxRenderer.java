//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 * this class is to display the differnt patterns in the combobox in the
 * EditFrame for choosing a pattern
 *
 * @author Sven Yannick Klein
 */
class ComboBoxRenderer extends JLabel implements ListCellRenderer<Object> {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 31L;

  private ImageIcon[] images;

  private String[] strings;

  public ComboBoxRenderer(ImageIcon[] images, String[] strings) {
    this.images = images;
    this.strings = strings;
    setOpaque(true);
    setHorizontalAlignment(LEFT);
    setVerticalAlignment(CENTER);
  }

  /*
   * This method finds the image and text corresponding
   * to the selected value and returns the label, set up
   * to display the text and image.
   */
  @Override
  public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
    //Get the selected index. (The index param isn't
    //always valid, so just use the value.)
    int selectedIndex = ((Integer)value).intValue();
    // int selectedIndex = index;

    if (isSelected) {
      setBackground(list.getSelectionBackground());
      setForeground(list.getSelectionForeground());
    } else {
      setBackground(list.getBackground());
      setForeground(list.getForeground());
    }

    //Set the icon and text.  If icon was null, say so.
    ImageIcon icon = images[selectedIndex];
    String string = strings[selectedIndex];
    setIcon(icon);
    if (icon != null) {
      setText(string);
      setFont(list.getFont());
    } else {
      setText(string);
      setFont(list.getFont());
    }

    return this;
  }
}
