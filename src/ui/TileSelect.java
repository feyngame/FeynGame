//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.FloatingImage;
import game.LineConfig;
import game.LineType;
import game.Vertex;
import game.MultiEdit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Random;

/**
 * This is used in lower part of the {@link ui.Frame} to preview all the available lines.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
public class TileSelect extends JPanel {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 6L;
  /**
   * This is the height of the control elements.
   */
  public static final int uiHeight = 60;
  /**
   * This is used to highlight the current selected {@link ui.TileLabel}.
   */
  public int selectedIndex = 0;
  /**
   * Represents the current chosen {@link game.LineConfig}.
   */
  LineConfig selected;
  /**
   * Represents the current chosen {@link game.LineConfig}.
   */
  Vertex selectedVertex;
  /**
   * Represents the current chosen {@link game.FloatingImage}.
   */
  FloatingImage selectedImage;
  /**
   * Gives access to the main frame;
   */
  Frame mainFrame;
  /**
   * the number of tiles in a row
   */
  public static int columns = 8;
  /**
   * the constructor that initializes the individual tiles and lays them out
   * @param mainFrame the main Window
   */
  TileSelect(Frame mainFrame) {

    ToolTipManager.sharedInstance().registerComponent(this);

    this.mainFrame = mainFrame;
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();

    this.setLayout(gridbag);
    c.weightx = 1;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = -1;

    /* eights counts the number of rows */

    int i = 0;
    int eights = 0;

    columns = (int) ( mainFrame.getContentPane().getWidth() / (uiHeight + 10) );

    if (this.mainFrame.gameMode == 0) {
      if (i == columns * (eights + 1)) { /* is always false */ /* is it?*/
        eights++;
        c.gridy++;
        c.gridx = 0;
      } else {
        c.gridx++;
      }

      AddToConfigTile addTile = new AddToConfigTile();
      addTile.addActionListener(new addListener());
      addTile.setPreferredSize(new Dimension(uiHeight, uiHeight));
      this.add(addTile, c);
      i++;
    }

    for (LineConfig t : LineConfig.presets) {
      if (!t.lineType.equals(LineType.GRAB)) {
        if (i == columns * (eights + 1)) {
          eights++;
          c.gridy++;
          c.gridx = 0;
        } else {
          c.gridx++;
        }
        TileLabel l = new TileLabel(t);
        l.setPreferredSize(new Dimension(uiHeight, uiHeight));
        if (mainFrame.gameMode == 1)
          i++;
        l.addActionListener(new myListener(i - 1));
        if (mainFrame.gameMode == 1)
          i--;
        this.add(l, c);
        i++;
      }
    }

    if (mainFrame.gameMode == 0) {
      for (Map.Entry<Vertex, String> v : Vertex.vertexPresets) {
        if (i == columns * (eights + 1)) {
          eights++;
          c.gridy++;
          c.gridx = 0;
        } else {
          c.gridx++;
        }
        Vertex t = v.getKey();
        VertexLabel l = new VertexLabel(t);
        l.setPreferredSize(new Dimension(uiHeight, uiHeight));
        l.addActionListener(new myListener(i - 1));
        this.add(l, c);
        i++;
      }
    }
    if (mainFrame.gameMode == 0) {
      for (FloatingImage fI : FloatingImage.fIPresets) {
        if (i == columns * (eights + 1)) {
          eights++;
          c.gridy++;
          c.gridx = 0;
        } else {
          c.gridx++;
        }
        ImageLabel l = new ImageLabel(fI);
        l.addActionListener(new myListener(i - 1));
        l.setPreferredSize(new Dimension(uiHeight, uiHeight));
        this.add(l, c);
        i++;
      }
    }

    this.setGrabMode();
  }
  /**
   * Changes the current selected {@link ui.TileLabel} and redraws border. Used in {@link myListener#actionPerformed(ActionEvent)}
   *
   * @param index new index
   */
  void setSelected(int index) {
    selectedIndex = index;
    if (index >= FloatingImage.fIPresets.size() + LineConfig.presets.size() + Vertex.vertexPresets.size() - 1 && mainFrame.gameMode == 0) return;
    if (index >= LineConfig.presets.size() - 1 && mainFrame.gameMode != 0) return;
    if (index < 0) return;
    if (index < LineConfig.presets.size() - 1) {
      selected = LineConfig.presets.get(index);
      mainFrame.getDrawPane().selectType = 4;
      if (mainFrame.getDrawPane().multiEdit != null) {
        mainFrame.getDrawPane().multiEdit.fin();
      }
      if (mainFrame.gameMode == 0)
        mainFrame.getDrawPane().multiEdit = new MultiEdit(mainFrame, selected);
      selectedVertex = null;
      selectedImage = null;
      if (mainFrame.eframe != null) {
        mainFrame.eframe.editPane.upEditPane();
        mainFrame.eframe.repaint();
      }
    } else if (index < LineConfig.presets.size() - 1 + Vertex.vertexPresets.size()) {
      selectedVertex = Vertex.vertexPresets.get(index - LineConfig.presets.size() + 1).getKey();
      mainFrame.getDrawPane().selectType = 4;
      if (mainFrame.getDrawPane().multiEdit != null) {
        mainFrame.getDrawPane().multiEdit.fin();
      }
      if (mainFrame.gameMode == 0)
        mainFrame.getDrawPane().multiEdit = new MultiEdit(mainFrame, selectedVertex);
      selected = null;
      selectedImage = null;
      if (mainFrame.eframe != null) {
        mainFrame.eframe.editPane.upEditPane();
        mainFrame.eframe.repaint();
      }
    } else {
      selectedImage = FloatingImage.fIPresets.get(index - LineConfig.presets.size() + 1 - Vertex.vertexPresets.size());
      mainFrame.getDrawPane().selectType = 4;
      if (mainFrame.getDrawPane().multiEdit != null) {
        mainFrame.getDrawPane().multiEdit.fin();
      }
      if (mainFrame.gameMode == 0)
        mainFrame.getDrawPane().multiEdit = new MultiEdit(mainFrame, selectedImage);
      selected = null;
      selectedVertex = null;
      if (mainFrame.eframe != null) {
        mainFrame.eframe.editPane.upEditPane();
        mainFrame.eframe.repaint();
      }
    }
    for (Component l : this.getComponents()) {
      try {
        ((TileLabel) l).setBorder(UIManager.getBorder("Button.border"));
      } catch (Exception ex) {
        try {
          ((VertexLabel) l).setBorder(UIManager.getBorder("Button.border"));
        } catch (Exception e) {
          try {
            ((ImageLabel) l).setBorder(UIManager.getBorder("Button.border"));
          } catch (Exception ignored) {
          }
        }
      }
    }
    if (true) {
      mainFrame.changeCursorToDefault();
      if (mainFrame.gameMode == 1)
        index -= 1;
      try {
        TileLabel l = (TileLabel) this.getComponents()[index + 1];
        l.setBorder(BorderFactory.createLineBorder(Color.black, 5));
      } catch (Exception ex) {
        try {
          VertexLabel l = (VertexLabel) this.getComponents()[index + 1];
          l.setBorder(BorderFactory.createLineBorder(Color.black, 5));
        } catch (Exception e) {
          try {
            ImageLabel l = (ImageLabel) this.getComponents()[index + 1];
            l.setBorder(BorderFactory.createLineBorder(Color.black, 5));
          } catch (Exception exx) {
            exx.printStackTrace();
          }
        }
      }
    } else {
      mainFrame.changeCursorToGrab();
    }

    mainFrame.repaint();
  }

  /**
   * Iterates next {@link ui.TileLabel}.
   */
  void next() {
    int newIndex = selectedIndex + 1;
    if (newIndex >= FloatingImage.fIPresets.size() + LineConfig.presets.size() + Vertex.vertexPresets.size() - 1 && mainFrame.gameMode == 0) {
      newIndex = 0;
    } else if (newIndex > LineConfig.presets.size() - 1 && mainFrame.gameMode != 0) {
      newIndex = 0;
    }
    this.setSelected(newIndex);
  }

  /**
   * Iterates previous {@link ui.TileLabel}.
   */
  void prev() {
    int newIndex;
    if (selectedIndex == 0 && mainFrame.gameMode == 0) {
      newIndex = LineConfig.presets.size() + Vertex.vertexPresets.size() + FloatingImage.fIPresets.size() - 2;
    } else if (selectedIndex == 0) {
      newIndex = LineConfig.presets.size() - 2;
    } else {
      newIndex = selectedIndex - 1;
    }
    this.setSelected(newIndex);
  }
  /**
   * sets the grab mode as "grab" is not a tile anymore
   */
  void setGrabMode() {
    for (Component l : this.getComponents()) {
      try {
        ((TileLabel) l).setBorder(UIManager.getBorder("Button.border"));
      } catch (Exception ex) {
        try {
          ((VertexLabel) l).setBorder(UIManager.getBorder("Button.border"));
        } catch (Exception e) {
          try {
            ((ImageLabel) l).setBorder(UIManager.getBorder("Button.border"));
          } catch (Exception ignored) {
          }
        }
      }
    }
    this.selected = LineConfig.presets.get(LineConfig.presets.size() - 1);
    this.selectedIndex = -1;
    System.gc();
    mainFrame.repaint();
  }
  /**
   * delete a config from the presets
   * @param index the index of the tile corresponding to the preset to be
   * deleted
   */
  public void deleteFromConfig(int index) {
    try {
      String type;
      if (index < LineConfig.presets.size() - 1) {
        type = "line";
      } else if (index < LineConfig.presets.size() - 1 + Vertex.vertexPresets.size()) {
        type = "vertex";
      } else {
        type = "image";
      }
      int selection = JOptionPane.showConfirmDialog(null, "Do you want to delete the " + type + " from the presets?", "Delete " + type, JOptionPane.OK_CANCEL_OPTION);

      if (selection == 0) {
        switch (type) {
          case "line":
            LineConfig.delete(index);
            break;
          case "vertex":
            Vertex.delete(index - LineConfig.presets.size() + 1);
            break;
          case "image":
            FloatingImage.delete(index - LineConfig.presets.size() + 1 - Vertex.vertexPresets.size());
            break;
        }
        mainFrame.reloadPresets();
        mainFrame.tileTabs.setCurrUnsaved();
        Frame.frame.requestFocus();
      }
    } catch (HeadlessException ex) {
      System.err.println("HeadlessException: " + ex);
    }
  }
  /**
   * Is used as an {@link ActionListener} for the {@link ui.TileLabel}.
   * right click: delete from config
   * left click: activate
   */
  class myListener implements ActionListener {
    private final int index;

    myListener(int i) {
      index = i;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (((e.getModifiers() & ActionEvent.SHIFT_MASK) != 0) && mainFrame.gameMode == 0) {
        TileSelect.this.deleteFromConfig(index);
      } else {
        TileSelect.this.setSelected(index);
        mainFrame.repaint();
      }
      Frame.frame.requestFocus();
    }
  }

  /**
   * Is used as an {@link ActionListener} for the {@link ui.TileLabel}.
   */
  class addListener implements ActionListener {
    addListener() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
        LineConfig.presets.remove(LineConfig.presets.size() - 1);

        LineConfig lc = new LineConfig(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig);
        LineConfig.presets.add(lc);

        Random r = new Random();
        String a = "";
        a += (char) (r.nextInt(26) + 'a');
        a += (char) (r.nextInt(26) + 'a');
        a += (char) (r.nextInt(26) + 'a');
        String ident1 = a.toLowerCase();
        String ident2 = a.toUpperCase();
        boolean isAlreadyUsed = true;
        while(isAlreadyUsed) {
          isAlreadyUsed = false;
          for (LineConfig lc1 : LineConfig.presets) {
            if (ident1.equals(lc1.identifier.getKey()) || ident1.equals(lc1.identifier.getValue()) || ident2.equals(lc1.identifier.getKey()) || ident2.equals(lc1.identifier.getValue())) {
              isAlreadyUsed= true;
              break;
            }
          }
          if (isAlreadyUsed) {
            a = "";
            a += (char) (r.nextInt(26) + 'a');
            a += (char) (r.nextInt(26) + 'a');
            a += (char) (r.nextInt(26) + 'a');
            ident1 = a.toLowerCase();
            ident2 = a.toUpperCase();
          }
        }
        lc.identifier = new AbstractMap.SimpleEntry<String, String>(ident1, ident2);
        LineConfig.presets.add(new LineConfig(LineType.GRAB, Color.BLACK));
      } else if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {
        Vertex.vertexPresets.add(new AbstractMap.SimpleEntry<>(new Vertex(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1)), ""));
      } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0) {
        if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
          Vertex.vertexPresets.add(new AbstractMap.SimpleEntry<>(new Vertex(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex), ""));
        } else {
          FloatingImage.fIPresets.add(new FloatingImage(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage));
        }
      }
      mainFrame.reloadPresets();
      mainFrame.tileTabs.setCurrUnsaved();
      Frame.frame.requestFocus();
    }
  }
}
