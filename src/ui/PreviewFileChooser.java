package ui;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

/**
 * A JfileChooser that can display a preview of the save file
 */
public class PreviewFileChooser extends JFileChooser {
  private static final long serialVersionUID = 1L;

  private static final int DEFAULT_WIDTH = 300;

  private final PreviewDrawPane pdp;
  
  public PreviewFileChooser() {
    this.pdp = setAccessoryImpl(DEFAULT_WIDTH);
    addSelectionChangeHandle();
  }

  public PreviewFileChooser(File currentDirectory, FileSystemView fsv) {
    super(currentDirectory, fsv);
    this.pdp = setAccessoryImpl(DEFAULT_WIDTH);
    addSelectionChangeHandle();
  }

  public PreviewFileChooser(File currentDirectory) {
    super(currentDirectory);
    this.pdp = setAccessoryImpl(DEFAULT_WIDTH);
    addSelectionChangeHandle();
  }

  public PreviewFileChooser(FileSystemView fsv) {
    super(fsv);
    this.pdp = setAccessoryImpl(DEFAULT_WIDTH);
    addSelectionChangeHandle();
  }

  public PreviewFileChooser(String currentDirectoryPath, FileSystemView fsv) {
    super(currentDirectoryPath, fsv);
    this.pdp = setAccessoryImpl(DEFAULT_WIDTH);
    addSelectionChangeHandle();
  }

  public PreviewFileChooser(String currentDirectoryPath) {
    super(currentDirectoryPath);
    this.pdp = setAccessoryImpl(DEFAULT_WIDTH);
    addSelectionChangeHandle();
  }

  private PreviewDrawPane setAccessoryImpl(int width) {
    JPanel acccon = new JPanel(new BorderLayout());
    PreviewDrawPane pdp = new PreviewDrawPane(300, Frame.frame);
    JLabel title = new JLabel("Save file preview");
    JPanel pad = new JPanel(new BorderLayout());
    title.setHorizontalAlignment(SwingConstants.CENTER);
    pad.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pad.add(pdp, BorderLayout.CENTER);
    acccon.add(pad, BorderLayout.CENTER);
    acccon.add(title, BorderLayout.NORTH);
    super.setAccessory(acccon);
    return pdp;
  }
  
  private void addSelectionChangeHandle() {
    addPropertyChangeListener(e -> {
      if(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(e.getPropertyName())) {
        File file = getSelectedFile();
        if(file == null) {
          pdp.clearPreview();
        } else {
          pdp.updatePreview(file);
        }
        pdp.repaint();
      } else if(JFileChooser.SELECTED_FILES_CHANGED_PROPERTY.equals(e.getPropertyName())) {
        File[] files = getSelectedFiles();
        if(files == null || files.length == 0) {
          pdp.clearPreview();
        } else {
          pdp.updatePreview(files[files.length - 1]); //show the last one
        }
        pdp.repaint();
      }
    });
  }

  @Override
  public void setAccessory(JComponent newAccessory) {
    throw new UnsupportedOperationException("Cannot change accessory on this file chooser");
  }

  public PreviewDrawPane getPreviewPane() {
    return pdp;
  }
  
  public void setPreviewWidth(int width) {
    pdp.setPreferredWidth(width);
  }  
}
