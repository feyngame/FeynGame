//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.RepaintManager;
/**
 * This class is used to print any component. Is used for printing the {@link ui.Frame#drawPane}.
 */

public class PrintUtilities implements Printable {
  private static final Logger LOGGER = Frame.getLogger(PrintUtilities.class);
  /**
   * the component that is printed
   */
  private Component componentToBePrinted;
  /**
   * the BoundingBoxCalculator that is potentially shown
   */
  private BoundingBoxCalculator bbCalc;
  /**
   * the bounds of the diagram
   */
  private Rectangle2D.Double boundingBox;
  /**
   * whether or not the whole canvas was printed
   */
  private boolean fullCanvas;
  /**
   * basic constructor: sets the component that will be printed
   * @param componentToBePrinted the component that will be printed
   */
  public PrintUtilities(Component componentToBePrinted, Rectangle boundingBox, boolean fullCanvas) {
    this.componentToBePrinted = componentToBePrinted;
    this.fullCanvas = fullCanvas;
    this.boundingBox = new Rectangle2D.Double((double) boundingBox.x, (double) boundingBox.y, (double) boundingBox.width, (double) boundingBox.height);
  }

  public static void printComponent(Component c, Rectangle boundingBox, boolean fullCanvas) {
    new PrintUtilities(c, boundingBox, fullCanvas).print();
  }

  public static void disableDoubleBuffering(Component c) {
    RepaintManager currentManager = RepaintManager.currentManager(c);
    currentManager.setDoubleBufferingEnabled(false);
  }

  public static void enableDoubleBuffering(Component c) {
    RepaintManager currentManager = RepaintManager.currentManager(c);
    currentManager.setDoubleBufferingEnabled(true);
  }

  public void print() {
    PrinterJob printJob = PrinterJob.getPrinterJob();
    printJob.setPrintable(this);
    if (printJob.printDialog())
      try {
        printJob.print();
      } catch (PrinterException pe) {
        LOGGER.log(Level.SEVERE, "Print error", pe);
      }
  }

  public int print(Graphics g, PageFormat pageFormat, int pageIndex) {
    if (pageIndex > 0) {
      return (NO_SUCH_PAGE);
    } else {
      Graphics2D g2d = (Graphics2D) g;

      Dimension dim = componentToBePrinted.getSize();
      double cHeight = dim.getHeight();
      double cWidth = dim.getWidth();

      double pHeight = pageFormat.getImageableHeight();
      double pWidth = pageFormat.getImageableWidth();

      double xyRatio = Math.min(pWidth / cWidth, pHeight / cHeight);

      g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
      g2d.scale(xyRatio, xyRatio);
      disableDoubleBuffering(componentToBePrinted);
      componentToBePrinted.print(g2d);
      enableDoubleBuffering(componentToBePrinted);

      if (bbCalc == null && Print.bbcalc) {
        boundingBox.x *= xyRatio;
        boundingBox.y *= xyRatio;
        boundingBox.width *= xyRatio;
        boundingBox.height *= xyRatio;
        bbCalc = new BoundingBoxCalculator(cWidth * xyRatio, cHeight * xyRatio, pageFormat, boundingBox, fullCanvas);
        bbCalc.showBB();
      }

      return (PAGE_EXISTS);
    }
  }
}
