//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.GridLayout;
import java.awt.Taskbar;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import resources.GetResources;
/**
 * after printing, this window shows data to properly crop the pdf in latex
 * with includegraphics
 *
 * @author Sven Yannick Klein
 */
public class BoundingBoxCalculator extends JDialog implements KeyListener {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 16L;
  /**
   * true if runs on macOs because of bug of FileChooser Look and Feel
   */
  private static boolean macOs;
  /*
   * the height of the whole page
   */
  public double height;
  /*
   * the width of the whole page
   */
  public double width;
  /*
   * the height of the rectangle that can be actually drawn on
   */
  public double ulx;
  /*
   * the width of the rectangle that can actually be drawn on
   */
  public double uly;
  /*
   * some variables storing the different margins and corrdinates etc.
   */
  public double llx, lly, urx, ury, dllx, dlly, durx, dury, ddurx, ddury;
  /**
   * whether or not the full canvas was printed
   */
  public boolean fullCanvas;
  /**
   * height and width of the component
   */
  public double widthComp, heightComp;
  public Rectangle2D.Double boundingBox;
  public PageFormat pf;
  /**
   * constructor for this class that sets the dimensions, calculates the
   * variables and builds the window
   * @param widthComp the width of the component that was printed
   * @param heightComp the height of the component that was printed
   * @param pf the page format of the page that was printed on
   * @param boundingBox the bounding box of the diagram itself
   * @param fullCanvas wether or not the full canvas was printed
   */
  public BoundingBoxCalculator(double widthComp, double heightComp, PageFormat pf, Rectangle2D.Double boundingBox, boolean fullCanvas) {
    this.setType(Window.Type.POPUP);

    this.widthComp = widthComp;
    this.heightComp = heightComp;
    this.boundingBox = boundingBox;
    this.fullCanvas = fullCanvas;
    this.pf = pf;
    
    height = pf.getHeight();
    width = pf.getWidth();
    ulx = pf.getImageableX();
    uly = pf.getImageableY();


    lly = height - uly - heightComp;
    llx = ulx;
    urx = ulx + widthComp;
    ury = lly + heightComp;

    dllx = llx;
    dlly = lly;
    durx = width - widthComp - ulx;
    dury = uly;

    ddurx = widthComp - boundingBox.x - boundingBox.width;
    ddury = heightComp - boundingBox.y - boundingBox.height;
    double tmp = ddury;
    ddury = boundingBox.y;
    boundingBox.y = tmp;

    JFrame.setDefaultLookAndFeelDecorated(true);

    this.setLayout(new GridLayout(0, 1, 5, 5));
    this.setTitle("Bounding Box");
    this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    if (fullCanvas)
      this.add(new JLabel("Parameters for the whole printed area:"));

    JPanel trimPane = new JPanel(new GridLayout(1, 0, 0, 0));
    JLabel trimLabel = new JLabel("\\includegraphics with trim: ");
    JTextField trimField = new JTextField("trim = " + (int) Math.round(dllx) + " " + (int) Math.round(dlly) + " " + (int) Math.round(durx) + " " + (int) Math.round(dury));
    trimField.setEditable(false);
    trimPane.add(trimLabel);
    trimPane.add(trimField);
    JButton trimButton = new JButton("Copy to Clipboard");
    trimButton.addActionListener(e -> {
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.setContents(new StringSelection("trim = " + (int) Math.round(dllx) + " " + (int) Math.round(dlly) + " " + (int) Math.round(durx) + " " + (int) Math.round(dury)), null);
    });
    trimPane.add(trimButton);
    this.add(trimPane);

    JPanel viewportPane = new JPanel(new GridLayout(1, 0, 0, 0));
    JLabel viewportLabel = new JLabel("\\includegraphics with viewport: ");
    JTextField viewportField = new JTextField("viewport = " + (int) Math.round(llx) + " " + (int) Math.round(lly) + " " + (int) Math.round(urx) + " " + (int) Math.round(ury));
    viewportField.setEditable(false);
    viewportPane.add(viewportLabel);
    viewportPane.add(viewportField);
    JButton viewportButton = new JButton("Copy to Clipboard");
    viewportButton.addActionListener(e -> {
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.setContents(new StringSelection("viewport = " + (int) Math.round(llx) + " " + (int) Math.round(lly) + " " + (int) Math.round(urx) + " " + (int) Math.round(ury)), null);
    });
    viewportPane.add(viewportButton);
    this.add(viewportPane);

    if (fullCanvas) {
      this.add(new JLabel("Parameters for the area the diagram is drawn on:"));

      JPanel trim2Pane = new JPanel(new GridLayout(1, 0, 0, 0));
      JLabel trim2Label = new JLabel("\\includegraphics with trim: ");
      JTextField trim2Field = new JTextField("trim = " + (int) Math.round(dllx + boundingBox.x) + " " + (int) Math.round(dlly + boundingBox.y) + " " + (int) Math.round(durx + ddurx) + " " + (int) Math.round(dury + ddury));
      trim2Field.setEditable(false);
      trim2Pane.add(trim2Label);
      trim2Pane.add(trim2Field);
      JButton trim2Button = new JButton("Copy to Clipboard");
      trim2Button.addActionListener(e -> {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(new StringSelection("trim = " + (int) Math.round(dllx + boundingBox.x) + " " + (int) Math.round(dlly + boundingBox.y) + " " + (int) Math.round(durx + ddurx) + " " + (int) Math.round(dury + ddury)), null);
      });
      trim2Pane.add(trim2Button);
      this.add(trim2Pane);

      JPanel viewport2Pane = new JPanel(new GridLayout(1, 0, 0, 0));
      JLabel viewport2Label = new JLabel("\\includegraphics with viewport: ");
      JTextField viewport2Field = new JTextField("viewport = " + (int) Math.round(llx + boundingBox.x) + " " + (int) Math.round(lly + boundingBox.y) + " " + (int) Math.round(urx - ddurx) + " " + (int) Math.round(ury - ddury));
      viewport2Field.setEditable(false);
      viewport2Pane.add(viewport2Label);
      viewport2Pane.add(viewport2Field);
      JButton viewport2Button = new JButton("Copy to Clipboard");
      viewport2Button.addActionListener(e -> {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(new StringSelection("viewport = " + (int) Math.round(llx + boundingBox.x) + " " + (int) Math.round(lly + boundingBox.y) + " " + (int) Math.round(urx - ddurx) + " " + (int) Math.round(ury - ddury)), null);
      });
      viewport2Pane.add(viewport2Button);
      this.add(viewport2Pane);
    }

    JButton close = new JButton("Close");
    close.addActionListener(e -> {
      this.dispose();
    });
    this.add(close);
  }

  public void printBB() {
    System.out.println("Parameters for \"\\includegraphics\":");
    if (fullCanvas)
      System.out.println("\n\nParameters for the whole printed area:");

    System.out.println("\n\"\\includegraphics\" with trim:");
    System.out.println("trim = " + (int) Math.round(dllx) + " " + (int) Math.round(dlly) + " " + (int) Math.round(durx) + " " + (int) Math.round(dury));

    System.out.println("\n\"\\includegraphics\" with viewport:");
    System.out.println("viewport = " + (int) Math.round(llx) + " " + (int) Math.round(lly) + " " + (int) Math.round(urx) + " " + (int) Math.round(ury));

    if (fullCanvas) {
      System.out.println("\n\nParameters for the area the diagram is drawn on:");

      System.out.println("\n\"\\includegraphics\" with trim:");
      System.out.println("trim = " + (int) Math.round(dllx + boundingBox.x) + " " + (int) Math.round(dlly + boundingBox.y) + " " + (int) Math.round(durx + ddurx) + " " + (int) Math.round(dury + ddury));

      System.out.println("\n\"\\includegraphics\" with viewport:");
      System.out.println("viewport = " + (int) Math.round(llx + boundingBox.x) + " " + (int) Math.round(lly + boundingBox.y) + " " + (int) Math.round(urx - ddurx) + " " + (int) Math.round(ury - ddury));
    }
  }

  public String getViewportString() {
    if (fullCanvas)
      return "viewport = " + (int) Math.round(llx + boundingBox.x) + " " + (int) Math.round(lly + boundingBox.y) + " " + (int) Math.round(urx - ddurx) + " " + (int) Math.round(ury - ddury);
    else
      return "viewport = " + (int) Math.round(llx) + " " + (int) Math.round(lly) + " " + (int) Math.round(urx) + " " + (int) Math.round(ury);
  }

  public void showBB() {
    if (!this.isVisible() && Frame.GUI) {
      this.requestFocus();
      this.addKeyListener(this);
      this.pack();
      this.setLocationRelativeTo(null);
      this.setAlwaysOnTop(true);
      this.setVisible(true);
    }
  }

  @Override
  public void keyTyped(KeyEvent e) {
  }

  /**
   * Implements all keys from the this window (Ctrl + C).
   *
   * @param e {@link KeyEvent}
   */
  @Override
  public void keyPressed(KeyEvent e) {
    switch (e.getKeyCode()) {
      case KeyEvent.VK_C:
        if ((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) {
          Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
          clipboard.setContents(new StringSelection("trim = " + (int) Math.round(dllx) + " " + (int) Math.round(dlly) + " " + (int) Math.round(durx) + " " + (int) Math.round(dury)), null);
        }
        break;
    }
  }

  @Override
  public void keyReleased(KeyEvent e) {
  }
}
