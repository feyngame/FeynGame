//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import resources.GetResources;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import java.awt.*;
import java.awt.event.*;
import java.awt.Taskbar;

/**
 * The Frame that holds the EditPane for editing the selected object
 *
 * @author Sven Yannick Klein
 */
public class EditFrame extends JFrame {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 7L;
  /**
   * the edit frame ({@link ui.EditPane}) that holds all the components to modify the selected
   * object
   */
  public EditPane editPane;
  /**
   * the constructor: initalizes the component to edit the momentum arrow
   * as well as the editPane with wich the other properties of an Object
   * can be edited
   * @param mainFrame the mainFrame ({@link ui.Frame})
   */
  public EditFrame(Frame mainFrame, boolean alwaysOnTop) {
    super();

    this.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_E) {
            EditFrame.this.dispatchEvent(new WindowEvent(EditFrame.this, WindowEvent.WINDOW_CLOSING));
        }
      }
    });
    
    this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    
    MomArrowConf momArrowConfig = new MomArrowConf(mainFrame);
    this.setLayout(new BorderLayout());

    editPane = new EditPane(mainFrame, momArrowConfig);
    this.add(editPane, BorderLayout.CENTER);
    this.add(momArrowConfig, BorderLayout.SOUTH);
    this.setTitle("EditFrame");
    this.setSize(new Dimension(410, 400));
    this.pack();
    this.setLocationRelativeTo(null);
    this.setResizable(true);
    this.setAlwaysOnTop(alwaysOnTop);

    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    this.addWindowListener(new java.awt.event.WindowAdapter() {
      @Override
      public void windowClosing(java.awt.event.WindowEvent windowEvent) {
        mainFrame.derefEframe();
        mainFrame.requestFocus();
      }
    });
    requestFocusInWindow();
  }
}
