//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Taskbar;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import resources.GetResources;

/**
 * the frame for the help pages
 *
 * @author Sven Yannick Klein
 */
public class HelpPages extends JFrame implements TreeSelectionListener, TreeExpansionListener {
  private static final Logger LOGGER = Frame.getLogger(HelpPages.class);
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 22L;
  /**
   * this holds the structure tree for selecting differnt pages
   */
  JTree treeView;
  /**
   * this holds the text of the current page
   */
  JEditorPane page;
  /**
   * this is the tree strucure
   */
  ArrayList<DefaultMutableTreeNode> treeHirarchy = new ArrayList<>();
  /**
   * holds the {@link treeView}
   */
  JScrollPane treeScroll;
  /**
   * constructor: initializes all components and lays them out
   */
  public HelpPages() {
    super();

    /* basic look of the window */

    this.setTitle("Help");
    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    /* initialize the treeview */

    treeView = new JTree();

    treeView.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    this.initTree();

    treeView.addTreeSelectionListener(this);
    treeView.addTreeExpansionListener(this);

    treeScroll = new JScrollPane(treeView);

    /* construct the main page */

    page = new JEditorPane();
    page.setEditable(false);

    StyleSheet styleSheet = new StyleSheet();
    styleSheet.addRule("kbd {border-radius: 3px;padding: 5px 5px 5px;border: 1px outset #D3D3D3;box-shadow: 0px 1px #888888;font-family: \"Lucida Console\", Monaco, monospace;}");
    styleSheet.addRule("* {font-family: sans-serif;}");
    styleSheet.addRule("table {border-collapse: collapse;width : 100%;}");
    styleSheet.addRule("td {border: 1px solid #D3D3D3;padding: 15px;}");
    styleSheet.addRule("th {text-align: left;border: 1px solid #D3D3D3;padding: 15px;font-weight: normal;background-color : #F8F8F8;color: #2e2e2e;border-bottom: 2px solid #D3D3D3;}");
    styleSheet.addRule("h2 {display: block;font-size: 1.5em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h3 {display: block;font-size: 1.17em;margin-top: 1em;margin-bottom: 1em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h4 {display: block;font-size: 1em;margin-top: 1.33em;margin-bottom: 1.33em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h1 {display: block;font-size: 2em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");

    EditorKit kit = JEditorPane.createEditorKitForContentType("text/html");
    ((HTMLEditorKit) kit).setStyleSheet(styleSheet);
    HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();
    page.setEditorKit(kit);
    page.setDocument(doc);

    /* this allows for links to other pages as well as to webpages */
    page.addHyperlinkListener(e -> {
      if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
        if (Desktop.isDesktopSupported()) {
          try {
            if (e.getDescription().startsWith("http")) {
              Desktop.getDesktop().browse(e.getURL().toURI());
            } else {
              showPage(e.getDescription());
              treeView.setSelectionPath(null);
            }
          } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Could not open " + e.getDescription() + ".\n", ex);
          }
        }
      }
    });

    showPage("Help");
    JScrollPane pageScroll = new JScrollPane(page);

    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, treeScroll, pageScroll);

    splitPane.setOneTouchExpandable(true);
    splitPane.setDividerLocation(150);

    //Provide minimum sizes for the two components in the split pane
    Dimension minimumSize = new Dimension(100, 50);
    treeScroll.setMinimumSize(minimumSize);
    pageScroll.setMinimumSize(minimumSize);

    this.add(splitPane);

    this.setSize(new Dimension(1000, 500));
    this.setLocationRelativeTo(null);
    this.setVisible(true);
  }
  /**
   * initializes the tree strucure form the Treeview.xml file
   */
  private void initTree() {
    InputStream in = GetResources.class.getResourceAsStream("helppages" + "/" + "TreeView.xml");
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    StringBuilder htmlText = new StringBuilder();
    String line;
    try {
      while ((line = reader.readLine()) != null) {
        line = line.replaceAll("\\t", "");
        line = line.replaceAll("\\s", "");
        if (line.startsWith("</")) {
          treeHirarchy.remove(treeHirarchy.size() - 1);
        } else if (line.startsWith("<") && !line.startsWith("<?")) {
          line = line.replaceAll("<", "");
          line = line.replaceAll(">", "");
          DefaultMutableTreeNode newTreeNode = new DefaultMutableTreeNode(line);
          if (treeHirarchy.size() >= 1) {
            treeHirarchy.get(treeHirarchy.size() - 1).add(newTreeNode);
            treeHirarchy.add(newTreeNode);
          } else {
            treeView = new JTree(newTreeNode);
            treeHirarchy.add(newTreeNode);
          }
        }
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
  /**
   * sets the content of the currently shown page
   * @param pageID the name of the file to set the content from
   */
  private void showPage(String pageID) {

    boolean debug = false;
    pageID += ".html";
    InputStream in = GetResources.class.getResourceAsStream("helppages" + "/" + pageID);
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    StringBuilder htmlText = new StringBuilder();
    String line;
    try {
      while ((line = reader.readLine()) != null) {

        if (debug) System.out.println("reading line: " + line);

        if (line.contains("<img src=\"")) {

          if (debug) System.out.println("Line contains image!");
          String[] segs = line.split("<img src=\"");
          for (String seg : segs) {
            if (debug) System.out.println("Reading segment: " + seg);
            if (seg.contains("\"/>")) {
              if (debug) System.out.println("segment contains image!");
              String[] subSegs = seg.split("\"/>", 2);
              if (debug) System.out.println("image: " + subSegs[0]);
              String src = GetResources.class.getResource("helppages" + System.getProperty("file.separator") + subSegs[0]).toString();
              htmlText.append("<img src=\"" + src + "\"/>");
              if (subSegs.length > 1) {
                htmlText.append(subSegs[1]);
                if (debug) System.out.println(src + subSegs[1]);
              }
            } else {
              htmlText.append(seg);
              if (debug) System.out.println(seg);
            }

          }
        } else {
          htmlText.append(line);
        }
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    if (debug) System.out.println("Whole page: \n" + htmlText.toString());
    page.setText(htmlText.toString());
  }

  /* Required by TreeSelectionListener interface. */
  /**
   * listens to a node of the tree being selected and sets the contents of
   * the window to the page associated with the node
   * @param e the event
   */
  public void valueChanged(TreeSelectionEvent e) {

    DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeView.getLastSelectedPathComponent();

    /* if nothing is selected */
    if (node == null) return;

    /* retrieve the node that was selected */
    Object nodeInfo = node.getUserObject();

    showPage(nodeInfo.toString());
    treeScroll.setSize(treeView.getPreferredSize());
    revalidate();
    repaint();
  }
  // Required by TreeExpansionListener interface.
  /**
   * listens to expansion events of the treeview and resizes the components
   * of the window
   * @param e the event
   */
  public void treeExpanded(TreeExpansionEvent e) {
    treeScroll.setSize(treeView.getPreferredSize());
    revalidate();
    repaint();
  }

  // Required by TreeExpansionListener interface.
  /**
   * listens to collapse events of the treeview and resizes the components
   * of the window
   * @param e the event
   */
  public void treeCollapsed(TreeExpansionEvent e) {
    treeScroll.setSize(treeView.getPreferredSize());
    revalidate();
    repaint();
  }

}
