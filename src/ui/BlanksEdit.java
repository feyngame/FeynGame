//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.Taskbar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;

import game.Blank;
import game.FeynLine;
import resources.GetResources;

/**
 * The Frame that holds the EditPane for editing the selected object
 *
 * @author Sven Yannick Klein
 */
public class BlanksEdit extends JFrame {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 8L;
  /**
   * the settings panel where one can set a specific blank
   */
  JPanel settingsPanel;
  /**
   * the different numbermodels for the JSpinners
   */
  SpinnerNumberModel lengthModel;
  SpinnerNumberModel posModel;
  /**
   * the JSpinners
   */
  JSpinner lengthSpinner;
  JSpinner posSpinner;
  /**
   * the selected line
   */
  private FeynLine fl;
  /**
   * wether or not this frame is updating
   */
  private boolean updating = false;
  /**
   * the index of the selected blank
   */
  private int selected = 0;
  /**
   * the main frame
   */
  private Frame mainFrame;
  /*
   * the constructor: initalizes the component to edit the momentum arrow
   * as well as the editPane with wich the other properties of an Object
   * can be edited
   * @param mainFrame the mainFrame ({@link ui.Frame})
   */
  public BlanksEdit(Frame mainFrame) {
    super();

    this.mainFrame = mainFrame;

    settingsPanel = new JPanel();
    settingsPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    settingsPanel.setAlignmentX(LEFT_ALIGNMENT);
    settingsPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

    JPanel lengthPanel = new JPanel();
    lengthPanel.add(new JLabel(" Length relative to line: "));
    lengthPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    lengthPanel.setAlignmentX(LEFT_ALIGNMENT);
    lengthPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    lengthModel = new SpinnerNumberModel(.1d, 0d, 1d, .05d);
    lengthSpinner = new JSpinner(lengthModel);
    lengthSpinner.setEditor(new JSpinner.NumberEditor(lengthSpinner, "0.00"));
    lengthSpinner.addChangeListener(e -> {
      if (!updating) {
        double value = Double.parseDouble(lengthSpinner.getValue().toString());
        if (fl.blanksUnsorted.get(selected).getLength() != value)
          mainFrame.saveForUndo();
        fl.blanksUnsorted.get(selected).setLength(value);
        fl.sortBlanks();
      }
    });
    lengthPanel.add(lengthSpinner, LEFT_ALIGNMENT);
    lengthPanel.setAlignmentX(LEFT_ALIGNMENT);
    settingsPanel.add(lengthPanel, LEFT_ALIGNMENT);

    JPanel posPanel = new JPanel();
    posPanel.add(new JLabel(" Position: "));
    posPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
    posPanel.setAlignmentX(LEFT_ALIGNMENT);
    posPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    posModel = new SpinnerNumberModel(.1d, 0d, 1d, .05d);
    posSpinner = new JSpinner(posModel);
    posSpinner.setEditor(new JSpinner.NumberEditor(posSpinner, "0.00"));
    posSpinner.addChangeListener(e -> {
      if (!updating) {
        double value = Double.parseDouble(posSpinner.getValue().toString());
        if (fl.blanksUnsorted.get(selected).getPartOfPath() != value)
          mainFrame.saveForUndo();
        fl.blanksUnsorted.get(selected).setPartOfPath(value);
        fl.sortBlanks();
      }
    });
    posPanel.add(posSpinner, LEFT_ALIGNMENT);
    posPanel.setAlignmentX(LEFT_ALIGNMENT);
    settingsPanel.add(posPanel, LEFT_ALIGNMENT);


    this.update();

    this.setLocationRelativeTo(null);
    this.setResizable(false);
    this.setVisible(true);

    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    this.addWindowListener(new java.awt.event.WindowAdapter() {
      @Override
      public void windowClosing(java.awt.event.WindowEvent windowEvent) {
        mainFrame.derefBEframe();
        mainFrame.requestFocus();
      }
    });
  }
  /**
   * updated the whole frame (when a new Blank/FeynLine is selected)
   */
  public void update() {

    for (Component comp : this.getContentPane().getComponents()) {
      this.remove(comp);
    }

    if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {

      this.fl = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);

      if (selected >= fl.blanksUnsorted.size())
        selected = 0;

      this.setLayout(new BorderLayout());
      Color bgColor = UIManager.getColor("Panel.background");

      JPanel blanks = new JPanel();
      blanks.setBackground(bgColor.darker());
      blanks.setOpaque(true);
      blanks.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
      blanks.setAlignmentX(LEFT_ALIGNMENT);
      blanks.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

      NewTabButton nTab = new NewTabButton();
      nTab.addActionListener(new AddListener());
      nTab.setSize(nTab.getPreferredSize());
      nTab.setBackground(bgColor.darker());

      for (int i = 1; i <= fl.blanksUnsorted.size(); i += 1) {
        String title = "Blank " + String.valueOf(i);
        TabButton button = new TabButton(title, (int) blanks.getPreferredSize().getHeight());
        if (i - 1 != selected)
          button.setBackground(bgColor.darker());
        else
          button.setBackground(bgColor);

        button.addActionListener(new SelectListener(i-1));
        button.close.addActionListener(new RemoveListener(i-1));
        button.setSize(button.getPreferredSize());
        blanks.add(button, LEFT_ALIGNMENT);
      }

      blanks.add(nTab, LEFT_ALIGNMENT);
      this.add(blanks, BorderLayout.NORTH);

      if (fl.blanksUnsorted.size() > 0) {
        lengthSpinner.setEnabled(true);
        posSpinner.setEnabled(true);

        lengthSpinner.setValue(fl.blanksUnsorted.get(selected).getLength());
        posSpinner.setValue(fl.blanksUnsorted.get(selected).getPartOfPath());
      } else {
        lengthSpinner.setEnabled(false);
        posSpinner.setEnabled(false);
      }

      this.add(settingsPanel);

    } else {
      this.add(new JLabel("Select a line"));
    }
    this.pack();
    mainFrame.requestFocus();
    this.getContentPane().validate();
    this.getContentPane().repaint();
  }
  /**
   * Is used as an {@link ActionListener} for the buttons to select a blank
   */
  class SelectListener implements ActionListener {
    private final int index;

    SelectListener(int index) {
      this.index = index;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      selected = index;
      update();
      Frame.frame.requestFocus();
    }
  }
  /**
   * Is used as an {@link ActionListener} for the close buttons of the buttons to select a blank
   */
  class RemoveListener implements ActionListener {
    private final int index;

    RemoveListener(int index) {
      this.index = index;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (selected == index)
        selected = index - 1;
      else if (selected > index)
        selected -= 1;

      if (selected == -1)
        selected = 0;

      fl.blanksUnsorted.remove(index);
      fl.sortBlanks();
      update();
      Frame.frame.requestFocus();
    }
  }
  /**
   * Is used as an {@link ActionListener} for the button to add a new blank
   */
  class AddListener implements ActionListener {

    AddListener() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      fl.blanksUnsorted.add(new Blank());
      selected = fl.blanksUnsorted.size() - 1;
      fl.sortBlanks();
      update();
      Frame.frame.requestFocus();
    }
  }
}
