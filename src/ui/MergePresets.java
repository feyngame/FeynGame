//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Taskbar;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import game.Presets;
import resources.GetResources;
/**
 * this class opens a window that lets one select two of the currently opened models and merge them to a new model
 */
public class MergePresets extends JFrame {
  /**
   * maps the checkboxes to the correct identifiers for the presets
   */
  private HashMap<JCheckBox, String> checkToPresetName;
  /**
   * the main frame
   */
  private Frame mainFrame;
  /**
   * the function to open the window
   * @param mainFrame the mainFrame
   */
  public MergePresets(Frame mainFrame) {
    this.setAlwaysOnTop(false);
    this.setResizable(false);

    this.mainFrame = mainFrame;

    /* look of the window */

    this.setTitle("Export Feynman diagram");

    this.setIconImage(GetResources.getAppIcon());
    try {
        final Taskbar taskbar = Taskbar.getTaskbar();
        taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    this.setLayout(new GridLayout(0, 1));
    this.add(new JLabel(" Select the models to merge into a new model "));

    checkToPresetName = new HashMap<>();

    for (Map.Entry<String, Presets> entry : mainFrame.models.entrySet()) {
      String titleShort = entry.getKey();
      //BUGFIX: on windows, the separator is \, which is an invalid regex
      String separator = System.getProperty("file.separator");
      if("\\".equals(separator)) separator = "\\\\";
      titleShort = titleShort.split(separator)[titleShort.split(separator).length - 1];
      titleShort = titleShort.substring(0, titleShort.length() - 6);
      checkToPresetName.put(new JCheckBox(titleShort), entry.getKey());
    }

    for (Map.Entry<JCheckBox, String> entry : this.checkToPresetName.entrySet()) {
      this.add(entry.getKey());
    }

    JPanel buttonPanel = new JPanel(new FlowLayout());

    JButton ok = new JButton("Merge models");
    ok.addActionListener(e -> {
      ArrayList<String> modelsToMerge = new ArrayList<>();
      for (Map.Entry<JCheckBox, String> entry : checkToPresetName.entrySet()) {
        if (entry.getKey().isSelected()) {
          modelsToMerge.add(entry.getValue());
        }
      }
      if (modelsToMerge.size() < 2) {
        this.setVisible(false);
        JOptionPane.showMessageDialog(mainFrame, "Please select at least two models", "Warning", JOptionPane.WARNING_MESSAGE);
        this.setVisible(true);
        return;
      }
      Presets preset = new Presets(mainFrame.models.get(modelsToMerge.get(0)));

      for (int i = 1; i < modelsToMerge.size(); i++) {
        preset.merge(mainFrame.models.get(modelsToMerge.get(i)));
      }
      mainFrame.tileTabs.addModel(preset, "");

      mainFrame.tileTabs.update();

      this.setVisible(false);
    });
    buttonPanel.add(ok);

    JButton cancel = new JButton("Cancel");
    cancel.addActionListener(e -> {
      this.setVisible(false);
    });
    buttonPanel.add(cancel);

    this.add(buttonPanel);

    this.pack();
    this.setLocationRelativeTo(null);
    this.setVisible(true);
  }
}
