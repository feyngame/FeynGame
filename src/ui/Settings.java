//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Taskbar;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterators;
import java.util.Vector;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.DoublePredicate;
import java.util.function.DoubleSupplier;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.StreamSupport;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import layout.GraphLayoutAlgorithm;
import qgraf.LoadedQgrafStyle;
import qgraf.ModelData;
import qgraf.QgrafImportException;
import qgraf.QgrafImportManager;
import qgraf.QgrafStyleFile;
import resources.GetResources;
import resources.Java8Support;

/**
 * the frame for the help pages
 *
 * @author Sven Yannick Klein
 */
public class Settings extends JFrame implements TreeSelectionListener, TreeExpansionListener, WindowFocusListener {
  private static final Logger LOGGER = Frame.getLogger(Settings.class);
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 20L;

  public static final String LAYOUT_UI_TITLE = "Layout Options";
  public static final String QGRAF_UI_TITLE  = "Qgraf Options";
  public static final String ROOT_UI_TITLE  = "Settings";
  private static final String LATEX_UI_TITLE  = "LaTeX Options";
  private static final String FEYNGAME_UI_TITLE  = "Feyngame Options";

  //To be run when thsi setting window gains focus
  private List<Runnable> onFocusGained = new ArrayList<>();

  private final DefaultListModel<LoadedQgrafStyle> qgrafStyleListModel;

   private final Frame frame;
  /**
   * this holds the structure tree for selecting different pages
   */
  private final JTree treeView;
  /**
   * Contains all settings pages
   */
  private final JPanel pageContainer;
  /**
   * this holds all pages in a card layout
   */
  private final CardLayout pageContainerLayout;
  /**
   * holds the {@link treeView}
   */
  private final JScrollPane treeScroll;
  /**
   * constructs the frame
   * @param frame The main {@link Frame} of the application
   */
  public Settings(Frame frame, boolean alwaysOnTop) {
    super();
    this.frame = frame;
    this.qgrafStyleListModel = new DefaultListModel<>();

    /* basic look of the window */

    this.setTitle(ROOT_UI_TITLE);
    this.setAlwaysOnTop(alwaysOnTop);
    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    /* initialize the treeview */

    DefaultMutableTreeNode root = initTree();
    treeView = new JTree(root);
    treeView.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    treeView.addTreeSelectionListener(this);
    treeView.addTreeExpansionListener(this);

    treeScroll = new JScrollPane(treeView);

    pageContainerLayout = new CardLayout();
    pageContainer = new JPanel(pageContainerLayout);
    pageContainer.add(wrapInScrollPane(createRootPanel()), ROOT_UI_TITLE);
    pageContainer.add(wrapInScrollPane(createFeynGamePanel()), FEYNGAME_UI_TITLE);
    if(Frame.gameMode == 0) {
      pageContainer.add(wrapInScrollPane(createQgrafPanel()), QGRAF_UI_TITLE);
      pageContainer.add(wrapInScrollPane(createLayoutPanel()), LAYOUT_UI_TITLE);
    }
    pageContainer.add(wrapInScrollPane(createLatexPanel()), LATEX_UI_TITLE);
    pageContainerLayout.show(pageContainer, ROOT_UI_TITLE);

//    JScrollPane pageScroll = new JScrollPane(pageContainer);
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, treeScroll, pageContainer);
    splitPane.setOneTouchExpandable(true);
    splitPane.setDividerLocation(150);

    //Provide minimum sizes for the two components in the split pane
    Dimension minimumSize = new Dimension(100, 50);
    treeScroll.setMinimumSize(minimumSize);
    pageContainer.setMinimumSize(minimumSize);

    this.add(splitPane);
    this.addWindowFocusListener(this);
    this.pack();
    this.setSize(new Dimension(800, 500));

    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE); //Every time the menu is clicked, a new instance is created
  }

  private JComponent wrapInScrollPane(JComponent page) {
    JPanel scrollCon = new JPanel(new BorderLayout());
    JScrollPane scrollPane = new JScrollPane(page);
    scrollCon.add(scrollPane, BorderLayout.CENTER);
    return scrollCon;
  }

  private static interface DocumentChangeListener extends DocumentListener {
    public void textChanged();
    @Override public default void insertUpdate(DocumentEvent e) {
      textChanged();
    }
    @Override public default void removeUpdate(DocumentEvent e) {
      textChanged();
    }
    @Override public default void changedUpdate(DocumentEvent e) {
      textChanged();
    }
  }

  /**
   * Uses the current locale to parse numbers with the correct decimal separator,
   * but unlike {@link NumberFormat#parse(String)} the entire string has to be a valid number.
   * Trailing whitespace will be removed.
   * @param value The string to parse
   * @return The parsed number
   * @throws NumberFormatException If the string is not a valid number
   */
  private static double parseExactDouble(String value) throws NumberFormatException {
    String stripped = Java8Support.stripTrailing(value);
    ParsePosition pos = new ParsePosition(0);
    Number number = NumberFormat.getNumberInstance().parse(stripped, pos);
    if(pos.getErrorIndex() == -1 && pos.getIndex() == stripped.length()) {
      return number.doubleValue();
    } else {
      throw new NumberFormatException();
    }
  }

  public static JComponent createIntegerInput(IntPredicate isValid, IntConsumer set, IntSupplier get,
      Consumer<Runnable> onUpdate, int preferredWidth) {
    JTextField text = new JTextField();
    JPanel border = new JPanel();
    text.getDocument().addDocumentListener(new DocumentChangeListener() {
      @Override public void textChanged() {
        try {
          int number = Integer.parseInt(text.getText());
          if(isValid.test(number)) {
            border.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            set.accept(number);
          } else {
            border.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
          }
        } catch (NumberFormatException e) {
          border.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
        }
      }
    });
    text.setPreferredSize(new Dimension(preferredWidth, text.getPreferredSize().height));
    border.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
    border.add(text);

    if(onUpdate != null)
      onUpdate.accept(() -> text.setText(Integer.toString(get.getAsInt())));

    return border;
  }

  public static JComponent createBooleanInput(String label, Consumer<Boolean> set,
      Supplier<Boolean> get, Consumer<Runnable> onUpdate, int preferredWidth) {
    JCheckBox box = new JCheckBox(label);
    box.addActionListener(e -> set.accept(box.isSelected()));
    box.setPreferredSize(new Dimension(preferredWidth, box.getPreferredSize().height));
    if(onUpdate != null)
      onUpdate.accept(() -> box.setSelected(get.get()));
    return box;
  }

  public static JComponent createDoubleInput(DoublePredicate isValid, DoubleConsumer set,
      DoubleSupplier get, Consumer<Runnable> onUpdate, int preferredWidth) {
    JTextField text = new JTextField();
    JPanel border = new JPanel();
    text.getDocument().addDocumentListener(new DocumentChangeListener() {
      @Override public void textChanged() {
        try {
          double number = parseExactDouble(text.getText());
          if(isValid.test(number)) {
            border.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            set.accept(number);
          } else {
            border.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
          }
        } catch (NumberFormatException e) {
          border.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
        }
      }
    });
    text.setPreferredSize(new Dimension(preferredWidth, text.getPreferredSize().height));
    border.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
    border.add(text);

    if(onUpdate != null)
      onUpdate.accept(() -> text.setText(NumberFormat.getNumberInstance().format(get.getAsDouble())));

    return border;
  }

  /**
   * initializes the tree strucure form the Treeview.xml file
   */
  private DefaultMutableTreeNode initTree() {
    DefaultMutableTreeNode root = new DefaultMutableTreeNode(ROOT_UI_TITLE);

    DefaultMutableTreeNode feynGameTreeNode = new DefaultMutableTreeNode(FEYNGAME_UI_TITLE);
    DefaultMutableTreeNode qgrafTreeNode  = new DefaultMutableTreeNode(QGRAF_UI_TITLE);
    DefaultMutableTreeNode layoutTreeNode = new DefaultMutableTreeNode(LAYOUT_UI_TITLE);
    DefaultMutableTreeNode latexTreeNode = new DefaultMutableTreeNode(LATEX_UI_TITLE);
    
    root.add(feynGameTreeNode);
    if(Frame.gameMode == 0) {
      root.add(qgrafTreeNode);
      root.add(layoutTreeNode);
    }
    root.add(latexTreeNode);

    return root;
  }
  /**
   * sets the currently shown page
   * @param pageID the name of the file to set the content from
   */
  @SuppressWarnings("unchecked")
  public void setShownPage(String pageID, boolean setTreePath) {
    if(setTreePath) {
      Enumeration<TreeNode> children = ((DefaultMutableTreeNode) treeView.getModel().getRoot()).children();
      Optional<DefaultMutableTreeNode> t =
          StreamSupport.stream(Spliterators.spliteratorUnknownSize(Java8Support.toIterator(children), 0), false)
          .map(x -> (DefaultMutableTreeNode) x)
          .filter(x -> Objects.equals(x.getUserObject(), pageID))
          .findAny()
          .map(x -> (DefaultMutableTreeNode) x);
      if(t.isPresent()) treeView.setSelectionPath(new TreePath(t.get().getPath()));
    }
    
    pageContainerLayout.show(pageContainer, pageID);
  }

  private JPanel createRootPanel() {
    JPanel page = new JPanel();
    page.add(new JLabel("FeynGame Settings"));
    return page;
  }

  public static GridBagConstraints gbc(int col, int row, int width, int fill) {
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridx = col;
    gbc.gridy = row;
    gbc.gridheight = 1;
    gbc.gridwidth = width;
    gbc.fill = fill;
    gbc.insets = new Insets(2, 5, 2, 5);
    return gbc;
  }

  private JComponent createFeynGamePanel() {
    JPanel page = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel topLevelContainer = new JPanel(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridx = 0;
    gbc.insets = new Insets(5, 0, 5, 0);

    JCheckBox alwaysOnTop = new JCheckBox("Dialog windows are always on top of the main window", frame.dialogFramesAlwaysOnTop);
    topLevelContainer.add(alwaysOnTop, gbc);
    alwaysOnTop.addActionListener(e -> {
      frame.updateAlwaysOnTop(alwaysOnTop.isSelected());
    });
    alwaysOnTop.setEnabled(Toolkit.getDefaultToolkit().isAlwaysOnTopSupported());
    
    JCheckBox resetExportDestination = new JCheckBox("Reset export destination when operning a .fg file", frame.resetExportDestinationOnLoad);
    topLevelContainer.add(resetExportDestination, gbc);
    resetExportDestination.addActionListener(e -> {
      frame.resetExportDestinationOnLoad = resetExportDestination.isSelected();
    });
    
    page.add(topLevelContainer);
    return page;
  }
  
  public void updatePresets() {
    int i = GraphLayoutAlgorithm.getSelectedPresetIndex();
    Collection<GraphLayoutAlgorithm> algs = GraphLayoutAlgorithm.getLayoutAlgorithms();
    presetsBox.setModel(new DefaultComboBoxModel<>(new Vector<>(algs)));
    if(!algs.isEmpty()) presetsBox.setSelectedIndex(i);
    
    List<Runnable> ofg = new ArrayList<>();
    createPresetsCards(presetsPanelLayout, presetsPanel, algs, ofg::add);
    onFocusGained.remove(nestedRunnable);
    nestedRunnable = () -> ofg.forEach(Runnable::run);
    onFocusGained.add(nestedRunnable);
    
    presetsPanelLayout.show(presetsPanel, Integer.toString(i));
    onFocusGained.forEach(Runnable::run);
    removePreset.setEnabled(algs.size() > 1);
  }
  
  private JPanel presetsPanel;
  private CardLayout presetsPanelLayout;
  private Runnable nestedRunnable;
  private JComboBox<GraphLayoutAlgorithm> presetsBox;
  private JButton removePreset;
  private int layoutStrutWidth;
  
  private JComponent createLayoutPanel() {
    JPanel page = new JPanel(new FlowLayout(FlowLayout.LEFT));

    JPanel topLevelContainer = new JPanel(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridx = 0;
    gbc.insets = new Insets(5, 0, 5, 0);
    
    for(JComponent comp : GraphLayoutAlgorithm.createGeneralOptions(onFocusGained::add)) {
      topLevelContainer.add(comp, gbc);
    }
    
    //add the rest to topLevelContainer
    JLabel l = new JLabel("Configure the layout presets:");
    l.setHorizontalAlignment(JLabel.CENTER);
    topLevelContainer.add(l, gbc);
    presetsBox = new JComboBox<>();
    presetsBox.setPreferredSize(new Dimension(Math.max(150, presetsBox.getPreferredSize().width), presetsBox.getPreferredSize().height));
    presetsBox.addActionListener(e -> {
      int i = presetsBox.getSelectedIndex();
      GraphLayoutAlgorithm.setSelectedPresetIndex(i);
      presetsPanelLayout.show(presetsPanel, Integer.toString(i));
      onFocusGained.forEach(Runnable::run);
    });
    JButton addPreset = new JButton("Add Preset");
    addPreset.addActionListener(e -> {
      String name = JOptionPane.showInputDialog(this, "Enter name for layout preset");
      if(name == null) return;
      if(name.contains(";") || name.contains(":")) {
        JOptionPane.showMessageDialog(this, "Invalid name: must not contain ':' or ';'", "Invalid characters", JOptionPane.ERROR_MESSAGE); 
        return;
      }
      GraphLayoutAlgorithm.addNewPreset(name);
    });
    removePreset = new JButton("Remove Preset");
    removePreset.addActionListener(e -> {
      GraphLayoutAlgorithm.removeCurrentPreset();
    });
    JPanel presetsEditRow = new JPanel(new FlowLayout(FlowLayout.LEFT));
    presetsEditRow.add(presetsBox);
    presetsEditRow.add(addPreset);
    presetsEditRow.add(removePreset);
    topLevelContainer.add(presetsEditRow, gbc);
    
    presetsPanelLayout = new CardLayout();
    presetsPanel = new JPanel();
    presetsPanel.setLayout(presetsPanelLayout);
    layoutStrutWidth = topLevelContainer.getPreferredSize().width;
    updatePresets();
    topLevelContainer.add(presetsPanel, gbc);
    
    JButton reset = new JButton("Reset all to defaults");
    reset.addActionListener(e -> {
      GraphLayoutAlgorithm.resetLayoutPresets();
      GraphLayoutAlgorithm.setDefaultOrientation();
//      GraphLayoutAlgorithm.alwaysRandomizeLayout(false);
//      GraphLayoutAlgorithm.alwaysReorderExternals(false);
//      GraphLayoutAlgorithm.useRectangleForBounds(false);
      ModelData.setDashed(true);
      onFocusGained.forEach(Runnable::run);
    });
    topLevelContainer.add(reset, gbc);
    onFocusGained.forEach(Runnable::run);
    page.add(topLevelContainer);
    return page;
  }

  private void createPresetsCards(CardLayout layout, JPanel panel, Iterable<GraphLayoutAlgorithm> algs, Consumer<Runnable> onFocusGained) {
    panel.removeAll();
    panel.setLayout(layout);
    
    int i = 0;
    for(GraphLayoutAlgorithm gla : algs) {
      JPanel p = new JPanel(new GridBagLayout());
      GridBagConstraints gbc = new GridBagConstraints();
      gbc.fill = GridBagConstraints.HORIZONTAL;
      gbc.gridx = 0;
      gbc.insets = new Insets(5, 0, 5, 0);
      
      for(JComponent box : gla.createOptions(onFocusGained)) {
        p.add(box, gbc);
      }
      
      gbc.insets = new Insets(0, 0, 0, 0);
      p.add(Box.createHorizontalStrut(layoutStrutWidth), gbc);
      panel.add(p, Integer.toString(i));
      i += 1;
    }
  }
  
  private JComponent createQgrafPanel() {
    if(Frame.gameMode == 1) {
      JPanel page = new JPanel();
      page.add(new JLabel("Not available in InFin mode"));
      return page;
    }

    JPanel page = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel vertical = new JPanel();
    vertical.setLayout(new BoxLayout(vertical, BoxLayout.Y_AXIS));

    JPanel pasting = new JPanel(new FlowLayout(FlowLayout.LEFT));
    pasting.setBorder(BorderFactory.createCompoundBorder(
        BorderFactory.createTitledBorder("Qgraf Paste settings"),
        BorderFactory.createEmptyBorder(3,3,3,3)));

    JCheckBox clearPaste = new JCheckBox("Remove existing lines when pasting qgraf diagrams", frame.clearCanvasOnPaste);
    clearPaste.addActionListener(e -> {
      frame.clearCanvasOnPaste = clearPaste.isSelected();
    });
    pasting.add(clearPaste);

    JPanel titled = new JPanel(new BorderLayout(10, 10));
    titled.setBorder(BorderFactory.createCompoundBorder(
        BorderFactory.createTitledBorder("Default Styles"),
        BorderFactory.createEmptyBorder(5,5,5,5)));

    JLabel label = new JLabel("The default qgraf style files will be loaded when FeynGame is started.");
    titled.add(label, BorderLayout.NORTH);
    JPanel buttons = new JPanel(new GridBagLayout());

    JButton add = new JButton("Add");
    JButton export = new JButton("Export");
    JButton edit = new JButton("Edit");
    JButton remove = new JButton("Remove");

    add   .setPreferredSize(new Dimension(100, add   .getPreferredSize().height));
    export .setPreferredSize(new Dimension(100, export .getPreferredSize().height));
    edit  .setPreferredSize(new Dimension(100, edit  .getPreferredSize().height));
    remove.setPreferredSize(new Dimension(100, remove.getPreferredSize().height));

    export .setEnabled(false);
    edit  .setEnabled(false);
    remove.setEnabled(false);

    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridx = 0;
    gbc.insets = new Insets(5, 0, 5, 0);
    buttons.add(add, gbc);
    buttons.add(export, gbc);
    buttons.add(edit, gbc);
    buttons.add(remove, gbc);

    titled.add(buttons, BorderLayout.EAST);

    JLabel fullPath = new JLabel("Full path: N/A");
    fullPath.setVisible(false);

    qgrafStyleListModel.clear();
    JList<LoadedQgrafStyle> styleNames = new JList<>(qgrafStyleListModel);
    styleNames.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION); //Allow mulitselect, to remove more than one at a time
    styleNames.setPreferredSize(new Dimension(300, styleNames.getPreferredSize().height));
    reloadDefaultStyleList();
    styleNames.addListSelectionListener(e -> {
      List<LoadedQgrafStyle> selected = styleNames.getSelectedValuesList();
      if(selected.isEmpty()) {
        export.setEnabled(false);
        edit.setEnabled(false);
        remove.setEnabled(false);
        fullPath.setText("Full path: N/A");
      } else if(selected.size() == 1) {
        LoadedQgrafStyle sel = selected.get(0);
        export.setEnabled(true);
        edit.setEnabled(!sel.isBuiltinStyle());
        remove.setEnabled(!sel.isBuiltinStyle());
        fullPath.setText("Full path: " + (sel.isBuiltinStyle() ? "builtin" : sel.getPath().toString()));
      } else {
        export.setEnabled(false);
        edit.setEnabled(false);
        remove.setEnabled(selected.stream().allMatch(Java8Support.not(LoadedQgrafStyle::isBuiltinStyle)));
        fullPath.setText("Full path: N/A");
      }
    });
    if(!qgrafStyleListModel.isEmpty()) styleNames.setSelectedIndex(0);

    //Button handlers
    add.addActionListener(e -> {
      QgrafImportManager.doGuiStyleLoad(this, true);
      reloadDefaultStyleList();
    });
    export.addActionListener(e -> {
      for(LoadedQgrafStyle file : styleNames.getSelectedValuesList()) {
        JFileChooser jfc = new JFileChooser();
        jfc.setMultiSelectionEnabled(false);
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfc.setDialogType(JFileChooser.SAVE_DIALOG);
        jfc.setDialogTitle("Export style file " + file.getName() + " to");

        if(jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
          Path exportTo = jfc.getSelectedFile().toPath();
          if(Files.exists(exportTo)) {
            if(JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(frame,
                "File already exists. Overwrite file?", "Overwrite", JOptionPane.YES_NO_OPTION)) {
              return; //abort
            }
          }

          try {
            if(file.isBuiltinStyle()) {
              String path = "/resources/qgrafstyles/" + file.getStyle().getName();
              try(InputStream is = GetResources.class.getResourceAsStream(path)) {
                Files.copy(is, exportTo, StandardCopyOption.REPLACE_EXISTING); //already asked
              }
            } else {
              Files.copy(file.getPath(), exportTo, StandardCopyOption.REPLACE_EXISTING);
            }
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        }
      }
    });
    edit.addActionListener(e -> {
      LoadedQgrafStyle selected = styleNames.getSelectedValue();
      if(selected.isBuiltinStyle()) return;

      JFileChooser jfc = new JFileChooser();
      jfc.setMultiSelectionEnabled(false);
      jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      jfc.setDialogType(JFileChooser.OPEN_DIALOG);
      jfc.setDialogTitle("Choose qgraf style file to replace the selected one");
//      jfc.setCurrentDirectory(new File(selected.file).getParentFile());
      jfc.removeChoosableFileFilter(jfc.getAcceptAllFileFilter());
      jfc.addChoosableFileFilter(new FileNameExtensionFilter("qgraf style (*.sty)", "sty"));
      jfc.addChoosableFileFilter(jfc.getAcceptAllFileFilter());

      if(jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
        File file = jfc.getSelectedFile();

        //Remove the old one
        Path toDelete = selected.getPath().toAbsolutePath();
        Path superFolder = QgrafImportManager.getQgrafDefaultStyleFolder();
        if(toDelete.startsWith(superFolder)) { //Safety check, don't delete random files
          try {
            Files.delete(toDelete);
          } catch (IOException e1) {
            LOGGER.log(Level.SEVERE, "Cannot delete default style", e);
          }
        } else {
          LOGGER.warning("Cannot delete default style: not in folder");
        }

        try {
          QgrafStyleFile.fromFile(file.toPath());
          QgrafImportManager.addAsQgrafDefaultStyle(file.getAbsoluteFile().toPath());
        } catch (IOException ex) {
          JOptionPane.showMessageDialog(this, "File " + file + " can not be accessed: " + ex.getMessage(), "IO Error", JOptionPane.ERROR_MESSAGE);
        } catch (QgrafImportException ex) {
          JOptionPane.showMessageDialog(this, "File " + file + " Is not a valid style file: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        reloadDefaultStyleList();
      }
    });
    remove.addActionListener(e -> {
      for(LoadedQgrafStyle selected : styleNames.getSelectedValuesList()) {
        if(selected.isBuiltinStyle()) return;

        Path toDelete = selected.getPath().toAbsolutePath();
        Path superFolder = QgrafImportManager.getQgrafDefaultStyleFolder();
        if(toDelete.startsWith(superFolder)) { //Safety check, don't delete random files
          try {
            Files.delete(toDelete);
          } catch (IOException e1) {
            LOGGER.log(Level.SEVERE, "Cannot delete default style", e);
          }
        } else {
          LOGGER.warning("Cannot delete default style: not in folder");
        }
      }
      reloadDefaultStyleList();
    });

    JPanel options = new JPanel(new FlowLayout(FlowLayout.LEFT));
    options.setBorder(BorderFactory.createCompoundBorder(
        BorderFactory.createTitledBorder("Qgraf import settings"),
        BorderFactory.createEmptyBorder(3,3,3,3)));
    JCheckBox deg6 = new JCheckBox("Limit vertex degree to 6", !QgrafImportManager.unlimitedVertexDegree());
    options.add(deg6);
    deg6.addActionListener(e -> {
      QgrafImportManager.unlimitedVertexDegree(!deg6.isSelected());
    });
    
    titled.add(styleNames, BorderLayout.CENTER);
    titled.add(fullPath, BorderLayout.SOUTH);
    vertical.add(options);
    vertical.add(titled);
    vertical.add(pasting);
    vertical.add(Box.createVerticalGlue());

    page.add(vertical);

    return page;
  }

  private void reloadDefaultStyleList() {
    QgrafImportManager.updateQgrafDefaultStyles();
    qgrafStyleListModel.clear();

    if(Frame.gameMode == 0) {
      for(LoadedQgrafStyle loaded : QgrafImportManager.getQgrafStyles()) {
        if(loaded.isDefaultStyle())
          qgrafStyleListModel.addElement(loaded);
      }
    }
  }

  private JPanel createLatexPanel() {
    JPanel page = new JPanel();
    SpringLayout layout = new SpringLayout();
    page.setLayout(layout);
    JPanel latexPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
    JLabel latexSizeLabel = new JLabel("Font size for LaTeX labels: ");
    latexPanel.add(latexSizeLabel);

    SpinnerNumberModel numModelLatexSize = new SpinnerNumberModel(ui.Frame.LaTeXFontSize, 1, 72, 1);
    JSpinner spinnerLatexSize = new JSpinner(numModelLatexSize);
    spinnerLatexSize.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      int fontSize = (int) s.getValue();
      ui.Frame.setLaTeXFontSize(fontSize);
    });
    latexPanel.add(spinnerLatexSize);

    latexPanel.setAlignmentX(LEFT_ALIGNMENT);
    page.setAlignmentX(LEFT_ALIGNMENT);
    page.add(latexPanel);

    return page;
  }
  /* Required by TreeSelectionListener interface. */
  /**
   * listens to a node of the tree being selected and sets the contents of
   * the window to the page associated with the node
   * @param e the event
   */
  @Override
  public void valueChanged(TreeSelectionEvent e) {

    DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeView.getLastSelectedPathComponent();

    /* if nothing is selected */
    if (node == null) return;

    /* retrieve the node that was selected */
    Object nodeInfo = node.getUserObject();

    setShownPage(nodeInfo.toString(), false);
    treeScroll.setSize(treeView.getPreferredSize());
    revalidate();
    repaint();
  }
  // Required by TreeExpansionListener interface.
  /**
   * listens to expansion events of the treeview and resizes the components
   * of the window
   * @param e the event
   */
  @Override
  public void treeExpanded(TreeExpansionEvent e) {
    treeScroll.setSize(treeView.getPreferredSize());
    revalidate();
    repaint();
  }

  // Required by TreeExpansionListener interface.
  /**
   * listens to collapse events of the treeview and resizes the components
   * of the window
   * @param e the event
   */
  @Override
  public void treeCollapsed(TreeExpansionEvent e) {
    treeScroll.setSize(treeView.getPreferredSize());
    revalidate();
    repaint();
  }

  /**
   * The settings window is not modal, so the default style list could change while this is open.
   * This is qhy the list is reloaded every time the window regains focus
   */
  @Override
  public void windowGainedFocus(WindowEvent e) {
    reloadDefaultStyleList();
    onFocusGained.forEach(Runnable::run);
  }

  @Override
  public void windowLostFocus(WindowEvent e) {}

}
