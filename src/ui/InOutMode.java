//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Taskbar;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import game.InOutLevel;
import resources.GetResources;
/**
 * This class is a Window that lets one select a level file for the InFin
 * game mode (which was initially named InOutMode) and a time for the timer
 * @author Sven Yannick Klein
 */
public class InOutMode extends JFrame {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 18L;
  /**
   * The path to the currently selected level file
   */
  private String levelPath;
  /**
   * Whether or not there should be a timer
   */
  private Boolean timer = false;
  /**
   * the total time that one has to get points (in sec)
   */
  private int totTime = 60;
  /**
   * the JSpinner that controls the {@link #totTime}
   */
  private JSpinner timeSpinner;
  /**
   * the last value of {@link #timeSpinner} since it was last changed
   */
  private Object lastValueTimeSpinner = null;
  /**
   * constructor of the window: constructs the window and initializes the
   * subcomponents as well as lays them out
   */
  public InOutMode() {
    /* basic look of the window */
    this.setTitle("Select a level");
    this.setResizable(false);

    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    this.setLayout(gridbag);
    c.weightx = 0.01;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = 0;
    c.insets = new Insets(50, 50, 50, 50);
    c.gridwidth = 2;

    JLabel img = new JLabel(GetResources.getIcon("inOutIcon.png"));
    this.add(img, c);


    /* let user set a timer*/
    c.insets = new Insets(0, 0, 0, 0);
    c.gridy++;
    JCheckBox timerCB = new JCheckBox("Use a timer with duration in seconds: ");
    timerCB.addItemListener(e -> {
      timer = timerCB.isSelected();
      if (timer) {
        timeSpinner.setEnabled(true);
      } else {
        timeSpinner.setEnabled(false);
      }
    });
    this.add(timerCB, c);

    c.gridwidth = 1;
    c.gridx += 2;

    SpinnerModel timeModel = new SpinnerNumberModel(60, 1, 10000, 1);
    timeSpinner = new JSpinner(timeModel);
    timeSpinner.addChangeListener((ChangeEvent e) -> {
      try {
        timeSpinner.commitEdit();
      } catch (java.text.ParseException ex) {
        ex.printStackTrace();
      }
      if (!timeSpinner.getValue().equals(lastValueTimeSpinner) || lastValueTimeSpinner == null)
        totTime = (int) Float.parseFloat(timeSpinner.getValue().toString());
      lastValueTimeSpinner = timeSpinner.getValue();
    });
    timeSpinner.setEnabled(false);
    this.add(timeSpinner, c);

    c.gridx = 0;
    c.gridwidth = 1;
    c.gridy++;

    /*Let user select the level file */

    JLabel label = new JLabel(" Level file: ");
    this.add(label, c);

    c.gridx++;
    c.fill = (GridBagConstraints.HORIZONTAL);
    c.weightx = 1;
    JTextField selPath = new JTextField();
    selPath.setHorizontalAlignment(JTextField.LEADING);
    selPath.setColumns(1);
    selPath.addActionListener(e -> levelPath = selPath.getText());
    this.add(selPath, c);
    c.weightx = 0.01;
    c.gridx++;
    JButton selFile = new JButton("Select File");
    selFile.addActionListener(e -> {
      final JFileChooser fc = new JFileChooser();

      fc.setMultiSelectionEnabled(false);
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setDialogType(JFileChooser.OPEN_DIALOG);
      fc.setDialogTitle("Load InFin Level file");

      if (levelPath == null) {
        fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
      } else {
        fc.setCurrentDirectory(new File(levelPath));
      }

      fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

      FileFilter filter = new FileNameExtensionFilter("InFin Level files (*.if)", "if");
      fc.addChoosableFileFilter(filter);
      fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());

      int returnVal;
      returnVal = fc.showSaveDialog(this);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fc.getSelectedFile();
        String withExtension = fc.getSelectedFile().getAbsolutePath();
        if (!withExtension.toLowerCase().endsWith(".if"))
          withExtension += ".if";

        levelPath = withExtension;
        selPath.setText(levelPath);

      }
    });
    this.add(selFile, c);

    /* let the user start the game*/

    c.gridx = 0;
    c.gridy++;
    c.gridwidth = 3;
    JButton start = new JButton("Start selected level");
    start.addActionListener(e -> {
      if (levelPath == null || levelPath.equals("") || !levelPath.endsWith(".if")) {
        JOptionPane.showMessageDialog(this, "The given path is not valid!", "Error", JOptionPane.ERROR_MESSAGE);
      } else {
        new Frame(new InOutLevel(levelPath), this.timer, this.totTime);
        this.dispose();
      }
    });
    this.add(start, c);

    c.gridx = 0;
    c.gridy++;
    c.gridwidth = 3;
    JButton startDefault = new JButton("Start default level");
    startDefault.addActionListener(e -> {
      new Frame(new InOutLevel(), this.timer, this.totTime);
      this.dispose();
    });
    this.add(startDefault, c);

    this.pack();
    this.setLocationRelativeTo(null);
    this.setVisible(true);
  }
}
