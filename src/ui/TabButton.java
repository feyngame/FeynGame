//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import javax.swing.*;
import java.awt.*;
import java.util.regex.Pattern;
import javax.swing.UIManager.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.border.MatteBorder;
import java.awt.Dimension;

public class TabButton extends CustomTabButton {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 28L;
  /**
   * the title of the Button
   */
  public String title;
  /**
   * the "close" Button
   */
  public CustomBackgroundsButton close;

  public TabButton(String title, int height) {


    Color norColor = UIManager.getColor("MenuItem.background");
    Color selColor = UIManager.getColor("MenuItem.selectionBackground");

    if (norColor == null) {
      norColor = this.getBackground();
    }
    if (selColor == null) {
      selColor = new Color((norColor.getRed() + 128) % 256, (norColor.getGreen() + 128) % 256, (norColor.getBlue() + 128) % 256);
    }

    this.setOpaque(true);
    this.setBackground(norColor);

    if (norColor.getGreen() + norColor.getBlue() + norColor.getRed() > 384) {
      this.setHoverBackgroundColor(norColor.darker());
    } else {
      this.setHoverBackgroundColor(norColor.brighter());
    }
    this.setPressedBackgroundColor(selColor);

    this.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 0));
    this.setBorder(BorderFactory.createMatteBorder(0,0,0,1,Color.BLACK));

    this.setMargin(new Insets(0, 0, 0, 0));
    this.title = title;

    /*String titleShort = title.split(Pattern.quote(System.getProperty("file.separator")))[title.split(Pattern.quote(System.getProperty("file.separator"))).length - 1];
    titleShort = titleShort.substring(0, titleShort.length() - 6);

    JLabel label = new JLabel(titleShort);*/

    JLabel label = new JLabel(title);

    if (title == null || title.equals("")) {
      label = new JLabel("untitled");
    }
    int prefWid = (int) label.getPreferredSize().getWidth();
    label.setPreferredSize(new Dimension(prefWid, height));

    // close = new CustomBackgroundsButton("<html>&#10005</html>", this);
    close = new CustomBackgroundsButton("X", this);
    close.setBorderPainted(false);
    close.setMargin(new Insets(-10, -10, -10, -10));
    if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      close.setMargin(new Insets(-1, 2, -1, 0));
    }
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      close.setMargin(new Insets(-5, -10, -5, -10));
    }
    close.setSize(close.getPreferredSize());
    close.setOpaque(true);
    close.setBackground(norColor);
    if (norColor.getGreen() + norColor.getBlue() + norColor.getRed() > 384) {
      close.setHoverBackgroundColor(norColor.darker());
    } else {
      close.setHoverBackgroundColor(norColor.brighter());
    }
    close.setBackgroundColorSelect(selColor);
    if (selColor.getGreen() + selColor.getBlue() + selColor.getRed() > 384) {
      close.setHoverBackgroundColorSelect(selColor.darker());
    } else {
      close.setHoverBackgroundColorSelect(selColor.brighter());
    }
    close.setBackgroundColorHover(this.getHoverBackgroundColor());
    Color hvColor = this.getHoverBackgroundColor();
    if (hvColor.getGreen() + hvColor.getBlue() + hvColor.getRed() > 384) {
      close.setHoverBackgroundColorHover(hvColor.darker());
    } else {
      close.setHoverBackgroundColorHover(hvColor.brighter());
    }

    close.setFocusable(false);

    this.add(label);
    this.add(close);

    this.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseEntered(java.awt.event.MouseEvent evt) {
        close.hovered = true;
        thisHovered = true;
      }

      public void mouseExited(java.awt.event.MouseEvent evt) {
        close.hovered = false;
        thisHovered = false;
      }
    });

    this.setSize(this.getPreferredSize());

    this.setAlignmentX(LEFT_ALIGNMENT);
  }
}
