//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;

import org.jgrapht.Graph;
import org.jgrapht.alg.drawing.model.Box2D;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import game.Blank;
import game.FeynLine;
import game.Filling;
import game.FloatingObject;
import game.LineConfig;
import game.LineType;
import game.Momentum;
import game.MultiEdit;
import game.Point2D;
import game.Shaped;
import game.Vertex;
import layout.GraphLayoutAlgorithm;
import qgraf.QgrafDiagram;
import qgraf.QgrafDiagram.GraphEdge;
import qgraf.QgrafDiagram.GraphVertex;
import qgraf.QgrafOutputFile;

/**
 * This is the main part of the {@link ui.Frame}. It displays all lines from {@link #lines}.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
@SuppressWarnings("serial")
public class Pane extends JPanel {
  public static int latexSize = 20;
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 2L;
  /**
   * the main frame of application
   */
  private Frame mainFrame;
  /**
   * The thread that is updating the dashOffset
   */
  private final Thread updateThread;
  /**
   * Background grid can be enables and this is the their spacing.
   */
  static int gridSize = 10;
  /**
   * Indicates if there are helper Lines drawn for knowing were to grab a line
   */
  public boolean helperLines = true;
  public String toolTipMsg;
  /*
   * Identifies wether a line or a vertex is selected
   * 0: line selected
   * 1: vertex selected
   * 2: floatingObject selected
   * 3: shaped selected
   * 4: MultiEdit is selected (a tile is selected)
   */
  public int selectType;
  /**
   * if a multiedit is selected, this is the multiedit
   */
  public MultiEdit multiEdit;
  /**
   * for checking connectivity of the diagram: holds for every vertex if it is connected to first vertex
   */
  public ArrayList<Boolean> vis;
  /**
   * for checking connectivity of the diagram: holds for every line if it is connected to first vertex
   */
  public ArrayList<Boolean> visLine;
  /**
   * for checking connectivity of the diagram: holds for every line drawn by the gamemode if it is connected to first vertex
   */
  public ArrayList<Boolean> visInFinLines;
  /**
   * Contains all lines drawn to the screen.
   */
  public ArrayList<FeynLine> lines;
  /**
   * Contains all vertices drawn to the screen
   */
  public ArrayList<Vertex> vertices;
  /**
   * Contains all {@link game.FloatingObject}s drawn to the screen
   */
  public ArrayList<FloatingObject> fObjects;
  /**
   * Contains all {@link game.Shaped} objects drawn to the screen
   */
  public ArrayList<Shaped> shapes;
  /**
   * List that stores the shape configurations for Undo Button
   */
  ArrayList<ArrayList<Shaped>> undoListShapes;
  /**
   * List that stores the shape configurations for Redo Button
   */
  ArrayList<ArrayList<Shaped>> redoListShapes;
  /**
   * List that stores the line configurations for Undo Button
   */
  ArrayList<ArrayList<FeynLine>> undoListLines;
  /**
   * List that stores the line configurations for Redo Button
   */
  ArrayList<ArrayList<FeynLine>> redoListLines;
  /**
   * List that stores the Vertex configurations for Undo Button
   */
  ArrayList<ArrayList<Vertex>> undoListVertices;
  /**
   * List that stores the Vertex configurations for Redo Button
   */
  ArrayList<ArrayList<Vertex>> redoListVertices;
  /**
   * List that stores the Vertex configurations for Undo Button
   */
  ArrayList<ArrayList<FloatingObject>> undoListfObjects;
  /**
   * List that stores lines that the initial and final state consist of in the InFin gameMode
   */
  ArrayList<FeynLine> inFinLines;
  /**
   * List that stores the Vertex configurations for Redo Button
   */
  ArrayList<ArrayList<FloatingObject>> redoListfObjects;
  /**
   * list of points where a vertex is present that is not in the model file
   */
  ArrayList<Point2D> pointsOfError = new ArrayList<>();
  /**
   * Whether or not the grid is drawn.
   */
  public boolean grid = true;
  /**
   * List of angles to clip to
   */
  ArrayList<Double> angles = new ArrayList<>(Arrays.asList(Math.PI / 4, Math.PI / 18));
  /**
   * Whether or not the current selected line is highlighted.
   */
  public boolean showCurrentSelectedLine = true;
  /**
   * Used when adding new lines to {@link #lines} in {@link ui.Frame#mouseDragged(MouseEvent)}, {@link ui.Frame#mousePressed(MouseEvent)}, {@link ui.Frame#mouseReleased(MouseEvent)}.
   */
  boolean mouseDown = false;
  /**
   * the maximum length of undoLists and redoLists
   */
  int maxHistory = 1000;
  /**
   * the scale with which the diagram is displayed
   */
  public double scale = 1;
  /**
   * where the point (0, 0) is displayed on the canvas
   */
  public Point2D zeroPoint = new Point2D(0, 0);
  /**
   * draw labels
   */
  static public boolean drawLabel = true;
  /** the manual bounding box **/
  public double xMBB, yMBB, wMBB, hMBB;
  /** whether or not to draw the manual bounding box **/
  public boolean mBB = false;
  /** can store the original position while dragging the bounding box*/ 
  public double xMBBsaved, yMBBsaved;
  /** turn off anti-aliasing **/
  public boolean noAA = false;
  
  /**
   * copy constructor for printing and other exports
   *
   * @param old the pane to be copied
   */
  public Pane(Pane old, boolean startDashUpdateThread) {
    ToolTipManager.sharedInstance().registerComponent(this);
    ToolTipManager.sharedInstance().setInitialDelay(0);

    this.setPreferredSize(old.getPreferredSize());
    this.mainFrame = old.mainFrame;

    this.mainFrame = old.mainFrame;

    this.selectType = old.selectType;

    this.grid = old.grid;

    this.showCurrentSelectedLine = old.showCurrentSelectedLine;

    this.maxHistory = old.maxHistory;

    this.mouseDown = old.mouseDown;

    this.toolTipMsg = old.toolTipMsg;

    this.helperLines = old.helperLines;

    this.zeroPoint = new Point2D(old.zeroPoint);
    
    lines = new ArrayList<>();
    for (FeynLine fl : old.lines) {
      lines.add(fl.getCopy());
    }

    shapes = new ArrayList<>();
    for (Shaped fl : old.shapes) {
      shapes.add(fl.getCopy());
    }

    vertices = new ArrayList<>();
    for (Vertex v : old.vertices) {
      vertices.add(new Vertex(v));
    }

    fObjects = new ArrayList<>();
    for (FloatingObject fO : old.fObjects) {
      fObjects.add(new FloatingObject(fO));
    }

    inFinLines = new ArrayList<>();
    for (FeynLine fl : old.inFinLines) {
      inFinLines.add(fl.getCopy());
    }

    undoListLines = new ArrayList<>();
    for (ArrayList<FeynLine> arrayList : old.undoListLines) {
      ArrayList<FeynLine> array = new ArrayList<>();
      for (FeynLine fl : arrayList) {
        array.add(fl.getCopy());
      }
      undoListLines.add(array);
    }
    redoListLines = new ArrayList<>();
    for (ArrayList<FeynLine> arrayList : old.redoListLines) {
      ArrayList<FeynLine> array = new ArrayList<>();
      for (FeynLine fl : arrayList) {
        array.add(fl.getCopy());
      }
      redoListLines.add(array);
    }
    undoListVertices = new ArrayList<>();
    for (ArrayList<Vertex> arrayList : old.undoListVertices) {
      ArrayList<Vertex> array = new ArrayList<>();
      for (Vertex v : arrayList) {
        array.add(new Vertex(v));
      }
      undoListVertices.add(array);
    }
    redoListVertices = new ArrayList<>();
    for (ArrayList<Vertex> arrayList : old.redoListVertices) {
      ArrayList<Vertex> array = new ArrayList<>();
      for (Vertex v : arrayList) {
        array.add(new Vertex(v));
      }
      redoListVertices.add(array);
    }
    undoListfObjects = new ArrayList<>();
    for (ArrayList<FloatingObject> arrayList : old.undoListfObjects) {
      ArrayList<FloatingObject> array = new ArrayList<>();
      for (FloatingObject fO : arrayList) {
        array.add(new FloatingObject(fO));
      }
      undoListfObjects.add(array);
    }
    redoListfObjects = new ArrayList<>();
    for (ArrayList<FloatingObject> arrayList : old.redoListfObjects) {
      ArrayList<FloatingObject> array = new ArrayList<>();
      for (FloatingObject fO : arrayList) {
        array.add(new FloatingObject(fO));
      }
      redoListfObjects.add(array);
    }
    undoListShapes = new ArrayList<>();
    for (ArrayList<Shaped> arrayList : old.undoListShapes) {
      ArrayList<Shaped> array = new ArrayList<>();
      for (Shaped sh : arrayList) {
        array.add(sh.getCopy());
      }
      undoListShapes.add(array);
    }
    redoListShapes = new ArrayList<>();
    for (ArrayList<Shaped> arrayList : old.redoListShapes) {
      ArrayList<Shaped> array = new ArrayList<>();
      for (Shaped sh : arrayList) {
        array.add(sh.getCopy());
      }
      redoListShapes.add(array);
    }

    /* the actice object feature is to continually change the dshOffset*/
    if(startDashUpdateThread) {
      this.updateThread = new Thread(new DashOffsetUpdater(), "DashOffsetUpdater-" + THREAD_COUNT.getAndIncrement());
      this.updateThread.setDaemon(true);
      this.updateThread.start();
    } else {
      this.updateThread = null;
    }
  }
  
  /**
   * Initializes {@link #lines} and starts parallel {@link Thread} to change {@link game.LineConfig#dashOffset} of current selected line to highlight it (if {@link #showCurrentSelectedLine}).
   * @param mainFrame the mainFrame this panel is put onto
   */
  Pane(Frame mainFrame, boolean startDashUpdateThread) {

    this.mainFrame = mainFrame;

    this.setPreferredSize(new Dimension(300, 500));

    ToolTipManager.sharedInstance().registerComponent(this);

    lines = new ArrayList<>();

    vertices = new ArrayList<>();

    fObjects = new ArrayList<>();

    shapes = new ArrayList<>();

    undoListLines = new ArrayList<>();
    redoListLines = new ArrayList<>();
    undoListVertices = new ArrayList<>();
    redoListVertices = new ArrayList<>();
    undoListfObjects = new ArrayList<>();
    redoListfObjects = new ArrayList<>();
    inFinLines = new ArrayList<>();
    undoListShapes = new ArrayList<>();
    redoListShapes = new ArrayList<>();

    /* the active object feature is to continually change the dshOffset*/
    if(startDashUpdateThread) {
      this.updateThread = new Thread(new DashOffsetUpdater(), "DashOffsetUpdater-" + THREAD_COUNT.getAndIncrement());
      this.updateThread.setDaemon(true);
      this.updateThread.start();
    } else {
      this.updateThread = null;
    }
  }
  
  private static final AtomicInteger THREAD_COUNT = new AtomicInteger();
  private final class DashOffsetUpdater implements Runnable {
    @Override
    public void run() {
      while (true) {
        if (Pane.this.lines.size() > 0) {
          Pane.this.lines.get(Pane.this.lines.size() - 1).lineConfig.dashOffset = Pane.this.lines.get(Pane.this.lines.size() - 1).lineConfig.dashOffset + 1f;
          Pane.this.repaint();
        }
        if (Pane.this.vertices.size() > 0) {
          Pane.this.vertices.get(Pane.this.vertices.size() - 1).border.dashOffset = Pane.this.vertices.get(Pane.this.vertices.size() - 1).border.dashOffset + 1f;
          Pane.this.repaint();
        }
        if (Pane.this.fObjects.size() > 0) {
          if (Pane.this.fObjects.get(Pane.this.fObjects.size() - 1).v)
            Pane.this.fObjects.get(Pane.this.fObjects.size() - 1).vertex.border.dashOffset = Pane.this.fObjects.get(Pane.this.fObjects.size() - 1).vertex.border.dashOffset + 1f;
          Pane.this.repaint();
        }
        if (Pane.this.shapes.size() > 0) {
          Pane.this.shapes.get(Pane.this.shapes.size() - 1).lineConfig.dashOffset += 1f;
          Pane.this.repaint();
        }
        try {
          Thread.sleep(50);
        } catch (InterruptedException e) {
          return; //interrupt means thread termination was requested
        }
      }
    }
  }

  public QgrafOutputFile getCurretQgrafFile() {
    return mainFrame.currentQgrafFile;
  }
  
  public List<LineConfig> getCurretForceModel() {
    return mainFrame.modelForAllQgrafLoads;
  }
  
  /**
   * Draws {@link String} ('Press F1') to the background of the {@link ui.Pane}.
   * Calls {@link game.FeynLine#draw(Graphics2D, boolean)} on all {@link #lines}.
   * Draws all vertices, that have more than one line connected.
   * Draws the grid if {@link #grid}.
   *
   * @param g2 Graphics variable
   */
  @Override
  public void paintComponent(Graphics g2) {
    super.paintComponent(g2);
    this.setOpaque(false);
    // this.setBackground(Color.RED);
    Graphics2D g = (Graphics2D) g2.create();

    RenderingHints rh = new RenderingHints(
          RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON);
    if (!noAA) g.setRenderingHints(rh);

    /* the text that is displayed in the background */
    if (vertices.size() == 0 && shapes.size() == 0 && lines.size() == 0 && fObjects.size() == 0 && inFinLines.size() == 0 && this.mainFrame.tiles.selected != null && this.mainFrame.tiles.selected.lineType == LineType.GRAB) {
      String text = "<html><font size=\"+20\"><center>Select a tile<br>&darr&ensp&darr";

      JLabel label = new JLabel(text, JLabel.CENTER);

      label.setForeground(Color.GRAY);
      label.setSize(label.getPreferredSize());
      int x = (this.getWidth() - label.getWidth()) / 2;
      int y = (this.getHeight() - label.getHeight()) / 2;
      g.translate(x, y);
      label.paint(g);
      g.translate(-x, -y);

    } else if (shapes.size() == 0 && lines.size() == 0 && fObjects.size() == 0 && inFinLines.size() == 0 && this.mainFrame.tiles.selected != null) {
      String text = "<html><font size=\"+20\"><center>Draw a line<br>by dragging the mouse";

      JLabel label = new JLabel(text, JLabel.CENTER);

      label.setForeground(Color.GRAY);
      label.setSize(label.getPreferredSize());
      int x = (this.getWidth() - label.getWidth()) / 2;
      int y = (this.getHeight() - label.getHeight()) / 2;
      g.translate(x, y);
      label.paint(g);
      g.translate(-x, -y);
    } else if (shapes.size() == 0 && lines.size() == 0 && fObjects.size() == 0 && inFinLines.size() == 0 && this.mainFrame.tiles.selectedVertex != null) {

      String text = "<html><font size=\"+20\"><center>Draw a vertex<br>by clicking on the canvas";

      JLabel label = new JLabel(text, JLabel.CENTER);

      label.setForeground(Color.GRAY);
      label.setSize(label.getPreferredSize());
      int x = (this.getWidth() - label.getWidth()) / 2;
      int y = (this.getHeight() - label.getHeight()) / 2;
      g.translate(x, y);
      label.paint(g);
      g.translate(-x, -y);
    } else if (shapes.size() == 0 && lines.size() == 0 && fObjects.size() == 0 && inFinLines.size() == 0 && this.mainFrame.tiles.selectedImage != null) {

      String text = "<html><font size=\"+20\"><center>Draw an image<br>by clicking on the canvas";

      JLabel label = new JLabel(text, JLabel.CENTER);

      label.setForeground(Color.GRAY);
      label.setSize(label.getPreferredSize());
      int x = (this.getWidth() - label.getWidth()) / 2;
      int y = (this.getHeight() - label.getHeight()) / 2;
      g.translate(x, y);
      label.paint(g);
      g.translate(-x, -y);
    }

    g.scale(this.scale, this.scale);
    g.translate(this.zeroPoint.x, this.zeroPoint.y);

    /* debugging auto bounds*/
    if (Print.SHOW_AUTO_BOUNDS) {
      g.setColor(Color.BLACK);
      Rectangle rec = this.getBoundsPainted();
      g.drawRect((int) rec.getX(), (int) rec.getY(), (int) rec.getWidth(), (int) rec.getHeight());
    }

    /** draw manual bounding box **/
    if (this.mBB) {
      g.setColor(Color.DARK_GRAY);
      g.drawRect((int) xMBB, (int) yMBB, (int) wMBB, (int) hMBB);
    }

    /* draw Grid */
    // if (grid) {
    //   Stroke oldS = g.getStroke();
    //   Color oldC = g.getColor();
    //   int gridDotSize = gridSize / 5;
    //   if (gridDotSize > 6) {
    //     gridDotSize = 6;
    //   }

    //   g.setColor(Color.gray);
    //   g.setStroke(new BasicStroke(0.5f));
    //   for (int x = gridSize / 2 - (int) Math.round(this.zeroPoint.x / gridSize) * gridSize; x < this.getWidth() / this.scale - zeroPoint.x; x += gridSize)
    //     for (int y = gridSize / 2 - (int) Math.round(this.zeroPoint.y / gridSize) * gridSize; y < this.getHeight() / this.scale - this.zeroPoint.y; y += gridSize)
    //       g.fillRect(x - gridDotSize / 2, y - gridDotSize / 2, gridDotSize, gridDotSize);

    //   g.setStroke(oldS);
    //   g.setColor(oldC);
    // }

    int i = 0;

    /* draw lines */
    for (FeynLine line : lines) {

      line.drawBlanks = true;

      if (FeynLine.autoBlanks) {
        line.blanksUnsorted = new ArrayList<>();
        line.blanks = new ArrayList<>();
        for (int j = i+1; j < lines.size(); j++) {
          for (Map.Entry<Double, Double> entry : line.intersects(lines.get(j))) {
            line.blanksUnsorted.add(new Blank(entry.getValue(), entry.getKey()));
          }
        }
        line.sortBlanks();
      } else {
        if (line.blanks == null)
          line.blanks = new ArrayList<>();
        if (line.blanksUnsorted == null)
          line.blanksUnsorted = new ArrayList<>();
        line.calculateArch();
      }

      i++;
      if (!helperLines || (line.lineConfig.lineType != LineType.FERMION/* && !line.lineConfig.showArrow*/)) {
        line.draw(g, i == lines.size() && showCurrentSelectedLine && selectType == 0);
      }
      if (line.momArrow != null && line.momArrow.drawMomentum)
        line.momArrow.draw(g);
    }

    for (FeynLine line : inFinLines) {
      line.draw(g, false);
    }
    /* draw Vertices */
    i = 0;
    for (Vertex vertex : vertices) {
      i++;
      vertex.draw(g, i == vertices.size() && showCurrentSelectedLine && selectType == 1);
    }
    i = 0;
    for (FloatingObject fO : fObjects) {
      i++;
      fO.draw(g, i == fObjects.size() && showCurrentSelectedLine && selectType == 2);
    }
    i = 0;
    /* draw lines */
    for (FeynLine line : lines) {
      i++;
      if (helperLines && (line.lineConfig.lineType == LineType.FERMION/* && line.lineConfig.showArrow*/) && line.draggedMode == 2) {
        line.draw(g, i == lines.size() && showCurrentSelectedLine && selectType == 0);
      }
    }

    i = 0;
    /* draw shaped objects */
    for (Shaped s : shapes) {
      i++;
      s.draw(g, i == shapes.size() && showCurrentSelectedLine && selectType == 3, helperLines);
    }

    g.setStroke(new BasicStroke());

    i = 0;

    /* draw helper lines */
    if (this.helperLines) {
      for (Vertex v : vertices) {
        if (v.showDesc && v.description != null && !v.description.equals("")) {
          Point2D place = new Point2D(v.origin.x + v.descPosX, v.origin.y + v.descPosY);
          g.setColor(Color.BLACK);
          g.drawOval((int) Math.round(place.x) - 10, (int) Math.round(place.y) - 10, 2 * 10, 2 * 10);
          g.setColor(Color.GRAY);
          g.drawLine((int) Math.round(place.x), (int) Math.round(place.y), (int) Math.round(v.origin.x), (int) Math.round(v.origin.y));
        }
      }
      for (FloatingObject fO : fObjects) {
        if (fO.v) {
          if (fO.vertex.showDesc && fO.vertex.description != null && !fO.vertex.description.equals("")) {
            Point2D place = new Point2D(fO.vertex.origin.x + fO.vertex.descPosX, fO.vertex.origin.y + fO.vertex.descPosY);
            g.setColor(Color.BLACK);
            g.drawOval((int) Math.round(place.x) - 10, (int) Math.round(place.y) - 10, 2 * 10, 2 * 10);
            g.setColor(Color.GRAY);
            g.drawLine((int) Math.round(place.x), (int) Math.round(place.y), (int) Math.round(fO.vertex.origin.x), (int) Math.round(fO.vertex.origin.y));
          }
        } else {
          if (fO.floatingImage.showDesc && fO.floatingImage.description != null && !fO.floatingImage.description.equals("")) {
            Point2D place = new Point2D(fO.floatingImage.center.x + fO.floatingImage.descPosX, fO.floatingImage.center.y + fO.floatingImage.descPosY);
            g.setColor(Color.BLACK);
            g.drawOval((int) Math.round(place.x) - 10, (int) Math.round(place.y) - 10, 2 * 10, 2 * 10);
            g.setColor(Color.GRAY);
            g.drawLine((int) Math.round(place.x), (int) Math.round(place.y), (int) Math.round(fO.floatingImage.center.x), (int) Math.round(fO.floatingImage.center.y));
          }
        }
      }
      for (FeynLine line : lines) {
        i++;

        if (line.showDesc && line.lineConfig.description != null && !line.lineConfig.description.equals("")) {
          double len = Math.sqrt((line.getStartX() - line.getEndX()) * (line.getStartX() - line.getEndX()) + (line.getStartY() - line.getEndY()) * (line.getStartY() - line.getEndY()));
          Point2D place;
          Point2D referencePoint;
          if (Math.abs(line.height) < .1) {

            double normtanx = line.getStartX() - line.getEndX();
            normtanx /= len;
            double normtany = line.getStartY() - line.getEndY();
            normtany /= len;
            place = new Point2D(line.getStartX() - (len * line.partPlaceDesc) * normtanx + (line.descDis) * normtany, line.getStartY() - (len * line.partPlaceDesc) * normtany - (line.descDis) * normtanx);

            double partPlaceDesc = line.partPlaceDesc;
            if (partPlaceDesc < 0)
              partPlaceDesc = 0;
            else if (partPlaceDesc > 1)
              partPlaceDesc = 1;
            referencePoint = new Point2D(line.getStartX() - (len * partPlaceDesc) * normtanx, line.getStartY() - (len * partPlaceDesc) * normtany);
          } else {
            double partArch = line.deltaArch * line.partPlaceDesc;
            double placeArch = line.archStart + partArch;
            double radius = line.radius + line.descDis;
            double x;
            double y;
            if (line.height > 0) {
              y = line.center.y + radius * Math.sin(placeArch);
              x = line.center.x + radius * Math.cos(placeArch);
              if (line.partPlaceDesc > 1) {
                partArch = line.deltaArch;
                placeArch = line.archStart + partArch;
              } else if (line.partPlaceDesc < 0) {
                partArch = 0;
                placeArch = line.archStart + partArch;
              }
              referencePoint = new Point2D(line.center.x + line.radius * Math.cos(placeArch), line.center.y + line.radius * Math.sin(placeArch));
            } else {
              y = line.center.y - radius * Math.sin(placeArch);
              x = line.center.x - radius * Math.cos(placeArch);
              if (line.partPlaceDesc > 1) {
                partArch = line.deltaArch;
                placeArch = line.archStart + partArch;
              } else if (line.partPlaceDesc < 0) {
                partArch = 0;
                placeArch = line.archStart + partArch;
              }
              referencePoint = new Point2D(line.center.x - line.radius * Math.cos(placeArch), line.center.y - line.radius * Math.sin(placeArch));
            }
            place = new Point2D(x, y);
          }

          if (line.getStart().equals(line.getEnd()))
            g.rotate(line.circAngle, line.getStart().x, line.getStart().y);
          g.setColor(Color.BLACK);
          g.drawOval((int) Math.round(place.x) - 10, (int) Math.round(place.y) - 10, 2 * 10, 2 * 10);
          g.setColor(Color.GRAY);
          g.drawLine((int) Math.round(place.x), (int) Math.round(place.y), (int) Math.round(referencePoint.x), (int) Math.round(referencePoint.y));
          if (line.getStart().equals(line.getEnd()))
            g.rotate(-line.circAngle, line.getStart().x, line.getStart().y);
        }

        if (line.lineConfig.lineType == LineType.GLUON || line.lineConfig.lineType == LineType.PHOTON || line.lineConfig.lineType == LineType.SSPIRAL || line.lineConfig.lineType == LineType.SWAVE) {
          Point2D start = line.getStart();
          Point2D end = line.getEnd();
          LineConfig helperLineConfig = new LineConfig(0, LineType.FERMION, (float) 1, (float) 1, (float) 1, (float) 1, (float) 0, Color.RED, false, (float) 0, 0,0);
          LineConfig helperLineConfigHighlighted = new LineConfig(0, LineType.FERMION, (float) 2, (float) 1, (float) 1, (float) 1, (float) 0, Color.BLUE, false, (float) 0, 0,0);

          if (line.draggedMode != -1 && line.draggedMode != 0 && line.draggedMode != 1) {
            FeynLine helperLine = new FeynLine(start, end, helperLineConfig, line.getHeight());
            helperLine.showDesc = false;
            helperLine.hideMom();
            if (line.getStart().distance(line.getEnd()) < 1 && Math.abs(line.height) > .1) {
              g.rotate(line.circAngle, line.getStartX(), line.getStartY());
            }
            helperLine.draw(g, false);
            if (line.getStart().distance(line.getEnd()) < 1 && Math.abs(line.height) > .1) {
              g.rotate( - line.circAngle, line.getStartX(), line.getStartY());
            }
          } else {
            if (Math.abs(line.height) < .1) {
              Point2D ankerPoint1 = new Point2D((start.x - end.x) / 4 + end.x, (start.y - end.y) / 4 + end.y);
              Point2D ankerPoint2 = new Point2D((end.x - start.x) / 4 + start.x, (end.y - start.y) / 4 + start.y);
              if (line.draggedMode == -1) {
                FeynLine helperLine1 = new FeynLine(start, ankerPoint2, helperLineConfig, 0);
                FeynLine helperLine2 = new FeynLine(ankerPoint2, ankerPoint1, helperLineConfigHighlighted, 0);
                FeynLine helperLine3 = new FeynLine(ankerPoint1, end, helperLineConfig, 0);
                helperLine1.showDesc = false;
                helperLine2.showDesc = false;
                helperLine3.showDesc = false;
                helperLine1.draw(g, false);
                helperLine2.draw(g, false);
                helperLine3.draw(g, false);
              } else if (line.draggedMode == 0) {
                FeynLine helperLine1 = new FeynLine(start, ankerPoint2, helperLineConfigHighlighted, 0);
                FeynLine helperLine2 = new FeynLine(ankerPoint2, ankerPoint1, helperLineConfig, 0);
                FeynLine helperLine3 = new FeynLine(ankerPoint1, end, helperLineConfig, 0);
                helperLine1.showDesc = false;
                helperLine2.showDesc = false;
                helperLine3.showDesc = false;
                helperLine1.draw(g, false);
                helperLine2.draw(g, false);
                helperLine3.draw(g, false);
              } else if (line.draggedMode == 1) {
                FeynLine helperLine1 = new FeynLine(start, ankerPoint2, helperLineConfig, 0);
                FeynLine helperLine2 = new FeynLine(ankerPoint2, ankerPoint1, helperLineConfig, 0);
                FeynLine helperLine3 = new FeynLine(ankerPoint1, end, helperLineConfigHighlighted, 0);
                helperLine1.showDesc = false;
                helperLine2.showDesc = false;
                helperLine3.showDesc = false;
                helperLine1.draw(g, false);
                helperLine2.draw(g, false);
                helperLine3.draw(g, false);
              }
            } else {

              if (line.getStart().equals(line.getEnd())) {
                g.rotate(line.circAngle, line.getStartX(), line.getStartY());
              }
              double archEndGes = Math.atan2(end.y - line.center.y, end.x - line.center.x);
              double archStartGes = Math.atan2(start.y - line.center.y, start.x - line.center.x);
              double deltaArchGes = (-archStartGes + archEndGes) * -Math.signum(line.height);

              while (deltaArchGes > 2 * Math.PI) {
                deltaArchGes -= 2 * Math.PI;
              }
              while (deltaArchGes <= 0) {
                deltaArchGes += 2 * Math.PI;
              }

              double arch1 = -deltaArchGes / 4;
              double arch2 = -3 * deltaArchGes / 4;

              if (line.height < 0) {
                arch1 -= archStartGes;
                arch1 *= -1;
                arch1 += Math.PI / 2;
                arch2 -= archStartGes;
                arch2 *= -1;
              } else {
                arch1 += archStartGes;
                arch1 += Math.PI / 2;
                arch2 += archStartGes;
              }
              arch2 += Math.PI / 2;

              double pointX1 = +line.center.x - Math.signum(line.height) * line.radius * Math.sin(-arch1);
              double pointY1 = +line.center.y - Math.signum(line.height) * line.radius * Math.cos(-arch1);
              double pointX2 = +line.center.x - Math.signum(line.height) * line.radius * Math.sin(-arch2);
              double pointY2 = +line.center.y - Math.signum(line.height) * line.radius * Math.cos(-arch2);

              Point2D ankerPoint1 = new Point2D(pointX1, pointY1);
              Point2D ankerPoint2 = new Point2D(pointX2, pointY2);

              if (line.draggedMode == -1) {
                FeynLine helperLine1 = new FeynLine(start, ankerPoint1, helperLineConfig, FeynLine.getHeight(line.radius, start, ankerPoint1, line.center));
                FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, helperLineConfigHighlighted, FeynLine.getHeight(line.radius, ankerPoint1, ankerPoint2, line.center));
                FeynLine helperLine3 = new FeynLine(ankerPoint2, end, helperLineConfig, FeynLine.getHeight(line.radius, ankerPoint2, end, line.center));
                helperLine1.showDesc = false;
                helperLine2.showDesc = false;
                helperLine3.showDesc = false;
                helperLine1.draw(g, false, true);
                helperLine2.draw(g, false, true);
                helperLine3.draw(g, false, true);
              } else if (line.draggedMode == 0) {
                FeynLine helperLine1 = new FeynLine(start, ankerPoint1, helperLineConfigHighlighted, FeynLine.getHeight(line.radius, start, ankerPoint1, line.center));
                FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, helperLineConfig, FeynLine.getHeight(line.radius, ankerPoint1, ankerPoint2, line.center));
                FeynLine helperLine3 = new FeynLine(ankerPoint2, end, helperLineConfig, FeynLine.getHeight(line.radius, ankerPoint2, end, line.center));
                helperLine1.showDesc = false;
                helperLine2.showDesc = false;
                helperLine3.showDesc = false;
                helperLine1.draw(g, false, true);
                helperLine2.draw(g, false, true);
                helperLine3.draw(g, false, true);
              } else if (line.draggedMode == 1) {
                FeynLine helperLine1 = new FeynLine(start, ankerPoint1, helperLineConfig, FeynLine.getHeight(line.radius, start, ankerPoint1, line.center));
                FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, helperLineConfig, FeynLine.getHeight(line.radius, ankerPoint1, ankerPoint2, line.center));
                FeynLine helperLine3 = new FeynLine(ankerPoint2, end, helperLineConfigHighlighted, FeynLine.getHeight(line.radius, ankerPoint2, end, line.center));
                helperLine1.showDesc = false;
                helperLine2.showDesc = false;
                helperLine3.showDesc = false;
                helperLine1.draw(g, false, true);
                helperLine2.draw(g, false, true);
                helperLine3.draw(g, false, true);
              }

              if (line.getStart().equals(line.getEnd())) {
                g.rotate( - line.circAngle, line.getStartX(), line.getStartY());
              }
            }
          }
        } else if (!(line.draggedMode != -1 && line.draggedMode != 0 && line.draggedMode != 1)) {
          Point2D start = line.getStart();
          Point2D end = line.getEnd();
          LineConfig helperLineConfig = new LineConfig(line.lineConfig);
          helperLineConfig.showArrow = false;
          helperLineConfig.lineType = LineType.FERMION;
          LineConfig helperLineConfigHighlighted = new LineConfig(line.lineConfig);
          helperLineConfigHighlighted.showArrow = false;
          helperLineConfigHighlighted.lineType = LineType.FERMION;

          double stroke = helperLineConfigHighlighted.stroke;

          if (stroke < 20) {
            stroke *= (float) 1.5;
          } else {
            stroke += 5;
          }

          helperLineConfigHighlighted.stroke = (float) stroke;

          Color color = (helperLineConfigHighlighted.color);

          if (color.getRGB() == Color.BLACK.getRGB()) {
            color = Color.GRAY;
          }

          helperLineConfigHighlighted.color = new Color(255 - color.getRed(), 255 - color.getBlue(), 255 - color.getGreen());

          LineConfig helperLineConfigEnd = new LineConfig(helperLineConfig);
          helperLineConfigEnd.lineType = LineType.FERMION;

          LineConfig helperLineConfigHighlightedEnd = new LineConfig(helperLineConfigHighlighted);
          helperLineConfigHighlightedEnd.lineType = LineType.FERMION;

          LineConfig helperLineConfigStart = new LineConfig(helperLineConfig);
          helperLineConfigStart.lineType = LineType.FERMION;

          LineConfig helperLineConfigHighlightedStart = new LineConfig(helperLineConfigHighlighted);
          helperLineConfigHighlightedStart.lineType = LineType.FERMION;

          if (line.lineConfig.lineType == LineType.FERMION && line.lineConfig.showArrow) {
            if (line.lineConfig.arrowPlace < .25) {
              helperLineConfigStart.arrowPlace = line.lineConfig.arrowPlace * 4;
              helperLineConfigStart.showArrow = true;
              helperLineConfigStart.arrowPlace = line.lineConfig.arrowPlace * 4;

              helperLineConfigHighlightedStart.arrowPlace = line.lineConfig.arrowPlace * 4;
              helperLineConfigHighlightedStart.showArrow = true;
              helperLineConfigHighlightedStart.arrowPlace = line.lineConfig.arrowPlace * 4;
            } else if (line.lineConfig.arrowPlace <= .75) {
              helperLineConfigHighlighted.arrowPlace = (line.lineConfig.arrowPlace - .25) * 2;
              helperLineConfigHighlighted.showArrow = true;
              helperLineConfigHighlighted.arrowPlace = (line.lineConfig.arrowPlace - .25) * 2;

              helperLineConfig.arrowPlace = (line.lineConfig.arrowPlace - .25) * 2;
              helperLineConfig.showArrow = true;
              helperLineConfig.arrowPlace = (line.lineConfig.arrowPlace - .25) * 2;
            } else {
              helperLineConfigEnd.arrowPlace = (line.lineConfig.arrowPlace - .75) * 4;
              helperLineConfigEnd.showArrow = true;
              helperLineConfigEnd.arrowPlace = (line.lineConfig.arrowPlace - .75) * 4;

              helperLineConfigHighlightedEnd.arrowPlace = (line.lineConfig.arrowPlace - .75) * 4;
              helperLineConfigHighlightedEnd.showArrow = true;
              helperLineConfigHighlightedEnd.arrowPlace = (line.lineConfig.arrowPlace - .75) * 4;
            }
          }

          if (Math.abs(line.height) < .1) {
            Point2D ankerPoint1 = new Point2D((start.x - end.x) / 4 + end.x, (start.y - end.y) / 4 + end.y);
            Point2D ankerPoint2 = new Point2D((end.x - start.x) / 4 + start.x, (end.y - start.y) / 4 + start.y);
            if (line.draggedMode == -1) {
              FeynLine helperLine1 = new FeynLine(start, ankerPoint2, helperLineConfigStart, 0);
              FeynLine helperLine2 = new FeynLine(ankerPoint2, ankerPoint1, helperLineConfigHighlighted, 0);
              FeynLine helperLine3 = new FeynLine(ankerPoint1, end, helperLineConfigEnd, 0);
              helperLine1.showDesc = false;
              helperLine2.showDesc = false;
              helperLine3.showDesc = false;
              if (line.showDesc) {
                if (line.partPlaceDesc < .25) {
                  helperLine1.partPlaceDesc = line.partPlaceDesc * 4;
                  helperLine1.descDis = line.descDis;
                  helperLine1.rotDesc = line.rotDesc;
                  helperLine1.descScale = line.descScale;
                  helperLine1.showDesc = true;
                } else if (line.partPlaceDesc <= .75) {
                  helperLine2.partPlaceDesc = (line.partPlaceDesc - .25) * 2;
                  helperLine2.descDis = line.descDis;
                  helperLine2.rotDesc = line.rotDesc;
                  helperLine2.descScale = line.descScale;
                  helperLine2.showDesc = true;
                } else {
                  helperLine3.partPlaceDesc = (line.partPlaceDesc - .75) * 4;
                  helperLine3.descDis = line.descDis;
                  helperLine3.rotDesc = line.rotDesc;
                  helperLine3.descScale = line.descScale;
                  helperLine3.showDesc = true;
                }
              }
              helperLine1.draw(g, i == lines.size() && showCurrentSelectedLine);
              helperLine2.draw(g, i == lines.size() && showCurrentSelectedLine);
              helperLine3.draw(g, i == lines.size() && showCurrentSelectedLine);
            } else if (line.draggedMode == 0) {
              FeynLine helperLine1 = new FeynLine(start, ankerPoint2, helperLineConfigHighlightedStart, 0);
              FeynLine helperLine2 = new FeynLine(ankerPoint2, ankerPoint1, helperLineConfig, 0);
              FeynLine helperLine3 = new FeynLine(ankerPoint1, end, helperLineConfigEnd, 0);
              helperLine1.showDesc = false;
              helperLine2.showDesc = false;
              helperLine3.showDesc = false;
              if (line.showDesc) {
                if (line.partPlaceDesc < .25) {
                  helperLine1.partPlaceDesc = line.partPlaceDesc * 4;
                  helperLine1.descDis = line.descDis;
                  helperLine1.rotDesc = line.rotDesc;
                  helperLine1.descScale = line.descScale;
                  helperLine1.showDesc = true;
                } else if (line.partPlaceDesc <= .75) {
                  helperLine2.partPlaceDesc = (line.partPlaceDesc - .25) * 2;
                  helperLine2.descDis = line.descDis;
                  helperLine2.rotDesc = line.rotDesc;
                  helperLine2.descScale = line.descScale;
                  helperLine2.showDesc = true;
                } else {
                  helperLine3.partPlaceDesc = (line.partPlaceDesc - .75) * 4;
                  helperLine3.descDis = line.descDis;
                  helperLine3.rotDesc = line.rotDesc;
                  helperLine3.descScale = line.descScale;
                  helperLine3.showDesc = true;
                }
              }
              helperLine1.draw(g, i == lines.size() && showCurrentSelectedLine);
              helperLine2.draw(g, i == lines.size() && showCurrentSelectedLine);
              helperLine3.draw(g, i == lines.size() && showCurrentSelectedLine);
            } else if (line.draggedMode == 1) {
              FeynLine helperLine1 = new FeynLine(start, ankerPoint2, helperLineConfigStart, 0);
              FeynLine helperLine2 = new FeynLine(ankerPoint2, ankerPoint1, helperLineConfig, 0);
              FeynLine helperLine3 = new FeynLine(ankerPoint1, end, helperLineConfigHighlightedEnd, 0);
              helperLine1.showDesc = false;
              helperLine2.showDesc = false;
              helperLine3.showDesc = false;
              if (line.showDesc) {
                if (line.partPlaceDesc < .25) {
                  helperLine1.partPlaceDesc = line.partPlaceDesc * 4;
                  helperLine1.descDis = line.descDis;
                  helperLine1.rotDesc = line.rotDesc;
                  helperLine1.descScale = line.descScale;
                  helperLine1.showDesc = true;
                } else if (line.partPlaceDesc <= .75) {
                  helperLine2.partPlaceDesc = (line.partPlaceDesc - .25) * 2;
                  helperLine2.descDis = line.descDis;
                  helperLine2.rotDesc = line.rotDesc;
                  helperLine2.descScale = line.descScale;
                  helperLine2.showDesc = true;
                } else {
                  helperLine3.partPlaceDesc = (line.partPlaceDesc - .75) * 4;
                  helperLine3.descDis = line.descDis;
                  helperLine3.rotDesc = line.rotDesc;
                  helperLine3.descScale = line.descScale;
                  helperLine3.showDesc = true;
                }
              }
              helperLine1.draw(g, i == lines.size() && showCurrentSelectedLine);
              helperLine2.draw(g, i == lines.size() && showCurrentSelectedLine);
              helperLine3.draw(g, i == lines.size() && showCurrentSelectedLine);
            }
          } else {
            if (line.getStart().equals(line.getEnd())) {
              g.rotate(line.circAngle, line.getStartX(), line.getStartY());
            }
            line.calculateArch();

            double arch1 = -line.deltaArch / 4;
            double arch2 = -3 * line.deltaArch / 4;

            if (line.height < 0) {
              arch1 -= line.archStart;
              arch1 *= -1;
              arch1 += Math.PI / 2;
              arch2 -= line.archStart;
              arch2 *= -1;
            } else {
              arch1 -= line.archStart;
              arch1 *= -1;
              arch1 += Math.PI / 2;
              arch2 -= line.archStart;
              arch2 *= -1;
            }
            arch2 += Math.PI / 2;

            double pointX1 = +line.center.x - Math.signum(line.height) * line.radius * Math.sin(-arch1);
            double pointY1 = +line.center.y - Math.signum(line.height) * line.radius * Math.cos(-arch1);
            double pointX2 = +line.center.x - Math.signum(line.height) * line.radius * Math.sin(-arch2);
            double pointY2 = +line.center.y - Math.signum(line.height) * line.radius * Math.cos(-arch2);

            Point2D ankerPoint1 = new Point2D(pointX1, pointY1);
            Point2D ankerPoint2 = new Point2D(pointX2, pointY2);

            if (line.draggedMode == -1) {
              FeynLine helperLine1 = new FeynLine(start, ankerPoint1, helperLineConfigStart, FeynLine.getHeight(line.radius, start, ankerPoint1, line.center));
              FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, helperLineConfigHighlighted, FeynLine.getHeight(line.radius, ankerPoint1, ankerPoint2, line.center));
              FeynLine helperLine3 = new FeynLine(ankerPoint2, end, helperLineConfigEnd, FeynLine.getHeight(line.radius, ankerPoint2, end, line.center));
              helperLine1.showDesc = false;
              helperLine2.showDesc = false;
              helperLine3.showDesc = false;
              if (line.showDesc) {
                if (line.partPlaceDesc < .25) {
                  helperLine1.partPlaceDesc = line.partPlaceDesc * 4;
                  helperLine1.descDis = line.descDis;
                  helperLine1.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine1.rotDesc += line.circAngle;
                  }
                  helperLine1.descScale = line.descScale;
                  helperLine1.showDesc = true;
                } else if (line.partPlaceDesc <= .75) {
                  helperLine2.partPlaceDesc = (line.partPlaceDesc - .25) * 2;
                  helperLine2.descDis = line.descDis;
                  helperLine2.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine2.rotDesc += line.circAngle;
                  }
                  helperLine2.descScale = line.descScale;
                  helperLine2.showDesc = true;
                } else {
                  helperLine3.partPlaceDesc = (line.partPlaceDesc - .75) * 4;
                  helperLine3.descDis = line.descDis;
                  helperLine3.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine3.rotDesc += line.circAngle;
                  }
                  helperLine3.descScale = line.descScale;
                  helperLine3.showDesc = true;
                }
              }
              helperLine1.draw(g, i == lines.size() && showCurrentSelectedLine, true);
              helperLine2.draw(g, i == lines.size() && showCurrentSelectedLine, true);
              helperLine3.draw(g, i == lines.size() && showCurrentSelectedLine, true);
            } else if (line.draggedMode == 0) {
              FeynLine helperLine1 = new FeynLine(start, ankerPoint1, helperLineConfigHighlightedStart, FeynLine.getHeight(line.radius, start, ankerPoint1, line.center));
              FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, helperLineConfig, FeynLine.getHeight(line.radius, ankerPoint1, ankerPoint2, line.center));
              FeynLine helperLine3 = new FeynLine(ankerPoint2, end, helperLineConfigEnd, FeynLine.getHeight(line.radius, ankerPoint2, end, line.center));
              helperLine1.showDesc = false;
              helperLine2.showDesc = false;
              helperLine3.showDesc = false;
              if (line.showDesc) {
                if (line.partPlaceDesc < .25) {
                  helperLine1.partPlaceDesc = line.partPlaceDesc * 4;
                  helperLine1.descDis = line.descDis;
                  helperLine1.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine1.rotDesc += line.circAngle;
                  }
                  helperLine1.descScale = line.descScale;
                  helperLine1.showDesc = true;
                } else if (line.partPlaceDesc <= .75) {
                  helperLine2.partPlaceDesc = (line.partPlaceDesc - .25) * 2;
                  helperLine2.descDis = line.descDis;
                  helperLine2.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine2.rotDesc += line.circAngle;
                  }
                  helperLine2.descScale = line.descScale;
                  helperLine2.showDesc = true;
                } else {
                  helperLine3.partPlaceDesc = (line.partPlaceDesc - .75) * 4;
                  helperLine3.descDis = line.descDis;
                  helperLine3.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine3.rotDesc += line.circAngle;
                  }
                  helperLine3.descScale = line.descScale;
                  helperLine3.showDesc = true;
                }
              }
              helperLine1.draw(g, i == lines.size() && showCurrentSelectedLine, true);
              helperLine2.draw(g, i == lines.size() && showCurrentSelectedLine, true);
              helperLine3.draw(g, i == lines.size() && showCurrentSelectedLine, true);
            } else if (line.draggedMode == 1) {
              FeynLine helperLine1 = new FeynLine(start, ankerPoint1, helperLineConfigStart, FeynLine.getHeight(line.radius, start, ankerPoint1, line.center));
              FeynLine helperLine2 = new FeynLine(ankerPoint1, ankerPoint2, helperLineConfig, FeynLine.getHeight(line.radius, ankerPoint1, ankerPoint2, line.center));
              FeynLine helperLine3 = new FeynLine(ankerPoint2, end, helperLineConfigHighlightedEnd, FeynLine.getHeight(line.radius, ankerPoint2, end, line.center));
              helperLine1.showDesc = false;
              helperLine2.showDesc = false;
              helperLine3.showDesc = false;
              if (line.showDesc) {
                if (line.partPlaceDesc < .25) {
                  helperLine1.partPlaceDesc = line.partPlaceDesc * 4;
                  helperLine1.descDis = line.descDis;
                  helperLine1.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine1.rotDesc += line.circAngle;
                  }
                  helperLine1.descScale = line.descScale;
                  helperLine1.showDesc = true;
                } else if (line.partPlaceDesc <= .75) {
                  helperLine2.partPlaceDesc = (line.partPlaceDesc - .25) * 2;
                  helperLine2.descDis = line.descDis;
                  helperLine2.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine2.rotDesc += line.circAngle;
                  }
                  helperLine2.descScale = line.descScale;
                  helperLine2.showDesc = true;
                } else {
                  helperLine3.partPlaceDesc = (line.partPlaceDesc - .75) * 4;
                  helperLine3.descDis = line.descDis;
                  helperLine3.rotDesc = line.rotDesc;
                  if (line.getStart().equals(line.getEnd()) && Math.abs(line.height) > .01) {
                    helperLine3.rotDesc += line.circAngle;
                  }
                  helperLine3.descScale = line.descScale;
                  helperLine3.showDesc = true;
                }
              }
              helperLine1.draw(g, i == lines.size() && showCurrentSelectedLine, true);
              helperLine2.draw(g, i == lines.size() && showCurrentSelectedLine, true);
              helperLine3.draw(g, i == lines.size() && showCurrentSelectedLine, true);
            }

            if (line.getStart().equals(line.getEnd())) {
              g.rotate( - line.circAngle, line.getStartX(), line.getStartY());
            }
          }
        }
      }
    }

    Rectangle clRec = g.getClipBounds();

    /* draw helper circles for the center of all the objects despite lines*/
    i = 0;
    for (Vertex v : vertices) {
      i++;
      Shape shape = new Ellipse2D.Float((int) Math.round(v.origin.x) - 10, (int) Math.round(v.origin.y) - 10, 2 * 10, 2 * 10);
      g.setClip(shape);
      g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
      v.draw(g, i == vertices.size() && showCurrentSelectedLine && selectType == 1, true);
    }
    i = 0;
    for (FloatingObject fO : fObjects) {
      i++;
      if (fO.v) {
        Shape shape = new Ellipse2D.Float((int) Math.round(fO.vertex.origin.x) - 10, (int) Math.round(fO.vertex.origin.y) - 10, 2 * 10, 2 * 10);
        g.setClip(shape);
        g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
      } else {
        Shape shape = new Ellipse2D.Float((int) Math.round(fO.floatingImage.center.x) - 10, (int) Math.round(fO.floatingImage.center.y) - 10, 2 * 10, 2 * 10);
        g.setClip(shape);
        g.clipRect(clRec.x, clRec.y, clRec.width, clRec.height);
      }
      fO.draw(g, i == fObjects.size() && showCurrentSelectedLine && selectType == 2, true);
    }
    g.setClip(clRec);

    if (helperLines) {
      Color tmpColor = g.getColor();
      g.setColor(Color.BLACK);
      g.setXORMode(Color.WHITE);
      for (FloatingObject fO : fObjects) {
        if (fO.v) {
          g.drawOval((int) Math.round(fO.vertex.origin.x) - 10, (int) Math.round(fO.vertex.origin.y) - 10, 2 * 10, 2 * 10);
        } else {
          g.drawOval((int) Math.round(fO.floatingImage.center.x) - 10, (int) Math.round(fO.floatingImage.center.y) - 10, 2 * 10, 2 * 10);
        }
      }
      for (Vertex v : vertices) {
        g.drawOval((int) Math.round(v.origin.x) - 10, (int) Math.round(v.origin.y) - 10, 2 * 10, 2 * 10);
      }
      g.setColor(tmpColor);
    }

    g.setColor(new Color(1f, 0f, 0f, 1f));
    g.setStroke(new BasicStroke(5f));

    for (Point2D p : this.pointsOfError) {
      g.drawOval((int) Math.round(p.x) - 10, (int) Math.round(p.y) - 10, 20, 20);
    }

    g.dispose();
  }

  /**
   * Calculates the MBB size from the content
   */
  protected void calculateAutoMBBSize() {
    Rectangle rec = getBoundsPainted();
    if(rec.width == 0 && rec.height == 0) { //Just put it in the center
      rec = getBounds();
      xMBB = Math.floor(rec.getX() + rec.getWidth() * 0.25);
      yMBB = Math.floor(rec.getY() + rec.getHeight() * 0.25);
      wMBB = Math.ceil(rec.getWidth() * 0.5);
      hMBB = Math.ceil(rec.getHeight() * 0.5);
    } else {
      xMBB = rec.getX();
      yMBB = rec.getY();
      wMBB = Math.max(rec.getWidth(), 0);
      hMBB = Math.max(rec.getHeight(), 0);
    }
  }
  
  private Point2D alignToGrid(Point2D p) {
    if (grid) {
      int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
      int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
      x += Pane.gridSize / 2;
      y += Pane.gridSize / 2;
      if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= Frame.clipDistance) {
        return new Point2D(x, y);
      }
    }
    return p;
  }
  
  /**
   * Adds all lines and vertices fron the diagram to this Pane.
   * Clearing the existing lines must be called explicitly.
   * The graph has to have position information
   * @param graph The graph to put into the pane
   * @param refocus If true, the pane's zeroPoint will be set to center the current layout area
   * @param afterLayout True if this is after a diagram auto layout
   */
  public void putDiagramData(QgrafDiagram graph, boolean refocus, boolean afterLayout) {
    if(!graph.hasPositionData()) throw new IllegalArgumentException("Requires position data");
    
    Graph<GraphVertex, GraphEdge> g = graph.getAsGraph();
    LayoutModel2D<GraphVertex> m = graph.getPositionData();
    Map<GraphVertex, Point2D> vertexPos = new HashMap<>(); //real positions, possibly flipped

    
    for(QgrafDiagram.GraphVertex v : g.vertexSet()) {
      Point2D vertPos;
      org.jgrapht.alg.drawing.model.Point2D modelPos = m.get(v);
      if(GraphLayoutAlgorithm.flipDiagramOrientation()) {
        vertPos = new Point2D(modelPos.getY(), -modelPos.getX());
      } else {
        vertPos = new Point2D(modelPos.getX(), modelPos.getY());
      }
      if(afterLayout && GraphLayoutAlgorithm.gridAutoLayout()) vertPos = alignToGrid(vertPos);
      vertexPos.put(v, vertPos);

      if(!v.getType().isExternal()) vertices.add(v.getStyle().getVertex(vertPos));
    }

    for(GraphEdge edge : g.edgeSet()) {
      GraphVertex v1 = g.getEdgeSource(edge);
      GraphVertex v2 = g.getEdgeTarget(edge);
      lines.add(edge.getStyle().getFeynLine(vertexPos.get(v1), vertexPos.get(v2)));
    }

    if(refocus) focusOnLayoutArea();
    repaint();
  }
  
  public Box2D getDiagramLayoutArea() {
    if(!mBB) {
      if(GraphLayoutAlgorithm.flipDiagramOrientation()) {
        return new Box2D(-zeroPoint.y - getHeight() / scale, -zeroPoint.x,
            getHeight() / scale, getWidth() / scale);
      } else {
        return new Box2D(-zeroPoint.x, -zeroPoint.y, getWidth() / scale, getHeight() / scale);
      }
    } else {
      if(GraphLayoutAlgorithm.flipDiagramOrientation()) {
        return new Box2D(-yMBB - hMBB, xMBB, hMBB, wMBB);
      } else {
        return new Box2D(xMBB, yMBB, wMBB, hMBB);
      }
    }
  }
  
  /**
   * Changes the ZeroPoint such that the MBB is in the center of the screen
   */
  public void focusOnLayoutArea() {
    if(mBB) {
      double pw = getWidth() / scale;
      double ph = getHeight() / scale;
      
      zeroPoint.x = -(xMBB + wMBB/2.0 - pw / 2.0);
      zeroPoint.y = -(yMBB + hMBB/2.0 - ph / 2.0);
    } //else, the pane's current location IS the layout area
  }
  
  public void clearPane(int clearMode) { //Not all flags are supported here
    if((clearMode & Frame.CLEARMODE_CLEAR_LINES) != 0) {
      lines.clear();
      vertices.clear();
    }

    if((clearMode & Frame.CLEARMODE_CLEAR_SHAPES) != 0) {
      fObjects.clear();
      shapes.clear();

      game.Filling.imageNumberToImage = new HashMap<>();
      game.Filling.pathToImageNumber = new HashMap<>();
      game.Filling.nameToJVG = new HashMap<>();
    }

    if((clearMode & Frame.CLEARMODE_CLEAR_MBB) != 0) {
      mBB = false;
      xMBB = 0;
      yMBB = 0;
      wMBB = 0;
      hMBB = 0;
    }

    if((clearMode & Frame.CLEARMODE_RESET_PANE_STATE) != 0) {
      scale = 1;
      zeroPoint = new Point2D(0, 0);
    }
  }
  
  /**
   * Checks all {@link #lines} whether they satisfy the {@link game.LineConfig#rules} parsed from the config/model (or defined by default).
   *
   * @param gameMode the gameMode: 0: free drawing; 1 : InFinMode
   * @return report of the check, contains amongst other all errors.
   */
  ArrayList<String> checkLines(int gameMode) {

    ArrayList<String> errors = new ArrayList<>();

    Set<Point2D> vertices = new HashSet<>(); // no duplicates here
    HashMap<Point2D, Integer> outsideVertices = new HashMap<>(); // no duplicates here


    for (FeynLine line : lines) {
      for (LineConfig lc : LineConfig.presets) {
        if (lc.equals(line.lineConfig)) {
          line.lineConfig.identifier = new AbstractMap.SimpleEntry<String, String>(lc.identifier.getKey(), lc.identifier.getValue());
          break;
        }
      }

      vertices.add(line.getStart());
      vertices.add(line.getEnd());

      outsideVertices.put(line.getStart(), outsideVertices.containsKey(line.getStart()) ? outsideVertices.get(line.getStart()) + 1 : 1);
      outsideVertices.put(line.getEnd(), outsideVertices.containsKey(line.getEnd()) ? outsideVertices.get(line.getEnd()) + 1 : 1);
    }

    for (FeynLine line : inFinLines) {
      for (LineConfig lc : LineConfig.presets) {
        if (lc.equals(line.lineConfig)) {
          line.lineConfig.identifier = new AbstractMap.SimpleEntry<String, String>(lc.identifier.getKey(), lc.identifier.getValue());
          break;
        }
      }
      vertices.add(line.getStart());
      vertices.add(line.getEnd());

      outsideVertices.put(line.getStart(), outsideVertices.containsKey(line.getStart()) ? outsideVertices.get(line.getStart()) + 1 : 1);
      outsideVertices.put(line.getEnd(), outsideVertices.containsKey(line.getEnd()) ? outsideVertices.get(line.getEnd()) + 1 : 1);
    }

    int insideVertices = 0;
    for (Integer i : outsideVertices.values())
      if (i > 1) insideVertices++;

    errors.add("Checking " + lines.size() + " Lines and " + (insideVertices) + " Vertices:");


    for (Point2D p : vertices) {
      HashMap<String, Integer> count = new HashMap<>();

      for (FeynLine fl : lines) {
        Point2D pp1 = fl.getStart();
        Point2D pp2 = fl.getEnd();
        if (pp1.equals(p))
          count.put(fl.lineConfig.identifier.getKey(), (count.get(fl.lineConfig.identifier.getKey()) == null ? 0 : count.get(fl.lineConfig.identifier.getKey())) + 1);
        if (pp2.equals(p))
          count.put(fl.lineConfig.identifier.getValue(), (count.get(fl.lineConfig.identifier.getValue()) == null ? 0 : count.get(fl.lineConfig.identifier.getValue())) + 1);
      }

      for (FeynLine fl : inFinLines) {
        Point2D pp1 = fl.getStart();
        Point2D pp2 = fl.getEnd();
        if (pp1.equals(p))
          count.put(fl.lineConfig.identifier.getKey(), (count.get(fl.lineConfig.identifier.getKey()) == null ? 0 : count.get(fl.lineConfig.identifier.getKey())) + 1);
        if (pp2.equals(p))
          count.put(fl.lineConfig.identifier.getValue(), (count.get(fl.lineConfig.identifier.getValue()) == null ? 0 : count.get(fl.lineConfig.identifier.getValue())) + 1);
      }

      int sum = 0;
      for (int i : count.values())
        sum += i;
      if (sum == 1) // end/start vertex
        continue;
      // if (sum == 2)
      //  if (count.keySet().size() == 2) {
      //    boolean okey = false;
      //    String a = (String) count.keySet().toArray()[0];
      //    String b = (String) count.keySet().toArray()[1];
      //    for (LineConfig lc : LineConfig.presets.subList(0, LineConfig.presets.size() - 1))
      //      if (lc.identifier.getKey().equals(a) && lc.identifier.getValue().equals(b))
      //        okey = true;
      //      else if (lc.identifier.getValue().equals(a) && lc.identifier.getKey().equals(b))
      //        okey = true;
      //    if (okey)
      //      continue;
      //  }

      boolean found = false;
      for (Map.Entry<HashMap<String, Integer>, String> mask : LineConfig.rules)
        if (mask.getKey().equals(count)) {
          found = true;
          break;
        }

      if (!found) {
        StringBuilder con = new StringBuilder();
        for (int i = 0; i < count.size(); i++)
          con.append(new String(new char[(int) (count.values().toArray()[i])]).replace("\0", count.keySet().toArray()[i] + ", "));
        errors.add("Vertex {" + con.substring(0, con.length() - 2) + "} is not in model file.");

        this.pointsOfError.add(p);
      }
    }

    if (gameMode == 1) {
      if (!noEnds())
        errors.add("There are propagators whose ends are not connected to anything.");
    }

    if (this.lines.size() == 0 && this.inFinLines.size() == 0) {
      errors.add("There are no propagators in this diagram!");
      return errors;
    }

    if (!this.connected()) {
      errors.add("This diagram is not connected!");
    }

    return errors;
  }

  /**
   * Sets the location of the tooltip to the upper right corner
   */
  @Override
  public Point getToolTipLocation(MouseEvent e) {
    if (this.toolTipMsg == null) {
      ToolTipManager.sharedInstance().setEnabled(false);
    } else {
      ToolTipManager.sharedInstance().setEnabled(true);
    }
    return new Point(10, 10);
  }

  /**
   * Checks if there are any changes in the lines at the point that requires to add/remove vertices
   *
   * @param point the point to be checked
   */
  public void checkForVertexChangeLess(Point2D point) {

    boolean debug = false;

    if (debug) System.out.println("Checking for Vertex at (" + point.x + ", " + point.y + ")");

    point = new Point2D(Math.round(point.x), Math.round(point.y));
    int count = 0;
    String vIdentifier = "";
    HashMap<String, Integer> lIdentifiers = new HashMap<>();
    for (FeynLine fl : lines) {

      Point2D start = new Point2D((double) Math.round(fl.getStartX()), (double) Math.round(fl.getStartY()));
      Point2D end = new Point2D((double) Math.round(fl.getEndX()), (double) Math.round(fl.getEndY()));

      if ((start.x - point.x) * (start.x - point.x) + (start.y - point.y) * (start.y - point.y) < 1) {
        if(!fl.fixStart) {
          fl.setStartX(point.x);
          fl.setStartY(point.y);
        }
        count++;
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getKey(), (lIdentifiers.get(fl.lineConfig.identifier.getKey()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getKey())) + 1);
        } catch (Exception ignored) {
        }
      } else if ((end.x - point.x) * (end.x - point.x) + (end.y - point.y) * (end.y - point.y) < 1) {
        count++;
        if (!fl.fixEnd) {
          fl.setEndX(point.x);
          fl.setEndY(point.y);
        }
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getValue(), (lIdentifiers.get(fl.lineConfig.identifier.getValue()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getValue())) + 1);
        } catch (Exception ignored) {
        }
      }
    }

    for (FeynLine fl : inFinLines) {

      Point2D start = new Point2D((double) Math.round(fl.getStartX()), (double) Math.round(fl.getStartY()));
      Point2D end = new Point2D((double) Math.round(fl.getEndX()), (double) Math.round(fl.getEndY()));

      if ((start.x - point.x) * (start.x - point.x) + (start.y - point.y) * (start.y - point.y) < 1) {
        count++;
        if(!fl.fixStart) {
          fl.setStartX(point.x);
          fl.setStartY(point.y);
        }
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getKey(), (lIdentifiers.get(fl.lineConfig.identifier.getKey()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getKey())) + 1);
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      } else if ((end.x - point.x) * (end.x - point.x) + (end.y - point.y) * (end.y - point.y) < 1) {
        count++;
        if (!fl.fixEnd) {
          fl.setEndX(point.x);
          fl.setEndY(point.y);
        }
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getValue(), (lIdentifiers.get(fl.lineConfig.identifier.getValue()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getValue())) + 1);
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }

    for (Shaped s : this.shapes) {
      Map.Entry<Point2D, Boolean> start = s.nPoint(point, 1);
      if (start.getValue()) {
        if (debug) System.out.println("Shape is near!");
        count++;
      }
    }

    for (Map.Entry<HashMap<String, Integer>, String> mask : LineConfig.rules) {
      if (mask.getKey().equals(lIdentifiers)) {
        vIdentifier = mask.getValue();
        break;
      }
    }
    if (debug) System.out.println("Found " + count + " entities attached");

    int index = 0;
    for (Vertex vertex : vertices) {
      Point2D start = new Point2D((double) Math.round(vertex.origin.x), (double) Math.round(vertex.origin.y));

      if ((start.x - point.x) * (start.x - point.x) + (start.y - point.y) * (start.y - point.y) < 1)
        break;
      index++;
    }

    if (index == vertices.size() && count > 1 && vIdentifier.equals("")) {
      vertices.add(new Vertex(point, "default"));
      if (debug) System.out.println("index: " + index + "\nvertices.size(): " + vertices.size() + "\ncount:" + count);
    } else if (index == vertices.size() && count > 1) {
      if (debug) System.out.println("index: " + index + "\nvertices.size(): " + vertices.size() + "\ncount:" + count);
      vertices.add(new Vertex(point, vIdentifier));
      return;
    } else if (index != vertices.size() && count < 2) {
      if (debug) System.out.println("index: " + index + "\nvertices.size(): " + vertices.size() + "\ncount:" + count);
      if (vertices.get(index).autoremove)
        vertices.remove(index);
      return;
    }
  }

  /**
   * Checks if there are any changes in the lines at the point that requires to change/add/remove vertices
   *
   * @param point the point to be checked
   */
  public void checkForVertex(Point2D point) {

    boolean debug = false;

    if (debug) System.out.println("Checking for Vertex at (" + point.x + ", " + point.y + ")");

    point = new Point2D(Math.round(point.x), Math.round(point.y));
    int count = 0;
    String vIdentifier = "";
    HashMap<String, Integer> lIdentifiers = new HashMap<>();
    for (FeynLine fl : lines) {
      Point2D start = new Point2D((double) Math.round(fl.getStartX()), (double) Math.round(fl.getStartY()));
      Point2D end = new Point2D((double) Math.round(fl.getEndX()), (double) Math.round(fl.getEndY()));

      if ((start.x - point.x) * (start.x - point.x) + (start.y - point.y) * (start.y - point.y) < 1) {
        count++;
        if(!fl.fixStart) {
          fl.setStartX(point.x);
          fl.setStartY(point.y);
        }
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getKey(), (lIdentifiers.get(fl.lineConfig.identifier.getKey()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getKey())) + 1);
        } catch (Exception ignored) {
        }
      } else if ((end.x - point.x) * (end.x - point.x) + (end.y - point.y) * (end.y - point.y) < 1) {
        count++;
        if (!fl.fixEnd) {
          fl.setEndX(point.x);
          fl.setEndY(point.y);
        }
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getValue(), (lIdentifiers.get(fl.lineConfig.identifier.getValue()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getValue())) + 1);
        } catch (Exception ignored) {
        }
      }
    }

    for (FeynLine fl : inFinLines) {
      Point2D start = new Point2D((double) Math.round(fl.getStartX()), (double) Math.round(fl.getStartY()));
      Point2D end = new Point2D((double) Math.round(fl.getEndX()), (double) Math.round(fl.getEndY()));

      if ((start.x - point.x) * (start.x - point.x) + (start.y - point.y) * (start.y - point.y) < 1) {
        count++;
        if(!fl.fixStart) {
          fl.setStartX(point.x);
          fl.setStartY(point.y);
        }
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getKey(), (lIdentifiers.get(fl.lineConfig.identifier.getKey()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getKey())) + 1);
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      } else if ((end.x - point.x) * (end.x - point.x) + (end.y - point.y) * (end.y - point.y) < 1) {
        count++;
        if (!fl.fixEnd) {
          fl.setEndX(point.x);
          fl.setEndY(point.y);
        }
        try {
          lIdentifiers.put(fl.lineConfig.identifier.getValue(), (lIdentifiers.get(fl.lineConfig.identifier.getValue()) == null ? 0 : lIdentifiers.get(fl.lineConfig.identifier.getValue())) + 1);
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }

    for (Shaped s : this.shapes) {
      Map.Entry<Point2D, Boolean> start = s.nPoint(point, 1);
      if (start.getValue()) {
        if (debug) System.out.println("Shape is near!");
        count++;
      }
    }

    for (Map.Entry<HashMap<String, Integer>, String> mask : LineConfig.rules) {
      if (mask.getKey().equals(lIdentifiers)) {
        vIdentifier = mask.getValue();
        break;
      }
    }
    if (debug) System.out.println("Found " + count + " entities attached");

    int index = 0;
    for (Vertex vertex : vertices) {
      Point2D start = new Point2D((double) Math.round(vertex.origin.x), (double) Math.round(vertex.origin.y));

      if ((start.x - point.x) * (start.x - point.x) + (start.y - point.y) * (start.y - point.y) < 1)
        break;
      index++;
    }

    if (index == vertices.size() && count > 1 && vIdentifier.equals("")) {
      vertices.add(new Vertex(point, "default"));
      if (debug) System.out.println("index: " + index + "\nvertices.size(): " + vertices.size() + "\ncount:" + count);
    } else if (index == vertices.size() && count > 1) {
      if (debug) System.out.println("index: " + index + "\nvertices.size(): " + vertices.size() + "\ncount:" + count);
      vertices.add(new Vertex(point, vIdentifier));
      return;
    } else if (index != vertices.size() && count < 2) {
      if (debug) System.out.println("index: " + index + "\nvertices.size(): " + vertices.size() + "\ncount:" + count);
      if (vertices.get(index).autoremove)
        vertices.remove(index);
      return;
    }

    if (!vIdentifier.equals("") && vertices.get(index).autoremove) {
      boolean found = false;
      for (Map.Entry<Vertex, String> me : Vertex.vertexPresets) {
        if (me.getValue().equals(vIdentifier)) {
          found = true;
          break;
        }
      }
      if (!found)
        return;

      if (debug) System.out.println("Changing vertex style");
      Vertex v = new Vertex(point, vIdentifier);
      vertices.get(index).turnTo(v);
    }
  }

  /**
   * Checks if there is more than one vertex at the point and removes all but one
   *
   * @param point the point to be checked
   */
  public void checkForDoubleVertex(Point2D point) {
    int index = 0;
    ArrayList<Integer> indices = new ArrayList<>();
    for (Vertex vx : vertices) {
      if (vx.origin.equals(point)) {
        indices.add(index);
      }
      index++;
    }
    if (indices.size() >= 2) {
      for (int i = indices.size() - 2; i >= 0; i--) {
        int index1 = indices.get(i);
        if (vertices.get(index1).autoremove)
          vertices.remove(index1);
        else if (vertices.get(indices.get(indices.size() - 1)).autoremove)
          vertices.remove(indices.get(indices.size() - 1));
      }
    }
  }
  
  /**
   * Determines the relevant area of the pane. If there is no manual bounding box enabled, this will simply
   * be the area that the diagram is drawn on (see {@link #getBoundsPainted()}). Otherwise, it will be the
   * area covered by the manual bounding box
   * @return The relevant bounds of the pane, either selected by the user through the MBB or automatically determined.
   */
  public Rectangle2D getDiagramBounds() {
    if(mBB) {
      return new Rectangle2D.Double(xMBB, yMBB, wMBB, hMBB);
    } else {
      return getBoundsPainted();
    }
  }
  
  /**
   * function to determine the area on the canvas that is actually painted
   * on
   * @return the bounds in terms of a "Rectangle" object
   */
  public Rectangle getBoundsPainted() {
    double top = Double.MAX_VALUE;
    double bottom = - Double.MAX_VALUE;
    double right = - Double.MAX_VALUE;
    double left = Double.MAX_VALUE;

    for (Shaped s : this.shapes) {
      if (s.showDesc && s.description != null) {
        Point2D placeFOF = new Point2D(s.origin.x + s.descPosX, s.origin.y + s.descPosY);
        JLabel labelFOF = new JLabel(s.description, JLabel.CENTER);
        double labelWidthFOF = /*label.getPreferredSize().width*/labelFOF.getPreferredSize().width * Math.abs(Math.cos(s.rotDesc)) + labelFOF.getPreferredSize().height * Math.abs(Math.sin(s.rotDesc));
        double labelHeightFOF = /*label.getPreferredSize().height*/ labelFOF.getPreferredSize().width * Math.abs(Math.sin(s.rotDesc)) + labelFOF.getPreferredSize().height * Math.abs(Math.cos(s.rotDesc));
        if (!s.description.contains("<html>")) {
          TeXFormula formula = new TeXFormula(s.description);
              TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
            icon.setForeground(Color.BLACK);
            labelWidthFOF = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(s.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(s.rotDesc));
            labelHeightFOF = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(s.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(s.rotDesc));
        }
        labelHeightFOF *= s.descScale;
        labelWidthFOF *= s.descScale;

        top = Math.min(top, placeFOF.y - labelHeightFOF / 2);
        bottom = Math.max(bottom, placeFOF.y + labelHeightFOF / 2);
        right = Math.max(right, placeFOF.x + labelWidthFOF / 2);
        left = Math.min(left, placeFOF.x - labelWidthFOF / 2);
      }
      double hh = s.height / 2 * Math.cos( - s.rotation) + s.width / 2 * Math.sin( - s.rotation);
      double ww = s.width / 2 * Math.cos( - s.rotation) + s.height / 2 * Math.sin( - s.rotation);

      top = Math.min(top, s.origin.y - Math.abs(hh));
      bottom = Math.max(bottom, s.origin.y + Math.abs(hh));
      right = Math.max(right, s.origin.x + Math.abs(ww));
      left = Math.min(left, s.origin.x - Math.abs(ww));
    }

    for (FloatingObject fO : this.fObjects) {
      if (!fO.v && fO.floatingImage.name != null) {
        if (fO.floatingImage.showDesc && fO.floatingImage.description != null) {
          Point2D placeFOF = new Point2D(fO.floatingImage.center.x + fO.floatingImage.descPosX, fO.floatingImage.center.y + fO.floatingImage.descPosY);
          JLabel labelFOF = new JLabel(fO.floatingImage.description, JLabel.CENTER);
          double labelWidthFOF = /*label.getPreferredSize().width*/labelFOF.getPreferredSize().width * Math.abs(Math.cos(fO.floatingImage.rotDesc)) + labelFOF.getPreferredSize().height * Math.abs(Math.sin(fO.floatingImage.rotDesc));
          double labelHeightFOF = /*label.getPreferredSize().height*/ labelFOF.getPreferredSize().width * Math.abs(Math.sin(fO.floatingImage.rotDesc)) + labelFOF.getPreferredSize().height * Math.abs(Math.cos(fO.floatingImage.rotDesc));
          if (!fO.floatingImage.description.contains("<html>")) {
            TeXFormula formula = new TeXFormula(fO.floatingImage.description);
                TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
              icon.setForeground(Color.BLACK);
              labelWidthFOF = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(fO.floatingImage.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(fO.floatingImage.rotDesc));
              labelHeightFOF = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(fO.floatingImage.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(fO.floatingImage.rotDesc));
          }
          labelHeightFOF *= fO.floatingImage.descScale;
          labelWidthFOF *= fO.floatingImage.descScale;

          top = Math.min(top, placeFOF.y - labelHeightFOF / 2);
          bottom = Math.max(bottom, placeFOF.y + labelHeightFOF / 2);
          right = Math.max(right, placeFOF.x + labelWidthFOF / 2);
          left = Math.min(left, placeFOF.x - labelWidthFOF / 2);
        }
        double height = fO.floatingImage.scale * ((Filling.getJavaVectorGraphic(fO.floatingImage.name).maxY - Filling.getJavaVectorGraphic(fO.floatingImage.name).minY) * Math.cos(fO.floatingImage.rotation / 180 * Math.PI) + (Filling.getJavaVectorGraphic(fO.floatingImage.name).maxX - Filling.getJavaVectorGraphic(fO.floatingImage.name).minX) * Math.sin(fO.floatingImage.rotation / 180 * Math.PI));

        double width = fO.floatingImage.scale * ((Filling.getJavaVectorGraphic(fO.floatingImage.name).maxY - Filling.getJavaVectorGraphic(fO.floatingImage.name).minY) * Math.abs(Math.sin(fO.floatingImage.rotation / 180 * Math.PI)) + (Filling.getJavaVectorGraphic(fO.floatingImage.name).maxX - Filling.getJavaVectorGraphic(fO.floatingImage.name).minX) * Math.abs(Math.cos(fO.floatingImage.rotation / 180 * Math.PI)));

        top = Math.min(top, fO.floatingImage.center.y - Math.abs(height));
        bottom = Math.max(bottom, fO.floatingImage.center.y + Math.abs(height));
        right = Math.max(right, fO.floatingImage.center.x + Math.abs(width));
        left = Math.min(left, fO.floatingImage.center.x - Math.abs(width));
      } else if (fO.v) {
        if (fO.vertex.showDesc && fO.vertex.description != null) {
          Point2D placeFOV = new Point2D(fO.vertex.origin.x + fO.vertex.descPosX, fO.vertex.origin.y + fO.vertex.descPosY);
          JLabel labelFOV = new JLabel(fO.vertex.description, JLabel.CENTER);
          double labelWidthFOV = /*label.getPreferredSize().width*/labelFOV.getPreferredSize().width * Math.abs(Math.cos(fO.vertex.rotDesc)) + labelFOV.getPreferredSize().height * Math.abs(Math.sin(fO.vertex.rotDesc));
          double labelHeightFOV = /*label.getPreferredSize().height*/ labelFOV.getPreferredSize().width * Math.abs(Math.sin(fO.vertex.rotDesc)) + labelFOV.getPreferredSize().height * Math.abs(Math.cos(fO.vertex.rotDesc));
          if (!fO.vertex.description.contains("<html>")) {
            TeXFormula formula = new TeXFormula(fO.vertex.description);
                TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
              icon.setForeground(Color.BLACK);
              labelWidthFOV = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(fO.vertex.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(fO.vertex.rotDesc));
              labelHeightFOV = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(fO.vertex.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(fO.vertex.rotDesc));
          }
          labelHeightFOV *= fO.vertex.descScale;
          labelWidthFOV *= fO.vertex.descScale;

          top = Math.min(top, placeFOV.y - labelHeightFOV / 2);
          bottom = Math.max(bottom, placeFOV.y + labelHeightFOV / 2);
          right = Math.max(right, placeFOV.x + labelWidthFOV / 2);
          left = Math.min(left, placeFOV.x - labelWidthFOV / 2);
        }
        top = Math.min(top, fO.vertex.origin.y - fO.vertex.size - fO.vertex.border.stroke / 2);
        bottom = Math.max(bottom, fO.vertex.origin.y + fO.vertex.size + fO.vertex.border.stroke / 2);
        right = Math.max(right, fO.vertex.origin.x + fO.vertex.size + fO.vertex.border.stroke / 2);
        left = Math.min(left, fO.vertex.origin.x - fO.vertex.size - fO.vertex.border.stroke / 2);
      } else {
        if (fO.floatingImage.showDesc && fO.floatingImage.description != null) {
          Point2D placeFOI = new Point2D(fO.floatingImage.center.x + fO.floatingImage.descPosX, fO.floatingImage.center.y + fO.floatingImage.descPosY);
          JLabel labelFOI = new JLabel(fO.floatingImage.description, JLabel.CENTER);
          double labelWidthFOI = /*label.getPreferredSize().width*/labelFOI.getPreferredSize().width * Math.abs(Math.cos(fO.floatingImage.rotDesc)) + labelFOI.getPreferredSize().height * Math.abs(Math.sin(fO.floatingImage.rotDesc));
          double labelHeightFOI = /*label.getPreferredSize().height*/ labelFOI.getPreferredSize().width * Math.abs(Math.sin(fO.floatingImage.rotDesc)) + labelFOI.getPreferredSize().height * Math.abs(Math.cos(fO.floatingImage.rotDesc));
          if (!fO.floatingImage.description.contains("<html>")) {
            TeXFormula formula = new TeXFormula(fO.floatingImage.description);
                TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
              icon.setForeground(Color.BLACK);
              labelWidthFOI = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(fO.floatingImage.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(fO.floatingImage.rotDesc));
              labelHeightFOI = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(fO.floatingImage.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(fO.floatingImage.rotDesc));
          }
          labelHeightFOI *= fO.floatingImage.descScale;
          labelWidthFOI *= fO.floatingImage.descScale;

          top = Math.min(top, placeFOI.y - labelHeightFOI / 2);
          bottom = Math.max(bottom, placeFOI.y + labelHeightFOI / 2);
          right = Math.max(right, placeFOI.x + labelWidthFOI / 2);
          left = Math.min(left, placeFOI.x - labelWidthFOI / 2);
        }
        double height = fO.floatingImage.scale * (Filling.imageNumberToImage.get(fO.floatingImage.imageNumber).getHeight() * Math.cos(fO.floatingImage.rotation / 180 * Math.PI) + Filling.imageNumberToImage.get(fO.floatingImage.imageNumber).getWidth() * Math.sin(fO.floatingImage.rotation / 180 * Math.PI));
        double width = fO.floatingImage.scale * (Filling.imageNumberToImage.get(fO.floatingImage.imageNumber).getHeight() * Math.abs(Math.sin(fO.floatingImage.rotation / 180 * Math.PI)) + Filling.imageNumberToImage.get(fO.floatingImage.imageNumber).getWidth() * Math.abs(Math.cos(fO.floatingImage.rotation / 180 * Math.PI)));

        top = Math.min(top, fO.floatingImage.center.y - Math.abs(height));
        bottom = Math.max(bottom, fO.floatingImage.center.y + Math.abs(height));
        right = Math.max(right, fO.floatingImage.center.x + Math.abs(width));
        left = Math.min(left, fO.floatingImage.center.x - Math.abs(width));
      }
    }

    for (Vertex v : vertices) {
      if (v.showDesc && v.description != null) {
        Point2D placeV = new Point2D(v.origin.x + v.descPosX, v.origin.y + v.descPosY);
        JLabel labelV = new JLabel(v.description, JLabel.CENTER);
        double labelWidthV = /*label.getPreferredSize().width*/labelV.getPreferredSize().width * Math.abs(Math.cos(v.rotDesc)) + labelV.getPreferredSize().height * Math.abs(Math.sin(v.rotDesc));
        double labelHeightV = /*label.getPreferredSize().height*/ labelV.getPreferredSize().width * Math.abs(Math.sin(v.rotDesc)) + labelV.getPreferredSize().height * Math.abs(Math.cos(v.rotDesc));
        if (!v.description.contains("<html>")) {
          TeXFormula formula = new TeXFormula(v.description);
          TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
          icon.setForeground(Color.BLACK);
          labelWidthV = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(v.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(v.rotDesc));
          labelHeightV = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(v.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(v.rotDesc));
        }
        labelHeightV *= v.descScale;
        labelWidthV *= v.descScale;

        top = Math.min(top, placeV.y - labelHeightV / 2);
        bottom = Math.max(bottom, placeV.y + labelHeightV / 2);
        right = Math.max(right, placeV.x + labelWidthV / 2);
        left = Math.min(left, placeV.x - labelWidthV / 2);
      }
      top = Math.min(top, v.origin.y - v.size - v.border.stroke / 2);
      bottom = Math.max(bottom, v.origin.y + v.size + v.border.stroke / 2);
      right = Math.max(right, v.origin.x + v.size + v.border.stroke / 2);
      left = Math.min(left, v.origin.x - v.size - v.border.stroke / 2);
    }

    for (FeynLine fl : lines) {

      if (fl.isMom()) {
        Momentum mom = fl.momArrow;
        if (Math.abs(fl.height) < .1) {
          double len = Math.sqrt((fl.getStartX() - fl.getEndX()) * (fl.getStartX() - fl.getEndX()) + (fl.getStartY() - fl.getEndY()) * (fl.getStartY() - fl.getEndY()));

                double normtanx = - fl.getStartX() + fl.getEndX();
                normtanx /= len;
                double normtany = - fl.getStartY() + fl.getEndY();
                normtany /= len;

                double startPos;
                double endPos;
                if(!mom.invert) {
                    startPos = mom.position - mom.length / len / 2d;
                    endPos = mom.position + (mom.length - mom.tipSize) / len / 2d;
                } else {
                    startPos = mom.position - (mom.length - mom.tipSize) / len / 2d;
                    endPos = mom.position + mom.length / len / 2d;
                }

                Point2D start = new Point2D(
                    fl.getStartX() +
                    normtanx * len * startPos + normtany * mom.distance,
                    fl.getStartY() +
                    normtany * len * startPos - normtanx * mom.distance
                );
                Point2D end = new Point2D(
                    fl.getStartX() +
                    normtanx * len * endPos + normtany * mom.distance,
                    fl.getStartY() +
                    normtany * len * endPos - normtanx * mom.distance
                );

          java.awt.geom.Path2D.Double arrow = new java.awt.geom.Path2D.Double();
            double rotAngle = fl.angle;

                if (mom.invert)
                    rotAngle += Math.PI;

                Point2D upper = new Point2D(0d, ((double) mom.tipSize)/2d);
            Point2D lower = new Point2D(0d, - ((double) mom.tipSize)/2d);
            Point2D tip = new Point2D((double) mom.tipSize, 0d);
            upper.rotate(- rotAngle, 0, 0);
            lower.rotate(- rotAngle, 0, 0);
            tip.rotate(- rotAngle, 0, 0);

            arrow.moveTo((int) upper.x, (int) upper.y);
            arrow.lineTo((int) lower.x, (int) lower.y);
            arrow.lineTo((int) tip.x, (int) tip.y);

                AffineTransform tf = new AffineTransform();
                if (!mom.invert)
              tf.translate(end.x, end.y);
                else
                    tf.translate(start.x, start.y);
          arrow.transform(tf);

          top = Math.min(top, start.y - mom.stroke * normtany);
          top = Math.min(top, end.y - mom.stroke * normtany);
          bottom = Math.max(bottom, start.y + mom.stroke * normtany);
          bottom = Math.max(bottom, end.y + mom.stroke * normtany);
          right = Math.max(right, start.x + mom.stroke * normtanx);
          right = Math.max(right, end.x + mom.stroke * normtanx);
          left = Math.min(left, start.x - mom.stroke * normtanx);
          left = Math.min(left, end.x - mom.stroke * normtanx);

          Rectangle aBounds = arrow.getBounds();
          top = Math.min(top, aBounds.getY());
          bottom = Math.max(bottom, aBounds.getY() + aBounds.getHeight());
          left = Math.min(left, aBounds.getX());
          right = Math.max(right, aBounds.getX() + aBounds.getWidth());

          if (mom.showDesc && mom.description != null && !mom.description.equals("") && mom.descScale != 0) {
            if (mom.description.contains("<html>")) {
                Point2D place;
                JLabel label = new JLabel(mom.description, JLabel.CENTER);

                label.setForeground(Color.BLACK);
                label.setSize(label.getPreferredSize());
                double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.abs(Math.cos(mom.rotDesc)) + label.getPreferredSize().height * Math.abs(Math.sin(mom.rotDesc));
                double labelHeight = /*label.getPreferredSize().height*/ label.getPreferredSize().width * Math.abs(Math.sin(mom.rotDesc)) + label.getPreferredSize().height * Math.abs(Math.cos(mom.rotDesc));
                labelHeight *= mom.descScale;
                labelWidth *= mom.descScale;

                place = new Point2D(start.x + (mom.length * mom.partPlaceDesc) * normtanx + (mom.descDis) * normtany, start.y + (mom.length * mom.partPlaceDesc) * normtany - (mom.descDis) * normtanx);

              top = Math.min(top, place.y - labelHeight/2);
              bottom = Math.max(bottom, place.y + labelHeight/2);
              left = Math.min(left, place.x - labelWidth/2);
              right = Math.max(right, place.x + labelWidth/2);
                    } else {
                        Point2D place;
                        TeXFormula formula = new TeXFormula(mom.description);
                    TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
                  icon.setForeground(Color.BLACK);
                  double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(mom.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(mom.rotDesc));
                  double labelHeight = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(mom.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(mom.rotDesc));
                  labelHeight *= mom.descScale;
                  labelWidth *= mom.descScale;

                place = new Point2D(start.x + (mom.length * mom.partPlaceDesc) * normtanx + (mom.descDis) * normtany, start.y + (mom.length * mom.partPlaceDesc) * normtany - (mom.descDis) * normtanx);

              top = Math.min(top, place.y - labelHeight/2);
              bottom = Math.max(bottom, place.y + labelHeight/2);
              left = Math.min(left, place.x - labelWidth/2);
              right = Math.max(right, place.x + labelWidth/2);
                    }
                }
        } else {
          double len = fl.radius * fl.deltaArch;

          double radius = Math.abs(fl.radius) - Math.signum(fl.height) * mom.distance;

          double lenn = Math.signum(fl.height) * radius * fl.deltaArch;

          double startPos;
          double endPos;
          if(mom.invert) {
              startPos = mom.position - mom.length / lenn / 2d;
              endPos = mom.position + (mom.length - mom.tipSize) / lenn / 2d;
          } else {
              startPos = mom.position - (mom.length - mom.tipSize) / lenn / 2d;
              endPos = mom.position + mom.length / lenn / 2d;
          }

          Point2D start;
          Point2D end;


          double partArchStart = -fl.deltaArch * startPos;
          double partArchEnd = -fl.deltaArch * endPos;
          double placeArchStart = fl.archStart - partArchStart + Math.PI/2;
          double placeArchEnd = fl.archStart - partArchEnd + Math.PI/2;
          Arc2D.Double arc = new Arc2D.Double(
              (fl.center.x - radius),
              (fl.center.y - radius),
              (2d * radius),
              (2d * radius),
              ( - (placeArchStart) * 180d / Math.PI + 90d),
              ( - (placeArchEnd - placeArchStart) * 180d / Math.PI),
              Arc2D.OPEN
          );

          java.awt.geom.Path2D.Double arrow = new java.awt.geom.Path2D.Double();

          double rotAngle = 0;
          if (mom.invert) {
              rotAngle -= fl.deltaArch * ( - endPos + .5);
          } else {
              rotAngle -= fl.deltaArch * (- startPos + .5);
          }

          if (mom.invert)
              rotAngle += Math.PI;

          Point2D upper = new Point2D(0d, ((double) mom.tipSize)/2d);
          Point2D lower = new Point2D(0d, - ((double) mom.tipSize)/2d);
          Point2D tip = new Point2D((double) mom.tipSize, 0d);
          upper.rotate( - fl.angle, 0, 0);
          lower.rotate( - fl.angle, 0, 0);
          tip.rotate( - fl.angle, 0, 0);

          arrow.moveTo((int) upper.x, (int) upper.y);
          arrow.lineTo((int) lower.x, (int) lower.y);
          arrow.lineTo((int) tip.x, (int) tip.y);

              AffineTransform tf = new AffineTransform();
              if (mom.invert) {
            tf.translate(
                      ((Arc2D) arc).getEndPoint().getX(),
                      ((Arc2D) arc).getEndPoint().getY()
                  );
              } else {
            tf.translate(
                      ((Arc2D) arc).getStartPoint().getX(),
                      ((Arc2D) arc).getStartPoint().getY()
                  );
              }
              tf.rotate(rotAngle);
          arrow.transform(tf);

          if (fl.getStart().equals(fl.getEnd()) && Math.abs(fl.height) > .1) {
            AffineTransform aft = new AffineTransform();
            aft.setToRotation(fl.circAngle, fl.getStart().x, fl.getStart().y);
            arrow.transform(aft);
            Point2D originRot = new Point2D(fl.center.x, fl.center.y);
            originRot.rotate(- fl.circAngle, fl.getStartX(), fl.getStartY());
            arc = new Arc2D.Double(
                      (originRot.x - radius),
                      (originRot.y - radius),
                      (2d * radius),
                      (2d * radius),
                      ( - (placeArchStart + fl.circAngle) * 180d / Math.PI + 90d),
                      ( - (placeArchEnd - placeArchStart) * 180d / Math.PI),
                      Arc2D.PIE
                  );
          }
          Rectangle aBounds = arrow.getBounds();
          top = Math.min(top, aBounds.getY());
          bottom = Math.max(bottom, aBounds.getY() + aBounds.getHeight());
          left = Math.min(left, aBounds.getX());
          right = Math.max(right, aBounds.getX() + aBounds.getWidth());
          Rectangle2D bBounds = arc.getBounds2D();
          top = Math.min(top, bBounds.getY());
          bottom = Math.max(bottom, bBounds.getY() + bBounds.getHeight());
          left = Math.min(left, bBounds.getX());
          right = Math.max(right, bBounds.getX() + bBounds.getWidth());

          if (mom.showDesc && mom.description != null && !mom.description.equals("") && mom.descScale != 0) {

            if (mom.description.contains("<html>")) {

              Point2D place;
              JLabel label = new JLabel(mom.description, JLabel.CENTER);

              label.setForeground(Color.BLACK);
              label.setSize(label.getPreferredSize());
              double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.abs(Math.cos(mom.rotDesc)) + label.getPreferredSize().height * Math.abs(Math.sin(mom.rotDesc));
              double labelHeight = /*label.getPreferredSize().height*/ label.getPreferredSize().width * Math.abs(Math.sin(mom.rotDesc)) + label.getPreferredSize().height * Math.abs(Math.cos(mom.rotDesc));
              labelHeight *= mom.descScale;
              labelWidth *= mom.descScale;

              double deltaArch = placeArchEnd - placeArchStart;
              double partArch = deltaArch * mom.partPlaceDesc;
              double placeArch = placeArchStart + partArch - Math.signum(fl.height) * Math.PI/2;
              double lradius = radius - Math.signum(fl.height) * mom.descDis;
              double x;
              double y;
              if (fl.height > 0) {
                  y = fl.center.y + lradius * Math.sin(placeArch);
                  x = fl.center.x + lradius * Math.cos(placeArch);
              } else {
                  y = fl.center.y - lradius * Math.sin(placeArch);
                  x = fl.center.x - lradius * Math.cos(placeArch);
              }
              place = new Point2D(x, y);

              if (fl.getStart().equals(fl.getEnd())) {
                place.rotate(-fl.circAngle, fl.getStart().x, fl.getStart().y);
                top = Math.min(top, place.y - labelHeight/2);
                bottom = Math.max(bottom, place.y + labelHeight/2);
                left = Math.min(left, place.x - labelWidth/2);
                right = Math.max(right, place.x + labelWidth/2);
                } else {
                top = Math.min(top, place.y - labelHeight/2);
                bottom = Math.max(bottom, place.y + labelHeight/2);
                left = Math.min(left, place.x - labelWidth/2);
                right = Math.max(right, place.x + labelWidth/2);
                }
                    } else {
                        Point2D place;
                        TeXFormula formula = new TeXFormula(mom.description);
                    TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
                  icon.setForeground(Color.BLACK);
                  double labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(mom.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(mom.rotDesc));
                  double labelHeight = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(mom.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(mom.rotDesc));
                labelHeight *= mom.descScale;
                labelWidth *= mom.descScale;

                        double deltaArch = placeArchEnd - placeArchStart;
                        double partArch = deltaArch * mom.partPlaceDesc;
                        double placeArch = placeArchStart + partArch - Math.signum(fl.height) * Math.PI/2;
                        double lradius = radius - Math.signum(fl.height) * mom.descDis;
                        double x;
                        double y;
                        if (fl.height > 0) {
                            y = fl.center.y + lradius * Math.sin(placeArch);
                            x = fl.center.x + lradius * Math.cos(placeArch);
                        } else {
                            y = fl.center.y - lradius * Math.sin(placeArch);
                            x = fl.center.x - lradius * Math.cos(placeArch);
                        }
                        place = new Point2D(x, y);

                        if (fl.getStart().equals(fl.getEnd())) {
                  place.rotate(-fl.circAngle, fl.getStart().x, fl.getStart().y);
                top = Math.min(top, place.y - labelHeight/2);
                bottom = Math.max(bottom, place.y + labelHeight/2);
                left = Math.min(left, place.x - labelWidth/2);
                right = Math.max(right, place.x + labelWidth/2);
                } else {
                top = Math.min(top, place.y - labelHeight/2);
                bottom = Math.max(bottom, place.y + labelHeight/2);
                left = Math.min(left, place.x - labelWidth/2);
                right = Math.max(right, place.x + labelWidth/2);
                }
                    }
          }
        }
      }

      if (fl.showDesc && fl.lineConfig.description != null) {
        double len = Math.sqrt((fl.getStartX() - fl.getEndX()) * (fl.getStartX() - fl.getEndX()) + (fl.getStartY() - fl.getEndY()) * (fl.getStartY() - fl.getEndY()));
        Point2D place;
        if (Math.abs(fl.height) < .1) {

          double normtanx = fl.getStartX() - fl.getEndX();
          normtanx /= len;
          double normtany = fl.getStartY() - fl.getEndY();
          normtany /= len;
          place = new Point2D(fl.getStartX() - (len * fl.partPlaceDesc) * normtanx + (fl.descDis) * normtany, fl.getStartY() - (len * fl.partPlaceDesc) * normtany - (fl.descDis) * normtanx);
        } else {
          double partArch = fl.deltaArch * fl.partPlaceDesc;
          double placeArch = fl.archStart + partArch;
          double radius = fl.radius + fl.descDis;
          double x;
          double y;
          if (fl.height > 0) {
            y = fl.center.y + radius * Math.sin(placeArch);
            x = fl.center.x + radius * Math.cos(placeArch);
          } else {
            y = fl.center.y - radius * Math.sin(placeArch);
            x = fl.center.x - radius * Math.cos(placeArch);
          }
          place = new Point2D(x, y);
          if (fl.getStart().equals(fl.getEnd())) {
            place.rotate( - fl.circAngle, fl.getStartX(), fl.getStartY());
          }
        }
        JLabel label = new JLabel(fl.lineConfig.description, JLabel.CENTER);
        double labelWidth = /*label.getPreferredSize().width*/label.getPreferredSize().width * Math.abs(Math.cos(fl.rotDesc)) + label.getPreferredSize().height * Math.abs(Math.sin(fl.rotDesc));
        double labelHeight = /*label.getPreferredSize().height*/ label.getPreferredSize().width * Math.abs(Math.sin(fl.rotDesc)) + label.getPreferredSize().height * Math.abs(Math.cos(fl.rotDesc));
        if (!fl.lineConfig.description.contains("<html>")) {
          TeXFormula formula = new TeXFormula(fl.lineConfig.description);
              TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, latexSize);
            icon.setForeground(Color.BLACK);
            labelWidth = /*label.getPreferredSize().width*/icon.getIconWidth() * Math.abs(Math.cos(fl.rotDesc)) + icon.getIconHeight() * Math.abs(Math.sin(fl.rotDesc));
            labelHeight = /*label.getPreferredSize().height*/ icon.getIconWidth() * Math.abs(Math.sin(fl.rotDesc)) + icon.getIconHeight() * Math.abs(Math.cos(fl.rotDesc));
        }
        labelHeight *= fl.descScale;
        labelWidth *= fl.descScale;

        top = Math.min(top, place.y - labelHeight / 2);
        bottom = Math.max(bottom, place.y + labelHeight / 2);
        right = Math.max(right, place.x + labelWidth / 2);
        left = Math.min(left, place.x - labelWidth / 2);
      }
      if (Math.abs(fl.height) < .1) {
        double topFl = Math.min(fl.getStartY(), fl.getEndY());
        double bottomFl = Math.max(fl.getStartY(), fl.getEndY());
        double leftFl = Math.min(fl.getStartX(), fl.getEndX());
        double rightFl = Math.max(fl.getStartX(), fl.getEndX());
        double dirX = fl.getEndX() - fl.getStartX();
        double dirY = fl.getEndY() - fl.getStartY();
        double dis = dirX * dirX + dirY * dirY;
        dis = Math.sqrt(dis);
        if (fl.lineConfig.lineType == LineType.PHOTON || fl.lineConfig.lineType == LineType.GLUON || fl.lineConfig.lineType == LineType.SWAVE || fl.lineConfig.lineType == LineType.SSPIRAL) {
          double dirX2 = dirX / dis * fl.lineConfig.wiggleHeight;
          double dirY2 = dirY / dis * fl.lineConfig.wiggleHeight;
          topFl -= Math.abs(dirX2);
          bottomFl += Math.abs(dirX2);
          leftFl -= Math.abs(dirY2);
          rightFl += Math.abs(dirY2);
        }
        double dirX1 = dirX / dis * fl.lineConfig.stroke / 2;
        double dirY1 = dirY / dis * fl.lineConfig.stroke / 2;

        if (fl.lineConfig.showArrow && fl.lineConfig.stroke / 2 < fl.lineConfig.arrowSize * 7 / 10) {
          dirX1 = dirX / dis * fl.lineConfig.arrowSize * 7 / 10;
          dirY1 = dirY / dis * fl.lineConfig.arrowSize * 7 / 10;
        }
        topFl -= Math.abs(dirX1);
        bottomFl += Math.abs(dirX1);
        leftFl -= Math.abs(dirY1);
        rightFl += Math.abs(dirY1);
        top = Math.min(top, topFl);
        bottom = Math.max(bottom, bottomFl);
        right = Math.max(right, rightFl);
        left = Math.min(left, leftFl);
      } else {

        fl.calculateArch();

        double wiggleHeight = fl.lineConfig.stroke / 2;
        if (fl.lineConfig.lineType == LineType.PHOTON || fl.lineConfig.lineType == LineType.GLUON || fl.lineConfig.lineType == LineType.SWAVE || fl.lineConfig.lineType == LineType.SSPIRAL)
          wiggleHeight += fl.lineConfig.wiggleHeight;

        if (fl.lineConfig.showArrow && fl.lineConfig.arrowSize * 7 / 10 > fl.lineConfig.stroke / 2)
          wiggleHeight += fl.lineConfig.arrowSize * 7 / 10;

        if (fl.getStart().equals(fl.getEnd())) {
          fl.center.x -= fl.getStartX();
          fl.center.y -= fl.getStartY();
          double tmpX = fl.center.x;
          fl.center.x = (int) Math.round(tmpX * Math.cos( - fl.circAngle) + fl.center.y * Math.sin( - fl.circAngle));
          fl.center.y = (int) Math.round(tmpX * Math.sin( - fl.circAngle) + fl.center.y * Math.cos( - fl.circAngle));
          fl.center.x += fl.getStartX();
          fl.center.y += fl.getStartY();
        }

        double startX = fl.getStartX();
        double startY = fl.getStartY();
        double endX = fl.getEndX();
        double endY = fl.getEndY();
        double delStartX = fl.getStartX() - fl.center.x;
        double delEndX = fl.getEndX() - fl.center.x;
        double delStartY = fl.getStartY() - fl.center.y;
        double delEndY = fl.getEndY() - fl.center.y;
        double disStart = Math.sqrt(delStartX * delStartX + delStartY * delStartY);
        double disEnd = Math.sqrt(delEndX * delEndX + delEndY * delEndY);
        delEndX *= wiggleHeight / disEnd;
        delEndY *= wiggleHeight / disEnd;
        delStartX *= wiggleHeight / disStart;
        delStartY *= wiggleHeight / disStart;
        delEndX = Math.abs(delEndX);
        delEndY = Math.abs(delEndY);
        delStartY = Math.abs(delStartY);
        delStartX = Math.abs(delStartX);

        double topFl = Math.min(startY - delStartY, endY - delEndY);
        double bottomFl = Math.max(startY + delStartY, endY + delEndY);
        double rightFl = Math.max(startX + delStartX, endX + delEndX);
        double leftFl = Math.min(startX - delStartX, endX - delEndX);

        double archStart = fl.archStart;
        double archDis = fl.deltaArch;
        if (archStart < 0)
          archStart += 2 * Math.PI;
        if (archDis < 0)
          archDis += 2 * Math.PI;
        double deg90 = Math.PI / 2 - archStart;
        double deg180 = Math.PI - archStart;
        double deg270 = 3 * Math.PI / 2 - archStart;
        double deg360 = 2 * Math.PI - archStart;
        if (deg90 < 0)
          deg90 += 2 * Math.PI;
        if (deg180 < 0)
          deg180 += 2 * Math.PI;
        if (deg270 < 0)
          deg270 += 2 * Math.PI;
        if (deg360 < 0)
          deg360 += 2 * Math.PI;

        boolean bet90 = (deg90 < archDis);
        boolean bet180 = (deg180 < archDis);
        boolean bet270 = (deg270 < archDis);
        boolean bet360 = (deg360 < archDis);
        if (fl.height > 0) {
          if (!bet90) {
            bottomFl = Math.max(bottomFl, fl.center.y + Math.abs(fl.radius) + wiggleHeight);
          }
          if (!bet270) {
            topFl = Math.min(topFl, fl.center.y - Math.abs(fl.radius) - wiggleHeight);
          }
          if (!bet360) {
            rightFl = Math.max(rightFl, fl.center.x + Math.abs(fl.radius) + wiggleHeight);
          }
          if (!bet180) {
            leftFl = Math.min(leftFl, fl.center.x - Math.abs(fl.radius) - wiggleHeight);
          }
        } else {
          if (bet90) {
            bottomFl = Math.max(bottomFl, fl.center.y + Math.abs(fl.radius) + wiggleHeight);
          }
          if (bet270) {
            topFl = Math.min(topFl, fl.center.y - Math.abs(fl.radius) - wiggleHeight);
          }
          if (bet360) {
            rightFl = Math.max(rightFl, fl.center.x + Math.abs(fl.radius) + wiggleHeight);
          }
          if (bet180) {
            leftFl = Math.min(leftFl, fl.center.x - Math.abs(fl.radius) - wiggleHeight);
          }
        }
        top = Math.min(top, topFl);
        bottom = Math.max(bottom, bottomFl);
        right = Math.max(right, rightFl);
        left = Math.min(left, leftFl);
        if (fl.getStart().equals(fl.getEnd())) {
          fl.center.x -= fl.getStartX();
          fl.center.y -= fl.getStartY();
          double tmpX = fl.center.x;
          fl.center.x = (int) Math.round(tmpX * Math.cos(fl.circAngle) + fl.center.y * Math.sin(fl.circAngle));
          fl.center.y = (int) Math.round(tmpX * Math.sin(fl.circAngle) + fl.center.y * Math.cos(fl.circAngle));
          fl.center.x += fl.getStartX();
          fl.center.y += fl.getStartY();
        }
      }
    }
    if (right - left < 10) {
      right += 5;
      left -= 5;
    }

    if (bottom - top < 10) {
      top -= 5;
      bottom += 5;
    }
    return new Rectangle((int) Math.round(left - .05 * (right - left)), (int) Math.round(top - .05 * (bottom - top)), (int) Math.round(1.1 * (right - left)), (int) Math.round(1.1 * (bottom - top)));
  }

  /**
   * crops the pane to the dimension defined by rec
   *
   * @param rec the rectangle to wich the pane is cropped to
   */
  public void crop(Rectangle2D rec) {
    if(rec instanceof Rectangle) {
      crop((Rectangle) rec);
    } else {
      crop(new Rectangle((int) Math.ceil(rec.getX()), (int) Math.ceil(rec.getY()),
                         (int) Math.ceil(rec.getWidth()), (int) Math.ceil(rec.getHeight())));
    }
  }
  
  /**
   * crops the pane to the dimension defined by rec
   *
   * @param rec the rectangle to wich the pane is cropped to
   */
  public void crop(Rectangle rec) {
    for (Shaped s : this.shapes) {
      s.origin.x -= rec.x;
      s.origin.y -= rec.y;
    }
    for (FeynLine fl : lines) {
      fl.setStartX(fl.getStartX() - rec.x);
      fl.setStartY(fl.getStartY() - rec.y);
      fl.setEndX(fl.getEndX() - rec.x);
      fl.setEndY(fl.getEndY() - rec.y);
    }
    for (FeynLine fl : inFinLines) {
      fl.setStartX(fl.getStartX() - rec.x);
      fl.setStartY(fl.getStartY() - rec.y);
      fl.setEndX(fl.getEndX() - rec.x);
      fl.setEndY(fl.getEndY() - rec.y);
    }
    for (Vertex v : vertices) {
      v.origin.x -= rec.x;
      v.origin.y -= rec.y;
    }
    for (FloatingObject fO : fObjects) {
      if (fO.v) {
        fO.vertex.origin.x -= rec.x;
        fO.vertex.origin.y -= rec.y;
      } else {
        fO.floatingImage.center.x -= rec.x;
        fO.floatingImage.center.y -= rec.y;
      }
    }
    this.setSize(new Dimension(rec.width, rec.height));
    this.zeroPoint = new Point2D();
  }

  /**
   * checks if other vertices are connected to vertex with index v
   *
   * @param v the vertex to be checked
   */
  private void dfs(int v) {
    if (vis.get(v))
      return;
    vis.set(v, true);

    for (int k = 0; k < inFinLines.size(); k++) {
      FeynLine line = inFinLines.get(k);
      if (line.getStart().distance(vertices.get(v).origin) < 2 || line.getEnd().distance(vertices.get(v).origin) < 2)
        visInFinLines.set(k, true);
    }
    for (int j = 0; j < lines.size(); j++) {
      FeynLine line = lines.get(j);
      if (line.getEnd().distance(vertices.get(v).origin) < 2 || line.getStart().distance(vertices.get(v).origin) < 2) {
        visLine.set(j, true);
      }
      for (int i = 0; i < vertices.size(); i++) {
        if (!vis.get(i)) {
          if (line.getEnd().distance(vertices.get(i).origin) < 2 && line.getStart().distance(vertices.get(v).origin) < 2) {
            visLine.set(j, true);
            dfs(i);
          } else if (line.getStart().distance(vertices.get(i).origin) < 2 && line.getEnd().distance(vertices.get(v).origin) < 2) {
            visLine.set(j, true);
            dfs(i);
          }
        }
      }
    }
  }

  /**
   * checks if the diagram is connected
   *
   * @return true if the diagram is connected, false otherwise
   */
  private boolean connected() {
    vis = new ArrayList<>();
    for (Vertex v : vertices) {
      vis.add(false);
    }
    visLine = new ArrayList<>();
    for (FeynLine line : lines) {
      visLine.add(false);
    }
    visInFinLines = new ArrayList<>();
    for (FeynLine line : inFinLines) {
      visInFinLines.add(false);
    }
    if (vertices.size() == 0 && (lines.size() + inFinLines.size() > 1)) {
      return false;
    } else if (vertices.size() == 0 && lines.size() + inFinLines.size() == 1) {
      return true;
    }
    this.dfs(0);
    for (boolean v : vis) {
      if (!v) {
        return false;
      }
    }
    for (boolean v : visLine) {
      if (!v) {
        return false;
      }
    }
    for (boolean v : visInFinLines) {
      if (!v) {
        return false;
      }
    }
    return true;
  }

  /**
   * checks if there are any manually drawn propagators with loose ends
   *
   * @return true if there are no loose ends and false otherwise
   */
  private boolean noEnds() {
    for (FeynLine line : lines) {
      boolean noEnd = false;
      for (Vertex v : vertices) {
        if (line.getEnd().distance(v.origin) < 2 || line.fixEnd) {
          noEnd = true;
          break;
        }
      }
      if (!noEnd)
        return false;
      noEnd = false;
      for (Vertex v : vertices) {
        if (line.getStart().distance(v.origin) < 2 || line.fixStart) {
          noEnd = true;
          break;
        }
      }
      if (!noEnd)
        return false;
    }
    return true;
  }

  public void terminateThread() {
    if(updateThread != null) updateThread.interrupt();
  }
  
  /**
   * changes colors for bw printing
   */
  public void bw() {

    /* all lines black */
    for (FeynLine line : this.lines) {
      line.lineConfig.color = Color.BLACK;
    }
    for (FeynLine line : this.inFinLines) {
      line.lineConfig.color = Color.BLACK;
    }

    /* vertex borders black; filling is black for dark colors and white for bright colors*/
    for (Vertex v : this.vertices) {
      v.border.color = Color.BLACK;
      Color fColor = v.filling.color;
      if (fColor != null) {
        if (Math.sqrt(0.299 * fColor.getRed() * fColor.getRed() + 0.587 * fColor.getGreen() * fColor.getGreen() + 0.114 * fColor.getBlue() * fColor.getBlue()) > .5 * 254d) {
          v.filling.color = Color.WHITE;
        } else {
          v.filling.color = Color.BLACK;
        }
      }
    }

    for (FloatingObject fO : this.fObjects) {
      if (fO.v) {
        Vertex v = fO.vertex;
        v.border.color = Color.BLACK;
        Color fColor = v.filling.color;
        if (fColor != null) {
          if (Math.sqrt(0.299 * fColor.getRed() * fColor.getRed() + 0.587 * fColor.getGreen() * fColor.getGreen() + 0.114 * fColor.getBlue() * fColor.getBlue()) > .5 * 254d) {
            v.filling.color = Color.WHITE;
          } else {
            v.filling.color = Color.BLACK;
          }
        }
      }
    }
  }

  @Override
  public void print(Graphics g) {
    boolean restoreMBB = mBB;
    boolean restoreABB = Print.SHOW_AUTO_BOUNDS;
    Color restoreColor = getBackground();
    mBB = false;
    Print.SHOW_AUTO_BOUNDS = false;
    setBackground(Color.WHITE); 
    super.print(g);
    setBackground(restoreColor);
    mBB = restoreMBB;
    Print.SHOW_AUTO_BOUNDS = restoreABB;
  }
}
