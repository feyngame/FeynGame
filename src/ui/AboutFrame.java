//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Taskbar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import resources.GetResources;

/**
 * This class is the "About" window. It shows some basic informations about#
 * FeynGame
 * @author Sven Yannick Klein
 */
public class AboutFrame extends JFrame {
  private static final Logger LOGGER = Frame.getLogger(AboutFrame.class);
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 23L;
  /*
   * the scrollpane that holds the license text
   */
  JScrollPane license;
  /*
   * the button to show/not show the license
   */
  JButton expandButton;
  /*
   * a reference to the instance of this class itself
   */
  AboutFrame thisFrame;
  /*
   * this holds all the basic information about FeynGame
   */
  JEditorPane html;
  /*
   * basic constructor. constructs and shows this Window
   */
  public AboutFrame() {

    thisFrame = this;

    this.setResizable(false);

    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    this.setLayout(gridbag);
    c.weightx = 1;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = 0;
    c.insets = new Insets(0, 0, 0, 0);
    c.fill = (GridBagConstraints.HORIZONTAL);

    /* construct the panel that holds all the basic information*/
    html = new JEditorPane();
    html.setEditable(false);
    /* setting stylesheet to mimic the how readmes look on git (Why?
    just for fun). most of the rules are not supported however*/
    StyleSheet styleSheet = new StyleSheet();
    styleSheet.addRule("kbd {border-radius: 3px;padding: 5px 5px 5px;border: 1px outset #D3D3D3;box-shadow: 0px 1px #888888;font-family: \"Lucida Console\", Monaco, monospace;}");
    styleSheet.addRule("* {font-family: sans-serif;}");
    styleSheet.addRule("table {border-collapse: collapse;width : 100%;}");
    styleSheet.addRule("td {border: 1px solid #D3D3D3;padding: 15px;}");
    styleSheet.addRule("th {text-align: left;border: 1px solid #D3D3D3;padding: 15px;font-weight: normal;background-color : #F8F8F8;color: #2e2e2e;border-bottom: 2px solid #D3D3D3;}");
    styleSheet.addRule("h2 {display: block;font-size: 1.5em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h3 {display: block;font-size: 1.17em;margin-top: 1em;margin-bottom: 1em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h4 {display: block;font-size: 1em;margin-top: 1.33em;margin-bottom: 1.33em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h1 {display: block;font-size: 2em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");

    EditorKit kit = JEditorPane.createEditorKitForContentType("text/html");
    ((HTMLEditorKit) kit).setStyleSheet(styleSheet);
    HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();
    html.setEditorKit(kit);
    html.setDocument(doc);

    /* setting text for the panel that holds all basic information*/
    String nwln = System.getProperty("line.separator");
    html.setText("<h1>FeynGame</h1>" + nwln + "<br><p>Copyright (C) 2019-2024<br>by  Lars B&uuml;ndgen, Robert Harlander, Sven Yannick Klein, Maximilian Lipp, and Magnus Schaaf</p>" + nwln + "<p>This program comes with ABSOLUTELY NO WARRANTY</p>" + "<p>This is free software, and you are welcome to redistribute it under certain conditions</p>" + "<br><p>If you have questions, discovered bugs or have suggestions for improvement, please visit <u><a href=\"https://gitlab.com/feyngame/FeynGame\">our GitLab page</a></u>.</p><br><h3>Third party code</h3><br>\n\n&bull;<u><a href=\"https://github.com/opencollab/jlatexmath\">JLaTeXMath</a></u> (License: <u><a href=\"https://opensource.org/licenses/gpl-2.0.php\">GPL-2.0</a></u>)<br>&bull;<u><a href=\"https://jgrapht.org\">JGraphT</a></u> (License: <u><a href=\"https://jgrapht.org/#ack\">LGPL-2.1-or-later</a></u>)<br>&bull;<u><a href=\"http://www.jheaps.org\">JHeaps</a></u> (License: <u><a href=\"http://www.jheaps.org/about/\">Apache-2.0</a></u>)<br><br>\n\n<h3>Implemented algorithms</h3><br>\n\n&bull;an algorithm to determine the distance between an ellipse and a point by\n<u><a href=\"https://www.geometrictools.com/Documentation/DistancePointEllipseEllipsoid.pdf\">David Eberly</a></u><br>(License: <u><a href=\"http://creativecommons.org/licenses/by/4.0/\">Creative Commons Attribution 4.0 International License</a></u>)");

    /* needed to open the hyperlink to the gitlab page*/
    html.addHyperlinkListener(e -> {
      if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
        if (Desktop.isDesktopSupported()) {
          try {
            Desktop.getDesktop().browse(e.getURL().toURI());
          } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Could not open \"https://gitlab.com/feyngame/FeynGame\"", ex);
          }
        }
      }
    });

    this.add(html, c);

    expandButton = new JButton("Show License");
    expandButton.addActionListener(e -> {
      if (license.isVisible()) {
        expandButton.setText("Show License");
        license.setVisible(false);
      } else {
        expandButton.setText("Hide License");
        license.setVisible(true);
        license.setPreferredSize(new Dimension(html.getWidth(), (int) Math.round(html.getHeight() * 1.5)));
      }
      thisFrame.pack();
    });

    c.gridy++;
    this.add(expandButton, c);

    JTextPane licenseHTML = new JTextPane();
    licenseHTML.setEditable(false);

    StyleSheet styleSheet2 = new StyleSheet();
    styleSheet2.addRule("kbd {border-radius: 3px;padding: 5px 5px 5px;border: 1px outset #D3D3D3;box-shadow: 0px 1px #888888;font-family: \"Lucida Console\", Monaco, monospace;}");
    styleSheet2.addRule("* {font-family: sans-serif;}");
    styleSheet2.addRule("table {border-collapse: collapse;width : 100%;}");
    styleSheet2.addRule("td {border: 1px solid #D3D3D3;padding: 15px;}");
    styleSheet2.addRule("th {text-align: left;border: 1px solid #D3D3D3;padding: 15px;font-weight: normal;background-color : #F8F8F8;color: #2e2e2e;border-bottom: 2px solid #D3D3D3;}");
    styleSheet2.addRule("h2 {display: block;font-size: 1.5em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet2.addRule("h3 {display: block;font-size: 1.17em;margin-top: 1em;margin-bottom: 1em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet2.addRule("h4 {display: block;font-size: 1em;margin-top: 1.33em;margin-bottom: 1.33em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet2.addRule("h1 {display: block;font-size: 2em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");

    HTMLEditorKit kit2 = new HTMLEditorKit();
    kit2.setStyleSheet(styleSheet2);
    HTMLDocument doc2 = (HTMLDocument) kit2.createDefaultDocument();

    licenseHTML.setEditorKit(kit2);
    licenseHTML.setDocument(doc2);

    licenseHTML.setText(GetResources.getLicenseText());

    license = new JScrollPane(licenseHTML);

    license.setVisible(false);
    c.gridy++;
    this.add(license, c);

    this.setTitle("About FeynGame");
    this.pack();
    this.setLocationRelativeTo(null);
    this.setVisible(true);
    this.setResizable(false);
  }
}
