//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.*;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * This is the main part of the {@link ui.Frame}. It displays all lines from {@link #lines}.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
@SuppressWarnings("serial")
public class GridPanel extends JPanel {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 54355L;

    static int gridSpacing=3;

    Frame frame;

    GridPanel(Frame mainFrame) {
        this.frame = mainFrame;
    }

    @Override
    public void paintComponent(Graphics g2) {
        super.paintComponent(g2);
        this.setOpaque(true);
        this.setBackground(Color.WHITE);
        Graphics2D g = (Graphics2D) g2.create();

        // anti-aliasing decreases grid performance drastically
        // RenderingHints rh = new RenderingHints(
        //       RenderingHints.KEY_ANTIALIASING,
        //       RenderingHints.VALUE_ANTIALIAS_ON);
        // g.setRenderingHints(rh);

        g.scale(this.frame.getDrawPane().scale, this.frame.getDrawPane().scale);
        g.translate(this.frame.getDrawPane().zeroPoint.x, this.frame.getDrawPane().zeroPoint.y);

        /* draw Grid */
        if (this.frame.getDrawPane().grid) {
          Stroke oldS = g.getStroke();
          Color oldC = g.getColor();
          int gridDotSize = this.frame.getDrawPane().gridSize / 5;
          if (gridDotSize > 6) {
            gridDotSize = 6;
          }

          g.setStroke(new BasicStroke(0.5f));
          int xi = 0;
          for (int x = this.frame.getDrawPane().gridSize / 2 - (int) Math.round(this.frame.getDrawPane().zeroPoint.x / this.frame.getDrawPane().gridSize) * this.frame.getDrawPane().gridSize; x < this.getWidth() / this.frame.getDrawPane().scale - this.frame.getDrawPane().zeroPoint.x; x += this.frame.getDrawPane().gridSize) {
            int yi = 0;
            for (int y = this.frame.getDrawPane().gridSize / 2 - (int) Math.round(this.frame.getDrawPane().zeroPoint.y / this.frame.getDrawPane().gridSize) * this.frame.getDrawPane().gridSize; y < this.getHeight() / this.frame.getDrawPane().scale - this.frame.getDrawPane().zeroPoint.y; y += this.frame.getDrawPane().gridSize) {
                if (xi%(GridPanel.gridSpacing+1) == 0 && yi%(GridPanel.gridSpacing+1) == 0) {
                  g.setColor(Color.darkGray);
                  g.fillRect(x - gridDotSize / 2 - 1, y - gridDotSize / 2 - 1, gridDotSize + 1, gridDotSize + 1);
                } else {
                  g.setColor(Color.lightGray);
                  g.fillRect(x - gridDotSize / 2, y - gridDotSize / 2, gridDotSize, gridDotSize);
                }
              yi++;
            }
            xi++;
          }
          g.setStroke(oldS);
          g.setColor(oldC);
        }
    }
}
