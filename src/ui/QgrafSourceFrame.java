package ui;


import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import qgraf.QgrafDiagram;
import resources.GetResources;

public class QgrafSourceFrame extends JFrame {
  private static final long serialVersionUID = 1L;
  
  private final Frame parent;
  private final JScrollPane scrollPane;
  private final JTextArea textArea;
  
  public QgrafSourceFrame(Frame parent, boolean alwaysOnTop) {
    super("qgraf output file");
    this.parent = parent;
    setIconImage(GetResources.getAppIcon());
    setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    
    textArea = new JTextArea();
    textArea.setEditable(false);
    scrollPane = new JScrollPane(textArea);
    add(scrollPane);
    
    setAlwaysOnTop(alwaysOnTop);
    pack();
    setLocationRelativeTo(parent);
    setSize(600, 400);
  }
  
  public void updateSourceCode(QgrafDiagram diagram) {
    if((diagram == null || !diagram.hasSourceText())) {
      textArea.setText("");
      setVisible(false);
    } else {
      textArea.setText(diagram.getSourceText());
    }
  }
}
