//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.*;
import javafx.JavaFXToJavaVectorGraphic;
import resources.GetResources;

import org.scilab.forge.jlatexmath.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Objects;
import java.awt.event.*;

/**
 * the panel that holds all the components for editing the selected object
 *
 * @author Sven Yannick Klein
 */
public class EditPane extends JPanel {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 11L;
  /**
   * The combo box to select a new type
   */
  JComboBox<String> type;
  /**
   * The {@link ui.CustComboBoxModel} that holds the possible {@link game.Vertex#shape}s
   */
  CustComboBoxModel<String> vertModel;
  /**
   * The {@link ui.CustComboBoxModel} that holds the possible {@link game.LineType}
   */
  CustComboBoxModel<String> lineModel;
  /**
   * A button that directs to a JColorChooser for {@link game.Filling#color}
   */
  JButton vFillColor;
  /**
   * A button that directs to a JFileChooser for showing an Image in the {@link game.Filling} of a {@link game.Vertex}
   */
  JButton vFillImg;
  /**
   * A button that directs to a JColorChooser for {@link game.Border#color}
   */
  JButton vBorderColor;
  /**
   * A button that directs to a JColorChooser for {@link game.LineConfig#color}
   */
  JButton lColor;
  /**
   * The JCheckBox for drawing the line/border of vertex dashed
   */
  JCheckBox dashed;
  /**
   * The SpinnerNumberModel for the {@link #dashed} JCheckBox
   */
  SpinnerNumberModel numModel;
  /**
   * JSpinner controlling the dashLength of the border of a vertex or e line
   */
  JSpinner dashLength;
  /**
   * Component that holds all subcomponents ragarding dashes
   */
  JPanel dashComp;
  /**
   * The SpinnerNumberModel for the {@link #stroke}
   */
  SpinnerNumberModel numModelStroke;
  /**
   * The JSpinner contolling the stroke of the line/border of vertex
   */
  JSpinner stroke;
  /**
   * A Label indicating that {@link #stroke} does
   */
  JLabel strokeLabel;
  /**
   * A component holding all subcomponents regarding the stroke
   */
  JPanel strokeComp;
  /**
   * the {@link SpinnerNumberModel} for {@link #heightSize}
   */
  SpinnerNumberModel numModelHeightSize;
  /**
   * the JSpinner controlling the {@link game.FeynLine#height} or the {@link game.Vertex#size}
   */
  JSpinner heightSize;
  /**
   * a label indicating what {@link #heightSize} does
   */
  JLabel heightSizeLabel;
  /**
   * the component holding every subcomponent regarding size/height
   */
  JPanel heightSizeComp;
  /**
   * the component to select a preset for lines
   */
  PresetSelectLines preSelLin;
  /**
   * the component to select a preset for vertices
   */
  PresetSelectVertex preSelVer;
  /**
   * the component to select a preset for images
   */
  PresetSelectImage preSelImg;
  /**
   * the component holdingeverything connected to the arrow
   */
  JPanel arrowComp;
  /**
   * toggle showing of arrow
   */
  JCheckBox showArrow;
  /**
   * the JSpinner controlling the {@link game.LineConfig#arrowSize}
   */
  JSpinner arrowSize;
  /**
   * the {@link SpinnerNumberModel} for {@link #arrowSize}
   */
  SpinnerNumberModel numModelArrowSize;
    /**
   * the JSpinner controlling the {@link game.LineConfig#arrowDent}
   */
  JSpinner arrowDent;
  /**
   * a label indicating what {@link #arrowDent} does
   */
  JLabel arrowDentLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #arrowDent}
   */
  SpinnerNumberModel numModelArrowDent;
    /**
   * the JSpinner controlling the {@link game.LineConfig#arrowHeight}
   */
  JSpinner arrowHeight;
  /**
   * a label indicating what {@link #arrowHeight} does
   */
  JLabel arrowHeightLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #arrowHeight}
   */
  SpinnerNumberModel numModelArrowHeight;
  /**
   * the JSpinner controlling the {@link game.LineConfig#arrowSize}
   */
  JSpinner arrowPlace;
  /**
   * a label indicating what {@link #arrowSize} does
   */
  JLabel arrowPlaceLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #arrowPlace}
   */
  SpinnerNumberModel numModelArrowPlace;
  /**
   * the JSpinner controlling the {@link game.LineConfig#wiggleHeight}
   */
  JSpinner wiggleHeight;
  /**
   * a label indicating what {@link #wiggleHeight} does
   */
  JLabel wiggleHeightLabel;
  /**
   * the component holding every subcomponent regarding the wiggleHeight
   */
  JPanel wiggleHeightComp;
  /**
   * the {@link SpinnerNumberModel} for {@link #wiggleHeight}
   */
  SpinnerNumberModel numModelWiggleHeight;
  /**
   * the JSpinner controlling the {@link game.LineConfig#wiggleLength}
   */
  JSpinner wiggleLength;
  /**
   * a label indicating what {@link #wiggleLength} does
   */
  JLabel wiggleLengthLabel;
  /**
   * the component holding every subcomponent regarding wiggleLength
   */
  JPanel wiggleLengthComp;
  /**
   * the {@link SpinnerNumberModel} for {@link #wiggleLength}
   */
  SpinnerNumberModel numModelWiggleLength;
  /**
   * the JSpinner controlling the {@link game.LineConfig#wiggleOffset}
   */
  JSpinner wiggleOffset;
  /**
   * a label indicating what {@link #wiggleOffset} does
   */
  JLabel wiggleOffsetLabel;
  /**
   * the component holding every subcomponent regarding wiggleOffset
   */
  JPanel wiggleOffsetComp;
  /**
   * the {@link SpinnerNumberModel} for {@link #wiggleOffset}
   */
  SpinnerNumberModel numModelWiggleOffset;
  /**
   * the JSpinner controlling the {@link game.LineConfig#wiggleAsymmetry}
   */
  JSpinner wiggleAsymmetry;
  /**
   * a label indicating what {@link #wiggleAsymmetry} does
   */
  JLabel wiggleAsymmetryLabel;
  /**
   * the component holding every subcomponent regarding wiggleAsymmetry
   */
  JPanel wiggleAsymmetryComp;
  /**
   * the {@link SpinnerNumberModel} for {@link #wiggleAsymmetry}
   */
  SpinnerNumberModel numModelWiggleAsymmetry;
  /**
   * Shows the text message if nothing is selected;
   */
  JPanel emptyPane;
  /**
   * the Button to invert the current line
   */
  JButton invButton;
  /**
   * the JSpinner controlling the x-component of the position of a vertex or an image
   */
  JSpinner posX;
  /**
   * a label indicating what {@link #posX} does
   */
  JLabel posXLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #posX}
   */
  SpinnerNumberModel numModelPosX;
  /**
   * the JSpinner controlling the y-component of the position of a vertex or an image or the Start of a line
   */
  JSpinner posY;
  /**
   * a label indicating what {@link #posY} does
   */
  JLabel posYLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #posY}
   */
  SpinnerNumberModel numModelPosY;
  /**
   * Holds all the Components regarding the position of a vertex
   */
  JPanel posComp;
  /**
   * the JSpinner controlling the x-component of the end of a line
   */
  JSpinner endPosX;
  /**
   * a label indicating what {@link #endPosX} does
   */
  JLabel endPosXLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #endPosX}
   */
  SpinnerNumberModel numModelEndPosX;
  /**
   * the JSpinner controlling the y-component of the End of a line
   */
  JSpinner endPosY;
  /**
   * a label indicating what {@link #endPosY} does
   */
  JLabel endPosYLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #endPosY}
   */
  SpinnerNumberModel numModelEndPosY;

  /**
   * Holds all the Components regarding the height of a shape
   */
  JPanel heightSComp;
  /**
   * the JSpinner controlling the height of a shapes
   */
  JSpinner heightS;
  /**
   * a label indicating what {@link #heightS} does
   */
  JLabel heightSLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #endPosY}
   */
  SpinnerNumberModel numModelHeightS;


  /**
   * Holds all the Components regarding the length of the straight part of gluon/photon lines
   */
  JPanel straightLenComp;
  /**
   * the JSpinner controlling the length of the straight part of gluon/photon lines
   */
  JSpinner straightLen;
  /**
   * a label indicating what {@link #straightLen} does
   */
  JLabel straightLenLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #straightLen}
   */
  SpinnerNumberModel numModelStraightLen;

  /**
   * Holds all the Components regarding the length of the straight part of gluon/photon lines at the arrows
   */
  JPanel straightLenArrowComp;
  /**
   * the JSpinner controlling the length of the straight part of gluon/photon lines at the arrows
   */
  JSpinner straightLenArrow;
  /**
   * a label indicating what {@link #straightLenArrow} does
   */
  JLabel straightLenArrowLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #straightLenArrow}
   */
  SpinnerNumberModel numModelStraightLenArrow;

  /**
   * Holds all the Components regarding the end Position of a line
   */
  JPanel endPosComp;
  /**
   * determines, if the line is drawn as double
   */
  JCheckBox doubleLine;
  /**
   * select a JavaFx preset for filling of vector
   */
  JComboBox<Integer> pattern;
  /**
   * The {@link ui.CustComboBoxModel} that holds the possible javafx presets
   */
  CustComboBoxModel<String> patternModel;
  /**
   * the component holding everything together regarding the description/label
   */
  JPanel descriptionPanel;
  /**
   * the Textfield displaying the description/label
   */
  JTextField descriptionField;
  /**
   * toggle displaying of discription
   */
  JCheckBox showDesc;
  /**
   * toggle autoremove for vertices
   */
  JCheckBox autoremove;
  /**
   * contol rotation of description/label
   */
  JSpinner rotDesc;
  /**
   * control distance between description/label and line // x-comp relative to vertex/image
   */
  JSpinner descDis;
  /**
   * control the point on the path the description/label is displayed at // y-comp relative to vertex/image
   */
  JSpinner partPlaceDesc;
  /**
   * control the scale of the description/label
   */
  JSpinner descScale;
  /**
   * component that holds the description/label
   */
  JPanel descPanel;
  /**
   * saves the description parameters as default
   */
  JButton defaultButton;
  /**
   * Label for partPlaceDesc
   */
  JLabel partLabel;
  /**
   * Label for descDis
   */
  JLabel disLabel;
  /**
   * number model for partPlaceDesc
   */
  SpinnerNumberModel numModelPart;
  /**
   * shows wether the actionListners should do something
   */
  private Boolean updating;
  /**
   * the Frame all the lines are drawn on
   */
  private Frame mainFrame;
  /**
   * the last Value of the {@link #numModel} since it was last changed
   */
  private Object lastValueDashSpinner = null;
  /**
   * the last value of the {@link #numModelStroke} since it was last changed
   */
  private Object lastValueDashSpinnerStroke = null;
  /**
   * the last value of {@link #numModelHeightSize} since it was last changed
   */
  private Object lastValueDashSpinnerHeightSize = null;
  /**
   * the last value of {@link #numModelArrowSize} since it was last changed
   */
  private Object lastValueDashSpinnerArrowSize = null;
  /**
   * the last value of {@link #numModelArrowDent} since it was last changed
   */
  private Object lastValueDashSpinnerArrowDent = null;
  /**
   * the last value of {@link #numModelArrowHeight} since it was last changed
   */
  private Object lastValueDashSpinnerArrowHeight = null;
  /**
   * the last value of {@link #numModelArrowPlace} since it was last changed
   */
  private Object lastValueDashSpinnerArrowPlace = null;
  /**
   * the last value of {@link #numModelWiggleHeight} since it was last changed
   */
  private Object lastValueDashSpinnerWiggleHeight = null;
  /**
   * the last value of {@link #numModelWiggleLength} since it was last changed
   */
  private Object lastValueDashSpinnerWiggleLength = null;
  /**
   * the last value of {@link #numModelWiggleOffset} since it was last changed
   */
  private Object lastValueDashSpinnerWiggleOffset = null;
  /**
   * the last value of {@link #numModelWiggleAsymmetry} since it was last changed
   */
  private Object lastValueDashSpinnerWiggleAsymmetry = null;
  /**
   * the last value of {@link #numModelPosX} since it was last changed
   */
  private Object lastValueDashSpinnerPosX = null;
  /**
   * the last value of {@link #numModelPosY} since it was last changed
   */
  private Object lastValueDashSpinnerPosY = null;
  /**
   * the last value of {@link #numModelPosX} since it was last changed
   */
  private Object lastValueDashSpinnerEndPosX = null;
  /**
   * the last value of {@link #numModelPosY} since it was last changed
   */
  private Object lastValueDashSpinnerEndPosY = null;
  /**
   * the last value of {@link #numModelHeightS} since it was last changed
   */
  private Object lastValueDashSpinnerHeightS = null;
  /**
   * the last value of {@link #numModelStraightLen} since it was last changed
   */
  private Object lastValueDashSpinnerStraightLen = null;
  /**
   * the last value of {@link #numModelStraightLenArrow} since it was last changed
   */
  private Object lastValueDashSpinnerStraightLenArrow = null;
  /**
   * the JSpinner controlling the orientation of a closed line loop
   */
  JSpinner circAngle;
  /**
   * a label indicating what {@link #circAngle} does
   */
  JLabel circAngleLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #circAngle}
   */
  SpinnerNumberModel numModelCircAngle;
  /**
   * the last value of {@link #numModelCircAngle} since it was last changed
   */
  private Object lastValueDashSpinnerCircAngle = null;
  /**
   * The component holding everything regarding the orientation of closed loops
   */
  JPanel circAngleComp;
  /**
   * the component for the top
   */
  JPanel top;
  /**
   * the config panel for the different types
   */
  JPanel configPanel;
  /**
   * the momArrowConfig
   */
  MomArrowConf mAC;
  /**
   * The component holding everything regarding scale of the filling of Shapes/Vertices
   */
  JPanel fillScaleComp;
  /**
   * the JSpinner controlling the orientation of a closed line loop
   */
  JSpinner fillScale;
  /**
   * a label indicating what {@link #fillScale} does
   */
  JLabel fillScaleLabel;
  /**
   * the {@link SpinnerNumberModel} for {@link #fillScale}
   */
  SpinnerNumberModel numModelFillScale;
  /**
   * the last value of {@link #numModelFillScale} since it was last changed
   */
  private Object lastValueDashSpinnerFillScale = null;
  /**
   * the panel holding all the components for the label/description for multiEdits
   */
  JPanel meDescPanel;
  /**
   * the textfield holding the label/description for multiEdits
   */
  JTextField meDescField;
  /**
   * the label for the label/description for multiEdits
   */
  JLabel meDescLabel;
  /**
   * contructor: initializes all subcommponents and lays them out
   * @param mainFrame the main Window (@link ui.Frame)
   * @param mAC the panel to edit the momentum arrow (@link ui.MomArrowConf)
   */
  public EditPane(Frame mainFrame, MomArrowConf mAC) {

    updating = false;

    this.mAC = mAC;

    /*This is what is displayed if no object is selected*/

    emptyPane = new JPanel(new GridBagLayout());

    JLabel emptyMsg = new JLabel(" Select an Object");
    Font currentFont = emptyMsg.getFont();
    Font newFont = currentFont.deriveFont(currentFont.getSize() * 4F);
    emptyMsg.setFont(newFont);
    emptyPane.add(emptyMsg);
    this.add(emptyPane, LEFT_ALIGNMENT);

    this.mainFrame = mainFrame;

    this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

    /* Now come the components in the upper part of the window*/

    vertModel = new CustComboBoxModel<>(Vertex.shapes);
    lineModel = new CustComboBoxModel<>(LineType.types);
    type = new JComboBox<>();
    type.addActionListener(e -> {
      if (mainFrame.getDrawPane().selectType == 0 && !updating) {

        String typeString = (String) Objects.requireNonNull(type.getSelectedItem());

        if (typeString.toUpperCase().equals("PLAIN"))
          typeString = "FERMION";

        if (typeString.toUpperCase().equals("SPIRAL"))
          typeString = "GLUON";

        if (typeString.toUpperCase().equals("WAVE"))
          typeString = "PHOTON";

        if (typeString.toUpperCase().equals("SWAVE"))
          typeString = "PHOTINO";

        if (typeString.toUpperCase().equals("SSPIRAL"))
          typeString = "GLUINO";

        if (!mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType().equals(typeString)) {
          mainFrame.saveForUndo();
        }
        mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.changeType(typeString);
        this.upEditPane();
      } else if (mainFrame.getDrawPane().selectType == 1 && !updating) {
        String newShape = (String) type.getSelectedItem();
        assert newShape != null;
        if (!mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).shape.equals(newShape.toLowerCase())) {
          mainFrame.saveForUndo();
        }
        mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).shape = newShape.toLowerCase();
      } else if (mainFrame.getDrawPane().selectType == 2 && !updating && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
        String newShape = (String) type.getSelectedItem();
        assert newShape != null;
        if (!mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.shape.equals(newShape.toLowerCase())) {
          mainFrame.saveForUndo();
        }
        mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.shape = newShape.toLowerCase();
      } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0 && !updating) {
        if (!mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.lineType().equals(type.getSelectedItem())) {
          mainFrame.saveForUndo();
        }
        mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.changeType((String) Objects.requireNonNull(type.getSelectedItem()));
        this.upEditPane();

      } else if (mainFrame.getDrawPane().selectType == 4 && !updating) {
        if (mainFrame.getDrawPane().multiEdit.type == 0) {
          String typeString = (String) Objects.requireNonNull(type.getSelectedItem());

          if (typeString.toUpperCase().equals("PLAIN"))
            typeString = "FERMION";

          if (typeString.toUpperCase().equals("SPIRAL"))
            typeString = "GLUON";

          if (typeString.toUpperCase().equals("WAVE"))
            typeString = "PHOTON";

          if (typeString.toUpperCase().equals("SWAVE"))
            typeString = "PHOTINO";

          if (typeString.toUpperCase().equals("SSPIRAL"))
            typeString = "GLUINO";

          if (!mainFrame.getDrawPane().multiEdit.lineType().equals(typeString)) {
            mainFrame.saveForUndo();
          }
          mainFrame.getDrawPane().multiEdit.changeType(typeString);
          this.upEditPane();
          mainFrame.tiles.repaint();
        } else if (mainFrame.getDrawPane().multiEdit.type == 1) {
          String newShape = (String) type.getSelectedItem();
          assert newShape != null;
          if (!mainFrame.getDrawPane().multiEdit.getShape().equals(newShape.toLowerCase())) {
            mainFrame.saveForUndo();
          }
          mainFrame.getDrawPane().multiEdit.setShape(newShape.toLowerCase());
          mainFrame.tiles.repaint();
          this.upEditPane();
        }
      }
    });

    invButton = new JButton(" Invert line");
    invButton.addActionListener(e -> {
      mainFrame.saveForUndo();
      Point2D temp = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getStart();
      mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setStart(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getEnd());
      mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setEnd(temp);
      mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).phase *= -1;
      mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setHeight(-mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getHeight());
      mainFrame.combineLines(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getStart());
      mainFrame.combineLines(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getEnd());
      mainFrame.getDrawPane().checkForVertex(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getStart());
      mainFrame.getDrawPane().checkForVertex(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getEnd());
    });

    doubleLine = new JCheckBox(" Double line");
    doubleLine.addItemListener(e -> {
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.doubleLine != doubleLine.isSelected()) {
            mainFrame.saveForUndo();
          }
          if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.doubleLine) {
            double newStroke = (double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.stroke;
            newStroke *= 1d/3d;
            mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.stroke = (int) Math.round(newStroke);
          } else {
            mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.stroke *= 3;
          }
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.doubleLine = doubleLine.isSelected();
          mainFrame.repaint();
          upEditPane();
        } else if (mainFrame.getDrawPane().selectType == 3) {
          if (mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.doubleLine != doubleLine.isSelected()) {
            mainFrame.saveForUndo();
          }
          if (mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.doubleLine) {
            double newStroke = (double) mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.stroke;
            newStroke *= 1d/3d;
            mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.stroke = (int) Math.round(newStroke);
          } else {
            mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.stroke *= 3;
          }
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.doubleLine = doubleLine.isSelected();
          mainFrame.repaint();
          upEditPane();
        } else if (mainFrame.getDrawPane().selectType == 4 && !updating && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.saveForUndo();
          if (mainFrame.getDrawPane().multiEdit.getDoubleLine()) {
            double newStroke = mainFrame.getDrawPane().multiEdit.getStroke();
            newStroke *= 1d/3d;
            mainFrame.getDrawPane().multiEdit.setStroke((float) newStroke);
          } else {
            double newStroke = mainFrame.getDrawPane().multiEdit.getStroke();
            newStroke *= 3d;
            mainFrame.getDrawPane().multiEdit.setStroke((float) newStroke);
          }
          mainFrame.getDrawPane().multiEdit.setDoubleLine(!mainFrame.getDrawPane().multiEdit.getDoubleLine());
          mainFrame.repaint();
          upEditPane();
        }
      }
    });

    lColor = new JButton(" Color");
    lColor.addActionListener(e -> {
      if (mainFrame.getDrawPane().selectType == 0) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.color);
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.color = newColor;
        }
      } else if (mainFrame.getDrawPane().selectType == 3) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.color);
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.color = newColor;
        }
      } else if (mainFrame.getDrawPane().selectType == 4 && !updating && mainFrame.getDrawPane().multiEdit.type == 0) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().multiEdit.getColor());
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().multiEdit.setColor(newColor);
          mainFrame.repaint();
        }
      }
    });

    vFillColor = new JButton(" Fill color");
    vFillColor.addActionListener(e -> {
      if (mainFrame.getDrawPane().selectType == 1) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.color);
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.graphic = false;
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.color = newColor;
        }
      } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.color);
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.graphic = false;
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.color = newColor;
        }
      } else if (mainFrame.getDrawPane().selectType == 3) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.color);
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.graphic = false;
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.color = newColor;
        }
      } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 1) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().multiEdit.getFillColor());
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().multiEdit.setFillColor(newColor);
          mainFrame.repaint();
        }
      }
    });

    vBorderColor = new JButton(" Border color");
    vBorderColor.addActionListener(e -> {
      if (mainFrame.getDrawPane().selectType == 1) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.color);
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.color = newColor;
        }
      } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.color);
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.color = newColor;
        }
      } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 1) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", mainFrame.getDrawPane().multiEdit.getBorderColor());
        if (newColor != null) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().multiEdit.setBorderColor(newColor);
          mainFrame.repaint();
        }
      }
    });

    vFillImg = new JButton(" Select image / JavaFx file");
    vFillImg.addActionListener(e -> {
      if (mainFrame.getDrawPane().selectType == 1)
        mainFrame.selectImage();
      if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v)
        mainFrame.selectImageFl();
      if (mainFrame.getDrawPane().selectType == 3)
        mainFrame.selectImageSFl();
      if (mainFrame.getDrawPane().selectType == 4)
        mainFrame.selectImageME();
      mainFrame.repaint();
    });

    // patternModel = new CustComboBoxModel<>(new Integer[] {0, 1, 2, 3});
    pattern = new JComboBox<Integer>(new Integer[] {0, 1, 2, 3, 4});
    pattern.addActionListener(e -> {
      if (mainFrame.getDrawPane().selectType == 1 && !updating) {
        int newPatternI = (int) pattern.getSelectedItem() - 1;
        if (newPatternI < 0) {
          if (mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.name != null)
            mainFrame.saveForUndo();
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.name = null;
          if (mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.color == null)
            mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.color = Color.BLACK;
          upEditPane();
          return;
        }
        String newPattern = JavaFXToJavaVectorGraphic.getPattern(newPatternI);
        if (mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.name == null || !mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.name.equals(newPattern.toLowerCase())) {
          mainFrame.saveForUndo();
          if (newPatternI == 1 || newPatternI == 2) mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.scale = 10.;
          else mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.scale = 1.;
        }
        assert newPattern != null;
        mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.name = Filling.addPattern(newPattern.toLowerCase());
        if (mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.color.equals(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.color)) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.color = Frame.compColor(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).filling.color);
        }
      } else if (mainFrame.getDrawPane().selectType == 2 && !updating && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
        int newPatternI = (int) pattern.getSelectedItem() - 1;
        if (newPatternI < 0) {

          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.name != null)
            mainFrame.saveForUndo();
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.name = null;

          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.color == null)
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.color = Color.BLACK;
          upEditPane();
          return;
        }
        String newPattern = JavaFXToJavaVectorGraphic.getPattern(newPatternI);
        assert newPattern != null;
        if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.name == null || !mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.name.equals(newPattern.toLowerCase())) {
          mainFrame.saveForUndo();
          if (newPatternI == 1 || newPatternI == 2) mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.scale = 10.;
          else mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.scale = 1.;
        }
        mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.name = Filling.addPattern(newPattern.toLowerCase());
        if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.color.equals(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.color)) {
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.color = Frame.compColor(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.filling.color);
        }
      } else if (mainFrame.getDrawPane().selectType == 3) {
        int newPatternI = (int) pattern.getSelectedItem() - 1;
        if (newPatternI < 0) {
          if (mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.name != null)
            mainFrame.saveForUndo();
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.name = null;
          if (mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.color == null)
            mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.color = Color.BLACK;
          upEditPane();
          return;
        }
        String newPattern = JavaFXToJavaVectorGraphic.getPattern(newPatternI);
        if (mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.name == null || !mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.name.equals(newPattern.toLowerCase())) {
          mainFrame.saveForUndo();
          if (newPatternI == 1 || newPatternI == 2) mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.scale = 10.;
          else mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.scale = 1.;
        }
        assert newPattern != null;
        mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.name = Filling.addPattern(newPattern.toLowerCase());
        if (mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.color.equals(mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.color)) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.color = Frame.compColor(mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).fill.color);
        }
      } else if (mainFrame.getDrawPane().selectType == 4) {
        if (mainFrame.getDrawPane().multiEdit.type == 1) {
          int newPatternI = (int) pattern.getSelectedItem() - 1;
          if (newPatternI < 0) {
            if (mainFrame.getDrawPane().multiEdit.getName() != null)
              mainFrame.saveForUndo();
            mainFrame.getDrawPane().multiEdit.purgeName();
            if (mainFrame.getDrawPane().multiEdit.getFillColor() == null)
              mainFrame.getDrawPane().multiEdit.setFillColor(Color.BLACK);
            upEditPane();
            return;
          }
          String newPattern = JavaFXToJavaVectorGraphic.getPattern(newPatternI);
          if (mainFrame.getDrawPane().multiEdit.getName() == null || !mainFrame.getDrawPane().multiEdit.getName().equals(newPattern.toLowerCase())) {
            mainFrame.saveForUndo();
            if (newPatternI == 1 || newPatternI == 2) mainFrame.getDrawPane().multiEdit.setScale(10.);
            else  mainFrame.getDrawPane().multiEdit.setScale(1.);
          }
          assert newPattern != null;
          mainFrame.getDrawPane().multiEdit.setName(Filling.addPattern(newPattern.toLowerCase()));
          if (mainFrame.getDrawPane().multiEdit.getFillColor().equals(mainFrame.getDrawPane().multiEdit.getBorderColor())) {
            mainFrame.getDrawPane().multiEdit.setFillColor(Frame.compColor(mainFrame.getDrawPane().multiEdit.getBorderColor()));
          }
        }
      }
      upEditPane();
      mainFrame.repaint();
    });
    ImageIcon[] imageIcons = GetResources.getPatternIcons();
    String strings[] = new String[5];
    strings[0] = " No Pattern";
    for (int i = 0; i < JavaFXToJavaVectorGraphic.patternsIn.size(); i++) {
      strings[i+1] = JavaFXToJavaVectorGraphic.patternsIn.get(i);
    }
    ComboBoxRenderer renderer = new ComboBoxRenderer(imageIcons, strings);
    pattern.setRenderer(renderer);

    autoremove = new JCheckBox(" Automatically remove");
    autoremove.addItemListener(e -> {
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).autoremove = autoremove.isSelected();
        }
      }
    });

    top = new JPanel(new GridLayout(0, 2));
    top.setAlignmentX(LEFT_ALIGNMENT);

    this.add(top);

    /* here come now the options given in the middle of the window
     * these are mostly pixel precise configurations of the Object
     * via JSpinners */

    dashComp = new JPanel(new GridLayout());

    dashed = new JCheckBox();

    dashed.addItemListener(e -> {
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.dashed != dashed.isSelected()) {
            mainFrame.saveForUndo();
          }
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.dashed = dashed.isSelected();
        } else if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {
          if (mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.dashed != dashed.isSelected()) {
            mainFrame.saveForUndo();
          }
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.dashed = dashed.isSelected();
        } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.dashed != dashed.isSelected()) {
            mainFrame.saveForUndo();
          }
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.dashed = dashed.isSelected();
        } else if (mainFrame.getDrawPane().selectType == 3) {
          if (mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.dashed != dashed.isSelected()) {
            mainFrame.saveForUndo();
          }
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.dashed = dashed.isSelected();
        } else if (mainFrame.getDrawPane().selectType == 4) {
          if (mainFrame.getDrawPane().multiEdit.type == 0 || mainFrame.getDrawPane().multiEdit.type == 1) {
            mainFrame.saveForUndo();
            mainFrame.getDrawPane().multiEdit.setDashed(!mainFrame.getDrawPane().multiEdit.getDashed());
          }
        }
        mainFrame.repaint();
        upEditPane();
      }
    });

    dashComp.add(dashed);

    numModel = new SpinnerNumberModel(1, 1, 1, 1.0);
    dashLength = new JSpinner(numModel);
    dashLength.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinner == null || !lastValueDashSpinner.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.dashLength = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.dashLength = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.dashLength = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.dashLength = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4) {
          if (mainFrame.getDrawPane().multiEdit.type == 0 || mainFrame.getDrawPane().multiEdit.type == 1) {
            mainFrame.saveForUndo();
            mainFrame.getDrawPane().multiEdit.setDashLength(Float.parseFloat(s.getValue().toString()));
          }
        }
        mainFrame.repaint();
        lastValueDashSpinner = s.getValue();
      }
    });

    dashComp.add(dashLength);
    dashComp.setAlignmentX(LEFT_ALIGNMENT);

    strokeComp = new JPanel(new GridLayout());

    numModelStroke = new SpinnerNumberModel(1, 0, 1000, 1.0);
    stroke = new JSpinner(numModelStroke);
    stroke.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerStroke == null || !lastValueDashSpinnerStroke.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.stroke = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.stroke = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.stroke = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.stroke = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4) {
          if (mainFrame.getDrawPane().multiEdit.type == 0 || mainFrame.getDrawPane().multiEdit.type == 1) {
            mainFrame.getDrawPane().multiEdit.setStroke(Float.parseFloat(s.getValue().toString()));
          }
        }
        mainFrame.repaint();
        lastValueDashSpinnerStroke = s.getValue();
      }
    });

    strokeLabel = new JLabel();
    strokeComp.add(strokeLabel);
    strokeComp.add(stroke);
    strokeComp.setAlignmentX(LEFT_ALIGNMENT);

    heightSizeComp = new JPanel(new GridLayout());

    numModelHeightSize = new SpinnerNumberModel(1, -10000, 10000, .05);
    heightSize = new JSpinner(numModelHeightSize);
    heightSize.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerHeightSize == null || !lastValueDashSpinnerHeightSize.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).height = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).size = (int) Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.size = (int) Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0) {
          mainFrame.saveForUndo();
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.scale = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.saveForUndo();
          Shaped ss = mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1);
          double diff = Float.parseFloat(s.getValue().toString()) - ss.width;
          ss.chWidth(mainFrame, diff);
        } else if (mainFrame.getDrawPane().selectType == 4) {
          if (mainFrame.getDrawPane().multiEdit.type == 1) {
            mainFrame.saveForUndo();
            mainFrame.getDrawPane().multiEdit.setSize((int) Float.parseFloat(s.getValue().toString()));
          } else if (mainFrame.getDrawPane().multiEdit.type == 2) {
            mainFrame.saveForUndo();
            mainFrame.getDrawPane().multiEdit.setScale(Float.parseFloat(s.getValue().toString()));
          }
        }
        mainFrame.repaint();
        lastValueDashSpinnerHeightSize = s.getValue();
      }
    });

    heightSizeLabel = new JLabel();
    heightSizeComp.add(heightSizeLabel);
    heightSizeComp.add(heightSize);
    heightSizeComp.setAlignmentX(LEFT_ALIGNMENT);

    heightSComp = new JPanel(new GridLayout());

    numModelHeightS = new SpinnerNumberModel(10, 10, 10000, 1.0);
    heightS = new JSpinner(numModelHeightS);
    heightS.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerHeightS == null || !lastValueDashSpinnerHeightS.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          Shaped ss = mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1);
          double diff = Float.parseFloat(s.getValue().toString()) - ss.height;
          ss.chHeight(mainFrame, diff);
        }
        mainFrame.repaint();
        lastValueDashSpinnerHeightS = s.getValue();
      }
    });

    heightSLabel = new JLabel(" Height");
    heightSComp.add(heightSLabel);
    heightSComp.add(heightS);
    heightSComp.setAlignmentX(LEFT_ALIGNMENT);
    heightSComp.setVisible(true);

    arrowComp = new JPanel(new GridLayout(2, 4));

    showArrow = new JCheckBox("Draw arrow size: ");
    showArrow.addItemListener(e -> {
      if (mainFrame.getDrawPane().selectType == 0) {
        mainFrame.saveForUndo();
        mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.showArrow = showArrow.isSelected();
        if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.showArrow) {
          arrowSize.setEnabled(true);
          arrowPlace.setEnabled(true);
          arrowDent.setEnabled(true);
          arrowHeight.setEnabled(true);
          straightLenArrow.setEnabled(true);
        } else {
          arrowSize.setEnabled(false);
          arrowPlace.setEnabled(false);
          arrowDent.setEnabled(false);
          arrowHeight.setEnabled(false);
          straightLenArrow.setEnabled(false);
        }
      } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
        mainFrame.saveForUndo();
        mainFrame.getDrawPane().multiEdit.setShowArrow(showArrow.isSelected());
        if (mainFrame.getDrawPane().multiEdit.getShowArrow()) {
          arrowSize.setEnabled(true);
          arrowPlace.setEnabled(true);
          arrowDent.setEnabled(true);
          arrowHeight.setEnabled(true);
          straightLenArrow.setEnabled(true);
        } else {
          arrowSize.setEnabled(false);
          arrowPlace.setEnabled(false);
          arrowDent.setEnabled(false);
          arrowHeight.setEnabled(false);
          straightLenArrow.setEnabled(false);
        }
        mainFrame.repaint();
      }
    });
    arrowComp.add(showArrow);

    numModelArrowSize = new SpinnerNumberModel(1, 1, 10000, 1.0);
    arrowSize = new JSpinner(numModelArrowSize);
    arrowSize.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerArrowSize == null || !lastValueDashSpinnerArrowSize.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowSize = (int) Double.parseDouble(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setArrowSize(Double.parseDouble(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerArrowSize = s.getValue();
      }
    });

    arrowComp.add(arrowSize);

    numModelArrowDent = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    arrowDent = new JSpinner(numModelArrowDent);
    arrowDent.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerArrowDent == null || !lastValueDashSpinnerArrowDent.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowDent = (int) Double.parseDouble(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setArrowDent(Double.parseDouble(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerArrowDent = s.getValue();
      }
    });
    arrowDentLabel = new JLabel(" Arrow dent");
    arrowComp.add(arrowDentLabel);
    arrowComp.add(arrowDent);

    numModelArrowHeight = new SpinnerNumberModel(1, 1, 10000, 1.0);
    arrowHeight = new JSpinner(numModelArrowHeight);
    arrowHeight.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerArrowHeight == null || !lastValueDashSpinnerArrowHeight.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowHeight = (int) Double.parseDouble(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setArrowHeight(Double.parseDouble(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerArrowHeight = s.getValue();
      }
    });
    arrowHeightLabel = new JLabel(" Arrow height");
    arrowComp.add(arrowHeightLabel);
    arrowComp.add(arrowHeight);

    numModelArrowPlace = new SpinnerNumberModel(.5, 0, 1, .05);
    arrowPlace = new JSpinner(numModelArrowPlace);
    arrowPlace.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerArrowPlace == null || !lastValueDashSpinnerArrowPlace.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowPlace = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setArrowPlace(Float.parseFloat(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerArrowPlace = s.getValue();
      }
    });

    arrowPlaceLabel = new JLabel(" Arrow position");
    arrowComp.add(arrowPlaceLabel);
    arrowComp.add(arrowPlace);
    arrowComp.setAlignmentX(LEFT_ALIGNMENT);

    this.add(arrowComp);

    wiggleHeightComp = new JPanel(new GridLayout());

    numModelWiggleHeight = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    wiggleHeight = new JSpinner(numModelWiggleHeight);
    wiggleHeight.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerWiggleHeight == null || !lastValueDashSpinnerWiggleHeight.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleHeight = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).rotation = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.rotation = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0) {
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.rotation = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.wiggleHeight = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4) {
          if (mainFrame.getDrawPane().multiEdit.type == 0) {
            mainFrame.getDrawPane().multiEdit.setWiggleHeight(Float.parseFloat(s.getValue().toString()));
          } else {
            mainFrame.getDrawPane().multiEdit.setRotation(Float.parseFloat(s.getValue().toString()));
          }
        }
        mainFrame.repaint();
        lastValueDashSpinnerWiggleHeight = s.getValue();
      }
    });

    wiggleHeightLabel = new JLabel();
    wiggleHeightComp.add(wiggleHeightLabel);
    wiggleHeightComp.add(wiggleHeight);
    wiggleHeightComp.setAlignmentX(LEFT_ALIGNMENT);

    wiggleLengthComp = new JPanel(new GridLayout());

    numModelWiggleLength = new SpinnerNumberModel(5, 5, 10000, 1.0);
    wiggleLength = new JSpinner(numModelWiggleLength);
    wiggleLength.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerWiggleLength == null || !lastValueDashSpinnerWiggleLength.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleLength = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.wiggleLength = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setWiggleLength(Float.parseFloat(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerWiggleLength = s.getValue();
      }
    });

    wiggleLengthLabel = new JLabel(" Wiggle length");
    wiggleLengthComp.add(wiggleLengthLabel);
    wiggleLengthComp.add(wiggleLength);
    wiggleLengthComp.setAlignmentX(LEFT_ALIGNMENT);

    wiggleOffsetComp = new JPanel(new GridLayout());

    numModelWiggleOffset = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    // this.upEditPane();
    wiggleOffset = new JSpinner(numModelWiggleOffset);
    wiggleOffset.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerWiggleOffset == null || !lastValueDashSpinnerWiggleOffset.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleOffset = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.wiggleOffset = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setWiggleOffset(Float.parseFloat(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerWiggleOffset = s.getValue();
      }
    });

    wiggleOffsetLabel = new JLabel(" Wiggle offset");
    wiggleOffsetComp.add(wiggleOffsetLabel);
    wiggleOffsetComp.add(wiggleOffset);
    wiggleOffsetComp.setAlignmentX(LEFT_ALIGNMENT);


    wiggleAsymmetryComp = new JPanel(new GridLayout());

    numModelWiggleAsymmetry = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    // this.upEditPane();
    wiggleAsymmetry = new JSpinner(numModelWiggleAsymmetry);
    wiggleAsymmetry.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerWiggleAsymmetry == null || !lastValueDashSpinnerWiggleAsymmetry.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleAsymmetry = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.wiggleAsymmetry = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setWiggleAsymmetry(Float.parseFloat(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerWiggleAsymmetry = s.getValue();
      }
    });

    wiggleAsymmetryLabel = new JLabel(" Wiggle asymmetry");
    wiggleAsymmetryComp.add(wiggleAsymmetryLabel);
    wiggleAsymmetryComp.add(wiggleAsymmetry);
    wiggleAsymmetryComp.setAlignmentX(LEFT_ALIGNMENT);

    straightLenComp = new JPanel(new GridLayout());

    numModelStraightLen = new SpinnerNumberModel(1, 0, 10000, 1.0);
    // this.upEditPane();
    straightLen = new JSpinner(numModelStraightLen);
    straightLen.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerStraightLen == null || !lastValueDashSpinnerStraightLen.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.straight_len = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.straight_len = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setStraightLength(Float.parseFloat(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerStraightLen = s.getValue();
      }
    });

    straightLenLabel = new JLabel(" Straight length");
    straightLenComp.add(straightLenLabel);
    straightLenComp.add(straightLen);
    straightLenComp.setAlignmentX(LEFT_ALIGNMENT);

    straightLenArrowComp = new JPanel(new GridLayout());

    numModelStraightLenArrow = new SpinnerNumberModel(1, 0, 10000, 1.0);
    // this.upEditPane();
    straightLenArrow = new JSpinner(numModelStraightLenArrow);
    straightLenArrow.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerStraightLenArrow == null || !lastValueDashSpinnerStraightLenArrow.equals(s.getValue())) && !updating) {
        mainFrame.saveForUndo();
        if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.straight_len_arrow = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.straight_len_arrow = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 0) {
          mainFrame.getDrawPane().multiEdit.setStraightLengthArrow(Float.parseFloat(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerStraightLenArrow = s.getValue();
      }
    });

    straightLenArrowLabel = new JLabel(" Straight length arrow");
    straightLenArrowComp.add(straightLenArrowLabel);
    straightLenArrowComp.add(straightLenArrow);
    straightLenArrowComp.setAlignmentX(LEFT_ALIGNMENT);

    circAngleComp = new JPanel(new GridLayout());

    circAngleLabel = new JLabel(" Loop rotation");

    numModelCircAngle = new SpinnerNumberModel(0, -10000, 10000, 1.0);
    circAngle = new JSpinner(numModelCircAngle);
    circAngle.addChangeListener(e -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerCircAngle == null || !lastValueDashSpinnerCircAngle.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).circAngle = Math.PI / 180 * Double.parseDouble(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3) {
          Shaped ss = mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1);
          double diff = Math.PI / 180 * Double.parseDouble(s.getValue().toString()) - ss.rotation;
          ss.rotate(mainFrame, diff);
        }
        mainFrame.repaint();
        lastValueDashSpinnerCircAngle = s.getValue();
      }
    });
    circAngleComp.add(circAngleLabel);
    circAngleComp.add(circAngle);
    circAngleComp.setAlignmentX(LEFT_ALIGNMENT);

    fillScaleComp = new JPanel(new GridLayout());

    fillScaleLabel = new JLabel(" Scale of filling");

    numModelFillScale = new SpinnerNumberModel(1, 1, 10000, .05);
    fillScale = new JSpinner(numModelFillScale);
    fillScale.addChangeListener(e -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerFillScale == null || !lastValueDashSpinnerFillScale.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 1) {
          Vertex v = mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1);
          v.filling.scale = (double) s.getValue();
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            Vertex v = mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex;
            v.filling.scale = (double) s.getValue();
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          Shaped sh = mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1);
          sh.fill.scale = (double) s.getValue();
        } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit.type == 1) {
          mainFrame.getDrawPane().multiEdit.setScale(Float.parseFloat(s.getValue().toString()));
        }
        mainFrame.repaint();
        lastValueDashSpinnerFillScale = s.getValue();
      }
    });
    fillScaleComp.add(fillScaleLabel);
    fillScaleComp.add(fillScale);
    fillScaleComp.setAlignmentX(LEFT_ALIGNMENT);

    configPanel = new JPanel(new GridLayout(0, 2));
    configPanel.setAlignmentX(LEFT_ALIGNMENT);
    this.add(configPanel);

    this.add(new JSeparator(JSeparator.HORIZONTAL));

    /* now come the components where the position of the object can
     * can be configured*/

    posComp = new JPanel(new GridLayout());

    numModelPosX = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    posX = new JSpinner(numModelPosX);
    posX.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerPosX == null || !lastValueDashSpinnerPosX.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          FeynLine fl = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);
          double angle = 0;
          boolean newAngle = false;
          if (Math.abs(fl.height) > .01 && !fl.getStart().equals(fl.getEnd())) {
            Point2D middle = new Point2D(fl.getStartX()/2 + fl.getEndX()/2, fl.getStartY()/2 + fl.getEndY()/2);
            // angle -= Math.PI;
            if (fl.height > 0) {
              angle = - Math.atan2(middle.y - fl.center.y, middle.x - fl.center.x);
              angle -= Math.PI / 2;
              angle *= -1;
            } else {
              angle = - Math.atan2( - middle.x + fl.center.x, - middle.y + fl.center.y);
              angle -= Math.PI;
            }
            while (angle < 0)
              angle += 2 * Math.PI;
            while (angle > Math.PI*2)
              angle -= Math.PI;
            newAngle = true;
          }
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setStartX(Float.parseFloat(s.getValue().toString()));
          if (fl.getEnd().equals(fl.getStart()) && Math.abs(fl.height) > .01 && newAngle)
            fl.circAngle = angle;
        } else if (mainFrame.getDrawPane().selectType == 1) {
          Vertex v = mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1);
          for (FeynLine line : mainFrame.getDrawPane().lines) {
            if (line.getEnd().distance(v.origin) < 1.5 && line.getStart().distance(v.origin) < 1.5) {
              line.setStartX(Float.parseFloat(s.getValue().toString()));
              line.setEndX(Float.parseFloat(s.getValue().toString()));
            } else if (line.getStart().distance(v.origin) < 1.5) {
              line.setStartX(Float.parseFloat(s.getValue().toString()));
            } else if (line.getEnd().distance(v.origin) < 1.5) {
              line.setEndX(Float.parseFloat(s.getValue().toString()));
            }
          }
          v.origin.x = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.origin.x = Float.parseFloat(s.getValue().toString());
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.center.x = Float.parseFloat(s.getValue().toString());
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          double dis = Float.parseFloat(s.getValue().toString()) - mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).origin.x;
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).translate(mainFrame, dis, 0);
        }
        mainFrame.repaint();
        lastValueDashSpinnerPosX = s.getValue();
      }
    });
    posXLabel = new JLabel();
    posComp.add(posXLabel);
    posComp.add(posX);
    numModelPosY = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    posY = new JSpinner(numModelPosY);
    posY.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerPosY == null || !lastValueDashSpinnerPosY.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          FeynLine fl = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);
          double angle = 0;
          boolean newAngle = false;
          if (Math.abs(fl.height) > .01 && !fl.getStart().equals(fl.getEnd())) {
            Point2D middle = new Point2D(fl.getStartX()/2 + fl.getEndX()/2, fl.getStartY()/2 + fl.getEndY()/2);
            // angle -= Math.PI;
            if (fl.height > 0) {
              angle = - Math.atan2(middle.y - fl.center.y, middle.x - fl.center.x);
              angle -= Math.PI / 2;
              angle *= -1;
            } else {
              angle = - Math.atan2( - middle.x + fl.center.x, - middle.y + fl.center.y);
              angle -= Math.PI;
            }
            while (angle < 0)
              angle += 2 * Math.PI;
            while (angle > Math.PI*2)
              angle -= Math.PI;
            newAngle = true;
          }
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setEndX(Float.parseFloat(s.getValue().toString()));
          if (fl.getEnd().equals(fl.getStart()) && Math.abs(fl.height) > .01 && newAngle)
            fl.circAngle = angle;
        } else if (mainFrame.getDrawPane().selectType == 1) {
          Vertex v = mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1);
          for (FeynLine line : mainFrame.getDrawPane().lines) {
            if (line.getEnd().distance(v.origin) < 1.5 && line.getStart().distance(v.origin) < 1.5) {
              line.setStartY(Float.parseFloat(s.getValue().toString()));
              line.setEndY(Float.parseFloat(s.getValue().toString()));
            } else if (line.getStart().distance(v.origin) < 1.5) {
              line.setStartY(Float.parseFloat(s.getValue().toString()));
            } else if (line.getEnd().distance(v.origin) < 1.5) {
              line.setEndY(Float.parseFloat(s.getValue().toString()));
            }
          }
          v.origin.y = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.origin.y = Float.parseFloat(s.getValue().toString());
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.center.y = Float.parseFloat(s.getValue().toString());
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          double dis = Float.parseFloat(s.getValue().toString()) - mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).origin.y;
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).translate(mainFrame, 0, dis);
        }
        mainFrame.repaint();
        lastValueDashSpinnerPosY = s.getValue();
      }
    });
    posYLabel = new JLabel();
    posComp.add(posYLabel);
    posComp.add(posY);
    posComp.setAlignmentX(LEFT_ALIGNMENT);
    this.add(posComp);

    endPosComp = new JPanel(new GridLayout());

    numModelEndPosX = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    endPosX = new JSpinner(numModelEndPosX);
    endPosX.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerEndPosX == null || !lastValueDashSpinnerEndPosX.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          FeynLine fl = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);
          double angle = 0;
          boolean newAngle = false;
          if (Math.abs(fl.height) > .01 && !fl.getStart().equals(fl.getEnd())) {
            Point2D middle = new Point2D(fl.getStartX()/2 + fl.getEndX()/2, fl.getStartY()/2 + fl.getEndY()/2);
            // angle -= Math.PI;
            if (fl.height > 0) {
              angle = - Math.atan2(middle.y - fl.center.y, middle.x - fl.center.x);
              angle -= Math.PI / 2;
              angle *= -1;
            } else {
              angle = - Math.atan2( - middle.x + fl.center.x, - middle.y + fl.center.y);
              angle -= Math.PI;
            }
            while (angle < 0)
              angle += 2 * Math.PI;
            while (angle > Math.PI*2)
              angle -= Math.PI;
            newAngle = true;
          }
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setStartY(Float.parseFloat(s.getValue().toString()));
          if (fl.getEnd().equals(fl.getStart()) && Math.abs(fl.height) > .01 && newAngle)
            fl.circAngle = angle;
        }
        mainFrame.repaint();
        lastValueDashSpinnerEndPosX = s.getValue();
      }
    });
    endPosXLabel = new JLabel(" Y (foot)");
    endPosComp.add(endPosXLabel);
    endPosComp.add(endPosX);
    numModelEndPosY = new SpinnerNumberModel(1, -10000, 10000, 1.0);
    endPosY = new JSpinner(numModelEndPosY);
    endPosY.addChangeListener((ChangeEvent e) -> {
      JSpinner s = (JSpinner) e.getSource();
      if ((lastValueDashSpinnerEndPosY == null || !lastValueDashSpinnerEndPosY.equals(s.getValue())) && !updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          FeynLine fl = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);
          double angle = 0;
          boolean newAngle = false;
          if (Math.abs(fl.height) > .01 && !fl.getStart().equals(fl.getEnd())) {
            Point2D middle = new Point2D(fl.getStartX()/2 + fl.getEndX()/2, fl.getStartY()/2 + fl.getEndY()/2);
            // angle -= Math.PI;
            if (fl.height > 0) {
              angle = - Math.atan2(middle.y - fl.center.y, middle.x - fl.center.x);
              angle -= Math.PI / 2;
              angle *= -1;
            } else {
              angle = - Math.atan2( - middle.x + fl.center.x, - middle.y + fl.center.y);
              angle -= Math.PI;
            }
            while (angle < 0)
              angle += 2 * Math.PI;
            while (angle > Math.PI*2)
              angle -= Math.PI;
            newAngle = true;
          }
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setEndY(Float.parseFloat(s.getValue().toString()));
          if (fl.getEnd().equals(fl.getStart()) && Math.abs(fl.height) > .01 && newAngle)
            fl.circAngle = angle;
        }
        mainFrame.repaint();
        lastValueDashSpinnerEndPosY = s.getValue();
      }
    });
    endPosYLabel = new JLabel(" Y (head)");
    endPosComp.add(endPosYLabel);
    endPosComp.add(endPosY);
    endPosComp.setAlignmentX(LEFT_ALIGNMENT);
    this.add(endPosComp);

    this.add(new JSeparator(JSeparator.HORIZONTAL));

    /* following are the components that let the label be configured*/

    GridBagLayout gridbag2 = new GridBagLayout();
    GridBagConstraints c2 = new GridBagConstraints();
    c2.weightx = .001;
    c2.weighty = 1;
    c2.gridy = 0;
    c2.gridx = 0;
    c2.insets = new Insets(0, 0, 0, 0);

    meDescPanel = new JPanel(gridbag2);
    meDescLabel = new JLabel(" Label: ");
    meDescPanel.add(meDescLabel, c2);
    meDescField = new JTextField();
    meDescField.setHorizontalAlignment(JTextField.LEADING);
    meDescField.setColumns(1);
    meDescField.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {

      }

      @Override
      public void focusLost(FocusEvent e) {
        for (ActionListener al : meDescField.getActionListeners())
          al.actionPerformed(null);
      }
    });
    meDescField.addActionListener(e -> {
      String text = new String(meDescField.getText());
      if (!updating && text != null) {
        if (!text.contains("<html>")) {
          try {
            TeXFormula formula = new TeXFormula(text);
          } catch (org.scilab.forge.jlatexmath.ParseException ex) {
            Object[] options = {"Show error message", "Cancel"};
            int n = JOptionPane.showOptionDialog(
              this,
              "The string could not be processed correctly",
              "LaTeX error",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.ERROR_MESSAGE,
              null,
              options,
              options[0]
            );
            if (n == 0) {
              JOptionPane.showMessageDialog(this,
                ex.getMessage(),
                "LaTeX error message",
                JOptionPane.ERROR_MESSAGE
              );
            }
            upEditPane();
            return;
          }
        }
        if (mainFrame.getDrawPane().selectType == 4) {
          mainFrame.getDrawPane().multiEdit.setDescription(text);
          mainFrame.repaint();
          upEditPane();
        }
      }
    });
    c2.fill = (GridBagConstraints.HORIZONTAL);
    c2.gridx++;
    c2.weightx = 1;
    meDescPanel.add(meDescField, c2);
    meDescPanel.setAlignmentX(LEFT_ALIGNMENT);


    descPanel = new JPanel();

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    descPanel.setLayout(gridbag);
    c.weightx = .001;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = 0;
    c.insets = new Insets(0, 0, 0, 0);

    showDesc = new JCheckBox(" Show label: ");
    showDesc.addItemListener(e -> {
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).showDesc = showDesc.isSelected();
        } else if (mainFrame.getDrawPane().selectType == 1) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).showDesc = showDesc.isSelected();
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.showDesc = showDesc.isSelected();
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.showDesc = showDesc.isSelected();
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).showDesc = showDesc.isSelected();
        } else if (mainFrame.getDrawPane().selectType == 4) {
          mainFrame.getDrawPane().multiEdit.setShowDesc(showDesc.isSelected());
        }
        if (showDesc.isSelected()) {
          rotDesc.setEnabled(true);
          partPlaceDesc.setEnabled(true);
          descDis.setEnabled(true);
          descScale.setEnabled(true);
        } else {
          rotDesc.setEnabled(false);
          partPlaceDesc.setEnabled(false);
          descDis.setEnabled(false);
          descScale.setEnabled(false);
        }
        mainFrame.repaint();
      }
    });
    descPanel.add(showDesc, c);

    c.fill = (GridBagConstraints.HORIZONTAL);
    c.gridx++;
    c.weightx = 1;
    descriptionField = new JTextField();
    descriptionField.setHorizontalAlignment(JTextField.LEADING);
    descriptionField.setColumns(1);
    descriptionField.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {

      }

      @Override
      public void focusLost(FocusEvent e) {
        for (ActionListener al : descriptionField.getActionListeners())
          al.actionPerformed(null);
      }
    });
    descriptionField.addActionListener(e -> {
      String text = new String(descriptionField.getText());
      if (!updating && text != null) {
        if (!text.contains("<html>")) {
          try {
            TeXFormula formula = new TeXFormula(text);
          } catch (org.scilab.forge.jlatexmath.ParseException ex) {
            Object[] options = {"Show error message", "Cancel"};
            int n = JOptionPane.showOptionDialog(
              this,
              "The string could not be processed correctly",
              "LaTeX error",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.ERROR_MESSAGE,
              null,
              options,
              options[0]
            );
            if (n == 0) {
              JOptionPane.showMessageDialog(this,
                ex.getMessage(),
                "LaTeX error message",
                JOptionPane.ERROR_MESSAGE
              );
            }
            upEditPane();
            return;
          }
        }
        if (mainFrame.getDrawPane().selectType == 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.description = text;
        } else if (mainFrame.getDrawPane().selectType == 1) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).description = text;
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.description = text;
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.description = text;
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).description = text;
        } else if (mainFrame.getDrawPane().selectType == 4) {
          mainFrame.getDrawPane().multiEdit.setDescription(text);
        }
        mainFrame.repaint();
      }
    });
    descPanel.add(descriptionField, c);
    descPanel.setAlignmentX(LEFT_ALIGNMENT);

    this.add(descPanel);

    descriptionPanel = new JPanel();

    GridBagConstraints gbc = new GridBagConstraints();
    descriptionPanel.setLayout(new GridBagLayout());
    gbc.weightx = 1;
    gbc.weighty = 1;
    gbc.gridy = 0;
    gbc.gridx = 0;
    gbc.gridwidth = 4;
    gbc.fill = GridBagConstraints.HORIZONTAL;

    gbc.insets = new Insets(0, 0, 0, 0);
    gbc.gridwidth = 1;
    JLabel rotLabel = new JLabel(" Rotation:");
    descriptionPanel.add(rotLabel, gbc);
    gbc.gridx++;
    SpinnerNumberModel numModelRotDesc = new SpinnerNumberModel(0, -10000, 10000, 1.0);
    rotDesc = new JSpinner(numModelRotDesc);
    rotDesc.addChangeListener(e -> {
      JSpinner s = (JSpinner) e.getSource();
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).rotDesc = Math.PI / 180 * Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).rotDesc = Math.PI / 180 * Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.rotDesc = Math.PI / 180 * Float.parseFloat(s.getValue().toString());
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.rotDesc = Math.PI / 180 * Float.parseFloat(s.getValue().toString());
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).rotDesc = Math.PI / 180 * Float.parseFloat(s.getValue().toString());
        }
      }
    });
    descriptionPanel.add(rotDesc, gbc);

    gbc.gridx++;

    JLabel scaleLabel = new JLabel(" Scale:");
    descriptionPanel.add(scaleLabel, gbc);
    gbc.gridx++;
    SpinnerNumberModel numModelScaleDesc = new SpinnerNumberModel(1, 0, 10000, .05);
    descScale = new JSpinner(numModelScaleDesc);
    descScale.addChangeListener(e -> {
      JSpinner s = (JSpinner) e.getSource();
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).descScale = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).descScale = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.descScale = Float.parseFloat(s.getValue().toString());
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.descScale = Float.parseFloat(s.getValue().toString());
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).descScale = Float.parseFloat(s.getValue().toString());
        }
      }
    });
    descriptionPanel.add(descScale, gbc);

    gbc.gridx = 0;
    gbc.gridy++;
    partLabel = new JLabel(" Part of path:");
    descriptionPanel.add(partLabel, gbc);
    gbc.gridx++;
    numModelPart = new SpinnerNumberModel(0.5, -10000, 10000, .05);
    partPlaceDesc = new JSpinner(numModelPart);
    partPlaceDesc.addChangeListener(e -> {
      JSpinner s = (JSpinner) e.getSource();
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).partPlaceDesc = Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).descPosX = (int) Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.descPosX = (int) Float.parseFloat(s.getValue().toString());
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.descPosX = (int) Float.parseFloat(s.getValue().toString());
          }
        } else if (mainFrame.getDrawPane().selectType == 1) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).descPosX = (int) Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 3) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).descPosX = (int) Float.parseFloat(s.getValue().toString());
        }
      }
    });
    descriptionPanel.add(partPlaceDesc, gbc);
    gbc.gridx++;
    disLabel = new JLabel(" Distance to line:");
    descriptionPanel.add(disLabel, gbc);
    gbc.gridx++;
    SpinnerNumberModel numModelDis = new SpinnerNumberModel(0, -10000, 10000, 1.0);
    descDis = new JSpinner(numModelDis);
    descDis.addChangeListener(e -> {
      JSpinner s = (JSpinner) e.getSource();
      if (!updating) {
        if (mainFrame.getDrawPane().selectType == 0) {
          mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).descDis = (int) Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 1) {
          mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).descPosY = (int) Float.parseFloat(s.getValue().toString());
        } else if (mainFrame.getDrawPane().selectType == 2) {
          if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.descPosY = (int) Float.parseFloat(s.getValue().toString());
          } else {
            mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.descPosY = (int) Float.parseFloat(s.getValue().toString());
          }
        } else if (mainFrame.getDrawPane().selectType == 3) {
          mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).descPosY = (int) Float.parseFloat(s.getValue().toString());
        }
      }
    });
    descriptionPanel.add(descDis, gbc);

    defaultButton = new JButton(" Set as default label parameters ");
    gbc.gridx = 0;
    gbc.gridy++;
    gbc.gridwidth = 4;
    defaultButton.addActionListener(e -> {
      if (mainFrame.getDrawPane().selectType == 0) {
        mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).setDefault();
      } else if (mainFrame.getDrawPane().selectType == 1) {
        mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).setDefault();
      } else if (mainFrame.getDrawPane().selectType == 2) {
        if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.setDefault();
        } else {
          mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.setDefault();
        }
      } else if (mainFrame.getDrawPane().selectType == 3) {
        mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).setDefault();
      }
    });

    descriptionPanel.add(defaultButton, gbc);
    descriptionPanel.setAlignmentX(LEFT_ALIGNMENT);

    this.add(descriptionPanel);

    /* these are the tiles where the config of the selected object can be
     * set to a preset */

    preSelLin = new PresetSelectLines(mainFrame);
    preSelVer = new PresetSelectVertex(mainFrame);
    preSelImg = new PresetSelectImage(mainFrame);
    preSelLin.setAlignmentX(LEFT_ALIGNMENT);
    preSelVer.setAlignmentX(LEFT_ALIGNMENT);
    preSelImg.setAlignmentX(LEFT_ALIGNMENT);
    this.add(preSelLin);
    this.add(preSelVer);
    this.add(preSelImg);

    this.addComponentListener(new ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        preSelLin.resize(getWidth());
        preSelVer.resize(getWidth());
        preSelImg.resize(getWidth());
      }
    });

    this.upEditPane();
  }

  /**
   * if some properties of the selected object is changed or the another object is selected, this will be called to update all the necessary things
   */
  public void upEditPane() {

    /* upodating has to be set to true to disable all the actionListners
     * of the subcomponents because otherwise the properties of the selected
     * object change which is not wanted */
    updating = true;

    for (Component comp : this.getComponents()) {
      if (comp instanceof JSeparator)
        comp.setVisible(true);
    }

    this.remove(meDescPanel);

    /* a line is selected*/
    if (mainFrame.getDrawPane().selectType == 0 && mainFrame.getDrawPane().lines.size() > 0) {
      mAC.update();

      defaultButton.setText(" Set as default label parameters for this line type ");

      lColor.setText(" Color");

      try {
        descriptionField.setText(new String(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.description));
      } catch (NullPointerException ex) {
        descriptionField.setText("");
      }

      partLabel.setText(" Part of path");
      disLabel.setText(" Distance to line");

      numModelPart.setMaximum((double) 10000f);
      numModelPart.setStepSize((double) .05f);
      numModelPart.setMinimum((double) -10000f);

      partPlaceDesc.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).partPlaceDesc);
      descDis.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).descDis);
      rotDesc.setValue(180d / Math.PI * mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).rotDesc);
      showDesc.setSelected(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).showDesc);
      descScale.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).descScale);

      if (showDesc.isSelected()) {
        rotDesc.setEnabled(true);
        partPlaceDesc.setEnabled(true);
        descDis.setEnabled(true);
        descScale.setEnabled(true);
      } else {
        descScale.setEnabled(false);
        rotDesc.setEnabled(false);
        descDis.setEnabled(false);
        partPlaceDesc.setEnabled(false);
      }

      FeynLine currLine = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);
      if (currLine.getStart().equals(currLine.getEnd())) {
        circAngle.setValue(180d / Math.PI * currLine.circAngle);
        circAngleComp.setVisible(true);
      } else {
        circAngleComp.setVisible(false);
      }
      circAngleLabel.setText(" Loop rotation");

      String typeString = currLine.lineConfig.lineType();

      if (typeString.toUpperCase().equals("FERMION"))
        typeString = "Plain";

      if (typeString.toUpperCase().equals("GLUON"))
        typeString = "Spiral";

      if (typeString.toUpperCase().equals("PHOTON"))
        typeString = "Wave";

      if (typeString.toUpperCase().equals("GLUINO") || typeString.toUpperCase().equals("SSPIRAL"))
        typeString = "Sspiral";

      if (typeString.toUpperCase().equals("PHOTINO") || typeString.toUpperCase().equals("SWAVE"))
        typeString = "Swave";

      lineModel.setSelectedItem(typeString);
      type.setModel(lineModel);

      dashed.setText(" Dashed");
      dashed.setSelected(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.dashed);
      doubleLine.setSelected(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.doubleLine);

      if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.dashed) {
        numModel.setMaximum(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).length());
        numModel.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.dashLength);
        numModel.setMinimum(1d);
        if (dashLength != null) {
          dashLength.setEnabled(true);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(true);
        }
      } else {
        if (dashLength != null) {
          dashLength.setEnabled(false);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(false);
        }
      }

      if (strokeLabel != null)
        strokeLabel.setText(" Stroke size");

      numModelStroke.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.stroke);
      if (stroke != null) {
        stroke.setEnabled(true);
        ((JSpinner.DefaultEditor) stroke.getEditor()).getTextField().setEditable(true);
      }

      if (heightSizeLabel != null)
        heightSizeLabel.setText(" Curvature");

      numModelHeightSize.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).height);
      numModelHeightSize.setMinimum(-10000d);
      numModelHeightSize.setStepSize(1d);
      if (heightSize != null) {
        heightSize.setEnabled(true);
        ((JSpinner.DefaultEditor) heightSize.getEditor()).getTextField().setEditable(true);
      }

      showArrow.setSelected(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.showArrow);
      if (showArrow.isSelected()) {
        numModelArrowSize.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowSize);
        numModelArrowDent.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowDent);
        numModelArrowHeight.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowHeight);

        if (arrowSize != null) {
          arrowSize.setEnabled(true);
          ((JSpinner.DefaultEditor) arrowSize.getEditor()).getTextField().setEditable(true);
        }

        if (arrowDent != null) {
          arrowDent.setEnabled(true);
          ((JSpinner.DefaultEditor) arrowDent.getEditor()).getTextField().setEditable(true);
        }

        if (arrowHeight != null) {
          arrowHeight.setEnabled(true);
          ((JSpinner.DefaultEditor) arrowHeight.getEditor()).getTextField().setEditable(true);
        }

        numModelArrowPlace.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowPlace);
        if (arrowPlace != null) {
          arrowPlace.setEnabled(true);
          ((JSpinner.DefaultEditor) arrowPlace.getEditor()).getTextField().setEditable(true);
        }
      } else {
        numModelArrowPlace.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowPlace);
        numModelArrowSize.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.arrowSize);
        arrowSize.setEnabled(false);
        arrowPlace.setEnabled(false);
        arrowDent.setEnabled(false);
        arrowHeight.setEnabled(false);
      }

      if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.PHOTON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.GLUON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SSPIRAL || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SWAVE) {
        numModelWiggleHeight.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleHeight);
        wiggleHeightComp.setVisible(true);
        wiggleHeightLabel.setText(" Wiggle height");
        if (wiggleHeight != null) {
          wiggleHeight.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleHeight.getEditor()).getTextField().setEditable(true);
        }
        numModelWiggleLength.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleLength);
        wiggleLengthComp.setVisible(true);
        if (wiggleLength != null) {
          wiggleLength.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleLength.getEditor()).getTextField().setEditable(true);
        }
      } else {
        wiggleHeightComp.setVisible(false);
        wiggleLengthComp.setVisible(false);
      }

      if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.GLUON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.PHOTON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SSPIRAL || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SWAVE) {
        numModelWiggleOffset.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleOffset);
        wiggleOffsetComp.setVisible(true);
        if (wiggleOffset != null) {
          wiggleOffset.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleOffset.getEditor()).getTextField().setEditable(true);
        }
      } else {
        wiggleOffsetComp.setVisible(false);
      }

      if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.GLUON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SSPIRAL) {
        wiggleOffsetLabel.setText(" Wiggle offset");
      } else if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.PHOTON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SWAVE) {
        wiggleOffsetLabel.setText(" Wiggle sharpness");
      }

      if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.GLUON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SSPIRAL) {
        wiggleAsymmetryLabel.setText(" Wiggle asymmetry");
        numModelWiggleAsymmetry.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.wiggleAsymmetry);
        wiggleAsymmetryComp.setVisible(true);
        if (wiggleAsymmetry != null) {
          wiggleAsymmetry.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleAsymmetry.getEditor()).getTextField().setEditable(true);
        }
      } else {
        wiggleAsymmetryComp.setVisible(false);
      }

      if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.GLUON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.PHOTON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SSPIRAL || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SWAVE) {
        numModelStraightLen.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.straight_len);
        straightLenComp.setVisible(true);
        if (straightLen != null) {
          straightLen.setEnabled(true);
          ((JSpinner.DefaultEditor) straightLen.getEditor()).getTextField().setEditable(true);
        }
      } else {
        straightLenComp.setVisible(false);
      }

      if ((mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.GLUON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.PHOTON || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SSPIRAL || mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.SWAVE)) {
        numModelStraightLenArrow.setValue((double) mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.straight_len_arrow);
        straightLenArrowComp.setVisible(true);
        if (straightLenArrow != null) {
          straightLenArrow.setEnabled(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.showArrow);
          ((JSpinner.DefaultEditor) straightLenArrow.getEditor()).getTextField().setEditable(true);
        }
      } else {
        ((JSpinner.DefaultEditor) straightLenArrow.getEditor()).getTextField().setEditable(false);
        straightLenArrow.setEnabled(false);
      }

      posYLabel.setText(" X (head)");
      numModelPosY.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getEndX());
      if (posY != null) {
        posY.setEnabled(true);
        ((JSpinner.DefaultEditor) posY.getEditor()).getTextField().setEditable(true);
      }
      posXLabel.setText(" X (foot)");
      numModelPosX.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getStartX());
      if (posX != null) {
        posX.setEnabled(true);
        ((JSpinner.DefaultEditor) posX.getEditor()).getTextField().setEditable(true);
      }
      numModelEndPosY.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getEndY());
      if (posY != null) {
        endPosY.setEnabled(true);
        ((JSpinner.DefaultEditor) endPosY.getEditor()).getTextField().setEditable(true);
      }
      numModelEndPosX.setValue(mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).getStartY());
      if (endPosY != null) {
        endPosY.setEnabled(true);
        ((JSpinner.DefaultEditor) endPosY.getEditor()).getTextField().setEditable(true);
      }

      if (mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1).lineConfig.lineType == LineType.FERMION) {
        configPanel.removeAll();
        configPanel.add(strokeComp);
        configPanel.add(heightSizeComp);
        configPanel.add(circAngleComp);
        configPanel.add(dashComp);
        configPanel.revalidate();
      } else {
        FeynLine line = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);
        if (!line.getStart().equals(line.getEnd()) || Math.abs(line.height) < .01) {
          configPanel.removeAll();
          configPanel.add(strokeComp);
          configPanel.add(heightSizeComp);
          configPanel.add(wiggleHeightComp);
          configPanel.add(wiggleLengthComp);
          configPanel.add(wiggleOffsetComp);
          if (line.lineConfig.lineType == LineType.GLUON || line.lineConfig.lineType == LineType.SSPIRAL) {
            configPanel.add(wiggleAsymmetryComp);
          }
          configPanel.add(straightLenComp);
          configPanel.add(straightLenArrowComp);
          configPanel.add(dashComp);
          configPanel.revalidate();
        } else {
          configPanel.removeAll();
          configPanel.add(strokeComp);
          configPanel.add(heightSizeComp);
          configPanel.add(wiggleHeightComp);
          configPanel.add(wiggleLengthComp);
          configPanel.add(wiggleOffsetComp);
          if (line.lineConfig.lineType == LineType.GLUON || line.lineConfig.lineType == LineType.SSPIRAL) {
            configPanel.add(wiggleAsymmetryComp);
          }
          configPanel.add(straightLenComp);
          configPanel.add(straightLenArrowComp);
          configPanel.add(circAngleComp);
          configPanel.add(dashComp);
          configPanel.revalidate();
        }
      }
      configPanel.setVisible(true);

      top.removeAll();
      top.add(type);
      top.add(lColor);
      top.add(invButton);
      top.add(doubleLine);

      arrowComp.setVisible(true);
      emptyPane.setVisible(false);
      strokeComp.setVisible(true);
      heightSizeComp.setVisible(true);
      dashComp.setVisible(true);
      preSelLin.setVisible(true);
      preSelVer.setVisible(false);
      preSelImg.setVisible(false);
      endPosComp.setVisible(true);
      posComp.setVisible(true);
      descriptionPanel.setVisible(true);
      descPanel.setVisible(true);
      top.setVisible(true);
      mAC.setVisible(true);

    } else if (mainFrame.getDrawPane().selectType == 1 && mainFrame.getDrawPane().vertices.size() > 0) {

      /* an internal vertex is selected */

      defaultButton.setText(" Set as default label parameters ");

      try {
        descriptionField.setText(new String(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).description));
      } catch (NullPointerException ex) {
        descriptionField.setText("");
      }
      vertModel.setSelectedItem(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).shape);
      type.setModel(vertModel);

      numModelPart.setStepSize((double) 1);
      numModelPart.setMaximum((double) 10000);
      numModelPart.setMinimum((double) -10000);

      partLabel.setText(" Rel. x-comp.");
      disLabel.setText(" Rel. y-comp.");

      partPlaceDesc.setValue((double)  mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).descPosX);
      descDis.setValue(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).descPosY);
      rotDesc.setValue(180d / Math.PI * mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).rotDesc);
      showDesc.setSelected(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).showDesc);
      descScale.setValue(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).descScale);

      if (showDesc.isSelected()) {
        rotDesc.setEnabled(true);
        partPlaceDesc.setEnabled(true);
        descDis.setEnabled(true);
        descScale.setEnabled(true);
      } else {
        descScale.setEnabled(false);
        rotDesc.setEnabled(false);
        descDis.setEnabled(false);
        partPlaceDesc.setEnabled(false);
      }

      int index = JavaFXToJavaVectorGraphic.getIndexOfPattern(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1));
      pattern.setSelectedIndex(index);

      dashed.setText(" Border dashed");
      dashed.setSelected(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.dashed);

      autoremove.setSelected(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).autoremove);

      if (mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.dashed) {
        numModel.setMaximum((mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).size * 2 * Math.PI));
        numModel.setMinimum(1d);
        numModel.setValue((double) mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.dashLength);
        if (dashLength != null) {
          dashLength.setEnabled(true);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(true);
        }
      } else {
        if (dashLength != null) {
          dashLength.setEnabled(false);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(false);
        }
      }

      if (strokeLabel != null)
        strokeLabel.setText(" Border stroke size");

      numModelStroke.setValue((double) mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).border.stroke);
      if (stroke != null) {
        stroke.setEnabled(true);
        ((JSpinner.DefaultEditor) stroke.getEditor()).getTextField().setEditable(true);
      }

      if (heightSizeLabel != null)
        heightSizeLabel.setText(" Size");

      numModelHeightSize.setValue((double)  mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).size);
      numModelHeightSize.setMinimum((double) 0);
      numModelHeightSize.setStepSize(1d);
      if (heightSize != null) {
        heightSize.setEnabled(true);
        ((JSpinner.DefaultEditor) heightSize.getEditor()).getTextField().setEditable(true);
      }

      numModelWiggleHeight.setValue((double) mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).rotation);
      wiggleHeightComp.setVisible(true);
      wiggleHeightLabel.setText(" Rotation");
      if (wiggleHeight != null) {
        wiggleHeight.setEnabled(true);
        ((JSpinner.DefaultEditor) wiggleHeight.getEditor()).getTextField().setEditable(true);
      }

      posYLabel.setText(" Y (center)");
      numModelPosY.setValue(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).origin.y);
      if (posY != null) {
        posY.setEnabled(true);
        ((JSpinner.DefaultEditor) posY.getEditor()).getTextField().setEditable(true);
      }
      posXLabel.setText(" X (center)");
      numModelPosX.setValue(mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() - 1).origin.x);
      if (posX != null) {
        posX.setEnabled(true);
        ((JSpinner.DefaultEditor) posX.getEditor()).getTextField().setEditable(true);
      }

      configPanel.removeAll();
      configPanel.add(strokeComp);
      configPanel.add(heightSizeComp);
      configPanel.add(wiggleHeightComp);
      configPanel.add(dashComp);
      Vertex v = mainFrame.getDrawPane().vertices.get(mainFrame.getDrawPane().vertices.size() -1);
      if (v.filling.graphic || v.filling.name != null) {
        fillScale.setValue(v.filling.scale);
        configPanel.add(fillScaleComp);
      }
      configPanel.revalidate();
      configPanel.setVisible(true);

      top.removeAll();
      top.add(type);
      top.add(pattern);
      top.add(vFillColor);
      top.add(vFillImg);
      top.add(vBorderColor);
      top.add(autoremove);

      emptyPane.setVisible(false);
      strokeComp.setVisible(true);
      heightSizeComp.setVisible(true);
      dashComp.setVisible(true);
      preSelLin.setVisible(false);
      preSelVer.setVisible(true);
      arrowComp.setVisible(false);
      wiggleLengthComp.setVisible(false);
      wiggleHeightComp.setVisible(true);
      wiggleOffsetComp.setVisible(false);
      wiggleAsymmetryComp.setVisible(false);
      endPosComp.setVisible(false);
      posComp.setVisible(true);
      preSelImg.setVisible(false);
      descriptionPanel.setVisible(true);
      descPanel.setVisible(true);
      circAngleComp.setVisible(false);
      fillScaleComp.setVisible(true);
      top.setVisible(true);
      mAC.setVisible(false);
    } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0 && mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).v) {

      /* a free floating vertex is selected*/

      defaultButton.setText(" Set as default label parameters ");

      try {
        descriptionField.setText(new String(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.description));
      } catch (NullPointerException ex) {
        descriptionField.setText("");
      }

      vertModel.setSelectedItem(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.shape);
      type.setModel(vertModel);

      numModelPart.setStepSize(1);
      numModelPart.setMaximum((double) 10000);
      numModelPart.setMinimum((double) -10000);

      partLabel.setText(" Rel. x-comp.");
      disLabel.setText(" Rel. y-comp.");

      partPlaceDesc.setValue((double)  mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.descPosX);
      descDis.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.descPosY);
      rotDesc.setValue(180d / Math.PI * mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.rotDesc);
      showDesc.setSelected(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.showDesc);
      descScale.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.descScale);

      if (showDesc.isSelected()) {
        rotDesc.setEnabled(true);
        partPlaceDesc.setEnabled(true);
        descDis.setEnabled(true);
        descScale.setEnabled(true);
      } else {
        descScale.setEnabled(false);
        rotDesc.setEnabled(false);
        descDis.setEnabled(false);
        partPlaceDesc.setEnabled(false);
      }
      int index = JavaFXToJavaVectorGraphic.getIndexOfPattern(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex);
      pattern.setSelectedIndex(index);

      dashed.setText(" Border dashed");
      dashed.setSelected(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.dashed);

      if (mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.dashed) {
        numModel.setMaximum((mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.size * 2 * Math.PI));
        numModel.setMinimum(1d);
        numModel.setValue((double) mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.dashLength);
        if (dashLength != null) {
          dashLength.setEnabled(true);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(true);
        }
      } else {
        if (dashLength != null) {
          dashLength.setEnabled(false);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(false);
        }
      }

      if (strokeLabel != null)
        strokeLabel.setText(" Border stroke size");

      numModelStroke.setValue((double) mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.border.stroke);
      if (stroke != null) {
        stroke.setEnabled(true);
        ((JSpinner.DefaultEditor) stroke.getEditor()).getTextField().setEditable(true);
      }

      if (heightSizeLabel != null)
        heightSizeLabel.setText(" Size");

      numModelHeightSize.setValue((double)  mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.size);
      numModelHeightSize.setMinimum(0d);
      numModelHeightSize.setStepSize(1d);
      if (heightSize != null) {
        heightSize.setEnabled(true);
        ((JSpinner.DefaultEditor) heightSize.getEditor()).getTextField().setEditable(true);
      }

      numModelWiggleHeight.setValue((double) mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).vertex.rotation);
      wiggleHeightComp.setVisible(true);
      wiggleHeightLabel.setText(" Rotation");
      if (wiggleHeight != null) {
        wiggleHeight.setEnabled(true);
        ((JSpinner.DefaultEditor) wiggleHeight.getEditor()).getTextField().setEditable(true);
      }

      posYLabel.setText(" Y (center)");
      numModelPosY.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).getCenter().y);
      if (posY != null) {
        posY.setEnabled(true);
        ((JSpinner.DefaultEditor) posY.getEditor()).getTextField().setEditable(true);
      }
      posXLabel.setText(" X (center)");
      numModelPosX.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).getCenter().x);
      if (posX != null) {
        posX.setEnabled(true);
        ((JSpinner.DefaultEditor) posX.getEditor()).getTextField().setEditable(true);
      }

      configPanel.removeAll();
      configPanel.add(strokeComp);
      configPanel.add(heightSizeComp);
      configPanel.add(wiggleHeightComp);
      configPanel.add(dashComp);
      Vertex v = mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() -1).vertex;
      if (v.filling.graphic || v.filling.name != null) {
        fillScale.setValue(v.filling.scale);
        configPanel.add(fillScaleComp);
      }
      configPanel.revalidate();
      configPanel.setVisible(true);

      top.removeAll();
      top.add(type);
      top.add(pattern);
      top.add(vFillColor);
      top.add(vFillImg);
      top.add(vBorderColor);

      emptyPane.setVisible(false);
      strokeComp.setVisible(true);
      heightSizeComp.setVisible(true);
      dashComp.setVisible(true);
      preSelLin.setVisible(false);
      preSelVer.setVisible(true);
      arrowComp.setVisible(false);
      wiggleLengthComp.setVisible(false);
      wiggleHeightComp.setVisible(true);
      wiggleOffsetComp.setVisible(false);
      wiggleAsymmetryComp.setVisible(false);
      endPosComp.setVisible(false);
      posComp.setVisible(true);
      preSelImg.setVisible(false);
      descriptionPanel.setVisible(true);
      descPanel.setVisible(true);
      circAngleComp.setVisible(false);
      top.setVisible(true);
      mAC.setVisible(false);
    } else if (mainFrame.getDrawPane().selectType == 2 && mainFrame.getDrawPane().fObjects.size() > 0) {

      /* a free floating image is selected*/

      defaultButton.setText(" Set as default label parameters ");

      try {
        descriptionField.setText(new String(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.description));
      } catch (NullPointerException ex) {
        descriptionField.setText("");
      }

      numModelWiggleHeight.setValue((double) mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.rotation);
      wiggleHeightLabel.setText(" Rotation");
      if (wiggleHeight != null) {
        wiggleHeight.setEnabled(true);
        ((JSpinner.DefaultEditor) wiggleHeight.getEditor()).getTextField().setEditable(true);
      }

      numModelPart.setStepSize(1);
      numModelPart.setMaximum((double) 10000);
      numModelPart.setMinimum((double) -10000);

      partLabel.setText(" Rel. x-comp.");
      disLabel.setText(" Rel. y-comp.");

      partPlaceDesc.setValue((double)  mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.descPosX);
      descDis.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.descPosY);
      rotDesc.setValue(180d / Math.PI * mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.rotDesc);
      showDesc.setSelected(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.showDesc);
      descScale.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.descScale);

      if (showDesc.isSelected()) {
        rotDesc.setEnabled(true);
        partPlaceDesc.setEnabled(true);
        descDis.setEnabled(true);
        descScale.setEnabled(true);
      } else {
        descScale.setEnabled(false);
        rotDesc.setEnabled(false);
        descDis.setEnabled(false);
        partPlaceDesc.setEnabled(false);
      }
      if (heightSizeLabel != null)
        heightSizeLabel.setText(" Scale of image");

      numModelHeightSize.setValue((double)  mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.scale);
      numModelHeightSize.setMinimum((double) -10000);
      numModelHeightSize.setStepSize(.05);
      if (heightSize != null) {
        heightSize.setEnabled(true);
        ((JSpinner.DefaultEditor) heightSize.getEditor()).getTextField().setEditable(true);
      }

      posYLabel.setText(" Y-comp of center");
      numModelPosY.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).getCenter().y);
      if (posY != null) {
        posY.setEnabled(true);
        ((JSpinner.DefaultEditor) posY.getEditor()).getTextField().setEditable(true);
      }
      posXLabel.setText(" X-comp of center");
      numModelPosX.setValue(mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).getCenter().x);
      if (posX != null) {
        posX.setEnabled(true);
        ((JSpinner.DefaultEditor) posX.getEditor()).getTextField().setEditable(true);
      }

      configPanel.removeAll();
      configPanel.add(heightSizeComp);
      configPanel.add(wiggleHeightComp);
      configPanel.revalidate();
      configPanel.setVisible(true);

      emptyPane.setVisible(false);
      strokeComp.setVisible(false);
      heightSizeComp.setVisible(true);
      dashComp.setVisible(false);
      preSelLin.setVisible(false);
      preSelVer.setVisible(false);
      arrowComp.setVisible(false);
      wiggleLengthComp.setVisible(false);
      wiggleHeightComp.setVisible(true);
      wiggleOffsetComp.setVisible(false);
      wiggleAsymmetryComp.setVisible(false);
      endPosComp.setVisible(false);
      posComp.setVisible(true);
      preSelImg.setVisible(true);
      descriptionPanel.setVisible(true);
      descPanel.setVisible(true);
      circAngleComp.setVisible(false);
      top.setVisible(false);
      mAC.setVisible(false);
    } else if (mainFrame.getDrawPane().selectType == 3 && mainFrame.getDrawPane().shapes.size() > 0) {

      /* a shape is selected */

      defaultButton.setText(" Set as default label parameters ");

      Shaped s = mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1);

      int index = JavaFXToJavaVectorGraphic.getIndexOfPatternS(s);
      pattern.setSelectedIndex(index);

      try {
        descriptionField.setText(new String(s.description));
      } catch (NullPointerException ex) {
        descriptionField.setText("");
      }

      numModelPart.setStepSize(1);
      numModelPart.setMaximum((double) 10000);
      numModelPart.setMinimum((double) -10000);

      partLabel.setText(" Rel. x-comp.");
      disLabel.setText(" Rel. y-comp.");

      partPlaceDesc.setValue((double) s.descPosX);
      descDis.setValue(s.descPosY);
      rotDesc.setValue(180d / Math.PI * s.rotDesc);
      showDesc.setSelected(s.showDesc);
      descScale.setValue(s.descScale);

      if (showDesc.isSelected()) {
        rotDesc.setEnabled(true);
        partPlaceDesc.setEnabled(true);
        descDis.setEnabled(true);
        descScale.setEnabled(true);
      } else {
        descScale.setEnabled(false);
        rotDesc.setEnabled(false);
        descDis.setEnabled(false);
        partPlaceDesc.setEnabled(false);
      }
      circAngle.setValue(180d / Math.PI * s.rotation);
      circAngleComp.setVisible(true);
      circAngleLabel.setText(" Rotation");

      lineModel.setSelectedItem(mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.lineType());
      type.setModel(lineModel);

      dashed.setText(" Dashed");
      dashed.setSelected(mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.dashed);
      doubleLine.setSelected(mainFrame.getDrawPane().shapes.get(mainFrame.getDrawPane().shapes.size() - 1).lineConfig.doubleLine);

      if (s.lineConfig.dashed) {
        numModel.setMaximum((double) 2 * (s.width + s.height));
        numModel.setValue((double) s.lineConfig.dashLength);
        numModel.setMinimum(1d);
        if (dashLength != null) {
          dashLength.setEnabled(true);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(true);
        }
      } else {
        if (dashLength != null) {
          dashLength.setEnabled(false);
          ((JSpinner.DefaultEditor) dashLength.getEditor()).getTextField().setEditable(false);
        }
      }

      if (strokeLabel != null)
        strokeLabel.setText(" Stroke size");

      numModelStroke.setValue((double) s.lineConfig.stroke);
      if (stroke != null) {
        stroke.setEnabled(true);
        ((JSpinner.DefaultEditor) stroke.getEditor()).getTextField().setEditable(true);
      }

      if (heightSizeLabel != null)
        heightSizeLabel.setText(" Width");

      numModelHeightSize.setValue((double) s.width);
      numModelHeightSize.setMinimum((double) 10);
      numModelHeightSize.setStepSize(1d);
      if (heightSize != null) {
        heightSize.setEnabled(true);
        ((JSpinner.DefaultEditor) heightSize.getEditor()).getTextField().setEditable(true);
      }

      if (s.lineConfig.lineType == LineType.PHOTON || s.lineConfig.lineType == LineType.GLUON || s.lineConfig.lineType == LineType.SWAVE || s.lineConfig.lineType == LineType.SSPIRAL) {
        numModelWiggleHeight.setValue((double) s.lineConfig.wiggleHeight);
        wiggleHeightComp.setVisible(true);
        wiggleHeightLabel.setText(" Wiggle height");
        if (wiggleHeight != null) {
          wiggleHeight.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleHeight.getEditor()).getTextField().setEditable(true);
        }
        numModelWiggleLength.setValue((double) s.lineConfig.wiggleLength);
        wiggleLengthComp.setVisible(true);
        if (wiggleLength != null) {
          wiggleLength.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleLength.getEditor()).getTextField().setEditable(true);
        }
      } else {
        wiggleHeightComp.setVisible(false);
        wiggleLengthComp.setVisible(false);
      }

      if (s.lineConfig.lineType == LineType.GLUON || s.lineConfig.lineType == LineType.PHOTON || s.lineConfig.lineType == LineType.SSPIRAL || s.lineConfig.lineType == LineType.SWAVE) {
        numModelWiggleOffset.setValue((double) s.lineConfig.wiggleOffset);
        wiggleOffsetComp.setVisible(true);
        if (wiggleOffset != null) {
          wiggleOffset.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleOffset.getEditor()).getTextField().setEditable(true);
        }
      } else {
        wiggleOffsetComp.setVisible(false);
      }

      if (s.lineConfig.lineType == LineType.GLUON || s.lineConfig.lineType == LineType.SSPIRAL) {
        wiggleOffsetLabel.setText(" Wiggle offset");
      } else if (s.lineConfig.lineType == LineType.PHOTON || s.lineConfig.lineType == LineType.SWAVE) {
        wiggleOffsetLabel.setText(" Wiggle sharpness");
      }

      if (s.lineConfig.lineType == LineType.GLUON || s.lineConfig.lineType == LineType.SSPIRAL) {
        numModelWiggleAsymmetry.setValue((double) s.lineConfig.wiggleAsymmetry);
        wiggleAsymmetryComp.setVisible(true);
        wiggleAsymmetryLabel.setText(" Wiggle asymmetry");
        if (wiggleAsymmetry != null) {
          wiggleAsymmetry.setEnabled(true);
          ((JSpinner.DefaultEditor) wiggleAsymmetry.getEditor()).getTextField().setEditable(true);
        }
      } else {
        wiggleAsymmetryComp.setVisible(false);
      }

      posYLabel.setText(" Y-comp of center");
      numModelPosY.setValue(s.origin.y);
      if (posY != null) {
        posY.setEnabled(true);
        ((JSpinner.DefaultEditor) posY.getEditor()).getTextField().setEditable(true);
      }
      posXLabel.setText(" X-comp of center");
      numModelPosX.setValue(s.origin.x);
      if (posX != null) {
        posX.setEnabled(true);
        ((JSpinner.DefaultEditor) posX.getEditor()).getTextField().setEditable(true);
      }

      heightS.setValue((double) s.height);

      lColor.setText(" Line color");

      if (s.lineConfig.lineType == LineType.FERMION || true) {
        configPanel.removeAll();
        configPanel.add(strokeComp);
        configPanel.add(circAngleComp);
        configPanel.add(heightSizeComp);
        configPanel.add(heightSComp);
        configPanel.add(dashComp);
        if (s.fill.graphic || s.fill.name != null) {
          fillScale.setValue(s.fill.scale);
          configPanel.add(fillScaleComp);
        }
        configPanel.revalidate();
      } else {
        configPanel.removeAll();
        configPanel.add(strokeComp);
        configPanel.add(circAngleComp);
        configPanel.add(heightSizeComp);
        configPanel.add(heightSComp);
        configPanel.add(wiggleHeightComp);
        configPanel.add(wiggleLengthComp);
        configPanel.add(wiggleOffsetComp);
        if (s.lineConfig.lineType == LineType.GLUON || s.lineConfig.lineType == LineType.SSPIRAL) {
          configPanel.add(wiggleAsymmetryComp);
        }
        configPanel.add(dashComp);
        if (s.fill.graphic || s.fill.name != null) {
          fillScale.setValue(s.fill.scale);
          configPanel.add(fillScaleComp);
        }
        configPanel.revalidate();
      }
      configPanel.setVisible(true);

      top.removeAll();
      top.add(pattern);
      top.add(vFillColor);
      top.add(lColor);
      top.add(vFillImg);
      top.add(doubleLine);

      arrowComp.setVisible(false);
      emptyPane.setVisible(false);
      strokeComp.setVisible(true);
      heightSizeComp.setVisible(true);
      dashComp.setVisible(true);
      preSelLin.setVisible(false);
      preSelVer.setVisible(false);
      preSelImg.setVisible(false);
      endPosComp.setVisible(false);
      posComp.setVisible(true);
      descriptionPanel.setVisible(true);
      descPanel.setVisible(true);
      top.setVisible(true);
      mAC.setVisible(false);

    } else if (mainFrame.getDrawPane().selectType == 4 && mainFrame.getDrawPane().multiEdit != null) {
      MultiEdit mE = mainFrame.getDrawPane().multiEdit;

      if (mE.type == 0) {

        arrowComp.setVisible(true);

        top.removeAll();
        top.add(type);
        top.add(lColor);
        top.add(doubleLine);

        String typeString = mE.lineType();

        if (typeString.toUpperCase().equals("FERMION"))
          typeString = "Plain";

        if (typeString.toUpperCase().equals("GLUON"))
          typeString = "Spiral";

        if (typeString.toUpperCase().equals("PHOTON"))
          typeString = "Wave";

        if (typeString.toUpperCase().equals("SSPIRAL") || typeString.toUpperCase().equals("GLUINO"))
          typeString = "Sspiral";

        if (typeString.toUpperCase().equals("SWAVE") || typeString.toUpperCase().equals("PHOTINO"))
          typeString = "Swave";

        lineModel.setSelectedItem(typeString);
        type.setModel(lineModel);

        lColor.setText(" Color");
        if (strokeLabel != null)
          strokeLabel.setText(" Stroke size");
        wiggleHeightLabel.setText(" Wiggle height");
        if (mE.getLineType() == LineType.GLUON || mE.getLineType() == LineType.SSPIRAL) {
          wiggleOffsetLabel.setText(" Wiggle offset");
        } else {
          wiggleOffsetLabel.setText(" Wiggle sharpness");
        }
        dashed.setText(" Dashed");

        doubleLine.setSelected(mE.getDoubleLine());

        dashed.setSelected(mE.getDashed());
        dashLength.setEnabled(dashed.isSelected());
        dashLength.setValue((double) mE.getDashLength());
        numModel.setMaximum((double) 10000000);
        numModel.setMinimum(1d);

        stroke.setValue((double) mE.getStroke());

        showArrow.setSelected(mE.getShowArrow());
        arrowPlace.setEnabled(showArrow.isSelected());
        arrowSize.setEnabled(showArrow.isSelected());
        arrowDent.setEnabled(showArrow.isSelected());
        arrowHeight.setEnabled(showArrow.isSelected());
        arrowPlace.setValue(mE.getArrowPlace());
        arrowSize.setValue((double) mE.getArrowSize());
        arrowDent.setValue((double) mE.getArrowDent());
        arrowHeight.setValue((double) mE.getArrowHeight());

        wiggleHeight.setValue((double) mE.getWiggleHeight());
        wiggleLength.setValue((double) mE.getWiggleLength());
        wiggleOffset.setValue((double) mE.getWiggleOffset());
        wiggleAsymmetry.setValue((double) mE.getWiggleAsymmetry());
        straightLen.setValue((double) mE.getStraightLength());
        straightLenArrow.setValue((double) mE.getStraightLengthArrow());
        straightLenArrow.setEnabled(mE.getShowArrow());

        if (mE.getLineType() == LineType.FERMION) {
          configPanel.removeAll();
          configPanel.add(strokeComp);
          configPanel.add(circAngleComp);
          configPanel.add(dashComp);
          configPanel.revalidate();
        } else {
          configPanel.removeAll();
          configPanel.add(strokeComp);
          configPanel.add(wiggleHeightComp);
          configPanel.add(wiggleLengthComp);
          configPanel.add(wiggleOffsetComp);
          if (mE.getLineType() == LineType.GLUON || mE.getLineType() == LineType.SSPIRAL) {
            configPanel.add(wiggleAsymmetryComp);
          }
          configPanel.add(straightLenComp);
          configPanel.add(straightLenArrowComp);
          configPanel.add(dashComp);
          configPanel.revalidate();
        }
        top.setVisible(true);
        configPanel.setVisible(true);
        preSelLin.setVisible(true);
        preSelVer.setVisible(false);
        preSelImg.setVisible(false);

      } else if (mE.type == 1) {
        arrowComp.setVisible(false);

        if (strokeLabel != null)
          strokeLabel.setText(" Border stroke size");
        dashed.setText(" Border dashed");
        wiggleHeightLabel.setText(" Rotation");
        if (heightSizeLabel != null)
          heightSizeLabel.setText(" Size");

        vertModel.setSelectedItem(mE.getShape());
        type.setModel(vertModel);

        int index = JavaFXToJavaVectorGraphic.getIndexOfPattern(mE.getVertex());
        pattern.setSelectedIndex(index);

        dashed.setSelected(mE.getDashed());
        dashLength.setEnabled(dashed.isSelected());
        dashLength.setValue((double) mE.getDashLength());
        numModel.setMaximum((double) mE.getSize() * 2 * Math.PI);
        numModel.setMinimum(1d);

        stroke.setValue((double) mE.getStroke());

        heightSize.setValue((double) mE.getSize());
        numModelHeightSize.setMinimum(0d);
        numModelHeightSize.setStepSize(1d);

        wiggleHeight.setValue((double) mE.getRotation());

        configPanel.removeAll();
        configPanel.add(strokeComp);
        configPanel.add(heightSizeComp);
        configPanel.add(wiggleHeightComp);
        configPanel.add(dashComp);
        if (mE.getGraphic() || mE.getName() != null) {
          fillScale.setValue(mE.getScale());
          configPanel.add(fillScaleComp);
        }
        configPanel.revalidate();
        configPanel.setVisible(true);

        top.removeAll();
        top.add(type);
        top.add(pattern);
        top.add(vFillColor);
        top.add(vFillImg);
        top.add(vBorderColor);
        top.setVisible(true);
        preSelLin.setVisible(false);
        preSelVer.setVisible(true);
        preSelImg.setVisible(false);
      } else if (mE.type == 2) {
        arrowComp.setVisible(false);

        configPanel.removeAll();
        configPanel.add(heightSizeComp);
        configPanel.add(wiggleHeightComp);
        configPanel.revalidate();
        configPanel.setVisible(true);

        if (heightSizeLabel != null)
          heightSizeLabel.setText(" Scale of image");

        wiggleHeightLabel.setText(" Rotation");

        heightSize.setValue(mE.getScale());
        numModelHeightSize.setMinimum((double) -10000);
        numModelHeightSize.setStepSize(.01);

        wiggleHeight.setValue((double) mE.getRotation());

        top.setVisible(false);
        preSelLin.setVisible(false);
        preSelVer.setVisible(false);
        preSelImg.setVisible(true);
      }

      /*meDescField.setText(mE.getDescription());
      this.add(meDescPanel);
      meDescPanel.setVisible(true);*/

      try {
        descriptionField.setText(new String(mE.getDescription()));
      } catch (NullPointerException ex) {
        descriptionField.setText("");
      }

      showDesc.setSelected(mE.getShowDesc());


      emptyPane.setVisible(false);
      dashComp.setVisible(true);
      strokeComp.setVisible(true);
      heightSizeComp.setVisible(true);
      wiggleLengthComp.setVisible(true);
      wiggleHeightComp.setVisible(true);
      wiggleOffsetComp.setVisible(true);
      wiggleAsymmetryComp.setVisible(true);
      endPosComp.setVisible(false);
      posComp.setVisible(false);
      descriptionPanel.setVisible(false);
      descPanel.setVisible(true);
      circAngleComp.setVisible(true);
      mAC.setVisible(false);

      for (Component comp : this.getComponents()) {
        if (comp instanceof JSeparator)
          comp.setVisible(false);
      }
    } else {

      /* nothing is selected */

      configPanel.setVisible(false);

      for (Component comp : this.getComponents()) {
        if (comp instanceof JSeparator)
          comp.setVisible(false);
      }

      emptyPane.setVisible(true);
      dashComp.setVisible(false);
      strokeComp.setVisible(false);
      heightSizeComp.setVisible(false);
      preSelLin.setVisible(false);
      preSelVer.setVisible(false);
      arrowComp.setVisible(false);
      wiggleLengthComp.setVisible(false);
      wiggleHeightComp.setVisible(false);
      wiggleOffsetComp.setVisible(false);
      wiggleAsymmetryComp.setVisible(false);
      endPosComp.setVisible(false);
      posComp.setVisible(false);
      preSelImg.setVisible(false);
      descriptionPanel.setVisible(false);
      descPanel.setVisible(false);
      circAngleComp.setVisible(false);
      top.setVisible(false);
      mAC.setVisible(false);
    }
    preSelLin.resize(getWidth());
    preSelVer.resize(getWidth());
    preSelImg.resize(getWidth());
    if (mainFrame.getEframe() != null) {
      mainFrame.getEframe().repaint();
      mainFrame.getEframe().pack();
    }
    updating = false;
  }
}
