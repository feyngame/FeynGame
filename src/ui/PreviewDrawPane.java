package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.swing.JPanel;

import game.FeynLine;
import game.FloatingObject;
import game.LineType;
import game.Point2D;
import game.Shaped;
import game.Vertex;

public class PreviewDrawPane extends JPanel {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Frame.getLogger(PreviewDrawPane.class);

  private static final RenderingHints AA_ON = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
  private static final int EDGE_SIZE = 20;
  private static final int MIN_DRAW_SIZE = 20;
  private static final int MIN_TOTAL_SIZE = MIN_DRAW_SIZE + 2*EDGE_SIZE;
  
  private final Pane drawPane;
  private int preferredWidth;
  
  public PreviewDrawPane(int preferredWidth, Frame frameForInternalPane) {
    this.drawPane = new Pane(frameForInternalPane, false);
    this.drawPane.helperLines = false;
    this.drawPane.grid = false;
    this.drawPane.showCurrentSelectedLine = false;
    this.drawPane.mBB = false;
    this.preferredWidth = preferredWidth;
    setBackground(Color.WHITE);
    clearPreview();
  }
  
  public void clearPreview() {
    drawPane.clearPane(Frame.CLEARMODE_CLEAR_ALL);
  }
  
  @SuppressWarnings("unchecked")
  public void updatePreview(File saveFile) {
    try (FileInputStream fis = new FileInputStream(saveFile);
        ObjectInputStream ois = new ObjectInputStream(fis)) {
      String diaVersion = (String) ois.readObject();
      try {
        drawPane.lines = (ArrayList<FeynLine>) ois.readObject();
        boolean earlierThanAFA = false;
        if (diaVersion.equals("1.0.1") || diaVersion.equals("1.0.0"))
          earlierThanAFA = true;
        for (FeynLine line : drawPane.lines) {
          if (line.lineConfig.lineType == LineType.SCALAR) {
            earlierThanAFA = true;
            break;
          }
        }
        if (earlierThanAFA) {
          for (FeynLine line : drawPane.lines) {
            if (line.lineConfig.lineType == LineType.SCALAR) {
              line.lineConfig.lineType = LineType.FERMION;
            } else if (line.lineConfig.lineType == LineType.FERMION) {
              line.lineConfig.showArrow = true;
            }
          }
        }
        for (FeynLine line: drawPane.lines) {
          if (line.lineConfig.arrowHeight <= 0 && line.lineConfig.showArrow) line.lineConfig.arrowHeight = line.lineConfig.arrowSize;
        }
        drawPane.vertices = (ArrayList<Vertex>) ois.readObject();
        drawPane.fObjects = (ArrayList<FloatingObject>) ois.readObject();
        game.Filling.pathToImageNumber = (HashMap<String, Integer>) ois.readObject();
        drawPane.redoListLines = (ArrayList<ArrayList<FeynLine>>) ois.readObject();
        drawPane.redoListVertices = (ArrayList<ArrayList<Vertex>>) ois.readObject();
        drawPane.redoListfObjects = (ArrayList<ArrayList<FloatingObject>>) ois.readObject();
        drawPane.undoListLines = (ArrayList<ArrayList<FeynLine>>) ois.readObject();
        drawPane.undoListVertices = (ArrayList<ArrayList<Vertex>>) ois.readObject();
        drawPane.undoListfObjects = (ArrayList<ArrayList<FloatingObject>>) ois.readObject();
        //wobbleFloatingVertex()
        try {
          drawPane.shapes = (ArrayList<Shaped>) ois.readObject();
          drawPane.undoListShapes = (ArrayList<ArrayList<Shaped>>) ois.readObject();
          drawPane.redoListShapes = (ArrayList<ArrayList<Shaped>>) ois.readObject();
        } catch (EOFException ignored) {
          drawPane.shapes = new ArrayList<>();
          drawPane.undoListShapes = new ArrayList<>();
          drawPane.redoListShapes = new ArrayList<>();
        }
      } catch (Exception ex1) {
        LOGGER.log(Level.SEVERE, ex1.getMessage(), ex1);
      }
    } catch (StreamCorruptedException e) {
      //Don't log this one at all, it is part of control flow because there is no other way to test if a file is a fg file.
      clearPreview();
    } catch (Exception ex) {
      clearPreview();
      LOGGER.log(Level.SEVERE, null, ex);
    }
    
    Rectangle bounds = drawPane.getBoundsPainted();
    drawPane.crop(bounds);
  }
  
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    final Graphics2D g2d = (Graphics2D) g.create();
    g2d.setRenderingHints(AA_ON);
    
    
    if(getSize().width <= MIN_TOTAL_SIZE || getSize().height <= MIN_TOTAL_SIZE) { 
      //too small to display anything
      return;
    }

    double xscale = (double)getWidth() / (double)drawPane.getWidth();
    double yscale = (double)getHeight() / (double)drawPane.getHeight();
    double scale = Math.min(xscale, yscale);
    double shiftX = -((double)drawPane.getWidth() * scale - (double)getWidth()) / 2.0;
    double shiftY = -((double)drawPane.getHeight() * scale - (double)getHeight()) / 2.0;
    g2d.translate(shiftX, shiftY);
    g2d.scale(scale, scale);
    
    drawPane.paintComponent(g2d);
  }
  
  @Override
  public Dimension getPreferredSize() {
    return new Dimension(preferredWidth, super.getPreferredSize().height);
  }
  
  public void setPreferredWidth(int width) {
    preferredWidth = width;
    getParent().revalidate();
  }
  
  private static Stream<Point2D> calculateArchPoint(FeynLine line) {
    if(Math.abs(line.getHeight()) > 0) {
      double midX = (line.getEndX() + line.getStartX()) / 2.0;
      double midY = (line.getEndY() + line.getStartY()) / 2.0;
      double slopeX = line.getEndX() - line.getStartX();
      double slopeY = line.getEndY() - line.getStartY();
      double slopeNorm = Math.sqrt(slopeX * slopeX + slopeY * slopeY);
      //perp is -slopeY, +slopeX
      double archX = midX - slopeY * line.getHeight() / slopeNorm;
      double archY = midY + slopeX * line.getHeight() / slopeNorm;
      Point2D archPoint = new Point2D(archX, archY);
      return Stream.of(line.getStart(), line.getEnd(), archPoint);
    } else {
      return Stream.of(line.getStart(), line.getEnd());
    }
  }
  
}
