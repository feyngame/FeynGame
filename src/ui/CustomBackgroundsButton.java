//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class CustomBackgroundsButton extends JButton {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 33L;

  private Color hoverBackgroundColor;
  private Color pressedBackgroundColor;
  private Color backgroundColorSelect;
  private Color hoverBackgroundColorSelect;
  private Color pressedBackgroundColorSelect;
  private Color backgroundColorHover;
  private Color hoverBackgroundColorHover;
  private Color pressedBackgroundColorHover;
  public boolean pressed = false;
  public boolean hovered = false;
  public boolean thisHovered = false;
  private TabButton parent;

  public CustomBackgroundsButton() {
    this(null, null);
  }

  public CustomBackgroundsButton(String text, TabButton parent) {
    super(text);
    this.parent = parent;
    super.setContentAreaFilled(false);
  }

  @Override
  protected void paintComponent(Graphics g) {
    this.setOpaque(false);
    if (pressed) {
      if (thisHovered) {
        g.setColor(hoverBackgroundColorSelect);
      } else {
        g.setColor(backgroundColorSelect);
      }
    } else if (hovered || thisHovered) {
      if (thisHovered) {
        g.setColor(hoverBackgroundColorHover);
      } else {
        g.setColor(backgroundColorHover);
      }
    } else {
      if (thisHovered) {
        g.setColor(hoverBackgroundColor);
      } else {
        g.setColor(getBackground());
      }
    }
    g.fillRect(0, 0, getWidth(), getHeight());
    this.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseEntered(java.awt.event.MouseEvent evt) {
        parent.hovered = true;
        thisHovered = true;
      }

      public void mouseExited(java.awt.event.MouseEvent evt) {
        parent.hovered = false;
        thisHovered = false;
      }
    });
    super.paintComponent(g);
  }

  @Override
  public void setContentAreaFilled(boolean b) {
  }

  public Color getHoverBackgroundColor() {
    return hoverBackgroundColor;
  }

  public void setHoverBackgroundColor(Color hoverBackgroundColor) {
    this.hoverBackgroundColor = hoverBackgroundColor;
  }

  public Color getPressedBackgroundColor() {
    return pressedBackgroundColor;
  }

  public void setPressedBackgroundColor(Color pressedBackgroundColor) {
    this.pressedBackgroundColor = pressedBackgroundColor;
  }

  public Color getHoverBackgroundColorSelect() {
    return hoverBackgroundColorSelect;
  }

  public void setBackgroundColorSelect(Color backgroundColorSelect) {
    this.backgroundColorSelect = backgroundColorSelect;
  }

  public Color getBackgroundColorSelect() {
    return backgroundColorSelect;
  }

  public void setHoverBackgroundColorSelect(Color hoverBackgroundColorSelect) {
    this.hoverBackgroundColorSelect = hoverBackgroundColorSelect;
  }

  public Color getPressedBackgroundColorSelect() {
    return pressedBackgroundColorSelect;
  }

  public void setPressedBackgroundColorSelect(Color pressedBackgroundColorSelect) {
    this.pressedBackgroundColorSelect = pressedBackgroundColorSelect;
  }

  public Color getHoverBackgroundColorHover() {
    return hoverBackgroundColorHover;
  }

  public void setBackgroundColorHover(Color backgroundColorHover) {
    this.backgroundColorHover = backgroundColorHover;
  }

  public Color getBackgroundColorHover() {
    return backgroundColorHover;
  }

  public void setHoverBackgroundColorHover(Color hoverBackgroundColorHover) {
    this.hoverBackgroundColorHover = hoverBackgroundColorHover;
  }

  public Color getPressedBackgroundColorHover() {
    return pressedBackgroundColorHover;
  }

  public void setPressedBackgroundColorHover(Color pressedBackgroundColorHover) {
    this.pressedBackgroundColorHover = pressedBackgroundColorHover;
  }
}
