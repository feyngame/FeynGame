//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Taskbar;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import game.Presets;
import resources.GetResources;
/**
 * this class opens a window that lets one select wether or not to convert the canvas to a model file after loading a ".fg"
 * file
 */
public class ConvCanToModelDialog extends JFrame {
  /**
   * checkbox to let the user set the chosen option to be used from now on automatically
   */
  private JCheckBox askAgain;
  /**
   * the drawPane from which to create the model file form
   */
  private Pane drawPane;
  /**
   * the dialog frame
   */
  private JDialog dia;
  /**
   * the main frame
   */
  private Frame mainFrame;
  /**
   * the function to open the window
   * @param mainFrame the mainFrame
   * @param drawPane the canvas to convert
   * @param name the file for the new model (does not create the file !!!!!)
   */
  public ConvCanToModelDialog(Frame mainFrame, Pane drawPane, String name) {
    /* initialization of the window; somehow wanted to ensure that this
     * window behaves like a simple popup window on all platforms
     * including ones working with tiling window managers etc.*/

    this.setAlwaysOnTop(false);
    this.setResizable(false);

    this.mainFrame = mainFrame;

    /* look of the window */

    this.setTitle("Export Feynman diagram");

    this.setIconImage(GetResources.getAppIcon());
    try {
        final Taskbar taskbar = Taskbar.getTaskbar();
        taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    this.drawPane = drawPane;

    this.setLayout(new GridLayout(0, 1));

    this.add(new JLabel(" Do you want to create a model file from the contents of the diagram? "));

    JPanel buttonPanel = new JPanel(new FlowLayout());

    JButton ok = new JButton("Yes");
    ok.addActionListener(e -> {

      if (askAgain.isSelected()) {
        mainFrame.convCanToModel = 1;
      }
      Presets preset = new Presets(drawPane);
      mainFrame.tileTabs.addModel(preset, name);

      mainFrame.tileTabs.update();

      this.setVisible(false);
      mainFrame.requestFocus();
    });
    buttonPanel.add(ok);

    JButton cancel = new JButton("No");
    cancel.addActionListener(e -> {
      if (askAgain.isSelected()) {
        mainFrame.convCanToModel = 2;
      }
      this.setVisible(false);
      mainFrame.requestFocus();
    });
    buttonPanel.add(cancel);

    this.add(buttonPanel);

    askAgain = new JCheckBox("Do not ask again");
    askAgain.setSelected(false);

    this.add(askAgain);

    this.pack();
    this.setLocationRelativeTo(null);
    this.setVisible(true);
  }
}
