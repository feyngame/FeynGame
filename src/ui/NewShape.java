//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.*;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.*;
import java.awt.*;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * this class opens a window that lets one select the shape (Rectangle/Oval
 * atm) for a {@link game.Shaped} object to be added to the canvas
 */
public class NewShape {
  /**
   * the to be newly added {@link game.Shaped} object
   */
  private Shaped nS;
  /**
   * the window that is shown
   */
  private JDialog dia;
  /**
   * the compoent that lets one customize the new {@link nS} object
   */
  private JPanel confPanel;
  /**
   * constructor: does nothing
   */
  public NewShape(){

  }
  /**
   * the function to open the window to let the shape be added
   * @param frame the main Window where the shape is added to
   * @param origin the point where the shape is added to
   */
  public void newShape(Frame frame, Point2D origin) {
    /* initialization of the window; somehow wanted to ensure that this
     * window behaves like a simple popup window on all platforms
     * including ones working with tiling window managers etc.*/
    dia = new JDialog();
    dia.setUndecorated(false);
    // dia.setAlwaysOnTop(false);
    dia.setResizable(false);
    // dia.setType(Type.POPUP);

    /* initialization of the shape to be added at the correct spot */
    nS = new Shaped(origin);

    dia.setLayout(new BorderLayout());

    /* select the shape (Rectangle/Oval atm)*/

    JPanel shapePanel = new JPanel();
    JLabel shapeLabel = new JLabel("Select the shape");
    JComboBox<String> shapeBox = new JComboBox<String>(Shaped.SHAPES.toArray(new String[Shaped.SHAPES.size()]));
    shapeBox.addActionListener(e -> {
      nS.shape = (String) shapeBox.getSelectedItem();
    });
    shapePanel.add(shapeLabel);
    shapePanel.add(shapeBox);
    dia.add(shapePanel, BorderLayout.NORTH);

    /* select a line preset as the border config for {@link nS}
     * this is not added to the window as we dont want the user to
     * costumize the shape further at this point*/

    confPanel = new JPanel();

    for (LineConfig lc : LineConfig.presets) {
      if (lc.lineType != LineType.GRAB) {
        ConfigButton cB = new ConfigButton(lc);
        cB.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        cB.addActionListener(new CBAL(lc));
        confPanel.add(cB);
      }
    }

    // dia.add(confPanel, BorderLayout.CENTER);

    JPanel buttonPanel = new JPanel(new GridLayout(1, 2));

    /* add the shape or cancel */

    JButton okButton = new JButton("Add shape");
    okButton.addActionListener(e -> {
      frame.saveForUndo();
      frame.getDrawPane().shapes.add(nS);
      frame.getDrawPane().selectType = 3;
      dia.setVisible(false);
    });
    buttonPanel.add(okButton);

    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(e -> {
      dia.setVisible(false);
    });
    buttonPanel.add(cancelButton);

    dia.add(buttonPanel, BorderLayout.SOUTH);

    dia.pack();

    dia.setLocationRelativeTo(null);

    dia.setVisible(true);

  }
  /**
   * this subclass is for the Buttons that lets one select the line configs as
   * a border for the shape
   * not used!
   */
  class ConfigButton extends JButton {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 21L;
    /**
     * the line config to be set as the border config
     */
    public LineConfig lc;
    /**
     * there may be a label for the line config
     */
    private final JLabel label;
    /**
     * basic constructor of the button
     * @param lc the line config
     */
    private ConfigButton(LineConfig lc) {
      this.lc = lc;
      this.setBackground(Color.WHITE);
      this.setOpaque(false);
      if (lc.lineType != LineType.GRAB) {
        //noinspection SuspiciousNameCombination
        this.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
      }
      if (lc.description != null) {
        label = new JLabel(lc.description);
      } else {
        label = new JLabel("");
      }
    }

    /**
     * Creates a temporary {@link game.FeynLine} and calls {@link game.FeynLine#draw(Graphics2D, boolean)} to draw a preview.
     *
     * @param g2 Graphics object to draw to
     */
    @Override
    protected void paintComponent(Graphics g2) {
      super.paintComponent(g2);

      Graphics2D g = (Graphics2D) g2.create();

      RenderingHints rh = new RenderingHints(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
      g.setRenderingHints(rh);

      if (lc.lineType == LineType.GRAB)
        return;

      if (lc.description == null) {
        int x = 0;
        int y = getWidth() / 2;
        int width = getWidth();

        g.setColor(Color.BLACK);

        FeynLine fl = new FeynLine(new game.Line(x + width / 8, y, x + width - width / 8, y), lc, 0);
        fl.hideMom();
        fl.draggedMode = 2;
        fl.draw(g, false);
      } else {
        int x = 0;
        int width = getWidth();

        g.setColor(Color.BLACK);
        double widthL = label.getPreferredSize().getWidth();
        double heightL = label.getPreferredSize().getHeight();
        int offset = (int) (width - widthL) / 2;


        g.translate(offset, -5 - (int) heightL + getHeight());
        label.setSize(label.getPreferredSize());
        label.paint(g);
        g.translate(-offset, 5 + (int) heightL - getHeight());

        int restheight = getHeight() - (int) heightL - 5;

        g.setColor(Color.BLACK);

        FeynLine fl = new FeynLine(new game.Line(x + width / 8, restheight / 2, x + width - width / 8, restheight / 2), lc, 0);
        fl.hideMom();
        fl.showDesc = false;
        fl.draggedMode = 2;
        fl.draw(g, false);
      }
    }
  }
  /**
   * the actions listeners for the {@link ConfigButton}s
   */
  class CBAL implements ActionListener {
    /**
     * the line config of the button
     */
    LineConfig lc;
    /**
     * basic constructor
     * @param lc the lineConfig of the corresponding button
     */
    CBAL(LineConfig lc) {
      this.lc = lc;
    }
    /**
     * when the corresponding button is pressed, set the border config of
     * {link nS} to the lineConfig of corresponding to the button
     * @param e the ActionEvent when the button is pressed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      nS.lineConfig = new LineConfig(lc);

      for (Component l : confPanel.getComponents()) {
        if (((ConfigButton) l).lc.equals(this.lc)) {
          ((ConfigButton) l).setBorder(BorderFactory.createLineBorder(Color.black, 5));
        } else {
          ((ConfigButton) l).setBorder(UIManager.getBorder("Button.border"));
        }
      }
    }
  }
}
