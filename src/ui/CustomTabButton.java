//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.*;
import javax.swing.UIManager;

public class CustomTabButton extends JButton {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 27L;

  private Color hoverBackgroundColor;
  private Color pressedBackgroundColor;
  public boolean pressed = false;
  public boolean hovered = false;
  public boolean thisHovered = false;

  public CustomTabButton() {
    this(null);
  }

  public CustomTabButton(String text) {
    super(text);
    super.setContentAreaFilled(false);
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    this.setOpaque(true);

    Graphics2D g2d = (Graphics2D) g.create();

    Color bgColor = new Color(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue());

    Color borderColor = UIManager.getColor("InternalFrame.borderColor");

    borderColor = Color.WHITE;

    g2d.setColor(borderColor);
    g2d.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

    g2d.drawLine(0, 0, 0, getHeight());
    g2d.drawLine(0, getHeight(), getWidth(), getHeight());
    g2d.drawLine(getWidth(), getHeight(), getWidth(), 0);
    if (!getModel().isPressed()) {
      g2d.drawLine(getWidth(), 0, 0, 0);
    }

    if (pressed) {
      g2d.setColor(pressedBackgroundColor);
    } else if (thisHovered || hovered) {
      g2d.setColor(hoverBackgroundColor);
    } else {
      g2d.setColor(bgColor);
    }
    g2d.fillRect(0, 0, getWidth(), getHeight());

    g2d.dispose();
  }

  @Override
  public void setContentAreaFilled(boolean b) {
  }

  public Color getHoverBackgroundColor() {
    return hoverBackgroundColor;
  }

  public void setHoverBackgroundColor(Color hoverBackgroundColor) {
    this.hoverBackgroundColor = hoverBackgroundColor;
  }

  public Color getPressedBackgroundColor() {
    return pressedBackgroundColor;
  }

  public void setPressedBackgroundColor(Color pressedBackgroundColor) {
    this.pressedBackgroundColor = pressedBackgroundColor;
  }
}
