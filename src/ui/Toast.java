//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.Point2D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import static java.awt.GraphicsDevice.WindowTranslucency.*;
import java.awt.event.ActionEvent;
/**
 * this is like a tooltip at the right top corner of the canvas
 */
class Toast extends JDialog {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 9L;
  /**
   * the translucency of the window, it is decreased when the tooltip
   * disappears creating a fade out effect (system needs to support
   * translucency)
   */
  double d;
  /**
   * the height of the dislayed text
   */
  int hei;
  /**
   * the width of the displayed text
   */
  int wid;
  /**
   * a JLabel that houses the text that will be displayed
   */
  JLabel text;
  /**
   * wether or not translucency is supported by the system
   */
  boolean translucencySupported;
  /**
   * the canvas (Why convert to JPanel?)
   */
  JPanel drawPanel;
  /**
   * the center of the toast
   */
  Point2D center;
  /**
   * the scale with which the toast is getting smaller at the end of its
   * lifetime (the closing animation)
   */
  double scale;
  /**
   * the factor with which the scale gets multiplied at each step of the
   * ending animation
   */
  double scaleStep = 1;
  /**
   * constructor: sets all the right flags of the window so that it behaves
   * like a tooltip, checks it translucency is supported, places it correctly
   * DOES NOT SHOW THE WINDOW!!!
   * @param s the string to display
   * @param drawPanel the canvas on top of which to display this popup
   */
  Toast(String s, Pane drawPanel) {
    this.drawPanel = drawPanel;
    /* make this a proper tooltip*/
    this.setUndecorated(true);
    this.setAlwaysOnTop(true);
    // this.setResizable(false);
    this.setType(Type.POPUP);
    // make the background transparent
    // this.setBackground(new Color(0, 0, 0, 0));
    text = new JLabel(s, JLabel.CENTER);
    text.setSize(text.getPreferredSize());
    // /* check for support of translucency*/
    // // Determine what the default GraphicsDevice can support.
    // GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    // GraphicsDevice gd = ge.getDefaultScreenDevice();
    // translucencySupported = gd.isWindowTranslucencySupported(TRANSLUCENT);

    /* get dimensions of the displayed message*/

    wid = text.getPreferredSize().width/*g.getFontMetrics().stringWidth(s)*/;
    hei = text.getPreferredSize().height/*g.getFontMetrics().getHeight()*/;

    // create a panel
    InfoPanel p = new InfoPanel();

    p.addMouseListener(new MouseListener() {
      @Override
      public void mouseClicked(MouseEvent mouseEvent) {
        Toast.this.setVisible(false);
      }

      @Override
      public void mousePressed(MouseEvent mouseEvent) {

      }

      @Override
      public void mouseReleased(MouseEvent mouseEvent) {

      }

      @Override
      public void mouseEntered(MouseEvent mouseEvent) {

      }

      @Override
      public void mouseExited(MouseEvent mouseEvent) {

      }
    });

    this.add(p);



    /* place the window correctly */
    Point toConv = new Point(drawPanel.getWidth() - 20 - wid, 0);
    SwingUtilities.convertPointToScreen(toConv, drawPanel);
    Point compPos = drawPanel.getLocationOnScreen();
    toConv.x = drawPanel.getWidth() - 20 - wid + compPos.x;
    Point2D pos = new Point2D(toConv.x, toConv.y);
    center = new Point2D(pos.x + wid/2, pos.y + hei/2 + 20);
    this.setScale(1d);
  }

  void setPosition() {
    double width = this.wid + 20;
    double height = this.hei + 20;
    width *= scale;
    height *= scale;

    this.setLocation((int) Math.round(center.x - width / 2), (int) Math.round(center.y - height/2));
  }
  /**
   * sets a new scale and repositions the window
   * @param scale the new scale
   */
  void setScale(double scale) {
    this.scale = scale;
    this.setSize((int) Math.round((this.wid + 20) * scale), (int) Math.round((this.hei + 20) * scale));
    this.setPosition();
    this.revalidate();
    this.repaint();
  }

  /**
   * action listener for the timer
   */
  private class TimerListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      scaleStep -= 0.01;
      if (scaleStep > 0) {
        setScale(scale * scaleStep);
        Timer timer = new Timer(1, new TimerListener());
        timer.setRepeats(false);
        timer.start();
      } else {
        setVisible(false);
      }
    }
  }

  /**
   * function to pop up the toast
   * starts the timer that hides the window eventually (possibly after
   * fade-out)
   */
  void showtoast() {

    try {
      this.setVisible(true);
      this.setFocusableWindowState(false);
      drawPanel.getParent().requestFocusInWindow();

      ActionListener taskPerformer = evt -> {
        Timer timer = new Timer(1, new TimerListener());
        timer.setRepeats(false);
        timer.start();
      };
      Timer timer = new Timer(2000, taskPerformer);
      timer.setRepeats(false);
      timer.start();

    } catch (Exception e) {
      e.printStackTrace();
    }

    /* first version faded out the toasts */
    // if (translucencySupported) {
    //     /* when translucency is supported decrease the opacity when timer
    //     * has finished coninually and eventually hiding the window*/
    //     try {
    //         this.setOpacity(1.0f);
    //         this.setVisible(true);
    //         this.setFocusableWindowState(false);
    //         drawPanel.getParent().requestFocusInWindow();
    //
    //         ActionListener taskPerformer = evt -> {
    //             for (d = 1.0; d > 0.2; d -= 0.1) {
    //                 ActionListener taskPerformer1 = evt1 -> Toast.this.setOpacity((float) d);
    //                 Timer timer = new Timer(100, taskPerformer1);
    //                 timer.setRepeats(false);
    //                 timer.start();
    //
    //             }
    //
    //             // set the visibility to false
    //             Toast.this.setVisible(false);
    //         };
    //         Timer timer = new Timer(2000, taskPerformer);
    //         timer.setRepeats(false);
    //         timer.start();
    //
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //     }
    // } else {
    //     /* when translucency is not supported hide the window when timer
    //     * ends*/
    //     try {
    //         this.setVisible(true);
    //         this.setFocusableWindowState(false);
    //         drawPanel.requestFocusInWindow();
    //
    //         ActionListener taskPerformer2 = evt -> {
    //             // set the visibility to false
    //             Toast.this.setVisible(false);
    //         };
    //         Timer timer = new Timer(2100, taskPerformer2);
    //         timer.setRepeats(false);
    //         timer.start();
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //     }
    // }
  }
  /**
   * this class is what is actually displayed inside the popup window
   */
  class InfoPanel extends JPanel {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 10L;

    @Override
    public void paintComponent(Graphics g) {
      //super.paintComponent(g);

      Graphics2D g2d = (Graphics2D) g.create();


      // draw the boundary of the toast and fill it
      // Color bg = UIManager.getColor("Panel.background");
      // g.setColor(bg);
      // g.fillRect(10, 10, wid + 20, hei + 20);
      // g.setColor(bg);
      // g.drawRect(10, 10, wid + 20, hei + 20);

      // set the color of text
      // Color fg = UIManager.getColor("Label.foreground");
      // g.setColor(fg);
      // Font font = UIManager.get("Label.font");
      // g.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
      // g.setFont(UIManager.get("Label.font"));

      g2d.scale(scale, scale);

      g2d.translate(10, 10);

      text.paint(g2d);

      g2d.translate(-10, -10);

      g2d.scale(1/scale, 1/scale);
      // g.drawString(s, 25, 27);
      // int t = 250;
      //
      // // draw the shadow of the toast
      // for (int i = 0; i < 4; i++) {
      //     t -= 60;
      //     g.setColor(new Color(0, 0, 0, t));
      //     g.drawRect(10 - i, 10 - i, wid + 20 + i * 2,
      //             hei + 20 + i * 2);
      // }

    }
  }
}
