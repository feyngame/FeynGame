//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * this is used in the editframe to choose the image presets as the image of the
 * currently selected image
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
public class PresetSelectImage extends JScrollPane {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 14L;
  /**
   * Gives access to the main frame;
   */
  Frame mainFrame;
  /**
   * the container
   */
  JPanel panel;
  /**
   * the constructor that initializes the individual tiles and lays them out
   * @param mainFrame the main Window
   */
  PresetSelectImage(Frame mainFrame) {


    panel = new JPanel();

    int uiHeight = TileSelect.uiHeight;

    this.mainFrame = mainFrame;
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();

    panel.setLayout(gridbag);
    c.weightx = 1;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = -1;

    int i = 0;
    int eights = 0;

    for (FloatingImage t : FloatingImage.fIPresets) {
      if (i == 8 * (eights + 1)) {
        eights++;
        c.gridy++;
        c.gridx = 0;
      } else {
        c.gridx++;
      }
      ImageLabel l = new ImageLabel(t);
      l.setPreferredSize(new Dimension(uiHeight, uiHeight));
      if (mainFrame.gameMode == 1)
        i++;
      l.addActionListener(new myListener(i));
      if (mainFrame.gameMode == 1)
        i--;
      panel.add(l, c);
      i++;
    }

    int numberOfTiles = FloatingImage.fIPresets.size();
    panel.setPreferredSize(new Dimension(Math.min((numberOfTiles), 8) * TileSelect.uiHeight, (int) (Math.ceil((numberOfTiles) / 8.0f)) * (TileSelect.uiHeight)));

    panel.setVisible(true);

    this.setViewportView(panel);
    this.setPreferredSize(new Dimension(Math.min((numberOfTiles), 8) * TileSelect.uiHeight + 10, (int) Math.min(Math.ceil((numberOfTiles) / 8.0f), 3) * TileSelect.uiHeight + 10));

  }
  /**
   * resize the container to fill the width with a fitting number of tiles per row
   */
  public void resize(int width) {
    panel.removeAll();

    int uiHeight = TileSelect.uiHeight;

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();

    panel.setLayout(gridbag);
    c.weightx = 1;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = -1;

    int i = 0;
    int eights = 0;

    int columns = (int) width / (uiHeight + 10);

    for (FloatingImage t : FloatingImage.fIPresets) {
      if (i == columns * (eights + 1)) {
        eights++;
        c.gridy++;
        c.gridx = 0;
      } else {
        c.gridx++;
      }
      ImageLabel l = new ImageLabel(t);
      l.setPreferredSize(new Dimension(uiHeight, uiHeight));
      if (mainFrame.gameMode == 1)
        i++;
      l.addActionListener(new myListener(i));
      if (mainFrame.gameMode == 1)
        i--;
      panel.add(l, c);
      i++;
    }

    int numberOfTiles = FloatingImage.fIPresets.size();
    panel.setPreferredSize(new Dimension(Math.min((numberOfTiles), columns) * (TileSelect.uiHeight+10) + 5, (int) (Math.ceil((numberOfTiles) / (float) columns)) * (TileSelect.uiHeight + 10) + 5));

    panel.setVisible(true);

    this.setViewportView(panel);
    this.setPreferredSize(new Dimension(Math.min((numberOfTiles), columns) * (TileSelect.uiHeight+10) + 10, (int) Math.min(Math.ceil((numberOfTiles) / (float) columns), 3) * (TileSelect.uiHeight + 10) + 10));

    this.repaint();
  }
  /**
   * Is used as an {@link ActionListener} for the Tiles.
   * Upon pressing the tile, this turns the currently selected image to the
   * preset according to the tile
   */
  class myListener implements ActionListener {
    private final int index;

    myListener(int i) {
      index = i;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      mainFrame.saveForUndo();
      if (mainFrame.getDrawPane().selectType == 2) {
        mainFrame.getDrawPane().fObjects.get(mainFrame.getDrawPane().fObjects.size() - 1).floatingImage.turnTo(FloatingImage.fIPresets.get(index));
        mainFrame.getEframe().editPane.upEditPane();
      } else if (mainFrame.getDrawPane().selectType == 4) {
        mainFrame.getDrawPane().multiEdit.finalize = false;
        int sel = mainFrame.tiles.selectedIndex;
        mainFrame.getDrawPane().multiEdit.turnTo(FloatingImage.fIPresets.get(index));
        mainFrame.tileTabs.update();
        if (index < FloatingImage.fIPresets.size() + LineConfig.presets.size() + Vertex.vertexPresets.size() && index >= 0)
          mainFrame.tiles.setSelected(sel);
        mainFrame.getEframe().editPane.upEditPane();
        mainFrame.getDrawPane().multiEdit.finalize = true;
      }
    }
  }
}
