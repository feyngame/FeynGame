//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import game.*;
import java.util.*;
import ui.*;
import game.Presets;

public class TileSelectTabbed extends JPanel {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 5L;
  /**
   * the mainFrame
   */
  private Frame mainFrame;
  /**
   * a list containing all used names for models
   */
  private ArrayList<String> usedNames = new ArrayList<>();
  /**
   * contains all the names that had to be changed
   * key: the changed name
   * value: the original name
   */
  private HashMap<String, String> changedNames = new HashMap<>();
  /**
   * the selected tab
   */
  private String selected;


  public TileSelectTabbed(Frame mainFrame) {

    this.setBackground(UIManager.getColor("Menu.background"));
    this.setOpaque(true);
    this.mainFrame = mainFrame;

    this.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

    this.setAlignmentX(LEFT_ALIGNMENT);

    this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));

    this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
  }
  /**
   * updates the tabs
   */
  public void update() {
    Component[] componentList = this.getComponents();

    for(Component c : componentList){
      this.remove(c);
    }

    NewTabButton nTab = new NewTabButton();
    nTab.addActionListener(new addListener());
    int height = (int) nTab.getPreferredSize().getHeight();

    for (Map.Entry<String, Presets> entry : mainFrame.models.entrySet()) {
      String title = entry.getKey();
      String titleShort = title.split(Pattern.quote(System.getProperty("file.separator")))[title.split(Pattern.quote(System.getProperty("file.separator"))).length - 1];
      titleShort = titleShort.substring(0, titleShort.length() - 6);

      TabButton button = new TabButton(titleShort, height);
      button.title = entry.getKey();
      button.addActionListener(new selectListener(entry.getKey()));
      button.close.addActionListener(new closeListener(entry.getKey()));
      this.add(button, LEFT_ALIGNMENT);
    }
    this.add(nTab, LEFT_ALIGNMENT);

    this.select(selected);
  }
  /**
   * removes the currently selected model
   */
  public void removeCurrent() {

    if (mainFrame.models.get(selected).unsaved) {

      this.select(selected);

      if (mainFrame.configPath == null || mainFrame.configPath.equals("")) {
        Object[] options = {"Save model", "Don't save"};
        int n = JOptionPane.showOptionDialog(
          this,
          "Do you want to save the model before closing",
          "Save model?",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 0) {
          mainFrame.saveAsConfig();
        }
      } else {
        Object[] options = {"Save model", "Don't save"};
        int n = JOptionPane.showOptionDialog(
          this,
          "Do you want to save the model before closing",
          "Save model?",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 0) {
          mainFrame.saveConfig();
        }
      }
    }

    mainFrame.models.remove(selected);
    usedNames.remove(selected);
    if (changedNames.containsKey(selected))
      changedNames.remove(selected);
  }
  /**
   * removes a model from the model list
   * @param key the name/key for the model to be removed
   */
  public void remove(String key) {

    if (mainFrame.models.get(key).unsaved) {
      String selected_key = selected;

      this.select(key);

      if (mainFrame.configPath == null || mainFrame.configPath.equals("")) {
        Object[] options = {"Save model", "Don't save"};
        int n = JOptionPane.showOptionDialog(
          this,
          "Do you want to save the model before closing",
          "Save model?",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 0) {
          mainFrame.saveAsConfig();
        }
      } else {
        Object[] options = {"Save model", "Don't save"};
        int n = JOptionPane.showOptionDialog(
          this,
          "Do you want to save the model before closing",
          "Save model?",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 0) {
          mainFrame.saveConfig();
        }
      }


      this.select(selected_key);
    }

    mainFrame.models.remove(key);
    usedNames.remove(key);
    if (changedNames.containsKey(key))
      changedNames.remove(key);

    if (selected.equals(key)) {
      if (mainFrame.models.entrySet().size() > 0) {
        for (Map.Entry<String, Presets> entry: mainFrame.models.entrySet()) {
          mainFrame.models.get(entry.getKey()).applyPresets();

          this.selected = entry.getKey();

          if (changedNames.containsKey(entry.getKey()))
            mainFrame.configPath = changedNames.get(entry.getKey());
          else
            mainFrame.configPath = entry.getKey();

          mainFrame.reloadTiles();

          this.updateSelected();
          break;
        }
      } else {
        mainFrame.loadConfig("");
        mainFrame.models.get(this.getCurrentKey()).allowUnsaved = false;
        mainFrame.models.get(this.getCurrentKey()).unsaved = false;
      }
    }
    this.update();
  }
  /**
   * saves the current opened model
   */
  public void saveCurrent() {
    if (!(selected == null || selected.equals("") || mainFrame.models.entrySet().size() < 1)) {
      boolean allowUnsaved = mainFrame.models.get(selected).allowUnsaved;
      boolean unsaved = mainFrame.models.get(selected).unsaved;

      mainFrame.models.remove(selected);
      mainFrame.models.put(selected, new Presets());

      mainFrame.models.get(selected).allowUnsaved = allowUnsaved;
      mainFrame.models.get(selected).unsaved = unsaved;
    }
  }
  /**
   * when a tab is pressed, the presets change
   * @param key the name/key of the model/preset to be selected
   */
  public void select(String key) {

    this.saveCurrent();

    mainFrame.models.get(key).applyPresets();

    this.selected = key;

    if (changedNames.containsKey(key))
      mainFrame.configPath = changedNames.get(key);
    else
      mainFrame.configPath = key;

    mainFrame.reloadTiles();

    this.updateSelected();
  }
  /**
   * updates which tabs should be represented as not active/active
   */
  public void updateSelected() {
    Component[] componentList = this.getComponents();

    for(Component c : componentList){
      if (c instanceof TabButton) {
        TabButton tab = (TabButton) c;
        tab.pressed = tab.title.equals(this.selected);
        tab.close.pressed = tab.title.equals(this.selected);
      }
    }
  }
  /**
   * Is used as an {@link ActionListener} for the {@link ui.TabButton}.
   */
  class selectListener implements ActionListener {
    private final String title;

    selectListener(String i) {
      title = i;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      select(title);
      Frame.frame.requestFocus();
    }
  }
  /**
   * Is used as an {@link ActionListener} for the close Button of the {@link ui.TabButton}.
   */
  class closeListener implements ActionListener {
    private final String title;

    closeListener(String i) {
      title = i;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      remove(title);
      Frame.frame.requestFocus();
    }
  }
  /**
   * Is used as an {@link ActionListener} for the {@link ui.NewTabButton} that lets one add a new model file.
   */
  class addListener implements ActionListener {

    addListener() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      mainFrame.loadNewConfig();
    }
  }
  /**
   * adds the Preset to the list of presets and assigns a name
   * @param preset the preset/model
   * @param name the name; if "" then changed to "unnamed" + number and new entry in this.changedNames is created; if already used name is altered and a new entry in this.changedNames is created
   */
  public void addModel(Presets preset, String name) {
    String changedName = name;
    if (name == null || name.equals("")) {
      changedName = "default.model";
    }
    if (!changedName.endsWith(".model"))
      changedName += ".model";
    int i = 1;
    String temp = changedName;
    temp = temp.substring(0, temp.length() - 6);
    while (usedNames.contains(changedName)) {
      changedName = temp + String.valueOf(i) + ".model";
      i++;
    }
    if (!changedName.equals(name)) {
      changedNames.put(changedName, name);
    }
    usedNames.add(changedName);
    mainFrame.models.put(changedName, preset);

    mainFrame.models.get(changedName).applyPresets();

    this.selected = changedName;

    if (changedNames.containsKey(changedName))
      mainFrame.configPath = changedNames.get(changedName);
    else
      mainFrame.configPath = changedName;

    mainFrame.reloadTiles();

    this.update();

    this.updateSelected();
  }
  /**
   * returns the key of the currently selected tab
   * @return see above
   */
  public String getCurrentKey() {
    return new String(selected);
  }
  /**
   * set the unsaved flag to true for a model
   */
  public void setCurrUnsaved() {
    if (mainFrame.models.get(selected).allowUnsaved) {
      mainFrame.models.get(selected).unsaved = true;
    }
  }

}
