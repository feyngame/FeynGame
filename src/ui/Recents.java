//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Recents extends JMenu {

  private Frame mF;

  public Recents(Frame mainFrame) {
    super();

    mF = mainFrame;

    this.setText("Open Recent");

    update();
  }

  public void addEntry(String entry) {
    if (Frame.recentEntries.contains(entry)) {
      int index = Frame.recentEntries.indexOf(entry);
      for (int i = index-1; i>=0; i--) {
        Frame.recentEntries.set(i+1,Frame.recentEntries.get(i));
      }
      Frame.recentEntries.set(0,entry);
    } else {
      Frame.recentEntries.add(0,entry);
      while (Frame.recentEntries.size()>12) {
        Frame.recentEntries.remove(Frame.recentEntries.size()-1);
      }
    }
    update();
  }

  public void update() {

    removeAll();
    
    for (String entry : Frame.recentEntries) {
      JMenuItem menuItem = new JMenuItem(entry);
      menuItem.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent ae) {
          mF.loadDiagram(entry, new Pane(mF.drawPane, true));
        }
      });
      add(menuItem);
    }
  }

}