//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Taskbar;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import game.JavaVectorGraphic;
import resources.GetResources;

/**
 * this class is a window that shows all the options for printing and upon
 * confirmation prints the canvas
 */
public class Print extends JFrame implements WindowListener {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 19L;
  private static final Insets WEST_INSETS = new Insets(5, 0, 5, 5);
  private static final Insets EAST_INSETS = new Insets(5, 5, 5, 0);
  /**
   * whether the boundingboxcalculator is shown
   */
  public static boolean bbcalc = false;
  /**
   * whether or not the b/w filter is used
   */
  public static boolean bw = false;
  /**
   * what type of boundaries to use
   * 0: automatic boundaries
   * 1: bounds are based on canvas size
   * 2: maximum boundaries
   */
  public static int bounds = 0;
  /**
   * the main frame of feyngame
   */
  Frame mainFrame;
  /**
   * radio button to select automatic calculation of boundaries
   */
  JRadioButton autoBounds;
  /**
   * radio button to select use of drawPane as boundaries
   */
  JRadioButton fullBounds;
  /**
   * radio button to select maximum boundaries
   */
  JRadioButton maxBounds;
  /**
   * radio button to select manual boundaries
   */
  JRadioButton mbbBounds;
  /**
   * checkbox for bbcalc
   */
  JCheckBox bbCalc;
  /**
   * whether to use the internal b/w filter
   */
  JCheckBox bwCB;
  /**
   * wether or not to show the automatic gnerated bounds on the canvas
   */
  public static boolean SHOW_AUTO_BOUNDS = false;
  private boolean cpMBB;
  /**
   * the constructor: initializes the subcomponents and lays them out
   * @param mainFrame the main Window
   */
  public Print(Frame mainFrame) {
    // this.setType(Window.Type.POPUP);
    this.mainFrame = mainFrame;
    this.setResizable(false);

    Print.SHOW_AUTO_BOUNDS = true;
    mainFrame.repaint();

    cpMBB = mainFrame.getDrawPane().mBB;

    /* look of the window */

    this.setTitle("Print Feynman diagram");

    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    this.addWindowListener(new java.awt.event.WindowAdapter() {
      @Override
      public void windowClosing(java.awt.event.WindowEvent windowEvent) {
        mainFrame.derefEframe();
        mainFrame.requestFocus();
      }
    });

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    this.setLayout(gridbag);

    c = createGbc(1, 1);

    /* set auto bounds on/off*/
    autoBounds = new JRadioButton("Restrict printing area to automatic bounding box");
    autoBounds.addActionListener(e -> {
       mainFrame.getDrawPane().mBB = mbbBounds.isSelected();
       Print.SHOW_AUTO_BOUNDS = autoBounds.isSelected();
       mainFrame.repaint();
    });
    Print.SHOW_AUTO_BOUNDS = (bounds==0);
    //mainFrame.repaint();
    autoBounds.setSelected(bounds == 0);
    autoBounds.setAlignmentX(LEFT_ALIGNMENT);
    this.add(autoBounds, c);

    c = createGbc(1, 2);

    c.gridy++;
    mbbBounds = new JRadioButton("Restrict printing area to manual bounding box");
    mbbBounds.addActionListener(e -> {
       mainFrame.getDrawPane().mBB = mbbBounds.isSelected();
       Print.SHOW_AUTO_BOUNDS = autoBounds.isSelected();
       mainFrame.repaint();
    });
    mbbBounds.setEnabled(true);
    if (mainFrame.getDrawPane().wMBB == 0 || mainFrame.getDrawPane().hMBB == 0) mbbBounds.setEnabled(false);
    mbbBounds.setSelected(bounds == 3);
    mbbBounds.setAlignmentX(LEFT_ALIGNMENT);
    mainFrame.getDrawPane().mBB = (bounds == 3);
    this.add(mbbBounds, c);

    c = createGbc(1, 3);

    c.gridy++;
    fullBounds = new JRadioButton("Print full canvas");
    fullBounds.addActionListener(e -> {
       mainFrame.getDrawPane().mBB = mbbBounds.isSelected();
       Print.SHOW_AUTO_BOUNDS = autoBounds.isSelected();
       mainFrame.repaint();
    });
    fullBounds.setSelected(bounds == 1);
    fullBounds.setAlignmentX(LEFT_ALIGNMENT);
    this.add(fullBounds, c);

    c = createGbc(1, 4);

    c.gridy++;
    maxBounds = new JRadioButton("Print on maximum size");
    maxBounds.addActionListener(e -> {
       mainFrame.getDrawPane().mBB = mbbBounds.isSelected();
       Print.SHOW_AUTO_BOUNDS = autoBounds.isSelected();
       mainFrame.repaint();
    });
    maxBounds.setSelected(bounds == 2);
    maxBounds.setAlignmentX(LEFT_ALIGNMENT);
    this.add(maxBounds, c);

    c = createGbc(1, 5);

    ButtonGroup group = new ButtonGroup();
    group.add(autoBounds);
    group.add(fullBounds);
    group.add(maxBounds);

    /* set the displaying of the viewbox/trimparamters after printing to
    * on/off */
    bbCalc = new JCheckBox("Display viewbox/trim parameters ");
    bbCalc.setSelected(false);
    bbCalc.setAlignmentX(LEFT_ALIGNMENT);
    bbCalc.setSelected(bbcalc);
    c.gridy++;
    this.add(bbCalc, c);

    c = createGbc(1, 6);

    /* set rudimentary b/w filter on/off */
    bwCB = new JCheckBox("Print in black/white");
    bwCB.setSelected(bw);
    bwCB.setAlignmentX(LEFT_ALIGNMENT);
    c.gridy++;
    this.add(bwCB, c);

    c = createGbc(1, 7);

    JPanel buttonPanel = new JPanel();

    /* confirm/print */
    JButton printButton = new JButton("Print / to PDF");
    printButton.addActionListener(e -> {

      this.setVisible(false);
      Print.SHOW_AUTO_BOUNDS = false;

      bbcalc = bbCalc.isSelected();
      bw = bwCB.isSelected();
      if (autoBounds.isSelected()) {
        bounds = 0;
        printAutoBounds();
      } else if (fullBounds.isSelected()) {
        bounds = 1;
        printFullBounds();
      } else if (maxBounds.isSelected()) {
        bounds = 2;
        printMaxBounds();
      } else if (mbbBounds.isSelected()) {
        bounds = 3;
        printManualBounds();
      }

      this.dispose();
    });
    buttonPanel.add(printButton);
    JButton closeButton = new JButton("Cancel");
    closeButton.addActionListener(e -> {
      this.dispose();
    });
    buttonPanel.add(closeButton);

    c.gridy++;
    this.add(buttonPanel, c);

    this.pack();
    this.setLocationRelativeTo(null);
    this.addWindowListener(this);
    this.setVisible(true);
  }
  /**
   * when the window is closed
   * @param e the window event
   */
  public void windowClosing(WindowEvent e) {
  }

  public void windowClosed(WindowEvent e) {
    Print.SHOW_AUTO_BOUNDS = false;
    mainFrame.getDrawPane().mBB = cpMBB;
    mainFrame.repaint();
  }

  public void windowOpened(WindowEvent e) {
  }

  public void windowIconified(WindowEvent e) {
  }

  public void windowDeiconified(WindowEvent e) {
  }

  public void windowActivated(WindowEvent e) {
  }

  public void windowDeactivated(WindowEvent e) {
  }
  /**
   * this function prints the canvas after cropping it to the automatic
   * boundaries
   */
  private void printAutoBounds() {
    JavaVectorGraphic.drawVector = true;
    Pane toBePrinted = new Pane(mainFrame.getDrawPane(), false);
    toBePrinted.grid = false;
    toBePrinted.helperLines = false;
    toBePrinted.showCurrentSelectedLine = false;
    if (bwCB.isSelected()) {
      toBePrinted.bw();
    }
    toBePrinted.crop(toBePrinted.getBoundsPainted());
    PrintUtilities.printComponent(toBePrinted, toBePrinted.getBoundsPainted(), false);
    JavaVectorGraphic.drawVector = false;
  }
  /**
   * this function prints the canvas after cropping it to the manual
   * boundaries
   */
  private void printManualBounds() {
    JavaVectorGraphic.drawVector = true;
    Pane toBePrinted = new Pane(mainFrame.getDrawPane(), false);
    toBePrinted.grid = false;
    toBePrinted.helperLines = false;
    toBePrinted.showCurrentSelectedLine = false;
    if (bwCB.isSelected()) {
      toBePrinted.bw();
    }
    toBePrinted.crop(new Rectangle((int) mainFrame.getDrawPane().xMBB, (int) mainFrame.getDrawPane().yMBB, (int) mainFrame.getDrawPane().wMBB, (int) mainFrame.getDrawPane().hMBB));
    PrintUtilities.printComponent(toBePrinted, toBePrinted.getBoundsPainted(), false);
    JavaVectorGraphic.drawVector = false;
  }
  /**
   * this method prints the full canvas
   */
  private void printFullBounds() {
    JavaVectorGraphic.drawVector = true;
    Pane toBePrinted = new Pane(mainFrame.getDrawPane(), false);
    toBePrinted.grid = false;
    toBePrinted.helperLines = false;
    toBePrinted.showCurrentSelectedLine = false;
    if (bwCB.isSelected()) {
      toBePrinted.bw();
    }
    Rectangle bounds = mainFrame.getDrawPane().getBounds();
    bounds.x /= mainFrame.getDrawPane().scale;
    bounds.y /= mainFrame.getDrawPane().scale;
    bounds.width /= mainFrame.getDrawPane().scale;
    bounds.height /= mainFrame.getDrawPane().scale;
    bounds.x -= mainFrame.getDrawPane().zeroPoint.x;
    bounds.y -= mainFrame.getDrawPane().zeroPoint.y;
    toBePrinted.crop(bounds);
    PrintUtilities.printComponent(toBePrinted, toBePrinted.getBoundsPainted(), true);
    JavaVectorGraphic.drawVector = false;
  }
  /**
   * this funtion prints the canvas after expanding it to a maximum canvas Size
   * boundaries
   */
  private void printMaxBounds() {
    JavaVectorGraphic.drawVector = true;
    Pane toBePrinted = new Pane(mainFrame.getDrawPane(), false);
    toBePrinted.grid = false;
    toBePrinted.helperLines = false;
    toBePrinted.showCurrentSelectedLine = false;
    if (bwCB.isSelected()) {
      toBePrinted.bw();
    }
    Rectangle bounds = toBePrinted.getBoundsPainted();

    double width = Frame.pdfExportDim.getWidth();
    if (width < bounds.width) {
      Object[] options = {"Keep width", "Print all of the diagram"};
        int n = JOptionPane.showOptionDialog(
          this,
          "The diagram is wider than the width of the chosen maximal output dimension. Should the diagram be cropped and therefore painted objects be lost or should the width of the output dimension be changed to fit the diagram?",
          "Diagram to wide for output dimension!",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.WARNING_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 1) {
          width = bounds.width;
        }
    }

    double height = Frame.pdfExportDim.getHeight();
    if (height < bounds.height) {
      Object[] options = {"Keep height", "Print all of the diagram"};
        int n = JOptionPane.showOptionDialog(
          this,
          "The diagram is taller than the height of the chosen maximal output dimension. Should the diagram be cropped and therefore painted objects be lost or should the height of the output dimension be changed to fit the diagram?",
          "Diagram to tall for output dimension!",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.WARNING_MESSAGE,
          null,
          options,
          options[0]);
        if (n == 1) {
          height = bounds.height;
        }
    }

    toBePrinted.crop(new Rectangle(Math.round(bounds.x), Math.round(bounds.y), (int) Math.round(width), (int) Math.round(height)));
    PrintUtilities.printComponent(toBePrinted, toBePrinted.getBoundsPainted(), true);
    JavaVectorGraphic.drawVector = false;
  }
  /**
   * automatic generation of the gridbagconstraints form the position (x, y)
   * @param x the x-coordinate
   * @param y the y-coordinate
   * @return the gridbagconstraints
   */
  private GridBagConstraints createGbc(int x, int y) {
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridx = x;
    gbc.gridy = y;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;

    gbc.anchor = (x == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
    gbc.fill = (x == 0) ? GridBagConstraints.BOTH
      : GridBagConstraints.HORIZONTAL;

    gbc.insets = (x == 0) ? WEST_INSETS : EAST_INSETS;
    gbc.weightx = (x == 0) ? 0.1 : 1.0;
    gbc.weighty = 1.0;
    return gbc;
  }

}
