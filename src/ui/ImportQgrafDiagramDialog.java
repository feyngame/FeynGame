package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import qgraf.LoadedQgrafStyle;
import qgraf.ModelData;
import qgraf.ProgressUpdate;
import qgraf.QgrafImportException;
import qgraf.QgrafImportManager;
import qgraf.QgrafOutputFile;

public class ImportQgrafDiagramDialog extends JDialog implements WindowListener, ProgressUpdate {
  private static final long serialVersionUID = -1484321695712431128L;
  private static final Logger LOGGER = Frame.getLogger(ImportQgrafDiagramDialog.class);
  
  private final JComboBox<LoadedQgrafStyle> styleChooser;
  private final JRadioButton singleDiagram;
  private final JRadioButton allDiagrams;
  private final JSpinner diagramIndex;
  private final JProgressBar bar;
  private final JLabel currentFileLabel;
  private final JButton importButton;
  private final JButton addStyle;
  private final JButton cancel;
  private final JCheckBox momenta;
  
  private Path selectedFile;
  private ModelData model;
  private QgrafOutputFile result;
  private Worker worker;
  private boolean showCancelMessage;
  
  private static final class WorkerUpdateContainer {
    private static enum Method {
      INIT, UPDATE, CANCEL;
    }
    
    private final long filePointer;
    private final int diagramCount;
    private final int pathCount;
    private final int peakPathCount;
    private final int lineCount;
    private final int peakLineCount;
    private final Method method;
    
    private WorkerUpdateContainer(long filePointer, int diagramCount, int pathCount, int peakPathCount, int lineCount, int peakLineCount) {
      this.filePointer = filePointer;
      this.diagramCount = diagramCount;
      this.pathCount = pathCount;
      this.peakPathCount = peakPathCount;
      this.lineCount = lineCount;
      this.peakLineCount = peakLineCount;
      this.method = Method.UPDATE;
    }
    
    private WorkerUpdateContainer(long filePointer) {
      this.filePointer = filePointer;
      this.diagramCount = 0;
      this.pathCount = 0;
      this.peakPathCount = 0;
      this.lineCount = 0;
      this.peakLineCount = 0;
      this.method = Method.INIT;
    }
    
    private WorkerUpdateContainer() {
      this.filePointer = 0;
      this.diagramCount = 0;
      this.pathCount = 0;
      this.peakPathCount = 0;
      this.lineCount = 0;
      this.peakLineCount = 0;
      this.method = Method.CANCEL;
    }
  }
  
  private final class Worker extends SwingWorker<QgrafOutputFile, WorkerUpdateContainer> implements ProgressUpdate {

    private final int selectedIndex;
    private final Path selectedFile;
    private final ModelData model;
    
    private Worker(int selectedIndex, Path selectedFile, ModelData model) {
      this.selectedIndex = selectedIndex;
      this.selectedFile = selectedFile;
      this.model = model;
    }
    
    @Override
    protected QgrafOutputFile doInBackground() throws Exception {
      final QgrafOutputFile r = QgrafOutputFile.fromFileParallel(selectedFile, getSelectedStyle().getStyle(),
          selectedIndex, Worker.this, model);
      if(r == null) throw new QgrafImportException("Invalid diagram index (not in file)");
      return r;
    }

    @Override
    protected void process(List<WorkerUpdateContainer> chunks) {
      if(chunks.size() >= 2 && chunks.get(0).method == WorkerUpdateContainer.Method.INIT)
      process(chunks.get(0));
      
      if(chunks.isEmpty()) return;
      WorkerUpdateContainer mostRecent = chunks.get(chunks.size() - 1);
      process(mostRecent);
    }
    
    private void process(WorkerUpdateContainer wuc) {
      switch (wuc.method) {
      case INIT:
        ImportQgrafDiagramDialog.this.init(wuc.filePointer);
        break;
      case UPDATE:
        ImportQgrafDiagramDialog.this.update(wuc.filePointer, wuc.diagramCount, wuc.pathCount,
            wuc.peakPathCount, wuc.lineCount, wuc.peakLineCount);
        break;
      case CANCEL:
        ImportQgrafDiagramDialog.this.cancel();
        break;
      }
    }
    
    @Override
    protected void done() {
      try {
        QgrafOutputFile result = get();
        ImportQgrafDiagramDialog.this.closeDialog(result);
      } catch (InterruptedException e) {
        ImportQgrafDiagramDialog.this.cancel();
      } catch (ExecutionException e) {
        Throwable t = e.getCause();
        ImportQgrafDiagramDialog.this.cancel();
        if(t instanceof QgrafImportException) {
          JOptionPane.showMessageDialog(ImportQgrafDiagramDialog.this,
              "Error: invalid style file or output file", "Error", JOptionPane.ERROR_MESSAGE);
        } else if(t instanceof IOException) {
          JOptionPane.showMessageDialog(ImportQgrafDiagramDialog.this,
              "Error: cannot open diagram file", "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    }

    @Override
    public void init(long fileLength) {
      publish(new WorkerUpdateContainer(fileLength));
    }

    @Override
    public void update(long filePointer, int diagramCount, int pathCount, int peakPathCount, int lineCount, int peakLineCount) {
      publish(new WorkerUpdateContainer(filePointer, diagramCount, pathCount, peakPathCount, lineCount, peakLineCount));
    }

    @Override
    public void cancel() {
      publish(new WorkerUpdateContainer());
    }
    
    @Override
    public void acceptLogMessage(String message) {
      //This is displayed in the UI, in slightly less detail, so just log it at finer log levels
      LOGGER.fine(message);
    }
  }
  
  public ImportQgrafDiagramDialog(Frame owner) {
    super(owner, "Import qgraf diagram", true);
    this.styleChooser = new JComboBox<>(); //default model is mutable
    this.styleChooser.setEditable(false);
    this.styleChooser.setMinimumSize(new Dimension(200, styleChooser.getMinimumSize().height));
    
    addStyle = new JButton("Add style");
    addStyle.addActionListener(e -> {
      addQgrafStyle();
    });
    
    importButton = new JButton("Import");
    importButton.addActionListener(e -> {
      doImport();
    });
    importButton.setMinimumSize(new Dimension(100, styleChooser.getMinimumSize().height));
    
    ButtonGroup group = new ButtonGroup();
    allDiagrams = new JRadioButton("Load all diagrams", true);
    singleDiagram = new JRadioButton("Load single diagram", false);
    group.add(allDiagrams);
    group.add(singleDiagram);
    
    diagramIndex = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
    diagramIndex.setMinimumSize(new Dimension(150, diagramIndex.getMinimumSize().height));
    diagramIndex.setEnabled(false);
    singleDiagram.addActionListener(e -> {
      diagramIndex.setEnabled(singleDiagram.isSelected());
    });
    allDiagrams.addActionListener(e -> {
      diagramIndex.setEnabled(singleDiagram.isSelected());
    });
    
    momenta = new JCheckBox("Import momentum arrows (if the style supports it)", QgrafImportManager.loadMomentumArrows());
    momenta.addActionListener(e -> QgrafImportManager.loadMomentumArrows(momenta.isSelected()));
    
    bar = new JProgressBar();
    bar.setMinimumSize(new Dimension(bar.getMinimumSize().width, 30));
    bar.setStringPainted(true);
    bar.setString("");
    
    currentFileLabel = new JLabel("Current file: N/A");
    JLabel indexLabel = new JLabel("Diagram Index: ");
    indexLabel.setHorizontalAlignment(SwingConstants.RIGHT);
//    statusLabel = new JLabel(" "); //Space because otherwise preferred height is 0 ?
//    statusLabel.setMinimumSize(new Dimension(150, statusLabel.getMinimumSize().height));
    
    cancel = new JButton("Cancel");
    cancel.addActionListener(e -> {
      cancelParallel();
      this.result = null;
      this.showCancelMessage = true;
      this.setVisible(false);
    });
    
    JPanel indexWithLabel = new JPanel(new BorderLayout());
    indexWithLabel.add(indexLabel, BorderLayout.LINE_START);
    indexWithLabel.add(diagramIndex, BorderLayout.CENTER);
    
    this.setLayout(new GridBagLayout());
    this.add(styleChooser, gbc(0, 1, 2, GridBagConstraints.HORIZONTAL));
    this.add(addStyle, gbc(2, 1, 1, GridBagConstraints.HORIZONTAL));
    this.add(singleDiagram, gbc(1, 2, 1, GridBagConstraints.HORIZONTAL));
    this.add(allDiagrams, gbc(0, 2, 1, GridBagConstraints.HORIZONTAL));
    this.add(momenta, gbc(0, 4, 3, GridBagConstraints.HORIZONTAL));
//    this.add(diagramIndex, gbc(1, 2, 1, GridBagConstraints.HORIZONTAL));
    this.add(importButton, gbc(1, 6, 1, GridBagConstraints.HORIZONTAL));
    this.add(bar, gbc(0, 5, 2, GridBagConstraints.HORIZONTAL));
//    this.add(changeFile, gbc(2, 0, 1, GridBagConstraints.HORIZONTAL));
    this.add(currentFileLabel, gbc(0, 0, 2, GridBagConstraints.HORIZONTAL));
//    this.add(indexLabel, gbc(0, 2, 1, GridBagConstraints.HORIZONTAL));
//    this.add(statusLabel, gbc(0, 5, 1, GridBagConstraints.HORIZONTAL));
    this.add(cancel, gbc(2, 6, 1, GridBagConstraints.HORIZONTAL));
    this.add(indexWithLabel, gbc(0, 3, 2, GridBagConstraints.HORIZONTAL));
    
    this.setResizable(false);
    this.getRootPane().setDefaultButton(importButton);
    if(diagramIndex.isEnabled()) diagramIndex.requestFocusInWindow();
    this.addWindowListener(this);
    this.pack();
    this.setLocationRelativeTo(owner);
  }
  
  private static GridBagConstraints gbc(int col, int row, int width, int fill) {
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridx = col;
    gbc.gridy = row;
    gbc.gridheight = 1;
    gbc.gridwidth = width;
    gbc.fill = fill;
    gbc.insets = new Insets(5, 5, 5, 5);
    return gbc;
  }
  
  private void closeDialog(QgrafOutputFile result) {
    if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException(); //otherwise mutables have to be volatile
    setInteractiveComponentsEnabled(true);
    this.model = null;
    this.selectedFile = null;
    this.worker = null;
    this.result = Objects.requireNonNull(result);
    setVisible(false); //returns from showDialog
  }
  
  public boolean showCancelMessage() {
    return showCancelMessage;
  }
  
  public void setSelectedStyle(LoadedQgrafStyle style) {
    updateAvailableStyles();
    if(style != null) styleChooser.setSelectedItem(style);
  }
  
  public LoadedQgrafStyle getSelectedStyle() {
    return (LoadedQgrafStyle)styleChooser.getSelectedItem();
  }
  
  public void setImportAll(boolean importAll) {
    allDiagrams.setSelected(importAll);
    singleDiagram.setSelected(!importAll);
    diagramIndex.setEnabled(!importAll);
  }
  
  public boolean getImportAll() {
    return allDiagrams.isSelected();
  }
  
  public int getSelectedDiagramIndex() {
    if(getImportAll()) {
      return QgrafOutputFile.LOAD_ALL_INDEX_VALUE;
    } else {
      return (int)diagramIndex.getValue() - 1;
    }
  }
  
  public void setSelectedDiagramIndex(int index) {
    diagramIndex.setValue(index + 1);
  }
  
  public QgrafOutputFile showDialog(Path path, ModelData model) {
    if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException();
    updateAvailableStyles();
    setInteractiveComponentsEnabled(true);
    bar.setValue(0);
    bar.setString(" ");
    
    if(diagramIndex.isEnabled()) diagramIndex.requestFocusInWindow();
    this.model = model;
    this.selectedFile = path;
    this.showCancelMessage = false;
    currentFileLabel.setText(path.toAbsolutePath().toString());
    pack();
    setVisible(true); //Blocking, until the modal dialog is closed
    return this.result;
  }
  
  private void updateAvailableStyles() {
    LoadedQgrafStyle last = getSelectedStyle();
    styleChooser.removeAllItems();
    for(LoadedQgrafStyle lqs : QgrafImportManager.getQgrafStyles()) {
      styleChooser.addItem(lqs);
    }
    if(last != null) styleChooser.setSelectedItem(last);
  }
  
  private void doImport() {
    if(styleChooser.getSelectedItem() == null) {
      JOptionPane.showMessageDialog(this, "No style selected", "Error", JOptionPane.ERROR_MESSAGE);
    } else {
      final int di;
      di = getSelectedDiagramIndex();
      if(di != QgrafOutputFile.LOAD_ALL_INDEX_VALUE && di < 0) {
        JOptionPane.showMessageDialog(this, "Index must be positive", "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
      startParallelImportImpl(di);
    }
  }
  
  private void setInteractiveComponentsEnabled(boolean value) {
    momenta.setEnabled(value);
    momenta.setSelected(QgrafImportManager.loadMomentumArrows());
    importButton.setEnabled(value);
    //  cancel.setEnabled(value); //cancel is never disabled 
    addStyle.setEnabled(value);
    diagramIndex.setEnabled(value && singleDiagram.isSelected());
    singleDiagram.setEnabled(value);
    allDiagrams.setEnabled(value);
    styleChooser.setEnabled(value);
  }
  
  private void startParallelImportImpl(int selectedIndex) {
    if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException();
    setInteractiveComponentsEnabled(false);
    
    worker = new Worker(selectedIndex, selectedFile, model);
    worker.execute();
  }
  
  private void cancelParallel() {
    if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException();
    
    if(worker != null) {
      worker.cancel(true);
    }
  }
  
  private void updateComboBox(LoadedQgrafStyle toSelect) {
    styleChooser.removeAllItems();
    
    for(LoadedQgrafStyle style : QgrafImportManager.getQgrafStyles()) {
      styleChooser.addItem(style);
    }
    if(toSelect != null) styleChooser.setSelectedItem(toSelect);
  }

  private void addQgrafStyle() {
    LoadedQgrafStyle l = QgrafImportManager.doGuiStyleLoad(this, false);
    updateComboBox(l);
  }

  @Override
  public void windowClosing(WindowEvent e) {
    cancelParallel();
  }
  @Override
  public void windowOpened(WindowEvent e) {}
  @Override
  public void windowClosed(WindowEvent e) {}
  @Override
  public void windowIconified(WindowEvent e) {}
  @Override
  public void windowDeiconified(WindowEvent e) {}
  @Override
  public void windowActivated(WindowEvent e) {}
  @Override
  public void windowDeactivated(WindowEvent e) {}

  @Override
  public void acceptLogMessage(String message) {
    LOGGER.fine(message);
  }

  @Override
  public void init(long fileLength) {
    bar.setMinimum(0);
    bar.setMaximum((int) fileLength);
  }

  @Override
  public void update(long filePointer, int diagramCount, int pathCount, int peakPathCount, int lineCount, int peakLineCount) {
    LOGGER.fine("Diagrams: " + diagramCount + "; P:" + pathCount + "/" + peakPathCount + "; L:" + lineCount + "/" + peakLineCount);
    bar.setString("Current diagram count: " + diagramCount);
    bar.setValue((int) filePointer);
  }
  
  @Override
  public void cancel() {
    setInteractiveComponentsEnabled(true);
    bar.setValue(0);
    bar.setString(" ");
  }
  
}
