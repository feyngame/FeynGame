//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.*;

import org.scilab.forge.jlatexmath.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Objects;
/**
 * the panel that holds all the components for editing the momentum arrow
 *
 * @author Sven Yannick Klein
 */
public class MomArrowConf extends JPanel {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 15L;
  /**
   * the main panel
   */
  Frame mainFrame;
  /**
   * toggle drawing of Momentum Arrow#
   */
  JCheckBox showMomentumBox;
  /**
   * the component holding everything to configure the momentum arrow
   */
  JPanel momConf;
  /**
   * the labels for the different JSpinners
   */
  JLabel lengthLabel;
  JLabel strokeLabel;
  JLabel positionLabel;
  JLabel distanceLabel;
  JLabel tipSizeLabel;
  JLabel tipHeightLabel;
  JLabel tipDentLabel;
  JLabel descDisLabel;
  JLabel partPlaceDescLabel;
  JLabel rotDescLabel;
  JLabel descScaleLabel;
  /**
   * the different numbermodels for the JSpinners
   */
  SpinnerNumberModel lengthModel;
  SpinnerNumberModel strokeModel;
  SpinnerNumberModel positionModel;
  SpinnerNumberModel distanceModel;
  SpinnerNumberModel tipSizeModel;
  SpinnerNumberModel tipHeightModel;
  SpinnerNumberModel tipDentModel;
  SpinnerNumberModel descDisModel;
  SpinnerNumberModel partPlaceDescModel;
  SpinnerNumberModel rotDescModel;
  SpinnerNumberModel descScaleModel;
  SpinnerNumberModel dashLengthModel;
  /**
   * the JSpinners
   */
  JSpinner lengthSpinner;
  JSpinner strokeSpinner;
  JSpinner positionSpinner;
  JSpinner distanceSpinner;
  JSpinner tipSizeSpinner;
  JSpinner tipHeightSpinner;
  JSpinner tipDentSpinner;
  JSpinner descDisSpinner;
  JSpinner partPlaceDescSpinner;
  JSpinner rotDescSpinner;
  JSpinner descScaleSpinner;
  JSpinner dashLengthSpinner;
  /**
   * the checkboxes
   */
  JCheckBox lineColorBox;
  JCheckBox invertBox;
  JCheckBox showDescBox;
  JCheckBox doubleLineBox;
  JCheckBox dashedBox;
  /**
   * the buttons
   */
  JButton colorButton;
  JButton defaultButton;
  /**
   * the currently selected line
   */
  FeynLine line;
  /**
   * whether or not {@link update} member function is running
   */
  boolean updating;
  /**
   * the textfields
   */
  JTextField descriptionField;
  /**
   * the constructor of the panel: initializes the subcomponents and lays
   * them out
   * @param mainFrame a reference to the main Window
   */
  public MomArrowConf(Frame mainFrame) {
    /* basic look*/
    this.mainFrame = mainFrame;

    this.setLayout(new BorderLayout());

    showMomentumBox = new JCheckBox(" Show the momentum arrow");
    showMomentumBox.addItemListener(e -> {
      if (!updating) {
        mainFrame.saveForUndo();
        if (line.isMom()) {
          line.hideMom();
        } else {
          line.showMom();
        }
      }
      this.update();
      if (mainFrame.getEframe() != null) {
        mainFrame.getEframe().repaint();
        mainFrame.getEframe().pack();
      }
    });
    this.add(showMomentumBox, BorderLayout.NORTH);

    /* the components for editing the momentum arrow itself (and not its
     * label)*/

    momConf = new JPanel(new BorderLayout());
    JPanel noDefButtonPanel = new JPanel();
    noDefButtonPanel.setLayout(new GridLayout(4, 2));

    JPanel lengthPanel = new JPanel(new GridLayout());
    lengthLabel = new JLabel(" Length");
    lengthModel = new SpinnerNumberModel(10, 0, 10000, 1.0);
    lengthSpinner = new JSpinner(lengthModel);
    lengthSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(lengthSpinner.getValue().toString());
        if (value != line.momArrow.length)
          mainFrame.saveForUndo();

        line.momArrow.length = value;
      }
    });
    lengthPanel.add(lengthLabel);
    lengthPanel.add(lengthSpinner);
    lengthPanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(lengthPanel);

    JPanel strokePanel = new JPanel(new GridLayout());
    strokeLabel = new JLabel(" Stroke size");
    strokeModel = new SpinnerNumberModel(2, 0, 10000, 1.0);
    strokeSpinner = new JSpinner(strokeModel);
    strokeSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(strokeSpinner.getValue().toString());
        if (value != line.momArrow.stroke)
          mainFrame.saveForUndo();

        line.momArrow.stroke = value;
      }
    });
    strokePanel.add(strokeLabel);
    strokePanel.add(strokeSpinner);
    strokePanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(strokePanel);

    JPanel positionPanel = new JPanel(new GridLayout());
    positionLabel = new JLabel(" Position");
    positionModel = new SpinnerNumberModel(.5, -10000, 10000, .05);
    positionSpinner = new JSpinner(positionModel);
    positionSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        double value = Double.parseDouble(positionSpinner.getValue().toString());
        if (value != line.momArrow.position)
          mainFrame.saveForUndo();

        line.momArrow.position = value;
      }
    });
    positionPanel.add(positionLabel);
    positionPanel.add(positionSpinner);
    positionPanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(positionPanel);

    JPanel distancePanel = new JPanel(new GridLayout());
    distanceLabel = new JLabel(" Distance");
    distanceModel = new SpinnerNumberModel(10, -10000, 10000, 1.0);
    distanceSpinner = new JSpinner(distanceModel);
    distanceSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(distanceSpinner.getValue().toString());
        if (value != line.momArrow.stroke)
          mainFrame.saveForUndo();

        line.momArrow.distance = value;
      }
    });
    distancePanel.add(distanceLabel);
    distancePanel.add(distanceSpinner);
    distancePanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(distancePanel);

    lineColorBox = new JCheckBox(" Use color of line");
    lineColorBox.addItemListener(e -> {
      if (!updating) {
        line.momArrow.lineColor = !line.momArrow.lineColor;
        if (line.momArrow.lineColor) {
          colorButton.setEnabled(false);
        } else {
          colorButton.setEnabled(true);
        }
      }
    });
    lineColorBox.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(lineColorBox);

    colorButton = new JButton("Color");
    colorButton.addActionListener(e -> {
      if (!updating) {
        Color newColor = JColorChooser.showDialog(mainFrame.getEframe(), "Choose color", line.momArrow.color);

        if(!newColor.equals(line.momArrow.color))
          mainFrame.saveForUndo();

        line.momArrow.color = newColor;
      }
    });
    colorButton.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(colorButton);

    JPanel tipSizePanel = new JPanel(new GridLayout());
    tipSizeLabel = new JLabel(" Size of tip");
    tipSizeModel = new SpinnerNumberModel(10, 1, 10000, 1.0);
    tipSizeSpinner = new JSpinner(tipSizeModel);
    tipSizeSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(tipSizeSpinner.getValue().toString());
        if (value != line.momArrow.stroke)
          mainFrame.saveForUndo();

        line.momArrow.tipSize = value;
      }
    });
    tipSizePanel.add(tipSizeLabel);
    tipSizePanel.add(tipSizeSpinner);
    tipSizePanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(tipSizePanel);

    JPanel tipDentPanel = new JPanel(new GridLayout());
    tipDentLabel = new JLabel(" Dent of tip");
    tipDentModel = new SpinnerNumberModel(10, -10000, 10000, 1.0);
    tipDentSpinner = new JSpinner(tipDentModel);
    tipDentSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(tipDentSpinner.getValue().toString());
        if (value != line.momArrow.stroke)
          mainFrame.saveForUndo();

        line.momArrow.tipDent = value;
      }
    });
    tipDentPanel.add(tipDentLabel);
    tipDentPanel.add(tipDentSpinner);
    tipDentPanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(tipDentPanel);

    JPanel tipHeightPanel = new JPanel(new GridLayout());
    tipHeightLabel = new JLabel(" Height of tip");
    tipHeightModel = new SpinnerNumberModel(10, 1, 10000, 1.0);
    tipHeightSpinner = new JSpinner(tipHeightModel);
    tipHeightSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(tipHeightSpinner.getValue().toString());
        if (value != line.momArrow.stroke)
          mainFrame.saveForUndo();

        line.momArrow.tipHeight = value;
      }
    });
    tipHeightPanel.add(tipHeightLabel);
    tipHeightPanel.add(tipHeightSpinner);
    tipHeightPanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(tipHeightPanel);

    JPanel dashedPanel = new JPanel(new GridLayout());
    dashedBox = new JCheckBox(" Dashed: ");
    dashedBox.addItemListener(e -> {
      if (!updating) {
        mainFrame.saveForUndo();
        line.momArrow.dashed = !line.momArrow.dashed;
        dashLengthSpinner.setEnabled(line.momArrow.dashed);
      }
    });
    dashLengthModel = new SpinnerNumberModel(10, 1, 10000, 1.0);
    dashLengthSpinner = new JSpinner(dashLengthModel);
    dashLengthSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(dashLengthSpinner.getValue().toString());
        if (value != line.momArrow.dashLength)
          mainFrame.saveForUndo();

        line.momArrow.dashLength = Math.round(value);
      }
    });
    dashedPanel.add(dashedBox);
    dashedPanel.add(dashLengthSpinner);
    dashedPanel.setAlignmentX(LEFT_ALIGNMENT);
    noDefButtonPanel.add(dashedPanel);

    invertBox = new JCheckBox(" Invert");
    invertBox.addItemListener(e -> {
      if (!updating) {
        mainFrame.saveForUndo();
        line.momArrow.invert = !line.momArrow.invert;
      }
    });
    noDefButtonPanel.add(invertBox);

    doubleLineBox = new JCheckBox(" Double line");
    doubleLineBox.addItemListener(e -> {
      if (!updating) {
        mainFrame.saveForUndo();
        line.momArrow.doubleLine = !line.momArrow.doubleLine;

        if (line.momArrow.doubleLine) {
          line.momArrow.stroke *= 3;
        } else {
          double newStroke = (double) line.momArrow.stroke;
          newStroke *= 1d/3d;
          line.momArrow.stroke = (int) Math.round(newStroke);
        }
        update();
      }
    });
    noDefButtonPanel.add(doubleLineBox);

    /* set the config of the current momentum arrow to be the default */

    defaultButton = new JButton("Set as default for momentum arrow for this line type");
    defaultButton.addActionListener(e -> {
      if (!updating) {
        line.setDefMom();
      }
    });

    noDefButtonPanel.setAlignmentX(LEFT_ALIGNMENT);
    momConf.add(noDefButtonPanel, BorderLayout.NORTH);
    defaultButton.setAlignmentX(LEFT_ALIGNMENT);

    /* the components concerning the label of the momentum arrow */

    JPanel descPanel = new JPanel();

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    descPanel.setLayout(gridbag);
    c.weightx = .001;
    c.weighty = 1;
    c.gridy = 0;
    c.gridx = 0;
    c.insets = new Insets(0, 0, 0, 0);

    showDescBox = new JCheckBox(" Show label: ");
    showDescBox.addItemListener(e -> {
      if (!updating) {
        line.momArrow.showDesc = !line.momArrow.showDesc;
        this.update();
      }
    });
    descPanel.add(showDescBox, c);

    c.fill = (GridBagConstraints.HORIZONTAL);
    c.gridx++;
    c.weightx = 1;
    descriptionField = new JTextField();
    descriptionField.setHorizontalAlignment(JTextField.LEADING);
    descriptionField.setColumns(1);
    descriptionField.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {

      }

      @Override
      public void focusLost(FocusEvent e) {
        for (ActionListener al : descriptionField.getActionListeners())
          al.actionPerformed(null);
      }
    });
    descriptionField.addActionListener(e -> {
      String text = new String(descriptionField.getText());
      if (!text.contains("<html>")) {
        try {
          TeXFormula formula = new TeXFormula(text);
        } catch (org.scilab.forge.jlatexmath.ParseException ex) {
          Object[] options = {"Show error message", "Cancel"};
          int n = JOptionPane.showOptionDialog(
            this,
            "The string could not be processed correctly",
            "LaTeX error",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.ERROR_MESSAGE,
            null,
            options,
            options[0]
          );
          if (n == 0) {
            JOptionPane.showMessageDialog(this,
              ex.getMessage(),
              "LaTeX error message",
              JOptionPane.ERROR_MESSAGE
            );
          }
          update();
          return;
        }
      }
      if (!updating && text != null) {
        line.momArrow.description = text;
        mainFrame.repaint();
      }
    });
    descPanel.add(descriptionField, c);
    descPanel.setAlignmentX(LEFT_ALIGNMENT);

    momConf.add(descPanel, BorderLayout.CENTER);

    JPanel descConf = new JPanel(new GridLayout(2, 2));

    JPanel descDisPanel = new JPanel(new GridLayout());
    descDisLabel = new JLabel(" Distance");
    descDisModel = new SpinnerNumberModel(10, -10000, 10000, 1.0);
    descDisSpinner = new JSpinner(descDisModel);
    descDisSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        int value = (int) Double.parseDouble(descDisSpinner.getValue().toString());
        if (value != line.momArrow.descDis)
          mainFrame.saveForUndo();

        line.momArrow.descDis = value;
      }
    });
    descDisPanel.add(descDisLabel);
    descDisPanel.add(descDisSpinner);
    descDisPanel.setAlignmentX(LEFT_ALIGNMENT);
    descConf.add(descDisPanel);

    JPanel partPlaceDescPanel = new JPanel(new GridLayout());
    partPlaceDescLabel = new JLabel(" Position");
    partPlaceDescModel = new SpinnerNumberModel(.5, -10000, 10000, .05);
    partPlaceDescSpinner = new JSpinner(partPlaceDescModel);
    partPlaceDescSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        double value = Double.parseDouble(partPlaceDescSpinner.getValue().toString());
        if (value != line.momArrow.partPlaceDesc)
          mainFrame.saveForUndo();

        line.momArrow.partPlaceDesc = value;
      }
    });
    partPlaceDescPanel.add(partPlaceDescLabel);
    partPlaceDescPanel.add(partPlaceDescSpinner);
    partPlaceDescPanel.setAlignmentX(LEFT_ALIGNMENT);
    descConf.add(partPlaceDescPanel);

    JPanel rotDescPanel = new JPanel(new GridLayout());
    rotDescLabel = new JLabel(" Rotation");
    rotDescModel = new SpinnerNumberModel(0, -10000, 10000, 1.0);
    rotDescSpinner = new JSpinner(rotDescModel);
    rotDescSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        double value = Double.parseDouble(rotDescSpinner.getValue().toString());
        if (value != line.momArrow.rotDesc)
          mainFrame.saveForUndo();

        line.momArrow.rotDesc = Math.PI / 180 * value;
      }
    });
    rotDescPanel.add(rotDescLabel);
    rotDescPanel.add(rotDescSpinner);
    rotDescPanel.setAlignmentX(LEFT_ALIGNMENT);
    descConf.add(rotDescPanel);

    JPanel descScalePanel = new JPanel(new GridLayout());
    descScaleLabel = new JLabel(" Scale");
    descScaleModel = new SpinnerNumberModel(1, 0, 10000, .05);
    descScaleSpinner = new JSpinner(descScaleModel);
    descScaleSpinner.addChangeListener((ChangeEvent e) -> {
      if (!updating) {
        double value = Double.parseDouble(descScaleSpinner.getValue().toString());
        if (value != line.momArrow.descScale)
          mainFrame.saveForUndo();

        line.momArrow.descScale = value;
      }
    });
    descScalePanel.add(descScaleLabel);
    descScalePanel.add(descScaleSpinner);
    descScalePanel.setAlignmentX(LEFT_ALIGNMENT);
    descConf.add(descScalePanel);

    momConf.add(descConf, BorderLayout.SOUTH);

    this.add(momConf, BorderLayout.CENTER);

    this.add(defaultButton, BorderLayout.SOUTH);
  }
  /**
   * updates the values of the components to fit the current momentum arrow
   */
  public void update() {
    /* like in {@link ui.EditPane} we dont want to trigger the
     * action listeners for the subcomponents because of this function */
    updating = true;

    /* if no line is present set this component invisible */
    if (mainFrame.getDrawPane().lines.size() == 0) {
      this.setVisible(false);
      updating = false;
      return;
    }
    this.setVisible(true);
    this.line = mainFrame.getDrawPane().lines.get(mainFrame.getDrawPane().lines.size() - 1);

    if (line.isMom()) {
      showMomentumBox.setSelected(true);
      momConf.setVisible(true);

      lengthSpinner.setValue((double) line.momArrow.length);
      strokeSpinner.setValue((double) line.momArrow.stroke);
      positionSpinner.setValue(line.momArrow.position);
      distanceSpinner.setValue((double) line.momArrow.distance);
      tipSizeSpinner.setValue((double) line.momArrow.tipSize);
      tipHeightSpinner.setValue((double) line.momArrow.tipHeight);
      tipDentSpinner.setValue((double) line.momArrow.tipDent);
      descDisSpinner.setValue((double) line.momArrow.descDis);
      partPlaceDescSpinner.setValue(line.momArrow.partPlaceDesc);
      rotDescSpinner.setValue((double) 180 / Math.PI * line.momArrow.rotDesc);
      descScaleSpinner.setValue(line.momArrow.descScale);
      dashLengthSpinner.setValue((double) line.momArrow.dashLength);

      invertBox.setSelected(line.momArrow.invert);
      lineColorBox.setSelected(line.momArrow.lineColor);
      showDescBox.setSelected(line.momArrow.showDesc);
      dashedBox.setSelected(line.momArrow.dashed);
      doubleLineBox.setSelected(line.momArrow.doubleLine);

      descDisSpinner.setEnabled(line.momArrow.showDesc);
      partPlaceDescSpinner.setEnabled(line.momArrow.showDesc);
      rotDescSpinner.setEnabled(line.momArrow.showDesc);
      descScaleSpinner.setEnabled(line.momArrow.showDesc);
      dashLengthSpinner.setEnabled(line.momArrow.dashed);

      try {
        descriptionField.setText(new String(line.momArrow.description));
      } catch (NullPointerException ex) {
        descriptionField.setText("");
      }
      descriptionField.setEnabled(line.momArrow.showDesc);

      if (line.momArrow.lineColor)
        colorButton.setEnabled(false);
      else
        colorButton.setEnabled(true);

    } else {
      /* if there is no momentum arrow on the current line, only show the
       * component to show the momentum arrow and hide everything else */
      showMomentumBox.setSelected(false);
      momConf.setVisible(false);
    }

    updating = false;
  }
}
