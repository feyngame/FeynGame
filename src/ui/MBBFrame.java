//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2022 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Taskbar;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;

import resources.GetResources;

/**
 * This class is the ui for the export of the canvas to different formats (PDF)
 * @author Sven Yannick Klein
 */
public class MBBFrame extends JFrame implements WindowListener {
  private static final long serialVersionUID = 1L;
  
  private final Frame frame;
  private final JSpinner spinnerx, spinnery, spinnerw, spinnerh;
  private final JCheckBox enabled;
  private final JButton reset;
  
  public MBBFrame(Frame mainFrame, boolean alwaysOnTop) {
    super();
    this.frame = mainFrame;
    this.setAlwaysOnTop(alwaysOnTop);
    this.setResizable(false);
    this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

    /* look of the window */

    this.setTitle("Manual Bounding Box");

    this.setIconImage(GetResources.getAppIcon());
    try {
        final Taskbar taskbar = Taskbar.getTaskbar();
        taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    this.setLayout(gridbag);

    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    c.insets = new Insets(2,2,2,2);

    enabled = new JCheckBox("Show Bounding Box");
    enabled.setSelected(frame.getDrawPane().mBB);
    this.add(enabled, c);

    c.gridx = 2;
    reset = new JButton("Reset");
    this.add(reset, c);
    
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    JLabel labelx = new JLabel("X: ");
    this.add(labelx, c);

    c.gridx ++;
    spinnerx = new JSpinner();
    spinnerx.addChangeListener(e -> {
      if(skipChangeEvent) return;
      frame.getDrawPane().xMBB = (double) Float.parseFloat(spinnerx.getValue().toString());
      frame.repaint();
    });
    spinnerx.setEnabled(frame.getDrawPane().mBB);
    ((JSpinner.DefaultEditor) spinnerx.getEditor()).getTextField().setColumns(6);
    this.add(spinnerx, c);


    c.gridx ++;
    JLabel labely = new JLabel("Y: ");
    this.add(labely, c);

    c.gridx ++;
    spinnery = new JSpinner();
    spinnery.addChangeListener(e -> {
      if(skipChangeEvent) return;
      frame.getDrawPane().yMBB = (double) Float.parseFloat(spinnery.getValue().toString());
      frame.repaint();
    });
    spinnery.setEnabled(frame.getDrawPane().mBB);
    ((JSpinner.DefaultEditor) spinnery.getEditor()).getTextField().setColumns(6);
    this.add(spinnery, c);

    c.gridy = 2;
    c.gridx = 0;
    c.gridwidth = 1;
    JLabel labelw = new JLabel("Width: ");
    this.add(labelw, c);

    c.gridx ++;
    spinnerw = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    spinnerw.addChangeListener(e -> {
      if(skipChangeEvent) return;
      frame.getDrawPane().wMBB = (double) Float.parseFloat(spinnerw.getValue().toString());
      frame.repaint();
    });
    spinnerw.setEnabled(frame.getDrawPane().mBB);
    ((JSpinner.DefaultEditor) spinnerw.getEditor()).getTextField().setColumns(6);
    this.add(spinnerw, c);

    c.gridx ++;
    JLabel labelh = new JLabel("Height: ");
    this.add(labelh, c);

    c.gridx ++;
    spinnerh = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    spinnerh.addChangeListener(e -> {
      if(skipChangeEvent) return;
      frame.getDrawPane().hMBB = (double) Float.parseFloat(spinnerh.getValue().toString());
      frame.repaint();
    });
    spinnerh.setEnabled(frame.getDrawPane().mBB);
    ((JSpinner.DefaultEditor) spinnerh.getEditor()).getTextField().setColumns(6);
    this.add(spinnerh, c);

    enabled.addActionListener(e -> {
      frame.getDrawPane().mBB = enabled.isSelected();
      if (frame.getDrawPane().mBB && (frame.getDrawPane().hMBB == 0 || frame.getDrawPane().wMBB == 0)) {
        frame.getDrawPane().calculateAutoMBBSize();
      }
      spinnerh.setEnabled(frame.getDrawPane().mBB);
      spinnerw.setEnabled(frame.getDrawPane().mBB);
      spinnery.setEnabled(frame.getDrawPane().mBB);
      spinnerx.setEnabled(frame.getDrawPane().mBB);
      spinnerh.setValue((int) frame.getDrawPane().hMBB);
      spinnerw.setValue((int) frame.getDrawPane().wMBB);
      spinnery.setValue((int) frame.getDrawPane().yMBB);
      spinnerx.setValue((int) frame.getDrawPane().xMBB);
      frame.showBoundingBox.setSelected(enabled.isSelected());
      frame.repaint();
    });
    
    reset.addActionListener(e -> {
      frame.getDrawPane().calculateAutoMBBSize();
      frame.getDrawPane().mBB = true;
      enabled.setSelected(true);
      frame.showBoundingBox.setSelected(true);
      updateMBBsize();
      frame.repaint();
    });
    
    updateMBBsize();
    
//    this.getRootPane().setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    this.pack();
    this.setLocationRelativeTo(null);
    this.addWindowListener(this);

    requestFocusInWindow();
  }
  
  private boolean skipChangeEvent = false;
  
  void updateMBBsize() {
    //prevents the setValue call from firing an event that will write back to the mbb.
    //That would not break anything, but it is still unnecessary
    skipChangeEvent = true;
    enabled.setSelected(frame.getDrawPane().mBB);
    spinnerx.setValue((int) frame.getDrawPane().xMBB);
    spinnery.setValue((int) frame.getDrawPane().yMBB);
    spinnerw.setValue((int) frame.getDrawPane().wMBB);
    spinnerh.setValue((int) frame.getDrawPane().hMBB);
    spinnerx.setEnabled(frame.getDrawPane().mBB);
    spinnery.setEnabled(frame.getDrawPane().mBB);
    spinnerw.setEnabled(frame.getDrawPane().mBB);
    spinnerh.setEnabled(frame.getDrawPane().mBB);
    skipChangeEvent = false;
  }

  @Override
  public void windowClosing(WindowEvent e) {
    frame.requestFocusInWindow();
  }
  @Override
  public void windowClosed(WindowEvent e) {}
  @Override
  public void windowOpened(WindowEvent e) {  }
  @Override
  public void windowIconified(WindowEvent e) {  }
  @Override
  public void windowDeiconified(WindowEvent e) {  }
  @Override
  public void windowActivated(WindowEvent e) {  }
  @Override
  public void windowDeactivated(WindowEvent e) {  }
}

