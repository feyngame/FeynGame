//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import game.FeynLine;
import game.LineConfig;
import game.LineType;

import javax.swing.*;
import java.awt.*;
import org.scilab.forge.jlatexmath.*;

/**
 * This component (which is not anymore a label but rather a button^^) is used in the {@link ui.TileSelect} to view all {@link game.LineType}.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
class TileLabel extends JButton {
  public static int latexSize = 20;
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 24L;
  /**
   * the config for the preset
   */
  private final LineConfig config;
  /**
   * the label for the description
   */
  private final JLabel label;
  /**
   * the constructor of this object for a specific config
   * @param lc the config
   */
  TileLabel(LineConfig lc) {
    this.config = lc;
    this.setBackground(Color.WHITE);
    this.setOpaque(false);
    if (lc.lineType != LineType.GRAB) {
      //noinspection SuspiciousNameCombination
      this.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
    }
    if (lc.description != null) {
      label = new JLabel(lc.description);
    } else {
      label = new JLabel("");
    }
  }


  /**
   * Creates a temporary {@link game.FeynLine} and calls {@link game.FeynLine#draw(Graphics2D, boolean)} to draw a preview.
   *
   * @param g2 Graphics object to draw to
   */
  @Override
  protected void paintComponent(Graphics g2) {
    super.paintComponent(g2);

    Graphics2D g = (Graphics2D) g2.create();

    RenderingHints rh = new RenderingHints(
          RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON);
    g.setRenderingHints(rh);

    if (config.lineType == LineType.GRAB)
      return;

    if (config.description == null) {
      int x = 0;
      int y = getWidth() / 2;
      int width = getWidth();

      g.setColor(Color.BLACK);

      FeynLine fl = new FeynLine(new game.Line(x + width / 8, y, x + width - width / 8, y), config, 0);
      fl.hideMom();
      fl.draggedMode = 2;
      fl.draw(g, false);
    } else if (config.description.contains("<html>")) {
      int x = 0;
      int width = getWidth();

      g.setColor(Color.BLACK);
      double widthL = label.getPreferredSize().getWidth();
      double heightL = label.getPreferredSize().getHeight();
      int offset = (int) (width - widthL) / 2;

      g.translate(offset, -5 - (int) heightL + getHeight());
      label.setSize(label.getPreferredSize());
      label.paint(g);
      g.translate(-offset, 5 + (int) heightL - getHeight());

      int restheight = getHeight() - (int) heightL - 5;

      g.setColor(Color.BLACK);

      FeynLine fl = new FeynLine(new game.Line(x + width / 8, restheight / 2, x + width - width / 8, restheight / 2), config, 0);
      fl.hideMom();
      fl.showDesc = false;
      fl.draggedMode = 2;
      fl.draw(g, false);
    } else {
      int x = 0;
      int width = getWidth();

      g.setColor(Color.BLACK);
      TeXFormula formula = new TeXFormula(config.description);
      TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_TEXT, latexSize);
      icon.setForeground(Color.BLACK);
      double widthL = icon.getIconWidth();
      double heightL = icon.getIconHeight();
      int offset = (int) (width - widthL) / 2;

      g.translate(offset, -5 - (int) heightL + getHeight());
      icon.paintIcon(this, g, 0, 0);
      g.translate(-offset, 5 + (int) heightL - getHeight());

      int restheight = getHeight() - (int) heightL - 5;

      g.setColor(Color.BLACK);

      FeynLine fl = new FeynLine(new game.Line(x + width / 8, restheight / 2, x + width - width / 8, restheight / 2), config, 0);
      fl.hideMom();
      fl.showDesc = false;
      fl.draggedMode = 2;
      fl.draw(g, false);
    }
  }
}
