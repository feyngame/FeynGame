package ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.Objects;

import javax.swing.ButtonModel;
import javax.swing.JToggleButton;

/**
 * A toggle button that renders a chain icon on the button area. Can be used to show that the
 * values of two inputs (usually width and height) are bound together by a property (fixed aspect ratio)
 */
public class ChainToggleButton extends JToggleButton {
  private static final long serialVersionUID = 1L;
  
  private static final Dimension MIN_SIZE = new Dimension(20, 40);
  private static final Dimension MAX_SIZE = new Dimension(20, Integer.MAX_VALUE);
  
  private Color disabledColor;
  
  public ChainToggleButton() {
    super();
    setForeground(Color.BLACK);
    disabledColor = Color.GRAY;
  }
  
  public ChainToggleButton(ButtonModel model) {
    this();
    setModel(model);
  }

  public Color getDisabledForeground() {
    return disabledColor;
  }
  
  public void setDisabledForeground(Color color) {
    this.disabledColor = Objects.requireNonNull(color);
  }
  
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Dimension size = getSize();
    Graphics2D g2 = (Graphics2D) g;
    g2.setColor(isEnabled() ? getForeground() : getDisabledForeground());
    
    //Center a 12x12 rect
    if(isSelected()) {
      g2.fillRect(size.width / 2 - 4, size.height / 2 - 6, 8, 12);
    } else {
      g2.fillRect(size.width / 2 - 4, size.height / 2 - 6, 8, 2);
      g2.fillRect(size.width / 2 - 4, size.height / 2 + 4, 8, 2);
    }
    Stroke s = g2.getStroke();
    g2.setStroke(new BasicStroke(1.5f));
    g2.drawLine(size.width / 2, 10, size.width / 2, size.height / 2 - 6);
    g2.drawLine(size.width / 2, size.height - 10, size.width / 2, size.height / 2 + 6);
    g2.drawLine(5, 10, size.width / 2, 10);
    g2.drawLine(5, size.height - 10, size.width / 2, size.height - 10);
    g2.setStroke(s);
  }

  @Override
  public Dimension getPreferredSize() {
    if(isPreferredSizeSet()) return super.getPreferredSize();
    return MIN_SIZE;
  }

  @Override
  public Dimension getMaximumSize() {
    if(isMaximumSizeSet()) return super.getMaximumSize();
    return MAX_SIZE;
  }

  @Override
  public Dimension getMinimumSize() {
    if(isMinimumSizeSet()) return super.getMinimumSize();
    return MIN_SIZE;
  }
  
}
