//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Taskbar;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StreamCorruptedException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.OverlayLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import org.jgrapht.alg.drawing.model.Box2D;
import org.scilab.forge.jlatexmath.ParseException;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import amplitude.FeynmanDiagram;
import export.ExportManager;
import game.FeynLine;
import game.Filling;
import game.FloatingImage;
import game.FloatingObject;
import game.InOutLevel;
import game.JavaVectorGraphic;
import game.Line;
import game.LineConfig;
import game.LineType;
import game.Momentum;
import game.MultiEdit;
import game.Point2D;
import game.Presets;
import game.Shaped;
import game.Vertex;
import layout.FeynGameDiagramStructureException;
import layout.GraphLayoutAlgorithm;
import layout.GraphLayoutException;
import qgraf.ModelData;
import qgraf.QgrafDiagram;
import qgraf.QgrafImportManager;
import qgraf.QgrafOutputFile;
import resources.GetResources;
import resources.Java8Support;

/**
 * The Frame that combines {@link ui.TileSelect} with {@link ui.TileLabel} and {@link ui.Pane}. It also has all the {@link KeyListener}, {@link MouseMotionListener} and {@link MouseListener}.
 * Here happens all the clipping to lines or Grid and the cutting of existing lines.
 *
 * @author Maximilian Lipp, Sven Yannick Klein
 */
@SuppressWarnings("JavadocReference")
public class Frame extends JFrame implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener, WindowListener {
  public static final Logger logger = Frame.getLogger(Frame.class);
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 4L;
  /**
   * whether or not to use debug mode
   */
  public static boolean DEBUG_MODE = false;
  /**
   * whether to not check for updates: -1, to always notify about new updates: -2, to update only on versions after the number given
   */
  public static String notifyUpdates = "-2";
  /**
   * the Menu Item for opening the blank screen
   */
  JMenuItem blankScreen;
  /**
   * whether or not to feyngame is run with gui
   */
  public static boolean GUI;
  /**
   * the dimensions of the drawPane
   */
  Dimension drawPaneSize;
  /**
   * the current version of FeynGame
   */
  private String version;
  /**
   * The distance at which the cursor is assumed to be on a line/point.
   */
  public static final int clipDistance = 10;
  /**
   * the dimension with which images get exported
   */
  public static Dimension imageExportDim = new Dimension(1920, 1080);
  /**
   * the dimension with which pdfs/prints at maximum canvas size get exported
   */
  public static Dimension pdfExportDim = new Dimension(7680, 4320);
  /**
   * true if runs on macOs because of bug of FileChooser Look and Feel
   */
  public static boolean macOs;
  /**
   * Static self reference used in {@link ui.TileSelect.myListener#actionPerformed(ActionEvent)} to {@link JFrame#requestFocus()} after clicking on a tile. Otherwise the {@link KeyListener} would not work afterwords.
   * Also used in some other locations
   */
  public static Frame frame;
  /**
   * Added in the {@link BorderLayout#CENTER} position.
   */
  public Pane drawPane;
  private GridPanel gridPanel;
  /**
   * The menu available when a qgraf file is loaded ({@code null} in InFin mode
   */
  private final JMenu qgrafMenu;
  //TODO add JMenuItem for export all in file menu
  private final JMenuItem qgrafUpdateModelExt;
  /**
   * holds all the models; String is the name of the model file and TileSelect is the TileSelect object holding the model
   */
  public HashMap<String, Presets> models;
  /**
   * the panel that holds all the tabs for the models
   */
  public TileSelectTabbed tileTabs;
  /**
   * the component holding the tabs and the scroll pane for the tiles
   */
  private JPanel tilePanel;
  /**
   * Added in the {@link BorderLayout#SOUTH} position.
   */
  public TileSelect tiles;
  /**
   * gives the game/drawing mode: 0: free drawing; 1: InFinMode
   */
  public static int gameMode = 0;
  /**
   *
   */
  public static int LaTeXFontSize = 20;
  /**
   * if a vertex is dragged this indicates what lines are attached to it and wether its their end/start/both
   * <ul><li><B>-1</B>is the end</li>
   * <li><B>0</B>is both</li>
   * <li><B>1</B>is the start</li></ul>
   */
  ArrayList<Integer> lineModes;
  /**
   * the x-component of the current location of the mouse
   */
  double currX;
  /**
   * the y-component of the current location of the mouse
   */
  double currY;
  /**
   * the x-component of the distance between the point that is dragged and the mouse
   */
  double dragDisX = 0;
  /**
   * the y-component of the distance between the point that is dragged and the mouse the mouse
   */
  double dragDisY = 0;
  /**
   * the vector from end to start of the lastly copied line
   */
  Point2D disCopyLen;
  /**
   * the height of the line that was last copied
   */
  double copyHeight = 0;
  /**
   * the path to the currently active config/model file
   */
  String configPath;
  /**
   * Holds the TileSelect
   */
  JScrollPane tileScroll;
  /**
   * Variable for calculating the dragged distance in the {@link #mouseDragged(MouseEvent)}. It saves the location where the drag started (where was first clicked).
   */
  private Point.Double draggedDist;
  /**
   * When dragging entire lines via shift, this variable saves the original location.
   */
  private FeynLine draggedLine;
  /**
   * If {@link game.LineType#GRAB} is selected, this safes the mode:
   * <ul><li><B>-1</B> means changing both the start and the end point of the {@link game.FeynLine#line}</li>
   * <li><B>0</B> means changing the start point of the {@link game.FeynLine#line}</li>
   * <li><B>1</B> means changing the end point of the {@link game.FeynLine#line}</li>
   * <li><B>2</B> means that a vertex is dragged</li>
   * <li><B>3</B> means that a floating object is dragged</li>
   * <li><B>4</B> means that a description of a line is dragged</li>
   * <li><B>5</B> means that a description of a vertex is dragged</li>
   * <li><B>6</B> means that a description of a floatingObject is dragged</li>
   * <li><B>7</B> means the background is dragged (everything is moved in the opposite direction)</li>
   * <li><B>8</B> means that a shape is dragged</li>
   * <li><B>9</B> means that a description of a shape is dragged</li>
   * <li><B>10</B> means that a side of the manual bounding box is dragged. The variable boundingBoxDraggedSide will hold the dragged side</li>
   * <li><B>11</B> means that the entire manual bounding box is dragged.</li>
   * <li><B>12</B> means that opposite sides of the manual bounding box are dragged. The variable boundingBoxDraggedSide will hold the dragged side</li></ul>
   */
  private int draggedMode; // -1 drag, 0,1 move

  /**
   * If draggedMode is 10, this is the dragged side:
   * <ul>
   * <li>1 is the left side</li>
   * <li>2 is the top side</li>
   * <li>3 is the right side</li>
   * <li>4 is the bottom side</li>
   * <li>5 is the top-left corner</li>
   * <li>6 is the top-right corner</li>
   * <li>7 is the bottom-right corner</li>
   * <li>8 is the bottom-left corner</li>
   * </ul>
   */
  private int boundingBoxDraggedSide;

  /**
   * Counter used to identify wether a mouse scroll is continued or began newly
   */
  private int scrollCounter;
  /**
   * used to reset the {@link ui.Frame#scrollCounter}
   */
  private javax.swing.Timer wheelMovementTimer;
  /**
   * The EditFrame used to edit lines or vertices
   */
  public final EditFrame eframe;
  /**
   * The BlanksEdit used to edit blanks of lines
   */
  public BlanksEdit beframe;
  /**
   * the path to the currently opened file
   */
  private String currentFile;
  /**
   * gives the level if game mode is 1 (InFin Mode)
   */
  private InOutLevel inOutLevel;
  /**
   * the current challenge in the inOutLevel
   */
  private Map.Entry<ArrayList<String>, ArrayList<String>> challenge;
  /**
   * the points that the player currently has
   */
  private int points = 0;
  /**
   * the points one gets for finishing the current challenge
   */
  private int challengePoints;
  /**
   * displays the points the player has
   */
  private JLabel pointsLabel;
  /**
   * displays the points of the current challenge
   */
  private JLabel currPointsLabel;
  /**
   * displays the ramaining time
   */
  private JLabel time;
  /**
   * the remaining time in sec
   */
  private int remTime;
  /**
   * the total time of one run
   */
  private int totTime;
  /**
   * the path to the directory where the last diagram was saved to/loaded from
   */
  private String diagramSavePath;
  /**
   * The currently loaded qgraf file, null if no qgraf is loaded
   */
  public QgrafOutputFile currentQgrafFile;

  protected final JCheckBoxMenuItem showBoundingBox;
  /**
   * Index of active diagram in {@link #currentQgrafFile}
   */
  private int currentQgrafFileIndex;
  /**
   * This model should be applied to all diagrams loaded from the open qgraf file.
   */
  public List<LineConfig> modelForAllQgrafLoads;
  /**
   * If true, pasting a qgraf diagram will clear existing lines
   */
  public boolean clearCanvasOnPaste = true;
  /**
   * the path to the directory where the last config/model was saved to/loaded from
   */
  private String configSavePath;
  /**
   * the path to the directory where the last image was saved to
   */
  public String imgSavePath;
  /**
   * the path to the directory where the last image/".fx" file was loaded from
   */
  private String imgLoadPath;
  /**
   * true if any key is pressed
   */
  private boolean keyClicked;
  /**
   * saves the height of the line when scrolling and line height is below 10
   */
  private double scrollHeight = 0;
  /**
   * global toast defined here such that it can be deleted to avoid duplications
   */
  private Toast t;

  public boolean dialogFramesAlwaysOnTop = Toolkit.getDefaultToolkit().isAlwaysOnTopSupported();
  public boolean resetExportDestinationOnLoad = false;
  
  /****************************************************
   * Declare all dialogs as one instance that will be hidden and reused if necessary. This prevents additional work trying
   * to restore the previous state of that window.
   ***************************************************/

  private final QgrafSourceFrame sourceFrame;
  private final MBBFrame mbbFrame;
  public final Settings settingsFrame;

  private final JCheckBoxMenuItem gridAutoLayout;

  /**
   * the menu radiobuttons to control the clipping angle
   */
  private JRadioButtonMenuItem noAngleRadio;
  private JRadioButtonMenuItem PI2AngleRadio;
  private JRadioButtonMenuItem PI4AngleRadio;
  private JRadioButtonMenuItem PI16AngleRadio;
  /**
   * saves the shape to be pasted
   */
  private Shaped copShape;
  /**
   * action for deletion
   */
  private DelAction delAction;
  /**
   * wether or not to convert the canvas to a model file when loading a ".fg" file
   * 0: not decided: ask every time
   * 1: no
   * 2: yes
   */
  public int convCanToModel = 0;
  /**
   * whether or not to show tooltips
   */
  private boolean toolTip = false;

  public ButtonGroup edgeLengthButtonGroup;
  public ButtonGroup legOrderButtonGroup;
  private JButton retry, check, next;
  private boolean resizePossible;
  static public String fileNameProp = "untitled";

  private final JMenu layoutPresetsSubMenu = new JMenu("Layout presets");

  public static final JLabel statusLabel = new JLabel("Status");

  private Recents recents;

  public static ArrayList<String> recentEntries = new ArrayList<String>();

  public static void setStatusText(String text) {
    statusLabel.setText(text);
  }
  
  public void updateLayoutPresetsMenu(int selection, Iterable<GraphLayoutAlgorithm> algs) {
    if(layoutPresetsSubMenu == null) {
      logger.fine("Cannot edit layout menu: not initialized");
      return;
    }
    
    layoutPresetsSubMenu.removeAll();
    ButtonGroup bg = new ButtonGroup();
    int i = 0;
    for(GraphLayoutAlgorithm gla : algs) {
      final int j = i; //capture in closure
      JRadioButtonMenuItem jrbmi = new JRadioButtonMenuItem(gla.getName(), false);
      bg.add(jrbmi);
      if(i == selection) jrbmi.setSelected(true);
      jrbmi.addActionListener(e -> {
        if(jrbmi.isSelected()) {
          GraphLayoutAlgorithm.setSelectedPresetIndex(j);
        }
      });
      i += 1;
      layoutPresetsSubMenu.add(jrbmi);
    }
    
    layoutPresetsSubMenu.addSeparator();
    
    /*
     * The cycle menu items are defined here so we can easily add a keybind if desired.
     */
    JMenuItem cyclePrevPreset = new JMenuItem("Cycle previous");
    cyclePrevPreset.setVisible(false);
    cyclePrevPreset.addActionListener(e -> {
      int current = GraphLayoutAlgorithm.getSelectedPresetIndex();
      int size = GraphLayoutAlgorithm.getLayoutAlgorithms().size();
      if(current == 0) {
        GraphLayoutAlgorithm.setSelectedPresetIndex(size - 1);
      } else {
        GraphLayoutAlgorithm.setSelectedPresetIndex(current - 1);
      }
    });
    JMenuItem cycleNextPreset = new JMenuItem("Cycle next");
    cycleNextPreset.setVisible(false);
    cyclePrevPreset.addActionListener(e -> {
      int current = GraphLayoutAlgorithm.getSelectedPresetIndex();
      int size = GraphLayoutAlgorithm.getLayoutAlgorithms().size();
      if(current == size - 1) {
        GraphLayoutAlgorithm.setSelectedPresetIndex(0);
      } else {
        GraphLayoutAlgorithm.setSelectedPresetIndex(current + 1);
      }
    });
    JMenuItem editPresets = new JMenuItem("Edit presets");
    editPresets.addActionListener(e -> {
      if(settingsFrame != null) {
        settingsFrame.setVisible(true);
        settingsFrame.setShownPage(Settings.LAYOUT_UI_TITLE, true);
      }
    });
    layoutPresetsSubMenu.add(cyclePrevPreset);
    layoutPresetsSubMenu.add(cycleNextPreset);
    layoutPresetsSubMenu.add(editPresets);
    
    layoutPresetsSubMenu.revalidate();
  }
  
  /**
   * this constructor opens the main Window of FeynGame with the default
   * model file
   */
  public Frame() {
    this(null);
  }
  /**
   * Opens FeynGame in the InFin mode:
   * Puts frame together, sets title, size and listener. Also it loads the version from the {@code version.txt} file and puts it in the title as a kind of version number.
   *
   * @param timer     wether or not to use a timer or not
   * @param level     the level that is played
   * @param totalTime the total time one has to gather points
   */
  public Frame(InOutLevel level, Boolean timer, int totalTime) {
    this(1, null, true, level, timer, totalTime);
  }

  /**
   * deletes all images from memory not used
   */
  private void delImages() {

    boolean debug = false;

    ArrayList<String> keyList = new ArrayList<String>(game.Filling.pathToImageNumber.keySet());

    for (String key : keyList) {
      Boolean del = true;
      for (Vertex v : drawPane.vertices) {
        if (v.filling.name == null && v.filling.graphic) {
          if (v.filling.imageNumber == game.Filling.pathToImageNumber.get(key)) {
            del = false;
            break;
          }
        }
      }
      if (del) {
        for (FloatingObject fO : drawPane.fObjects) {
          if (fO.v) {
            Vertex v = fO.vertex;
            if (v.filling.name == null && v.filling.graphic) {
              if (v.filling.imageNumber == game.Filling.pathToImageNumber.get(key)) {
                del = false;
                break;
              }
            }
          } else {
            FloatingImage fI = fO.floatingImage;
            if (fI.name == null) {
              if (fI.imageNumber == game.Filling.pathToImageNumber.get(key)) {
                del = false;
                break;
              }
            }
          }
        }
      }
      if (del) {
        for (Shaped s : drawPane.shapes) {
          Filling f = s.fill;
          if (f.name == null && f.graphic) {
            if (f.imageNumber == game.Filling.pathToImageNumber.get(key)) {
              del = false;
              break;
            }
          }
        }
      }
      if (del) {
        Frame.logger.fine("Removing image " + key + " from Memory ...");
        Integer imageNumber = game.Filling.pathToImageNumber.get(key);
        game.Filling.pathToImageNumber.remove(key);
        game.Filling.imageNumberToImage.remove(imageNumber);
      }
    }
  }
  /**
   * deletes all non used JavaFX from memory
   */
  private void delJavaFX() {
    boolean debug = false;

    ArrayList<String> keyList = new ArrayList<String>(game.Filling.nameToJVG.keySet());

    for (String key : keyList) {
      Boolean del = true;
      for (Vertex v : drawPane.vertices) {
        if (v.filling.name != null) {
          if (v.filling.name == key) {
            del = false;
            break;
          }
        }
      }
      if (del) {
        for (FloatingObject fO : drawPane.fObjects) {
          if (fO.v) {
            Vertex v = fO.vertex;
            if (v.filling.name != null) {
              if (v.filling.name == key) {
                del = false;
                break;
              }
            }
          } else {
            FloatingImage fI = fO.floatingImage;
            if (fI.name != null) {
              if (fI.name == key) {
                del = false;
                break;
              }
            }
          }
        }
      }
      if (del) {
        for (Shaped s : drawPane.shapes) {
          Filling f = s.fill;
          if (f.name != null) {
            if (f.name == key) {
              del = false;
              break;
            }
          }
        }
      }
      if (del) {
        Frame.logger.fine("Removing JavaFX " + key + " from Memory ...");
        game.Filling.nameToJVG.remove(key);
        if (javafx.JavaFXToJavaVectorGraphic.patternsOut.contains(key)) {
          javafx.JavaFXToJavaVectorGraphic.patternsOut.remove(key);
        }
      }
    }
  }
  /**
   * clears all redo/undo lists
   */
  private void deleteHistory() {
    drawPane.redoListLines.clear();
    drawPane.undoListLines.clear();
    drawPane.redoListVertices.clear();
    drawPane.undoListVertices.clear();
    drawPane.redoListfObjects.clear();
    drawPane.undoListfObjects.clear();
    drawPane.redoListShapes.clear();
    drawPane.undoListShapes.clear();
    this.delImages();
    this.delJavaFX();
  }

  private int compareVersion(String ver1, String ver2) {
    int v1 = 0;
    int v2 = 0;
    String[] split1 = ver1.split("\\.");
    int i = split1.length-1;
    while (i >= 0) {
      v1 += Integer.parseInt(split1[i].trim()) * (int) Math.pow(100., split1.length-i);
      i-=1;
    }
    String[] split2 = ver2.split("\\.");
    i = split2.length-1;
    while (i >= 0) {
      v2 += Integer.parseInt(split2[i].trim()) * (int) Math.pow(100., split2.length-i);
      i-=1;
    }
    return v1 - v2;
  }

  private void checkForUpdates() {
    if (notifyUpdates.equals("-1")) return;
    
    // link to file: https://gitlab.com/feyngame/FeynGame/-/raw/master/src/version.txt?ref_type=heads&inline=false
    final String updateURL = "https://gitlab.com/feyngame/FeynGame/-/raw/master/src/version.txt?ref_type=heads&inline=false"; 
    URL url;
    try {
      url = new URL(updateURL);
    } catch (MalformedURLException e) {
      logger.log(Level.WARNING, "Malformed update url " + updateURL, e);
      return;
    }
    
    Thread updateCheckerThread = new Thread(() -> {
      try(Scanner s = new Scanner(url.openStream())) {
        String updateVersion = "";
        while (s.hasNext()) {
          updateVersion = updateVersion + s.next();
        }
        updateVersion = updateVersion.replaceAll(System.getProperty("line.separator"), "");
        final String updateVersionFinal = updateVersion.replaceAll("\n", "");

        if (updateVersionFinal.equals(notifyUpdates)) return;

        if (compareVersion(version.split(" ")[0].replaceAll(" ", ""), updateVersionFinal) < 0) {
          SwingUtilities.invokeLater(() -> {
            showUpdate(updateVersionFinal);
          });
        }
      } catch (Exception ignored) {
        logger.warning("Fetching updates failed: " + ignored.getMessage());
      }
    }, "UpdateChecker");
    updateCheckerThread.setDaemon(true);
    updateCheckerThread.start();
  }

  private void showUpdate(String newVersion) {
    Object[] options = {"Ok", "Take me to the Update", "Do not notify me about updates", "Do not notify me about this update"};
    int ret = JOptionPane.showOptionDialog(this, "There is new version of FeynGame.", "FeynGame update", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
    if (ret == 1) {
      Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
      if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
        try {
          desktop.browse(new URL("https://gitlab.com/feyngame/FeynGame").toURI());
          return;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } else if (ret == 2) {
      notifyUpdates = "-1";
    } else if (ret == 3) {
      notifyUpdates = newVersion;
    }
  }

  public Frame(String path) {
    this(path, true);
  }

  /**
   * initializes the main Window of FeynGame in the drawing mode
   * Puts frame together, sets title, size and listener. Also it loads the time from the {@code date.txt} file and puts it in the title as a kind of version number.
   *
   * @param path path to config/model file
   * @param show whether or not to actually show the frame
   */
  public Frame(String path, boolean show) {
    this(0, path, show, null, false, 0);
  }

  public static String readFeynGameVersion() {
    /* get version*/
    URL resource = Frame.class.getResource("/version.txt");
    if(resource == null) {
      logger.warning("Cannot find version.txt in JAR file");
      return "";
    }

    byte[] readData = new byte[100];
    try(InputStream is = resource.openStream()) {
      int read = is.read(readData);
      if (read > 0) {
        String res = new String(readData);
        res = res.replaceAll(System.getProperty("line.separator"), "");
        res = res.replaceAll("\n", "");
        return res;
      }
    } catch (IOException e) {
      logger.log(Level.WARNING, "Cannot access version.txt in JAR file", e);
    } catch (Exception ignored) {
      logger.log(Level.SEVERE, "", ignored);
    }
    return "";
  }

  public Frame(int mode, String path, boolean show, InOutLevel level, Boolean timer, int totalTime) {
    super();
    String os = System.getProperty("os.name");
    macOs = os.toLowerCase().startsWith("mac");
    frame = this;
    QgrafImportManager.initFrame(this);
    Frame.GUI = show;
    this.disCopyLen = new Point2D(0, 100);
    
    resizePossible = false;

    this.gameMode = mode;
    if (mode == 1) {
      this.totTime = totalTime;
      this.inOutLevel = level;
      FeynLine.showDescDef = true;
    }

    /* Basic look of the window*/

    this.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}
    
    Frame.logger.info("Running on " + os);
    Frame.logger.info("Working directory: " + System.getProperty("user.dir"));

    recents = new Recents(this);

    this.scrollCounter = 0;

    /* version number */
    this.version = readFeynGameVersion();
    
    if (gameMode == 0) { this.setTitle("Untitled.fg - " + "FeynGame v" + version); }
    else { this.setTitle("FeynGame v" + version); }
    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    Frame.statusLabel.setBorder(new EmptyBorder(0,10,0,10));
    Frame.statusLabel.setText("Welcome to FeynGame v" + version);

    /* the drawing canvas */

    if (gameMode == 1) {
      FeynLine.descDisDef *= 2;
      FeynLine.descScaleDef *=2;
      FeynLine.descDisDef *= 2;
    }

    /* the main part of the window */

    drawPane = new Pane(this, true);
    drawPane.setAlignmentX(0.5f);
    drawPane.setAlignmentY(0.5f);
    QgrafImportManager.updateQgrafDefaultStyles();
    if (gameMode == 1) {
      drawPane.grid = true;
      drawPane.helperLines = false;
      drawPane.angles = new ArrayList<>();
    }
    drawPane.setPreferredSize(new Dimension(600, 400));
    this.drawPaneSize = new Dimension(600, 400);
    this.getPreferences();
    Dimension drawPaneSizeCopy = new Dimension(this.drawPaneSize);
    JPanel canvasPanel = new JPanel();
    canvasPanel.setLayout(new OverlayLayout(canvasPanel));
    gridPanel = new GridPanel(this);
    gridPanel.setAlignmentX(0.5f);
    gridPanel.setAlignmentY(0.5f);
    canvasPanel.add(drawPane);
    canvasPanel.add(gridPanel);
    this.add(canvasPanel, BorderLayout.CENTER);

    if (show) {
      checkForUpdates();
    }

    JPanel gamePanel = new JPanel(new BorderLayout());
    if (gameMode == 1) {
      gamePanel.setAlignmentX(0.5f);
      gamePanel.setAlignmentY(0.5f);

      JPanel pointPanel = new JPanel();
      GridBagLayout gb = new GridBagLayout();
      GridBagConstraints gbc = new GridBagConstraints();
      pointPanel.setLayout(gb);

      gbc.weightx = 1;
      gbc.weighty = 1;
      gbc.gridy = 0;
      gbc.gridx = 0;
      gbc.fill = (GridBagConstraints.HORIZONTAL);
      // gbc.insets = new Insets(0, 100000, 0, 100000);
      pointPanel.setBorder(BorderFactory.createLineBorder(Color.black, 4));

      JLabel labelPoints = new JLabel("<html><h3> Points of this challenge: ");

      pointPanel.add(labelPoints, gbc);
      gbc.gridx++;
      currPointsLabel = new JLabel();
      pointPanel.add(currPointsLabel, gbc);
      gbc.gridx++;
      JLabel labelOverPoints = new JLabel("<html><h3> Points:");
      pointPanel.add(labelOverPoints, gbc);
      gbc.gridx++;
      pointsLabel = new JLabel("<html><h2, style=\"color:Blue\"> 0");
      pointPanel.add(pointsLabel, gbc);
      if (timer) {
        gbc.gridx++;
        pointPanel.add(new JLabel("<html><h3> Remaining time: "), gbc);
        time = new JLabel("<html><h2> " + this.totTime);
        gbc.gridx++;
        pointPanel.add(time, gbc);
      }
      gamePanel.add(pointPanel, BorderLayout.NORTH);


      JPanel tmpPanel = new JPanel(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.gridx = 0;
      c.gridy = 0;
      c.weighty = 1;
      c.weightx = 1;
      c.fill = GridBagConstraints.HORIZONTAL;
      retry = new JButton("Retry");
      retry.addActionListener(e -> this.reloadChallenge());
      check = new JButton("Finish");
      check.addActionListener(e -> {
        this.checkLines();
        Frame.this.requestFocus();
      });
      next = new JButton("Skip");
      next.addActionListener(e -> this.loadChallenge());
      tmpPanel.add(retry, c);
      c.gridx++;
      tmpPanel.add(next, c);
      c.gridx++;
      tmpPanel.add(check, c);
      gamePanel.add(tmpPanel, BorderLayout.CENTER);
      // this.add(gamePanel, BorderLayout.CENTER);
    }

    JPanel southPanel = new JPanel();
    southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
    if (gameMode == 0) {
      /* loads the model file and constructs the tile section at the bottom*/
      models = new HashMap<String, Presets>();
      tilePanel = new JPanel(new BorderLayout());
      tileTabs = new TileSelectTabbed(this);
      if (path == null || path.equals("")) {
        this.loadDefaultConfig();
      } else {
        this.loadConfig(path);
        this.models.get(tileTabs.getCurrentKey()).unsaved = false;
      }
      tilePanel.add(tileTabs, BorderLayout.NORTH);
      tilePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
      southPanel.add(tilePanel);
      tiles.setGrabMode();
      Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
      drawPane.setCursor(cursor);
      /* Load the diagram from the last time FeynGame was open from the disk*/
      this.getLastDiagram();
    } else {
      /* load the model file given in the model file and constructs the
       * tiles*/
      tilePanel = new JPanel(new BorderLayout());
      if (level.configPath == null) {
        Frame.logger.info("No model file given in level file, using default model...");
      } else {
        Frame.logger.info("Model file: " + level.configPath);
      }
      this.loadConfig(level.configPath);
      tilePanel.add(gamePanel, BorderLayout.NORTH);
      tilePanel.add(tiles, BorderLayout.CENTER);
      tilePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
      southPanel.add(tilePanel);
      southPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
      resizeTiles();
      // this.pack();
      tiles.setGrabMode();
      Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
      drawPane.setCursor(cursor);

      drawPane.setSize(drawPaneSizeCopy);
      /* load the first challenge*/
      this.loadChallenge();
      drawPane.setSize(drawPaneSizeCopy);
    }
    Frame.statusLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
    southPanel.add(Frame.statusLabel);
    this.add(southPanel, BorderLayout.SOUTH);


    /*
     * Initialize all reused dialogs here; after drawPane but before menu.
     */
    edgeLengthButtonGroup = new ButtonGroup();
    legOrderButtonGroup = new ButtonGroup();

    this.sourceFrame = new QgrafSourceFrame(this, dialogFramesAlwaysOnTop);
    this.mbbFrame = new MBBFrame(this, dialogFramesAlwaysOnTop);
    this.settingsFrame = new Settings(this, dialogFramesAlwaysOnTop);
    this.eframe = new EditFrame(this, dialogFramesAlwaysOnTop);
//    QgrafImportManager.initFrame(this);

    /* the menu */

    JMenuBar menu = new JMenuBar();
    JMenu file = new JMenu("File");
    JMenuItem openNew = new JMenuItem("New");
    openNew.addActionListener(e -> {
      this.openNew();
      setStatusText("New diagram");
      });
    if (!macOs) {
      openNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
    } else {
      openNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.META_DOWN_MASK));
    }
    file.add(openNew);

    JMenuItem loadDiagram = new JMenuItem("Open");
    loadDiagram.addActionListener(e -> this.loadDiagram());
    if (!macOs) {
      loadDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_DOWN_MASK));
    } else {
      loadDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.META_DOWN_MASK));
    }
    openNew.setEnabled(gameMode == 0);
    file.add(loadDiagram);

    recents.setEnabled(gameMode == 0);
    file.add(recents);

    JMenuItem importQgrafFile = new JMenuItem("Import qgraf");
    importQgrafFile.addActionListener(e -> {
      QgrafImportManager.doGuiImport(this);
    });
    file.add(importQgrafFile);

    JMenuItem saveDiagram = new JMenuItem("Save");
    saveDiagram.addActionListener(e -> this.saveDiagram());
    if (!macOs) {
      saveDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
    } else {
      saveDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.META_DOWN_MASK));
    }
    file.add(saveDiagram);

    JMenuItem saveAsDiagram = new JMenuItem("Save as");
    saveAsDiagram.addActionListener(e -> this.saveAsDiagram());
    if (!macOs) {
      saveAsDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_DOWN_MASK | InputEvent.CTRL_DOWN_MASK));
    } else {
      saveAsDiagram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_DOWN_MASK | InputEvent.META_DOWN_MASK));
    }
    file.add(saveAsDiagram);

    file.addSeparator();

    JMenuItem print = new JMenuItem("Print");
    print.addActionListener(e -> this.printDrawPane());
    if (!macOs) {
      print.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));
    } else {
      print.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.META_DOWN_MASK));
    }
    file.add(print);

    JMenuItem export = new JMenuItem("Export");
    export.addActionListener(e -> this.exportDrawPane());
    if (!macOs) {
      export.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK));
    } else {
      export.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.META_DOWN_MASK));
    }
    file.add(export);

    JMenuItem convToImageCB = new JMenuItem("Export to clipboard");
    convToImageCB.addActionListener(e -> this.saveImageToCB());
    if (!macOs) {
      convToImageCB.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
    } else {
      convToImageCB.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.META_DOWN_MASK));
    }
    file.add(convToImageCB);

    file.addSeparator();

    JMenuItem settings = new JMenuItem("Settings");
    settings.addActionListener(e -> {
      if(settingsFrame != null) settingsFrame.setVisible(true);
    });
    file.add(settings);

    JMenuItem exit = new JMenuItem("Exit");
    exit.addActionListener(e -> {
      this.setPreferences();
      if (gameMode == 0) { this.setLastDiagram(); }
      System.exit(0);
    });
    if (!macOs) {
      exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
    } else {
      exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.META_DOWN_MASK));
    }
    file.add(exit);

    menu.add(file);

    JMenu edit = new JMenu("Edit");

    JMenuItem undo = new JMenuItem("Undo");
    undo.addActionListener(e -> this.undo());
    if (!macOs) {
      undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
    } else {
      undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.META_DOWN_MASK));
    }
    edit.add(undo);

    JMenuItem redo = new JMenuItem("Redo");
    redo.addActionListener(e -> this.redo());
    if (!macOs) {
      redo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_DOWN_MASK));
    } else {
      redo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.META_DOWN_MASK));
    }
    edit.add(redo);

    edit.addSeparator();

    JMenuItem copy = new JMenuItem("Copy");
    copy.addActionListener(e -> {
      if (!this.keyClicked) {
        this.currX = this.drawPane.getSize().width / 2;
        this.currY = this.drawPane.getSize().height / 2;
      }
      this.copyToClipboard();
      Frame.this.requestFocusInWindow();
    });
    if (!macOs) {
      copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
    } else {
      copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.META_DOWN_MASK));
    }
    edit.add(copy);

    JMenuItem paste = new JMenuItem("Paste");
    paste.addActionListener(e -> {
      if (!this.keyClicked) {
        this.currX = this.drawPane.getSize().width / 2;
        this.currY = this.drawPane.getSize().height / 2;
      }
      this.pasteFromClipBoard();
    });
    if (!macOs) {
      paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
    } else {
      paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.META_DOWN_MASK));
    }
    edit.add(paste);

    edit.addSeparator();

    JMenuItem delAll;
    if (gameMode == 0) { delAll = new JMenuItem("Delete all objects"); }
    else { delAll = new JMenuItem("Reload challenge"); }
    delAll.addActionListener(e -> {
      this.saveForUndo();
      if (gameMode == 0) {
        Frame.statusLabel.setText("Deleted all objects");
        clearCanvas(CLEARMODE_CLEAR_DIAGRAM);
      } else {
        this.reloadChallenge();
        Frame.statusLabel.setText("Challenge reloaded");
      }
    });
    delAll.setAccelerator(KeyStroke.getKeyStroke("SPACE"));
    edit.add(delAll);

    /**JMenuItem delSel = new JMenuItem("Delete selected object");
    delSel.addActionListener(e -> {
      if (drawPane.lines.size() > 0 && drawPane.selectType == 0) {
        this.saveForUndo();
        Point2D oldStart = drawPane.lines.get(drawPane.lines.size() - 1).getStart();
        Point2D oldEnd = drawPane.lines.get(drawPane.lines.size() - 1).getEnd();
        Frame.this.removeLine(drawPane.lines.size() - 1);
        drawPane.checkForVertex(new Point2D((int) Math.round(oldStart.x), (int) Math.round(oldStart.y)));
        drawPane.checkForVertex(new Point2D((int) Math.round(oldEnd.x), (int) Math.round(oldEnd.y)));
      } else if (drawPane.fObjects.size() > 0 && drawPane.selectType == 2) {
        this.saveForUndo();
        drawPane.fObjects.remove(drawPane.fObjects.size() - 1);
        Frame.this.repaint();
      } else if (drawPane.selectType == 3 && drawPane.shapes.size() > 0) {
        this.saveForUndo();
        Shaped removedShape = drawPane.shapes.remove(drawPane.shapes.size() - 1);
        ArrayList<Point2D> pointList = new ArrayList<>();
        for (Vertex v : drawPane.vertices) {
          pointList.add(v.origin);
        }
        for (Point2D point : pointList) {
          drawPane.checkForVertex(point);
        }
        //In case the deleted shape was the layout rectangle, update the checkbox state
        GraphLayoutAlgorithm.useRectangleForBounds(getLayoutRectangle() != null);
        drawPane.lastLayoutRect = removedShape;

        Frame.this.repaint();
      } else if (drawPane.selectType == 1 && drawPane.vertices.size() > 0){
        Vertex v = drawPane.vertices.get(drawPane.vertices.size() - 1);
        if (!v.autoremove) {
          Point2D point = v.origin;
          v.autoremove = true;
          combineLines(point);
          drawPane.checkForVertex(point);
        }
      }
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (this.beframe != null)
        beframe.update();
      Frame.this.requestFocusInWindow();
    });*/
    delAction = new DelAction("Delete selected object", this);
    JMenuItem delSel = new JMenuItem(delAction);
    delSel.setAccelerator(KeyStroke.getKeyStroke("BACK_SPACE"));
    edit.add(delSel);

    edit.addSeparator();

    //    JMenuItem autoLayout = new JMenuItem("Auto-Layout");
    //    autoLayout.addActionListener(e -> doAutoLayout());
    //    if (!macOs) {
    //      autoLayout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
    //    } else {
    //      autoLayout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.META_DOWN_MASK));
    //    }
    //    edit.add(autoLayout);
    //
    //    edit.addSeparator();

    JMenu angleSubMenu = new JMenu("Clipping angles");

    ButtonGroup angleGroup = new ButtonGroup();

    noAngleRadio = new JRadioButtonMenuItem("<html>Not clipping to any angles");
    noAngleRadio.addActionListener(e -> {
      drawPane.angles = new ArrayList<>();
    });
    angleGroup.add(noAngleRadio);
    angleSubMenu.add(noAngleRadio);

    PI2AngleRadio = new JRadioButtonMenuItem("<html>Clipping to 90&deg");
    PI2AngleRadio.addActionListener(e -> {
      drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 2));
    });
    angleGroup.add(PI2AngleRadio);
    angleSubMenu.add(PI2AngleRadio);

    PI4AngleRadio = new JRadioButtonMenuItem("<html>Clipping to 45&deg");
    PI4AngleRadio.addActionListener(e -> {
      drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 4));
    });
    angleGroup.add(PI4AngleRadio);
    angleSubMenu.add(PI4AngleRadio);

    PI16AngleRadio = new JRadioButtonMenuItem("<html>Clipping to 22.5&deg");
    PI16AngleRadio.addActionListener(e -> {
      drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 16));
    });
    angleGroup.add(PI16AngleRadio);
    angleSubMenu.add(PI16AngleRadio);

    if (drawPane.angles.equals(new ArrayList<>(Collections.singletonList(Math.PI / 16)))) {
      PI16AngleRadio.setSelected(true);
    } else if (drawPane.angles.equals(new ArrayList<>(Collections.singletonList(Math.PI / 4)))) {
      PI4AngleRadio.setSelected(true);
    } else if (drawPane.angles.equals(new ArrayList<>(Collections.singletonList(Math.PI / 2)))) {
      PI2AngleRadio.setSelected(true);
    } else {
      noAngleRadio.setSelected(true);
    }

    edit.add(angleSubMenu);

    JMenuItem addImage = new JMenuItem("Add new image / JavaFx");
    addImage.addActionListener(e -> {
      if (!keyClicked) {
        currX = drawPane.getWidth() / 2;
        currY = drawPane.getHeight() / 2;
      }
      if (drawPane.selectType == 1 && drawPane.vertices.size() > 0) {
        this.selectImage();
        Frame.this.requestFocus();
      } else if (drawPane.selectType == 2 && drawPane.fObjects.size() > 0 && drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
        this.selectImageFl();
        Frame.this.requestFocus();
      } else if (drawPane.selectType == 3 && drawPane.shapes.size() > 0) {
        this.selectImageSFl();
        Frame.this.requestFocus();
      } else {
        this.addImage();
        Frame.this.requestFocus();
      }
    });
    addImage.setEnabled(gameMode == 0);
    edit.add(addImage);

    JMenuItem addShape = new JMenuItem("Add new shape");
    addShape.addActionListener(e -> {
      if (!keyClicked) {
        currX = drawPane.getWidth() / 2;
        currY = drawPane.getHeight() / 2;
      }
      Point2D p = new Point2D(currX, currY);
      if (drawPane.grid) {
        int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
        int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
        x += Pane.gridSize / 2;
        y += Pane.gridSize / 2;
        if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
          p = new Point2D(x, y);
        }
      }
      (new NewShape()).newShape(this, p);
      Frame.this.requestFocus();
    });
    addShape.setEnabled(gameMode == 0);
    edit.add(addShape);

    /*
    edit.addSeparator();

    JMenuItem mBB = new JMenuItem("Manual Bounding Box");
    mBB.addActionListener(e -> {
      if(mbbframe == null) {
        mbbframe = new MBBFrame(Frame.this);
      } else {
        mbbframe.toFront();
      }
    });
    edit.add(mBB);
     */

    edit.addSeparator();

    JMenu histSizeMenu = new JMenu("Steps to keep saved in history");
    ButtonGroup histGroup = new ButtonGroup();

    JRadioButtonMenuItem noHist = new JRadioButtonMenuItem("Do not save history");
    noHist.addActionListener(e -> {
      drawPane.maxHistory = 0;
      this.updateHistory();
    });
    histGroup.add(noHist);
    histSizeMenu.add(noHist);

    JRadioButtonMenuItem hist10 = new JRadioButtonMenuItem("Keep the last 10 steps");
    hist10.addActionListener(e -> {
      drawPane.maxHistory = 10;
      this.updateHistory();
    });
    histGroup.add(hist10);
    histSizeMenu.add(hist10);

    JRadioButtonMenuItem hist50 = new JRadioButtonMenuItem("Keep the last 50 steps");
    hist50.addActionListener(e -> {
      drawPane.maxHistory = 50;
      this.updateHistory();
    });
    histGroup.add(hist50);
    histSizeMenu.add(hist50);

    JRadioButtonMenuItem hist100 = new JRadioButtonMenuItem("Keep the last 100 steps");
    hist100.addActionListener(e -> {
      drawPane.maxHistory = 100;
      this.updateHistory();
    });
    histGroup.add(hist100);
    histSizeMenu.add(hist100);

    JRadioButtonMenuItem hist500 = new JRadioButtonMenuItem("Keep the last 500 steps");
    hist500.addActionListener(e -> {
      drawPane.maxHistory = 500;
      this.updateHistory();
    });
    histGroup.add(hist500);
    histSizeMenu.add(hist500);

    JRadioButtonMenuItem hist1000 = new JRadioButtonMenuItem("Keep the last 1000 steps");
    hist1000.addActionListener(e -> {
      drawPane.maxHistory = 1000;
      this.updateHistory();
    });
    histGroup.add(hist1000);
    histSizeMenu.add(hist1000);

    if (drawPane.maxHistory == 0) {
      noHist.setSelected(true);
    } else if (drawPane.maxHistory == 10) {
      hist10.setSelected(true);
    } else if (drawPane.maxHistory == 50) {
      hist50.setSelected(true);
    } else if (drawPane.maxHistory == 100) {
      hist100.setSelected(true);
    } else if (drawPane.maxHistory == 500) {
      hist500.setSelected(true);
    } else if (drawPane.maxHistory == 1000) {
      hist1000.setSelected(true);
    }

    edit.add(histSizeMenu);

    JMenuItem delHist = new JMenuItem("Delete history");
    delHist.addActionListener(e -> deleteHistory());
    edit.add(delHist);

    menu.add(edit);

    if(gameMode == 0) {
      JMenu layout = new JMenu("Layout");

      JMenuItem autoLayout = new JMenuItem("Apply automatic layout");
      autoLayout.addActionListener(e -> {
        //Close the edit frame before layouting
        if(this.eframe != null) {
          this.eframe.setVisible(false);
          this.derefEframe();
        }

        this.saveForUndo();

        try {
          QgrafDiagram qgdg = new QgrafDiagram(drawPane.vertices, drawPane.lines, drawPane.getDiagramLayoutArea());
          setDiagram(qgdg, CLEARMODE_CLEAR_LINES, false, true, true, false);
        } catch (FeynGameDiagramStructureException ex) {
          JOptionPane.showMessageDialog(this, "Cannot auto-layout: " + ex.getMessage(), "Diagram error", JOptionPane.ERROR_MESSAGE);
        }
      });
      if (!macOs) {
        autoLayout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
      } else {
        autoLayout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.META_DOWN_MASK));
      }
      layout.add(autoLayout);

      JMenuItem moveLayout = new JMenuItem("Move diagram to bounds");
      moveLayout.addActionListener(e -> {
        Rectangle oldBoundsRect = drawPane.getBoundsPainted();
        Box2D oldBounds = new Box2D(oldBoundsRect.x, oldBoundsRect.y, oldBoundsRect.width, oldBoundsRect.height);
        Box2D newBounds = drawPane.getDiagramLayoutArea();
        for(Vertex v : drawPane.vertices) {
          org.jgrapht.alg.drawing.model.Point2D vo = new org.jgrapht.alg.drawing.model.Point2D(v.origin.x, v.origin.y);
          org.jgrapht.alg.drawing.model.Point2D p = GraphLayoutAlgorithm.scaledTranslate(vo, oldBounds, newBounds);
          v.origin = new Point2D(p.getX(), p.getY());
        }
        for(FeynLine fl : drawPane.lines) {
          org.jgrapht.alg.drawing.model.Point2D so = new org.jgrapht.alg.drawing.model.Point2D(fl.getStartX(), fl.getStartY());
          org.jgrapht.alg.drawing.model.Point2D sn = GraphLayoutAlgorithm.scaledTranslate(so, oldBounds, newBounds);
          fl.setStart(new Point2D(sn.getX(), sn.getY()));
          org.jgrapht.alg.drawing.model.Point2D eo = new org.jgrapht.alg.drawing.model.Point2D(fl.getEndX(), fl.getEndY());
          org.jgrapht.alg.drawing.model.Point2D en = GraphLayoutAlgorithm.scaledTranslate(eo, oldBounds, newBounds);
          fl.setEnd(new Point2D(en.getX(), en.getY()));
        }
      });
      layout.add(moveLayout);

      layout.addSeparator();

      showBoundingBox = new JCheckBoxMenuItem("Show bounding box", drawPane.mBB);
      showBoundingBox.addActionListener(e -> {
        drawPane.mBB = showBoundingBox.isSelected();
        if(drawPane.mBB && (drawPane.wMBB == 0 || drawPane.hMBB == 0))
          drawPane.calculateAutoMBBSize();
        if(mbbFrame != null) mbbFrame.updateMBBsize();
        repaint();
      });
      layout.add(showBoundingBox);

      JMenuItem configBoundingBox = new JMenuItem("Configure bounding box");
      configBoundingBox.addActionListener(e -> {
        if(mbbFrame != null) {
          mbbFrame.setVisible(true);
          mbbFrame.toFront();
          mbbFrame.requestFocusInWindow();
        }
      });
      layout.add(configBoundingBox);
      layout.addSeparator();


      JMenu externalLegOrder = new JMenu("External leg order");
      JRadioButtonMenuItem swapLegs = new JRadioButtonMenuItem("Adjust leg order");
      JRadioButtonMenuItem keepLegs = new JRadioButtonMenuItem("Keep leg order (old)");
      JRadioButtonMenuItem outLegs = new JRadioButtonMenuItem("Keep leg order");
      externalLegOrder.add(swapLegs);
      externalLegOrder.add(keepLegs);
      externalLegOrder.add(outLegs);
      layout.add(externalLegOrder);

      legOrderButtonGroup.add(swapLegs);
      legOrderButtonGroup.add(keepLegs);
      legOrderButtonGroup.add(outLegs);

      keepLegs.setVisible(false); //TODO decide to keep or remove it

      int v = GraphLayoutAlgorithm.getSelectedLayoutAlgorithm();
      if(v == 0) keepLegs.setSelected(true);
      else if (v == 1 || v == 2) swapLegs.setSelected(true);
      else if (v == 3 || v == 4) outLegs.setSelected(true);
      swapLegs.addActionListener(e -> GraphLayoutAlgorithm.setSelectedLayoutAlgorthm(1)); //TODO 2?
      keepLegs.addActionListener(e -> GraphLayoutAlgorithm.setSelectedLayoutAlgorthm(0));
      outLegs. addActionListener(e -> GraphLayoutAlgorithm.setSelectedLayoutAlgorthm(3)); //TODO 4?

      gridAutoLayout = new JCheckBoxMenuItem("Auto-layout aligns to grid");
      gridAutoLayout.setSelected(GraphLayoutAlgorithm.gridAutoLayout());
      gridAutoLayout.addActionListener(e -> GraphLayoutAlgorithm.gridAutoLayout(gridAutoLayout.isSelected()));
      layout.add(gridAutoLayout);

      layout.add(layoutPresetsSubMenu);

      menu.add(layout);


      JMenu model = new JMenu("Model");

      JMenuItem loadDefaultConfig = new JMenuItem("Load default model");
      loadDefaultConfig.addActionListener(e -> this.loadDefaultConfig());
      loadDefaultConfig.setEnabled(gameMode == 0);
      model.add(loadDefaultConfig);

      JMenuItem setDefaultConfig = new JMenuItem("Set current model as default");
      setDefaultConfig.addActionListener(e -> this.setDefaultConfig());
      setDefaultConfig.setEnabled(gameMode == 0);
      model.add(setDefaultConfig);

      model.addSeparator();

      JMenuItem loadConfig = new JMenuItem("Load model file");
      loadConfig.addActionListener(e -> this.loadNewConfig());
      loadConfig.setEnabled(gameMode == 0);
      model.add(loadConfig);

      JMenuItem saveConfig = new JMenuItem("Save model file");
      saveConfig.addActionListener(e -> this.saveConfig());
      model.add(saveConfig);

      JMenuItem saveConfigAs = new JMenuItem("Save model file as");
      saveConfigAs.addActionListener(e -> this.saveAsConfig());
      model.add(saveConfigAs);

      JMenuItem mergeConfig = new JMenuItem("Merge models");
      mergeConfig.addActionListener(e -> new MergePresets(this));
      mergeConfig.setEnabled(gameMode == 0);
      model.add(mergeConfig);

      model.addSeparator();

      JMenuItem convPaneToModel = new JMenuItem("Convert canvas to model");
      convPaneToModel.addActionListener(e -> {

        String name = "";
        if (this.currentFile != null)
          name = this.currentFile;
        if (name.endsWith(".fg"))
          name = name.substring(0, name.length() - 3);

        if (name != "")
          name += ".model";
        tileTabs.addModel(new Presets(drawPane), name);
      });
      convPaneToModel.setEnabled(gameMode == 0);
      model.add(convPaneToModel);

      JMenuItem applyModelToCanvas = new JMenuItem("Apply model to canvas");
      setDefaultConfig.setEnabled(gameMode == 0);
      applyModelToCanvas.addActionListener(e -> {
        forceLineStyles(LineConfig.presets);
      });
      model.add(applyModelToCanvas);

      qgrafUpdateModelExt = new JMenuItem("Apply model to all diagrams");
      qgrafUpdateModelExt.addActionListener(e -> {
        forceLineStyles(LineConfig.presets);
        modelForAllQgrafLoads = Java8Support.listCopyOf(LineConfig.presets);
      });
      qgrafUpdateModelExt.setEnabled(false);

      model.add(qgrafUpdateModelExt);

      model.addSeparator();

      JMenuItem importQgrafModel = new JMenuItem("Import qgraf model");
      importQgrafModel.addActionListener(e -> QgrafImportManager.doGuiModelLoad(this));

      model.add(importQgrafModel);

      menu.add(model);
    } else {
      gridAutoLayout = null;
      showBoundingBox = null;
      qgrafUpdateModelExt = null;
    }

    JMenu view = new JMenu("View");

    JMenuItem checkLines = new JMenuItem("Check for errors");
    checkLines.addActionListener(e -> this.checkLines());
    checkLines.setAccelerator(KeyStroke.getKeyStroke("F"));
    view.add(checkLines);

    JMenuItem editScreen = new JMenuItem("Open/Close EditFrame");
    editScreen.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      if (eframe != null && !eframe.isVisible()) {
        t = new Toast("Show EditFrame", this.drawPane);
        eframe.setVisible(true);
        eframe.editPane.upEditPane();
        eframe.repaint();
        eframe.requestFocusInWindow();
      } else if(eframe != null && eframe.isVisible()) {
        t = new Toast("Hide EditFrame", this.drawPane);
        eframe.setVisible(false);
        Frame.this.requestFocusInWindow();
      }
      //t.showtoast();
    });
    editScreen.setEnabled(gameMode == 0);
    editScreen.setAccelerator(KeyStroke.getKeyStroke("E"));
    view.add(editScreen);

    /*JMenu blankSubMenu = new JMenu("Blanks");

    JCheckBoxMenuItem autoBlanks = new JCheckBoxMenuItem("Automatically create blanks");
    autoBlanks.setState(FeynLine.autoBlanks);
    autoBlanks.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      if (!FeynLine.autoBlanks) {
        t = new Toast("Automatic blanks", this.drawPane);
        if (beframe != null) {
          beframe.dispose();
          beframe = null;
        }


        blankScreen.setEnabled(false);
      } else {
        t = new Toast("No automatic blanks", this.drawPane);
        blankScreen.setEnabled(true);

        for (FeynLine line : drawPane.lines) {
          line.blanks.clear();
          line.blanksUnsorted.clear();
        }
      }
      //t.showtoast();
      Frame.this.requestFocusInWindow();
      FeynLine.autoBlanks = autoBlanks.getState();
      Frame.this.repaint();
    });

    blankSubMenu.add(autoBlanks);

    blankScreen = new JMenuItem("Open/Close BlankFrame");
    blankScreen.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      if (beframe == null) {
        t = new Toast("Show BlankFrame", this.drawPane);
        beframe = new BlanksEdit(this);
      } else {
        t = new Toast("Hide BlankFrame", this.drawPane);
        beframe.dispose();
        beframe = null;
      }
      //t.showtoast();
      Frame.this.requestFocusInWindow();
    });
    blankScreen.setAccelerator(KeyStroke.getKeyStroke("B"));
    blankScreen.setEnabled(!FeynLine.autoBlanks);
    blankSubMenu.add(blankScreen);

    view.add(blankSubMenu);*/

    view.addSeparator();

    JCheckBoxMenuItem grid = new JCheckBoxMenuItem("Grid");
    grid.setAccelerator(KeyStroke.getKeyStroke("G"));
    grid.setState(drawPane.grid);
    grid.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      if (!this.drawPane.grid)
        t = new Toast("Show grid", this.drawPane);
      else
        t = new Toast("Hide grid", this.drawPane);
      //t.showtoast();
      this.drawPane.grid = grid.getState();
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    grid.setSelected(true);
    if (!drawPane.grid) {
      grid.setSelected(false);
    }
    view.add(grid);

    JMenuItem incGrid = new JMenuItem("Increase grid spacing");
    incGrid.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.ALT_DOWN_MASK));
    incGrid.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      t = new Toast("Increase grid spacing", this.drawPane);
      //t.showtoast();
      Pane.gridSize += 1;
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    view.add(incGrid);

    JMenuItem decGrid = new JMenuItem("Decrease grid spacing");
    decGrid.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.ALT_DOWN_MASK));
    decGrid.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      t = new Toast("Decrease grid spacing", this.drawPane);
      Pane.gridSize -= 1;
      if (Pane.gridSize < clipDistance) {
        Pane.gridSize = clipDistance;
        t = new Toast("Minimal grid spacing", this.drawPane);
      }
      //t.showtoast();
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    view.add(decGrid);

    JMenuItem incZoom = new JMenuItem("Zoom in");
    incZoom.setAccelerator(KeyStroke.getKeyStroke("UP"));
    incZoom.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      t = new Toast("Zoom in", this.drawPane);
      //t.showtoast();
      Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
      drawPane.scale *= 1.1;
      drawPane.zeroPoint = new Point2D(currX / drawPane.scale - mPosPane.x, currY / drawPane.scale - mPosPane.y);
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    incZoom.setEnabled(gameMode == 0);
    view.add(incZoom);

    JMenuItem decZoom = new JMenuItem("Zoom out");
    decZoom.setAccelerator(KeyStroke.getKeyStroke("DOWN"));
    decZoom.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      t = new Toast("Zoom in", this.drawPane);
      //t.showtoast();
      Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
      drawPane.scale /= 1.1;
      drawPane.zeroPoint = new Point2D(currX / drawPane.scale - mPosPane.x, currY / drawPane.scale - mPosPane.y);
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    decZoom.setEnabled(gameMode == 0);
    view.add(decZoom);

    JMenuItem resetZoom = new JMenuItem("Reset zoom");
    resetZoom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, InputEvent.CTRL_DOWN_MASK));
    if (macOs) resetZoom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, InputEvent.META_DOWN_MASK));
    resetZoom.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      t = new Toast("Reset zoom", this.drawPane);
      //t.showtoast();

      Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
      drawPane.scale = 1;
      drawPane.zeroPoint = new Point2D(currX / drawPane.scale - mPosPane.x, currY / drawPane.scale - mPosPane.y);
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    resetZoom.setEnabled(gameMode == 0);
    view.add(resetZoom);

    JMenuItem resetShift = new JMenuItem("Reset translation");
    resetShift.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, InputEvent.SHIFT_DOWN_MASK));
    resetShift.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      t = new Toast("Reset translation", this.drawPane);
      //t.showtoast();
      drawPane.zeroPoint = new Point2D(0, 0);
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    resetShift.setEnabled(gameMode == 0);
    view.add(resetShift);

    JCheckBoxMenuItem helperL = new JCheckBoxMenuItem("Helper lines");
    helperL.setAccelerator(KeyStroke.getKeyStroke("H"));
    helperL.setState(drawPane.helperLines);
    helperL.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      if (!this.drawPane.helperLines)
        t = new Toast("Show helper lines", this.drawPane);
      else
        t = new Toast("Hide helper lines", this.drawPane);
      //t.showtoast();
      this.drawPane.helperLines = helperL.getState();
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    helperL.setSelected(true);
    if (!drawPane.helperLines) {
      helperL.setSelected(false);
    }
    view.add(helperL);

    JCheckBoxMenuItem showCurrent = new JCheckBoxMenuItem("Highlight active object");
    showCurrent.setAccelerator(KeyStroke.getKeyStroke("A"));
    showCurrent.setState(drawPane.showCurrentSelectedLine);
    showCurrent.addActionListener(e -> {
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      if (!this.drawPane.showCurrentSelectedLine)
        t = new Toast("Highlight active object", this.drawPane);
      else
        t = new Toast("Do not highlight active object", this.drawPane);
      //t.showtoast();
      this.drawPane.showCurrentSelectedLine = showCurrent.getState();
      Frame.this.repaint();
      Frame.this.requestFocusInWindow();
    });
    showCurrent.setSelected(true);
    if (!drawPane.showCurrentSelectedLine) {
      showCurrent.setSelected(false);
    }
    view.add(showCurrent);

    JCheckBoxMenuItem toolTipMen = new JCheckBoxMenuItem("Show Tooltip");
    toolTipMen.setAccelerator(KeyStroke.getKeyStroke("T"));
    toolTipMen.setState(toolTip);
    toolTipMen.addActionListener(e -> {
      toolTip = toolTipMen.getState();
      Frame.this.requestFocusInWindow();
      Frame.this.repaint();
    });
    toolTipMen.setSelected(true);
    if (!toolTip) toolTipMen.setSelected(false);
    view.add(toolTipMen);

    menu.add(view);

    if(gameMode == 0) {
      qgrafMenu = new JMenu("qgraf");
      JMenuItem qgrafMenuChoose = new JMenuItem("Choose diagram");
      qgrafMenuChoose.addActionListener(e -> {
        if(currentQgrafFile == null) return;
        while(true) {
          String input = JOptionPane.showInputDialog(this, "Loaded file has " +
              currentQgrafFile.getDiagramCount() + " diagrams. Select index to load:");
          if(input == null) return; //Closed
          int index;
          try {
            index = Integer.parseInt(input);
          } catch(NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Not a valid number", "Error", JOptionPane.ERROR_MESSAGE);
            continue;
          }

          if(index <= 0 || index > currentQgrafFile.getDiagramCount()) {
            JOptionPane.showMessageDialog(this, "Not in range 1 - " + currentQgrafFile.getDiagramCount(),
                "Error", JOptionPane.ERROR_MESSAGE);
            continue;
          }

          QgrafDiagram dia = currentQgrafFile.getDiagram(index - 1);
          dia.clearPositionData();
          //Clear lines and history, but not the currently loaded file
          setDiagram(dia, CLEARMODE_CLEAR_LINES | CLEARMODE_CLEAR_HISTORY, true, true, true, !QgrafImportManager.loadMomentumArrows());
          if(modelForAllQgrafLoads != null) forceLineStyles(modelForAllQgrafLoads);
          this.currentQgrafFileIndex = index - 1;
          setTitle("qgraf imported: " + currentQgrafFile.getName() + " (diagram " + (currentQgrafFileIndex+1) + "/" + currentQgrafFile.getDiagramCount() + ")");
          break;
        }
      });

      JMenuItem qgrafMenuPrev = new JMenuItem("Show previous");
      qgrafMenuPrev.addActionListener(e -> {
        if(currentQgrafFileIndex == -1) {
          logger.severe("No file loaded");
        } else if(currentQgrafFileIndex == 0) {
          Toolkit.getDefaultToolkit().beep();
        } else {
          currentQgrafFileIndex -= 1;
          QgrafDiagram dia = currentQgrafFile.getDiagram(currentQgrafFileIndex);
          dia.clearPositionData();
          //Clear lines and history, but not the currently loaded file
          setDiagram(dia, CLEARMODE_CLEAR_LINES | CLEARMODE_CLEAR_HISTORY, true, true, true, !QgrafImportManager.loadMomentumArrows());
          if(modelForAllQgrafLoads != null) forceLineStyles(modelForAllQgrafLoads);
          setTitle("qgraf imported: " + currentQgrafFile.getName() + " (diagram " + (currentQgrafFileIndex+1) + "/" + currentQgrafFile.getDiagramCount() + ")");
        }
      });
      if (!macOs) {
        qgrafMenuPrev.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.CTRL_DOWN_MASK));
      } else {
        qgrafMenuPrev.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, InputEvent.META_DOWN_MASK));
      }

      JMenuItem qgrafMenuNext = new JMenuItem("Show next");
      qgrafMenuNext.addActionListener(e -> {
        if(currentQgrafFileIndex == -1) {
          logger.severe("No file loaded");
        } else if(currentQgrafFileIndex == currentQgrafFile.getDiagramCount() - 1) {
          Toolkit.getDefaultToolkit().beep();
        } else {
          currentQgrafFileIndex += 1;
          QgrafDiagram dia = currentQgrafFile.getDiagram(currentQgrafFileIndex);
          dia.clearPositionData();
          //Clear lines and history, but not the currently loaded file
          setDiagram(dia, CLEARMODE_CLEAR_LINES | CLEARMODE_CLEAR_HISTORY, true, true, true, !QgrafImportManager.loadMomentumArrows());
          if(modelForAllQgrafLoads != null) forceLineStyles(modelForAllQgrafLoads);
          setTitle("qgraf imported: " + currentQgrafFile.getName() + " (diagram " + (currentQgrafFileIndex+1) + "/" + currentQgrafFile.getDiagramCount() + ")");
        }
      });
      if (!macOs) {
        qgrafMenuNext.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.CTRL_DOWN_MASK));
      } else {
        qgrafMenuNext.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, InputEvent.META_DOWN_MASK));
      }

      JMenuItem qgrafMenuExport = new JMenuItem("Export all diagrams");
      qgrafMenuExport.addActionListener(e -> {
        //        boolean success = exportAllFrame.showDialog();
        ExportManager.doGuiExport(frame, true);
      });

      JMenuItem qgrafMenuSource = new JMenuItem("Show qgraf output");
      qgrafMenuSource.addActionListener(e -> {
        if(sourceFrame != null) {
          sourceFrame.setVisible(true);
          sourceFrame.toFront();
        }
      });

      JMenuItem qgrafMenuClose = new JMenuItem("Close (keep current)");
      qgrafMenuClose.addActionListener(e -> {
        clearCanvas(CLEARMODE_CLEAR_META);
      });

      JMenuItem qgrafUpdateModel = new JMenuItem("Apply model to all diagrams");
      qgrafUpdateModel.addActionListener(e -> {
        forceLineStyles(LineConfig.presets);
        modelForAllQgrafLoads = Java8Support.listCopyOf(LineConfig.presets);
      });

      qgrafMenu.add(qgrafMenuPrev);
      qgrafMenu.add(qgrafMenuNext);
      qgrafMenu.add(qgrafMenuChoose);
      qgrafMenu.addSeparator();
      qgrafMenu.add(qgrafUpdateModel);
      qgrafMenu.add(qgrafMenuExport);
      qgrafMenu.add(qgrafMenuSource);
      qgrafMenu.addSeparator();
      qgrafMenu.add(qgrafMenuClose);
      qgrafMenu.setVisible(false);
      menu.add(qgrafMenu);
    } else {
      qgrafMenu = null; //inFin mode
    }

    JMenu about = new JMenu("Help");

    JMenuItem helpPages = new JMenuItem("Show Help");
    helpPages.addActionListener(e -> {
      new HelpPages();
      Frame.this.requestFocusInWindow();
    });
    about.add(helpPages);

    //JMenuItem f1Screen = new JMenuItem("Show keyboard shortcuts");
    //f1Screen.addActionListener(e -> this.openF1());
    //f1Screen.setAccelerator(KeyStroke.getKeyStroke("S"));
    //about.add(f1Screen);

    JMenuItem aboutFeynGame = new JMenuItem("About FeynGame");
    aboutFeynGame.addActionListener(e -> {
      this.aboutFGDialog();
      Frame.this.requestFocusInWindow();
    });
    about.add(aboutFeynGame);

    JMenuItem showLogs = new JMenuItem("View Log");
    showLogs.addActionListener(e -> {
      this.showLogs();
      Frame.this.requestFocusInWindow();
    });
    about.add(showLogs);

    menu.add(about);

    this.setJMenuBar(menu);
    

    this.requestFocus();

    this.addMouseWheelListener(this);

    this.addKeyListener(this);

    this.addWindowListener(this);

    drawPane.addMouseListener(this);

    drawPane.addMouseMotionListener(this);

    this.setTransferHandler(new FrameTransferHandler());

    /* this is because when the window is resized, the lines drawn by
     * FeynGame for the challenge are not in the correct place anymore
     * and therefore we reload the challenge */
    this.addComponentListener(new ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        if (!resizePossible) return;
        // This is only called when the user releases the mouse button.
        resizeTiles();
        if (gameMode == 1 && challenge != null) { reloadChallenge(); }
        drawPaneSize = Frame.this.drawPane.getSize();
      }
    });
    this.drawPane.setPreferredSize(this.drawPaneSize);

    this.pack();
    this.setLocationRelativeTo(null);

    drawPane.setSize(drawPaneSizeCopy);
    this.pack();
    this.setLocationRelativeTo(null);

    this.setVisible(show);
    this.requestFocus();
    if (gameMode == 1) {
      /* start the timer to limit the time the player has to complete
       * challenges if timer = true */
      if (timer) {
        remTime = totTime;
        javax.swing.Timer timerrr = new javax.swing.Timer(1000, new timeTimerActionListener());
        timerrr.setRepeats(false);
        timerrr.start();
      }
    }
    drawPane.setSize(drawPaneSizeCopy);

//    this.getPreferences();

    resizePossible = true;
  }
  /**
   * when the maximum length of the history was changed, this deletes all the parts from history not needed anymore
   */
  private void updateHistory() {

    if (drawPane.undoListLines.size() > 0) {
      while (drawPane.undoListLines.size() > drawPane.maxHistory) {
        drawPane.undoListLines.remove(0);
      }
    }

    if (drawPane.undoListVertices.size() > 0) {
      while (drawPane.undoListVertices.size() > drawPane.maxHistory) {
        drawPane.undoListVertices.remove(0);
      }
    }

    if (drawPane.undoListfObjects.size() > 0) {
      while (drawPane.undoListfObjects.size() > drawPane.maxHistory) {
        drawPane.undoListfObjects.remove(0);
      }
    }

    if (drawPane.undoListShapes.size() > 0) {
      while (drawPane.undoListShapes.size() > drawPane.maxHistory) {
        drawPane.undoListShapes.remove(0);
      }
    }

    if (drawPane.redoListLines.size() > 0) {
      while (drawPane.redoListLines.size() > drawPane.maxHistory) {
        drawPane.redoListLines.remove(0);
      }
    }

    if (drawPane.redoListVertices.size() > 0) {
      while (drawPane.redoListVertices.size() > drawPane.maxHistory) {
        drawPane.redoListVertices.remove(0);
      }
    }

    if (drawPane.redoListfObjects.size() > 0) {
      while (drawPane.redoListfObjects.size() > drawPane.maxHistory) {
        drawPane.redoListfObjects.remove(0);
      }
    }

    if (drawPane.redoListShapes.size() > 0) {
      while (drawPane.redoListShapes.size() > drawPane.maxHistory) {
        drawPane.redoListShapes.remove(0);
      }
    }

  }

  /**
   * Make provided image transparent wherever color matches the provided color.
   *
   * @param im BufferedImage whose color will be made transparent.
   * @return Image with transparency applied.
   */
  public static Image makeColorTransparent(final BufferedImage im) {
    final ImageFilter filter = new RGBImageFilter() {
      // the color we are looking for (white)... Alpha bits are set to opaque
      public final int markerRGB = 0xFFFFFFFF;

      public final int filterRGB(final int x, final int y, final int rgb) {
        Color col = new Color(rgb, true);
        int brightness = col.getRed() + col.getGreen() + col.getBlue();
        if ((rgb | 0xFF000000) == markerRGB) {
          // if (brightness >= 750) {
          // Mark the alpha bits as zero - transparent
          return 0x00FFFFFF & rgb;
        } else {
          // nothing to do
          return rgb;
        }
      }
    };
    final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
    return Toolkit.getDefaultToolkit().createImage(ip);
  }

  /**
   * Getter function for {@link drawPane}
   *
   * @return {@link drawPane}
   */
  public Pane getDrawPane() {
    return this.drawPane;
  }

  public void forceLineStyles(List<LineConfig> configs) {
    Map<Map.Entry<String, String>, LineConfig> idToStyle = new HashMap<>();
    for(LineConfig lc : configs) {
      if(lc.identifier != null) {
        idToStyle.put(lc.identifier, lc);
      }
    }
    for(FeynLine fl : drawPane.lines) {
      Map.Entry<String, String> id = fl.lineConfig.identifier;
      LineConfig newStyle = idToStyle.get(id);
      if(newStyle != null) {
        fl.changeConfigTo(newStyle);
      }
    }
  }

  /**
   * Looks for any line in {@link ui.Pane#lines} that is closer than dist to the {@link Point2D} p and returns its index and the closest {@link Point2D} on the line.
   *
   * @param dist   the maximum distance allowed for any line
   * @param p      the location where the line should be
   * @param offset set to 1 if the last line in {@link ui.Pane#lines} (a.k.a. the current selected line) should be excluded from the search.
   * @return {@link Map.Entry} of type {@link Point2D} and {@link Integer}. The index of the closest line is given as second value and the closest location on the line to the {@link Point2D} p is given as first value. This {@link Point2D} can be one of the end points of the line.
   * @throws Exception if no {@link game.FeynLine} was found
   */
  private Map.Entry<Point2D, Integer> getNearLineIntersect(double dist, Point2D p, int offset) throws Exception {
    double tDist = -1;
    int index = -1, endPointIndex = -1;
    double ttDist = -1;
    Point2D endPoint = null, tIntersect = null;
    for (int i = 0; i < drawPane.lines.size() - offset; i++) {
      FeynLine pair = drawPane.lines.get(i);
      Line l = new Line(pair.getStartX(), pair.getStartY(), pair.getEndX(), pair.getEndY());
      Point2D tempIntersect = this.distPointShape(p, pair, clipDistance);

      if (tempIntersect != null && (tempIntersect.distance(p.x, p.y) < tDist || tDist < 0)) {
        tDist = tempIntersect.distance(p.x, p.y);
        tIntersect = tempIntersect;
        index = i;
      }
      if (p.distance(new Point2D(l.getStartX(), l.getStartY())) < ttDist || ttDist < 0) {
        ttDist = p.distance(new Point2D(l.getStartX(), l.getStartY()));
        endPoint = new Point2D(l.getStartX(), l.getStartY());
        endPointIndex = i;
      }
      if (p.distance(new Point2D(l.getEndX(), l.getEndY())) < ttDist || ttDist < 0) {
        ttDist = p.distance(new Point2D(l.getEndX(), l.getEndY()));
        endPoint = new Point2D(l.getEndX(), l.getEndY());
        endPointIndex = i;
      }

    }

    if (ttDist < dist && endPoint != null)
      return new AbstractMap.SimpleEntry<>(endPoint, endPointIndex);

    if (tDist < dist && index > -1)
      return new AbstractMap.SimpleEntry<>(tIntersect, index);

    throw new Exception("No nearby Point found");
  }

  /**
   * Cuts the {@link game.FeynLine} at position index in {@link ui.Pane#lines} at the {@link Point2D} location.
   * That means afterwards there is one more {@link game.FeynLine} in {@link ui.Pane#lines}.
   * The {@link game.LineConfig} is copied fro the original {@link game.FeynLine} and
   * {@link FeynLine#getHeight(double, Point2D, Point2D, Point)}
   * is called to get a line with in the same {@link game.FeynLine#radius} as the original {@link game.FeynLine}.
   *
   * @param index    index of the {@link game.FeynLine} to cut in {@link ui.Pane#lines}
   * @param location {@link Point2D} where to cut
   */
  private void cutLine(int index, Point2D location) {
    FeynLine l = drawPane.lines.get(index);
    boolean mom = (l.momArrow != null);
    if (l.isFixed())
      return;
    LineConfig lc = drawPane.lines.get(index).lineConfig;

    if (location.equals(new Point2D((float) l.getStartX(), (float) l.getStartY())) || location.equals(new Point2D((float) l.getEndX(), (float) l.getEndY())))
      return;

    Line l1 = new Line(l.getStartX(), l.getStartY(), location.x, location.y);
    Line l2 = new Line(location.x, location.y, l.getEndX(), l.getEndY());


    /* keep curvature radius const */
    double s1, s2;
    if (Math.abs(l.getHeight()) > .1) {
      if (l.getStart().equals(l.getEnd())) {
        l.center.x -= l.getStartX();
        l.center.y -= l.getStartY();
        double tmpX = l.center.x;
        l.center.x = tmpX * Math.cos( - l.circAngle) + l.center.y * Math.sin( - l.circAngle);
        l.center.y = tmpX * Math.sin( - l.circAngle) + l.center.y * Math.cos( - l.circAngle);
        l.center.x += l.getStartX();
        l.center.y += l.getStartY();
        // drawPane.vertices.add(new Vertex(new Point2D((double) l.center.x, (double) l.center.y)));
      }
      s1 = FeynLine.getHeight(l.radius, new Point2D((float) l1.getStartX(), (float) l1.getStartY()), new Point2D((float) l1.getEndX(), (float) l1.getEndY()), l.center);
      s2 = FeynLine.getHeight(l.radius, new Point2D((float) l2.getStartX(), (float) l2.getStartY()), new Point2D((float) l2.getEndX(), (float) l2.getEndY()), l.center);
    } else {
      s1 = 0;
      s2 = 0;
    }


    drawPane.lines.set(index, new FeynLine(l1, new LineConfig(lc), s1));
    drawPane.lines.get(index).showDesc = l.showDesc;
    drawPane.lines.get(index).descDis = l.descDis;
    drawPane.lines.get(index).partPlaceDesc = l.partPlaceDesc;
    drawPane.lines.get(index).rotDesc = l.rotDesc;
    drawPane.lines.get(index).descScale = l.descScale;
    drawPane.lines.add(index + 1, new FeynLine(l2, new LineConfig(lc), s2));
    drawPane.lines.get(index + 1).showDesc = l.showDesc;
    drawPane.lines.get(index + 1).descDis = l.descDis;
    drawPane.lines.get(index + 1).partPlaceDesc = l.partPlaceDesc;
    drawPane.lines.get(index + 1).rotDesc = l.rotDesc;
    drawPane.lines.get(index + 1).descScale = l.descScale;

    if (mom) {
      drawPane.lines.get(index).showMom();
      drawPane.lines.get(index).momArrow.copy(l.momArrow);
      drawPane.lines.get(index + 1).showMom();
      drawPane.lines.get(index + 1).momArrow.copy(l.momArrow);
    }
    drawPane.checkForVertex(new Point2D((int) Math.round(location.x), (int) Math.round(location.y)));

  }

  @Override
  public void setVisible(boolean b) {
    super.setVisible(b);
    if(b) GUI = true;
  }

  /**
   * Calculates the point on the {@link game.FeynLine} fl that is closest to {@link Point2D} p (and closer than distConstrain).
   * This function is used for curved {@link game.FeynLine}, when it is not so easy to calculate the location which p is closest to.
   * Here a lot angle manipulations is performed because most of the calculation is performed in polar coords.
   *
   * @param p             location from where to start looking
   * @param fl            {@link game.FeynLine} to check
   * @param distConstrain max. distance allowed (mainly {@link ui.Frame#clipDistance})
   * @return {@link Point2D} on {@link game.FeynLine} fl that is closest
   * @throws Exception if no point matches the conditions
   */
  @SuppressWarnings("SameParameterValue")
  private Point2D distPointShape(Point2D p, FeynLine fl, double distConstrain) throws Exception {

    if (p == null || fl == null) throw new Exception("false parameter passed");
    if (p.equals(new Point2D((int) Math.round(fl.getStartX()), (int) Math.round(fl.getStartY())))) return p;
    if (p.equals(new Point2D((int) Math.round(fl.getEndX()), (int) Math.round(fl.getEndY())))) return p;
    if (Math.abs(fl.getHeight()) < .1) {
      Line2D l = new Line2D.Double(fl.getStartX(), fl.getStartY(), fl.getEndX(), fl.getEndY());
      double dist = l.ptSegDist(new java.awt.geom.Point2D.Double(p.x, p.y));
      if (Math.abs(dist) < Math.abs(distConstrain)) {
        Point2D direction = new Point2D((l.getY1() - l.getY2()), -1 * (l.getX1() - l.getX2()));
        double length = direction.distance(new Point2D(0, 0));
        direction.x = (float) (direction.x / length);
        direction.y = (float) (direction.y / length);

        Point2D intersect1 = new Point2D(0, 0);
        intersect1.x = (float) (p.x + direction.x * dist);
        intersect1.y = (float) (p.y + direction.y * dist);

        Point2D intersect2 = new Point2D(0, 0);
        intersect2.x = (float) (p.x - direction.x * dist);
        intersect2.y = (float) (p.y - direction.y * dist);

        if (l.ptSegDist(new java.awt.geom.Point2D.Double(intersect1.x, intersect1.y)) < 1)
          return intersect1;
        if (l.ptSegDist(new java.awt.geom.Point2D.Double(intersect2.x, intersect2.y)) < 1)
          return intersect2;
      }
      return null;
    }

    if (fl.getStart().equals(fl.getEnd())) {
      Point2D center = new Point2D(fl.center.x, fl.center.y);
      center.x -= fl.getStartX();
      center.y -= fl.getStartY();
      double tmpX = center.x;
      center.x = (int) Math.round(center.x * Math.cos( - fl.circAngle) + center.y * Math.sin( - fl.circAngle));
      center.y = (int) Math.round(tmpX * Math.sin( - fl.circAngle) + center.y * Math.cos( - fl.circAngle));
      center.x += fl.getStartX();
      center.y += fl.getStartY();
      double dis = center.distance(p);
      dis = dis - Math.abs(fl.radius);
      if (Math.abs(dis) < Math.abs(distConstrain)) {
        double dirX = p.x - center.x;
        double dirY = p.y - center.y;
        double norm = Math.sqrt(dirX * dirX + dirY * dirY);
        dirX /= norm;
        dirY /= norm;
        p.x -= dirX * dis;
        p.y -= dirY * dis;
        return p;
      }
      return null;
    }

    /* Point p in polar koord */
    Point2D pPolar = fl.toPolar(p);

    double tmpArchFrom = fl.archStart;
    double tmpArchTo = fl.archEnd;
    double angleNow = pPolar.y;

    if (fl.getHeight() > .1) {
      double tttt = tmpArchFrom;
      tmpArchFrom = tmpArchTo;
      tmpArchTo = tttt;
    }


    while (tmpArchFrom >= 2 * Math.PI) tmpArchFrom -= 2 * Math.PI;
    while (tmpArchTo >= 2 * Math.PI) tmpArchTo -= 2 * Math.PI;
    while (angleNow >= 2 * Math.PI) angleNow -= 2 * Math.PI;
    while (tmpArchFrom < 0) tmpArchFrom += 2 * Math.PI;
    while (tmpArchTo < 0) tmpArchTo += 2 * Math.PI;
    while (angleNow < 0) angleNow += 2 * Math.PI;

    if (tmpArchFrom == tmpArchTo) {
      tmpArchFrom = 0;
      tmpArchTo = 2 * Math.PI;
    }

    if ((tmpArchFrom < tmpArchTo && tmpArchFrom < angleNow && angleNow < tmpArchTo) || (tmpArchFrom > tmpArchTo && (angleNow > tmpArchFrom || angleNow < tmpArchTo))) {
      double dist = Math.abs(Math.abs(fl.radius) - pPolar.x);
      if (Math.abs(dist) < Math.abs(distConstrain)) {
        return fl.toCart(new Point2D(Math.abs(fl.radius), pPolar.y));
      }
    }

    return null;
  }

  /**
   * This function removes a {@link game.FeynLine} from the {@link ui.Pane#lines}, using {@link #combineLines(Point2D)}.
   *
   * @param index the {@link game.FeynLine} with this index in {@link ui.Pane#lines} is removed
   */
  public void removeLine(int index) {
    if (drawPane.lines.size() <= index || index < 0) return;

    Point2D p1, p2;
    p1 = drawPane.lines.get(index).getStart();
    p2 = drawPane.lines.get(index).getEnd();
    if (!drawPane.lines.get(index).isFixed()) {
      drawPane.lines.remove(index);

      combineLines(p1);
      combineLines(p2);
    }
  }

  /**
   * {@link ui.Pane#lines} are checked whether two {@link game.FeynLine} can be combined into one, at {@link Point.Double} p.
   *
   * @param p1 the {@link Point.Double} to check for any possible recombination
   */
  public void combineLines(Point2D p1) {
    ArrayList<Integer> list1 = new ArrayList<>();

    for (FeynLine line : drawPane.inFinLines) {
      if (line.getEnd().distance(p1) < 2)
        return;
      if (line.getStart().distance(p1) < 2)
        return;
    }

    for (Vertex v : drawPane.vertices) {
      if (!v.autoremove && v.origin.distance(p1) < 2)
        return;
    }

    for (int i = 0; i < drawPane.lines.size(); i++) {

      if ((int) Math.round(drawPane.lines.get(i).getStartX()) == (int) Math.round(p1.x) && (int) Math.round(drawPane.lines.get(i).getStartY()) == (int) Math.round(p1.y)) {
        if (drawPane.lines.get(i).isFixed())
          return;
        list1.add(i);
      } else if ((int) Math.round(drawPane.lines.get(i).getEndX()) == (int) Math.round(p1.x) && (int) Math.round(drawPane.lines.get(i).getEndY()) == (int) Math.round(p1.y)) {
        if (drawPane.lines.get(i).isFixed())
          return;
        list1.add(i);
      }
    }

    ArrayList<Map.Entry<Integer, Integer>> found = new ArrayList<>();

    if (list1.size() == 2) { // if the vertex has not exactly 2 lines attached, it cannot work
      for (int i = 0; i < list1.size(); i++) {
        for (int j = i + 1; j < list1.size(); j++)
          if (drawPane.lines.get(list1.get(i)).lineConfig.equals(drawPane.lines.get(list1.get(j)).lineConfig)) {
            if (drawPane.lines.get(list1.get(i)).height != 0 || drawPane.lines.get(list1.get(j)).height != 0) {
              if (Math.abs(drawPane.lines.get(list1.get(i)).radius - drawPane.lines.get(list1.get(j)).radius) < 1) {
                if (drawPane.lines.get(list1.get(i)).center.distance(drawPane.lines.get(list1.get(j)).center) < 0.02*Math.abs(drawPane.lines.get(list1.get(j)).radius))
                  found.add(new AbstractMap.SimpleEntry<>(list1.get(i), list1.get(j)));
              } else if (drawPane.lines.get(list1.get(i)).lineConfig.lineType == LineType.FERMION && !drawPane.lines.get(list1.get(i)).lineConfig.showArrow) {// }
                if (Math.abs(drawPane.lines.get(list1.get(i)).radius + drawPane.lines.get(list1.get(j)).radius) < 1) {
                  if (drawPane.lines.get(list1.get(i)).center.distance(drawPane.lines.get(list1.get(j)).center) < 0.02*Math.abs(drawPane.lines.get(list1.get(j)).radius)) {
                    Point2D temp = new Point2D(drawPane.lines.get(list1.get(i)).getStart());
                    drawPane.lines.get(list1.get(i)).setStart(drawPane.lines.get(list1.get(i)).getEnd());
                    drawPane.lines.get(list1.get(i)).setEnd(temp);
                    found.add(new AbstractMap.SimpleEntry<>(list1.get(i), list1.get(j)));
                  }
                }
              }
            } else {
              Point2D anf1 = drawPane.lines.get(list1.get(i)).getStart();
              Point2D end1 = drawPane.lines.get(list1.get(i)).getEnd();
              Point2D anf2 = drawPane.lines.get(list1.get(j)).getStart();
              Point2D end2 = drawPane.lines.get(list1.get(j)).getEnd();

              double n1X = (anf1.x - end1.x) / Math.sqrt((anf1.x - end1.x) * (anf1.x - end1.x) + (anf1.y - end1.y) * (anf1.y - end1.y));
              double n2X = (anf2.x - end2.x) / Math.sqrt((anf2.x - end2.x) * (anf2.x - end2.x) + (anf2.y - end2.y) * (anf2.y - end2.y));

              double n1Y = (anf1.y - end1.y) / Math.sqrt((anf1.x - end1.x) * (anf1.x - end1.x) + (anf1.y - end1.y) * (anf1.y - end1.y));
              double n2Y = (anf2.y - end2.y) / Math.sqrt((anf2.x - end2.x) * (anf2.x - end2.x) + (anf2.y - end2.y) * (anf2.y - end2.y));

              if (drawPane.lines.get(list1.get(i)).lineConfig.showArrow) {
                if (Math.abs(n1X - n2X) < .1 && Math.abs(n1Y - n2Y) < .1) {
                  found.add(new AbstractMap.SimpleEntry<>(list1.get(i), list1.get(j)));
                }
              }

              if(!drawPane.lines.get(list1.get(i)).lineConfig.showArrow) {
                if ((Math.abs(n1X - n2X) < .1 && Math.abs(n1Y - n2Y) < .1) || (Math.abs(n1X + n2X) < .1 && Math.abs(n1Y + n2Y) < .1)) {
                  if (Math.abs(n1X + n2X) < .1) {
                    Point2D temp = new Point2D(drawPane.lines.get(list1.get(i)).getStart());
                    drawPane.lines.get(list1.get(i)).setStart(drawPane.lines.get(list1.get(i)).getEnd());
                    drawPane.lines.get(list1.get(i)).setEnd(temp);
                  }
                  found.add(new AbstractMap.SimpleEntry<>(list1.get(i), list1.get(j)));
                }
              }
            }
          }
      }
    } else {
      return;
    }

    if (found.isEmpty()) return;

    for (Map.Entry<Integer, Integer> p : found) {
      int i = p.getKey();
      int j = p.getValue();
      FeynLine l1 = drawPane.lines.get(i);
      FeynLine l2 = drawPane.lines.get(j);
      l1.calculateArch();

      double height = 0;
      Point2D origin = l1.center;

      if (l1.getStart().equals(l2.getEnd()) && !l1.getEnd().equals(l2.getStart())) {
        if (l2.height != 0) {
          height = FeynLine.getHeight(l2.radius, l2.getStart(), l1.getEnd(), l2.center);
        }
        drawPane.lines.add(new FeynLine(l2.getStart(), l1.getEnd(), l1.lineConfig, height));
      } else if (l1.getEnd().equals(l2.getStart()) && !l1.getStart().equals(l2.getEnd())) {
        if (l2.height != 0) {
          height = FeynLine.getHeight(l2.radius, l1.getStart(), l2.getEnd(), l2.center);
        }
        drawPane.lines.add(new FeynLine(l1.getStart(), l2.getEnd(), l1.lineConfig, height));
      } else {
        if (l2.height != 0) {
          height = 2 * l2.radius * Math.signum(l2.height);
        }
        if (l2.getStart().equals(p1)) {
          drawPane.lines.add(new FeynLine(l1.getStart(), l2.getEnd(), l1.lineConfig, height));
        } else {
          drawPane.lines.add(new FeynLine(l2.getStart(), l1.getEnd(), l1.lineConfig, height));
        }
        if (l2.height != 0) {
          FeynLine fl11 = drawPane.lines.get(drawPane.lines.size() - 1);
          Point2D start = drawPane.lines.get(drawPane.lines.size() - 1).getStart();
          double angle = 0;
          Point2D center2 = new Point2D((double) l2.center.x, (double) l2.center.y);

          angle = - Math.atan2(start.y - center2.y, start.x - center2.x);
          angle -= Math.PI / 2;
          angle *= -1;

          if (l2.height < 0 && fl11.height > 0) {
            fl11.height *= -1;
            angle += Math.PI;
          }
          while (angle < 0)
            angle += 2 * Math.PI;
          while (angle > Math.PI*2)
            angle -= Math.PI * 2;

          drawPane.lines.get(drawPane.lines.size() - 1).circAngle = angle;
        }
      }

      FeynLine nL = drawPane.lines.get(drawPane.lines.size() - 1);

      boolean debugDesc = false;

      if (l1.showDesc && !l2.showDesc) {
        nL.showDesc = l1.showDesc;
        nL.rotDesc = l1.rotDesc;
        nL.partPlaceDesc = l1.partPlaceDesc;
        nL.descDis = l1.descDis;
        nL.descScale = l1.descScale;
        Frame.logger.finest("One line had a label");
      } else if (!l1.showDesc && l2.showDesc) {
        nL.showDesc = l2.showDesc;
        nL.rotDesc = l2.rotDesc;
        nL.partPlaceDesc = l2.partPlaceDesc;
        nL.descDis = l2.descDis;
        nL.descScale = l2.descScale;
        Frame.logger.finest("One line had a label");
      } else if (l1.showDesc && l2.showDesc && l1.lineConfig.description.equals(l2.lineConfig.description)) {
        nL.showDesc = true;
        nL.rotDesc = (l2.rotDesc + l1.rotDesc) / 2;
        nL.partPlaceDesc = (l2.partPlaceDesc + l1.partPlaceDesc) / 2;
        nL.descDis = (l2.descDis + l1.descDis) / 2;
        nL.descScale = (l2.descScale + l1.descScale) / 2;
        Frame.logger.finest("Both lines had the same label");
      } else if (l1.showDesc && l2.showDesc) {
        nL.showDesc = true;
        nL.rotDesc = (l2.rotDesc + l1.rotDesc) / 2;
        nL.partPlaceDesc = (l2.partPlaceDesc + l1.partPlaceDesc) / 2;
        nL.descDis = (l2.descDis + l1.descDis) / 2;
        nL.descScale = (l2.descScale + l1.descScale) / 2;

        Frame.logger.finest("The lines had the different label");

        String desc1 = l1.lineConfig.description;
        String desc2 = l2.lineConfig.description;
        boolean hasHTML = desc1.contains("<html>") || desc2.contains("<html>");
        desc1 = desc1.replaceAll("<html>", "");
        desc2 = desc2.replaceAll("<html>", "");
        if (hasHTML)
          nL.lineConfig.description = "<html>" + desc1 + " &ensp " + desc2;
        else
          nL.lineConfig.description = desc1 + " \\; " + desc2;
      } else {
        nL.showDesc = false;
        Frame.logger.finest("Neither of the lines had a label");
      }

      if (l1.isMom() && !l2.isMom()) {
        nL.showMom();
        nL.momArrow.copy(l1.momArrow);
      } else if (!l1.isMom() && l2.isMom()) {
        nL.showMom();
        nL.momArrow.copy(l2.momArrow);
      } else if (l1.isMom() && l2.isMom()) {
        nL.showMom();
        nL.momArrow.combine(l1.momArrow, l2.momArrow);
      } else {
        nL.hideMom();
      }

      for (int k = drawPane.lines.size() - 1; k >= 0; k--) {
        if (drawPane.lines.get(k) == l1 || drawPane.lines.get(k) == l2) {
          drawPane.lines.remove(k);
        }
      }
      drawPane.checkForVertex(new Point2D((int) Math.round(p1.x), (int) Math.round(p1.y)));
    }
    if (drawPane.lines.size() >= 2)
      Collections.swap(drawPane.lines, drawPane.lines.size() - 1, drawPane.lines.size() -2);

    Frame.this.repaint();
    if (this.eframe != null) {
      this.eframe.editPane.upEditPane();
      this.eframe.repaint();
    }
    if (this.beframe != null)
      beframe.update();
  }

  private Map.Entry<Point2D, Integer> clipPoint(Point2D p) {
    return clipPoint(p, null, -1);
  }

  private Map.Entry<Point2D, Integer> clipPoint(Point2D p, FeynLine fl) {
    return clipPoint(p, fl, -1);
  }

  private Map.Entry<Point2D, Integer> clipPoint(Point2D p, FeynLine fl, int dMode) {
    return clipPoint(p, fl, dMode, drawPane.grid);
  }

  private Map.Entry<Point2D, Integer> clipPoint(Point2D p, FeynLine fl, int dMode, boolean grid) {
    return clipPoint(p, fl, dMode, grid, clipDistance);
  }

  private Map.Entry<Point2D, Integer> clipPoint(Point2D p, FeynLine fl, int dMode, boolean grid, int clipDis) {
    return clipPoint(p, fl, dMode, grid, clipDis, (fl != null) ? 1 : 0);
  }

  private Map.Entry<Point2D, Integer> clipPoint(Point2D p, FeynLine fl, int dMode, boolean grid, int clipDis, int offset) {
    return clipPoint(p, fl, dMode, grid, clipDis, offset, false);
  }

  private Map.Entry<Point2D, Integer> clipPoint(Point2D p, FeynLine fl, int dMode, boolean grid, int clipDis, int offset, boolean ignoreLastVertex) {
    return clipPoint(p, fl, dMode, grid, clipDis, offset, ignoreLastVertex, drawPane.angles);
  }

  /**
   * This function looks for any possible clipping (snapping) that could be done to at the {@link Point2D} p.
   * <ul><li>If {@link ui.Pane#grid}, it returns a location on the grid.</li>
   * <li>If there are any other {@link ui.Pane#lines}, it checks for any close {@link game.FeynLine} with {@link #getNearLineIntersect(double, Point2D, int)}</li>
   * <li>If start or end {@link Point2D} of {@link game.FeynLine} fl is close to {@link Point2D} p, it will will return those.</li>
   * <li>Otherwise it just return {@link Point2D} p.</li></ul>
   *
   * @param p                the {@link Point2D} to check
   * @param fl               the {@link FeynLine}'s end points are checked for alignment (standard: {@code null}).
   * @param dMode            can be (standard: -1):<ul><li><b>-1</b> not checking the end or start point of fl</li><li><b>0</b> not checking the start point of fl</li><li><b>1</b> not checking the end point of fl</li></ul>
   * @param grid             wether or not it should clip to the grid (standard: {@link Pane#grid})
   * @param clipDis          the range on which is clipped
   * @param offset           set to 1 if the last line in {@link Pane#lines} (a.k.a. the current selected line) should be excluded from the search.
   * @param ignoreLastVertex set True if the clipping should exclude the last vertex
   * @param clipToAngle whether angle clipping is activated
   * @return {@link Map.Entry} of {@link Point2D} and {@link Integer}. The second value being the index of the {@link game.FeynLine} +1 or the negative Index of {@link game.Vertex} -1 in {@link ui.Pane#lines} and the first value being the location it clipped to.
   * If no {@link game.FeynLine} or {@link game.Vertex} was found, the index is 0.
   */
  @SuppressWarnings("ConstantConditions")
  private Map.Entry<Point2D, Integer> clipPoint(Point2D p, FeynLine fl, int dMode, boolean grid, int clipDis, int offset, boolean ignoreLastVertex, ArrayList<Double> clipToAngle) {

    boolean debug = false;

    Frame.logger.finest(p + " " + (fl != null) + " " + dMode);

    if (drawPane.shapes.size() > 0) {
      for (Shaped shape : drawPane.shapes) {
        if (!drawPane.grid) {
          Map.Entry<Point2D, Boolean> clipPoint = shape.nPoint(p, clipDis);
          if (clipPoint.getValue())
            return new AbstractMap.SimpleEntry<Point2D, Integer>(clipPoint.getKey(), 0);
        } else {
          Map.Entry<Point2D, Boolean> clipPoint = shape.nPoint(p, clipDis, Pane.gridSize);
          if (clipPoint.getValue())
            return new AbstractMap.SimpleEntry<Point2D, Integer>(clipPoint.getKey(), 0);
        }
      }
    }

    if (drawPane.inFinLines.size() > 0) {
      for (FeynLine line : drawPane.inFinLines) {
        if (line.getStart().distance(p) < clipDis) {
          return new AbstractMap.SimpleEntry<>(line.getStart(), 0);
        }
        if (line.getEnd().distance(p) < clipDis) {
          return new AbstractMap.SimpleEntry<>(line.getEnd(), 0);
        }
      }
    }

    if (drawPane.vertices.size() > 0) {
      try {
        int i = 0;
        ArrayList<Integer> indices = new ArrayList<>();
        int off = 0;
        if (ignoreLastVertex) off = 1;
        for (int j = 0; j < drawPane.vertices.size() - off; j++) {
          Vertex vx = drawPane.vertices.get(j);
          if ((p.x - vx.origin.x) * (p.x - vx.origin.x) + (p.y - vx.origin.y) * (p.y - vx.origin.y) <= 100) {
            indices.add(i);
            break;
          }
          i++;
        }
        if (indices.size() > 0) {
          int index = indices.get(0);
          return new AbstractMap.SimpleEntry<>(drawPane.vertices.get(index).origin, -index - 1);
        }
      } catch (Exception ex) {
        Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
      }
    }
    if (drawPane.lines.size() >= 1) {
      try {
        Frame.logger.finest("0");
        Map.Entry<Point2D, Integer> clipPoint = getNearLineIntersect(clipDis, new Point2D(p.x, p.y), offset);
        if (!grid) {
          clipPoint.setValue(clipPoint.getValue() + 1);
          int indexx = clipPoint.getValue() - 1;
          if (drawPane.lines.get(indexx).isFixed()) {
            Point2D intersect = clipPoint.getKey();
            if (drawPane.lines.get(indexx).fixStart) {
              intersect = new Point2D(drawPane.lines.get(indexx).getEndX(), drawPane.lines.get(indexx).getEndY());
            } else {
              intersect = new Point2D(drawPane.lines.get(indexx).getStartX(), drawPane.lines.get(indexx).getStartY());
            }
            clipPoint = new AbstractMap.SimpleEntry<>(intersect, indexx + 1);
          }
          return clipPoint;
        } else {
          int index = clipPoint.getValue();
          Point2D intersect = clipPoint.getKey();

          FeynLine fl1 = drawPane.lines.get(index);
          if (Math.abs(fl1.height) < .1) {
            double lenFl = Math.sqrt((fl1.getStart().x - fl1.getEnd().x) * (fl1.getStart().x - fl1.getEnd().x) + (fl1.getStart().y - fl1.getEnd().y) * (fl1.getStart().y - fl1.getEnd().y));
            double disToStart = Math.sqrt((fl1.getStart().x - intersect.x) * (fl1.getStart().x - intersect.x) + (fl1.getStart().y - intersect.y) * (fl1.getStart().y - intersect.y));

            int number = (int) (2f * Math.round(lenFl / 2f / Pane.gridSize));
            if (number != 0) {
              double part = lenFl / (number);
              double len = ((int) Math.round(disToStart / lenFl * number)) * part;
              double pointX = len * (fl1.getEnd().x - fl1.getStart().x) / lenFl + fl1.getStart().x;
              double pointY = len * (fl1.getEnd().y - fl1.getStart().y) / lenFl + fl1.getStart().y;

              intersect = new Point2D(pointX, pointY);
            } else {
              if (disToStart * 2. < lenFl) {
                intersect = new Point2D(fl1.getStartX(), fl1.getStartY());
              } else {
                intersect = new Point2D(fl1.getEndX(), fl1.getEndY());
              }
            }

          } else {
            if (fl1.getStart().equals(fl1.getEnd())) {
              Point2D origin = new Point2D(fl1.center.x, fl1.center.y);
              origin.x -= fl1.getStartX();
              origin.y -= fl1.getStartY();
              double tmpX = origin.x;
              origin.x = tmpX * Math.cos( - fl1.circAngle) + origin.y * Math.sin( - fl1.circAngle);
              origin.y = tmpX * Math.sin( - fl1.circAngle) + origin.y * Math.cos( - fl1.circAngle);
              origin.x += fl1.getStartX();
              origin.y += fl1.getStartY();
              double len = Math.PI * 2d * fl1.radius;
              int number = (int) (2d * Math.round(len / 2 / Pane.gridSize));
              double archPart = Math.PI * 2 / number;

              double archStartGes = Math.atan2(fl1.getStart().y - origin.y, fl1.getStart().x - origin.x);

              double archEndTeil = Math.atan2(intersect.y - origin.y, intersect.x - origin.x);
              double deltaArchTeil = (-archStartGes + archEndTeil) * - Math.signum(fl1.height);
              while (deltaArchTeil >= 2 * Math.PI) {
                deltaArchTeil -= 2 * Math.PI;
              }
              while (deltaArchTeil < 0) {
                deltaArchTeil += 2 * Math.PI;
              }

              int steps = (int) Math.round(deltaArchTeil / archPart);

              double arch = - ((steps) * archPart);
              if (fl1.height < 0) {
                arch -= archStartGes;
                arch *= -1;
                arch += Math.PI/2;
              } else {
                arch += archStartGes;
                arch += Math.PI/2;
              }

              double pointX = + origin.x - Math.signum(fl1.height) * fl1.radius * Math.sin( - arch);
              double pointY = + origin.y - Math.signum(fl1.height) * fl1.radius * Math.cos( - arch);

              intersect = new Point2D((int) Math.round(pointX), (int) Math.round(pointY));
            } else {
              double archEndGes = Math.atan2(fl1.getEnd().y - fl1.center.y, fl1.getEnd().x - fl1.center.x);
              double archStartGes = Math.atan2(fl1.getStart().y - fl1.center.y, fl1.getStart().x - fl1.center.x);
              double deltaArchGes = (-archStartGes + archEndGes) * - Math.signum(fl1.height);

              while (deltaArchGes > 2 * Math.PI) {
                deltaArchGes -= 2 * Math.PI;
              }
              while (deltaArchGes <= 0) {
                deltaArchGes += 2 * Math.PI;
              }

              double archEndTeil = Math.atan2(intersect.y - fl1.center.y, intersect.x - fl1.center.x);
              double deltaArchTeil = (-archStartGes + archEndTeil) * - Math.signum(fl1.height);

              while (deltaArchTeil >= 2 * Math.PI) {
                deltaArchTeil -= 2 * Math.PI;
              }
              while (deltaArchTeil < 0) {
                deltaArchTeil += 2 * Math.PI;
              }

              int number = (int) (2f * Math.round((deltaArchGes * fl1.radius / 2f / Pane.gridSize)));
              double archPart = deltaArchGes / number;
              int steps = (int) Math.round(deltaArchTeil / archPart);
              double arch = - ((steps) * archPart);
              if (fl1.height < 0) {
                arch -= archStartGes;
                arch *= -1;
                arch += Math.PI/2;
              } else {
                arch += archStartGes;
                arch += Math.PI/2;
              }
              double pointX = + fl1.center.x - Math.signum(fl1.height) * fl1.radius * Math.sin( - arch);
              double pointY = + fl1.center.y - Math.signum(fl1.height) * fl1.radius * Math.cos( - arch);

              intersect = new Point2D((int) Math.round(pointX), (int) Math.round(pointY));
            }
          }

          if (drawPane.lines.get(index).isFixed()) {
            if (drawPane.lines.get(index).fixStart) {
              intersect = new Point2D(drawPane.lines.get(index).getEndX(), drawPane.lines.get(index).getEndY());
            } else {
              intersect = new Point2D(drawPane.lines.get(index).getStartX(), drawPane.lines.get(index).getStartY());
            }
          }

          return new AbstractMap.SimpleEntry<>(intersect, index + 1);
        }
      } catch (Exception ignored) {
      }
    }

    if (grid) {
      int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
      int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
      x += Pane.gridSize / 2;
      y += Pane.gridSize / 2;

      Frame.logger.finest("2");

      try {
        getNearLineIntersect(clipDis, new Point2D(x, y), offset);
      } catch (Exception ignored) {
        if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
          return new AbstractMap.SimpleEntry<>(new Point2D(x, y), 0);
        }
      }
    }

    if (fl != null && dMode != -1) {
      Frame.logger.finest(fl.getStart() + " " + fl.getEnd() + " " + dMode);
      boolean t1 = (fl.getStartX() > 0 && Math.abs(p.x - fl.getStartX()) < clipDis && dMode != 0);
      boolean t2 = (fl.getStartY() > 0 && Math.abs(p.y - fl.getStartY()) < clipDis && dMode != 0);
      boolean t3 = (fl.getEndX() > 0 && Math.abs(p.x - fl.getEndX()) < clipDis && dMode != 1);
      boolean t4 = (fl.getEndY() > 0 && Math.abs(p.y - fl.getEndY()) < clipDis && dMode != 1);
      if (t1 && t2) /* End point is exactely equal to start point*/
        return new AbstractMap.SimpleEntry<>(new Point2D((int) Math.round(fl.getStartX()), (int) Math.round(fl.getStartY())), drawPane.lines.indexOf(fl) + 1);
      // if (t1) /* only x component is equal */
      //  return new AbstractMap.SimpleEntry<>(new Point2D((int) Math.round(fl.getStartX()), p.y), 0);
      // if (t2) /* only y component is equal */
      //  return new AbstractMap.SimpleEntry<>(new Point2D(p.x, (int) Math.round(fl.getStartY())), 0);
      if (t3 && t4)
        return new AbstractMap.SimpleEntry<>(new Point2D((int) Math.round(fl.getEndX()), (int) Math.round(fl.getEndY())), drawPane.lines.indexOf(fl) + 1);
      if (t3)
        return new AbstractMap.SimpleEntry<>(new Point2D((int) Math.round(fl.getEndX()), p.y), 0);
      if (t4)
        return new AbstractMap.SimpleEntry<>(new Point2D(p.x, (int) Math.round(fl.getEndY())), 0);
      Frame.logger.finest("1");
    }

    if (fl != null && dMode != -1 && clipToAngle != null && !clipToAngle.isEmpty()) {
      /* clipping to angle, must be after clipping to horizontal/vertical! */

      double del_x = (fl.getStartX() - p.x);
      double del_y = (fl.getStartY() - p.y);
      double dist = Math.sqrt(del_x * del_x + del_y * del_y);
      double angle = Math.atan2(del_y, del_x);
      ArrayList<Double> new_angle = new ArrayList<>();
      for (Double a : clipToAngle) {
        new_angle.add(Math.round(angle / a) * a);
        Frame.logger.finest("->>" + Math.abs(angle - new_angle.get(new_angle.size() - 1)));
      }
      ArrayList<Point2D> new_p = new ArrayList<>();
      for (double a : new_angle) {
        int new_x = (int) Math.round(fl.getStartX() - dist * Math.cos(a));
        int new_y = (int) Math.round(fl.getStartY() - dist * Math.sin(a));
        new_p.add(new Point2D(new_x, new_y));
      }

      for (int i = 0; i < new_angle.size(); i++) {
        if (Math.abs(angle - new_angle.get(i)) < 0.1) { //  && Math.round(new_angle.get(i) / clipToAngle.get(i)) % 2 != 1 /*for not clipping to horizonal/vertical*/
          return new AbstractMap.SimpleEntry<>(new_p.get(i), 0);
        }
      }
    }

    Frame.logger.finest("3");


    return new AbstractMap.SimpleEntry<>(new Point2D(p.x, p.y), 0);
  }
  /**
   * where is this function used?
   * apparently searches for the path where this class file is located
   * @return the path of this class file
   */
  private String parseStartPath() {
    String startPath = Frame.this.getClass().getResource(Frame.this.getClass().getSimpleName() + ".class").getPath();
    if (!startPath.contains("!")) {
      startPath = startPath.substring(0, startPath.lastIndexOf("/"));
      startPath = startPath.substring(0, startPath.lastIndexOf("/"));
      startPath = startPath.substring(0, startPath.lastIndexOf("/"));
      startPath = startPath.substring(0, startPath.lastIndexOf("/"));
    } else {
      startPath = startPath.substring(startPath.indexOf(":") + 1, startPath.lastIndexOf("!"));
    }
    startPath = startPath.substring(0, startPath.lastIndexOf("/"));
    return startPath;
  }

  @Override
  public void keyTyped(KeyEvent e) {

  }

  /**
   * Implements all keys from the program.
   *
   * @param e {@link KeyEvent}
   */
  @Override
  public void keyPressed(KeyEvent e) {
    this.keyClicked = true;
    switch (e.getKeyCode()) {
    case KeyEvent.VK_DELETE:
      delAction.actionPerformedMethod();
      break;
    case KeyEvent.VK_E:
      if (e.isShiftDown()) {
        if (t != null) t.setVisible(false);

        Point conv_p = new Point(0, 0);
        SwingUtilities.convertPointToScreen(conv_p, drawPane);
        t = new Toast("Print the dashing lengths of the lines", this.drawPane);
        //t.showtoast();
        Frame.this.requestFocusInWindow();
      }
      break;
    case KeyEvent.VK_UP:
      if (e.isAltDown()) {
        if (drawPane.grid) {
          Pane.gridSize += 1;
          Frame.this.repaint();
        }
        e.consume();
      } else if (gameMode == 0) {
        Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
        drawPane.scale *= 1.1;
        drawPane.zeroPoint = new Point2D(currX / drawPane.scale - mPosPane.x, currY / drawPane.scale - mPosPane.y);
        Frame.this.repaint();
        e.consume();
      }
      break;
    case KeyEvent.VK_RIGHT:
      if(!e.isControlDown() && !e.isMetaDown())
        tiles.next();
      break;
    case KeyEvent.VK_DOWN:
      if (e.isAltDown()) {
        if (drawPane.grid) {
          Pane.gridSize -= 1;
          if (Pane.gridSize < clipDistance) {
            if (t != null) t.setVisible(false);

            Point conv_p = new Point(0, 0);
            SwingUtilities.convertPointToScreen(conv_p, drawPane);
            t = new Toast("Minimal grid spacing", this.drawPane);
            //t.showtoast();
            Frame.this.requestFocusInWindow();
            Pane.gridSize = clipDistance;
          }
          Frame.this.repaint();
        }
        e.consume();
      } else if (gameMode == 0) {
        Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
        drawPane.scale /= 1.1;
        drawPane.zeroPoint = new Point2D(currX / drawPane.scale - mPosPane.x, currY / drawPane.scale - mPosPane.y);
        Frame.this.repaint();
        e.consume();
      }
      break;
    case KeyEvent.VK_LEFT:
      if(!e.isControlDown() && !e.isMetaDown())
        tiles.prev();
      break;
    case KeyEvent.VK_1:
      tiles.setSelected(0);
      break;
    case KeyEvent.VK_2:
      tiles.setSelected(1);
      break;
    case KeyEvent.VK_3:
      tiles.setSelected(2);
      break;
    case KeyEvent.VK_4:
      tiles.setSelected(3);
      break;
    case KeyEvent.VK_5:
      tiles.setSelected(4);
      break;
    case KeyEvent.VK_6:
      tiles.setSelected(5);
      break;
    case KeyEvent.VK_7:
      tiles.setSelected(6);
      break;
    case KeyEvent.VK_8:
      tiles.setSelected(7);
      break;
    case KeyEvent.VK_9:
      tiles.setSelected(8);
      break;
    case KeyEvent.VK_0:
      if (gameMode == 0 && e.isShiftDown()) {
        drawPane.zeroPoint = new Point2D(0, 0);
        Frame.this.repaint();
        e.consume();
      } else if (gameMode == 0 && ((e.isControlDown() && !macOs) || (macOs && e.isMetaDown()))) {
        Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
        drawPane.scale = 1;
        drawPane.zeroPoint = new Point2D(currX / drawPane.scale - mPosPane.x, currY / drawPane.scale - mPosPane.y);
        Frame.this.repaint();
        e.consume();
      } else {
        tiles.setSelected(9);
      }
      break;
    case KeyEvent.VK_M:
      tiles.setGrabMode();
      break;
    case KeyEvent.VK_X:
      if (t != null) t.setVisible(false);

      Point conv_p = new Point(0, 0);
      SwingUtilities.convertPointToScreen(conv_p, drawPane);
      t = new Toast("Print the lengths of the lines", this.drawPane);
      //t.showtoast();
      Frame.this.requestFocusInWindow();
      break;
    case KeyEvent.VK_D:
      if (drawPane.selectType == 0 && this.gameMode == 0 && drawPane.lines.size() > 0) {
        saveForUndo();
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.dashed = !drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.dashed;
      } else if (drawPane.selectType == 1 && this.gameMode == 0 && drawPane.vertices.size() > 0) {
        saveForUndo();
        drawPane.vertices.get(drawPane.vertices.size() - 1).border.dashed = !drawPane.vertices.get(drawPane.vertices.size() - 1).border.dashed;
      } else if (drawPane.selectType == 2 && drawPane.fObjects.size() > 0 && drawPane.fObjects.get(drawPane.fObjects.size() - 1).v && this.gameMode == 0) {
        saveForUndo();
        drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.border.dashed = !drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.border.dashed;
      } else if (drawPane.selectType == 3 && this.gameMode == 0 && drawPane.shapes.size() > 0) {
        saveForUndo();
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.dashed = !drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.dashed;
      } else if (drawPane.selectType == 4 && this.gameMode == 0) {
        if (drawPane.multiEdit.type == 0 || drawPane.multiEdit.type == 1) {
          saveForUndo();
          drawPane.multiEdit.setDashed(!drawPane.multiEdit.getDashed());
          tilePanel.repaint();
        }
      }
      Frame.this.repaint();
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (beframe != null)
        beframe.update();
      break;
    case KeyEvent.VK_I:
      if (drawPane.selectType == 0 && !e.isControlDown() && !e.isMetaDown() && drawPane.lines.size() > 0) {
        if (!this.drawPane.lines.get(drawPane.lines.size() - 1).isFixed()) {
          if (!e.isShiftDown()) {
            this.saveForUndo();
            Point2D temp = drawPane.lines.get(drawPane.lines.size() - 1).getStart();
            drawPane.lines.get(drawPane.lines.size() - 1).setStart(drawPane.lines.get(drawPane.lines.size() - 1).getEnd());
            drawPane.lines.get(drawPane.lines.size() - 1).setEnd(temp);
            drawPane.lines.get(drawPane.lines.size() - 1).phase *= -1;
            if (drawPane.lines.get(drawPane.lines.size() -1).getStart().equals(drawPane.lines.get(drawPane.lines.size() - 1).getEnd()))
              drawPane.lines.get(drawPane.lines.size() - 1).circAngle += Math.PI;
            drawPane.lines.get(drawPane.lines.size() - 1).setHeight(-drawPane.lines.get(drawPane.lines.size() - 1).getHeight());
            combineLines(drawPane.lines.get(drawPane.lines.size() - 1).getStart());
            combineLines(drawPane.lines.get(drawPane.lines.size() - 1).getEnd());
            drawPane.checkForVertex(drawPane.lines.get(drawPane.lines.size() - 1).getStart());
            drawPane.checkForVertex(drawPane.lines.get(drawPane.lines.size() - 1).getEnd());
          } else {
            if (drawPane.lines.get(drawPane.lines.size() - 1).isMom()) {
              this.saveForUndo();
              drawPane.lines.get(drawPane.lines.size() - 1).momArrow.invert = !drawPane.lines.get(drawPane.lines.size() - 1).momArrow.invert;
            }
          }
          if (this.eframe != null) {
            this.eframe.editPane.upEditPane();
            this.eframe.repaint();
          }
          if (beframe != null)
            beframe.update();
        }
      }
      break;
    case KeyEvent.VK_MINUS:
      if (drawPane.selectType == 0 && drawPane.lines.size() > 0 && this.gameMode == 0) {
        saveForUndo();
        if (drawPane.lines.size() > 0 && drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.doubleLine) {
          double newStroke = (double) drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.stroke;
          newStroke *= 1d/3d;
          drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.stroke = (int) Math.round(newStroke);
        } else {
          drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.stroke *= 3;
        }
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.doubleLine = !drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.doubleLine;
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (beframe != null)
          beframe.update();
      } else if (drawPane.selectType == 3 && drawPane.shapes.size() > 0 && this.gameMode == 0) {
        saveForUndo();
        if (drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.doubleLine) {
          double newStroke = (double) drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.stroke;
          newStroke *= 1d/3d;
          drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.stroke = (int) Math.round(newStroke);
        } else {
          drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.stroke *= 3;
        }
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.doubleLine = !drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.doubleLine;
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (beframe != null)
          beframe.update();
      } else if (drawPane.selectType == 4 && gameMode == 0) {
        if (drawPane.multiEdit.type == 0) {
          saveForUndo();
          if (drawPane.multiEdit.getDoubleLine()) {
            double newStroke = drawPane.multiEdit.getStroke();
            newStroke *= 1d/3d;
            drawPane.multiEdit.setStroke((float) newStroke);
          } else {
            double newStroke = drawPane.multiEdit.getStroke();
            newStroke *= 3d;
            drawPane.multiEdit.setStroke((float) newStroke);
          }

          drawPane.multiEdit.setDoubleLine(!drawPane.multiEdit.getDoubleLine());
          tilePanel.repaint();

          if (this.eframe != null) {
            this.eframe.editPane.upEditPane();
            this.eframe.repaint();
          }
          if (beframe != null)
            beframe.update();
        }
      }
      break;
    case KeyEvent.VK_L:
      if (((!e.isControlDown() && !macOs) || (macOs && !e.isMetaDown()))) {
        if (!e.isShiftDown()) {
          if (drawPane.selectType == 0 && drawPane.lines.size() > 0 && this.gameMode == 0) {
            saveForUndo();
            drawPane.lines.get(drawPane.lines.size() - 1).showDesc = !drawPane.lines.get(drawPane.lines.size() - 1).showDesc;
          } else if (drawPane.selectType == 1 && drawPane.vertices.size() > 0) {
            saveForUndo();
            drawPane.vertices.get(drawPane.vertices.size() - 1).showDesc = !drawPane.vertices.get(drawPane.vertices.size() - 1).showDesc;
          } else if (drawPane.selectType == 2 && drawPane.fObjects.size() > 0 &&  drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
            saveForUndo();
            drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.showDesc = !drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.showDesc;
          } else if (drawPane.selectType == 2 && drawPane.fObjects.size() > 0 &&  !drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
            saveForUndo();
            drawPane.fObjects.get(drawPane.fObjects.size() - 1).floatingImage.showDesc = !drawPane.fObjects.get(drawPane.fObjects.size() - 1).floatingImage.showDesc;
          } else if (drawPane.selectType == 3 && drawPane.shapes.size() > 0) {
            saveForUndo();
            drawPane.shapes.get(drawPane.shapes.size() - 1).showDesc = !drawPane.shapes.get(drawPane.shapes.size() - 1).showDesc;
          } else if (drawPane.selectType == 4 && drawPane.multiEdit != null) {
            saveForUndo();
            drawPane.multiEdit.setShowDesc(!drawPane.multiEdit.getShowDesc());
            tilePanel.repaint();
          }
        } else {
          if (drawPane.lines.get(drawPane.lines.size() - 1).isMom()) {
            saveForUndo();
            drawPane.lines.get(drawPane.lines.size() - 1).momArrow.showDesc = !drawPane.lines.get(drawPane.lines.size() - 1).momArrow.showDesc;
          }
        }
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (beframe != null)
          beframe.update();
      }
      break;
    case KeyEvent.VK_P:
      if (((!e.isControlDown() && !macOs) || (macOs && !e.isMetaDown()))) {
        if (drawPane.selectType == 0 && drawPane.lines.size() > 0 && this.gameMode == 0) {
          drawPane.lines.get(drawPane.lines.size() - 1).toggleMom();
        }
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (beframe != null)
          beframe.update();
      }
      break;
    case KeyEvent.VK_C:
      if (((!e.isControlDown() && !macOs) || (macOs && !e.isMetaDown()))) {
        if (t != null) t.setVisible(false);

        Point conv_pp = new Point(0, 0);
        SwingUtilities.convertPointToScreen(conv_pp, drawPane);
        t = new Toast("ERROR", this.drawPane);
        if (drawPane.angles.size() != 0 && drawPane.angles.get(0) == Math.PI/2) {
          drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 4));
          PI4AngleRadio.setSelected(true);
          t = new Toast("<html>Clipping to 45&deg", this.drawPane);
        } else if (drawPane.angles.size() != 0 && drawPane.angles.get(0) == Math.PI/4) {
          drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 16));
          PI16AngleRadio.setSelected(true);
          t = new Toast("<html>Clipping to 22.5&deg", this.drawPane);
        } else if (drawPane.angles.size() != 0 && drawPane.angles.get(0) == Math.PI/16) {
          drawPane.angles = new ArrayList<>();
          noAngleRadio.setSelected(true);
          t = new Toast("<html>Not clipping to any angles", this.drawPane);
        } else if (drawPane.angles.size() == 0) {
          drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 2));
          PI2AngleRadio.setSelected(true);
          t = new Toast("<html>Clipping to 90&deg", this.drawPane);
        }
        //t.showtoast();
        Frame.this.requestFocusInWindow();
        break;
      }
    }
  }

  @Override
  public void keyReleased(KeyEvent e) {
    this.keyClicked = false;
  }

  @Override
  public void mouseClicked(MouseEvent e) {

    Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);

    if (e.isShiftDown()) {
      Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x, mPosPane.y));
      int index = intersectPair.getValue();
      if (index < 0) {
        index *= -1;
        index -= 1;
        Vertex tmp_v = drawPane.vertices.get(index);
        tmp_v.autoremove = !drawPane.vertices.get(index).autoremove;
        drawPane.vertices.remove(index);
        drawPane.vertices.add(tmp_v);
      }
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (beframe != null)
        beframe.update();
    }

    tiles.setGrabMode();

  }

  /**
   * This handles the things that have to be done when the mouse is clicked
   * anywhere on the canvas depending on what tile is selected
   * @param e {@link KeyEvent}
   */
  @Override
  public void mousePressed(MouseEvent e) {

    Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);

    if (gameMode == 0) {
      if (SwingUtilities.isRightMouseButton(e)) {
        new PopMenu(e);
        return;
      }
    }

    if (this.scrollCounter == 1)
      return;

    if (gameMode == 0) {
      /* Drag background */
      draggedDist = new Point.Double(e.getX(), e.getY());
      draggedMode = 7;
    }

    if (e.isShiftDown()) {

      try {
        Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x, mPosPane.y), null, -1, false, 5);
        int index = intersectPair.getValue();
        if (index > 0 && drawPane.lines.size() > 0) {
          index--;

          if (drawPane.selectType == 0) {
            drawPane.selectType = 4;
            drawPane.multiEdit = new MultiEdit(drawPane.lines.get(drawPane.lines.size() -1).lineConfig, this);
            drawPane.multiEdit.add(drawPane.lines.get(index).lineConfig);
            return;
          } else if (drawPane.selectType == 4 && drawPane.multiEdit.type == 0) {
            drawPane.multiEdit.add(drawPane.lines.get(index).lineConfig);
            return;
          }

        } else if (index < 0 && drawPane.vertices.size() > 0) {
          index = -index - 1;

          if (drawPane.selectType == 1) {
            drawPane.selectType = 4;
            drawPane.multiEdit = new MultiEdit(drawPane.vertices.get(drawPane.vertices.size() - 1), this);
            drawPane.multiEdit.add(drawPane.vertices.get(index));
            return;
          } else if (drawPane.selectType == 4 && drawPane.multiEdit.type == 1) {
            drawPane.selectType = 4;
            drawPane.multiEdit.add(drawPane.vertices.get(index));
            return;
          } else if (drawPane.selectType == 3 && drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
            drawPane.selectType = 4;
            drawPane.multiEdit = new MultiEdit(drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex, this);
            drawPane.multiEdit.add(drawPane.vertices.get(index));
            return;
          }
        }
      } catch (Exception ignored) {
      }

      if (drawPane.fObjects.size() > 0) {
        for (FloatingObject fO : drawPane.fObjects) {
          if (fO.v) {
            Vertex v = fO.vertex;
            if (v.origin.distance(new Point2D(mPosPane.x, mPosPane.y)) < 5) {
              if (drawPane.selectType == 1) {
                drawPane.selectType = 4;
                drawPane.multiEdit = new MultiEdit(drawPane.vertices.get(drawPane.vertices.size() - 1), this);
                drawPane.multiEdit.add(v);
                return;
              } else if (drawPane.selectType == 4 && drawPane.multiEdit.type == 1) {
                drawPane.selectType = 4;
                drawPane.multiEdit.add(v);
                return;
              } else if (drawPane.selectType == 3 && drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
                drawPane.selectType = 4;
                drawPane.multiEdit = new MultiEdit(drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex, this);
                drawPane.multiEdit.add(v);
                return;
              }
            }
          } else {
            FloatingImage im = fO.floatingImage;
            if (im.center.distance(new Point2D(mPosPane.x, mPosPane.y)) < 5) {
              if (drawPane.selectType == 3 && !drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
                drawPane.selectType = 4;
                drawPane.multiEdit = new MultiEdit(drawPane.fObjects.get(drawPane.fObjects.size() - 1).floatingImage, this);
                drawPane.multiEdit.add(im);
                return;
              } else if (drawPane.selectType == 4 && drawPane.multiEdit.type == 4) {
                drawPane.multiEdit.add(im);
                drawPane.selectType = 4;
                return;
              }
            }
          }
        }
      }
    }

    if (drawPane.fObjects.size() > 0 && tiles.selected != null && tiles.selected.lineType == LineType.GRAB) {
      int index = 0;
      for (FloatingObject fO : drawPane.fObjects) {
        FloatingObject tmp = new FloatingObject(fO);
        if (fO.v) {
          if (fO.vertex.showDesc) {
            Point2D place = new Point2D(fO.vertex.origin.x + fO.vertex.descPosX, fO.vertex.origin.y + fO.vertex.descPosY);
            if (place.distance(mPosPane) < 10) {
              drawPane.fObjects.remove(index);
              drawPane.fObjects.add(tmp);
              drawPane.selectType = 2;
              draggedMode = 6;
              draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
              dragDisX = -place.x + mPosPane.x;
              dragDisY = mPosPane.y - place.y;
              if (this.drawPane.multiEdit != null)
                this.drawPane.multiEdit.fin();
              System.gc();
              return;
            }
          }
          if (fO.vertex.origin.distance(mPosPane) < 10) {
            drawPane.fObjects.remove(index);
            drawPane.fObjects.add(tmp);
            drawPane.selectType = 2;
            draggedMode = 3;
            draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
            dragDisX = -fO.vertex.origin.x + mPosPane.x;
            dragDisY = mPosPane.y - fO.vertex.origin.y;
            if (this.drawPane.multiEdit != null)
              this.drawPane.multiEdit.fin();
            System.gc();
            return;
          }
        } else {
          if (fO.floatingImage.showDesc) {
            Point2D place = new Point2D(fO.floatingImage.center.x + fO.floatingImage.descPosX, fO.floatingImage.center.y + fO.floatingImage.descPosY);
            if (place.distance(mPosPane) < 10) {
              drawPane.fObjects.remove(index);
              drawPane.fObjects.add(tmp);
              drawPane.selectType = 2;
              draggedMode = 6;
              draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
              dragDisX = -place.x + mPosPane.x;
              dragDisY = mPosPane.y - place.y;
              if (this.drawPane.multiEdit != null)
                this.drawPane.multiEdit.fin();
              System.gc();
              return;
            }
          }
          if (fO.floatingImage.center.distance(mPosPane) < 10) {
            drawPane.fObjects.remove(index);
            drawPane.fObjects.add(tmp);
            drawPane.selectType = 2;
            draggedMode = 3;
            draggedDist = new Point.Double(mPosPane.x, mPosPane.x);
            dragDisX = -fO.floatingImage.center.x + mPosPane.x;
            dragDisY = mPosPane.y - fO.floatingImage.center.y;
            if (this.drawPane.multiEdit != null)
              this.drawPane.multiEdit.fin();
            System.gc();
            return;
          }
        }
        index++;
      }
      if (index != drawPane.fObjects.size()) {
        if (this.drawPane.multiEdit != null)
          this.drawPane.multiEdit.fin();
        System.gc();
        return;
      }
    }

    int indexx = 0;
    for (Vertex v : this.drawPane.vertices) {
      if (!e.isShiftDown() && tiles.selected != null && tiles.selected.lineType == LineType.GRAB && v.showDesc) {
        Point2D place = new Point2D(v.origin.x + v.descPosX, v.origin.y + v.descPosY);
        if (place.distance(mPosPane) < 10) {
          Vertex tmp = drawPane.vertices.get(indexx);
          boolean autoremove = drawPane.vertices.get(indexx).autoremove;
          drawPane.vertices.remove(indexx);
          drawPane.vertices.add(tmp);
          drawPane.vertices.get(drawPane.vertices.size() -1).autoremove = autoremove;
          drawPane.selectType = 1;
          draggedMode = 5;
          draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
          dragDisX = -place.x + mPosPane.x;
          dragDisY = mPosPane.y - place.y;
          if (this.drawPane.multiEdit != null)
            this.drawPane.multiEdit.fin();
          System.gc();
          return;
        }
        indexx++;
      }
    }

    int i = 0;

    if (!e.isShiftDown() && tiles.selected != null && tiles.selected.lineType == LineType.GRAB && drawPane.lines.size() > 0) {
      for (FeynLine line : drawPane.lines) {
        if (line.showDesc) {
          double len = Math.sqrt((line.getStartX() - line.getEndX()) * (line.getStartX() - line.getEndX()) + (line.getStartY() - line.getEndY()) * (line.getStartY() - line.getEndY()));
          Point2D place;
          if (Math.abs(line.height) < .1) {

            double normtanx = line.getStartX() - line.getEndX();
            normtanx /= len;
            double normtany = line.getStartY() - line.getEndY();
            normtany /= len;
            place = new Point2D(line.getStartX() - (len * line.partPlaceDesc) * normtanx + (line.descDis) * normtany, line.getStartY() - (len * line.partPlaceDesc) * normtany - (line.descDis) * normtanx);
          } else {
            double partArch = line.deltaArch * line.partPlaceDesc;
            double placeArch = line.archStart + partArch;
            double radius = line.radius + line.descDis;
            double x;
            double y;
            if (line.height > 0) {
              y = line.center.y + radius * Math.sin(placeArch);
              x = line.center.x + radius * Math.cos(placeArch);
            } else {
              y = line.center.y - radius * Math.sin(placeArch);
              x = line.center.x - radius * Math.cos(placeArch);
            }
            place = new Point2D(x, y);
            if (line.getStart().equals(line.getEnd())) {
              place.rotate( - line.circAngle, line.getStartX(), line.getStartY());
            }
          }

          if (place.distance(mPosPane) < 10) {
            drawPane.lines.remove(i);
            drawPane.lines.add(line);
            drawPane.selectType = 0;
            draggedMode = 4;
            draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
            dragDisX = -place.x + mPosPane.x;
            dragDisY = mPosPane.y - place.y;
            if (this.drawPane.multiEdit != null)
              this.drawPane.multiEdit.fin();
            System.gc();
            return; // TODO: should this be a break??? is there any reason???
          }
        }
        i++;
      }
    }
    i = 0;
    if (!e.isShiftDown() && tiles.selected != null && tiles.selected.lineType == LineType.GRAB && drawPane.shapes.size() > 0) {
      for (Shaped s : drawPane.shapes) {
        if (s.showDesc) {
          Point2D descPoint = new Point2D(s.origin.x + s.descPosX, s.origin.y + s.descPosY);
          if (descPoint.distance(mPosPane) < 10) {
            s.hoverMode = 5;
            drawPane.shapes.remove(i);
            s.getAtt(Frame.this);
            drawPane.shapes.add(s);
            drawPane.selectType = 3;
            draggedMode = 8;
            draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
            if (this.drawPane.multiEdit != null)
              this.drawPane.multiEdit.fin();
            System.gc();
            return;
          }
        }
        Map.Entry<Point2D, Boolean> p = s.nPoint(mPosPane, 5d);
        if (p.getValue()) {
          Point2D point = new Point2D(mPosPane.x - s.origin.x, mPosPane.y - s.origin.y);
          point.rotate(s.rotation, 0, 0);
          double angle = 180 /Math.PI * Math.atan2(point.y / s.height * 2, point.x / s.width * 2);
          if ((45 < angle && angle <= 135) || (-135 < angle && angle < -45)) {
            s.hoverMode = 2;
            drawPane.shapes.remove(i);
            s.getAtt(Frame.this);
            drawPane.shapes.add(s);
            drawPane.selectType = 3;
            draggedMode = 8;
            draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
            if (this.drawPane.multiEdit != null)
              this.drawPane.multiEdit.fin();
            System.gc();
            return;
          } else {
            s.hoverMode = 3;
            drawPane.shapes.remove(i);
            drawPane.shapes.add(s);
            drawPane.selectType = 3;
            draggedMode = 8;
            draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
            if (this.drawPane.multiEdit != null)
              this.drawPane.multiEdit.fin();
            System.gc();
            return;
          }
        }
        if (s.origin.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
          s.hoverMode = 1;
          drawPane.shapes.remove(i);
          s.getAtt(Frame.this);
          drawPane.shapes.add(s);
          drawPane.selectType = 3;
          draggedMode = 8;
          draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
          if (this.drawPane.multiEdit != null)
            this.drawPane.multiEdit.fin();
          System.gc();
          return;
        }
        Point2D pp = new Point2D(mPosPane.x - s.origin.x, mPosPane.y - s.origin.y);
        double r = pp.distance(new Point2D(0, 0));
        if (r > (double) s.height / 4 && r < (double) s.height * 3 / 4) {
          pp.rotate(s.rotation, 0, 0);
          double angle = 180 /Math.PI * Math.atan2(pp.y / s.height * 2, pp.x / s.width * 2);
          if (Math.abs(Math.abs(angle) - 90) * r < 2.5 * 180 / Math.PI) {
            s.hoverMode = 4;
            drawPane.shapes.remove(i);
            s.getAtt(Frame.this);
            drawPane.shapes.add(s);
            drawPane.selectType = 3;
            draggedMode = 8;
            draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
            if (this.drawPane.multiEdit != null)
              this.drawPane.multiEdit.fin();
            System.gc();
            return;
          }
        }
        i++;
      }
    }

    if (tiles.selected != null && tiles.selected.lineType == LineType.GRAB && tiles.selectedIndex < LineConfig.presets.size()) {
      Cursor hourglassCursor = new Cursor(Cursor.MOVE_CURSOR);
      drawPane.setCursor(hourglassCursor);
      try {
        Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x, mPosPane.y), null, -1, false, 5);
        int index = intersectPair.getValue();
        if (index > 0 && drawPane.lines.size() > 0) {
          boolean found = false;
          index--;
          Point2D intersect = intersectPair.getKey();
          FeynLine fl = drawPane.lines.get(index);

          if (Math.abs(fl.height) < .1) {
            double lenFl = Math.sqrt((fl.getStart().x - fl.getEnd().x) * (fl.getStart().x - fl.getEnd().x) + (fl.getStart().y - fl.getEnd().y) * (fl.getStart().y - fl.getEnd().y));
            double disToStart = Math.sqrt((fl.getStart().x - intersect.x) * (fl.getStart().x - intersect.x) + (fl.getStart().y - intersect.y) * (fl.getStart().y - intersect.y));
            if (lenFl > 4 * disToStart) {
              if (!fl.fixStart) {
                draggedMode = 0;
                intersect = fl.getStart();
                found = true;
              }
            } else if (lenFl * 3 < 4 * disToStart) {
              if (!fl.fixEnd) {
                found = true;
                draggedMode = 1;
                intersect = fl.getEnd();
              }
            } else {
              if (!fl.fixStart && !fl.fixEnd) {
                draggedMode = -1;
                found = true;
              }
            }
          } else {

            double archEndGes;
            double archStartGes;
            double deltaArchGes;
            if (fl.getStart().equals(fl.getEnd())) {
              intersect.x -= fl.getEndX();
              intersect.y -= fl.getEndY();
              double tmpX = intersect.x;
              intersect.x = (int) Math.round(intersect.x * Math.cos( - fl.circAngle) + intersect.y * Math.sin( - fl.circAngle));
              intersect.y = (int) Math.round(tmpX * Math.sin( - fl.circAngle) + intersect.y * Math.cos( - fl.circAngle));
              intersect.x += fl.getEndX();
              intersect.y += fl.getEndY();
              deltaArchGes =  - Math.PI * 2;

            } else {
              archEndGes = Math.atan2(fl.getEnd().y - fl.center.y, fl.getEnd().x - fl.center.x);
              archStartGes = Math.atan2(fl.getStart().y - fl.center.y, fl.getStart().x - fl.center.x);
              deltaArchGes = (-archStartGes + archEndGes) * - Math.signum(fl.height);
            }

            while (deltaArchGes > 2 * Math.PI) {
              deltaArchGes -= 2 * Math.PI;
            }
            while (deltaArchGes <= 0) {
              deltaArchGes += 2 * Math.PI;
            }

            double archEndTeil = Math.atan2(intersect.y - fl.center.y, intersect.x - fl.center.x);
            double archStartTeil = Math.atan2(fl.getStart().y - fl.center.y, fl.getStart().x - fl.center.x);
            double deltaArchTeil = (-archStartTeil + archEndTeil) * -Math.signum(fl.height);

            while (deltaArchTeil >= 2 * Math.PI) {
              deltaArchTeil -= 2 * Math.PI;
            }
            while (deltaArchTeil < 0) {
              deltaArchTeil += 2 * Math.PI;
            }


            if (deltaArchGes / 4 > deltaArchTeil) {
              if (!fl.fixStart) {
                draggedMode = 0;
                found = true;
                intersect = fl.getStart();
              }
            } else if (deltaArchGes * 3 / 4 < deltaArchTeil) {
              if (!fl.fixEnd) {
                found = true;
                draggedMode = 1;
                intersect = fl.getEnd();
              }
            } else {
              if (!fl.fixStart && !fl.fixEnd) {
                draggedMode = -1;
                found = true;
              }
            }
          }

          if (found) {
            draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
            draggedLine = new FeynLine(fl);
            this.saveForUndo();
            drawPane.selectType = 0;
            dragDisX = -intersect.x + mPosPane.x;
            dragDisY = mPosPane.y - intersect.y;

            drawPane.lines.remove(index);
            drawPane.lines.add(fl);
            Frame.this.repaint();
            if (this.eframe != null) {
              this.eframe.editPane.upEditPane();
              this.eframe.repaint();
            }
            if (beframe != null)
              beframe.update();
          }
        } else if (index < 0 && drawPane.vertices.size() > 0) {
          this.saveForUndo();
          index = -index - 1;
          Vertex vx = new Vertex(drawPane.vertices.get(index));
          draggedMode = 2;
          lineModes = new ArrayList<>();
          int count = 0;
          int index1 = 0;
          draggedDist = new Point.Double(mPosPane.x, mPosPane.y);
          while (count + index1 < drawPane.lines.size()) {
            FeynLine line = drawPane.lines.get(index1);
            if (line.getEnd().distance(intersectPair.getKey()) < 1.5 && line.getStart().distance(intersectPair.getKey()) < 1.5) {
              count++;
              drawPane.lines.remove(index1);
              drawPane.lines.add(line);
              lineModes.add(0);
            } else if (line.getStart().distance(intersectPair.getKey()) < 1.5) {
              count++;
              drawPane.lines.remove(index1);
              drawPane.lines.add(line);
              lineModes.add(1);
            } else if (line.getEnd().distance(intersectPair.getKey()) < 1.5) {
              count++;
              drawPane.lines.remove(index1);
              drawPane.lines.add(line);
              lineModes.add(-1);
            } else {
              index1++;
            }
          }
          Point2D intersect = intersectPair.getKey();
          dragDisX = -intersect.x + mPosPane.x;
          dragDisY = mPosPane.y - intersect.y;
          boolean autoremove = drawPane.vertices.get(index).autoremove;
          drawPane.vertices.remove(index);
          drawPane.vertices.add(vx);
          drawPane.vertices.get(drawPane.vertices.size() - 1).autoremove = autoremove;
          drawPane.selectType = 1;
          Frame.this.repaint();
          if (this.eframe != null) {
            this.eframe.editPane.upEditPane();
            this.eframe.repaint();
          }
          if (beframe != null)
            beframe.update();
          if (this.drawPane.multiEdit != null)
            this.drawPane.multiEdit.fin();
          System.gc();
          return;
        }
      } catch (Exception ignored) {
      }
    }

    if (tiles.selectedIndex < LineConfig.presets.size() && tiles.selected != null && tiles.selected.lineType != LineType.GRAB) {
      Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x, mPosPane.y));
      Point2D intersect = intersectPair.getKey();
      this.saveForUndo();
      drawPane.lines.add(new FeynLine(new Line(intersect.x, intersect.y, intersect.x, intersect.y), new LineConfig(tiles.selected)));
      drawPane.checkForVertex(new Point2D(intersect.x, intersect.y));
      drawPane.mouseDown = true;
      drawPane.selectType = 0;
      draggedMode = -1;
    } else if (tiles.selectedVertex != null && tiles.selectedIndex >= LineConfig.presets.size() - 1 && tiles.selectedIndex < LineConfig.presets.size() + Vertex.vertexPresets.size()) {
      this.saveForUndo();
      Point2D p = new Point2D(mPosPane.x, mPosPane.y);
      if (drawPane.grid) {
        int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
        int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
        x += Pane.gridSize / 2;
        y += Pane.gridSize / 2;

        if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
          p = new Point2D(x, y);
        }
      }
      drawPane.fObjects.add(new FloatingObject(new Vertex(tiles.selectedVertex), p));
      drawPane.selectType = 2;
      drawPane.mouseDown = true;
      draggedMode = 3;
      Frame.this.repaint();
    } else if (tiles.selectedImage != null && tiles.selectedIndex >= LineConfig.presets.size() + Vertex.vertexPresets.size() - 1) {
      this.saveForUndo();
      Point2D p = new Point2D(mPosPane.x, mPosPane.y);
      if (drawPane.grid) {
        int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
        int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
        x += Pane.gridSize / 2;
        y += Pane.gridSize / 2;

        if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
          p = new Point2D(x, y);

        }
      }
      drawPane.fObjects.add(new FloatingObject(new FloatingImage(tiles.selectedImage), p));
      drawPane.selectType = 2;
      drawPane.mouseDown = true;
      draggedMode = 3;
      Frame.this.repaint();
    }

    //Set drag mode for the manual bounding box if the cursor is near the box
    if(tiles.selected != null && tiles.selected.lineType == LineType.GRAB && drawPane.mBB) {
      //mPosPane is the cursor pos, check distance to the four sides separately
      boundingBoxDraggedSide = 0;
      Cursor cursor = null;
      if(Math.abs(drawPane.xMBB - mPosPane.x) < clipDistance
          && mPosPane.y > drawPane.yMBB && mPosPane.y < drawPane.yMBB + drawPane.hMBB) {
        boundingBoxDraggedSide = 1; //left side
        cursor = Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
      } else if(Math.abs(drawPane.yMBB - mPosPane.y) < clipDistance
          && mPosPane.x > drawPane.xMBB && mPosPane.x < drawPane.xMBB + drawPane.wMBB) {
        boundingBoxDraggedSide = 2; //top side
        cursor = Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
      } else if(Math.abs(drawPane.xMBB + drawPane.wMBB - mPosPane.x) < clipDistance
          && mPosPane.y > drawPane.yMBB && mPosPane.y < drawPane.yMBB + drawPane.hMBB) {
        boundingBoxDraggedSide = 3; //right side
        cursor = Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
      } else if(Math.abs(drawPane.yMBB + drawPane.hMBB - mPosPane.y) < clipDistance
          && mPosPane.x > drawPane.xMBB && mPosPane.x < drawPane.xMBB + drawPane.wMBB) {
        boundingBoxDraggedSide = 4; //bottom side
        cursor = Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
      }
      //Then check the diagonals (no elseif is intentional)
      if(mPosPane.distance(drawPane.xMBB, drawPane.yMBB) < clipDistance) {
        boundingBoxDraggedSide = 5; //top left
        cursor = Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
      } else if(mPosPane.distance(drawPane.xMBB + drawPane.wMBB, drawPane.yMBB) < clipDistance) {
        boundingBoxDraggedSide = 6; //top right
        cursor = Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
      } else if(mPosPane.distance(drawPane.xMBB + drawPane.wMBB, drawPane.yMBB + drawPane.hMBB) < clipDistance) {
        boundingBoxDraggedSide = 7; //bottom right
        cursor = Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
      } else if(mPosPane.distance(drawPane.xMBB, drawPane.yMBB + drawPane.hMBB) < clipDistance) {
        boundingBoxDraggedSide = 8; //bottom left
        cursor = Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
      }

      if(boundingBoxDraggedSide != 0) {
        if(e.isShiftDown()) {
          drawPane.xMBBsaved = drawPane.xMBB;
          drawPane.yMBBsaved = drawPane.yMBB;
          drawPane.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
          draggedMode = 11;
        } else if((!macOs && e.isControlDown()) || (macOs && e.isMetaDown())){
          drawPane.setCursor(cursor);
          draggedMode = 12;
        } else if(!e.isAltDown()){
          drawPane.setCursor(cursor);
          draggedMode = 10;
        }
      }
    }

    if (this.eframe != null && tiles.selected != null && tiles.selected.lineType != LineType.GRAB) {
      this.eframe.editPane.upEditPane();
      this.eframe.repaint();
    }
    if (beframe != null)
      beframe.update();

    if (this.drawPane.multiEdit != null)
      this.drawPane.multiEdit.fin();
    System.gc();
  }

  /**
   * Ends any operation mode and deletes too short lines that where created unintentionally.
   *
   * @param e {@link KeyEvent}
   */
  @Override
  public void mouseReleased(MouseEvent e) {

    currX = e.getX();
    currY = e.getY();

    if (SwingUtilities.isRightMouseButton(e))
      return;

    dragDisX = 0;
    dragDisY = 0;

    drawPane.mouseDown = false;

    /*
     * Only modify lines if a line/shape/vertex/floatingObject was dragged,
     * not if background or MBB was dragged
     */
    if(draggedMode <= 4 || draggedMode == 8){
      if (drawPane.shapes.size() > 0) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).resetAtt();
      }

      if (drawPane.lines.size() > 0) {
        drawPane.checkForDoubleVertex(drawPane.lines.get(drawPane.lines.size() - 1).getStart());
        drawPane.checkForDoubleVertex(drawPane.lines.get(drawPane.lines.size() - 1).getEnd());
        drawPane.checkForVertex(drawPane.lines.get(drawPane.lines.size() - 1).getStart());
        drawPane.checkForVertex(drawPane.lines.get(drawPane.lines.size() - 1).getEnd());
      }

      /* cutting any existing lines */
      int index = drawPane.lines.size() - 1;
      if (drawPane.lines.size() > 0) {
        /* delete short lines - created by accident */
        if (drawPane.lines.get(index).length() < 2 * clipDistance || drawPane.lines.get(index).getEndY() == -1 || drawPane.lines.get(index).getEndX() == -1) {
          Point2D end = drawPane.lines.get(index).getEnd();
          Point2D start = drawPane.lines.get(index).getStart();
          drawPane.lines.remove(index);
          drawPane.checkForVertex(end);
          drawPane.checkForVertex(start);
        } else {

          /* cutLine if necessary */
          FeynLine l1D = drawPane.lines.get(index);
          Line newL = new Line(l1D.getStartX(), l1D.getStartY(), l1D.getEndX(), l1D.getEndY());

          Point2D p1 = newL.start;
          for (int i = 0; i < drawPane.lines.size() - 1; i++) {
            FeynLine tempL = drawPane.lines.get(i);
            Point2D pp1 = new Point2D(Math.round(tempL.getStartX()), Math.round(tempL.getStartY()));
            Point2D pp2 = new Point2D(Math.round(tempL.getEndX()), Math.round(tempL.getEndY()));

            if (pp1.distance(p1) < 1)
              continue;
            if (pp2.distance(p1) < 1)
              continue;

            try {
              if (Frame.this.distPointShape(p1, tempL, clipDistance) != null) {
                cutLine(i, p1);
              }
            } catch (Exception ex) {
              ex.printStackTrace();
            }
          }

          Point2D p2 = newL.end;
          for (int i = 0; i < drawPane.lines.size() - 1; i++) {
            FeynLine tempL = drawPane.lines.get(i);
            Point2D pp1 = new Point2D(Math.round(tempL.getStartX()), Math.round(tempL.getStartY()));
            Point2D pp2 = new Point2D(Math.round(tempL.getEndX()), Math.round(tempL.getEndY()));

            if (pp1.distance(p2) < 1)
              continue;
            if (pp2.distance(p2) < 1)
              continue;

            try {
              if (Frame.this.distPointShape(p2, tempL, clipDistance) != null) {
                cutLine(i, p2);
                break;
              }
            } catch (Exception ex) {
              ex.printStackTrace();
            }
          }

          /* combine Line if necessary
           * too time consuming to check all vertices here? -> lagging?*/
          /* is not a problem anymore ?*/
          if (/*tiles.selected.lineType.equals(LineType.GRAB)*/true) {
            HashSet<Point> alreadyChecked = new HashSet<>();

            for (int i = 0; i < drawPane.lines.size(); i++) {
              if (i == index) continue;
              if (!alreadyChecked.contains(new Point((int) Math.round(drawPane.lines.get(i).getStartX()), (int) Math.round(drawPane.lines.get(i).getStartY())))) {
                alreadyChecked.add(new Point((int) Math.round(drawPane.lines.get(i).getStartX()), (int) Math.round(drawPane.lines.get(i).getStartY())));
                combineLines(drawPane.lines.get(i).getStart());
              }
              if (!alreadyChecked.contains(new Point((int) Math.round(drawPane.lines.get(i).getEndX()), (int) Math.round(drawPane.lines.get(i).getEndY())))) {
                alreadyChecked.add(new Point((int) Math.round(drawPane.lines.get(i).getEndX()), (int) Math.round(drawPane.lines.get(i).getEndY())));
                combineLines(drawPane.lines.get(i).getEnd());
              }
            }
          }
        }
      }

      index = drawPane.lines.size() - 1;

      if(!drawPane.lines.isEmpty()) {
      
      /* delete short lines - created by accident */
      if (drawPane.lines.get(index).length() < 2 * clipDistance || drawPane.lines.get(index).getEndY() == -1 || drawPane.lines.get(index).getEndX() == -1) {
        Point2D end = drawPane.lines.get(index).getEnd();
        Point2D start = drawPane.lines.get(index).getStart();
        drawPane.lines.remove(index);
        drawPane.checkForVertex(end);
        drawPane.checkForVertex(start);
      } else {

        /* cutLine if necessary */
        FeynLine l1D = drawPane.lines.get(index);
        Line newL = new Line(l1D.getStartX(), l1D.getStartY(), l1D.getEndX(), l1D.getEndY());

        Point2D p1 = newL.start;
        for (int i = 0; i < drawPane.lines.size() - 1; i++) {
          FeynLine tempL = drawPane.lines.get(i);
          Point2D pp1 = new Point2D(Math.round(tempL.getStartX()), Math.round(tempL.getStartY()));
          Point2D pp2 = new Point2D(Math.round(tempL.getEndX()), Math.round(tempL.getEndY()));

          if (pp1.distance(p1) < 1)
            continue;
          if (pp2.distance(p1) < 1)
            continue;

          try {
            if (Frame.this.distPointShape(p1, tempL, clipDistance) != null) {
              cutLine(i, p1);
            }
          } catch (Exception ex) {
            Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
          }
        }

        Point2D p2 = newL.end;
        for (int i = 0; i < drawPane.lines.size() - 1; i++) {
          FeynLine tempL = drawPane.lines.get(i);
          Point2D pp1 = new Point2D(Math.round(tempL.getStartX()), Math.round(tempL.getStartY()));
          Point2D pp2 = new Point2D(Math.round(tempL.getEndX()), Math.round(tempL.getEndY()));

          if (pp1.distance(p2) < 1)
            continue;
          if (pp2.distance(p2) < 1)
            continue;

          try {
            if (Frame.this.distPointShape(p2, tempL, clipDistance) != null) {
              cutLine(i, p2);
              break;
            }
          } catch (Exception ex) {
            Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
          }
        }

        /* combine Line if necessary
         * too time consuming to check all vertices here? -> lagging?*/
        /* is not a problem anymore ?*/
        if (/*tiles.selected.lineType.equals(LineType.GRAB)*/true) {
          HashSet<Point> alreadyChecked = new HashSet<>();

          for (int i = 0; i < drawPane.lines.size(); i++) {
            if (i == index) continue;
            if (!alreadyChecked.contains(new Point((int) Math.round(drawPane.lines.get(i).getStartX()), (int) Math.round(drawPane.lines.get(i).getStartY())))) {
              alreadyChecked.add(new Point((int) Math.round(drawPane.lines.get(i).getStartX()), (int) Math.round(drawPane.lines.get(i).getStartY())));
              combineLines(drawPane.lines.get(i).getStart());
            }
            if (!alreadyChecked.contains(new Point((int) Math.round(drawPane.lines.get(i).getEndX()), (int) Math.round(drawPane.lines.get(i).getEndY())))) {
              alreadyChecked.add(new Point((int) Math.round(drawPane.lines.get(i).getEndX()), (int) Math.round(drawPane.lines.get(i).getEndY())));
              combineLines(drawPane.lines.get(i).getEnd());
            }
          }
        }
      }
      for (int ind = drawPane.lines.size() - 1 - Math.min(drawPane.lines.size() - 1, 5); ind < drawPane.lines.size(); ind ++) {
        FeynLine line = drawPane.lines.get(ind);
        if (line.length() < clipDistance) {
          Point2D tmpEnd = drawPane.lines.get(ind).getEnd();
          Point2D tmpStart = drawPane.lines.get(ind).getStart();
          drawPane.lines.remove(ind);
          drawPane.checkForVertex(tmpStart);
          drawPane.checkForVertex(tmpEnd);
          ind --;
        }
      }
    }
    }
    Frame.this.repaint();
    if (this.eframe != null) {
      this.eframe.editPane.upEditPane();
      this.eframe.repaint();
    }
    if (beframe != null)
      beframe.update();
    draggedDist = null;
    draggedLine = null;
    if (drawPane.selectType != 4)
      tiles.setGrabMode();
    Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
    drawPane.setCursor(cursor);
  }

  @Override
  public void mouseEntered(MouseEvent e) {

    currX = e.getX();
    currY = e.getY();

  }

  @Override
  public void mouseExited(MouseEvent e) {

    currX = e.getX();
    currY = e.getY();

  }

  /**
   * Handles anything that should happen when the mouse is dragged
   *
   * @param e {@link KeyEvent}
   */
  @Override
  public void mouseDragged(MouseEvent e) {
    currX = e.getX();
    currY = e.getY();
    Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);

    //This of block is for dragging any existing object
    if (tiles.selected != null && tiles.selected.lineType == LineType.GRAB && draggedDist != null) {
      //This will perform an action depending on the draggedMode variable.
      //The value of this variable is set by the mouseDown event that started the drag input

      //ctrl-drag will bend existing lines (always the selected one);
      if (((e.isControlDown() && !macOs) || (e.isMetaDown() && macOs)) && draggedMode == -1) {
        FeynLine fl = drawPane.lines.get(drawPane.lines.size() - 1);
        double theta = Math.atan2(mPosPane.y - fl.getEndY(), mPosPane.x - fl.getEndX());
        theta -= Math.atan2(fl.getStartY() - fl.getEndY(), fl.getStartX() - fl.getEndX());

        double newHeight = fl.getStart().distance(new Point2D(mPosPane.x, mPosPane.y)) * Math.sin( - theta);

        if (Math.abs(newHeight) > 10) {
          fl.height = newHeight;
        } else {
          fl.height = 0;
        }
        //mode=-1 will drag the entire line (grabbed at the midpoint)
      } else if (!e.isShiftDown() && draggedMode == -1) {
        FeynLine fl = drawPane.lines.get(drawPane.lines.size() - 1);
        if (drawPane.lines.size() <= 0)
          return;

        long delX = Math.round(mPosPane.x - draggedDist.x);
        long delY = Math.round(mPosPane.y - draggedDist.y);

        Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D((int) Math.round((draggedLine.getStartX() + delX)), (int) Math.round(draggedLine.getStartY() + delY)), fl); // checking if start point clips anywhere
        Point2D intersect = intersectPair.getKey();

        if (intersect.equals(new Point2D((int) Math.round(draggedLine.getStartX() + delX), (int) Math.round(draggedLine.getStartY() + delY))) || intersectPair.getValue().equals(drawPane.lines.size()) /*|| intersectPair.getValue().equals(0)*/) {
          intersectPair = clipPoint(new Point2D((int) Math.round(draggedLine.getEndX() + delX), (int) Math.round(draggedLine.getEndY() + delY)), fl); // checking if end point clips anywhere
          intersect = intersectPair.getKey();
          delX = (int) -Math.round(draggedLine.getEndX() - intersect.x);
          delY = (int) -Math.round(draggedLine.getEndY() - intersect.y);
        } else {
          delX = (int) -Math.round(draggedLine.getStartX() - intersect.x);
          delY = (int) -Math.round(draggedLine.getStartY() - intersect.y);
        }
        Point2D start = new Point2D((int) Math.round(fl.getStart().x), (int) Math.round(fl.getStart().y));
        Point2D end = new Point2D((int) Math.round(fl.getEnd().x), (int) Math.round(fl.getEnd().y));
        fl.setStartX(draggedLine.getStartX() + delX);
        fl.setStartY(draggedLine.getStartY() + delY);
        fl.setEndX(draggedLine.getEndX() + delX);
        fl.setEndY(draggedLine.getEndY() + delY);
        drawPane.checkForVertexChangeLess(new Point2D((int) Math.round(fl.getStart().x), (int) Math.round(fl.getStart().y)));
        drawPane.checkForVertexChangeLess(new Point2D((int) Math.round(fl.getEnd().x), (int) Math.round(fl.getEnd().y)));
        drawPane.checkForVertexChangeLess(start);
        drawPane.checkForVertexChangeLess(end);
        //drag line start or end
      } else if ((draggedMode == 1 || draggedMode == 0) && !e.isShiftDown()) {
        FeynLine fl = drawPane.lines.get(drawPane.lines.size() - 1);
        if (drawPane.lines.size() <= 0)
          return;

        double angle = 0;
        boolean newAngle = false;
        if (Math.abs(fl.height) > .01 && !fl.getStart().equals(fl.getEnd())) {
          Point2D middle = new Point2D(fl.getStartX()/2 + fl.getEndX()/2, fl.getStartY()/2 + fl.getEndY()/2);
          // angle -= Math.PI;
          if (fl.height > 0) {
            angle = - Math.atan2(middle.y - fl.center.y, middle.x - fl.center.x);
            angle -= Math.PI / 2;
            angle *= -1;
          } else {
            angle = - Math.atan2( - middle.x + fl.center.x, - middle.y + fl.center.y);
            angle -= Math.PI;
          }
          while (angle < 0)
            angle += 2 * Math.PI;
          while (angle > Math.PI*2)
            angle -= Math.PI;
          newAngle = true;
        }

        Point mousePos = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(mousePos, this.drawPane);
        Point2D realMousePos = new Point2D((double) mousePos.x, (double) mousePos.y);

        realMousePos = new Point2D(realMousePos.x / drawPane.scale - drawPane.zeroPoint.x, realMousePos.y / drawPane.scale - drawPane.zeroPoint.y);

        Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(realMousePos.x - dragDisX, realMousePos.y - dragDisY), fl, draggedMode);
        Point2D intersect = intersectPair.getKey();
        double x = intersect.x;
        double y = intersect.y;

        if (draggedMode == 0) {
          Point2D start = new Point2D((int) Math.round(fl.getStart().x), (int) Math.round(fl.getStart().y));
          fl.setStartX(x);
          fl.setStartY(y);
          drawPane.checkForVertexChangeLess(start);
          drawPane.checkForVertexChangeLess(new Point2D((int) Math.round(fl.getStart().x), (int) Math.round(fl.getStart().y)));
        } else {
          Point2D end = new Point2D((int) Math.round(fl.getEnd().x), (int) Math.round(fl.getEnd().y));
          fl.setEndX(x);
          fl.setEndY(y);
          drawPane.checkForVertexChangeLess(end);
          drawPane.checkForVertexChangeLess(new Point2D((int) Math.round(fl.getEnd().x), (int) Math.round(fl.getEnd().y)));
        }
        if (fl.getEnd().equals(fl.getStart()) && Math.abs(fl.height) > .01 && newAngle)
          fl.circAngle = angle;
        //This will drag a vertex
      } else if (draggedMode == 2 && !e.isShiftDown()) {
        Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY), null, -1, drawPane.grid, clipDistance, lineModes.size(), true);
        Point2D intersect = intersectPair.getKey();
        double x = intersect.x;
        double y = intersect.y;
        drawPane.vertices.get(drawPane.vertices.size() - 1).origin = intersect;
        int count = 0;
        for (int i = drawPane.lines.size() - lineModes.size(); i < drawPane.lines.size(); i++) {
          if (lineModes.get(count) == -1) {
            drawPane.lines.get(i).setEndX(x);
            drawPane.lines.get(i).setEndY(y);
          } else if (lineModes.get(count) == 0) {
            drawPane.lines.get(i).setEndX(x);
            drawPane.lines.get(i).setEndY(y);
            drawPane.lines.get(i).setStartX(x);
            drawPane.lines.get(i).setStartY(y);
          } else if (lineModes.get(count) == 1) {
            drawPane.lines.get(i).setStartX(x);
            drawPane.lines.get(i).setStartY(y);
          }
          count++;
        }
      } else if (!e.isShiftDown() && draggedMode == 3) {
        if (drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
          //Point2D p = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
          Point2D p = new Point2D(mPosPane.x, mPosPane.y);
          if (drawPane.grid) {
            int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
            int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
            x += Pane.gridSize / 2;
            y += Pane.gridSize / 2;
            if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
              p = new Point2D(x, y);
            }
          }
          drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.origin = p;
        } else {
          Point2D p = new Point2D(mPosPane.x, mPosPane.y);
          if (drawPane.grid) {
            int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
            int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
            x += Pane.gridSize / 2;
            y += Pane.gridSize / 2;
            if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
              p = new Point2D(x, y);
            }
          }
          drawPane.fObjects.get(drawPane.fObjects.size() - 1).floatingImage.center = p;
        }
      } else if (!e.isShiftDown() && draggedMode == 4) {
        FeynLine line = drawPane.lines.get(drawPane.lines.size() - 1);
        Point2D draggedPlace = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
        if (line.getStart().equals(line.getEnd())) {
          draggedPlace.rotate(line.circAngle, line.getStartX(), line.getStartY());
        }
        if (Math.abs(line.height) < .1) {
          double d = draggedPlace.distance(line.getStart());
          double l = line.getStart().distance(line.getEnd());
          double c = draggedPlace.distance(line.getEnd());
          double a = (d * d - c * c + l * l) / l / 2;
          double h = Math.sqrt(d * d - a * a);
          line.partPlaceDesc = a / l;
          line.descDis = (int) Math.round(h);
          double dd = (draggedPlace.x - line.getStartX()) * (line.getEndY() - line.getStartY()) - (draggedPlace.y - line.getStartY()) * (line.getEndX() - line.getStartX());
          if (dd > 0) {
            line.descDis *= -1;
          }
        } else {
          if (line.height > 0) {
            double arch = Math.atan2(draggedPlace.y - line.center.y, draggedPlace.x - line.center.x);
            while (arch < 0)
              arch += 2 * Math.PI;
            while (arch > 2 * Math.PI)
              arch -= 2 * Math.PI;
            double dArch = arch - line.archStart;
            line.descDis = (int) Math.round(draggedPlace.distance(new Point2D(line.center.x, line.center.y)) - line.radius);
            double deltaArch = line.deltaArch;
            double offsetArch = 2 * Math.PI - deltaArch;
            while (dArch < -offsetArch / 2) {
              dArch += 2 * Math.PI;
            }
            while (dArch > deltaArch + offsetArch / 2) {
              dArch -= 2 * Math.PI;
            }

            line.partPlaceDesc = dArch / line.deltaArch;

          } else {
            double arch = Math.atan2(draggedPlace.y - line.center.y, draggedPlace.x - line.center.x);
            while (arch < 0)
              arch += 2 * Math.PI;
            while (arch > 2 * Math.PI)
              arch -= 2 * Math.PI;
            double dArch = arch - line.archStart;
            line.descDis = (int) Math.round(-draggedPlace.distance(new Point2D(line.center.x, line.center.y)) - line.radius);
            double deltaArch = line.deltaArch;
            double offsetArch = 2 * Math.PI - deltaArch;
            while (dArch < -offsetArch / 2) {
              dArch += 2 * Math.PI;
            }
            while (dArch > deltaArch + offsetArch / 2) {
              dArch -= 2 * Math.PI;
            }

            line.partPlaceDesc = dArch / line.deltaArch;
          }
        }
        //Drag the entire diagram
      } else if (e.isShiftDown() && draggedMode == 7) {
        Point2D offset = new Point2D(e.getX() - draggedDist.x, e.getY() - draggedDist.y);
        if (drawPane.grid) {
          int x = (int) Math.round((offset.x) / Pane.gridSize) * Pane.gridSize;
          int y = (int) Math.round((offset.y) / Pane.gridSize) * Pane.gridSize;
          offset = new Point2D(x, y);
        }

        if (!offset.equals(new Point2D(0, 0))) {

          drawPane.zeroPoint.x += offset.x / drawPane.scale;
          drawPane.zeroPoint.y += offset.y / drawPane.scale;

          draggedDist = new java.awt.geom.Point2D.Double(draggedDist.x + offset.x, draggedDist.y + offset.y);
        }
      } else if (!e.isShiftDown() && draggedMode == 5) {
        Vertex v = drawPane.vertices.get(drawPane.vertices.size() - 1);
        Point2D nPlace = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
        nPlace.x -= v.origin.x;
        nPlace.y -= v.origin.y;
        v.descPosX = (int) Math.round(nPlace.x);
        v.descPosY = (int) Math.round(nPlace.y);
      } else if (!e.isShiftDown() && draggedMode == 6) {
        FloatingObject fO = drawPane.fObjects.get(drawPane.fObjects.size() - 1);
        if (fO.v) {
          Vertex v = fO.vertex;
          Point2D nPlace = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
          nPlace.x -= v.origin.x;
          nPlace.y -= v.origin.y;
          v.descPosX = (int) Math.round(nPlace.x);
          v.descPosY = (int) Math.round(nPlace.y);
        } else {
          FloatingImage fI = fO.floatingImage;
          Point2D nPlace = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
          nPlace.x -= fI.center.x;
          nPlace.y -= fI.center.y;
          fI.descPosX = (int) Math.round(nPlace.x);
          fI.descPosY = (int) Math.round(nPlace.y);
        }
      } else if (draggedMode == 8 && !e.isShiftDown()) {
        Shaped s = drawPane.shapes.get(drawPane.shapes.size() - 1);
        if (s.hoverMode == 1) {
          Point2D p = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
          if (drawPane.grid) {
            int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
            int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
            x += Pane.gridSize / 2;
            y += Pane.gridSize / 2;
            if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
              p = new Point2D(x, y);
            }
          }
          double delX = p.x - s.origin.x;
          double delY = p.y - s.origin.y;
          s.translate(this, delX, delY);
        } else if (s.hoverMode == 2) {
          Point2D newP = new Point2D(mPosPane.x, mPosPane.y);
          newP.x -= s.origin.x;
          newP.y -= s.origin.y;
          newP.rotate(s.rotation, 0, 0);
          double delH = Math.abs(((double) s.height) / 2d) - Math.abs(newP.y);
          s.chHeight(this, - delH);
        } else if (s.hoverMode == 3) {
          Point2D newP = new Point2D(mPosPane.x, mPosPane.y);
          newP.x -= s.origin.x;
          newP.y -= s.origin.y;
          newP.rotate(s.rotation, 0, 0);
          double delW = Math.abs(((double) s.width) / 2d) - Math.abs(newP.x);
          s.chWidth(this, - delW);
        } else if (s.hoverMode == 4) {
          double angle = Math.atan2(mPosPane.y - s.origin.y, mPosPane.x - s.origin.x);
          double delRot = s.rotation - angle + Math.PI/2;
          if (delRot > Math.PI / 2)
            delRot -= Math.PI;
          if (delRot < - Math.PI / 2)
            delRot += Math.PI;
          while (delRot >= Math.PI) {
            delRot -= Math.PI * 2;
          }
          while (delRot < - Math.PI) {
            delRot += Math.PI * 2;
          }
          s.rotate(this, -delRot);
        } else if (s.hoverMode == 5) {
          double delX = mPosPane.x - s.origin.x - s.descPosX;
          double delY = mPosPane.y - s.origin.y - s.descPosY;
          s.descPosX += delX;
          s.descPosY += delY;
        }
      } else if (draggedMode != 7 && draggedMode < 10) { //<10 to ignore the new drag modes
        //        //This branch is triggered when pressing shift while already holding and dragging a line;
        //        //it will bend that line. Not sure what the reason for this branch is, as bending can already be done
        //        //With ctrl/meta-drag instead. draggedMode was always -1 when testing
        //        FeynLine fl = drawPane.lines.get(drawPane.lines.size() - 1);
        //        if (drawPane.lines.size() <= 0)
        //          return;
        //        Point.Double direction = new Point.Double(fl.getEndX() - fl.getStartX(), (fl.getEndY() - fl.getStartY()));
        //        double norm = direction.distance(new Point.Double(0, 0));
        //        direction.x /= norm;
        //        direction.y /= norm;
        //        @SuppressWarnings("SuspiciousNameCombination") Point.Double dire2 = new Point.Double(-direction.y, direction.x);
        //        double addHeight = (mPosPane.x - draggedDist.x) * dire2.x + (mPosPane.y - draggedDist.y) * dire2.y;
        //        if (norm == 0) addHeight = (mPosPane.x - draggedDist.x) + (mPosPane.y - draggedDist.y);
        //        drawPane.lines.get(drawPane.lines.size() - 1).setHeight(draggedLine.getHeight() + addHeight); //TODO why error?
      } else if(draggedMode == 10) {

        Point2D p = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
        if (drawPane.grid) {
          int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
          int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
          x += Pane.gridSize / 2;
          y += Pane.gridSize / 2;
          if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
            p = new Point2D(x, y);
          }
        }

        if(boundingBoxDraggedSide == 1) {
          drawPane.wMBB += drawPane.xMBB - p.x;
          drawPane.xMBB = p.x;
          if(drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 3;
          }
        } else if(boundingBoxDraggedSide == 3) {
          drawPane.wMBB = p.x - drawPane.xMBB;
          if(drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 1;
          }
        } else if(boundingBoxDraggedSide == 2) {
          drawPane.hMBB += drawPane.yMBB - p.y;
          drawPane.yMBB = p.y;
          if(drawPane.hMBB < 0) {
            boundingBoxDraggedSide = 4;
          }
        } else if(boundingBoxDraggedSide == 4) {
          drawPane.hMBB = p.y - drawPane.yMBB;
          if(drawPane.hMBB < 0) {
            boundingBoxDraggedSide = 2;
          }
        } else if(boundingBoxDraggedSide == 5) {
          drawPane.wMBB += drawPane.xMBB - p.x;
          drawPane.xMBB = p.x;
          drawPane.hMBB += drawPane.yMBB - p.y;
          drawPane.yMBB = p.y;
          if(drawPane.hMBB < 0 && drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 7;
          } else if(drawPane.hMBB < 0) {
            boundingBoxDraggedSide = 8;
          } else if(drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 6;
          }
        } else if(boundingBoxDraggedSide == 6) {
          drawPane.wMBB = p.x - drawPane.xMBB;
          drawPane.hMBB += drawPane.yMBB - p.y;
          drawPane.yMBB = p.y;
          if(drawPane.hMBB < 0 && drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 8;
          } else if(drawPane.hMBB < 0) {
            boundingBoxDraggedSide = 7;
          } else if(drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 5;
          }
        } else if(boundingBoxDraggedSide == 7) {
          drawPane.wMBB = p.x - drawPane.xMBB;
          drawPane.hMBB = p.y - drawPane.yMBB;
          if(drawPane.hMBB < 0 && drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 5;
          } else if(drawPane.hMBB < 0) {
            boundingBoxDraggedSide = 6;
          } else if(drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 8;
          }
        } else if(boundingBoxDraggedSide == 8) {
          drawPane.wMBB += drawPane.xMBB - p.x;
          drawPane.xMBB = p.x;
          drawPane.hMBB = p.y - drawPane.yMBB;
          if(drawPane.hMBB < 0 && drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 6;
          } else if(drawPane.hMBB < 0) {
            boundingBoxDraggedSide = 5;
          } else if(drawPane.wMBB < 0) {
            boundingBoxDraggedSide = 7;
          }
        }
        if(mbbFrame != null) mbbFrame.updateMBBsize();
      } else if(draggedMode == 11) {
        double px = mPosPane.x - draggedDist.x + drawPane.xMBBsaved + drawPane.zeroPoint.x;
        double py = mPosPane.y - draggedDist.y + drawPane.yMBBsaved + drawPane.zeroPoint.y;
        if (drawPane.grid) {
          int x = (int) Math.round((px - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
          int y = (int) Math.round((py - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
          x += Pane.gridSize / 2;
          y += Pane.gridSize / 2;
          if (Math.sqrt((x - px) * (x - px) + (y - py) * (y - py)) <= clipDistance) {
            px = x;
            py = y;
          }
        }
        drawPane.xMBB = px;
        drawPane.yMBB = py;
        if(mbbFrame != null) mbbFrame.updateMBBsize();
      } else if(draggedMode == 12) {
        Point2D p = new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY);
        if (drawPane.grid) {
          int x = (int) Math.round((p.x - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
          int y = (int) Math.round((p.y - Pane.gridSize / 2) / Pane.gridSize) * Pane.gridSize;
          x += Pane.gridSize / 2;
          y += Pane.gridSize / 2;
          if (Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)) <= clipDistance) {
            p = new Point2D(x, y);
          }
        }

        if(boundingBoxDraggedSide == 1) {
          drawPane.wMBB += 2*(drawPane.xMBB - p.x);
          drawPane.xMBB = p.x;
          if(drawPane.wMBB < 0) boundingBoxDraggedSide = 3;
        } else if(boundingBoxDraggedSide == 2) {
          drawPane.hMBB += 2*(drawPane.yMBB - p.y);
          drawPane.yMBB = p.y;
          if(drawPane.hMBB < 0) boundingBoxDraggedSide = 4;
        } else if(boundingBoxDraggedSide == 3) {
          double d = p.x - drawPane.xMBB - drawPane.wMBB;
          drawPane.xMBB -= d;
          drawPane.wMBB += 2*d;
          if(drawPane.wMBB < 0) boundingBoxDraggedSide = 1;
        } else if(boundingBoxDraggedSide == 4) {
          double d = p.y - drawPane.yMBB - drawPane.hMBB;
          drawPane.yMBB -= d;
          drawPane.hMBB += 2*d;
          if(drawPane.hMBB < 0) boundingBoxDraggedSide = 2;
        } else if(boundingBoxDraggedSide == 5) {
          double oldRatio = drawPane.wMBB / drawPane.hMBB;
          double dx = drawPane.xMBB - p.x;
          double dy = drawPane.yMBB - p.y;
          if(dx > dy * oldRatio) {
            double nw = dx + drawPane.wMBB;
            drawPane.wMBB = nw;
            drawPane.hMBB = nw / oldRatio;
            drawPane.xMBB -= dx;
            drawPane.yMBB -= dx / oldRatio;
            if(nw < 0) boundingBoxDraggedSide = 7;
          } else {
            double nh = dy + drawPane.hMBB;
            drawPane.wMBB = nh * oldRatio;
            drawPane.hMBB = nh;
            drawPane.xMBB -= dy * oldRatio;
            drawPane.yMBB -= dy;
            if(nh < 0) boundingBoxDraggedSide = 7;
          }
        } else if(boundingBoxDraggedSide == 6) {
          double oldRatio = drawPane.wMBB / drawPane.hMBB;
          double dx = p.x - drawPane.xMBB - drawPane.wMBB;
          double dy = drawPane.yMBB - p.y;
          if(dx > dy * oldRatio) {
            double nw = dx + drawPane.wMBB;
            drawPane.wMBB = nw;
            drawPane.hMBB = nw / oldRatio;
            drawPane.yMBB -= dx / oldRatio;
            if(nw < 0) boundingBoxDraggedSide = 8;
          } else {
            double nh = dy + drawPane.hMBB;
            drawPane.wMBB = nh * oldRatio;
            drawPane.hMBB = nh;
            drawPane.yMBB -= dy;
            if(nh < 0) boundingBoxDraggedSide = 8;
          }
        } else if(boundingBoxDraggedSide == 7) {
          double oldRatio = drawPane.wMBB / drawPane.hMBB;
          double dx = p.x - drawPane.xMBB - drawPane.wMBB;
          double dy = p.y - drawPane.yMBB - drawPane.hMBB;
          if(dx > dy * oldRatio) {
            double nw = dx + drawPane.wMBB;
            drawPane.wMBB = nw;
            drawPane.hMBB = nw / oldRatio;
            if(nw < 0) boundingBoxDraggedSide = 5;
          } else {
            double nh = dy + drawPane.hMBB;
            drawPane.wMBB = nh * oldRatio;
            drawPane.hMBB = nh;
            if(nh < 0) boundingBoxDraggedSide = 5;
          }
        } else if(boundingBoxDraggedSide == 8) {
          double oldRatio = drawPane.wMBB / drawPane.hMBB;
          double dx = drawPane.xMBB - p.x;
          double dy = p.y - drawPane.yMBB - drawPane.hMBB;
          if(dx > dy * oldRatio) {
            double nw = dx + drawPane.wMBB;
            drawPane.wMBB = nw;
            drawPane.hMBB = nw / oldRatio;
            drawPane.xMBB -= dx;
            if(nw < 0) boundingBoxDraggedSide = 6;
          } else {
            double nh = dy + drawPane.hMBB;
            drawPane.wMBB = nh * oldRatio;
            drawPane.hMBB = nh;
            drawPane.xMBB -= dy * oldRatio;
            if(nh < 0) boundingBoxDraggedSide = 6;
          }
        }
      }
      normalizeNegativeMBBSize();
      if(mbbFrame != null) mbbFrame.updateMBBsize();

      Frame.this.repaint();
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (beframe != null)
        beframe.update();
      return;
    }

    //This if block is used for drawing a new line
    if (drawPane.mouseDown && drawPane.lines.size() > 0 && draggedMode != 2 && draggedMode != 3) {
      int i = drawPane.lines.size() - 1;

      Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x - dragDisX, mPosPane.y - dragDisY), drawPane.lines.get(i), 1);
      Point2D intersect = intersectPair.getKey();
      Point2D end = drawPane.lines.get(i).getEnd();
      drawPane.lines.get(i).setEndX(intersect.x);
      drawPane.lines.get(i).setEndY(intersect.y);
      drawPane.checkForVertexChangeLess(end);
      drawPane.checkForVertexChangeLess(new Point2D((int) Math.round(drawPane.lines.get(i).getEnd().x), (int) Math.round(drawPane.lines.get(i).getEnd().y)));
      Frame.this.repaint();
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (beframe != null)
        beframe.update();
    }
  }

  private void normalizeNegativeMBBSize() {
    if(drawPane.wMBB < 0) {
      drawPane.xMBB += drawPane.wMBB;
      drawPane.wMBB = -drawPane.wMBB;
    }
    if(drawPane.hMBB < 0) {
      drawPane.yMBB += drawPane.hMBB;
      drawPane.hMBB = -drawPane.hMBB;
    }
  }

  /**
   * Handles the highlighting and tooltip while hovering over elements.
   *
   * @param e {@link MouseEvent}
   */
  @Override
  public void mouseMoved(MouseEvent e) {
    currX = e.getX();
    currY = e.getY();
    Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
    for (int index = 0; index < drawPane.lines.size(); index++) {
      drawPane.lines.get(index).draggedMode = 2;
    }
    for (int index = 0; index < drawPane.vertices.size(); index++) {
      drawPane.vertices.get(index).hover = false;
    }
    for (int index = 0; index < drawPane.shapes.size(); index++) {
      drawPane.shapes.get(index).hoverMode = 0;
    }

    for (FloatingObject fO : drawPane.fObjects) {
      if (fO.v)
        fO.vertex.hover = false;
    }

    if (tiles == null)
      return;

    if (drawPane.fObjects.size() > 0 && tiles.selected != null && tiles.selected.lineType == LineType.GRAB) {
      int index = 0;
      for (FloatingObject fO : drawPane.fObjects) {
        if (fO.v) {
          if (fO.vertex.showDesc) {
            Point2D place = new Point2D(fO.vertex.origin.x + fO.vertex.descPosX, fO.vertex.origin.y + fO.vertex.descPosY);
            if (place.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
              if (toolTip) {
                drawPane.toolTipMsg = "Move description";
                drawPane.setToolTipText("Move description");
              }
              return;
            }
          }
          if (fO.vertex.origin.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
            fO.vertex.hover = true;
            if (toolTip) {
              drawPane.toolTipMsg = "Move Object";
              drawPane.setToolTipText("Move Object");
            }
            break;
          }
        } else {
          if (fO.floatingImage.showDesc) {
            Point2D place = new Point2D(fO.floatingImage.center.x + fO.floatingImage.descPosX, fO.floatingImage.center.y + fO.floatingImage.descPosY);
            if (place.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
              if (toolTip) {
                drawPane.toolTipMsg = "Move description";
                drawPane.setToolTipText("Move description");
              }
              return;
            }
          }
          if (fO.floatingImage.center.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
            if (toolTip) {
              drawPane.toolTipMsg = "Move Object";
              drawPane.setToolTipText("Move Object");
            }
            break;
          }
        }
        index++;
      }
      if (index != drawPane.fObjects.size())
        return;
    }
    if (drawPane.vertices.size() > 0 && tiles.selected != null && tiles.selected.lineType == LineType.GRAB) {
      int indexx = 0;
      for (Vertex v : this.drawPane.vertices) {
        if (v.showDesc) {
          Point2D place = new Point2D(v.origin.x + v.descPosX, v.origin.y + v.descPosY);
          if (place.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
            if (toolTip) {
              drawPane.toolTipMsg = "Move description";
              drawPane.setToolTipText("Move description");
            }
            return;
          }
          indexx++;
        }
      }
    }

    if (tiles.selected != null && tiles.selected.lineType == LineType.GRAB && drawPane.lines.size() > 0) {
      for (FeynLine line : drawPane.lines) {
        if (line.showDesc) {
          double len = Math.sqrt((line.getStartX() - line.getEndX()) * (line.getStartX() - line.getEndX()) + (line.getStartY() - line.getEndY()) * (line.getStartY() - line.getEndY()));
          Point2D place;
          if (Math.abs(line.height) < .1) {

            double normtanx = line.getStartX() - line.getEndX();
            normtanx /= len;
            double normtany = line.getStartY() - line.getEndY();
            normtany /= len;
            place = new Point2D(line.getStartX() - (len * line.partPlaceDesc) * normtanx + (line.descDis) * normtany, line.getStartY() - (len * line.partPlaceDesc) * normtany - (line.descDis) * normtanx);
          } else {
            line.calculateArch();
            double partArch = line.deltaArch * line.partPlaceDesc;
            double placeArch = line.archStart + partArch;
            double radius = line.radius + line.descDis;
            double x;
            double y;
            if (line.height > 0) {
              y = line.center.y + radius * Math.sin(placeArch);
              x = line.center.x + radius * Math.cos(placeArch);
            } else {
              y = line.center.y - radius * Math.sin(placeArch);
              x = line.center.x - radius * Math.cos(placeArch);
            }
            place = new Point2D(x, y);
            if (line.getStart().equals(line.getEnd())) {
              place.rotate( - line.circAngle, line.getStartX(), line.getStartY());
            }
          }

          if (place.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
            if (toolTip) {
              drawPane.toolTipMsg = "Move description";
              drawPane.setToolTipText("Move description");
            }
            return;
          }
        }
      }
    }

    if (tiles.selected != null && tiles.selected.lineType == LineType.GRAB && drawPane.shapes.size() > 0) {
      for (Shaped s : drawPane.shapes) {
        if (s.showDesc) {
          Point2D descPoint = new Point2D(s.origin.x + s.descPosX, s.origin.y + s.descPosY);
          if (descPoint.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
            s.hoverMode = 5;
            if (toolTip) {
              drawPane.toolTipMsg = "Move description";
              drawPane.setToolTipText("Move description");
            }
            return;
          }
        }
        Map.Entry<Point2D, Boolean> p = s.nPoint(new Point2D(mPosPane.x, mPosPane.y), 5d);
        if (p.getValue()) {
          Point2D point = new Point2D(mPosPane.x - s.origin.x, mPosPane.y - s.origin.y);
          point.rotate(s.rotation, 0, 0);
          double angle = 180 /Math.PI * Math.atan2(point.y / s.height * 2, point.x / s.width * 2);
          if ((45 < angle && angle <= 135) || (-135 < angle && angle < -45)) {
            s.hoverMode = 2;
            if (toolTip) {
              drawPane.toolTipMsg = "Stretch shape";
              drawPane.setToolTipText("Stretch shape");
            }
            return;
          } else {
            s.hoverMode = 3;
            if (toolTip) {
              drawPane.toolTipMsg = "Stretch shape";
              drawPane.setToolTipText("Stretch shape");
            }
            return;
          }
        }
        if (s.origin.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10) {
          s.hoverMode = 1;
          if (toolTip) {
            drawPane.toolTipMsg = "Move shape";
            drawPane.setToolTipText("Move shape");
          }
          return;
        }
        Point2D pp = new Point2D(mPosPane.x - s.origin.x, mPosPane.y - s.origin.y);
        double r = pp.distance(new Point2D(0, 0));
        if (r > (double) s.height / 4 && r < (double) s.height * 3 / 4) {
          pp.rotate(s.rotation, 0, 0);
          double angle = 180 /Math.PI * Math.atan2(pp.y / s.height * 2, pp.x / s.width * 2);
          if (Math.abs(Math.abs(angle) - 90) * r < 2.5 * 180 / Math.PI) {
            s.hoverMode = 4;
            if (toolTip) {
              drawPane.toolTipMsg = "Rotate shape";
              drawPane.setToolTipText("Rotate shape");
            }
            return;
          }
        }
      }
    }
    String tpmsg = "";
    if (tiles.selected != null && tiles.selected.lineType == LineType.GRAB) {
      drawPane.toolTipMsg = null;
      try {
        Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x, mPosPane.y), null, -1, false, 5);
        int index = intersectPair.getValue();
        Point2D intersect = intersectPair.getKey();
        if (index > 0) {
          index--;
          FeynLine fl = drawPane.lines.get(index);

          if (Math.abs(fl.height) < .1) {
            double lenFl = Math.sqrt((fl.getStart().x - fl.getEnd().x) * (fl.getStart().x - fl.getEnd().x) + (fl.getStart().y - fl.getEnd().y) * (fl.getStart().y - fl.getEnd().y));
            double disToStart = Math.sqrt((fl.getStart().x - intersect.x) * (fl.getStart().x - intersect.x) + (fl.getStart().y - intersect.y) * (fl.getStart().y - intersect.y));
            if (lenFl > 4 * disToStart) {
              if (!drawPane.lines.get(index).fixStart) {
                drawPane.lines.get(index).draggedMode = 0;
                tpmsg = "<html><table><td>drag:</td><td>move end of line</td><tr><td>wheel:</td><td>bend line</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>";
              }
            } else if (lenFl * 3 < 4 * disToStart) {
              if (!drawPane.lines.get(index).fixEnd) {
                drawPane.lines.get(index).draggedMode = 1;
                tpmsg = "<html><table><td>drag:</td><td>move end of line</td><tr><td>wheel:</td><td>bend line</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>";
              }
            } else {
              if (!drawPane.lines.get(index).fixEnd && !drawPane.lines.get(index).fixStart) {
                drawPane.lines.get(index).draggedMode = -1;
                tpmsg = "<html><table><td>drag:</td><td>move entire line</td><tr><td>wheel:</td><td>bend line</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>";
              }
            }
          } else {
            double archEndGes;
            double archStartGes;
            double deltaArchGes;
            if (fl.getStart().equals(fl.getEnd())) {
              intersect.x -= fl.getEndX();
              intersect.y -= fl.getEndY();
              double tmpX = intersect.x;
              intersect.x = (int) Math.round(intersect.x * Math.cos( - fl.circAngle) + intersect.y * Math.sin( - fl.circAngle));
              intersect.y = (int) Math.round(tmpX * Math.sin( - fl.circAngle) + intersect.y * Math.cos( - fl.circAngle));
              intersect.x += fl.getEndX();
              intersect.y += fl.getEndY();
              deltaArchGes = Math.PI * 2;

            } else {
              archEndGes = Math.atan2(fl.getEnd().y - fl.center.y, fl.getEnd().x - fl.center.x);
              archStartGes = Math.atan2(fl.getStart().y - fl.center.y, fl.getStart().x - fl.center.x);
              deltaArchGes = (-archStartGes + archEndGes) * - Math.signum(fl.height);
            }

            while (deltaArchGes > 2 * Math.PI) {
              deltaArchGes -= 2 * Math.PI;
            }
            while (deltaArchGes <= 0) {
              deltaArchGes += 2 * Math.PI;
            }

            double archEndTeil = Math.atan2(intersect.y - fl.center.y, intersect.x - fl.center.x);
            double archStartTeil = Math.atan2(fl.getStart().y - fl.center.y, fl.getStart().x - fl.center.x);
            double deltaArchTeil = (-archStartTeil + archEndTeil) * - Math.signum(fl.height);
            while (deltaArchTeil >= 2 * Math.PI) {
              deltaArchTeil -= 2 * Math.PI;
            }
            while (deltaArchTeil < 0) {
              deltaArchTeil += 2 * Math.PI;
            }

            if (deltaArchGes / 4 > deltaArchTeil) {
              if (!drawPane.lines.get(index).fixStart) {
                drawPane.lines.get(index).draggedMode = 0;
                tpmsg = "<html><table><td>drag:</td><td>move end of line</td><tr><td>wheel:</td><td>bend line</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>";
              }
            } else if (deltaArchGes * 3 / 4 < deltaArchTeil) {
              if (!drawPane.lines.get(index).fixEnd) {
                drawPane.lines.get(index).draggedMode = 1;
                tpmsg = "<html><table><td>drag:</td><td>move end of line</td><tr><td>wheel:</td><td>bend line</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>";
              }
            } else {
              if (!drawPane.lines.get(index).fixEnd && !drawPane.lines.get(index).fixStart) {
                drawPane.lines.get(index).draggedMode = -1;
                tpmsg = "<html><table><td>drag:</td><td>move entire line</td><tr><td>wheel:</td><td>bend line</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>";
              }
            }
          }

          Frame.this.repaint();
          if (!tpmsg.equals("") && toolTip) {
            drawPane.toolTipMsg = tpmsg;
            drawPane.setToolTipText(tpmsg);
          } else {
            drawPane.toolTipMsg = null;
            drawPane.setToolTipText(null);
          }
        } else if (index < 0) {
          index = -index - 1;
          drawPane.vertices.get(index).hover = true;
          if (toolTip) {
            drawPane.toolTipMsg = "<html><table><td>drag:</td><td>Move Vertex</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>";
            drawPane.setToolTipText("<html><table><td>drag:</td><td>Move Vertex</td><tr><td>press e:</td><td>open <tt>EditFrame</tt></td></table></html>");
          }
        }
      } catch (Exception ex) {
        drawPane.toolTipMsg = null;
        drawPane.setToolTipText(null);
      }
    }
  }

  /**
   * Changes the Cursor of the {@link drawPane} back to default
   */
  public void changeCursorToDefault() {
    drawPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  /**
   * Changes the Cursor of the {@link drawPane} to grab mode
   */
  public void changeCursorToGrab() {
    drawPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
  }

  /**
   * Prints the {@link drawPane} with the {@link PrintUtilities}-class
   */
  private void printDrawPane() {
    new Print(this);
  }
  /**
   * Exports the {@link drawPane} with the {@link ExportUI}-class
   */
  private void exportDrawPane() {
    ExportManager.doGuiExport(this, false);
  }

  /**
   * Handles all the actions performed by the mouse wheel
   *
   * @param e {@link MouseWheelEvent}
   */

  @Override
  public void mouseWheelMoved(MouseWheelEvent e) {
    if (drawPane.lines.size() <= 0 && drawPane.fObjects.size() <= 0 && drawPane.shapes.size() <= 0) return;

    if (this.scrollCounter == 0) {
      this.saveForUndo();
      if (drawPane.selectType == 3) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).getAtt(Frame.this);
      }
    }

    if (wheelMovementTimer != null && wheelMovementTimer.isRunning()) {
      wheelMovementTimer.stop();
    }
    wheelMovementTimer = new javax.swing.Timer(500, new WheelMovementTimerActionListener());
    wheelMovementTimer.setRepeats(false);
    wheelMovementTimer.start();

    try {
      this.eframe.editPane.upEditPane();
    } catch (NullPointerException ignored) {
    }
    if (beframe != null)
      beframe.update();

    this.scrollCounter = 1;

    boolean capsIsOn = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

    if (this.gameMode == 1) {
      if (drawPane.selectType == 0 && drawPane.lines.size() > 0) {
        if (Math.abs(drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation()) < 10 && Math.abs(scrollHeight + (double) e.getWheelRotation()) < 10) {
          if (drawPane.lines.get(drawPane.lines.size() - 1).height != 0) {
            scrollHeight = drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation();
          } else {
            scrollHeight += e.getWheelRotation();
          }
          drawPane.lines.get(drawPane.lines.size() - 1).height = 0;
        } else if (Math.abs(drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation()) < 10) {
          drawPane.lines.get(drawPane.lines.size() - 1).height = scrollHeight + (double) e.getWheelRotation();
          scrollHeight = 0;
        } else {
          drawPane.lines.get(drawPane.lines.size() - 1).addHeight(e.getWheelRotation());
        }
      }
      e.consume();
      Frame.this.repaint();
      try {
        this.eframe.editPane.upEditPane();
      } catch (NullPointerException ignored) {
      }
      if (beframe != null)
        beframe.update();
      return;
    }

    if (drawPane.selectType == 0 && drawPane.lines.size() > 0) {
      if (!e.isAltDown() && ((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !capsIsOn) {
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleOffset += e.getWheelRotation() * drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleOffset / drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength;
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength += e.getWheelRotation();
        if (drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength < 5) {
          drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength = 5;
        }
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleHeight += e.getWheelRotation();
        FeynLine fl = drawPane.lines.get(drawPane.lines.size() - 1);
        if (fl.lineConfig.lineType == LineType.GLUON || fl.lineConfig.lineType == LineType.PHOTON || fl.lineConfig.lineType == LineType.SSPIRAL || fl.lineConfig.lineType == LineType.SWAVE) {
          if (fl.descDis > 0) {
            fl.descDis += e.getWheelRotation();
          } else {
            fl.descDis -= e.getWheelRotation();
          }
        }
      } else if (capsIsOn && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleOffset += e.getWheelRotation() * drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleOffset / drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength;
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength += e.getWheelRotation();
        if (drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength < 5) {
          drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleLength = 5;
        }
      } else if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown() && capsIsOn) {
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleHeight += e.getWheelRotation();
        FeynLine fl = drawPane.lines.get(drawPane.lines.size() - 1);
        double multiplier = 0;
        if (drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.lineType == LineType.PHOTON) {
          multiplier = 19/25;
        } else if (drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.lineType == LineType.GLUON || drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.lineType == LineType.SSPIRAL) {
          if (drawPane.lines.get(drawPane.lines.size() - 1).descDis > 0) {
            multiplier = 2/5;
          } else {
            multiplier = 3/4;
          }
        }
        if (fl.descDis > 0) {
          fl.descDis += multiplier * e.getWheelRotation();
        } else {
          fl.descDis -= multiplier * e.getWheelRotation();
        }
      } else if (e.isAltDown() && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !capsIsOn) {
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.stroke = Math.max(drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.stroke - e.getWheelRotation(), 1);
        if (drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.stroke < 0) {
          drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.stroke = 0;
        }
      } else if (e.isAltDown() && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && capsIsOn) {
        drawPane.lines.get(drawPane.lines.size() - 1).lineConfig.wiggleOffset += e.getWheelRotation();
      } else if (drawPane.grid) {
        if (Math.abs(drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation()) < 10 && Math.abs(scrollHeight + (double) e.getWheelRotation()) < 10) {
          if (drawPane.lines.get(drawPane.lines.size() - 1).height != 0) {
            scrollHeight = drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation();
          } else {
            scrollHeight += e.getWheelRotation();
          }
          drawPane.lines.get(drawPane.lines.size() - 1).height = 0;
        } else if (Math.abs(drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation()) < 10) {
          drawPane.lines.get(drawPane.lines.size() - 1).height = scrollHeight + (double) e.getWheelRotation();
          scrollHeight = 0;
        } else {
          drawPane.lines.get(drawPane.lines.size() - 1).addHeight(e.getWheelRotation());
        }
      } else {
        if (Math.abs(drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation()) < 10 && Math.abs(scrollHeight + (double) e.getWheelRotation()) < 10) {
          if (drawPane.lines.get(drawPane.lines.size() - 1).height != 0) {
            scrollHeight = drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation();
          } else {
            scrollHeight += e.getWheelRotation();
          }
          drawPane.lines.get(drawPane.lines.size() - 1).height = 0;
        } else if (Math.abs(drawPane.lines.get(drawPane.lines.size() - 1).height + (double) e.getWheelRotation()) < 10) {
          drawPane.lines.get(drawPane.lines.size() - 1).height = scrollHeight + (double) e.getWheelRotation();
          scrollHeight = 0;
        } else {
          drawPane.lines.get(drawPane.lines.size() - 1).addHeight(e.getWheelRotation());
        }
      }
    } else if (drawPane.selectType == 1 && drawPane.vertices.size() > 0) {
      if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
        this.drawPane.vertices.get(drawPane.vertices.size() - 1).border.stroke = Math.max(drawPane.vertices.get(drawPane.vertices.size() - 1).border.stroke - e.getWheelRotation(), 0);
      } else if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
        this.drawPane.vertices.get(drawPane.vertices.size() - 1).size += e.getWheelRotation();
        this.drawPane.vertices.get(drawPane.vertices.size() - 1).size = Math.max(0, this.drawPane.vertices.get(drawPane.vertices.size() - 1).size);
      } else if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && e.isAltDown()) {
        this.drawPane.vertices.get(drawPane.vertices.size() - 1).rotation += e.getWheelRotation();
        this.drawPane.vertices.get(drawPane.vertices.size() - 1).rotation = Math.max(0, this.drawPane.vertices.get(drawPane.vertices.size() - 1).rotation);
      }
    } else if (drawPane.selectType == 2 && drawPane.fObjects.size() > 0) {
      if (drawPane.fObjects.get(drawPane.fObjects.size() - 1).v) {
        if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
          this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.border.stroke = Math.max(drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.border.stroke - e.getWheelRotation(), 0);
        } else if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
          this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.size += e.getWheelRotation();
          this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.size = Math.max(0, this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.size);
        } else if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && e.isAltDown()) {
          this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.rotation += e.getWheelRotation();
          this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.rotation = Math.max(0, this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).vertex.rotation);
        }
      } else {
        if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown()))) {
          this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).floatingImage.scale += .01 * e.getWheelRotation();
        } else if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown()))) {
          this.drawPane.fObjects.get(drawPane.fObjects.size() - 1).floatingImage.rotation += e.getWheelRotation();
        }
      }
    } else if (drawPane.selectType == 3) {
      if (!e.isAltDown() && ((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !capsIsOn) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleOffset += e.getWheelRotation() * drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleOffset / drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength;
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength += e.getWheelRotation();
        if (drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength < 5) {
          drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength = 5;
        }
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleHeight += e.getWheelRotation();
        /* for description */
        // Shaped fl = drawPane.shapes.get(drawPane.shapes.size() - 1);
        // if (fl.lineConfig.lineType == LineType.GLUON || fl.lineConfig.lineType == LineType.PHOTON) {
        //  if (fl.descDis > 0) {
        //    fl.descDis += e.getWheelRotation();
        //  } else {
        //    fl.descDis -= e.getWheelRotation();
        //  }
        // }
      } else if (capsIsOn && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleOffset += e.getWheelRotation() * drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleOffset / drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength;
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength += e.getWheelRotation();
        if (drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength < 5) {
          drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleLength = 5;
        }
      } else if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown() && capsIsOn) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleHeight += e.getWheelRotation();
        /* for moving description */
        // Shaped fl = drawPane.shapes.get(drawPane.shapes.size() - 1);
        // double multiplier = 0;
        // if (drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.lineType == LineType.PHOTON) {
        //  multiplier = 19/25;
        // } else if (drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.lineType == LineType.GLUON) {
        //  if (drawPane.shapes.get(drawPane.shapes.size() - 1).descDis > 0) {
        //    multiplier = 2/5;
        //  } else {
        //    multiplier = 3/4;
        //  }
        // }
        // if (fl.descDis > 0) {
        //  fl.descDis += multiplier * e.getWheelRotation();
        // } else {
        //  fl.descDis -= multiplier * e.getWheelRotation();
        // }
      } else if (e.isAltDown() && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !capsIsOn) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.stroke = Math.max(drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.stroke - e.getWheelRotation(), 1);
        if (drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.stroke < 0) {
          drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.stroke = 0;
        }
      } else if (e.isAltDown() && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && capsIsOn) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).lineConfig.wiggleOffset += e.getWheelRotation();
      } else {
        Shaped s = drawPane.shapes.get(drawPane.shapes.size() - 1);
        double min = Math.min(s.width, s.height);
        if (min + e.getWheelRotation() < 10)
          return;
        s.chWidth(this, Math.signum(e.getWheelRotation()) * Math.max(((double) s.width)/min*Math.abs(e.getWheelRotation()), 1));
        s.chHeight(this, Math.signum(e.getWheelRotation()) * Math.max(((double) s.height)/min*Math.abs(e.getWheelRotation()), 1));
        if (s.height == 0) {
          s.chHeight(this, 1);
        }
        if (s.width == 0) {
          s.chWidth(this, 1);
        }
      }
    } else if (drawPane.selectType == 4 && drawPane.multiEdit != null) {
      if (drawPane.multiEdit.type == 0) {
        if (!e.isAltDown() && ((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !capsIsOn) {
          drawPane.multiEdit.setWiggleOffset(drawPane.multiEdit.getWiggleOffset() + e.getWheelRotation() * drawPane.multiEdit.getWiggleOffset() / drawPane.multiEdit.getWiggleLength());

          drawPane.multiEdit.setWiggleLength(drawPane.multiEdit.getWiggleLength() + e.getWheelRotation());
          if (drawPane.multiEdit.getWiggleLength() < 5) {
            drawPane.multiEdit.setWiggleLength(5);
          }
          drawPane.multiEdit.setWiggleHeight(drawPane.multiEdit.getWiggleHeight() + e.getWheelRotation());
        } else if (capsIsOn && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
          drawPane.multiEdit.setWiggleOffset(drawPane.multiEdit.getWiggleOffset() + e.getWheelRotation() * drawPane.multiEdit.getWiggleOffset() / drawPane.multiEdit.getWiggleLength());

          drawPane.multiEdit.setWiggleLength(drawPane.multiEdit.getWiggleLength() + e.getWheelRotation());
          if (drawPane.multiEdit.getWiggleLength() < 5) {
            drawPane.multiEdit.setWiggleLength(5);
          }
        } else if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown() && capsIsOn) {
          drawPane.multiEdit.setWiggleHeight(drawPane.multiEdit.getWiggleHeight() + e.getWheelRotation());
        } else if (e.isAltDown() && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !capsIsOn) {
          drawPane.multiEdit.setStroke(Math.max(drawPane.multiEdit.getStroke() + e.getWheelRotation(), 1));
        } else if (e.isAltDown() && !((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && capsIsOn) {
          drawPane.multiEdit.setWiggleOffset(drawPane.multiEdit.getWiggleOffset() + e.getWheelRotation());
        }
      } else if (drawPane.multiEdit.type == 1) {
        if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
          drawPane.multiEdit.setStroke(Math.max(drawPane.multiEdit.getStroke() + e.getWheelRotation(), 0));
        } else if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && !e.isAltDown()) {
          drawPane.multiEdit.setSize(Math.max(drawPane.multiEdit.getSize() + e.getWheelRotation(), 0));
        } else if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown())) && e.isAltDown()) {
          drawPane.multiEdit.setRotation(Math.max(drawPane.multiEdit.getRotation() + e.getWheelRotation(), 0));
        }
      } else if (drawPane.multiEdit.type == 2) {
        if (!((e.isControlDown() && !macOs) || (macOs && e.isMetaDown()))) {
          drawPane.multiEdit.setScale(drawPane.multiEdit.getScale() + .01 * e.getWheelRotation());
        } else if (((e.isControlDown() && !macOs) || (macOs && e.isMetaDown()))) {
          drawPane.multiEdit.setRotation(Math.max(drawPane.multiEdit.getRotation() + e.getWheelRotation(), 0));
        }
      }
      tilePanel.repaint();
    }
    Frame.this.repaint();
    e.consume();
    try {
      this.eframe.editPane.upEditPane();
    } catch (NullPointerException ignored) {
    }
    if (beframe != null)
      beframe.update();
  }

  /**
   * assigns null to the {@link ui.Frame#eframe}
   */
  public void derefEframe() {
    //    this.eframe = null;
    //TODO can this method be removed
  }

  /**
   * assigns null to the {@link ui.Frame#beframe}
   */
  public void derefBEframe() {
    this.beframe = null;
  }

  /**
   * Saves the current lines and vertices so that it can be used later on
   */
  public void saveForUndo() {

    ArrayList<FeynLine> tmpArrayList = new ArrayList<>();
    for (FeynLine line : drawPane.lines) {
      tmpArrayList.add(line.getCopy());
    }
    drawPane.undoListLines.add(new ArrayList<>(tmpArrayList));
    drawPane.redoListLines = new ArrayList<>();
    while (drawPane.undoListLines.size() > drawPane.maxHistory) {
      drawPane.undoListLines.remove(0);
    }
    ArrayList<Vertex> tmpArrayListV = new ArrayList<>();
    for (Vertex v : drawPane.vertices) {
      tmpArrayListV.add(new Vertex(v));
    }
    drawPane.undoListVertices.add(new ArrayList<>(tmpArrayListV));
    drawPane.redoListVertices = new ArrayList<>();
    while (drawPane.undoListVertices.size() > drawPane.maxHistory) {
      drawPane.undoListVertices.remove(0);
    }
    ArrayList<FloatingObject> tmpArrayListF = new ArrayList<>();
    for (FloatingObject v : drawPane.fObjects) {
      tmpArrayListF.add(new FloatingObject(v));
    }
    drawPane.undoListfObjects.add(new ArrayList<>(tmpArrayListF));
    drawPane.redoListfObjects = new ArrayList<>();
    while (drawPane.undoListfObjects.size() > drawPane.maxHistory) {
      drawPane.undoListfObjects.remove(0);
    }
    ArrayList<Shaped> tmpArrayListS = new ArrayList<>();
    for (Shaped v : drawPane.shapes) {
      tmpArrayListS.add(v.getCopy());
    }
    drawPane.undoListShapes.add(new ArrayList<>(tmpArrayListS));
    drawPane.redoListShapes = new ArrayList<>();
    while (drawPane.undoListShapes.size() > drawPane.maxHistory) {
      drawPane.undoListShapes.remove(0);
    }
  }

  /**
   * What happens when time is up
   */
  private void timesUp() {
    JOptionPane.showMessageDialog(this, "<html>The time is up. You reached <b, style=\"color:Red\">" + this.points + "</b> points.", "Time is up", JOptionPane.PLAIN_MESSAGE);
    this.remTime = totTime;
    this.points = 0;
    this.pointsLabel.setText("<html><h2, style=\"color:Blue\"> 0");
    this.loadChallenge();
  }

  /**
   * Getter function for the EditFrame ({@link ui.Frame#eframe})
   *
   * @return {@link eframe}
   */
  public EditFrame getEframe() {
    return eframe;
  }

  /**
   * Function to set an image as the filling of the last {@link game.Vertex} of the {@link ui.Pane#vertices}
   */
  public void selectImage() {
    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What image do you want to load?");
    fc.setApproveButtonText("Load");

    if (this.imgLoadPath != null) {
      fc.setCurrentDirectory(new File(this.imgLoadPath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("Joint Photographic Experts Group (*.jpg/*.jpeg)", "jpg", "jpeg");
    fc.addChoosableFileFilter(filter);
    FileFilter filter1 = new FileNameExtensionFilter("Portable Network Graphics (*.png)", "png");
    fc.addChoosableFileFilter(filter1);
    FileFilter filter2 = new FileNameExtensionFilter("Graphics Interchange Format (*.gif)", "gif");
    fc.addChoosableFileFilter(filter2);
    FileFilter filter3 = new FileNameExtensionFilter("Device-Independent Bitmap (*.bmp)", "bmp");
    fc.addChoosableFileFilter(filter3);
    FileFilter filter4 = new FileNameExtensionFilter("JavaFx files (*.fx)", "fx");
    fc.addChoosableFileFilter(filter4);

    int returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        File file = fc.getSelectedFile();
        this.imgLoadPath = file.getParentFile().getAbsolutePath();
        String path = file.getPath();
        if (path.endsWith(".fx")) {
          Vertex v = this.drawPane.vertices.get(this.drawPane.vertices.size() - 1);
          this.drawPane.vertices.get(this.drawPane.vertices.size() - 1).filling.name = Filling.addPattern(path);
          if (v.filling.color.equals(v.border.color)) {
            v.filling.color = Frame.compColor(v.filling.color);
          }
        } else {
          this.drawPane.vertices.get(this.drawPane.vertices.size() - 1).filling.imageNumber = Filling.getImageNumber(path);
          this.drawPane.vertices.get(this.drawPane.vertices.size() - 1).filling.graphic = true;
          this.drawPane.vertices.get(this.drawPane.vertices.size() - 1).filling.name = null;
        }
        Frame.this.repaint();
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (this.beframe != null) {
          beframe.update();
        }
      } catch (Exception ex) {
        Frame.this.requestFocus();
        Frame.logger.severe("Error while loading image)");
      }
    }
  }
  /**
   * Function to set an image as the filling of the {@link game.MultiEdit}
   */
  public void selectImageME() {
    if (drawPane.multiEdit.type != 1) {
      return;
    }
    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What image do you want to load?");
    fc.setApproveButtonText("Load");

    if (this.imgLoadPath != null) {
      fc.setCurrentDirectory(new File(this.imgLoadPath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("Joint Photographic Experts Group (*.jpg/*.jpeg)", "jpg", "jpeg");
    fc.addChoosableFileFilter(filter);
    FileFilter filter1 = new FileNameExtensionFilter("Portable Network Graphics (*.png)", "png");
    fc.addChoosableFileFilter(filter1);
    FileFilter filter2 = new FileNameExtensionFilter("Graphics Interchange Format (*.gif)", "gif");
    fc.addChoosableFileFilter(filter2);
    FileFilter filter3 = new FileNameExtensionFilter("Device-Independent Bitmap (*.bmp)", "bmp");
    fc.addChoosableFileFilter(filter3);
    FileFilter filter4 = new FileNameExtensionFilter("JavaFx files (*.fx)", "fx");
    fc.addChoosableFileFilter(filter4);

    int returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        File file = fc.getSelectedFile();
        this.imgLoadPath = file.getParentFile().getAbsolutePath();
        String path = file.getPath();
        if (path.endsWith(".fx")) {
          drawPane.multiEdit.setName(Filling.addPattern(path));
          if (drawPane.multiEdit.getFillColor().equals(drawPane.multiEdit.getBorderColor())) {
            drawPane.multiEdit.setFillColor(Frame.compColor(drawPane.multiEdit.getFillColor()));
          }
        } else {
          drawPane.multiEdit.setImageNumber(Filling.getImageNumber(path));
        }
        Frame.this.repaint();
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (this.beframe != null) {
          beframe.update();
        }
      } catch (Exception ex) {
        Frame.this.requestFocus();
        Frame.logger.severe("Error while loading image)");
      }
    }
  }
  /**
   * returns the composite color of the input color
   * @param color the color of which the composite is asked for
   * @return the composite color
   */
  public static Color compColor(Color color) {
    return new Color(255 - color.getRed(), 255 - color.getBlue(), 255 - color.getGreen(), color.getAlpha());
  }

  /**
   * Function to set an image as the filling of the last {@link game.Vertex} of the {@link ui.Pane#fObjects}
   */
  public void selectImageFl() {
    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What image do you want to load?");
    fc.setApproveButtonText("Load");

    if (this.imgLoadPath != null) {
      fc.setCurrentDirectory(new File(this.imgLoadPath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("Joint Photographic Experts Group (*.jpg/*.jpeg)", "jpg", "jpeg");
    fc.addChoosableFileFilter(filter);
    FileFilter filter1 = new FileNameExtensionFilter("Portable Network Graphics (*.png)", "png");
    fc.addChoosableFileFilter(filter1);
    FileFilter filter2 = new FileNameExtensionFilter("Graphics Interchange Format (*.gif)", "gif");
    fc.addChoosableFileFilter(filter2);
    FileFilter filter3 = new FileNameExtensionFilter("Device-Independent Bitmap (*.bmp)", "bmp");
    fc.addChoosableFileFilter(filter3);
    FileFilter filter4 = new FileNameExtensionFilter("JavaFx files (*.fx)", "fx");
    fc.addChoosableFileFilter(filter4);

    int returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        File file = fc.getSelectedFile();
        this.imgLoadPath = file.getParentFile().getAbsolutePath();
        String path = file.getPath();

        if (path.endsWith(".fx")) {
          Vertex v = this.drawPane.fObjects.get(this.drawPane.fObjects.size() - 1).vertex;
          this.drawPane.fObjects.get(this.drawPane.fObjects.size() - 1).vertex.filling.name = Filling.addPattern(path);
          if (v.filling.color.equals(v.border.color)) {
            v.filling.color = Frame.compColor(v.filling.color);
          }
        } else {
          this.drawPane.fObjects.get(this.drawPane.fObjects.size() - 1).vertex.filling.imageNumber = Filling.getImageNumber(path);
          this.drawPane.fObjects.get(this.drawPane.fObjects.size() - 1).vertex.filling.graphic = true;
          this.drawPane.fObjects.get(this.drawPane.fObjects.size() - 1).vertex.filling.name = null;
        }
        this.drawPane.selectType = 2;
        Frame.this.repaint();
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (this.beframe != null) {
          beframe.update();
        }
      } catch (Exception ex) {
        Frame.this.requestFocus();
        Frame.logger.severe("Error while loading image)");
      }
    }
  }
  /**
   * Function to set an image as the filling of the last {@link game.Shaped} of the {@link ui.Pane#shapes}
   */
  public void selectImageSFl() {
    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What image do you want to load?");
    fc.setApproveButtonText("Load");

    if (this.imgLoadPath != null) {
      fc.setCurrentDirectory(new File(this.imgLoadPath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("Joint Photographic Experts Group (*.jpg/*.jpeg)", "jpg", "jpeg");
    fc.addChoosableFileFilter(filter);
    FileFilter filter1 = new FileNameExtensionFilter("Portable Network Graphics (*.png)", "png");
    fc.addChoosableFileFilter(filter1);
    FileFilter filter2 = new FileNameExtensionFilter("Graphics Interchange Format (*.gif)", "gif");
    fc.addChoosableFileFilter(filter2);
    FileFilter filter3 = new FileNameExtensionFilter("Device-Independent Bitmap (*.bmp)", "bmp");
    fc.addChoosableFileFilter(filter3);
    FileFilter filter4 = new FileNameExtensionFilter("JavaFx files (*.fx)", "fx");
    fc.addChoosableFileFilter(filter4);

    int returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        File file = fc.getSelectedFile();
        this.imgLoadPath = file.getParentFile().getAbsolutePath();
        String path = file.getPath();

        if (path.endsWith(".fx")) {
          Shaped sh = this.drawPane.shapes.get(this.drawPane.shapes.size() - 1);
          this.drawPane.shapes.get(this.drawPane.shapes.size() - 1).fill.name = Filling.addPattern(path);

          if (sh.fill.color.equals(sh.lineConfig.color)) {
            sh.fill.color = Frame.compColor(sh.fill.color);
          }
        } else {
          this.drawPane.shapes.get(this.drawPane.shapes.size() - 1).fill.imageNumber = Filling.getImageNumber(path);
          this.drawPane.shapes.get(this.drawPane.shapes.size() - 1).fill.graphic = true;
          this.drawPane.shapes.get(this.drawPane.shapes.size() - 1).fill.name = null;
        }
        this.drawPane.selectType = 3;
        Frame.this.repaint();
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (this.beframe != null) {
          beframe.update();
        }
      } catch (Exception ex) {
        Frame.this.requestFocus();
        Frame.logger.severe("Error while loading image)");
      }
    }
  }
  /**
   * Function to add an {@link game.FloatingImage} to {@link Pane#fObjects}.
   */
  public void addImage() {
    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What image do you want to load?");
    fc.setApproveButtonText("Load");

    if (this.imgLoadPath != null) {
      fc.setCurrentDirectory(new File(this.imgLoadPath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("Joint Photographic Experts Group (*.jpg/*.jpeg)", "jpg", "jpeg");
    fc.addChoosableFileFilter(filter);
    FileFilter filter1 = new FileNameExtensionFilter("Portable Network Graphics (*.png)", "png");
    fc.addChoosableFileFilter(filter1);
    FileFilter filter2 = new FileNameExtensionFilter("Graphics Interchange Format (*.gif)", "gif");
    fc.addChoosableFileFilter(filter2);
    FileFilter filter3 = new FileNameExtensionFilter("Device-Independent Bitmap (*.bmp)", "bmp");
    fc.addChoosableFileFilter(filter3);
    FileFilter filter4 = new FileNameExtensionFilter("JavaFx files (*.fx)", "fx");
    fc.addChoosableFileFilter(filter4);

    int returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        File file = fc.getSelectedFile();
        this.imgLoadPath = file.getParentFile().getAbsolutePath();
        String path = file.getPath();

        this.drawPane.fObjects.add(new FloatingObject(path, new Point2D(currX, currY)));
        Frame.this.repaint();
        this.drawPane.selectType = 2;
        if (this.eframe != null) {
          this.eframe.editPane.upEditPane();
          this.eframe.repaint();
        }
        if (this.beframe != null) {
          beframe.update();
        }
      } catch (Exception ex) {
        Frame.this.requestFocus();
        Frame.logger.severe("Error while loading image)");
      }
    }
  }

  public void copyToClipboard() {
    copShape = null;
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    if (this.drawPane.selectType == 0 && this.drawPane.lines.size() > 0) {
      String line = this.drawPane.lines.get(this.drawPane.lines.size() - 1).lineConfig.toFile(false); //TODO is false the correct choice here?
      clipboard.setContents(new StringSelection(line), null);
      Frame.statusLabel.setText("Copied content to clipboard");
      this.copyHeight = this.drawPane.lines.get(this.drawPane.lines.size() - 1).height;
      this.disCopyLen = new Point2D(this.drawPane.lines.get(this.drawPane.lines.size() - 1).getStartX() - this.drawPane.lines.get(this.drawPane.lines.size() - 1).getEndX(), this.drawPane.lines.get(this.drawPane.lines.size() - 1).getStartY() - this.drawPane.lines.get(this.drawPane.lines.size() - 1).getEndY());
    } else if (this.drawPane.selectType == 1 && this.drawPane.vertices.size() > 0) {
      clipboard.setContents(new StringSelection(this.drawPane.vertices.get(this.drawPane.vertices.size() - 1).toFile()), null);
      Frame.statusLabel.setText("Copied content to clipboard");
    } else if (this.drawPane.selectType == 2 && this.drawPane.fObjects.size() > 0) {
      clipboard.setContents(new StringSelection(this.drawPane.fObjects.get(this.drawPane.fObjects.size() - 1).toFile()), null);
      Frame.statusLabel.setText("Copied content to clipboard");
    } else if (drawPane.selectType == 3) {
      this.copShape = this.drawPane.shapes.get(this.drawPane.shapes.size() - 1).getCopy();
      clipboard.setContents(new StringSelection("ShapeSelected"), null);
      Frame.statusLabel.setText("Copied content to clipboard");
    }
  }

  public void pasteFromClipBoard() {
    Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    Transferable cbCont = clipboard.getContents(null);
    if (cbCont != null && cbCont.isDataFlavorSupported(DataFlavor.imageFlavor)) {
      try {

        Image image = (Image) cbCont.getTransferData(DataFlavor.imageFlavor);
        BufferedImage img;
        try {
          img = (BufferedImage) image;
        } catch (java.lang.ClassCastException castEx) {
          BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
          Graphics2D bGr = bimage.createGraphics();
          bGr.drawImage(image, 0, 0, null);
          bGr.dispose();
          img = bimage;
        }

        String path = System.getProperty("user.dir");
        String extension = "png";
        Random r = new Random();
        String a = "";
        a += (char) (r.nextInt(26) + 'a');
        a += (char) (r.nextInt(26) + 'a');
        a += (char) (r.nextInt(26) + 'a');
        File file = new File(path + File.separator + File.separator + a + ".png");

        while (file.exists()) {
          a += (char) (r.nextInt(26) + 'a');
          file = new File(path + File.separator + File.separator + a + ".png");
        }

        ImageIO.write(img, extension, file);

        drawPane.fObjects.add(new FloatingObject(file.getAbsolutePath(), mPosPane));

        file.delete();
      } catch (Exception e) {
        Frame.logger.severe("Error occured while pasting image: " + e);
      }

    } else if (cbCont != null && cbCont.isDataFlavorSupported(DataFlavor.stringFlavor)) {
      String result = "";
      try {
        result = (String) cbCont.getTransferData(DataFlavor.stringFlavor);
      } catch (UnsupportedFlavorException | IOException ex) {
        Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
      }
      try {
        if (result.startsWith("[")) {
          result = result.substring(1, result.length() - 1);
          Point2D start = new Point2D(
              disCopyLen.x / 2 + mPosPane.x,
              disCopyLen.y / 2 + mPosPane.y
              );
          Point2D end = new Point2D(
              -disCopyLen.x / 2 + mPosPane.x,
              -disCopyLen.y / 2 + mPosPane.y
              );
          LineConfig lc = new LineConfig(LineConfig.toConf(result.replaceAll("\\s+", "").split("[;,]")));
          FeynLine line = new FeynLine(start, end, lc, copyHeight);
          drawPane.lines.add(line);
          drawPane.selectType = 0;
        } else if (result.startsWith("(")) {
          result = result.substring(1, result.length() - 1);
          FloatingObject fO = new FloatingObject(new Vertex(Vertex.toConf(result.replaceAll("\\s+", "").split("[;,]")).getKey()), mPosPane);
          drawPane.fObjects.add(fO);
          drawPane.selectType = 2;
        } else if (result.startsWith("|")) {
          result = result.substring(1, result.length() - 1);
          FloatingObject fO = new FloatingObject(new FloatingImage(FloatingImage.toConf(result.replaceAll("\\s+", "").split("[;,]"))), mPosPane);
          drawPane.fObjects.add(fO);
          drawPane.selectType = 2;
        } else if (result.equals("ShapeSelected") && copShape != null) {
          Shaped sh = copShape.getCopy();
          sh.origin.x = mPosPane.x;
          sh.origin.y = mPosPane.y;
          drawPane.shapes.add(sh);
          return;
        } else { //If none of the above apply, attempt to run the code for qgraf import
          QgrafImportManager.doClipboardGuiImport(this, result);
        }
      } catch (Exception e) {
        Frame.logger.log(Level.SEVERE, "Error occured while pasting", e);
        //        System.err.println("Error occured while pasting: " + e);
        //        e.printStackTrace(); //TODO remove
        throw new RuntimeException(e);
      }
    }
    Frame.this.repaint();
  }

  private final class FrameTransferHandler extends TransferHandler {
    private static final long serialVersionUID = -4821090100035973353L;

    @Override
    @SuppressWarnings("deprecation")
    public boolean importData(TransferSupport support) {
      if(!support.isDrop()) return false;
      if(support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
        try {
          @SuppressWarnings("unchecked")
          List<File> files = (List<File>) support.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
          if(files.size() != 1) {
            JOptionPane.showMessageDialog(Frame.this, "Dropped " + files.size() + " files, expected 1",
                "Error", JOptionPane.ERROR_MESSAGE);
            return false;
          }

          Path path = files.get(0).toPath();
          //Handle both .fg files and qgraf files with any ending
          if(path.getFileName().toString().endsWith(".fg")) { //FeynGame save file
            Frame.this.loadDiagram(path, new Pane(Frame.this.drawPane, true));
          } else { //Treat as qgraf file
            QgrafImportManager.doGuiImport(Frame.this, path);
          }
          return true;

        } catch (UnsupportedFlavorException e) {
          throw new RuntimeException(e); //Should have been checked by above condizion
        } catch (IOException e) {
          JOptionPane.showMessageDialog(Frame.this, "Cannot access dropped resource: "
              + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
          return false;
        }
      } else if(support.isDataFlavorSupported(DataFlavor.plainTextFlavor)) {
        try {
          Reader reader = DataFlavor.plainTextFlavor.getReaderForText(support.getTransferable());
          String text = new BufferedReader(reader).lines().collect(
              Collectors.joining(System.lineSeparator(), "", System.lineSeparator())); //Trailing linebreak required
          QgrafImportManager.doClipboardGuiImport(Frame.this, text); //Use clipboard logic here, this will not ask for styles
          return true;
        } catch (UnsupportedFlavorException e) {
          JOptionPane.showMessageDialog(Frame.this, "Plaintext not supported for dragged data"
              + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
          return false;
        } catch (IOException e) {
          JOptionPane.showMessageDialog(Frame.this, "Cannot access dropped resource: "
              + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
          return false;
        }
      } else {
        return false;
      }
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean canImport(TransferSupport support) {
      if(!support.isDrop()) return false;
      if(!support.isDataFlavorSupported(DataFlavor.javaFileListFlavor) &&
          !support.isDataFlavorSupported(DataFlavor.plainTextFlavor)) return false;
      support.setDropAction(DnDConstants.ACTION_COPY);
      return true;
    }

  }



  /**
   * loads the config/model file specified by "path"
   *
   * @param path the path to the file to be loaded
   */
  public void loadConfig(String path) {
    this.configPath = path;
    this.loadConfig();
    if (path != null && !path.trim().isEmpty())
      Frame.statusLabel.setText("Loaded model: " + path);
  }

  /**
   * reloads the currently active config/model file
   */
  public void loadConfig() {
    if (gameMode == 0)
      this.tileTabs.saveCurrent();

    if (this.configPath != null) {
      LineConfig.initPresets(this.configPath);
    } else {
      LineConfig.initPresets();
    }
    game.Vertex.initPresets();
    if (this.configPath != null) {
      game.Vertex.initPresets(this.configPath);
    }

    if (this.configPath != null) {
      game.FloatingImage.initPresets(this.configPath);
    }

    if (gameMode == 0) {
      this.tileTabs.addModel(new Presets(), this.configPath);
    }
    if (tileTabs != null)
      tileTabs.update();
    else
      this.reloadTiles();
  }
  /**
   * reloads the tiles object, is actually the same as the function below
   */
  public void reloadTiles() {
    reloadPresets();
  }

  /**
   * Resize the Tiles (change number of columns)
   */
  private void resizeTiles() {
    reloadPresets(false);
  }

  public void reloadPresets() {
    reloadPresets(true);
  }

  /**
   * reloads the presets for LineConig/Vertex/Images
   */
  public void reloadPresets(boolean pack) {
    if (tiles != null && tileScroll != null) {
      tileScroll.remove(tiles);
      tilePanel.remove(tileScroll);
    }
    if (gameMode == 1 && tiles != null) {
      tilePanel.remove(tiles);
    }
    this.tiles = new TileSelect(this);
    int numberOfTiles = Vertex.vertexPresets.size() + LineConfig.presets.size() + FloatingImage.fIPresets.size();
    if (gameMode == 1) {
      numberOfTiles = LineConfig.presets.size();
    }
    tiles.setPreferredSize(new Dimension(Math.min((numberOfTiles), TileSelect.columns) * (TileSelect.uiHeight+10), (int) (Math.ceil((numberOfTiles) / (float) TileSelect.columns)) * (TileSelect.uiHeight+10)));
    if (gameMode == 0) {
      tileScroll = new JScrollPane(tiles);
      tileScroll.setPreferredSize(new Dimension(Math.min((numberOfTiles), TileSelect.columns) * (TileSelect.uiHeight+10) + 10, (int) Math.min(Math.ceil((numberOfTiles) / (float) TileSelect.columns), 3) * (TileSelect.uiHeight + 10) + 10));
      tilePanel.add(tileScroll, BorderLayout.SOUTH);
    } else {
      tiles.setPreferredSize(tiles.getPreferredSize());
      tilePanel.add(tiles, BorderLayout.SOUTH);
    }
    this.drawPane.setPreferredSize(this.drawPaneSize);
    this.getContentPane().validate();
    this.getContentPane().repaint();
    if (pack) { this.pack(); }
    this.updateEframe();
    Frame.this.repaint();
  }

  /**
   * Updates the tiles in the EditFrame after config/model file update
   */
  public void updateEframe() {
    if (this.eframe != null) {
      this.eframe.editPane.remove(this.eframe.editPane.preSelLin);
      this.eframe.editPane.remove(this.eframe.editPane.preSelVer);
      this.eframe.editPane.remove(this.eframe.editPane.preSelImg);
      this.eframe.editPane.preSelLin = new PresetSelectLines(this);
      this.eframe.editPane.preSelVer = new PresetSelectVertex(this);
      this.eframe.editPane.preSelImg = new PresetSelectImage(this);
      this.eframe.editPane.preSelLin.setAlignmentX(LEFT_ALIGNMENT);
      this.eframe.editPane.preSelVer.setAlignmentX(LEFT_ALIGNMENT);
      this.eframe.editPane.preSelImg.setAlignmentX(LEFT_ALIGNMENT);
      this.eframe.editPane.add(this.eframe.editPane.preSelLin);
      this.eframe.editPane.add(this.eframe.editPane.preSelVer);
      this.eframe.editPane.add(this.eframe.editPane.preSelImg);
      this.eframe.editPane.upEditPane();
      this.eframe.getContentPane().validate();
      this.eframe.getContentPane().repaint();
    }
  }

  /**
   * opens filechooser to choose file from where to load the diagram
   */
  @SuppressWarnings("unchecked")
  private void loadDiagram() {
    final JFileChooser fc = new PreviewFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What file do you want to load?");

    if (this.diagramSavePath != null) {
      fc.setCurrentDirectory(new File(this.diagramSavePath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("FeynGame files (*.fg)", "fg");
    fc.addChoosableFileFilter(filter);

    int returnVal;
    returnVal = fc.showOpenDialog(Frame.this);

    Pane oldPane = new Pane(this.drawPane, true);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = fc.getSelectedFile();
      if (!file.exists()) {
        String absPath = file.getAbsolutePath();
        if (!absPath.endsWith(".fg"))
          absPath += ".fg";
        file = new File(absPath);
        if (!file.exists()) {
          JOptionPane.showMessageDialog(this, "Selected file " + file.getAbsolutePath() + " does not exist", "File does not exist", JOptionPane.ERROR_MESSAGE);
          return;
        }
      }
      this.openNew();
      boolean success = this.loadDiagram(file, oldPane, false);
      if(success) this.convCanToModel();

    }
    Frame.this.repaint();
    //    Frame.this.requestFocus();
  }

  public void loadDiagram(String fgFile) {
    loadDiagram(new File(fgFile), null, false);
  }

  public boolean loadDiagram(String fgFile, Pane oldPane) {
    return loadDiagram(new File(fgFile), oldPane, false);
  }

  public boolean loadDiagram(Path fgFile, Pane oldPane) {
    return loadDiagram(fgFile.toFile(), oldPane, false);
  }

  /**
   * sets the input of the canvas to the diagram specified in the fg file
   * @param fgFile the fg_file to open
   * @param oldPane backup if loading fails
   * @param initialLoad Only pass true if loading last.fg. This will prevent the file from overwriting some defaults
   */
  @SuppressWarnings("unchecked")
  public boolean loadDiagram(File file, Pane oldPane, boolean initialLoad) {
    this.openNew();

    if (! file.isFile()) {
      if (oldPane != null) {
        Container paneLocation = drawPane.getParent();
        paneLocation.remove(drawPane);
        drawPane.terminateThread();
        drawPane = oldPane;
        paneLocation.add(drawPane, BorderLayout.CENTER, 0);
        drawPane.addMouseListener(this);
        drawPane.addMouseMotionListener(this);
        drawPane.revalidate();
      } else {
        this.clearCanvas(CLEARMODE_CLEAR_ALL);
      }
      Frame.logger.log(Level.WARNING, "Tried to load non-existing file at " + file.getAbsolutePath());
      JOptionPane.showMessageDialog(new JFrame(), "Failed to load the diagram at:\n" + file.getAbsolutePath() + "\nIt does not exist!", "Failed to load diagram", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (file.getName().lastIndexOf(".")>0) { fileNameProp = file.getName().substring(0,file.getName().lastIndexOf(".")); }
    else { fileNameProp = file.getName(); }

    this.setTitle(file.getName() + " - " + "FeynGame v" + this.version);
    if (!file.getName().matches("last\\.fg"))
      Frame.statusLabel.setText("Loaded diagram: " + file.getPath());
    if(!initialLoad && resetExportDestinationOnLoad) ExportManager.setNextGuiExportDestination(file, true);
    this.currentFile = file.getAbsolutePath();
    this.diagramSavePath = file.getParentFile().getAbsolutePath();
    if (this.diagramSavePath == null) {
      String filePath = new String(this.currentFile);
      int lastIndex = filePath.lastIndexOf(System.getProperty("file.separator"));
      this.diagramSavePath = filePath.substring(0, lastIndex);
    }
    String diaVersion = "";
    try (FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis)) {
      diaVersion = (String) ois.readObject();
      try {
        drawPane.lines = (ArrayList<FeynLine>) ois.readObject();
        boolean earlierThanAFA = false;
        if (diaVersion.equals("1.0.1") || diaVersion.equals("1.0.0"))
          earlierThanAFA = true;
        for (FeynLine line : drawPane.lines) {
          if (line.lineConfig.lineType == LineType.SCALAR) {
            earlierThanAFA = true;
            break;
          }
        }
        if (earlierThanAFA) {
          for (FeynLine line : drawPane.lines) {
            if (line.lineConfig.lineType == LineType.SCALAR) {
              line.lineConfig.lineType = LineType.FERMION;
            } else if (line.lineConfig.lineType == LineType.FERMION) {
              line.lineConfig.showArrow = true;
            }
          }
        }
        for (FeynLine line: drawPane.lines) {
          if (line.lineConfig.arrowHeight <= 0 && line.lineConfig.showArrow) line.lineConfig.arrowHeight = line.lineConfig.arrowSize;
        }
        drawPane.vertices = (ArrayList<Vertex>) ois.readObject();
        drawPane.fObjects = (ArrayList<FloatingObject>) ois.readObject();
        game.Filling.pathToImageNumber = (HashMap<String, Integer>) ois.readObject();
        drawPane.redoListLines = (ArrayList<ArrayList<FeynLine>>) ois.readObject();
        drawPane.redoListVertices = (ArrayList<ArrayList<Vertex>>) ois.readObject();
        drawPane.redoListfObjects = (ArrayList<ArrayList<FloatingObject>>) ois.readObject();
        drawPane.undoListLines = (ArrayList<ArrayList<FeynLine>>) ois.readObject();
        drawPane.undoListVertices = (ArrayList<ArrayList<Vertex>>) ois.readObject();
        drawPane.undoListfObjects = (ArrayList<ArrayList<FloatingObject>>) ois.readObject();
        this.wobbleFloatingVertex();
        try {
          drawPane.shapes = (ArrayList<Shaped>) ois.readObject();
          drawPane.undoListShapes = (ArrayList<ArrayList<Shaped>>) ois.readObject();
          drawPane.redoListShapes = (ArrayList<ArrayList<Shaped>>) ois.readObject();
        } catch (EOFException ignored) {
          drawPane.shapes = new ArrayList<>();
          drawPane.undoListShapes = new ArrayList<>();
          drawPane.redoListShapes = new ArrayList<>();
        }
        try {
          this.readNumberToImage(ois);
        } catch (Exception exception) {
          ArrayList<String> deletedIs = new ArrayList<>();

          for (String key : game.Filling.pathToImageNumber.keySet()) {

            try {
              game.Filling.imageNumberToImage.put(game.Filling.pathToImageNumber.get(key), ImageIO.read(new File(key)));
            } catch (IOException iex) {
              Frame.logger.severe("Image at " + key + " could not be loaded.");
              deletedIs.add(key);
            }
          }
          for (String key: deletedIs) {
            this.imageDeleted(key);
          }

          this.checkJavaFX();
        }
        try {
          game.Filling.nameToJVG = (HashMap<String, JavaVectorGraphic>) ois.readObject();
          this.addPatternsFX();
        } catch (Exception ex101) {
          this.checkJavaFX();
        }
        try {
          drawPane.scale = (double) ois.readObject();
          drawPane.zeroPoint = (Point2D) ois.readObject();
          drawPane.mBB = (boolean) ois.readObject();
          drawPane.xMBB = (double) ois.readObject();
          drawPane.yMBB = (double) ois.readObject();
          drawPane.wMBB = (double) ois.readObject();
          drawPane.hMBB = (double) ois.readObject();
        } catch (Exception ex102) {
          drawPane.scale = 1;
          drawPane.zeroPoint = new Point2D(0, 0);
          drawPane.mBB = false;
          drawPane.xMBB = 0.;
          drawPane.yMBB = 0.;
          drawPane.wMBB = 0.;
          drawPane.hMBB = 0.;
        }
      } catch (Exception ex1) {
        if (!version.equals("")) {

          JOptionPane.showMessageDialog(new JFrame(), "Failed to load the diagram at:\n" + file.getAbsolutePath() + "\nIt was saved with version " + diaVersion + " and you are running version " + version + "!", "Failed to load diagram", JOptionPane.ERROR_MESSAGE);
        }
        Frame.logger.log(Level.SEVERE, ex1.getMessage(), ex1);
      }
      try {
        game.Filling.newImageNumber = Collections.max(game.Filling.imageNumberToImage.keySet()) + 1;
      } catch (Exception ex) {
        game.Filling.newImageNumber = 0;
      }
      Frame.this.repaint();
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (this.beframe != null) {
        beframe.update();
      }
      if(!initialLoad) recents.addEntry(this.currentFile);
      return true;
    } catch (StreamCorruptedException | EOFException scex) {
      //drawPane is added to the canvasPanel, which is added to the content pane,
      //So find this canvas panel first; and also add back to the same location
      if (oldPane != null) {
        Container paneLocation = drawPane.getParent();
        paneLocation.remove(drawPane);
        drawPane.terminateThread();
        drawPane = oldPane;
        paneLocation.add(drawPane, BorderLayout.CENTER, 0);
        drawPane.addMouseListener(this);
        drawPane.addMouseMotionListener(this);
        drawPane.revalidate();
      } else {
        this.clearCanvas(CLEARMODE_CLEAR_ALL);
      }
      JOptionPane.showMessageDialog(this, "The given file was not an FeynGame diagram file!\n" + scex, "Failed to load file", JOptionPane.ERROR_MESSAGE);
      return false;
    } catch(IOException ex) {
      if (oldPane != null) {
        Container paneLocation = drawPane.getParent();
        paneLocation.remove(drawPane);
        drawPane.terminateThread();
        drawPane = oldPane;
        paneLocation.add(drawPane, BorderLayout.CENTER, 0);
        drawPane.addMouseListener(this);
        drawPane.addMouseMotionListener(this);
        drawPane.revalidate();
      } else {
        this.clearCanvas(CLEARMODE_CLEAR_ALL);
      }
      
      JOptionPane.showMessageDialog(this, "The file " + file.getAbsolutePath() + " can not be accessed or does not exist", "File IO Error", JOptionPane.ERROR_MESSAGE);
      Frame.logger.log(Level.SEVERE, "", ex);
      return false;
    } catch (Exception ex) {
      if (!version.equals("")) {
        String thisVersion = "";
        try {
          URL resource = getClass().getResource("/date.txt");
          byte[] readData = new byte[100];
          int read = resource.openStream().read(readData);
          if (read > 0)
            thisVersion = new String(readData);
        } catch (Exception ignored) {
        }
        thisVersion = thisVersion.replaceAll(System.getProperty("line.separator"), "");
        thisVersion = thisVersion.replaceAll("\n", "");
        if(oldPane != null) oldPane.terminateThread();
        JOptionPane.showMessageDialog(new JFrame(), "Failed to load the diagram at:\n" + diagramSavePath + "\nIt was saved with version " + version + "and you are running version " + thisVersion + "!", "Failed to load diagram", JOptionPane.ERROR_MESSAGE);
      }
      Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
      return false;
    } finally {
      if(oldPane != null) oldPane.terminateThread();
    }
  }
  /**
   * after a new Diagram was loaded, this may or may not ask if the user wants to convert the diagram
   * to a model
   * @return returns true if and only if a ConvCanToModelDialog was shown
   */
  public boolean convCanToModel() {

    if (drawPane.lines.size() + drawPane.vertices.size() + drawPane.fObjects.size() == 0)
      return false;
    if (this.convCanToModel == 2) {
      String name = "";
      if (this.currentFile != null)
        name = this.currentFile;
      if (name.endsWith(".fg"))
        name = name.substring(0, name.length() - 3);

      if (name != "")
        name += ".model";
      tileTabs.addModel(new Presets(drawPane), name);
      return false;
    } else if (this.convCanToModel == 0) {
      String name = "";
      if (this.currentFile != null)
        name = this.currentFile;
      if (name.endsWith(".fg"))
        name = name.substring(0, name.length() - 3);
      if (name != "")
        name += ".model";
      new ConvCanToModelDialog(this, drawPane, name);
      return true;
    }
    return false;
  }
  /**
   * adds a pattern to the known patterns
   */
  private void addPatternsFX() {
    for (String key : game.Filling.nameToJVG.keySet()) {
      if (!javafx.JavaFXToJavaVectorGraphic.patternsIn.contains(key)) {
        if (!javafx.JavaFXToJavaVectorGraphic.patternsOut.contains(key)) {
          javafx.JavaFXToJavaVectorGraphic.patternsOut.add(key);
        }
      }
    }
  }

  /**
   * saves the diagram to file chosen by FileChooser
   */
  private void saveAsDiagram() {
    final JFileChooser fc = new JFileChooser();

    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.SAVE_DIALOG);
    fc.setDialogTitle("Where do you want to save the Feynman diagram?");

    if (this.diagramSavePath != null) {
      fc.setCurrentDirectory(new File(this.diagramSavePath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.setSelectedFile(new File(fc.getCurrentDirectory() + "/" + Frame.fileNameProp + ".fg"));

    if (this.currentFile != null && !this.currentFile.equals("")) {
      fc.setSelectedFile(new File(this.currentFile));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("FeynGame files (*.fg)", "fg");
    fc.addChoosableFileFilter(filter);

    int returnVal;
    returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = fc.getSelectedFile();
      this.diagramSavePath = file.getParentFile().getAbsolutePath();
      String withExtension = fc.getSelectedFile().getAbsolutePath();
      if (!withExtension.toLowerCase().endsWith(".fg"))
        withExtension += ".fg";
      this.currentFile = withExtension;
      file = new File(this.currentFile);
      if (file.exists()) {
        Object[] options = {"Overwrite File", "Cancel"};
        int n = JOptionPane.showOptionDialog(
            this,
            "Do you want to overwrite the existing file",
            "Overwrite file?",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[0]);
        if (n == 1) {
          return;
        }
      }
      this.saveDiagram();
    }
    Frame.this.requestFocus();
  }

  /**
   * exports an image of the current diagram to the clipboard
   */
  private void saveImageToCB() {
    ExportManager.doClipboardGuiExport(this);
  }

  /**
   * undos the last step
   */
  private void undo() {
    try {

      ArrayList<FeynLine> tmpArrayList = new ArrayList<>();
      for (FeynLine line : drawPane.lines) {
        tmpArrayList.add(line.getCopy());
      }
      drawPane.redoListLines.add(new ArrayList<>(tmpArrayList));
      while (drawPane.undoListLines.size() > drawPane.maxHistory) {
        drawPane.redoListLines.remove(0);
      }
      ArrayList<Vertex> tmpArrayListV = new ArrayList<>();
      for (Vertex v : drawPane.vertices) {
        tmpArrayListV.add(new Vertex(v));
      }
      drawPane.redoListVertices.add(new ArrayList<>(tmpArrayListV));
      while (drawPane.undoListVertices.size() > drawPane.maxHistory) {
        drawPane.redoListVertices.remove(0);
      }
      ArrayList<FloatingObject> tmpArrayListF = new ArrayList<>();
      for (FloatingObject v : drawPane.fObjects) {
        tmpArrayListF.add(new FloatingObject(v));
      }
      drawPane.redoListfObjects.add(new ArrayList<>(tmpArrayListF));
      while (drawPane.undoListfObjects.size() > drawPane.maxHistory) {
        drawPane.redoListfObjects.remove(0);
      }
      ArrayList<Shaped> tmpArrayListS = new ArrayList<>();
      for (Shaped v : drawPane.shapes) {
        tmpArrayListS.add(v.getCopy());
      }
      drawPane.redoListShapes.add(new ArrayList<>(tmpArrayListS));
      while (drawPane.undoListShapes.size() > drawPane.maxHistory) {
        drawPane.redoListShapes.remove(0);
      }

      this.drawPane.lines = this.drawPane.undoListLines.get(this.drawPane.undoListLines.size() - 1);
      this.drawPane.vertices = this.drawPane.undoListVertices.get(this.drawPane.undoListVertices.size() - 1);
      this.drawPane.fObjects = this.drawPane.undoListfObjects.get(this.drawPane.undoListfObjects.size() - 1);
      this.drawPane.shapes = this.drawPane.undoListShapes.get(this.drawPane.undoListShapes.size() - 1);
      this.drawPane.undoListLines.remove(this.drawPane.undoListLines.size() - 1);
      this.drawPane.undoListVertices.remove(this.drawPane.undoListVertices.size() - 1);
      this.drawPane.undoListfObjects.remove(this.drawPane.undoListfObjects.size() - 1);
      this.drawPane.undoListShapes.remove(this.drawPane.undoListShapes.size() - 1);
      Frame.this.repaint();
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (this.beframe != null) {
        beframe.update();
      }
    } catch (Exception ex) {
      Frame.logger.info("Nothing to undo");
      Frame.statusLabel.setText("Nothing to undo");
    }
    Frame.this.requestFocus();
  }

  /**
   * redos the last step
   */
  private void redo() {
    try {
      ArrayList<FeynLine> tmpArrayList = new ArrayList<>();
      for (FeynLine line : drawPane.lines) {
        tmpArrayList.add(line.getCopy());
      }
      drawPane.undoListLines.add(new ArrayList<>(tmpArrayList));
      while (drawPane.undoListLines.size() > drawPane.maxHistory) {
        drawPane.undoListLines.remove(0);
      }
      ArrayList<Vertex> tmpArrayListV = new ArrayList<>();
      for (Vertex v : drawPane.vertices) {
        tmpArrayListV.add(new Vertex(v));
      }
      drawPane.undoListVertices.add(new ArrayList<>(tmpArrayListV));
      while (drawPane.undoListVertices.size() > drawPane.maxHistory) {
        drawPane.undoListVertices.remove(0);
      }
      ArrayList<FloatingObject> tmpArrayListF = new ArrayList<>();
      for (FloatingObject v : drawPane.fObjects) {
        tmpArrayListF.add(new FloatingObject(v));
      }
      drawPane.undoListfObjects.add(new ArrayList<>(tmpArrayListF));
      while (drawPane.undoListfObjects.size() > drawPane.maxHistory) {
        drawPane.undoListfObjects.remove(0);
      }
      ArrayList<Shaped> tmpArrayListS = new ArrayList<>();
      for (Shaped v : drawPane.shapes) {
        tmpArrayListS.add(v.getCopy());
      }
      drawPane.undoListShapes.add(new ArrayList<>(tmpArrayListS));
      while (drawPane.undoListShapes.size() > drawPane.maxHistory) {
        drawPane.undoListShapes.remove(0);
      }

      this.drawPane.lines = this.drawPane.redoListLines.get(this.drawPane.redoListLines.size() - 1);
      this.drawPane.vertices = this.drawPane.redoListVertices.get(this.drawPane.redoListVertices.size() - 1);
      this.drawPane.fObjects = this.drawPane.redoListfObjects.get(this.drawPane.redoListfObjects.size() - 1);
      this.drawPane.shapes = this.drawPane.redoListShapes.get(this.drawPane.redoListShapes.size() - 1);
      this.drawPane.redoListLines.remove(this.drawPane.redoListLines.size() - 1);
      this.drawPane.redoListVertices.remove(this.drawPane.redoListVertices.size() - 1);
      this.drawPane.redoListfObjects.remove(this.drawPane.redoListfObjects.size() - 1);
      this.drawPane.redoListShapes.remove(this.drawPane.redoListShapes.size() - 1);
      Frame.this.repaint();
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (this.beframe != null) {
        beframe.update();
      }
    } catch (Exception ex) {
      Frame.logger.info("Nothing to redo");
      Frame.statusLabel.setText("Nothing to undo");
    }
    Frame.this.requestFocus();
  }

  /**
   * deletes all objects
   */
  private void openNew() {
    clearCanvas(CLEARMODE_CLEAR_ALL);
  }

  /**
   * saves the diagram to the currently opened file
   */
  private void saveDiagram() {
    if (this.currentFile == null) {
      this.saveAsDiagram();
    } else {
      if (drawPane.multiEdit != null)
        drawPane.multiEdit.fin();
      if (drawPane.selectType == 4) {
        if (drawPane.lines.size() > 0)
          drawPane.selectType = 0;
        else if (drawPane.fObjects.size() > 0)
          drawPane.selectType = 2;
        else if (drawPane.shapes.size() > 0)
          drawPane.selectType = 3;
        else
          drawPane.selectType = 0;
      }
      File file = new File(this.currentFile);
      clearCanvas(CLEARMODE_CLEAR_META); //removes the open qgraf diagram, if it exists
      ExportManager.setNextGuiExportDestination(file, true);
      this.currentFile = file.getPath();

      if (file.getName().lastIndexOf(".")>0) { fileNameProp = file.getName().substring(0,file.getName().lastIndexOf(".")); }
      else { fileNameProp = file.getName(); }

      this.setTitle(file.getName() + " - " + "FeynGame v" + this.version);
      try (FileOutputStream fos = new FileOutputStream(file);
          ObjectOutputStream oos = new ObjectOutputStream(fos)) {
        String version = "";
        try {
          URL resource = getClass().getResource("/date.txt");
          byte[] readData = new byte[100];
          int read = resource.openStream().read(readData);
          if (read > 0)
            version = new String(readData);
        } catch (Exception ignored) {
        }
        version = version.replaceAll(System.getProperty("line.separator"), "");
        version = version.replaceAll("\n", "");
        oos.writeObject(version);
        oos.writeObject(drawPane.lines);
        oos.writeObject(drawPane.vertices);
        oos.writeObject(drawPane.fObjects);
        oos.writeObject(game.Filling.pathToImageNumber);
        oos.writeObject(drawPane.redoListLines);
        oos.writeObject(drawPane.redoListVertices);
        oos.writeObject(drawPane.redoListfObjects);
        oos.writeObject(drawPane.undoListLines);
        oos.writeObject(drawPane.undoListVertices);
        oos.writeObject(drawPane.undoListfObjects);
        //Save every shape that is not the layout rect. This is also an ArrayList<Shaped>, so it should be compatible
        oos.writeObject(drawPane.shapes);
        //        oos.writeObject(drawPane.shapes);
        oos.writeObject(drawPane.undoListShapes);
        oos.writeObject(drawPane.redoListShapes);
        this.writeNumberToImage(oos);
        oos.writeObject(game.Filling.nameToJVG);
        oos.writeObject(drawPane.scale);
        oos.writeObject(drawPane.zeroPoint);
        oos.writeObject(drawPane.mBB);
        oos.writeObject(drawPane.xMBB);
        oos.writeObject(drawPane.yMBB);
        oos.writeObject(drawPane.wMBB);
        oos.writeObject(drawPane.hMBB);
      } catch (Exception ex) {
        Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
      }
      Frame.this.requestFocus();
      Frame.statusLabel.setText("Diagram saved as: " + file.getPath());
    }
    recents.addEntry(this.currentFile);
  }

  /**
   * load default config/model file
   */
  public void loadDefaultConfig() {
    String defaultPath = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      defaultPath = System.getProperty("user.home") + "/Library/Preferences/FeynGame/default.model";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      defaultPath = System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\default.model";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      defaultPath = System.getProperty("user.home") + "/.config/FeynGame/default.model";
    }
    if (new File(defaultPath).isFile()) {
        Frame.logger.info("Loading default model: " + defaultPath);
        if (defaultPath != null && !defaultPath.trim().isEmpty())
          Frame.statusLabel.setText("Loaded default model: " + defaultPath);
        if (gameMode == 0)
          this.tileTabs.saveCurrent();
        LineConfig.initPresets(defaultPath);
        game.Vertex.initPresets(defaultPath);
        game.FloatingImage.initPresets(defaultPath);
        if (gameMode == 0)
          this.tileTabs.addModel(new Presets(), null);
        if (tileTabs != null)
          tileTabs.update();
        else
          this.reloadTiles();
    } else {
      Frame.logger.info("Loading fallback default model");
      this.loadConfig(null);
    }
  }

  /**
   * set default config/model file
   */
  public void setDefaultConfig() {
    String defaultPath = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      defaultPath = System.getProperty("user.home") + "/Library/Preferences/FeynGame/default.model";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      defaultPath = System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\default.model";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      defaultPath = System.getProperty("user.home") + "/.config/FeynGame/default.model";
    }
    if (new File(defaultPath).isFile()) {
      int response = JOptionPane.showConfirmDialog(Frame.this, "Default model already set. Do you want to overwrite it?", "Set Default Model", JOptionPane.YES_NO_OPTION);
      if (response != JOptionPane.YES_OPTION) return;
    }
    String tempPath = this.configPath;
    this.configPath = defaultPath;
    saveConfig();
    this.configPath = tempPath;
    Frame.logger.info("Set default model: " + defaultPath);
    Frame.statusLabel.setText("Set default model: " + defaultPath);
  }

  /**
   * opens FileChooser to choose file to load as config/model file
   */
  public void loadNewConfig() {
    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What file do you want to load?");

    if (this.configSavePath != null) {
      fc.setCurrentDirectory(new File(this.configSavePath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("Model files (*.model)", "model");
    fc.addChoosableFileFilter(filter);

    int returnVal;
    returnVal = fc.showOpenDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = fc.getSelectedFile();
      this.configSavePath = file.getParentFile().getAbsolutePath();
      this.loadConfig(file.getAbsolutePath());

      this.models.get(tileTabs.getCurrentKey()).allowUnsaved = true;
      this.models.get(tileTabs.getCurrentKey()).unsaved = false;
    }
    Frame.this.requestFocus();
  }

  /**
   * save the current config/model
   */
  public void saveConfig() {
    if (this.configPath == null || this.configPath.equals("")) {
      this.saveAsConfig();
      return;
    }

    try {
      File modelFile = new File(configPath);
      if (!modelFile.exists())
        modelFile.createNewFile();

      FileWriter fw = new FileWriter(modelFile);
      BufferedWriter bw = new BufferedWriter(fw);

      if (LineConfig.presets.size() > 0)
        bw.write(System.getProperty("line.separator") + "# Line definitions: " + System.getProperty("line.separator") + System.getProperty("line.separator"));

      for (LineConfig lc : LineConfig.presets) {
        if (!lc.lineType.equals(LineType.GRAB)) {
          bw.write(lc.toFile(false) + System.getProperty("line.separator"));
        }
      }

      if (LineConfig.rules.size() > 0)
        bw.write(System.getProperty("line.separator") + "# Vertex rules: " + System.getProperty("line.separator") + System.getProperty("line.separator"));

      for (Map.Entry<HashMap<String, Integer>, String> me : LineConfig.rules) {
        bw.write("{");
        HashMap<String, Integer> count = me.getKey();
        boolean first = true;
        for (String i : count.keySet()) {
          if (first) {
            first = false;
          } else {
            bw.write(", ");
          }
          for (int j = 0; j < count.get(i); j++) {
            bw.write(i);
            if (j != count.get(i) - 1)
              bw.write(", ");
          }
        }
        String ident = me.getValue();
        if (ident != null && !ident.equals("")) {
          bw.write(", type = " + ident);
        }
        String vertexTemplate = LineConfig.feynmanRuleTemplates.getVertexTemplate(count);
        if (vertexTemplate != null && !vertexTemplate.chars().allMatch(Character::isWhitespace)) {
          bw.write(", vertex = " + vertexTemplate);
        }
        String vertexFactor = LineConfig.feynmanRuleTemplates.getVertexFactor(count);
        if (vertexFactor != null && !vertexFactor.chars().allMatch(Character::isWhitespace)) {
          bw.write(", factor = " + vertexFactor);
        }
        bw.write("}" + System.getProperty("line.separator"));
      }

      if (Vertex.vertexPresets.size() > 0)
        bw.write(System.getProperty("line.separator") + "# Vertex definitions: " + System.getProperty("line.separator") + System.getProperty("line.separator"));

      for (Map.Entry<Vertex, String> me : Vertex.vertexPresets) {
        bw.write("(");
        bw.write(me.getKey().toFileWOBracket());
        String ident = me.getValue();
        if (ident != null && !ident.equals("")) {
          bw.write(", vname = " + ident);
        }
        bw.write(")" + System.getProperty("line.separator"));
      }

      if (FloatingImage.fIPresets.size() > 0)
        bw.write(System.getProperty("line.separator") + "# Image definitions: " + System.getProperty("line.separator") + System.getProperty("line.separator"));

      for (FloatingImage fI : FloatingImage.fIPresets) {
        bw.write(fI.toFile() + System.getProperty("line.seperator"));
      }

      bw.close();

      this.models.get(tileTabs.getCurrentKey()).unsaved = false;
      Frame.statusLabel.setText("Saved model file: " + modelFile.getPath());
    } catch (IOException ex) {
      Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }

  /**
   * save the current config/model to file specified by filechooser
   */
  public void saveAsConfig() {

    final JFileChooser fc = new JFileChooser();

    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.SAVE_DIALOG);
    fc.setDialogTitle("Where do you want to save the model file?");

    if (this.configSavePath != null) {
      fc.setCurrentDirectory(new File(this.configSavePath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }


    if (this.configPath != null && !this.configPath.equals("")) {
      fc.setSelectedFile(new File(this.configPath));
    } else {
      fc.setSelectedFile(new File(fc.getCurrentDirectory(), "/untitled.model"));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter = new FileNameExtensionFilter("Model files (*.model)", "model");
    fc.addChoosableFileFilter(filter);

    int returnVal;
    returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      String withExtension = fc.getSelectedFile().getAbsolutePath();
      if (!withExtension.toLowerCase().endsWith(".model"))
        withExtension += ".model";
      File newF = new File(withExtension);
      if (newF.exists()) {
        Object[] options = {"Overwrite File", "Cancel"};
        int n = JOptionPane.showOptionDialog(
            this,
            "Do you want to overwrite the existing file",
            "Overwrite file?",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[0]);
        if (n == 1) {
          return;
        }
      }


      String oldModel = tileTabs.getCurrentKey();
      boolean allowUnsaved = this.models.get(oldModel).allowUnsaved;
      boolean unsaved = this.models.get(oldModel).unsaved;
      this.configSavePath = newF.getParentFile().getAbsolutePath();
      this.configPath = newF.getAbsolutePath();
      this.saveConfig();
      this.configSavePath = newF.getParentFile().getAbsolutePath();
      this.configPath = newF.getAbsolutePath();
      this.loadConfig();
      String newModel = tileTabs.getCurrentKey();
      this.models.get(newModel).allowUnsaved = allowUnsaved;
      this.models.get(newModel).unsaved = unsaved;
      tileTabs.remove(oldModel);
      tileTabs.update();
    }
    Frame.this.requestFocus();
  }

  /**
   * opens cheat sheet for keyboard shortcuts
   */
  private void openF1() {
    if (t != null) t.setVisible(false);

    Point conv_p = new Point(0, 0);
    SwingUtilities.convertPointToScreen(conv_p, drawPane);
    t = new Toast("Open keyboard shortcuts", this.drawPane);
    //t.showtoast();
    Frame.this.requestFocusInWindow();
    JFrame help = new JFrame();
    help.setSize(new Dimension(485, 600));
    help.setIconImage(GetResources.getAppIcon());
    try {
      final Taskbar taskbar = Taskbar.getTaskbar();
      taskbar.setIconImage(GetResources.getAppIcon());
    } catch (Exception ignored) {}
    JTextPane html = new JTextPane();
    html.setEditable(false);

    StyleSheet styleSheet = new StyleSheet();
    styleSheet.addRule("kbd {border-radius: 3px;padding: 5px 5px 5px;border: 1px outset #D3D3D3;box-shadow: 0px 1px #888888;font-family: \"Lucida Console\", Monaco, monospace;}");
    styleSheet.addRule("* {font-family: sans-serif;}");
    styleSheet.addRule("table {border-collapse: collapse;width : 100%;}");
    styleSheet.addRule("td {border: 1px solid #D3D3D3;padding: 15px;}");
    styleSheet.addRule("th {text-align: left;border: 1px solid #D3D3D3;padding: 15px;font-weight: normal;background-color : #F8F8F8;color: #2e2e2e;border-bottom: 2px solid #D3D3D3;}");
    styleSheet.addRule("h2 {display: block;font-size: 1.5em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h3 {display: block;font-size: 1.17em;margin-top: 1em;margin-bottom: 1em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h4 {display: block;font-size: 1em;margin-top: 1.33em;margin-bottom: 1.33em;margin-left: 0;margin-right: 0;font-weight: bold;}");

    HTMLEditorKit kit = new HTMLEditorKit();
    kit.setStyleSheet(styleSheet);
    HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();
    html.setEditorKit(kit);
    html.setDocument(doc);
    JScrollPane text;

    if (!macOs) {
      html.setText(GetResources.getKeyAssignmentSheet());
    } else {
      html.setText(GetResources.getKeyAssignmentSheet().replaceAll("Ctrl", "&#8984"));
    }
    text = new JScrollPane(html);

    help.getContentPane().add(text, BorderLayout.CENTER);
    JButton ok = new JButton("ok");
    ok.addActionListener(e1 -> help.dispose());
    help.getContentPane().add(ok, BorderLayout.SOUTH);
    help.setTitle("Key Assignment");
    ok.addKeyListener(new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {

      }

      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) help.dispose();
      }

      @Override
      public void keyReleased(KeyEvent e) {

      }
    });
    help.setResizable(true);
    help.setVisible(true);
  }

  /**
   * relaods the current challenge from the level
   */
  private void reloadChallenge() {
    drawPane.inFinLines.clear();
    drawPane.lines.clear();
    drawPane.vertices.clear();
    drawPane.fObjects.clear();
    int height = Math.abs(this.drawPane.getSize().height);
    int width = Math.abs(this.drawPane.getSize().width);
    int i = 1;
    for (String ident : this.challenge.getKey()) {
      boolean found = false;
      for (LineConfig config : LineConfig.presets) {

        if (!(config.lineType == LineType.GRAB)) {
          if (ident.equals(config.identifier.getKey())) { // presets werden nicht geladen
            FeynLine fl = new FeynLine(new Point2D(-10, height / (this.challenge.getKey().size() + 1) * i), new Point2D(100, height / (this.challenge.getKey().size() + 1) * i), config, 0);
            fl.fixStart = true;
            fl.direction = +1; // positive
            drawPane.lines.add(fl);
            found = true;
            break;
          } else if (ident.equals(config.identifier.getValue())) {
            FeynLine fl = new FeynLine(new Point2D(100, height / (this.challenge.getKey().size() + 1) * i), new Point2D(-10, height / (this.challenge.getKey().size() + 1) * i), config, 0);
            fl.fixEnd = true;
            fl.direction = -1; // negative
            drawPane.lines.add(fl);
            found = true;
            break;
          }
        }
      }
      if (!found) {
        JOptionPane.showMessageDialog(this, "<html>The identifier <b>" + ident + "</b> from the level file could not be found in the model file", "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
      i++;
    }
    i = 1;
    for (String ident : this.challenge.getValue()) {
      boolean ffound = false;
      for (LineConfig config : LineConfig.presets) {
        if (!(config.lineType == LineType.GRAB)) {
          if (ident.equals(config.identifier.getKey())) {
            FeynLine fl = new FeynLine(new Point2D(width - 100, height / (this.challenge.getValue().size() + 1) * i), new Point2D(width + 10, height / (this.challenge.getValue().size() + 1) * i), config, 0);
            fl.fixEnd = true;
            fl.direction = +1; // positive
            drawPane.lines.add(fl);
            ffound = true;
            break;
          } else if (ident.equals(config.identifier.getValue())) {
            FeynLine fl = new FeynLine(new Point2D(width + 10, height / (this.challenge.getValue().size() + 1) * i), new Point2D(width - 100, height / (this.challenge.getValue().size() + 1) * i), config, 0);
            fl.fixStart = true;
            fl.direction = -1; // negative
            drawPane.lines.add(fl);
            ffound = true;
            break;
          }
        }
      }
      if (!ffound) {
        JOptionPane.showMessageDialog(this, "<html>The identifier <b>" + ident + "</b> from the level file could not be found in the model file", "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
      i++;
    }
    // this.drawPane.setPreferredSize(this.drawPaneSize);
    Frame.this.repaint();
  }

  protected void updateAlwaysOnTop(boolean value) {
    dialogFramesAlwaysOnTop = value;
    if(mbbFrame != null) mbbFrame.setAlwaysOnTop(value);
    if(sourceFrame != null) sourceFrame.setAlwaysOnTop(value);
    if(settingsFrame != null) settingsFrame.setAlwaysOnTop(value);
  }

  /**
   * loads a new random challenge from the current InOutLevel
   */
  private void loadChallenge() {

    int totalChallenges = this.inOutLevel.challengesEasy.size() + this.inOutLevel.challengesMedium.size() + this.inOutLevel.challengesHard.size();
    int random = (int) Math.floor(Math.random() * totalChallenges);
    if (random < this.inOutLevel.challengesEasy.size()) {
      this.challengePoints = 5;
      this.challenge = this.inOutLevel.challengesEasy.get(random);
    } else if (random < this.inOutLevel.challengesEasy.size() + this.inOutLevel.challengesMedium.size()) {
      this.challengePoints = 10;
      this.challenge = this.inOutLevel.challengesMedium.get(random - this.inOutLevel.challengesEasy.size());
    } else {
      this.challengePoints = 25;
      this.challenge = this.inOutLevel.challengesHard.get(random - this.inOutLevel.challengesEasy.size() - this.inOutLevel.challengesMedium.size());
    }
    this.currPointsLabel.setText("<html><h2, style=\"color:Green\"> " + this.challengePoints);
    this.reloadChallenge();
  }

  /**
   * checks if there are any errors and displays the information according to the gamemode; loads a new challenge if no errors in InFin game mode and reloads current if otherwise
   */
  private void checkLines() {
    try {
      drawPane.pointsOfError = new ArrayList<>();
      ArrayList<String> errors = Frame.this.drawPane.checkLines(gameMode);
      String msg = String.join("\n- ", errors);
      if (errors.size() == 1) {
        String shapeDisclaimer = "";
        if (this.gameMode == 0) {
          try {
            Class.forName("org.jgrapht.Graph");
            Class.forName("org.jheaps.Heap");
            FeynmanDiagram fd = new FeynmanDiagram(drawPane.lines);
            JButton buttonShowAmplitude = new JButton("Show Amplitude");
            buttonShowAmplitude.addActionListener(e -> {
              try {
                String amplitude = fd.parseAmplitude();
                TeXFormula form = new TeXFormula(amplitude);
                TeXIcon ti = form.new TeXIconBuilder().setStyle(TeXConstants.STYLE_DISPLAY).setSize(25).build();
                ti.setInsets(new Insets(15, 10, 15, 10));
                JOptionPane.showMessageDialog(Frame.this, ti, "Invariant Amplitude", JOptionPane.PLAIN_MESSAGE);
              } catch (ParseException pe) {
                JOptionPane.showMessageDialog(Frame.this, pe.getMessage(), "ParseException", JOptionPane.PLAIN_MESSAGE);
              }
            });
            JButton buttonCopyAmplitude = new JButton("Copy Amplitude to Clipboard");
            buttonCopyAmplitude.addActionListener(e -> {
              Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
              try {
                String amplitude = fd.parseAmplitude();
                clipboard.setContents(new StringSelection(amplitude), null);
              } catch (ParseException pe) {
                clipboard.setContents(new StringSelection(pe.getMessage()), null);
              }
            });
            JButton buttonDistributeMomenta = new JButton("Distribute Momenta");
            buttonDistributeMomenta.addActionListener(e -> {
              fd.distributeMomenta();
            });
            JButton buttonResetMomenta = new JButton("Reset Momenta");
            buttonResetMomenta.addActionListener(e -> {
              for (FeynLine line : drawPane.lines) {
                line.hideMom();
                line.momArrow.showDesc = false;
                line.momArrow.description = "";
              }
            });
            final Object[] inputs = new Object[] {
                new JLabel("Respect! It is all correct." + shapeDisclaimer, SwingConstants.CENTER),
                buttonShowAmplitude,
                buttonCopyAmplitude,
                buttonDistributeMomenta,
                buttonResetMomenta
            };
            JOptionPane.showConfirmDialog(Frame.this, inputs, "Respect", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE);
          } catch(ClassNotFoundException e) {
            Frame.logger.warning("could not find JGraphT/JHeaps library");
            JOptionPane.showMessageDialog(Frame.this, "Respect! It is all correct." + shapeDisclaimer, "Respect", JOptionPane.PLAIN_MESSAGE);
          }
        } else if (this.gameMode == 1) {
          JOptionPane.showMessageDialog(Frame.this, "Respect! It is all correct." + shapeDisclaimer, "Respect", JOptionPane.PLAIN_MESSAGE);
          this.points += this.challengePoints;
          this.loadChallenge();
          this.pointsLabel.setText("<html><h2, style=\"color:Blue\"> " + this.points);
        }
      } else {
        String shapeDisclaimer = "";
        String title = String.format("Found %d %s", errors.size() - 1, errors.size() == 2 ? "Error" : "Errors");
        if (!errors.contains("This diagram is not connected!")) {
          if (this.gameMode == 0) {
            try {
              Class.forName("org.jgrapht.Graph");
              Class.forName("org.jheaps.Heap");
              FeynmanDiagram fd = new FeynmanDiagram(drawPane.lines);
              JButton buttonDistributeMomenta = new JButton("Distribute Momenta");
              buttonDistributeMomenta.addActionListener(e -> {
                fd.distributeMomenta();
              });
              JButton buttonResetMomenta = new JButton("Reset Momenta");
              buttonResetMomenta.addActionListener(e -> {
                for (FeynLine line : drawPane.lines) {
                  line.hideMom();
                  line.momArrow.showDesc = false;
                  line.momArrow.description = "";
                }
              });
              final Object[] inputs = new Object[] {
                  new JTextArea(msg + shapeDisclaimer),
                  buttonDistributeMomenta,
                  buttonResetMomenta
              };
              JOptionPane.showConfirmDialog(Frame.this, inputs, title, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE);
            } catch(ClassNotFoundException e) {
              Frame.logger.warning("could not find JGraphT/JHeaps library");
              JOptionPane.showMessageDialog(Frame.this, msg + shapeDisclaimer, title, JOptionPane.WARNING_MESSAGE);
            }
          }
        } else {
          JOptionPane.showMessageDialog(Frame.this, msg + shapeDisclaimer, title, JOptionPane.WARNING_MESSAGE);
        }
      }
      drawPane.pointsOfError = new ArrayList<>();
    } catch (Exception ex) {
      Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }

  //replaces the old solution line.split(":")[1] beause on windows, paths can contain colons
  private static String readPrefsPath(String line) {
    return line.substring(line.indexOf(':') + 1);
  }

  private static List<String> readPrefsPathList(String line) {
    String list = line.substring(line.indexOf(':') + 1);
    return Arrays.asList(list.split(";"));
  }
  
  /**
   * loads the user preferences for the gamemode from the file stored under:
   * <ul><li>Windows: C:\Users\<i>User_Name</i>\AppData\Roaming\FeynGame\</li>
   * <li>GNU: /home/.config/FeynGame/</li>
   * <li>MacOs: ~/Library/Preferences/FeynGame/</li></ul>
   **/
  private void getPreferences() {
    String prefFileSt = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      if (this.gameMode == 0) {
        prefFileSt = (System.getProperty("user.home") + "/Library/Preferences/FeynGame/DrawMode.ini");
      } else if (this.gameMode == 1) {
        prefFileSt = (System.getProperty("user.home") + "/Library/Preferences/FeynGame/InFinMode.ini");
      }
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      if (this.gameMode == 0) {
        prefFileSt = (System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\DrawMode.ini");
      } else if (this.gameMode == 1) {
        prefFileSt = (System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\InFinMode.ini");
      }
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      if (this.gameMode == 0) {
        prefFileSt = (System.getProperty("user.home") + "/.config/FeynGame/DrawMode.ini");
      } else if (this.gameMode == 1) {
        prefFileSt = (System.getProperty("user.home") + "/.config/FeynGame/InFinMode.ini");
      }
    }
    File prefFile = new File(prefFileSt);
    if (!prefFile.canRead()) {
      GraphLayoutAlgorithm.updatePresetMenus();
      return;
    }

    recentEntries = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new FileReader(prefFile))) {
      String line;
      while ((line = br.readLine()) != null) {
        if (line.startsWith("Grid") && !line.startsWith("GridSize")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          drawPane.grid = value.toLowerCase().equals("true");
        } else if (line.startsWith("GridSize")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Pane.gridSize = (int) Float.parseFloat(value);
        } else if (line.startsWith("ShowCurrentlySelectedObject")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          drawPane.showCurrentSelectedLine = value.toLowerCase().equals("true");
        } else if (line.startsWith("HelperLines")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          drawPane.helperLines = value.toLowerCase().equals("true");
        } else if (line.startsWith("Diagrams_saved_at")) {
          this.diagramSavePath = readPrefsPath(line);
        } else if (line.startsWith("Models_saved_at")) {
          this.configSavePath = readPrefsPath(line);
        } else if (line.startsWith("Images_saved_at")) {
          this.imgSavePath = readPrefsPath(line);
        } else if (line.startsWith("Images_loaded_from")) {
          this.imgLoadPath = readPrefsPath(line);
        } else if (line.startsWith("Show_description:")) {
          String value = line.split(":")[1];
          FeynLine.showDescDef = value.toLowerCase().startsWith("true");
          FeynLine.showDescDefFERM = value.toLowerCase().startsWith("true");
          FeynLine.showDescDefGLUON = value.toLowerCase().startsWith("true");
          FeynLine.showDescDefPHOT = value.toLowerCase().startsWith("true");
        } else if (line.startsWith("Distance_description:")) {
          String value = line.split(":")[1];
          FeynLine.descDisDef = (int) Float.parseFloat(value);
          FeynLine.descDisDefFERM = (int) Float.parseFloat(value);
          FeynLine.descDisDefGLUON = (int) Float.parseFloat(value);
          FeynLine.descDisDefPHOT = (int) Float.parseFloat(value);
        } else if (line.startsWith("Place_description:")) {
          String value = line.split(":")[1];
          FeynLine.partPlaceDescDef = Float.parseFloat(value);
          FeynLine.partPlaceDescDefFERM = Float.parseFloat(value);
          FeynLine.partPlaceDescDefGLUON = Float.parseFloat(value);
          FeynLine.partPlaceDescDefPHOT = Float.parseFloat(value);
        } else if (line.startsWith("Rotation_description:")) {
          String value = line.split(":")[1];
          FeynLine.rotDescDef = Float.parseFloat(value);
          FeynLine.rotDescDefFERM = Float.parseFloat(value);
          FeynLine.rotDescDefPHOT = Float.parseFloat(value);
          FeynLine.rotDescDefGLUON = Float.parseFloat(value);
        } else if (line.startsWith("Scale_description:")) {
          String value = line.split(":")[1];
          FeynLine.descScaleDef = Float.parseFloat(value);
          FeynLine.descScaleDefFERM = Float.parseFloat(value);
          FeynLine.descScaleDefPHOT = Float.parseFloat(value);
          FeynLine.descScaleDefGLUON = Float.parseFloat(value);
        } else if (line.startsWith("Show_description_vertex")) {
          String value = line.split(":")[1];
          Vertex.showDescDef = value.toLowerCase().startsWith("true");
        } else if (line.startsWith("Description_vertex_rel_X")) {
          String value = line.split(":")[1];
          Vertex.descPosXDef = (int) Float.parseFloat(value);
        } else if (line.startsWith("Description_vertex_rel_Y")) {
          String value = line.split(":")[1];
          Vertex.descPosYDef = (int) Float.parseFloat(value);
        } else if (line.startsWith("Rotation_description_vertex:")) {
          String value = line.split(":")[1];
          Vertex.rotDescDef = Float.parseFloat(value);
        } else if (line.startsWith("Scale_description_vertex:")) {
          String value = line.split(":")[1];
          Vertex.descScaleDef = Float.parseFloat(value);
        } else if (line.startsWith("Show_description_image")) {
          String value = line.split(":")[1];
          FloatingImage.showDescDef = value.toLowerCase().startsWith("true");
        } else if (line.startsWith("Description_image_rel_X")) {
          String value = line.split(":")[1];
          FloatingImage.descPosXDef = (int) Float.parseFloat(value);
        } else if (line.startsWith("Description_image_rel_Y")) {
          String value = line.split(":")[1];
          FloatingImage.descPosYDef = (int) Float.parseFloat(value);
        } else if (line.startsWith("Rotation_description_image:")) {
          String value = line.split(":")[1];
          FloatingImage.rotDescDef = Float.parseFloat(value);
        } else if (line.startsWith("Scale_description_image:")) {
          String value = line.split(":")[1];
          FloatingImage.descScaleDef = Float.parseFloat(value);
        } else if (line.startsWith("Clipping_angles:")) {
          String value = line.split(":")[1];
          if (value.equals("PI/16")) {
            drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 16));
          } else if (value.equals("PI/4")) {
            drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 4));
          } else if (value.equals("PI/2")){
            drawPane.angles = new ArrayList<>(Collections.singletonList(Math.PI / 2));
          } else {
            drawPane.angles = new ArrayList<>();
          }
        } else if (line.startsWith("Use_BBCalc:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Print.bbcalc = value.toLowerCase().equals("true");
        } else if (line.startsWith("Use_BWFilter:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Print.bw = value.toLowerCase().equals("true");
        } else if (line.startsWith("Use_auto_bounds:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Print.bounds = 1;
          if (value.toLowerCase().equals("true")) {
            Print.bounds = 0;
          }
        } else if (line.startsWith("Print_bounds:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Print.bounds = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_length_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.lengthFERM = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_length_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.lengthGLUON = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_length_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.lengthPHOT = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_stroke_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.strokeFERM = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_stroke_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.strokeGLUON = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_stroke_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.strokePHOT = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_tipSize_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.tipSizeFERM = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_tipSize_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.tipSizeGLUON = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_tipSize_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.tipSizePHOT = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_position_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.positionFERM = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_position_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.positionGLUON = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_position_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.positionPHOT = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_distance_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.distanceFERM = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_distance_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.distanceGLUON = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_distance_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.distancePHOT = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_invert_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.invertFERM = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_invert_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.invertGLUON = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_invert_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.invertPHOT = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_lineColor_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.lineColorFERM = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_lineColor_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.lineColorGLUON = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_lineColor_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.lineColorPHOT = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_drawMomentum_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.drawMomentumFERM = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_drawMomentum_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.drawMomentumGLUON = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_drawMomentum_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.drawMomentumPHOT = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_color_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Color c = null;
          try {
            c = (Color) Color.class.getField(value.toUpperCase()).get(null);
          } catch (Exception ignore) {
          }

          if (c == null && value.length() == 8) {
            try {
              int b = Integer.parseInt(value.substring(6, 8), 16);
              int g = Integer.parseInt(value.substring(4, 6), 16);
              int r = Integer.parseInt(value.substring(2, 4), 16);
              int a = Integer.parseInt(value.substring(0, 2), 16);
              c = new Color(r, g, b, a);
            } catch (Exception ignore) {
            }
          }

          if (c == null && value.length() == 6) {
            try {
              int r = Integer.parseInt(value.substring(0, 2), 16);
              int g = Integer.parseInt(value.substring(2, 4), 16);
              int b = Integer.parseInt(value.substring(4, 6), 16);
              c = new Color(r, g, b);
            } catch (Exception ignore) {
            }
          }
          if (c != null)
            Momentum.colorFERM = c;
        } else if (line.startsWith("Mom_arrow_color_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Color c = null;
          try {
            c = (Color) Color.class.getField(value.toUpperCase()).get(null);
          } catch (Exception ignore) {
          }

          if (c == null && value.length() == 8) {
            try {
              int b = Integer.parseInt(value.substring(6, 8), 16);
              int g = Integer.parseInt(value.substring(4, 6), 16);
              int r = Integer.parseInt(value.substring(2, 4), 16);
              int a = Integer.parseInt(value.substring(0, 2), 16);
              c = new Color(r, g, b, a);
            } catch (Exception ignore) {
            }
          }

          if (c == null && value.length() == 6) {
            try {
              int r = Integer.parseInt(value.substring(0, 2), 16);
              int g = Integer.parseInt(value.substring(2, 4), 16);
              int b = Integer.parseInt(value.substring(4, 6), 16);
              c = new Color(r, g, b);
            } catch (Exception ignore) {
            }
          }
          if (c != null)
            Momentum.colorGLUON = c;
        } else if (line.startsWith("Mom_arrow_color_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Color c = null;
          try {
            c = (Color) Color.class.getField(value.toUpperCase()).get(null);
          } catch (Exception ignore) {
          }

          if (c == null && value.length() == 8) {
            try {
              int b = Integer.parseInt(value.substring(6, 8), 16);
              int g = Integer.parseInt(value.substring(4, 6), 16);
              int r = Integer.parseInt(value.substring(2, 4), 16);
              int a = Integer.parseInt(value.substring(0, 2), 16);
              c = new Color(r, g, b, a);
            } catch (Exception ignore) {
            }
          }

          if (c == null && value.length() == 6) {
            try {
              int r = Integer.parseInt(value.substring(0, 2), 16);
              int g = Integer.parseInt(value.substring(2, 4), 16);
              int b = Integer.parseInt(value.substring(4, 6), 16);
              c = new Color(r, g, b);
            } catch (Exception ignore) {
            }
          }
          if (c != null)
            Momentum.colorPHOT = c;
        } else if (line.startsWith("Mom_arrow_showDesc_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.showDescFERM = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_showDesc_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.showDescGLUON = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_showDesc_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.showDescPHOT = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_descDis_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.descDisFERM = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_descDis_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.descDisGLUON = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_descDis_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.descDisPHOT = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_partPlaceDesc_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.partPlaceDescFERM = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_partPlaceDesc_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.partPlaceDescGLUON = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_partPlaceDesc_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.partPlaceDescPHOT = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_rotDesc_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.rotDescFERM = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_rotDesc_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.rotDescGLUON = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_rotDesc_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.rotDescPHOT = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_descScale_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.descScaleFERM = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_descScale_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.descScaleGLUON = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_descScale_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.descScalePHOT = Double.parseDouble(value);
        } else if (line.startsWith("Mom_arrow_dashLength_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.dashLengthFERM = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_dashLength_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.dashLengthGLUON = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_dashLength_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.dashLengthPHOT = Integer.parseInt(value);
        } else if (line.startsWith("Mom_arrow_dashed_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.dashedFERM = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_dashed_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.dashedGLUON = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_dashed_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.dashedPHOT = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_doubleLine_FERM:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.doubleLineFERM = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_doubleLine_GLUON:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.doubleLineGLUON = value.toLowerCase().equals("true");
        } else if (line.startsWith("Mom_arrow_doubleLine_PHOT:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          Momentum.doubleLinePHOT = value.toLowerCase().equals("true");
        } else if (line.startsWith("Show_description_shape")) {
          String value = line.split(":")[1];
          Shaped.showDescDef = value.toLowerCase().startsWith("true");
        } else if (line.startsWith("Description_shape_rel_X")) {
          String value = line.split(":")[1];
          Shaped.descPosXDef = (int) Float.parseFloat(value);
        } else if (line.startsWith("Description_shape_rel_Y")) {
          String value = line.split(":")[1];
          Shaped.descPosYDef = (int) Float.parseFloat(value);
        } else if (line.startsWith("Rotation_description_shape:")) {
          String value = line.split(":")[1];
          Shaped.rotDescDef = Float.parseFloat(value);
        } else if (line.startsWith("Scale_description_shape:")) {
          String value = line.split(":")[1];
          Shaped.descScaleDef = Float.parseFloat(value);
        } else if (line.startsWith("Show_description_FERM:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.showDescDefFERM = value.toLowerCase().startsWith("true");
        } else if (line.startsWith("Distance_description_FERM:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.descDisDefFERM = (int) Float.parseFloat(value);
        } else if (line.startsWith("Place_description_FERM:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.partPlaceDescDefFERM = Float.parseFloat(value);
        } else if (line.startsWith("Rotation_description_FERM:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.rotDescDefFERM = Float.parseFloat(value);
        } else if (line.startsWith("Scale_description_FERM:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.descScaleDefFERM = Float.parseFloat(value);
        } else if (line.startsWith("Show_description_GLUON:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.showDescDefGLUON = value.toLowerCase().startsWith("true");
        } else if (line.startsWith("Distance_description_GLUON:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.descDisDefGLUON = (int) Float.parseFloat(value);
        } else if (line.startsWith("Place_description_GLUON:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.partPlaceDescDefGLUON = Float.parseFloat(value);
        } else if (line.startsWith("Rotation_description_GLUON:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.rotDescDefGLUON = Float.parseFloat(value);
        } else if (line.startsWith("Scale_description_GLUON:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.descScaleDefGLUON = Float.parseFloat(value);
        } else if (line.startsWith("Show_description_PHOT:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.showDescDefPHOT = value.toLowerCase().startsWith("true");
        } else if (line.startsWith("Distance_description_PHOT:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.descDisDefPHOT = (int) Float.parseFloat(value);
        } else if (line.startsWith("Place_description_PHOT:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.partPlaceDescDefPHOT = Float.parseFloat(value);
        } else if (line.startsWith("Rotation_description_PHOT:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.rotDescDefPHOT = Float.parseFloat(value);
        } else if (line.startsWith("Scale_description_PHOT:")) {
          String value = line.split(":")[1];
          FeynLine.LINE_TYPE_DEP_DEF = true;
          FeynLine.descScaleDefPHOT = Float.parseFloat(value);
        } else if (line.startsWith("Convert_canvas_to_model:")) {
          String value = line.split(":")[1];
          if (value.equals("false"))
            this.convCanToModel = 1;
          else
            this.convCanToModel = 2;
        } else if (line.startsWith("Max_history:")) {
          String value = line.split(":")[1];
          drawPane.maxHistory = Integer.parseInt(value);
        } else if (line.startsWith("Auto_blanks:")) {
          String value = line.split(":")[1];
          FeynLine.autoBlanks = value.toLowerCase().equals("true");
        } else if (line.startsWith("PDF_export_dim_width:")) {
          String value = line.split(":")[1];
          Frame.pdfExportDim.setSize(Double.parseDouble(value), Frame.pdfExportDim.getHeight());
        } else if (line.startsWith("PDF_export_dim_height:")) {
          String value = line.split(":")[1];
          Frame.pdfExportDim.setSize(Frame.pdfExportDim.getWidth(), Double.parseDouble(value));
        } else if (line.startsWith("Img_export_dim_width:")) {
          String value = line.split(":")[1];
          Frame.imageExportDim.setSize(Double.parseDouble(value), Frame.imageExportDim.getHeight());
        } else if (line.startsWith("Img_export_dim_height:")) {
          String value = line.split(":")[1];
          Frame.imageExportDim.setSize(Frame.imageExportDim.getWidth(), Double.parseDouble(value));
        } else if (line.startsWith("Show_ToolTip:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          toolTip = value.toLowerCase().equals("true");
        } else if(line.startsWith("Clear_Canvas_On_Paste:")) {
          this.clearCanvasOnPaste = Boolean.parseBoolean(line.split(":")[1]);
//        } else if(line.startsWith("Layout_options_string:")) { //NO LONGER USED
//          String options = line.split(":")[1];
//          GraphLayoutAlgorithm.getLayoutAlgorithm().loadParameters(options);
        } else if(line.startsWith("Static_Layout_Options:")) {
          String options = line.split(":")[1];
          GraphLayoutAlgorithm.loadStaticParameters(options); //TODO remove eventually
        } else if(line.startsWith("Layout_Presets:")) {
          String[] options = line.split(":");
          GraphLayoutAlgorithm.loadPresets(options);
        } else if(line.startsWith("Selected_Preset_Index")) {
          GraphLayoutAlgorithm.setSelectedPresetIndex(Integer.parseInt(line.split(":")[1]));
        } else if(line.startsWith("Unknown_Lines_Dashed:")) {
          ModelData.setDashed(Boolean.parseBoolean(line.split(":")[1]));
        } else if (line.startsWith("Canvas_width:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          drawPane.setPreferredSize(new Dimension(Integer.parseInt(value),(int) drawPane.getPreferredSize().getHeight()));
          drawPane.setSize(new Dimension(Integer.parseInt(value),(int) drawPane.getHeight()));
          if (drawPaneSize == null) drawPaneSize = new Dimension(Integer.parseInt(value), (int) drawPane.getHeight());
          else drawPaneSize = new Dimension(Integer.parseInt(value), (int) drawPaneSize.getHeight());
        } else if (line.startsWith("Canvas_height:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          drawPane.setPreferredSize(new Dimension((int) drawPane.getPreferredSize().getWidth(), Integer.parseInt(value)));
          drawPane.setSize(new Dimension((int) drawPane.getWidth(), Integer.parseInt(value)));
          if (drawPaneSize == null) drawPaneSize = new Dimension((int) drawPane.getWidth(), Integer.parseInt(value));
          else drawPaneSize = new Dimension((int) drawPaneSize.getWidth(), Integer.parseInt(value));
        } else if (line.startsWith("LaTeX_font_size:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          int fontSize = Integer.parseInt(value);
          ui.Frame.setLaTeXFontSize(fontSize);
        } else if(line.startsWith("Graph_Layout_External_Mode")) {
          int value = Integer.parseInt(line.split(":")[1]);
          GraphLayoutAlgorithm.setSelectedLayoutAlgorthm(value);
        } else if(line.startsWith("Graph_Layout_Grid_Auto_Layout")) {
          boolean value = Boolean.parseBoolean(line.split(":")[1]);
          GraphLayoutAlgorithm.gridAutoLayout(value);
        } else if(line.startsWith("Dialog_Always_on_Top")) {
          boolean value = Boolean.parseBoolean(line.split(":")[1]);
          dialogFramesAlwaysOnTop = value;
        } else if(line.startsWith("Reset_Export_Destination")) {
          boolean value = Boolean.parseBoolean(line.split(":")[1]);
          resetExportDestinationOnLoad = value;
        } else if (line.startsWith("Update_notifications:")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          notifyUpdates = value.replaceAll(" ", "");
        } else if (line.startsWith("Recents:")) {
          String value = line.replace("Recents:","");
          for (String entry : value.split("\\|")) {
            if(!entry.isEmpty()) recentEntries.add(entry);
          }
        }
        ExportManager.readPreferences(line);
        QgrafImportManager.readPreferences(line);
      }
      GraphLayoutAlgorithm.updatePresetMenus();
    } catch (Exception e) {
      Frame.logger.log(Level.SEVERE, e.getMessage(), e);
    }
    recents.update();
  }

  /**
   * writes the user preferences for the gamemode to the file stored under:
   * <ul><li>Windows: C:\\Users\\<i>User_Name</i>\\AppData\\Roaming\\FeynGame\\</li>
   * <li>GNU: /home/.config/FeynGame/</li>
   * <li>MacOs: ~/Library/Preferences/FeynGame/</li></ul>
   **/
  private void setPreferences() {
    String prefFileSt = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      if (this.gameMode == 0) {
        prefFileSt = (System.getProperty("user.home") + "/Library/Preferences/FeynGame/DrawMode.ini");
      } else if (this.gameMode == 1) {
        prefFileSt = (System.getProperty("user.home") + "/Library/Preferences/FeynGame/InFinMode.ini");
      }
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      if (this.gameMode == 0) {
        prefFileSt = (System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\DrawMode.ini");
      } else if (this.gameMode == 1) {
        prefFileSt = (System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\InFinMode.ini");
      }
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      if (this.gameMode == 0) {
        prefFileSt = (System.getProperty("user.home") + "/.config/FeynGame/DrawMode.ini");
      } else if (this.gameMode == 1) {
        prefFileSt = (System.getProperty("user.home") + "/.config/FeynGame/InFinMode.ini");
      }
    }
    File prefFile = new File(prefFileSt);
    try {
      prefFile.getParentFile().mkdirs();
      prefFile.createNewFile();
    } catch (IOException iex) {
      Frame.logger.log(Level.SEVERE, iex.getMessage(), iex);
    }
    if (!prefFile.canWrite()) {
      Frame.logger.severe("Could not save preferences.");
      return;
    }

    Frame.logger.info("Saved preferences to: " + prefFile.getAbsolutePath());

    try (BufferedWriter bw = new BufferedWriter(new FileWriter(prefFile))) {
      if (drawPane.grid) {
        bw.write("Grid:true" + System.getProperty("line.separator"));
      } else {
        bw.write("Grid:false" + System.getProperty("line.separator"));
      }

      bw.write("GridSize:" + String.valueOf(Pane.gridSize) + System.getProperty("line.separator"));

      if (drawPane.helperLines) {
        bw.write("HelperLines:true" + System.getProperty("line.separator"));
      } else {
        bw.write("HelperLines:false" + System.getProperty("line.separator"));
      }

      if (drawPane.showCurrentSelectedLine) {
        bw.write("ShowCurrentlySelectedObject:true" + System.getProperty("line.separator"));
      } else {
        bw.write("ShowCurrentlySelectedObject:false" + System.getProperty("line.separator"));
      }
      if (this.diagramSavePath != null) {
        bw.write("Diagrams_saved_at:" + this.diagramSavePath + System.getProperty("line.separator"));
      }
      if (this.configSavePath != null) {
        bw.write("Models_saved_at:" + this.configSavePath + System.getProperty("line.separator"));
      }
      if (this.imgSavePath != null) {
        bw.write("Images_saved_at:" + this.imgSavePath + System.getProperty("line.separator"));
      }
      if (this.imgLoadPath != null) {
        bw.write("Images_loaded_from:" + this.imgLoadPath + System.getProperty("line.separator"));
      }
      if (!FeynLine.LINE_TYPE_DEP_DEF) {
        bw.write("Show_description:" + FeynLine.showDescDef + System.getProperty("line.separator"));
        bw.write("Distance_description:" + FeynLine.descDisDef + System.getProperty("line.separator"));
        bw.write("Place_description:" + FeynLine.partPlaceDescDef + System.getProperty("line.separator"));
        bw.write("Rotation_description:" + FeynLine.rotDescDef + System.getProperty("line.separator"));
        bw.write("Scale_description:" + FeynLine.descScaleDef + System.getProperty("line.separator"));
      } else {
        bw.write("Show_description_FERM:" + FeynLine.showDescDefFERM + System.getProperty("line.separator"));
        bw.write("Distance_description_FERM:" + FeynLine.descDisDefFERM + System.getProperty("line.separator"));
        bw.write("Place_description_FERM:" + FeynLine.partPlaceDescDefFERM + System.getProperty("line.separator"));
        bw.write("Rotation_description_FERM:" + FeynLine.rotDescDefFERM + System.getProperty("line.separator"));
        bw.write("Scale_description_FERM:" + FeynLine.descScaleDefFERM + System.getProperty("line.separator"));

        bw.write("Show_description_GLUON:" + FeynLine.showDescDefGLUON + System.getProperty("line.separator"));
        bw.write("Distance_description_GLUON:" + FeynLine.descDisDefGLUON + System.getProperty("line.separator"));
        bw.write("Place_description_GLUON:" + FeynLine.partPlaceDescDefGLUON + System.getProperty("line.separator"));
        bw.write("Rotation_description_GLUON:" + FeynLine.rotDescDefGLUON + System.getProperty("line.separator"));
        bw.write("Scale_description_GLUON:" + FeynLine.descScaleDefGLUON + System.getProperty("line.separator"));

        bw.write("Show_description_PHOT:" + FeynLine.showDescDefPHOT + System.getProperty("line.separator"));
        bw.write("Distance_description_PHOT:" + FeynLine.descDisDefPHOT + System.getProperty("line.separator"));
        bw.write("Place_description_PHOT:" + FeynLine.partPlaceDescDefPHOT + System.getProperty("line.separator"));
        bw.write("Rotation_description_PHOT:" + FeynLine.rotDescDefPHOT + System.getProperty("line.separator"));
        bw.write("Scale_description_PHOT:" + FeynLine.descScaleDefPHOT + System.getProperty("line.separator"));
      }
      bw.write("Show_description_vertex:" + Vertex.showDescDef + System.getProperty("line.separator"));
      bw.write("Description_vertex_rel_X:" + Vertex.descPosXDef + System.getProperty("line.separator"));
      bw.write("Description_vertex_rel_Y:" + Vertex.descPosYDef + System.getProperty("line.separator"));
      bw.write("Rotation_description_vertex:" + Vertex.rotDescDef + System.getProperty("line.separator"));
      bw.write("Scale_description_vertex:" + Vertex.descScaleDef + System.getProperty("line.separator"));
      bw.write("Show_description_image:" + FloatingImage.showDescDef + System.getProperty("line.separator"));
      bw.write("Description_image_rel_X:" + FloatingImage.descPosXDef + System.getProperty("line.separator"));
      bw.write("Description_image_rel_Y:" + FloatingImage.descPosYDef + System.getProperty("line.separator"));
      bw.write("Rotation_description_image:" + FloatingImage.rotDescDef + System.getProperty("line.separator"));
      bw.write("Scale_description_image:" + FloatingImage.descScaleDef + System.getProperty("line.separator"));
      if (drawPane.angles.equals(new ArrayList<>(Collections.singletonList(Math.PI / 16)))) {
        bw.write("Clipping_angles:PI/16" + System.getProperty("line.separator"));
      } else if (drawPane.angles.equals(new ArrayList<>(Collections.singletonList(Math.PI / 4)))) {
        bw.write("Clipping_angles:PI/4" + System.getProperty("line.separator"));
      } else if (drawPane.angles.equals(new ArrayList<>(Collections.singletonList(Math.PI / 2)))) {
        bw.write("Clipping_angles:PI/2" + System.getProperty("line.separator"));
      } else {
        bw.write("Clipping_angles:None" + System.getProperty("line.separator"));
      }

      if (Print.bbcalc) {
        bw.write("Use_BBCalc:true" + System.getProperty("line.separator"));
      } else {
        bw.write("Use_BBCalc:false" + System.getProperty("line.separator"));
      }

      if (Print.bw) {
        bw.write("Use_BWFilter:true" + System.getProperty("line.separator"));
      } else {
        bw.write("Use_BWFilter:false" + System.getProperty("line.separator"));
      }

      bw.write("Print_bounds:" + String.valueOf(Print.bounds) + System.getProperty("line.separator"));

      /* momentum arrow preferences */
      bw.write("Mom_arrow_length_FERM:" + String.valueOf(Momentum.lengthFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_length_GLUON:" + String.valueOf(Momentum.lengthGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_length_PHOT:" + String.valueOf(Momentum.lengthPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_stroke_FERM:" + String.valueOf(Momentum.strokeFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_stroke_GLUON:" + String.valueOf(Momentum.strokeGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_stroke_PHOT:" + String.valueOf(Momentum.strokePHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_tipSize_FERM:" + String.valueOf(Momentum.tipSizeFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_tipSize_GLUON:" + String.valueOf(Momentum.tipSizeGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_tipSize_PHOT:" + String.valueOf(Momentum.tipSizePHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_invert_FERM:" + String.valueOf(Momentum.invertFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_invert_GLUON:" + String.valueOf(Momentum.invertGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_invert_PHOT:" + String.valueOf(Momentum.invertPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_position_FERM:" + String.valueOf(Momentum.positionFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_position_GLUON:" + String.valueOf(Momentum.positionGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_position_PHOT:" + String.valueOf(Momentum.positionPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_distance_FERM:" + String.valueOf(Momentum.distanceFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_distance_GLUON:" + String.valueOf(Momentum.distanceGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_distance_PHOT:" + String.valueOf(Momentum.distancePHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_drawMomentum_FERM:" + String.valueOf(Momentum.drawMomentumFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_drawMomentum_GLUON:" + String.valueOf(Momentum.drawMomentumGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_drawMomentum_PHOT:" + String.valueOf(Momentum.drawMomentumPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_lineColor_FERM:" + String.valueOf(Momentum.lineColorFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_lineColor_GLUON:" + String.valueOf(Momentum.lineColorGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_lineColor_PHOT:" + String.valueOf(Momentum.lineColorPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_color_FERM:" + Integer.toHexString(Momentum.colorFERM.getRGB()) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_color_GLUON:" + Integer.toHexString(Momentum.colorGLUON.getRGB()) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_color_PHOT:" + Integer.toHexString(Momentum.colorPHOT.getRGB()) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_showDesc_FERM:" + String.valueOf(Momentum.showDescFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_showDesc_GLUON:" + String.valueOf(Momentum.showDescGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_showDesc_PHOT:" + String.valueOf(Momentum.showDescPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_descDis_FERM:" + String.valueOf(Momentum.descDisFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_descDis_GLUON:" + String.valueOf(Momentum.descDisGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_descDis_PHOT:" + String.valueOf(Momentum.descDisPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_partPlaceDesc_FERM:" + String.valueOf(Momentum.partPlaceDescFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_partPlaceDesc_GLUON:" + String.valueOf(Momentum.partPlaceDescGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_partPlaceDesc_PHOT:" + String.valueOf(Momentum.partPlaceDescPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_rotDesc_FERM:" + String.valueOf(Momentum.rotDescFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_rotDesc_GLUON:" + String.valueOf(Momentum.rotDescGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_rotDesc_PHOT:" + String.valueOf(Momentum.rotDescPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_descScale_FERM:" + String.valueOf(Momentum.descScaleFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_descScale_GLUON:" + String.valueOf(Momentum.descScaleGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_descScale_PHOT:" + String.valueOf(Momentum.descScalePHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_dashed_FERM:" + String.valueOf(Momentum.dashedFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_dashed_GLUON:" + String.valueOf(Momentum.dashedGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_dashed_PHOT:" + String.valueOf(Momentum.dashedPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_dashLength_FERM:" + String.valueOf(Momentum.dashLengthFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_dashLength_GLUON:" + String.valueOf(Momentum.dashLengthGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_dashLength_PHOT:" + String.valueOf(Momentum.dashLengthPHOT) + System.getProperty("line.separator"));

      bw.write("Mom_arrow_doubleLine_FERM:" + String.valueOf(Momentum.doubleLineFERM) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_doubleLine_GLUON:" + String.valueOf(Momentum.doubleLineGLUON) + System.getProperty("line.separator"));
      bw.write("Mom_arrow_doubleLine_PHOT:" + String.valueOf(Momentum.doubleLinePHOT) + System.getProperty("line.separator"));

      bw.write("Show_description_shape:" + Shaped.showDescDef + System.getProperty("line.separator"));
      bw.write("Description_shape_rel_X:" + Shaped.descPosXDef + System.getProperty("line.separator"));
      bw.write("Description_shape_rel_Y:" + Shaped.descPosYDef + System.getProperty("line.separator"));
      bw.write("Rotation_description_shape:" + Shaped.rotDescDef + System.getProperty("line.separator"));
      bw.write("Scale_description_shape:" + Shaped.descScaleDef + System.getProperty("line.separator"));
      if (this.convCanToModel == 1)
        bw.write("Convert_canvas_to_model:false" + System.getProperty("line.separator"));
      if (this.convCanToModel == 2)
        bw.write("Convert_canvas_to_model:true" + System.getProperty("line.separator"));

      bw.write("Max_history:" + String.valueOf(drawPane.maxHistory) + System.getProperty("line.separator"));

      bw.write("Auto_blanks:" + String.valueOf(FeynLine.autoBlanks) + System.getProperty("line.separator"));
      bw.write("PDF_export_dim_width:" + String.valueOf(Frame.pdfExportDim.getWidth()) + System.getProperty("line.separator"));
      bw.write("PDF_export_dim_height:" + String.valueOf(Frame.pdfExportDim.getHeight()) + System.getProperty("line.separator"));
      bw.write("Img_export_dim_width:" + String.valueOf(Frame.imageExportDim.getWidth()) + System.getProperty("line.separator"));
      bw.write("Img_export_dim_height:" + String.valueOf(Frame.imageExportDim.getHeight()) + System.getProperty("line.separator"));

      if (toolTip) bw.write("Show_ToolTip:true" + System.getProperty("line.separator"));
      else bw.write("Show_ToolTip:false" + System.getProperty("line.separator"));
      bw.write("Canvas_width:" + String.valueOf((int) this.drawPaneSize.getWidth()) + System.getProperty("line.separator"));
      bw.write("Canvas_height:" + String.valueOf((int) this.drawPaneSize.getHeight()) + System.getProperty("line.separator"));
      bw.write("LaTeX_font_size:" + String.valueOf((int) ui.Frame.LaTeXFontSize) + System.getProperty("line.separator"));
      bw.write("Update_notifications:" + notifyUpdates + System.getProperty("line.separator"));

      bw.write("Clear_Canvas_On_Paste:" + Boolean.toString(this.clearCanvasOnPaste) + System.getProperty("line.separator"));
//      bw.write("Layout_options_string:" + GraphLayoutAlgorithm.getLayoutAlgorithm().saveParameters() + System.getProperty("line.separator"));
      bw.write("Static_Layout_Options:" + GraphLayoutAlgorithm.saveStaticParameters() + System.getProperty("line.separator"));
      bw.write("Layout_Presets:" + String.join(":", GraphLayoutAlgorithm.savePresets()) + System.getProperty("line.separator")); //Yes, they are joined with a colon on purpose
      //VERY IMPORTANT that 'Selected_Preset_Index' is written to file AFTER 'Layout_Presets'!!!
      bw.write("Selected_Preset_Index:" + Integer.toString(GraphLayoutAlgorithm.getSelectedPresetIndex()) + System.getProperty("line.separator"));
      bw.write("Unknown_Lines_Dashed:" + Boolean.toString(ModelData.getDashed()) + System.getProperty("line.separator"));
      bw.write("Graph_Layout_External_Mode:" + Integer.toString(GraphLayoutAlgorithm.getSelectedLayoutAlgorithm()) + System.getProperty("line.separator"));
      bw.write("Graph_Layout_Grid_Auto_Layout:" + Boolean.toString(GraphLayoutAlgorithm.gridAutoLayout()) + System.getProperty("line.separator"));
      bw.write("Dialog_Always_on_Top:" + Boolean.toString(dialogFramesAlwaysOnTop) + System.getProperty("line.separator"));
      bw.write("Recents:");
      for (int i = 0; i < recentEntries.size(); i++) {
        bw.write(recentEntries.get(i));
        if (i != recentEntries.size()-1)
          bw.write("|");
      }
      bw.write(System.getProperty("line.separator"));
      bw.write("Reset_Export_Destination:" + Boolean.toString(resetExportDestinationOnLoad) + System.getProperty("line.separator"));

      ExportManager.writePreferences(bw);
      QgrafImportManager.writePreferences(bw);
    } catch (Exception e) {
      Frame.logger.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  /**
   * writes the user diagram for the gamemode to the file stored under:
   * <ul><li>Windows: C:\\Users\\<i>User_Name</i>\\AppData\\Roaming\\FeynGame\\</li>
   * <li>GNU: /home/.config/FeynGame/</li>
   * <li>MacOs: ~/Library/Preferences/FeynGame/</li></ul>
   **/
  private void setLastDiagram() {
    String prefFileSt = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      prefFileSt = System.getProperty("user.home") + "/Library/Preferences/FeynGame/last.fg";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      prefFileSt = System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\last.fg";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      prefFileSt = System.getProperty("user.home") + "/.config/FeynGame/last.fg";
    }
    File prefFile = new File(prefFileSt);
    try {
      prefFile.getParentFile().mkdirs();
      prefFile.createNewFile();
    } catch (IOException iex) {
      Frame.logger.log(Level.SEVERE, iex.getMessage(), iex);
    }
    if (!prefFile.canWrite()) {
      Frame.logger.severe("Could not save diagram for later use.");
      return;
    }

    Frame.logger.info("Saved diagram to: " + prefFile.getAbsolutePath());

    this.currentFile = prefFile.getAbsolutePath();
    this.saveDiagram();
  }
  /**
   * this function writes the {@link game.Filling#imageNumberToImage} map
   * to an objectoutputstream
   * @param out the ObjectOutputStream
   * @throws IOException if the writing fails
   */
  private void writeNumberToImage(ObjectOutputStream out) throws IOException {
    boolean debug = false;
    Frame.logger.fine("Writing images ...");
    out.writeInt(game.Filling.imageNumberToImage.size()); // how many images are serialized?
    if (game.Filling.imageNumberToImage.size() == 0)
      return;
    for (Integer number : game.Filling.imageNumberToImage.keySet()) {
      if (game.Filling.imageNumberToImage.get(number) != null) {
        Frame.logger.fine("Writing Image number " + number + "...");
        out.writeInt(number);
        ImageIO.write(game.Filling.imageNumberToImage.get(number), "png", out); // png is lossless
      }
    }
    Frame.logger.fine("Writing JavaFX ...");
  }
  /**
   * this function reads the {@link game.Filling#imageNumberToImage} map
   * from an objectinputstream
   * @param in the ObjectInputStream
   * @throws IOException if the reading fails
   * @throws ClassNotFoundException ???
   */
  private void readNumberToImage(ObjectInputStream in) throws IOException, ClassNotFoundException {
    boolean debug = false;
    Frame.logger.fine("Reading images ...");
    final int imageCount = in.readInt();
    game.Filling.imageNumberToImage = new HashMap<Integer, BufferedImage>(imageCount);
    if (imageCount == 0) {
      return;
    }
    for (int i=0; i<imageCount; i++) {
      try {
        Integer inte = in.readInt();
        Frame.logger.fine("Reading Image number " + inte + "...");
        BufferedImage bI = ImageIO.read(in);
        game.Filling.imageNumberToImage.put(inte, bI);
      } catch (java.io.EOFException eofex) {
        Frame.logger.fine("End of file has been reached!");
        Frame.logger.fine("Reading JavaFX ...");
        return;
      }
    }
    Frame.logger.fine("Reading JavaFX ...");
  }
  /**
   * this sets for all vertices that are part of a floatingObject the
   * autoremove flag to true because the flag set to false leads to the
   * vertex "wobbling"
   */
  private void wobbleFloatingVertex() {
    for (FloatingObject fO : drawPane.fObjects) {
      if (fO.v) {
        fO.vertex.autoremove = true;
      }
    }
    for (ArrayList<FloatingObject> list : drawPane.undoListfObjects) {
      for (FloatingObject fO : list) {
        if (fO.v) {
          fO.vertex.autoremove = true;
        }
      }
    }
    for (ArrayList<FloatingObject> list : drawPane.redoListfObjects) {
      for (FloatingObject fO : list) {
        if (fO.v) {
          fO.vertex.autoremove = true;
        }
      }
    }
  }

  /**
   * loads the user diagram for the gamemode to the file stored under:
   * <ul><li>Windows: C:\\Users\\<i>User_Name</i>\\AppData\\Roaming\\FeynGame\\</li>
   * <li>GNU: /home/.config/FeynGame/</li>
   * <li>MacOs: ~/Library/Preferences/FeynGame/</li></ul>
   **/
  @SuppressWarnings("unchecked")
  private void getLastDiagram() {
    this.openNew();
    String prefFileSt = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      prefFileSt = System.getProperty("user.home") + "/Library/Preferences/FeynGame/last.fg";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      prefFileSt = System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\last.fg";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      prefFileSt = System.getProperty("user.home") + "/.config/FeynGame/last.fg";
    }
    File prefFile = new File(prefFileSt);
    if (!prefFile.canRead())
      return;

    String sp = this.diagramSavePath;
    String title = this.getTitle();
    String curr = this.currentFile;

    this.loadDiagram(prefFile, null, true);

    fileNameProp = "untitled";
    this.setTitle(title);
    //    ExportManager.setNextGuiExportDestination(new File("Untitled.fg"), true);
    this.currentFile = curr;
    this.diagramSavePath = sp;
  }
  /**
   * checks if any JavaFX files used in a ".fg" file was moved, renamed, or deleted and if so prompts to choose another one
   */
  private void checkJavaFX(){
    for (Vertex v : drawPane.vertices) {
      Filling f = v.filling;
      if (f.name != null) {
        File file = new File(f.name);
        if (!file.exists() && !javafx.JavaFXToJavaVectorGraphic.checkIfExist(f.name)) {
          f.name = newJavaFX(f.name);
        }
      }
    }
    for (FloatingObject fO : drawPane.fObjects) {
      if (fO.v) {
        Filling f = fO.vertex.filling;
        if (f.name != null) {
          File file = new File(f.name);
          if (!file.exists() && !javafx.JavaFXToJavaVectorGraphic.checkIfExist(f.name)) {
            f.name = newJavaFX(f.name);
          }
        }
      } else {
        if (fO.floatingImage.name != null) {
          File file = new File(fO.floatingImage.name);
          if (!file.exists() && !javafx.JavaFXToJavaVectorGraphic.checkIfExist(fO.floatingImage.name)) {
            fO.floatingImage.name = newJavaFX(fO.floatingImage.name);
          }
        }
      }
    }
    for (Shaped shape : drawPane.shapes) {
      Filling f = shape.fill;
      if (f.name != null) {
        File file = new File(f.name);
        if (!file.exists() && !javafx.JavaFXToJavaVectorGraphic.checkIfExist(f.name)) {
          f.name = newJavaFX(f.name);
        }
      }
    }
  }
  /**
   * if there is a JavaFX file missing this prompts to choose another one
   * @param oldName the old name of the missing JavaFX pattern
   * @return the name of the new pattern
   */
  private String newJavaFX(String oldName) {

    if (javafx.JavaFXToJavaVectorGraphic.patternsIn.contains(oldName))
      return oldName;

    if (javafx.JavaFXToJavaVectorGraphic.patternsOut.contains(oldName))
      return oldName;

    JOptionPane.showMessageDialog(new JFrame(), "JavaFX at " + oldName + " could not be loaded. Please select an JavaFX to replace it with.", "Failed to load JavaFX", JOptionPane.ERROR_MESSAGE);

    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What JavaFX do you want to load?");
    fc.setApproveButtonText("Load");

    if (this.imgLoadPath != null) {
      fc.setCurrentDirectory(new File(this.imgLoadPath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    FileFilter filter4 = new FileNameExtensionFilter("JavaFx files (*.fx)", "fx");
    fc.addChoosableFileFilter(filter4);

    int returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        File file = fc.getSelectedFile();
        this.imgLoadPath = file.getParentFile().getAbsolutePath();
        String path = file.getPath();

        return path;

      } catch (Exception ex) {
        Frame.this.requestFocus();
        Frame.logger.severe("Error while loading JavaFX)");
      }
    }
    return "";
  }
  /**
   * when an image was deleted this prompts you to choose a replacement image
   * @param key the path of the not found image
   */
  private void imageDeleted(String key) {
    JOptionPane.showMessageDialog(new JFrame(), "Image at " + key + " could not be loaded. Please select an image to replace it with.", "Failed to load image", JOptionPane.ERROR_MESSAGE);

    int imageNumber = Filling.pathToImageNumber.get(key);
    Filling.pathToImageNumber.remove(key);
    Filling.imageNumberToImage.remove(imageNumber);

    final JFileChooser fc = new JFileChooser();
    fc.setMultiSelectionEnabled(false);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setDialogType(JFileChooser.OPEN_DIALOG);
    fc.setDialogTitle("What image do you want to load?");
    fc.setApproveButtonText("Load");

    if (this.imgLoadPath != null) {
      fc.setCurrentDirectory(new File(this.imgLoadPath));
    } else if (System.getProperty("user.dir") != null && System.getProperty("user.dir") != "null" && System.getProperty("user.dir") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    } else if (System.getProperty("user.home") != null && System.getProperty("user.home") != "null" && System.getProperty("user.home") != "") {
      fc.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

    FileFilter filter = new FileNameExtensionFilter("Joint Photographic Experts Group (*.jpg/*.jpeg)", "jpg", "jpeg");
    fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());
    fc.addChoosableFileFilter(filter);
    FileFilter filter1 = new FileNameExtensionFilter("Portable Network Graphics (*.png)", "png");
    fc.addChoosableFileFilter(filter1);
    FileFilter filter2 = new FileNameExtensionFilter("Graphics Interchange Format (*.gif)", "gif");
    fc.addChoosableFileFilter(filter2);
    FileFilter filter3 = new FileNameExtensionFilter("Device-Independent Bitmap (*.bmp)", "bmp");
    fc.addChoosableFileFilter(filter3);
    FileFilter filter4 = new FileNameExtensionFilter("JavaFx files (*.fx)", "fx");
    fc.addChoosableFileFilter(filter4);

    int returnVal = fc.showSaveDialog(Frame.this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        File file = fc.getSelectedFile();
        this.imgLoadPath = file.getParentFile().getAbsolutePath();
        String path = file.getPath();

        game.Filling.pathToImageNumber.put(path, imageNumber);

        game.Filling.imageNumberToImage.put(game.Filling.pathToImageNumber.get(path), ImageIO.read(new File(path)));

      } catch (Exception ex) {
        Frame.this.requestFocus();
        Frame.logger.severe("Error while loading image)");
      }
    }

  }
  /**
   * when the window is closed, preferences get saved and in the drawing mode
   * also the current drawing gets saved
   * @param e the window event
   */
  public void windowClosing(WindowEvent e) {
    if (drawPane.multiEdit != null)
      drawPane.multiEdit.fin();
    this.setPreferences();
    if (this.gameMode == 0) {
      this.setLastDiagram();
    }
  }

  private JFileChooser setupModelFileChooser() {
    FileFilter labelledAcceptAll = new FileFilter() {
      @Override
      public String getDescription() {
        return "qgraf model files (*.*)";
      }

      @Override
      public boolean accept(File f) {
        return true;
      }
    };

    JFileChooser jfc;
    /*
     * There is a hard-to-reproduce and windows-specific bug that throws a NPE
     * somewhere deep inside internal classes. This only occurs sometimes, so a retry might fix it?
     */
    try {
      jfc = new JFileChooser();
    } catch (NullPointerException e) {
      jfc = new JFileChooser();
    }
    jfc.setMultiSelectionEnabled(false);
    jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    jfc.setApproveButtonText("Import");
    jfc.setDialogTitle("Select a qgraf model file to import");
    jfc.setAcceptAllFileFilterUsed(false);
    jfc.addChoosableFileFilter(labelledAcceptAll);

    return jfc;
  }

  public Presets getSelectedPresets() {
    return models.get(tileTabs.getCurrentKey());
  }

  public void setDiagramAndMultiMode(QgrafOutputFile out, boolean loadAll) {
    setDiagramAndMultiMode(out, 0, loadAll);
  }

  public void setDiagramAndMultiMode(QgrafOutputFile out, int checkedDiagramIndexBase0, boolean loadAll) {
    QgrafDiagram diagram = out.getDiagram(checkedDiagramIndexBase0);
    if(!setDiagram(diagram, CLEARMODE_CLEAR_ALL_EXCEPT_MBB, true, true, true, !QgrafImportManager.loadMomentumArrows())) return;

    if(loadAll) {
      currentQgrafFile = out;
      currentQgrafFileIndex = checkedDiagramIndexBase0;
      setExtendedQgrafMenuVisible(true);
      setTitle("qgraf imported: " + out.getName() + " (diagram " + (currentQgrafFileIndex+1) + "/" + currentQgrafFile.getDiagramCount() + ")");
    } else {
      currentQgrafFile = null;
      currentQgrafFileIndex = -1;
      setExtendedQgrafMenuVisible(false);
      setTitle("qgraf imported: " + out.getName());
    }
  }

  private boolean setDiagram(QgrafDiagram graph, int clearMode, boolean askUnknownLines, boolean applyLayout, boolean refocus, boolean deleteMomenta) {

    if(applyLayout) {
      try {
        graph.applySelectedLayout(drawPane.getDiagramLayoutArea());
      } catch (GraphLayoutException e) {
        JOptionPane.showMessageDialog(this, e.getMessage());
        return false;
      }
    }

    clearCanvas(clearMode);
    drawPane.putDiagramData(graph, refocus, applyLayout);
    sourceFrame.updateSourceCode(graph);

    if(deleteMomenta) {
      for(FeynLine fl : drawPane.lines) {
        fl.momArrow = null;
      }
    }

    if(askUnknownLines && graph.getModel() != null && graph.getModel().hasGeneratedLines()
        && graph.getModel().askUnknownLines()) {
      int result = JOptionPane.showOptionDialog(this, "Diagram contains unknown particles. Choose action:",
          "Create Model?", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
          null, new String[]{ "Create model", "Don't create model" }, "Create Model");

      if(result == 0) {
        Presets updated = graph.getModel().getUsedPreset();
        updated.allowUnsaved = true;
        updated.unsaved = true;
        tileTabs.addModel(updated, "qgraf-imported");
        //        tileTabs.remove(currentKey);

        tileTabs.update();
        tileTabs.setCurrUnsaved();
      }
    }

    Frame.this.repaint();

    return true;
  }

  public void setDiagramNoGui(QgrafDiagram graph, int clearMode) throws GraphLayoutException{
    graph.applySelectedLayout(drawPane.getDiagramLayoutArea());
    clearCanvas(clearMode);
    drawPane.putDiagramData(graph, true, true);
    sourceFrame.updateSourceCode(graph);
    Frame.this.repaint();
  }

  private void setExtendedQgrafMenuVisible(boolean visible) {
    if(qgrafMenu != null) {
      qgrafMenu.setVisible(visible);
    }
    if(qgrafUpdateModelExt != null) {
      qgrafUpdateModelExt.setEnabled(visible);
    }
  }

  /**
   * Alternative to {@link #openNew()} that does not reset some properties like file name, current diagram
   * and undo list
   */
  private void clearCanvas(int clearMode) {
    drawPane.clearPane(clearMode);

    if((clearMode & CLEARMODE_CLEAR_MBB) != 0) {
      if(showBoundingBox != null) showBoundingBox.setSelected(drawPane.mBB);
      if(mbbFrame != null) mbbFrame.updateMBBsize();
    }

    if((clearMode & CLEARMODE_CLEAR_HISTORY) != 0) {
      drawPane.undoListLines.clear();
      drawPane.redoListLines.clear();
      drawPane.undoListVertices.clear();
      drawPane.redoListVertices.clear();
      drawPane.undoListfObjects.clear();
      drawPane.redoListfObjects.clear();
      drawPane.redoListShapes.clear();
      drawPane.undoListShapes.clear();
    }

    if((clearMode & CLEARMODE_CLEAR_META) != 0) {
      fileNameProp = "untitled";
      this.currentFile = null;
      this.currentQgrafFile = null;
      this.currentQgrafFileIndex = -1;
      this.modelForAllQgrafLoads = null;
      setExtendedQgrafMenuVisible(false);
      this.setTitle("Untitled.fg - " + "FeynGame v" + this.version);
      //      ExportManager.setNextGuiExportDestination(new File("Untitled.fg"), true);
    }

    if(clearMode != CLEARMODE_NOTHING) {
      Frame.this.repaint();
      if (this.eframe != null) {
        this.eframe.editPane.upEditPane();
        this.eframe.repaint();
      }
      if (this.beframe != null)
        beframe.update();
    }
  }

  //Flag definitions for #clearCanvas method, which is more flexible than #openNew
  public static final int CLEARMODE_NOTHING = 0;
  public static final int CLEARMODE_CLEAR_LINES = 1; //Currently visible lines
  public static final int CLEARMODE_CLEAR_SHAPES = 8; //Currently visible shapes
  public static final int CLEARMODE_CLEAR_MBB = 16; //The layout rectangle
  public static final int CLEARMODE_CLEAR_HISTORY = 2; //Undo history
  public static final int CLEARMODE_CLEAR_META = 4; //Filename, window title etc
  public static final int CLEARMODE_RESET_PANE_STATE = 32; //The zeroPoint and scale oft he drawPane
  //Useful flag combinations
  public static final int CLEARMODE_CLEAR_DIAGRAM = CLEARMODE_CLEAR_LINES | CLEARMODE_CLEAR_SHAPES;
  public static final int CLEARMODE_CLEAR_DRAWPANE_CONTENT = CLEARMODE_CLEAR_DIAGRAM | CLEARMODE_CLEAR_MBB | CLEARMODE_RESET_PANE_STATE;
  public static final int CLEARMODE_CLEAR_ALL = (1 << 31) - 1; //all bits set
  public static final int CLEARMODE_CLEAR_ALL_EXCEPT_MBB = CLEARMODE_CLEAR_ALL & ~CLEARMODE_CLEAR_MBB;

  public void windowClosed(WindowEvent e) {
    //Only one frame is used during the entire runtime of the program, so dispose it upon exit
    if(sourceFrame != null) sourceFrame.dispose();
    if(mbbFrame != null) mbbFrame.dispose();
    if(settingsFrame != null) settingsFrame.dispose();
    if(eframe != null) eframe.dispose();
  }

  public void windowOpened(WindowEvent e) {
  }

  public void windowIconified(WindowEvent e) {
  }

  public void windowDeiconified(WindowEvent e) {
  }

  public void windowActivated(WindowEvent e) {
  }

  public void windowDeactivated(WindowEvent e) {
  }
  /**
   * open the About FeynGame window
   */
  private void aboutFGDialog() {
    new AboutFrame();
  }
  private void showLogs() {
    JDialog logDialog = new JDialog(this, "Log File", true);
    logDialog.setSize(800, 600);
    logDialog.setLayout(new BorderLayout());


    JTextArea textArea = new JTextArea();
    textArea.setEditable(false);

    final String logFilePath;
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      logFilePath = (System.getProperty("user.home") + "/Library/Preferences/FeynGame/FeynGame.log");
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      logFilePath = (System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\FeynGame.log");
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      logFilePath = (System.getProperty("user.home") + "/.config/FeynGame/FeynGame.log");
    } else {
      logFilePath = "FeynGame.log";
    }
    try (BufferedReader reader = new BufferedReader(new FileReader(logFilePath))) {
      String line;
      while ((line = reader.readLine()) != null) {
        textArea.append(line + "\n");
      }
    } catch (IOException e) {
        JOptionPane.showMessageDialog(this, "Failed to load log file: " + e.getMessage(),
                "Error", JOptionPane.ERROR_MESSAGE);
            return;
    }
    JScrollPane scrollPane = new JScrollPane(textArea);
    logDialog.add(scrollPane, BorderLayout.CENTER);
    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    JButton copyButton = new JButton("Copy to Clipboard");
    copyButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        StringSelection stringSelection = new StringSelection(textArea.getText());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
      }
    });
    JButton closeButton = new JButton("Close");
    closeButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        logDialog.dispose();
      }
    });
    buttonPanel.add(copyButton);
    buttonPanel.add(closeButton);
    logDialog.add(buttonPanel, BorderLayout.SOUTH);
    logDialog.setLocationRelativeTo(this);
    logDialog.setVisible(true);
  }
  /**
   * Resets the {@link ui.Frame#scrollCounter}
   */
  private class WheelMovementTimerActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      scrollCounter = 0;
      if (drawPane.selectType == 3) {
        drawPane.shapes.get(drawPane.shapes.size() - 1).resetAtt();
      }
    }
  }
  /**
   * counts down the time
   */
  private class timeTimerActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      remTime--;
      if (remTime == -1) {
        timesUp();
      }
      time.setText("<html><h2> " + remTime);
      javax.swing.Timer timerr = new javax.swing.Timer(1000, new timeTimerActionListener());
      timerr.setRepeats(false);
      timerr.start();
    }
  }

  /**
   * Inner class which handels the right click menu.
   */
  class PopMenu extends JPopupMenu {
    /**
     * Serial Version UID
     */
    static final long serialVersionUID = 3L;
    /**
     * index of line that the line_from_here was called from
     */
    int fl_index;
    /**
     * decides wether or not to cut the line given by fl_index
     */
    boolean checkfl_index = false;
    /**
     * basic constructor of the popup menu
     * @param e the mouse event that lead to this popup menu
     */
    PopMenu(MouseEvent e) {
      /*            JMenuItem test;

      this.add(test = new JMenuItem("test123", GetResources.getIcon("freeIcon.png", 32)));
      test.setHorizontalTextPosition(JMenuItem.RIGHT);
      test.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          System.out.println("click");
        }
      });

      this.add(new JSeparator()); // vs. this.addSeparator();

      this.add(test = new JMenuItem("test456", GetResources.getIcon("inOutIcon.png", 32)));
       */

      if (gameMode == 1) return;
      configureOptions(e);

      this.show(Frame.this, -Frame.this.getX() + e.getXOnScreen(), -Frame.this.getY() + e.getYOnScreen() + 5);
    }

    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
    }
    /**
     * this is the submemu "line from here" that lets one draw a line from
     * the point of the right click event
     * @param intersect the point of the right click event
     */
    private void line_from_here(Point2D intersect) {
      /*
      JMenu subMenu;
      this.add(subMenu = new JMenu("Line from here"));

      int rows = (int) drawPane.getHeight() / (TileSelect.uiHeight);
      int columns = (int) Math.max(Math.ceil((float) (LineConfig.presets.size() - 1) / (float) rows), 1);
      rows = (int) Math.ceil((float) (LineConfig.presets.size() - 1)/(float) columns);
      GridLayout lay = new GridLayout(rows, columns);
      subMenu.getPopupMenu().setLayout(lay);

      for (LineConfig tmp_line : LineConfig.presets) {
        if (tmp_line.lineType.equals(LineType.GRAB)) continue;

        TileLabel tl = new TileLabel(tmp_line);
        tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

        subMenu.add(tl);
        tl.setHorizontalTextPosition(JMenuItem.RIGHT);
        tl.addMouseMotionListener(new MouseMotionListener() {
          @Override
          public void mouseDragged(MouseEvent mouseEvent) {
            Point p = new Point(mouseEvent.getX(), mouseEvent.getY());
            p = SwingUtilities.convertPoint(tl, p, drawPane);
            MouseEvent me = new MouseEvent((Component) mouseEvent.getSource(), mouseEvent.getID(), mouseEvent.getWhen(), mouseEvent.getModifiersEx(), p.x, p.y, mouseEvent.getClickCount(), mouseEvent.isPopupTrigger());
            drawPane.dispatchEvent(me);
          }

          @Override
          public void mouseMoved(MouseEvent mouseEvent) {
          }
        });
        tl.addMouseListener(new MouseListener() {
          @Override
          public void mouseClicked(MouseEvent e) {

          }

          @Override
          public void mousePressed(MouseEvent e) {
            JPopupMenu c2;
            for (Component c1 : PopMenu.this.getComponents())
              if (c1 instanceof JMenu) {
                ((JMenu) c1).getPopupMenu().setLayout(new BorderLayout());
                JMenu c1_2 = (JMenu) c1;
                for (Component c : c1_2.getMenuComponents())
                  if ((c instanceof TileLabel)) c.setVisible(false);


                c2 = c1_2.getPopupMenu();
                c2.setOpaque(false);
                c2.setLocation(10, 10); // trigger redraw
              }

            PopMenu.this.removeAll();

            PopMenu.this.setLocation(10, 10); // trigger redraw

            Frame.this.saveForUndo();

            Point mousePos = MouseInfo.getPointerInfo().getLocation();
            SwingUtilities.convertPointFromScreen(mousePos, drawPane);
            Point2D realMousePos = new Point2D((double) mousePos.x, (double) mousePos.y);

            realMousePos = new Point2D(realMousePos.x / drawPane.scale - drawPane.zeroPoint.x, realMousePos.y / drawPane.scale - drawPane.zeroPoint.y);
            if (checkfl_index)
              cutLine(fl_index, intersect);

            drawPane.lines.add(new FeynLine(new Line(intersect.x, intersect.y, realMousePos.x , realMousePos.y), new LineConfig(tmp_line)));
            drawPane.checkForVertex(intersect);
            drawPane.mouseDown = true;
            drawPane.selectType = 0;
            draggedMode = 1;

            draggedDist = new Point.Double(realMousePos.x, realMousePos.y);
            Frame.this.drawPane.requestFocus();
            draggedLine = drawPane.lines.get(drawPane.lines.size() - 1);
            dragDisX = 0;
            dragDisY = 0;
            if (Frame.this.eframe != null) {
              Frame.this.eframe.editPane.upEditPane();
              Frame.this.eframe.repaint();
            }
            if (beframe != null) {
              beframe.update();
            }
          }

          @Override
          public void mouseReleased(MouseEvent e) {
            PopMenu.this.menuSelectionChanged(false);
          }

          @Override
          public void mouseEntered(MouseEvent e) {

          }

          @Override
          public void mouseExited(MouseEvent e) {

          }
        });
      }
       */
    }
    /**
     * this adds components to the popup menu that are shown when a line
     * was hit
     * @param fl a reference to the line that was hit
     * @param intersect the point where the hit took place
     * @param fl_index the index of the line
     */
    void lineClicked(FeynLine fl, Point2D intersect, int fl_index) {

      JMenuItem edit = new JMenuItem("Edit line");
      this.add(edit);
      edit.addActionListener(e -> {
        int index = drawPane.lines.indexOf(fl);
        if(index != -1) {
          drawPane.lines.remove(index);
          drawPane.lines.add(fl);
          if(eframe != null) {
            eframe.setVisible(true);
            eframe.toFront();
            eframe.editPane.upEditPane();
            eframe.repaint();
          }
        } else {
          logger.warning("Cannot find line in drawPane");
        }
      });

      /*
      JMenuItem flip = new JMenuItem("Invert line");
      this.add(flip);
      flip.addActionListener(e -> {
        if (!fl.isFixed()) {
          Frame.this.saveForUndo();
          Point2D temp = fl.getStart();
          fl.setStart(fl.getEnd());
          fl.setEnd(temp);
          fl.phase *= -1;
          if (fl.getStart().equals(fl.getEnd()))
            fl.circAngle += Math.PI;
          fl.setHeight(-fl.getHeight());
          combineLines(fl.getStart());
          combineLines(fl.getEnd());
          drawPane.checkForVertex(fl.getStart());
          drawPane.checkForVertex(fl.getEnd());
        }
        if (eframe != null) {
          eframe.editPane.upEditPane();
          eframe.repaint();
        }
        if (beframe != null)
          beframe.update();
      });

      if(fl.isMom()) {
        JMenuItem flipMom = new JMenuItem("Invert momentum");
        this.add(flipMom);
        flipMom.addActionListener(e -> {
          saveForUndo();
          fl.momArrow.invert = !fl.momArrow.invert;
          if (eframe != null) {
            eframe.editPane.upEditPane();
            eframe.repaint();
          }
          if (beframe != null)
            beframe.update();
        });
      }
       */

      this.addSeparator();

      JMenu subMenu;
      this.add(subMenu = new JMenu("Change line"));

      int rows = (int) drawPane.getHeight() / (TileSelect.uiHeight);
      int columns = (int) Math.max(Math.ceil((float) (LineConfig.presets.size() - 1) / (float) rows), 1);
      rows = (int) Math.ceil((float) (LineConfig.presets.size() - 1)/(float) columns);
      GridLayout lay = new GridLayout(rows, columns);
      subMenu.getPopupMenu().setLayout(lay);

      for (LineConfig tmp_line : LineConfig.presets) {
        if (tmp_line.lineType.equals(LineType.GRAB)) continue;

        TileLabel tl = new TileLabel(tmp_line);
        tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

        subMenu.add(tl);
        tl.setHorizontalTextPosition(JMenuItem.RIGHT);
        tl.addMouseListener(new MouseListener() {
          @Override
          public void mouseClicked(MouseEvent e) {

          }

          @Override
          public void mousePressed(MouseEvent e) {
            PopMenu.this.setVisible(false);

            Frame.this.saveForUndo();

            //drawPane.lines.add(new FeynLine(new Line(intersect.x, intersect.y, e.getXOnScreen()-Frame.this.getX() , e.getYOnScreen()-Frame.this.getY()), new LineConfig(tmp_line)));

            drawPane.lines.get(fl_index).changeConfigTo(tmp_line);

            drawPane.selectType = 0;
            if (Frame.this.eframe != null) {
              Frame.this.eframe.editPane.upEditPane();
              Frame.this.eframe.repaint();
            }
            if (beframe != null) {
              beframe.update();
            }
          }

          @Override
          public void mouseReleased(MouseEvent e) {

          }

          @Override
          public void mouseEntered(MouseEvent e) {

          }

          @Override
          public void mouseExited(MouseEvent e) {

          }
        });
      }

      if (!intersect.equals(fl.getStart()) && !intersect.equals(fl.getEnd())) {
        checkfl_index = true;
        this.fl_index = fl_index;
        //        line_from_here(intersect);
        if (Vertex.vertexPresets.size() > 0) {
          this.add(subMenu = new JMenu("Cut here"));

          rows = (int) drawPane.getHeight() / (TileSelect.uiHeight);
          columns = (int) Math.max(Math.ceil((float) (Vertex.vertexPresets.size()) / (float) rows), 1);
          rows = (int) Math.ceil((float) (Vertex.vertexPresets.size())/(float) columns);
          GridLayout lay_ = new GridLayout(rows, columns);
          subMenu.getPopupMenu().setLayout(lay_);

          /* any operation not changing the number of lines at the vertex, removes the vertex again */
          for (Map.Entry<Vertex, String> tmp_line : Vertex.vertexPresets) {

            VertexLabel tl = new VertexLabel(tmp_line.getKey());
            tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
            tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
            tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

            subMenu.add(tl);
            tl.setHorizontalTextPosition(JMenuItem.RIGHT);
            tl.addActionListener(e -> {
              PopMenu.this.setVisible(false);
              /* cutting line with index i at position intersect */
              /* since it was not clicked on either end of the fl, we are not close to an end */
              /* if the newly created vertex is not changed somehow, the next draw operation will recombine both lines */
              try {
                Frame.this.saveForUndo();
                cutLine(fl_index, intersect);
                drawPane.vertices.get(drawPane.vertices.size() -1).autoremove = false;
                Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(intersect.x, intersect.y), null, -1, false, 5);
                int index = intersectPair.getValue();
                if (index < 0) {
                  index = -index - 1;
                  Vertex new_vt = new Vertex(tmp_line.getKey());
                  new_vt.origin = drawPane.vertices.get(index).origin;
                  boolean autoremove = drawPane.vertices.get(index).autoremove;
                  drawPane.vertices.remove(index);
                  new_vt.autoremove = autoremove;
                  drawPane.vertices.add(new_vt);

                }
                Frame.this.repaint();
                drawPane.selectType = 1;
                if (Frame.this.eframe != null) {
                  Frame.this.eframe.editPane.upEditPane();
                  Frame.this.eframe.repaint();
                }
                if (beframe != null) {
                  beframe.update();
                }
              } catch (Exception ex) {
                Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
              }
            });
          }
        } else {
          this.add(subMenu = new JMenu("Cut here"));
          VertexLabel tl = new VertexLabel(new Vertex());
          tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

          subMenu.add(tl);
          tl.setHorizontalTextPosition(JMenuItem.RIGHT);
          tl.addActionListener(e -> {
            PopMenu.this.setVisible(false);
            /* cutting line with index i at position intersect */
            /* since it was not clicked on either end of the fl, we are not close to an end */
            /* if the newly created vertex is not changed somehow, the next draw operation will recombine both lines */
            try {
              Frame.this.saveForUndo();
              cutLine(fl_index, intersect);
              drawPane.vertices.get(drawPane.vertices.size() -1).autoremove = false;
              Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(intersect.x, intersect.y), null, -1, false, 5);
              int index = intersectPair.getValue();
              if (index < 0) {
                index = -index - 1;
                Vertex new_vt = new Vertex();
                new_vt.origin = drawPane.vertices.get(index).origin;
                boolean autoremove = drawPane.vertices.get(index).autoremove;
                drawPane.vertices.remove(index);
                new_vt.autoremove = autoremove;
                drawPane.vertices.add(new_vt);

              }
              Frame.this.repaint();
              drawPane.selectType = 1;
              if (Frame.this.eframe != null) {
                Frame.this.eframe.editPane.upEditPane();
                Frame.this.eframe.repaint();
              }
              if (beframe != null) {
                beframe.update();
              }
            } catch (Exception ex) {
              Frame.logger.log(Level.SEVERE, ex.getMessage(), ex);
            }
          });
        }


      } else {
        //        line_from_here(intersect);
        this.add(subMenu = new JMenu("Add vertex"));

        rows = (int) drawPane.getHeight() / (TileSelect.uiHeight);
        columns = (int) Math.max(Math.ceil((float) (Vertex.vertexPresets.size()) / (float) rows), 1);
        rows = (int) Math.ceil((float) Vertex.vertexPresets.size()/(float) columns);
        GridLayout lay_ = new GridLayout(rows, columns);
        subMenu.getPopupMenu().setLayout(lay_);
        /* any operation not changing the number of lines at the vertex, removes the vertex again */
        for (Map.Entry<Vertex, String> tmp_line : Vertex.vertexPresets) {

          VertexLabel tl = new VertexLabel(tmp_line.getKey());
          tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

          subMenu.add(tl);
          tl.setHorizontalTextPosition(JMenuItem.RIGHT);
          tl.addActionListener(e -> {
            Frame.this.saveForUndo();
            //Frame.this.drawPane.vertices.add(new Vertex(intersect, tmp_line.getValue()));
            Vertex vt = new Vertex(tmp_line.getKey());
            vt.origin = intersect;
            Frame.this.drawPane.vertices.add(vt);

            drawPane.selectType = 1;
            if (Frame.this.eframe != null) {
              Frame.this.eframe.editPane.upEditPane();
              Frame.this.eframe.repaint();
            }
            if (beframe != null) {
              beframe.update();
            }
            PopMenu.this.setVisible(false);
          });
        }

      }
    }
    /**
     * adds options when an floating object was hit
     * @param index the index of the hit floating object
     */
    void fObjectClicked(int index) {

      JMenu subMenu;

      this.add(subMenu = new JMenu("Change floatingObject"));

      int rows = (int) drawPane.getHeight() / (TileSelect.uiHeight);
      int columns = (int) Math.max(Math.ceil((float) (Vertex.vertexPresets.size()+FloatingImage.fIPresets.size()) / (float) rows), 1);
      rows = (int) Math.ceil((float) (Vertex.vertexPresets.size()+FloatingImage.fIPresets.size())/(float) columns);
      GridLayout lay = new GridLayout(rows, columns);
      subMenu.getPopupMenu().setLayout(lay);

      for (Map.Entry<Vertex, String> tmp_line : Vertex.vertexPresets) {

        VertexLabel tl = new VertexLabel(tmp_line.getKey());
        tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

        subMenu.add(tl);
        tl.setHorizontalTextPosition(JMenuItem.RIGHT);
        tl.addActionListener(e -> {
          Frame.this.saveForUndo();

          FloatingObject new_fo = new FloatingObject(tmp_line.getKey(), drawPane.fObjects.get(index).getCenter());
          drawPane.fObjects.remove(index);
          drawPane.fObjects.add(new_fo);

          drawPane.selectType = 2;
          if (Frame.this.eframe != null) {
            Frame.this.eframe.editPane.upEditPane();
            Frame.this.eframe.repaint();
          }
          if (beframe != null) {
            beframe.update();
          }
          PopMenu.this.setVisible(false);
        });
      }

      for (FloatingImage tmp_line : FloatingImage.fIPresets) {

        ImageLabel tl = new ImageLabel(tmp_line);
        tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

        subMenu.add(tl);
        tl.setHorizontalTextPosition(JMenuItem.RIGHT);
        tl.addActionListener(e -> {
          Frame.this.saveForUndo();

          FloatingObject new_fo = new FloatingObject(tmp_line, drawPane.fObjects.get(index).getCenter());
          drawPane.fObjects.remove(index);
          drawPane.fObjects.add(new_fo);

          drawPane.selectType = 2;
          if (Frame.this.eframe != null) {
            Frame.this.eframe.editPane.upEditPane();
            Frame.this.eframe.repaint();
          }
          if (beframe != null) {
            beframe.update();
          }
          PopMenu.this.setVisible(false);
        });
      }
      line_from_here(drawPane.fObjects.get(index).getCenter());
    }
    /**
     * this adds options when nothing was hit
     * @param intersect the point where there was clicked
     */
    void nothingClicked(Point2D intersect) {

      JMenu subMenu;
      line_from_here(intersect);
      if (Vertex.vertexPresets.size() > 0 || FloatingImage.fIPresets.size() > 0) {
        this.add(subMenu = new JMenu("Add floatingObject"));

        int rows = (int) drawPane.getHeight() / (TileSelect.uiHeight);
        int columns = (int) Math.max(Math.ceil((float) (Vertex.vertexPresets.size()+FloatingImage.fIPresets.size()) / (float) rows), 1);
        rows = (int) Math.ceil((float) (Vertex.vertexPresets.size()+FloatingImage.fIPresets.size())/(float) columns);
        GridLayout lay = new GridLayout(rows, columns);
        subMenu.getPopupMenu().setLayout(lay);
        /* any operation not chaning the number of lines at the vertex, removes the vertex again */
        for (Map.Entry<Vertex, String> tmp_line : Vertex.vertexPresets) {

          VertexLabel tl = new VertexLabel(tmp_line.getKey());
          tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

          subMenu.add(tl);
          tl.setHorizontalTextPosition(JMenuItem.RIGHT);
          tl.addActionListener(e -> {
            Frame.this.saveForUndo();
            Vertex vt = new Vertex(tmp_line.getKey());
            vt.origin = intersect;
            drawPane.fObjects.add(new FloatingObject(vt, intersect));

            drawPane.selectType = 1;
            if (Frame.this.eframe != null) {
              Frame.this.eframe.editPane.upEditPane();
              Frame.this.eframe.repaint();
            }
            if (beframe != null) {
              beframe.update();
            }
            PopMenu.this.setVisible(false);
          });
        }
        for (FloatingImage tmp_line : FloatingImage.fIPresets) {

          ImageLabel tl = new ImageLabel(tmp_line);
          tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
          tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

          subMenu.add(tl);
          tl.setHorizontalTextPosition(JMenuItem.RIGHT);
          tl.addActionListener(e -> {
            Frame.this.saveForUndo();
            FloatingImage vt = new FloatingImage(tmp_line);
            vt.center = intersect;
            drawPane.fObjects.add(new FloatingObject(vt, intersect));

            drawPane.selectType = 1;
            if (Frame.this.eframe != null) {
              Frame.this.eframe.editPane.upEditPane();
              Frame.this.eframe.repaint();
            }
            if (beframe != null) {
              beframe.update();
            }
            PopMenu.this.setVisible(false);
          });
        }
      }

      JMenuItem shape = new JMenuItem("Add new shape");
      shape.addActionListener(e -> {
        (new NewShape()).newShape(Frame.this, new Point2D(intersect.x, intersect.y));
        Frame.this.requestFocus();
        PopMenu.this.setVisible(false);
      });
      this.add(shape);
    }
    /**
     * when a vertex was hit, this lets one change the vertex config to a
     * preset
     * @param vt a reference to the vertex that was clicked
     * @param index the index of the vertex
     */
    void vertexClicked(Vertex vt, int index) {

      JMenu subMenu;

      line_from_here(vt.origin);

      /*
      this.add(subMenu = new JMenu("line from here"));

      for (LineConfig tmp_line : LineConfig.presets) {
        if (tmp_line.lineType.equals(LineType.GRAB)) continue;

        TileLabel tl = new TileLabel(tmp_line);
        tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

        subMenu.add(tl);
        tl.setHorizontalTextPosition(JMenuItem.RIGHT);
        tl.addMouseListener(new MouseListener() {
          @Override
          public void mouseClicked(MouseEvent e) {

          }

          @Override
          public void mousePressed(MouseEvent e) {
            PopMenu.this.setVisible(false);

            Frame.this.saveForUndo();

            drawPane.lines.add(new FeynLine(new Line(vt.origin.x, vt.origin.y, e.getXOnScreen() - Frame.this.getX(), e.getYOnScreen() - Frame.this.getY()), new LineConfig(tmp_line)));
            drawPane.checkForVertex(vt.origin);
            drawPane.mouseDown = true;
            drawPane.selectType = 0;
            draggedMode = -1;
            if (Frame.this.eframe != null) {
              Frame.this.eframe.editPane.upEditPane();
              Frame.this.eframe.repaint();
            }

            if (beframe != null) {
              beframe.update();
            }
          }

          @Override
          public void mouseReleased(MouseEvent e) {

          }

          @Override
          public void mouseEntered(MouseEvent e) {

          }

          @Override
          public void mouseExited(MouseEvent e) {

          }
        });
      }

       */

      this.add(subMenu = new JMenu("change vertex"));

      int rows = (int) drawPane.getHeight() / (TileSelect.uiHeight);
      int columns = (int) Math.max(Math.ceil((float) (Vertex.vertexPresets.size()) / (float) rows), 1);
      rows = (int) Math.ceil((float) (Vertex.vertexPresets.size())/(float) columns);
      GridLayout lay = new GridLayout(rows, columns);
      subMenu.getPopupMenu().setLayout(lay);

      for (Map.Entry<Vertex, String> tmp_line : Vertex.vertexPresets) {

        VertexLabel tl = new VertexLabel(tmp_line.getKey());
        tl.setMinimumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setPreferredSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));
        tl.setMaximumSize(new Dimension(TileSelect.uiHeight, TileSelect.uiHeight));

        subMenu.add(tl);
        tl.setHorizontalTextPosition(JMenuItem.RIGHT);
        tl.addActionListener(e -> {
          Frame.this.saveForUndo();

          Vertex new_vt = new Vertex(tmp_line.getKey());
          new_vt.origin = vt.origin;
          boolean autoremove = drawPane.vertices.get(index).autoremove;
          drawPane.vertices.remove(index);
          new_vt.autoremove = autoremove;
          drawPane.vertices.add(new_vt);

          drawPane.selectType = 1;
          if (Frame.this.eframe != null) {
            Frame.this.eframe.editPane.upEditPane();
            Frame.this.eframe.repaint();
          }
          if (beframe != null) {
            beframe.update();
          }
          PopMenu.this.setVisible(false);
        });
      }


    }
    /**
     * checks where the right mouse was clicked and adds the options to
     * the popup menu accordingly
     * @param e the mouse event which espacially holds the position where
     * the mouse was clicked
     */
    void configureOptions(MouseEvent e) {
      Point2D mPosPane = new Point2D(currX / drawPane.scale - drawPane.zeroPoint.x, currY / drawPane.scale - drawPane.zeroPoint.y);
      try {
        Map.Entry<Point2D, Integer> intersectPair = clipPoint(new Point2D(mPosPane.x, mPosPane.y), null, -1, false, 5);
        int index = intersectPair.getValue();
        if (index > 0) {
          boolean found = false;
          index--;
          Point2D intersect = intersectPair.getKey();
          FeynLine fl = drawPane.lines.get(index);

          if (Math.abs(fl.height) < .1) {
            double lenFl = Math.sqrt((fl.getStart().x - fl.getEnd().x) * (fl.getStart().x - fl.getEnd().x) + (fl.getStart().y - fl.getEnd().y) * (fl.getStart().y - fl.getEnd().y));
            double disToStart = Math.sqrt((fl.getStart().x - intersect.x) * (fl.getStart().x - intersect.x) + (fl.getStart().y - intersect.y) * (fl.getStart().y - intersect.y));
            if (lenFl > 4 * disToStart) {
              if (!fl.fixStart) {
                intersect = fl.getStart();
                found = true;
              }
            } else if (lenFl * 3 < 4 * disToStart) {
              if (!fl.fixEnd) {
                found = true;
                intersect = fl.getEnd();
              }
            } else {
              if (!fl.fixStart && !fl.fixEnd) {
                found = true;
              }
            }
          } else {

            double archEndGes = Math.atan2(fl.getEnd().y - fl.center.y, fl.getEnd().x - fl.center.x);
            double archStartGes = Math.atan2(fl.getStart().y - fl.center.y, fl.getStart().x - fl.center.x);
            double deltaArchGes = (-archStartGes + archEndGes) * -Math.signum(fl.height);

            while (deltaArchGes > 2 * Math.PI) {
              deltaArchGes -= 2 * Math.PI;
            }
            while (deltaArchGes <= 0) {
              deltaArchGes += 2 * Math.PI;
            }

            double archEndTeil = Math.atan2(intersect.y - fl.center.y, intersect.x - fl.center.x);
            double archStartTeil = Math.atan2(fl.getStart().y - fl.center.y, fl.getStart().x - fl.center.x);
            double deltaArchTeil = (-archStartTeil + archEndTeil) * -Math.signum(fl.height);

            while (deltaArchTeil >= 2 * Math.PI) {
              deltaArchTeil -= 2 * Math.PI;
            }
            while (deltaArchTeil < 0) {
              deltaArchTeil += 2 * Math.PI;
            }


            if (deltaArchGes / 4 > deltaArchTeil) {
              if (!fl.fixStart) {
                found = true;
                intersect = fl.getStart();
              }
            } else if (deltaArchGes * 3 / 4 < deltaArchTeil) {
              if (!fl.fixEnd) {
                found = true;
                intersect = fl.getEnd();
              }
            } else {
              if (!fl.fixStart && !fl.fixEnd) {
                found = true;
              }
            }
          }

          if (found)
            lineClicked(fl, intersect, index);

        } else if (index < 0) {
          index = -index - 1;
          Vertex vt = new Vertex(drawPane.vertices.get(index));
          vertexClicked(vt, index);
        } else {
          int tmp_index = 0;
          for (FloatingObject fO : drawPane.fObjects) {
            if (fO.v) {
              if (fO.vertex.origin.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10)
                break;
            } else if (fO.floatingImage.center.distance(new Point2D(mPosPane.x, mPosPane.y)) < 10)
              break;

            tmp_index++;
          }
          if (tmp_index != drawPane.fObjects.size())
            fObjectClicked(tmp_index);
          else
            nothingClicked(new Point2D(mPosPane.x, mPosPane.y));
        }
      } catch (Exception ignored) {
      }
    }

  }

  public static void setLaTeXFontSize(int fontSize) {
    ui.Frame.LaTeXFontSize = fontSize;
    ui.Pane.latexSize = fontSize;
    game.FeynLine.latexSize = fontSize;
    game.Momentum.latexSize = fontSize;
    game.Vertex.latexSize = fontSize;
    game.Shaped.latexSize = fontSize;
    game.FloatingImage.latexSize = fontSize;
    // constant font size in tiles
    // ui.TileLabel.latexSize = fontSize;
    // ui.VertexLabel.latexSize = fontSize;
    // ui.ImageLabel.latexSize = fontSize;
  }
  
  /**
   * This is a prefix for all logger names used by FeynGame. Usually this would be the common package
   * prefix of all FeynGame packages, but since FeynGame has a flat package structure where every package
   * ahs a different name and not prefix, the only common parent logger would be the root logger.
   * 
   * We cannot configure the root logger to log at level FINE ore lower in debug mode because
   * the Java packages internally also use loggers for debugging that would spam the log file
   * with unrelated messages.
   * 
   * The solution is to artificially prefix all loggers with "FeynGame." and configure
   * the FeynGame logger instead of the root logger. The configuration cannot be done at construction time
   * of the loggers, because they are created in static initializers, but certaing parameters such as
   * the debug mode have to be set later.
   */
  private static final String PARENT_LOGGER_NAME = "FeynGame";
  //retain a strong reference to this one
  private static final Logger PARENT_LOGGER = Logger.getLogger(PARENT_LOGGER_NAME);
  
  /**
   * Configures all loggers created by {@link #getLogger(String)} through the shared parent logger.
   * @param debug If true, the log level is set to FINER and exceptions include their stack traces.
   */
  public static void configureParentLogger(boolean debug, Level consoleLevel, Level fileLevel) {
    PARENT_LOGGER.setUseParentHandlers(false);
    for(Handler h : PARENT_LOGGER.getHandlers()) PARENT_LOGGER.removeHandler(h);
    
    String logFileSt = "FeynGame.log";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      logFileSt = (System.getProperty("user.home") + "/Library/Preferences/FeynGame/FeynGame.log");
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      logFileSt = (System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\FeynGame.log");
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      logFileSt = (System.getProperty("user.home") + "/.config/FeynGame/FeynGame.log");
    }
    
    try {
      File logFile = new File(logFileSt);
      logFile.getParentFile().mkdirs();
      FileHandler fileHandler = new FileHandler(logFile.getAbsolutePath());
      fileHandler.setFormatter(new SimpleFormatter());
      ConsoleHandler consoleHandler = new ConsoleHandler();
      Formatter plainFormatter = new Formatter() {
        @Override
        public String format(LogRecord record) {
          Throwable ex = record.getThrown();
          String logger = record.getLoggerName();
          Level level = record.getLevel();
          String message = record.getMessage();
          StringBuilder sb = new StringBuilder();
          sb.append("[").append(level.getName()).append("] ");

          if(debug) {
            sb.append("[").append(logger).append("] ");
          }
          
          if(ex == null) {
            sb.append(message);
          } else {
            if(message == null || message.isEmpty()) {
              sb.append(ex.getMessage());
            } else {
              sb.append(message).append(": ").append(ex.getMessage());
            }
            //log a stack trace for severe errors
            if(debug) {
              StringWriter sw = new StringWriter();
              ex.printStackTrace(new PrintWriter(sw));
              sb.append("\n");
              sb.append(sw.toString());
            }
          }

          return sb.append("\n").toString();
        }
      };
      consoleHandler.setFormatter(plainFormatter);
      PARENT_LOGGER.addHandler(fileHandler);
      PARENT_LOGGER.addHandler(consoleHandler);
      PARENT_LOGGER.setLevel(Level.ALL);
      if (debug) {
        consoleHandler.setLevel(Level.FINER);
        fileHandler.setLevel(Level.FINER);
      } else {
        consoleHandler.setLevel(Level.INFO);
        fileHandler.setLevel(Level.INFO);
      }
      
      if(consoleLevel != null) {
        consoleHandler.setLevel(consoleLevel);
      }
      
      if(fileLevel != null) {
        fileHandler.setLevel(fileLevel);
      }
    } catch (IOException e) {
      logger.log(Level.SEVERE, "Failed to configure parent logger", e);
      //log to syserr as well since it might not show up on the logger if we failed to set it up
      System.err.println("Failed to configure parent logger: " + e.getMessage());
    }
  }

  public static Logger getLogger(Class<?> clazz) {
    return getLogger(clazz.getName());
  }
  
  public static Logger getLogger(String name) {
    Logger l = Logger.getLogger(PARENT_LOGGER_NAME + "." + name);
    l.setUseParentHandlers(true);
    return l;
  }
}

class DelAction extends AbstractAction {

  private Frame frame;

  public DelAction(String text, Frame frame) {
    super(text);
    this.frame = frame;
  }

  public void actionPerformedMethod() {
    Pane drawPane = frame.getDrawPane();
    if (Frame.frame.tiles.selectedIndex != -1) { // delete tile
      Frame.frame.tiles.deleteFromConfig(Frame.frame.tiles.selectedIndex);
    } else if (drawPane.lines.size() > 0 && drawPane.selectType == 0) {
      if (!drawPane.lines.get(drawPane.lines.size() - 1).isFixed()) {
        frame.saveForUndo();
        Point2D oldStart = drawPane.lines.get(drawPane.lines.size() - 1).getStart();
        Point2D oldEnd = drawPane.lines.get(drawPane.lines.size() - 1).getEnd();
        frame.removeLine(drawPane.lines.size() - 1);
        drawPane.checkForVertex(new Point2D((int) Math.round(oldStart.x), (int) Math.round(oldStart.y)));
        drawPane.checkForVertex(new Point2D((int) Math.round(oldEnd.x), (int) Math.round(oldEnd.y)));
        frame.repaint();
      }
    } else if (drawPane.fObjects.size() > 0 && drawPane.selectType == 2) {
      frame.saveForUndo();
      drawPane.fObjects.remove(drawPane.fObjects.size() - 1);
      frame.repaint();
    } else if (drawPane.selectType == 3 && drawPane.shapes.size() > 0) {
      frame.saveForUndo();
      drawPane.shapes.remove(drawPane.shapes.size() - 1);
      ArrayList<Point2D> pointList = new ArrayList<>();
      for (Vertex v : drawPane.vertices) {
        pointList.add(v.origin);
      }
      for (Point2D point : pointList) {
        drawPane.checkForVertex(point);
      }
      frame.repaint();
    } else if (drawPane.selectType == 1 && drawPane.vertices.size() > 0){
      Vertex v = drawPane.vertices.get(drawPane.vertices.size() - 1);
      if (!v.autoremove) {
        Point2D point = v.origin;
        v.autoremove = true;
        frame.combineLines(point);
        drawPane.checkForVertex(point);
      }
    }
    if (frame.eframe != null) {
      frame.eframe.editPane.upEditPane();
      frame.eframe.repaint();
    }
    if (frame.beframe != null) {
      frame.beframe.update();
    }
  }

  public void actionPerformed(ActionEvent e) {
    actionPerformedMethod();
  }

}
