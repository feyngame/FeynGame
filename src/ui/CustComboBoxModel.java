//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package ui;

import javax.swing.*;
import java.util.ArrayList;

/**
 * A costum ComboBoxModel for Strings. Used in EditFrame for linetypes, shapes
 * of vertices and the pattern selection
 *
 * @author Sven Yannick Klein
 */
class CustComboBoxModel<T> extends AbstractListModel<T> implements ComboBoxModel<T> {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 12L;
  ArrayList<T> items;

  String selection = "";

  public CustComboBoxModel(java.util.List<T> items) {
    this.items = new ArrayList<>(items);
    for (int i = 0; i < items.size(); i++) {
      this.items.set(i, items.get(i));
    }
  }

  public T getElementAt(int index) {
    return items.get(index);
  }

  public int getSize() {
    return items.size();
  }

  public Object getSelectedItem() {
    return selection;
  }

  public void setSelectedItem(Object anItem) {
    selection = (String) anItem;
    selection = selection.substring(0, 1).toUpperCase() + selection.substring(1); //Capitalizes the first letter
  }
}
