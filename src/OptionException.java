
public class OptionException extends Exception {
  private static final long serialVersionUID = 1L;

  public static final int EC_ERROR = -1;
  public static final int EC_UNKNOWN_OPTION = -2;
  public static final int EC_WRONG_OPTION_VALUE_COUNT = -3;
  public static final int EC_DUPLICATE_OPTION = -4;
  public static final int EC_INVALID_OPTION_VALUE = -5;
  public static final int EC_INCOMPATIBLE_OPTIONS = -6;
  public static final int EC_FILE_NOT_FOUND = -7;
  public static final int EC_FILE_NOT_ACCESSIBLE = -8;
  public static final int EC_INVALID_FILE_CONTENT = -9;
  public static final int EC_UNKNOWN_EXPORT_FORMAT = -10;
  public static final int EC_INVALID_DIAGRAM = -11;
  public static final int EC_MISSING_REQIRED_OPTION = -12;
  public static final int EC_EXPORT_ERROR = -13;
  public static final int EC_HEADLESS = -14;
  
  private final int exitCode;
  
  public OptionException(String message, Throwable cause, int exitCode) {
    super(message, cause);
    if(exitCode == 0) throw new IllegalArgumentException("Cannot use exit code 0 (success) with an error");
    this.exitCode = exitCode;
  }

  public OptionException(String message, int exitCode) {
    super(message);
    if(exitCode == 0) throw new IllegalArgumentException("Cannot use exit code 0 (success) with an error");
    this.exitCode = exitCode;
  }
  
  public int getExitCode() {
    return exitCode;
  }
}