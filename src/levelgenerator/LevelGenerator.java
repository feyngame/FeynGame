//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019  Robert Harlander, Sven Yannick Klein and Maximilian Lipp
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

import javax.swing.*;
import javax.swing.UIManager.*;
import java.util.*;
import javax.swing.filechooser.FileFilter;
import java.io.*;
import java.awt.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import javax.swing.event.ChangeEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.nio.file.Files;
import javax.swing.SwingWorker.*;
import java.math.BigInteger;

/**
 * This is the initial frame of the level generator.
 *
 * @author Sven Yannick Klein
 */

public class LevelGenerator implements Runnable, WindowListener {
  /**
   * wehter or not to run in debug mode
   */
  private static boolean DEBUG = false;
  /**
   * the frame
   */
  private JFrame frame;
  /**
   * the path to the currently used config file
   */
  private String configPath;
  /**
   * the path to the qgraf executable
   */
  private String qgrafPath;
  /**
   * the maximal number of loops that are allowed
   */
  private int maxLoops;
  /**
   * the maximum number of the initial states
   */
  private int maxStatesIn = 2;
  /**
   * the minimum number of the initial states
   */
  private int minStatesIn = 1;
  /**
   * the maximum number of the final states
   */
  private int maxStatesFin = 2;
  /**
   * the minimum number of the final states
   */
  private int minStatesFin = 1;
  /**
   * the output file
   */
  private String outputPath;
  /**
   * the number of challenges to be generated
   */
  private int numberChallenges;
  /**
   * the qgraf executable
   */
  private File qgraf;
  /**
   * the qgraf model file
   */
  private File model;
  /**
   * the qgraf.dat file
   */
  private File qdat;
  /**
   * the file for the qgraf .sty style file
   */
  private File style;
  /**
   * the buffered Writer for the output
   */
  private BufferedWriter bwLevel;
  /**
   * the progress bar
   */
  private JProgressBar proBar;
  /**
   * the thread that writes the level file
   */
  private Task task;
  /**
   * the frame where the progress is displayed
   */
  private JFrame proFrame;
  /**
   * wether or not to delete files after generating has finished
   */
  private boolean delete = true;
  /**
   * main method: first method to be called
   * @param args command line arguments
   */
  public static void main (String[] args) {
    SwingUtilities.invokeLater(new LevelGenerator(args));
  }
  /**
   * constructor: sets the config path to the first argument given if given
   * @param args command line arguments
   */
  public LevelGenerator(String args[]) {
    try {
      this.configPath = args[0];
    } catch (Exception ex) {}
  }
  /**
   * runs the application: sets up the frame and all that
   */
  public void run() {
    frame = new JFrame("FeynGame Level Generator");

    frame.addWindowListener(this);

    this.getPreferences();

    frame.setLocationRelativeTo(null);
    // frame.setType(Window.Type.POPUP);

    JFrame newFrame = new JFrame("FeynGame");
    String os = System.getProperty("os.name");
    boolean macOs = os.toLowerCase().startsWith("mac");

    try {
      if (!macOs)
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      if (macOs) {
        try {
          for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
              UIManager.setLookAndFeel(info.getClassName());
              break;
            }
          }
        } catch (Exception e) {
          UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        }
      }
    } catch(Exception e) {
      e.printStackTrace();
    }

    JFrame.setDefaultLookAndFeelDecorated(true);
    frame.getContentPane().setBackground(UIManager.getColor ( "Panel.background" ));

    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints c = new GridBagConstraints();
    frame.setLayout(gridbag);
    c.weightx = 0.01;
    c.weighty = 1;
    c.fill = (GridBagConstraints.HORIZONTAL);
    c.gridy = 0;
    c.gridx = 0;
    c.insets = new Insets(0,0,0,0);

    JLabel selConfigFileLabel = new JLabel(" Model file: ");
    selConfigFileLabel.setHorizontalAlignment(JLabel.LEFT);
    frame.add(selConfigFileLabel, c);

    c.gridx ++;
    c.weightx = 1;
    c.gridwidth = 2;
    JTextField selConfigFileText = new JTextField();
    if (this.configPath != null)
      selConfigFileText.setText(this.configPath);
    selConfigFileText.setHorizontalAlignment(JTextField.LEADING);
    selConfigFileText.setColumns(1);
    selConfigFileText.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {

      }

      @Override
      public void focusLost(FocusEvent e) {
        for (ActionListener al : selConfigFileText.getActionListeners())
          al.actionPerformed(null);
      }
    });
    selConfigFileText.addActionListener(e -> {
      configPath = selConfigFileText.getText();
    });
    frame.add(selConfigFileText, c);
    c.weightx = 0.01;
    c.gridx += 2;
    c.gridwidth = 1;
    JButton selConfigFile = new JButton("Select File");
    selConfigFile.addActionListener(e -> {
      final JFileChooser fc = new JFileChooser();

      fc.setMultiSelectionEnabled(false);
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setDialogType(JFileChooser.OPEN_DIALOG);
      fc.setDialogTitle("Load Model file");

      if (configPath == null) {
        fc.setCurrentDirectory(new File(System.getProperty("user.home")));
      } else {
        fc.setCurrentDirectory(new File(configPath));
      }

      fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

      FileFilter filter = new FileNameExtensionFilter("Model files (*.model)","model");
      fc.addChoosableFileFilter(filter);
      fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());

      int returnVal;
      returnVal = fc.showSaveDialog(frame);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fc.getSelectedFile();
        String withExtension = fc.getSelectedFile().getAbsolutePath();
        if( !withExtension.toLowerCase().endsWith( ".model" ) )
          withExtension += ".model";

        configPath = withExtension;
        selConfigFileText.setText(configPath);

      }
    });
    frame.add(selConfigFile, c);

    JPanel numberPanel = new JPanel();
    GridBagLayout gb = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();
    numberPanel.setLayout(gb);
    gbc.weightx = 1;
    gbc.gridy = 0;
    gbc.weighty = 1;
    gbc.gridx = 0;
    gbc.fill = (GridBagConstraints.HORIZONTAL);
    gbc.insets = new Insets(0,0,0,0);


    JLabel maxNumInStatesLabel = new JLabel(" Maximum number of particles in the initial state: ");
    numberPanel.add(maxNumInStatesLabel, gbc);
    gbc.gridx ++;
    SpinnerModel maxNumInStatesModel = new SpinnerNumberModel((double) 1, (double) 1, (double) 10000, (double) 1);
    if (maxStatesIn != 0) {
      maxNumInStatesModel.setValue((double) maxStatesIn);
    }
    JSpinner maxNumInStatesSpinner = new JSpinner(maxNumInStatesModel);
    maxNumInStatesSpinner.addChangeListener((ChangeEvent e) -> {
      try {
        maxNumInStatesSpinner.commitEdit();
      } catch (java.text.ParseException ex) {}
      maxStatesIn = (int) Float.parseFloat(maxNumInStatesSpinner.getValue().toString());
    });
    numberPanel.add(maxNumInStatesSpinner, gbc);

    gbc.gridx ++;
    JLabel minNumInStatesLabel = new JLabel(" Minimum number of particles in the initial state: ");
    numberPanel.add(minNumInStatesLabel, gbc);
    gbc.gridx ++;
    SpinnerModel minNumInStatesModel = new SpinnerNumberModel((double) 1, (double) 1, (double) 10000, (double) 1);
    if (minStatesIn != 0) {
      minNumInStatesModel.setValue((double) minStatesIn);
    }
    JSpinner minNumInStatesSpinner = new JSpinner(minNumInStatesModel);
    minNumInStatesSpinner.addChangeListener((ChangeEvent e) -> {
      try {
        minNumInStatesSpinner.commitEdit();
      } catch (java.text.ParseException ex) {}
      minStatesIn = (int) Float.parseFloat(minNumInStatesSpinner.getValue().toString());
    });
    numberPanel.add(minNumInStatesSpinner, gbc);

    gbc.gridx = 0;
    gbc.gridy ++;

    JLabel maxNumFinStatesLabel = new JLabel(" Maximum number of particles in the final state: ");
    numberPanel.add(maxNumFinStatesLabel, gbc);
    gbc.gridx ++;
    SpinnerModel maxNumFinStatesModel = new SpinnerNumberModel((double) 1, (double) 1, (double) 10000, (double) 1);
    if (maxStatesFin != 0) {
      maxNumFinStatesModel.setValue((double) maxStatesFin);
    }
    JSpinner maxNumFinStatesSpinner = new JSpinner(maxNumFinStatesModel);
    maxNumFinStatesSpinner.addChangeListener((ChangeEvent e) -> {
      try {
        maxNumFinStatesSpinner.commitEdit();
      } catch (java.text.ParseException ex) {}
      maxStatesFin = (int) Float.parseFloat(maxNumFinStatesSpinner.getValue().toString());
    });
    numberPanel.add(maxNumFinStatesSpinner, gbc);

    gbc.gridx ++;
    JLabel minNumFinStatesLabel = new JLabel(" Minimum number of particles in the final state: ");
    numberPanel.add(minNumFinStatesLabel, gbc);
    gbc.gridx ++;
    SpinnerModel minNumFinStatesModel = new SpinnerNumberModel((double) 1, (double) 1, (double) 10000, (double) 1);
    if (minStatesFin != 0) {
      minNumFinStatesModel.setValue((double) minStatesFin);
    }
    JSpinner minNumFinStatesSpinner = new JSpinner(minNumFinStatesModel);
    minNumFinStatesSpinner.addChangeListener((ChangeEvent e) -> {
      try {
        minNumFinStatesSpinner.commitEdit();
      } catch (java.text.ParseException ex) {}
      minStatesFin = (int) Float.parseFloat(minNumFinStatesSpinner.getValue().toString());
    });
    numberPanel.add(minNumFinStatesSpinner, gbc);

    gbc.gridy ++;
    gbc.gridx = 0;
    gbc.gridwidth = 2;
    JLabel maxNumLoopsLabel = new JLabel(" Maximum number of loops: ");
    numberPanel.add(maxNumLoopsLabel, gbc);
    gbc.gridx += 2;
    SpinnerModel maxNumLoopsModel = new SpinnerNumberModel((double) 0, (double) 0, (double) 10000, (double) 1);
    maxNumLoopsModel.setValue((double) this.maxLoops);
    JSpinner maxNumLoopsSpinner = new JSpinner(maxNumLoopsModel);
    maxNumLoopsSpinner.addChangeListener((ChangeEvent e) -> {
      try {
        maxNumLoopsSpinner.commitEdit();
      } catch (java.text.ParseException ex) {}
      maxLoops = (int) Float.parseFloat(maxNumLoopsSpinner.getValue().toString());
    });
    numberPanel.add(maxNumLoopsSpinner, gbc);

    c.gridx = 0;
    c.gridy ++;
    c.gridwidth = 4;
    frame.add(numberPanel, c);

    c.gridx = 0;
    c.gridy ++;

    c.gridwidth = 2;
    JLabel numberChallengesLabel = new JLabel(" The number of challenges to be generated: ");
    frame.add(numberChallengesLabel, c);
    c.gridx += 2;

    SpinnerModel numberChallengesModel = new SpinnerNumberModel((double) 0, (double) 0, (double) 10000, (double) 1);
    numberChallengesModel.setValue((double) this.numberChallenges);
    JSpinner numberChallengesSpinner = new JSpinner(numberChallengesModel);
    numberChallengesSpinner.addChangeListener((ChangeEvent e) -> {
      try {
        numberChallengesSpinner.commitEdit();
      } catch (java.text.ParseException ex) {}
      numberChallenges = (int) Float.parseFloat(numberChallengesSpinner.getValue().toString());
    });
    frame.add(numberChallengesSpinner, c);

    c.weightx = 0.01;
    c.weighty = 1;
    c.gridy ++;
    c.gridwidth = 1;
    c.gridx = 0;
    c.insets = new Insets(0,0,0,0);

    JLabel selQGrafFileLabel = new JLabel(" QGraf Executable: ");
    frame.add(selQGrafFileLabel, c);

    c.gridx ++;
    c.fill = (GridBagConstraints.HORIZONTAL);
    c.weightx = 1;
    c.gridwidth = 2;
    JTextField selQGrafFileText = new JTextField();
    if (this.qgrafPath != null)
      selQGrafFileText.setText(this.qgrafPath);
    selQGrafFileText.setHorizontalAlignment(JTextField.LEADING);
    selQGrafFileText.setColumns(1);
    selQGrafFileText.addFocusListener(new FocusListener() {
          @Override
          public void focusGained(FocusEvent e) {

          }

          @Override
          public void focusLost(FocusEvent e) {
            for (ActionListener al : selQGrafFileText.getActionListeners())
            al.actionPerformed(null);
          }
      });
    selQGrafFileText.addActionListener(e -> {
      qgrafPath = selQGrafFileText.getText();
    });
    frame.add(selQGrafFileText, c);

    c.weightx = 0.01;
    c.gridx += 2;
    c.gridwidth = 1;
    JButton selQGrafFile = new JButton("Select File");
    selQGrafFile.addActionListener(e -> {
      final JFileChooser fc = new JFileChooser();

      fc.setMultiSelectionEnabled(false);
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setDialogType(JFileChooser.OPEN_DIALOG);
      fc.setDialogTitle("Select QGraf executable");

      if (qgrafPath == null) {
        fc.setCurrentDirectory(new File(System.getProperty("user.home")));
      } else {
        fc.setCurrentDirectory(new File(qgrafPath));
      }

      int returnVal;
      returnVal = fc.showSaveDialog(frame);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fc.getSelectedFile();

        qgrafPath = file.getAbsolutePath();
        selQGrafFileText.setText(qgrafPath);

      }
    });
    frame.add(selQGrafFile, c);

    c.weightx = 0.01;
    c.weighty = 1;
    c.gridy ++;
    c.gridx = 0;
    c.insets = new Insets(0,0,0,0);

    JLabel selOutputFileLabel = new JLabel(" Output File: ");
    frame.add(selOutputFileLabel, c);

    c.gridx ++;
    c.fill = (GridBagConstraints.HORIZONTAL);
    c.weightx = 1;
    c.gridwidth = 2;
    JTextField selOutputFileText = new JTextField();
    if (this.outputPath != null)
      selOutputFileText.setText(this.outputPath);
    selOutputFileText.setHorizontalAlignment(JTextField.LEADING);
    selOutputFileText.setColumns(1);
    selOutputFileText.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {

      }

      @Override
      public void focusLost(FocusEvent e) {
        for (ActionListener al : selOutputFileText.getActionListeners())
          al.actionPerformed(null);
      }
    });
    selOutputFileText.addActionListener(e -> {
      outputPath = selOutputFileText.getText();
    });
    frame.add(selOutputFileText, c);
    c.weightx = 0.01;
    c.gridx += 2;
    c.gridwidth = 1;
    JButton selOutputFile = new JButton("Select File");
    selOutputFile.addActionListener(e -> {
      final JFileChooser fc = new JFileChooser();

      fc.setMultiSelectionEnabled(false);
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setDialogType(JFileChooser.OPEN_DIALOG);
      fc.setDialogTitle("Load Output file");

      if (outputPath == null) {
        fc.setCurrentDirectory(new File(System.getProperty("user.home")));
      } else {
        fc.setCurrentDirectory(new File(outputPath));
      }

      fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());

      FileFilter filter = new FileNameExtensionFilter("InFin Level files (*.if)","if");
      fc.addChoosableFileFilter(filter);
      fc.addChoosableFileFilter(fc.getAcceptAllFileFilter());

      int returnVal;
      returnVal = fc.showSaveDialog(frame);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = fc.getSelectedFile();

        String withExtension = fc.getSelectedFile().getAbsolutePath();
        if( !withExtension.toLowerCase().endsWith( ".if" ) )
          withExtension += ".if";

        file = new File(withExtension);
        outputPath = file.getAbsolutePath();
        selOutputFileText.setText(outputPath);
      }
    });
    frame.add(selOutputFile, c);

    c.gridy ++;
    c.gridx = 0;
    c.gridwidth = 4;
    JPanel buttonPanel = new JPanel(new GridLayout(1, 3));
    JButton generate = new JButton("Generate InFin Level");
    generate.addActionListener(e -> {
      proFrame = new JFrame("Generating Level File ...");
      proFrame.setLayout(new BorderLayout());
      proBar = new JProgressBar(0, 100);
      proBar.setValue(0);
      proBar.setStringPainted(true);
      proFrame.setLocationRelativeTo(null);
      proFrame.add(proBar, BorderLayout.NORTH);

      JButton cancelButton = new JButton("Cancel");
      cancelButton.addActionListener(ae -> {
        while (task != null && task.getState() != SwingWorker.StateValue.DONE)
          task.cancel(true);
        try {
          bwLevel.flush();
          bwLevel.close();
        } catch (IOException ioex) {
          JOptionPane.showMessageDialog(frame, "The level file could not be closed properly: \n" + ioex + " \n Progress might be lost!", "Error", JOptionPane.ERROR_MESSAGE);
          ioex.printStackTrace();
        }
        if (delete) {
          File qgrafFile = new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "qgraf");

          boolean deleted = false;
          while (qgrafFile.exists() && !deleted) {
            deleted = qgrafFile.delete();
            if (LevelGenerator.DEBUG) System.out.println("Deleted qgraf: " + deleted);
          }

          deleted = false;
          while (model != null && model.exists() && !deleted) {
            deleted = model.delete();
            if (LevelGenerator.DEBUG) System.out.println("Deleted model: " + deleted);
          }

          deleted = false;
          while (qdat != null && qdat.exists() && !deleted) {
            deleted = qdat.delete();
            if (LevelGenerator.DEBUG) System.out.println("Deleted qgraf.dat: " + deleted);
          }

          deleted = false;
          while (style != null && style.exists() && !deleted) {
            deleted = style.delete();
            if (LevelGenerator.DEBUG) System.out.println("Deleted style.sty: " + deleted);
          }
        }
      });
      proFrame.add(cancelButton, BorderLayout.SOUTH);
      proFrame.pack();
      proFrame.setResizable(false);
      proFrame.setAlwaysOnTop(true);
      proFrame.setVisible(true);
      task = new Task();
      task.execute();
    });
    buttonPanel.add(generate);

    JButton cancel = new JButton("Exit");
    cancel.addActionListener(e -> {
      this.setPreferences();
      System.exit(0);
    });
    buttonPanel.add(cancel);

    JButton help = new JButton("Help");
    help.addActionListener(e -> {
      new Help();
    });
    buttonPanel.add(help);

    frame.add(buttonPanel, c);

    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setResizable(false);
    frame.setVisible(true);
  }
  /**
   * subclass to put the actual work of writing the level file to another
   * thread
   */
  class Task extends SwingWorker<Void, Void> {
    /*
     * Main task. Executed in background thread.
     */
    @Override
    public Void doInBackground() {
      generate();
      return null;
    }
    /*
     * Executed in event dispatching thread
     */
    @Override
    public void done() {
      if (LevelGenerator.DEBUG) System.out.println("Generation of level file is complete!");
      if (delete) {
        // new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "qgraf.dat").delete();
        // new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "qout").delete();
        // new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "model.mod").delete();
        // new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "qgraf.exe").delete();
        // new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "style.sty").delete();

        File toDelete;

        toDelete = new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "qgraf.dat");
        boolean deleted = false;
        while (toDelete.exists() && !deleted) {
          deleted = toDelete.delete();
          if (LevelGenerator.DEBUG) System.out.println("Deleted qgraf.dat: " + deleted);
        }

        toDelete = new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "qout");
        deleted = false;
        while (toDelete != null && toDelete.exists() && !deleted) {
          deleted = toDelete.delete();
          if (LevelGenerator.DEBUG) System.out.println("Deleted qout: " + deleted);
        }

        toDelete = new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "model.mod");
        deleted = false;
        while (toDelete != null && qdat.exists() && !deleted) {
          deleted = qdat.delete();
          if (LevelGenerator.DEBUG) System.out.println("Deleted model.mod: " + deleted);
        }

        toDelete = new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "qgraf.exe");
        deleted = false;
        while (toDelete != null && toDelete.exists() && !deleted) {
          deleted = toDelete.delete();
          if (LevelGenerator.DEBUG) System.out.println("Deleted qgraf.exe: " + deleted);
        }

        toDelete = new File(new File(outputPath).getParent() + System.getProperty("file.separator") + "style.sty");
        deleted = false;
        while (toDelete != null && toDelete.exists() && !deleted) {
          deleted = toDelete.delete();
          if (LevelGenerator.DEBUG) System.out.println("Deleted style.sty: " + deleted);
        }
      }
      proFrame.setVisible(false);
    }
  }
  /**
   * gives i!
   * @param i i
   * @return i!
   */
  private static BigInteger factorial(BigInteger i) {
    BigInteger ret = new BigInteger("1");
    for (BigInteger index = new BigInteger("1"); !index.equals(i.add(new BigInteger("1"))); index = index.add(new BigInteger("1")))
      ret = ret.multiply(index);
    return ret;
  }
  /**
   * gives // b \\ (b+a-1 over a)
   *       \\ a //
   * @param b b
   * @param a a
   * @return a^b
   */
  private static BigInteger over(BigInteger b, BigInteger a) {
    BigInteger ret = LevelGenerator.factorial(a.add(b).add(new BigInteger("-1")));
    ret = ret.divide(LevelGenerator.factorial(b.add(new BigInteger("-1"))));
    ret = ret.divide(LevelGenerator.factorial(a));
    return ret;
  }
  /**
   * generates a random integer in a range
   * @param min the lower bound
   * @param max the upper bound
   * @return a random number in the range (min, max)
   */
  private static int getRandomNumberInRange(int min, int max) {

    if (min >= max) {
      throw new IllegalArgumentException("max must be greater than min");
    }

    //Random r = new Random();
    //return r.nextInt((max - min) + 1) + min;
    int i = (int) Math.round((Math.random() * ((max - min) + 1)) + min);
    while (i < min || i > max)
      i = (int) Math.round((Math.random() * ((max - min) + 1)) + min);
    return i;
  }

  /**
   * generates the level file
   */
  private void generate() {

    boolean debug = LevelGenerator.DEBUG;

    if (maxLoops > 5) {
      JOptionPane.showMessageDialog(this.frame, "QGraf only allows for up to 5 loops!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (maxStatesIn + maxStatesFin > 9) {
      JOptionPane.showMessageDialog(this.frame, "QGraf only allows for up to 9 external legs!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (maxStatesIn + maxStatesFin + maxLoops > 9) {
      JOptionPane.showMessageDialog(this.frame, "QGraf only allows for up to 9 as a value for the number of external legs plus the number of loops!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    qgraf = new File(qgrafPath);
    File out = new File(outputPath);
    File config = new File(configPath);

    /* collect all identifiers in this object*/

    Vector<String> idents = new Vector<>();
    int numDiffIdents = 0;

    if (!config.exists()) {
      JOptionPane.showMessageDialog(this.frame, "Could not find model file!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (!qgraf.exists()) {
      JOptionPane.showMessageDialog(this.frame, "Could not find QGraf executable!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (!out.exists()) {
      try {
        if (!out.createNewFile()) {
          JOptionPane.showMessageDialog(this.frame, "Could not open output file!", "Error", JOptionPane.ERROR_MESSAGE);
          return;
        }
      } catch (IOException ioex) {
        JOptionPane.showMessageDialog(this.frame, "Could not open output file: " + ioex, "Error", JOptionPane.ERROR_MESSAGE);
        ioex.printStackTrace();
        return;
      }
    }

    if (!qgraf.canExecute()) {
      JOptionPane.showMessageDialog(this.frame, "The qgraf executable can not be executed!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    if (out.length() != 0) {
      Object[] options = {"Overwrite file", "Cancel"};
      int n = JOptionPane.showOptionDialog(this.frame, "The output file already has content. Do you want to override it?", "Output file has content", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
      if (n == 1) {
        return;
      }
    }

    model = new File(out.getParent() + System.getProperty("file.separator") + "model.mod");
    if (debug) System.out.println("Model file: " + model.getAbsolutePath());
    try {
      model.getParentFile().mkdirs();
      model.createNewFile();
    } catch (IOException iex) {
      JOptionPane.showMessageDialog(this.frame, "Error: " + iex, "Error", JOptionPane.ERROR_MESSAGE);
      iex.printStackTrace();
      if (delete) model.delete();
      return;
    }

    qdat = new File(out.getParent() + System.getProperty("file.separator") + "qgraf.dat");
    if (debug) System.out.println("qgraf.dat file: " + qdat.getAbsolutePath());
    try {
      qdat.getParentFile().mkdirs();
      qdat.createNewFile();
    } catch (IOException iex) {
      JOptionPane.showMessageDialog(this.frame, "Error: " + iex, "Error", JOptionPane.ERROR_MESSAGE);
      iex.printStackTrace();
      if (delete) {
        model.delete();
        qdat.delete();
      }
      return;
    }

    style = new File(out.getParent() + System.getProperty("file.separator") + "style.sty");
    if (debug) System.out.println("style file: " + style.getAbsolutePath());
    try {
      qdat.getParentFile().mkdirs();
      qdat.createNewFile();
    } catch (IOException iex) {
      JOptionPane.showMessageDialog(this.frame, "Error: " + iex, "Error", JOptionPane.ERROR_MESSAGE);
      iex.printStackTrace();
      if (delete) {
        model.delete();
        qdat.delete();
        style.delete();
      }
      return;
    }

    try {

      FileWriter fw2 = new FileWriter(style);
      BufferedWriter bw2 = new BufferedWriter(fw2);
      bw2.write("<prologue>" + System.getProperty("line.separator"));

      bw2.write("<diagram>" + System.getProperty("line.separator"));

      bw2.write("<epilogue>" + System.getProperty("line.separator"));
      bw2.write("<diagram_index>" + System.getProperty("line.separator"));

      bw2.write("<exit>" + System.getProperty("line.separator"));
      bw2.close();
    } catch (IOException iex) {
      JOptionPane.showMessageDialog(this.frame, "Error: " + iex, "Error", JOptionPane.ERROR_MESSAGE);
      iex.printStackTrace();
      if (delete) {
        model.delete();
        qdat.delete();
        style.delete();
      }
      return;
    }


    /* writes the qgraf model file */
    try {
      FileWriter fw = new FileWriter(model);
      BufferedWriter bw = new BufferedWriter(fw);

      if (!config.canRead()) {
        JOptionPane.showMessageDialog(this.frame, "Could not read model file!", "Error", JOptionPane.ERROR_MESSAGE);
        if (delete) {
          model.delete();
          qdat.delete();
          style.delete();
        }
        return;
      }

      FileReader fr = new FileReader(config);
      BufferedReader br = new BufferedReader(fr);

      ArrayList<String[]> commands = new ArrayList<>();
      ArrayList<String[]> vertices = new ArrayList<>();

      // String line;
      // Pattern pt = Pattern.compile("^(?:\\s*?)\\[(.[^#\\]\\[]*+)](?:\\s*?)#*"); // Line definition
      // Pattern pt2 = Pattern.compile("^(?:\\s*?)\\{(.[^#}{]*+)}(?:\\s*?)#*"); // vertex definition
      // while ((line = br.readLine()) != null) {
      //   Matcher matcher = pt.matcher(line);
      //   Matcher matcher2 = pt2.matcher(line);
      //   if (matcher.find())
      //     commands.add(matcher.group(1).replaceAll("\\s+", "").split("[;,]"));
      //   else if (matcher2.find())
      //     vertices.add(matcher2.group(1).replaceAll("\\s+", "").split("[;,]"));
      // }

      String str;
      while ((str = br.readLine()) != null) {
        if (str.replaceAll("\\s", "").startsWith("[") && str.replaceAll("\\s", "").endsWith("]")) {
          String stripped = str.replaceAll("\\s", "");
          stripped = stripped.substring(stripped.indexOf("[") + 1, stripped.lastIndexOf("]"));
          commands.add(stripped.split("(?<!\\\\)[;,]"));
        } else if (str.replaceAll("\\s", "").startsWith("{") && str.replaceAll("\\s", "").endsWith("}")) {
          String stripped = str.replaceAll("\\s", "");
          stripped = stripped.substring(stripped.indexOf("{") + 1, stripped.lastIndexOf("}"));
          vertices.add(stripped.split("(?<!\\\\)[;,]"));
        }
      }

      if (commands.size() == 0) {
        JOptionPane.showMessageDialog(this.frame, "No line definitions found in model file!", "Error", JOptionPane.ERROR_MESSAGE);
        if (delete) {
          model.delete();
          qdat.delete();
          style.delete();
        }
        return;
      }

      /* check for invalid commands/vertices */
      for (int i = 0; i < commands.size(); i++) {
        String a = commands.get(i)[0];
        String b = commands.get(i)[1];
        // vertices.add(new String[] {a, b});
        for (int j = 0; j < commands.size(); j++) {
          if (i == j) continue;
          if (commands.get(j)[0].equals(a) || commands.get(j)[1].equals(b) || commands.get(j)[1].equals(a) || commands.get(j)[0].equals(b)) {
            JOptionPane.showMessageDialog(this.frame, "Invalid line start/end naming in model file!", "Error", JOptionPane.ERROR_MESSAGE);
            if (delete) {
              model.delete();
              qdat.delete();
              style.delete();
            }
            return;
          }

        }
      }
      for (String[] rule : vertices) {
        if (rule.length < 2) {
          JOptionPane.showMessageDialog(this.frame, "Invalid vertex rule found with just one identifier in model file!", "Error", JOptionPane.ERROR_MESSAGE);
          if (delete) {
            model.delete();
            qdat.delete();
            style.delete();
          }
          return;
        }
        for (String s : rule) {
          if (!s.toLowerCase().startsWith("type") && !s.toLowerCase().startsWith("vertex") && !s.toLowerCase().startsWith("factor")) {
            boolean found = false;
            for (String[] command : commands)
              if (command[0].split("\\|")[0].equals(s.split("\\|")[0]) || command[1].split("\\|")[0].equals(s.split("\\|")[0])) {
                found = true;
                break;
              }
            if (!found) {
              JOptionPane.showMessageDialog(this.frame, "Invalid vertex rule found in model file!", "Error", JOptionPane.ERROR_MESSAGE);
              if (delete) {
                model.delete();
                qdat.delete();
                style.delete();
              }
              return;
            }
          }
        }
      }

      for (String[] a : commands) {
        bw.write(toModel(a));
        if (debug) System.out.println("identifier: " + a[0]);
        if (debug) System.out.println("identifier: " + a[1]);
        idents.add(a[0].replaceAll(" ", ""));
        idents.add(a[1].replaceAll(" ", ""));
        if (a[0].replaceAll(" ", "").equals(a[1].replaceAll(" ", "")))
          numDiffIdents ++;
        else
          numDiffIdents += 2;
      }
      if (debug) System.out.println("Number of different identifiers: " + numDiffIdents);

      /* reformat rules */
      for (String[] arr : vertices) {
        HashMap<Integer, String> temp = new HashMap<>();
        int i = 0;
        String ident = "";
        for (String ele : arr) {
          if (ele.toLowerCase().startsWith("type") || ele.toLowerCase().startsWith("vertex") || ele.toLowerCase().startsWith("factor")) {
            ident = ele.split("[=(\\-)]+")[1].replaceAll("\\s", "");
          } else {
            if (i == arr.length - 1) {
              String[] map = ele.split("\\|");
              ele = map[0];
              if (map.length > 1) {
                ident = map[1];
              }
            }
            temp.put(i, ele);
            i++;
          }
        }
        bw.write("[");
        for (int el = 0; el < temp.size() - 1; el ++) {
          bw.write(temp.get(el) + ", ");
          idents.add(temp.get(el));
        }
        bw.write(temp.get(temp.size() - 1) + "]" + System.getProperty("line.separator"));
      }

      bw.close();
    } catch (Exception ex) {
      String strEx = ex.getMessage();
      JOptionPane.showMessageDialog(this.frame, "Could not convert the model file to QGraf model file: " + strEx, "Error", JOptionPane.ERROR_MESSAGE);
      if (delete) {
        model.delete();
        qdat.delete();
        style.delete();
      }
      return;
    }

    BigInteger numPossible = new BigInteger("0");

    for (int indexIni = minStatesIn; indexIni <= maxStatesIn; indexIni++) {
      for (int indexOut = minStatesFin; indexOut <= maxStatesFin; indexOut++) {

        BigInteger possibleIn = LevelGenerator.over(new BigInteger(Integer.toString(numDiffIdents)), new BigInteger(Integer.toString(indexIni)));
        BigInteger possibleOut = LevelGenerator.over(new BigInteger(Integer.toString(numDiffIdents)), new BigInteger(Integer.toString(indexOut)));

        BigInteger prod = possibleIn.multiply(possibleOut);

        numPossible = numPossible.add(prod);
      }
    }

    if (debug) System.out.println("Number of combinatorically possible diagrams: " + numPossible);

    /* copy the qgraf exe to the folder of the output */

    String newQGrafName = out.getParent() + System.getProperty("file.separator") + "qgraf.exe";

    try {
      Files.copy(qgraf.toPath(), new File(newQGrafName).toPath());
    } catch (IOException ioexc) {
      JOptionPane.showMessageDialog(this.frame, "Could not copy QGraf executable: " + ioexc.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      ioexc.printStackTrace();
      if (delete) {
        model.delete();
        qdat.delete();
        style.delete();
      }
      return;
    }
    qgraf = new File(newQGrafName);

    /* writes the actual level file */
    try {
      FileWriter fwLevel = new FileWriter(out);
      bwLevel = new BufferedWriter(fwLevel);

      bwLevel.write("model= ./");
      bwLevel.write(out.getName());
      bwLevel.write(System.getProperty("line.separator"));

      FileReader frModel = new FileReader(configPath);
      BufferedReader brModel = new BufferedReader(frModel);
      String currentLine;
      while ((currentLine = brModel.readLine()) != null) {
        bwLevel.write(System.getProperty("line.separator"));
        bwLevel.write(currentLine);
      }

      bwLevel.write(System.getProperty("line.separator"));

      HashMap<Map.Entry<HashMap<String, Integer>, HashMap<String, Integer>>, Integer> alreadyChecked = new HashMap<>();

      int numberOfFound = 0;

      while (numberOfFound < numberChallenges && (!new BigInteger(Integer.toString(alreadyChecked.size())).equals(numPossible) || numPossible.signum() != 1)) {
        if (debug) System.out.println("Number of found challenges: " + numberOfFound);
        if (debug) System.out.println("Number of checked challenges: " + new BigInteger(Integer.toString(alreadyChecked.size())));
        if (debug) System.out.println("Target number of challenges: " + numberChallenges);
        if (debug) System.out.println("Number combinatorially possible: " + numPossible);

        /* calculate the progress in percent */

        double relative = Math.max((double) numberOfFound / numberChallenges, (double) alreadyChecked.size() / numPossible.intValue());

        double progress = (Math.exp(/*Math.log(5) **/ (relative)) - 1) / (Math.exp(/*Math.log(5) **/ 1) - 1);

        proBar.setValue((int) Math.round(100*progress));

        int inStates = minStatesIn;
        if (minStatesIn != maxStatesIn)
          inStates = LevelGenerator.getRandomNumberInRange(minStatesIn, maxStatesIn);
        int finStates = minStatesFin;
        if (minStatesFin != maxStatesFin)
          finStates = LevelGenerator.getRandomNumberInRange(minStatesFin, maxStatesFin);

        HashMap<String, Integer> in = new HashMap<String, Integer>();
        for (int i = 0; i < inStates; i++) {
          int random = (int) Math.floor(Math.random() * idents.size());
          String id = idents.get(random);
          if (in.containsKey(id)) {
            in.put(id, in.get(id) +1);
          } else {
            in.put(id, 1);
          }
        }

        HashMap<String, Integer> fin = new HashMap<String, Integer>();
        for (int i = 0; i < finStates; i++) {
          int random = (int) Math.floor(Math.random() * idents.size());
          String id = idents.get(random);
          if (fin.containsKey(id)) {
            fin.put(id, fin.get(id) +1);
          } else {
            fin.put(id, 1);
          }
        }

        if (!alreadyChecked.containsKey(new AbstractMap.SimpleEntry<HashMap<String, Integer>, HashMap<String, Integer>>(in, fin))) {
          alreadyChecked.put(new AbstractMap.SimpleEntry<HashMap<String, Integer>, HashMap<String, Integer>>(in, fin), 1);

          int numberOfDiagrams = 0;

          for (int loopNumber = 0; loopNumber < maxLoops + 1; loopNumber++) {
            boolean writable = false;
            while (!writable) {
              try {
                BufferedWriter test = new BufferedWriter(new FileWriter(qdat));
                writable = true;
                test.close();
              } catch (Exception ex) {
                writable = false;
                try {
                  if (debug) System.out.println("Waiting for qgraf.dat to be writeable ...");
                  Thread.sleep(5);
                } catch (Exception ex2) {
                  if (debug) System.err.println("Waiting failed ...");
                }
              }
            }

            FileWriter fw1 = new FileWriter(qdat);
            BufferedWriter bw1 = new BufferedWriter(fw1);

            try {

              bw1.write("output= \'qout\';" + System.getProperty("line.separator"));

              bw1.write("style= \'style.sty\';" + System.getProperty("line.separator"));

              bw1.write("model= \'" + "model.mod" + "\';" + System.getProperty("line.separator"));

              bw1.write("in= ");
              String ins = "";

              if (debug) System.out.println("Number of incoming particles: " + in.entrySet().size());
              for(Map.Entry<String, Integer> mapEntry : in.entrySet()) {
                for (int num = 0; num < mapEntry.getValue(); num++) {
                  ins += mapEntry.getKey() + ", ";
                }
              }
              ins = ins.substring(0, ins.length() - 2);
              ins += ";" + System.getProperty("line.separator");
              bw1.write(ins);

              bw1.write("out= ");
              String outs = "";

              if (debug) System.out.println("Number of outgoing particles: " + fin.entrySet().size());
              for(Map.Entry<String, Integer> mapEntry : fin.entrySet()) {
                for (int num = 0; num < mapEntry.getValue(); num++) {
                  outs += mapEntry.getKey() + ", ";
                }
              }
              outs = outs.substring(0, outs.length() - 2);
              outs += ";" + System.getProperty("line.separator");

              bw1.write(outs);

              bw1.write("loops= " + loopNumber + ";" + System.getProperty("line.separator"));

              bw1.write("loop_momentum= ;" + System.getProperty("line.separator"));

              bw1.write("options= ;" + System.getProperty("line.separator"));
              bw1.flush();
              bw1.close();
            } catch (Exception ex) {
              if (debug) System.err.println("Could not write qgraf.dat:");
              ex.printStackTrace();
            }

            if (!System.getProperty("os.name").toLowerCase().startsWith("win")) {

              ProcessBuilder processBuilder = new ProcessBuilder();
              if (debug)
                processBuilder.command("bash", "-c", "time ./" + qgraf.getName());
              else
                processBuilder.command("bash", "-c", "./" + qgraf.getName() + " &> /dev/null");
              processBuilder.directory(new File(qgraf.getParent()));

              try {

                Process process = processBuilder.start();

                StringBuilder output = new StringBuilder();

                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

                String line;
                while ((line = reader.readLine()) != null) {
                  output.append(line + System.getProperty("line.separator"));
                  if (debug) System.out.println(line);
                }

                if (debug) System.err.println("Errors: ");
                String s = "";
                while ((s = stdError.readLine()) != null) {
                  if (debug) System.err.println(s);
                }
                int exitVal = 0;
                try {
                  exitVal = process.waitFor();
                } catch (java.lang.InterruptedException ex) {
                  if (delete) {
                    qgraf.delete();
                    model.delete();
                    qdat.delete();
                    style.delete();
                  }
                  if (debug) System.err.println("QGraf was interrupted");
                  return;
                }
                if (exitVal != 0) {
                  String strEx = System.getProperty("line.separator") + output;

                  JOptionPane.showMessageDialog(this.frame, "Could not convert the model file to QGraf model file: " + strEx, "Error", JOptionPane.ERROR_MESSAGE);
                  if (delete) {
                    qgraf.delete();
                    model.delete();
                    qdat.delete();
                    style.delete();
                  }
                  return;
                }

              } catch (IOException e) {
                JOptionPane.showMessageDialog(new JFrame(), "Something went wrong " + System.getProperty("line.separator") + e, "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                if (delete) {
                  qgraf.delete();
                  model.delete();
                  qdat.delete();
                  style.delete();
                }
                return;
              }
            } else {
              ProcessBuilder processBuilder = new ProcessBuilder();
              processBuilder.command("cmd.exe", "/c", ".\\" + qgraf.getName());
              processBuilder.directory(new File(qgraf.getParent()));

              try {
                Process process = processBuilder.start();

                StringBuilder output = new StringBuilder();

                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

                String line;
                while ((line = reader.readLine()) != null) {
                  output.append(line + System.getProperty("line.separator"));
                  if (debug) System.out.println(line);
                }

                if (debug) System.err.println("Errors: ");
                String s = "";
                while ((s = stdError.readLine()) != null) {
                  if (debug) System.err.println(s);
                }
                int exitVal = 0;
                try {
                  exitVal = process.waitFor();
                } catch (InterruptedException ex) {
                  if (delete) {
                    qgraf.delete();
                    model.delete();
                    qdat.delete();
                    style.delete();
                  }
                  if (debug) System.err.println("QGraf was interrupted");
                  return;
                }
                if (exitVal != 0) {
                  String strEx = System.getProperty("line.separator") + output;

                  JOptionPane.showMessageDialog(this.frame, "Could not convert the model file to QGraf model file: " + strEx, "Error", JOptionPane.ERROR_MESSAGE);
                  if (delete) {
                    qgraf.delete();
                    model.delete();
                    qdat.delete();
                    style.delete();
                  }
                  return;
                }

              } catch (IOException e) {
                JOptionPane.showMessageDialog(new JFrame(), "Something went wrong " + System.getProperty("line.separator") + e, "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                if (delete) {
                  qgraf.delete();
                  model.delete();
                  qdat.delete();
                  style.delete();
                }
                return;
              }
            }

            try {

              File outPut = new File(qgraf.getParent() + System.getProperty("file.separator") + "qout");
              FileReader fReader = new FileReader(outPut);
              BufferedReader bReader = new BufferedReader(fReader);

              numberOfDiagrams += Integer.parseInt(bReader.readLine());
              fReader.close();
              if (debug) System.out.println("Number of Diagrams: " + numberOfDiagrams);

              outPut.delete();
            } catch (FileNotFoundException qgrafFailed) {
              if (debug) System.err.println("QGraf failed");
            }

          }

          if (numberOfDiagrams > 0) {
            numberOfFound ++;

            bwLevel.write(System.getProperty("line.separator"));
            bwLevel.write(System.getProperty("line.separator"));

            bwLevel.write("easy" + System.getProperty("line.separator"));
            bwLevel.write(System.getProperty("line.separator"));


            bwLevel.write("start: ");

            String ins = "";
            for(Map.Entry<String, Integer> mapEntry : in.entrySet()) {
              for (int num = 0; num < mapEntry.getValue(); num++) {
                ins += mapEntry.getKey() + ", ";
              }
            }
            ins = ins.substring(0, ins.length() - 2);
            ins += System.getProperty("line.separator");
            bwLevel.write(ins);

            bwLevel.write(System.getProperty("line.separator"));

            bwLevel.write("end: ");

            String outs = "";
            for(Map.Entry<String, Integer> mapEntry : fin.entrySet()) {
              for (int num = 0; num < mapEntry.getValue(); num++) {
                outs += mapEntry.getKey() + ", ";
              }
            }
            outs = outs.substring(0, outs.length() - 2);
            outs += System.getProperty("line.separator");
            bwLevel.write(outs);

            bwLevel.flush();

          }

        }

      }

      bwLevel.flush();
      bwLevel.close();
    } catch (IOException ex) {
      JOptionPane.showMessageDialog(new JFrame(), "File could not be opened " + System.getProperty("line.separator") + ex, "Error", JOptionPane.ERROR_MESSAGE);
      ex.printStackTrace();
    }

    if (delete) {
      qgraf.delete();
      model.delete();
      qdat.delete();
      style.delete();
    }
  }
  /**
   * Helper function parsing one config line entry and converting to propagator line in model file for qgraf.
   *
   * @param s one entry from config, specifying a {@link game.LineConfig}
   * @return parsed string to add to qgraf model file
   * @throws Exception if config entry is not valid
   */
  private String toModel(String[] s) {

    if (s.length < 3) {
      JOptionPane.showMessageDialog(this.frame, "Not enough arguments in line definition in model File: " + String.join(",", s), "Error", JOptionPane.ERROR_MESSAGE);
      return "";
    }
    String identifier1 = s[0];
    String identifier2 = s[1];

    String sign = "+";

    // if (!identifier1.equals(identifier2))
    //  sign = "-";
    //
    // String type = s[2].toLowerCase().replaceAll(" ", "");
    //
    // if (type.equals("gluon") || type.equals("spiral") ||type.equals("wave") || type.equals("photon"))
    //  sign = "+";

    String returnVal = ("[" + identifier1 + ", " + identifier2 + ", " + sign + "]" + System.getProperty("line.separator"));
    return returnVal;
  }

  /**
   * writes the preferences to the file stored under:
   * <ul><li>Windows: C:\\Users\\<i>User_Name</i>\\AppData\\Roaming\\FeynGame\\</li>
   * <li>GNU: /home/.config/FeynGame/</li>
   * <li>MacOs: ~/Library/Preferences/FeynGame/</li></ul>
   **/
  private void setPreferences() {
    String prefFileSt = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      prefFileSt = System.getProperty("user.home") + "/Library/Preferences/FeynGame/Generator.ini";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      prefFileSt = System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\Generator.ini";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      prefFileSt = System.getProperty("user.home") + "/.config/FeynGame/Generator.ini";
    }
    File prefFile = new File(prefFileSt);
    try {
      prefFile.getParentFile().mkdirs();
      prefFile.createNewFile();
    } catch (IOException iex) {
      System.err.println(iex);
    }
    if (!prefFile.canWrite()) {
      System.err.println("Could not save preferences use.");
      return;
    }

    System.out.println("Saved preferences to: " + prefFile.getAbsolutePath());

    try (BufferedWriter bw = new BufferedWriter(new FileWriter(prefFile))) {
      if (this.configPath != null)
        bw.write("Config_File:" + this.configPath + System.getProperty("line.separator"));

      if (this.qgrafPath != null)
        bw.write("QGraf_File:" + this.qgrafPath + System.getProperty("line.separator"));

      if (this.outputPath != null)
        bw.write("Output_File:" + this.outputPath + System.getProperty("line.separator"));

      bw.write("Max_num_Loops:" + String.valueOf(this.maxLoops) + System.getProperty("line.separator"));

      if (this.maxStatesIn != 0)
        bw.write("Max_states_in:" + String.valueOf(this.maxStatesIn) + System.getProperty("line.separator"));

      if (this.maxStatesFin != 0)
        bw.write("Max_states_fin:" + String.valueOf(this.maxStatesFin) + System.getProperty("line.separator"));

      if (this.minStatesIn != 0)
        bw.write("Min_states_in:" + String.valueOf(this.minStatesIn) + System.getProperty("line.separator"));

      if (this.minStatesFin != 0)
        bw.write("Min_states_fin:" + String.valueOf(this.minStatesFin) + System.getProperty("line.separator"));

      if (this.numberChallenges != 0)
        bw.write("Challenges:" + String.valueOf(this.numberChallenges) + System.getProperty("line.separator"));
    } catch (Exception e) {
      System.err.println(e);
    }
  }

  /**
   * loads the preferences from the file stored under:
   * <ul><li>Windows: C:\\Users\\<i>User_Name</i>\\AppData\\Roaming\\FeynGame\\</li>
   * <li>GNU: /home/.config/FeynGame/</li>
   * <li>MacOs: ~/Library/Preferences/FeynGame/</li></ul>
   **/
  private void getPreferences() {
    String prefFileSt = "";
    if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
      prefFileSt = System.getProperty("user.home") + "/Library/Preferences/FeynGame/Generator.ini";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
      prefFileSt = System.getProperty("user.home") + "\\AppData\\Roaming\\FeynGame\\Generator.ini";
    } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
      prefFileSt = System.getProperty("user.home") + "/.config/FeynGame/Generator.ini";
    }
    File prefFile = new File(prefFileSt);
    try {
      prefFile.getParentFile().mkdirs();
      prefFile.createNewFile();
    } catch (IOException iex) {
      System.err.println(iex);
    }
    if (!prefFile.canRead()) {
      System.err.println("Could not load preferences.");
      return;
    }
    try (BufferedReader br = new BufferedReader(new FileReader(prefFile))) {
      String line;
      while ((line = br.readLine()) != null) {
        if (line.startsWith("Config_File")) {
          String value = line.split(":", 2)[1];
          this.configPath = value;
        } else if (line.startsWith("QGraf_File")) {
          String value = line.split(":", 2)[1];
          this.qgrafPath = value;
        } else if (line.startsWith("Output_File")) {
          String value = line.split(":", 2)[1];
          this.outputPath = value;
        } else if (line.startsWith("Max_num_Loops")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          this.maxLoops = (int) Float.parseFloat(value);
        } else if (line.startsWith("Max_states_in")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          this.maxStatesIn = (int) Float.parseFloat(value);
        } else if (line.startsWith("Max_states_fin")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          this.maxStatesFin = (int) Float.parseFloat(value);
        } else if (line.startsWith("Min_states_in")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          this.minStatesIn = (int) Float.parseFloat(value);
        } else if (line.startsWith("Min_states_fin")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          this.minStatesFin = (int) Float.parseFloat(value);
        }  else if (line.startsWith("Challenges")) {
          String value = line.split(":")[1].replaceAll("\\s", "");
          this.numberChallenges = (int) Float.parseFloat(value);
        }
      }
    } catch (Exception e) {
      System.err.println(e);
    }
  }

  public void windowClosing(WindowEvent e) {
    this.setPreferences();
  }

  public void windowClosed(WindowEvent e) {
  }

  public void windowOpened(WindowEvent e) {
  }

  public void windowIconified(WindowEvent e) {
  }

  public void windowDeiconified(WindowEvent e) {
  }

  public void windowActivated(WindowEvent e) {
  }

  public void windowDeactivated(WindowEvent e) {
  }

  public void windowGainedFocus(WindowEvent e) {
  }

  public void windowLostFocus(WindowEvent e) {
  }

  public void windowStateChanged(WindowEvent e) {
  }
}
