//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

/**
 * The help window for the LevelGenerator.
 *
 * @author Sven Yannick Klein
 */

import javax.swing.*;
import java.awt.*;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

public class Help extends JFrame {
  /**
   * Serial Version UID
   */
  static final long serialVersionUID = 32L;

  public Help() {
    this.setLocationRelativeTo(null);
    this.setTitle("Help");
    // this.setType(Type.POPUP);
    this.setResizable(false);

    this.setLayout(new BorderLayout());

    JEditorPane page = new JEditorPane();
    page.setEditable(false);

    StyleSheet styleSheet = new StyleSheet();
    styleSheet.addRule("kbd {border-radius: 3px;padding: 5px 5px 5px;border: 1px outset #D3D3D3;box-shadow: 0px 1px #888888;font-family: \"Lucida Console\", Monaco, monospace;}");
    styleSheet.addRule("* {font-family: sans-serif;}");
    styleSheet.addRule("table {border-collapse: collapse;width : 100%;}");
    styleSheet.addRule("td {border: 1px solid #D3D3D3;padding: 15px;}");
    styleSheet.addRule("th {text-align: left;border: 1px solid #D3D3D3;padding: 15px;font-weight: normal;background-color : #F8F8F8;color: #2e2e2e;border-bottom: 2px solid #D3D3D3;}");
    styleSheet.addRule("h2 {display: block;font-size: 1.5em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h3 {display: block;font-size: 1.17em;margin-top: 1em;margin-bottom: 1em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h4 {display: block;font-size: 1em;margin-top: 1.33em;margin-bottom: 1.33em;margin-left: 0;margin-right: 0;font-weight: bold;}");
    styleSheet.addRule("h1 {display: block;font-size: 2em;margin-top: 0.83em;margin-bottom: 0.83em;margin-left: 0;margin-right: 0;font-weight: bold;}");

    EditorKit kit = JEditorPane.createEditorKitForContentType("text/html");
    ((HTMLEditorKit) kit).setStyleSheet(styleSheet);
    HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();
    page.setEditorKit(kit);
    page.setDocument(doc);

    page.setText(
      "<p text align = justify>"
      + "To use the level generator one needs to first specify the underlying model. Therefore one has to select a"
      + " model file (*.model) in the first line of the window either by providing the absolute file path to the model"
      + " or by selecting the model file in the file explorer that pops up when pressing the <b>Select File</b>"
      + " button. Below that, several different settings for the generated level file can be found (e.g. the maximum number"
      + " of loops). These settings should be self explanatory. It should be noted however, that the usage of QGraf"
      + " internally constrains these settings. The program will issue a warning however when any of the constraints are"
      + " not met. After these settings are set, one has to provide the path to a QGraf executable. This is possible in"
      + " exactly the same fashion as for the model file. The last step is to set the output file, i.e. the \".if\" file"
      + " where the level is stored in.<br><br>"

      + "The generation of the level file starts if the user presses the <b>Generate InFin Level</b> button and all the"
      + " constraints by QGraf are met. A new window will appear that shows the progress of the generation. This window"
      + " also contains a <b>Cancel</b> button. Upon pressing this button, the generation of the level is stopped, but"
      + " the challenges that have already been found are saved in the output file. When the generation has finished"
      + " the window that shows the progress closes and one can continue generating other level files or close the level"
      + " generator by pressing the <b>Exit</b> button."
      + "<p>"
    );
    Dimension minimumSize = new Dimension(500, 500);
    page.setPreferredSize(minimumSize);
    //page.setMinimumSize(minimumSize);

    this.add(page, BorderLayout.CENTER);


    JButton close = new JButton("Close");

    close.addActionListener(e -> {
      setVisible(false);
    });
    this.add(close, BorderLayout.SOUTH);

    this.pack();
    this.setVisible(true);
  }
}
