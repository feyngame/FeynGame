import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.stream.Stream;

import javax.print.attribute.standard.MediaSizeName;

import export.ExportFormat;
import export.ExportManager;
import export.ExportManager.PaperUnit;
import qgraf.QgrafOutputFile;

public class OptionParser {
  
  protected final List<String> args;
  protected boolean help = false;
  protected Path loadFg = null;
  protected Path loadIf = null;
  protected Path loadModel = null;
  protected String importStyle = null; //Keep the string since it might be the name of a default style, which has priority
  protected Path importQgraf = null;
  protected Path exportDest = null;
  protected String exportExt = null;
  protected int qgrafDiagramIndex = QgrafOutputFile.LOAD_ALL_INDEX_VALUE;
  protected Rectangle2D mbbArea = null;
  protected boolean applyLayout = false;
  protected boolean blackwhite = false;
  protected boolean transparent = false;
  protected int[] imageSize = null;
  protected MediaSizeName paperName = null;
  protected PaperUnit physicalSizeUnit = null;
  protected double[] physicalSize = null;
  protected boolean enableLabels = false;
  protected boolean disableLabels = false;
  protected boolean distributeMomenta = false;
  protected ExportFormat exportFormat = null;
  protected int infinTime = 0;
  protected boolean skipToDrawMode = false;
  protected boolean landscape = false;
  protected int[] multiRowCol = null;
  protected boolean version = false;
  protected boolean debug = false;
  protected boolean applyModel = false;
  protected Level fileLevel = null;
  protected Level consoleLevel = null;
  
  private OptionParser(String[] args) {
    this.args = Arrays.asList(args);
    if(this.args.contains(null)) throw new IllegalStateException();
  }
  
  private void doParse() throws OptionException {
    ListIterator<String> li = args.listIterator();
    
    boolean filenameMode = true;
    while(li.hasNext()) {
      String current = li.next();
      
      //initially start in filename mode, switch to param mode once the first param is detected
      if(current.startsWith("-")) {
        filenameMode = false; //Switch to parameter mode
      }
      
      if(filenameMode) {
        //In filename mode, we accept a list of file names, and the behavior depends on the file extension.
        Path path = Paths.get(current).toAbsolutePath();
        String extension = ExportManager.getFileExtension(path);
        if(!Files.exists(path)) throw new OptionException("File '" + path.toString() + "' does not exist", OptionException.EC_FILE_NOT_FOUND);
        
        if(extension.equals("fg")) {
          if(loadFg != null) throw new OptionException("Cannot load more than one Feyngame save file (.fg)", OptionException.EC_DUPLICATE_OPTION);
          loadFg = path;
        } else if(extension.equals("model")) {
          if(loadModel != null) throw new OptionException("Cannot load more than one Feyngame model file (.model)", OptionException.EC_DUPLICATE_OPTION);
          loadModel = path;
        } else if(extension.equals("if")) {
          if(loadIf != null) throw new OptionException("Cannot load more than one Feyngame InFin level file (.if)", OptionException.EC_DUPLICATE_OPTION);
          loadIf = path;
        } else if(extension.equals("sty")) {
          if(importStyle != null) throw new OptionException("Cannot load more than one Qgraf style file (.sty)", OptionException.EC_DUPLICATE_OPTION);
          importStyle = path.toString();
        } else if(extension.equals("out")) {
          if(importQgraf != null) throw new OptionException("Cannot load more than one Qgraf output file (.out)", OptionException.EC_DUPLICATE_OPTION);
          importQgraf = path;
        } else {
          //only support single diagram export with this option
          ExportManager.getFormatForExtension(extension, false).orElseThrow(
              () -> new OptionException("No known exporter for file with extension '" + extension + "'", OptionException.EC_UNKNOWN_EXPORT_FORMAT));
          if(exportDest != null) throw new OptionException("Cannot export to more than one destination", OptionException.EC_DUPLICATE_OPTION);
          exportDest = path;
        }
        
      } else { //!filenameMode aka parameter mode
        //In parameter mode, we parse "-param <values>" or "--long_param_name <values>"
        List<String> values = new ArrayList<>();
        if(!current.startsWith("-")) throw new IllegalStateException();
        //Count the number of values afterwards
        while(li.hasNext()) {
          String s = li.next();
          if(s.startsWith("-")) {
            li.previous(); //move back so that the - param is still read
            break;
          } else {
            values.add(s);
          }
        }
        
        /*
         * Allow for a few more ways than usual since a user that wants to see the help might not know the exact way to show it
         */
        if(current.equals("-h") || current.equals("-help") || current.equals("--help") || current.equals("-?") || current.equals("/?")) {
          help = true;
        } else if(current.equals("-v") || current.equals("--version")) {
          version = true;
        } else if(current.equals("--debug")) {
          debug = true;
        } else if(current.equals("-m") || current.equals("--model")) {
          Path path = parsePath(current, values, true);
          if(loadModel != null) throw new OptionException("Cannot load more than one Feyngame model file", OptionException.EC_DUPLICATE_OPTION);
          loadModel = path;
        } else if(current.equals("-d") || current.equals("--diagram")) { //TODO allow no-args to go to draw mode
          if(values.size() == 0) {
            skipToDrawMode = true;
          } else {
            Path path = parsePath(current, values, true);
            if(loadFg != null) throw new OptionException("Cannot load more than one Feyngame save file", OptionException.EC_DUPLICATE_OPTION);
            loadFg = path;
          }
        } else if(current.equals("-l") || current.equals("--level")) {
          Path path = parsePath(current, values, true);
          if(loadIf != null) throw new OptionException("Cannot load more than one Feyngame InFin level file", OptionException.EC_DUPLICATE_OPTION);
          loadIf = path;
        } else if(current.equals("-s") || current.equals("--qgraf-style")
            || current.equals("-qs") || current.equals("--qgraf_style")) { //also support flags from previous FeynGame versions, but don't advertise them
          if(importStyle != null) throw new OptionException("Cannot load more than one qgraf style file", OptionException.EC_DUPLICATE_OPTION);
          if(values.size() != 1) throw new OptionException("Option '" + current + "' requires exactly one value", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          importStyle = values.get(0);
        } else if(current.equals("-q") || current.equals("--qgraf-file")
            || current.equals("-qo") || current.equals("--qgraf_output")) {
          Path path = parsePath(current, values, true);
          if(importQgraf != null) throw new OptionException("Cannot load more than one qgraf output file", OptionException.EC_DUPLICATE_OPTION);
          importQgraf = path;
        } else if(current.equals("-e") || current.equals("--export")) {
          Path path = parsePath(current, values, false);
          if(exportDest != null) throw new OptionException("Cannot export to more than one destination", OptionException.EC_DUPLICATE_OPTION);
          exportDest = path;
        } else if(current.equals("-f") || current.equals("--export-format") 
            || current.equals("-of") || current.equals("--output_format")) {
          if(exportExt != null) throw new OptionException("Cannot explicitly set more than one export format", OptionException.EC_DUPLICATE_OPTION);
          if(values.size() != 1) throw new OptionException("Option '" + current + "' requires exactly one value", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          exportExt = values.get(0);
        } else if(current.equals("--diagram-index") || current.equals("--diagram_index")) {
          int val = parsePosInt(current, values, true);
          if(qgrafDiagramIndex != QgrafOutputFile.LOAD_ALL_INDEX_VALUE) throw new OptionException("Cannot set diagram index more than once", OptionException.EC_DUPLICATE_OPTION);
          qgrafDiagramIndex = val;
        } else if(current.equals("-b") || current.equals("--bounding-box") || current.equals("--bounds")) {
          if(mbbArea != null) throw new OptionException("Cannot set the bounding box more than once", OptionException.EC_DUPLICATE_OPTION);
          if(values.size() == 2) { //w,h
            int w = parsePosInt(current, values.get(0), false);
            int h = parsePosInt(current, values.get(1), false);
            mbbArea = new Rectangle(w, h);
          } else if(values.size() == 4) { //x,y,w,h
            int x = parsePosInt(current, values.get(0), false);
            int y = parsePosInt(current, values.get(1), false);
            int w = parsePosInt(current, values.get(2), false);
            int h = parsePosInt(current, values.get(3), false);
            mbbArea = new Rectangle(x, y, w, h);
          } else {
            throw new OptionException("Option '" + current + "' expects either two or four values", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          }
        } else if(current.equals("--apply-layout")) {
          if(applyLayout) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          applyLayout = true;
        } else if(current.equals("--blackwhite") || current.equals("--greyscale") || current.equals("--grayscale")) {
          if(blackwhite) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          blackwhite = true;
        } else if(current.equals("--transparent")) {
          if(transparent) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          transparent = true;
        } else if(current.equals("--landscape")) {
          if(landscape) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          landscape = true;
        } else if(current.equals("--image-size")) { //<width> or <width> <height>
          if(imageSize != null) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          if(values.size() == 1) {
            imageSize = new int[] {parsePosInt(current, values.get(0), false)};
          } else if(values.size() == 2) {
            imageSize = new int[] {parsePosInt(current, values.get(0), false),
                                   parsePosInt(current, values.get(1), false)};
          } else {
            throw new OptionException("option " + current + " expects one or two values", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          }
        } else if(current.equals("--paper-size")) { //<width> <unit> or <width> <height> <unit> or <paper> 
          if(paperName != null || physicalSizeUnit != null || physicalSize != null)
            throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          if(values.size() == 1) {
            paperName = parsePaperName(values.get(0));
          } else if(values.size() == 2) {
            physicalSize = new double[] {parsePosDouble(current, values.get(0), false)};
            physicalSizeUnit = export.ExportManager.PaperUnit.fromString(values.get(1))
                .orElseThrow(() -> new OptionException("Expected a paper size unit (like cm or mm), but found '" + values.get(1) + "'", OptionException.EC_INVALID_OPTION_VALUE));
          } else if(values.size() == 3) {
            physicalSize = new double[] {parsePosDouble(current, values.get(0), false),
                                         parsePosDouble(current, values.get(1), false)};
            physicalSizeUnit = export.ExportManager.PaperUnit.fromString(values.get(2))
                .orElseThrow(() -> new OptionException("Expected a paper size unit (like cm or mm), but found '" + values.get(2) + "'", OptionException.EC_INVALID_OPTION_VALUE));
          } else {
            throw new OptionException("option " + current + " expects one, two or three values", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          }
        } else if(current.equals("--labels")) {
          if(enableLabels) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          enableLabels = true;
        } else if(current.equals("--no-labels")) {
          if(disableLabels) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          disableLabels = true;
        } else if(current.equals("--momenta")) {
          if(distributeMomenta) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          distributeMomenta = true;
        } else if(current.equals("-t") || current.equals("--time-limit")) {
          if(infinTime != 0) throw new OptionException("Cannot set time limit more than once", OptionException.EC_DUPLICATE_OPTION);
          infinTime = parsePosInt(current, values, true);
        } else if(current.equals("--diagrams-per-page")) {
          if(multiRowCol != null) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          if(values.size() == 2) {
            int rows = parsePosInt(current, values.get(0), false);
            int cols = parsePosInt(current, values.get(1), false);
            multiRowCol = new int[] {rows, cols};
          } else {
            throw new OptionException("Option " + current + " expects exactly two values", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          }
        } else if(current.equals("--apply-model")) {
          if(applyModel) throw new OptionException("Cannot set " + current + " twice", OptionException.EC_DUPLICATE_OPTION);
          applyModel = true;
        } else if(current.equals("--log-level")) {
          if(values.size() == 1) {
            Level level = parseLogLevel(values.get(0));
            consoleLevel = level;
            fileLevel = level;
          } else {
            throw new OptionException("Option " + current + " expects exactly one value", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          }
        } else if(current.equals("--console-log-level")) {
          if(values.size() == 1) {
            consoleLevel = parseLogLevel(values.get(0));
          } else {
            throw new OptionException("Option " + current + " expects exactly one value", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          }
        } else if(current.equals("--file-log-level")) {
          if(values.size() == 1) {
            fileLevel = parseLogLevel(values.get(0));
          } else {
            throw new OptionException("Option " + current + " expects exactly one value", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
          }
        } else {
          throw new OptionException("Unknown option '" + current + "'", OptionException.EC_UNKNOWN_OPTION);
        }
        
      } //end if filenameMode
    } //end while args

    if(importStyle == null && importQgraf != null) {
      throw new OptionException("If a qgraf output file is given, a qgraf style file must be provided as well", OptionException.EC_MISSING_REQIRED_OPTION);
    }
    
    //Find the ExportFormat
    String x = getExportFormat();
    if(x != null) {
      exportFormat = ExportManager.getFormatForExtension(x, doMultiExport())
          .orElseThrow(() -> new OptionException("Cannot find an exporter for extension " + x, OptionException.EC_UNKNOWN_EXPORT_FORMAT));
    }
    
    if(loadIf != null) { //check for InFin mode options
      if(hasExportRelatedOption()) throw new OptionException("Cannot use export-related options when loading InFin level", OptionException.EC_INCOMPATIBLE_OPTIONS);
      if(distributeMomenta || applyLayout || enableLabels) throw new OptionException("Cannot use options that operate on diagrams when loading an InFin mode level", OptionException.EC_INCOMPATIBLE_OPTIONS);
      if(importStyle != null) throw new OptionException("Cannot load a qgraf style when loading an InFin mode level", OptionException.EC_INCOMPATIBLE_OPTIONS);
    }
    
    if(loadFg == null && importQgraf == null) { //check that we don't have any options that operate on diagrams since we don't have one
      if(hasExportRelatedOption()) throw new OptionException("Cannot use export-related options when not either loading a .fg file or importing from qgraf", OptionException.EC_INCOMPATIBLE_OPTIONS);
      if(distributeMomenta || applyLayout || enableLabels) throw new OptionException("Cannot use options that operate on diagrams when not either loading a .fg file or importing from qgraf", OptionException.EC_INCOMPATIBLE_OPTIONS);
    }
    
    if(exportDest == null && hasExportRelatedOption()) //check that we don't have any export options
      throw new OptionException("Cannot use export-related options without an export destination file", OptionException.EC_MISSING_REQIRED_OPTION);
    
    if(importQgraf == null && qgrafDiagramIndex != QgrafOutputFile.LOAD_ALL_INDEX_VALUE) //check that we don't have any qgraf import options
      throw new OptionException("Cannot set qgraf diagram index without importing from qgraf file", OptionException.EC_MISSING_REQIRED_OPTION);
    
    if(loadFg != null && loadIf != null)
      throw new OptionException("Cannot load a draw mode (.fg) and InFin mode (.if) file at the same time", OptionException.EC_INCOMPATIBLE_OPTIONS);
    
    if(loadFg != null && importQgraf != null)
      throw new OptionException("Cannot load a draw mode (.fg) file and qgraf file at the same time", OptionException.EC_INCOMPATIBLE_OPTIONS);
    
    if(importQgraf != null && loadIf != null)
      throw new OptionException("Cannot load an InFin mode (.if) file and a qgraf file at the same time", OptionException.EC_INCOMPATIBLE_OPTIONS);
    
    if(imageSize != null && (paperName != null || physicalSize != null))
      throw new OptionException("Cannot set image size and paper size at the same time", OptionException.EC_INCOMPATIBLE_OPTIONS);
    
    if(enableLabels && disableLabels) {
      throw new OptionException("Cannot use --labels and --no-labels together", OptionException.EC_INCOMPATIBLE_OPTIONS);
    }
  }
  
  @SuppressWarnings("serial")
  private static Level parseLogLevel(String text) throws OptionException {
    for(Field f : Level.class.getFields()) {
      int m = f.getModifiers();
      if(Modifier.isStatic(m) && Modifier.isPublic(m) && Modifier.isFinal(m)) {
        if(f.getName().equalsIgnoreCase(text)) {
          try {
            return (Level) f.get(null);
          } catch (IllegalArgumentException | IllegalAccessException e) {
            return null;
          }
        }
      }
    }
    
    try {
      int l = Integer.parseInt(text);
      return new Level("Custom(" + l + ")", l) {};
    } catch (NumberFormatException e) {
      throw new OptionException("Log level is not a valid level name or integer", e, OptionException.EC_INVALID_OPTION_VALUE);
    }
  }
  
  private static MediaSizeName parsePaperName(String paper) throws OptionException {
    if(paper.equalsIgnoreCase("a0") || paper.equalsIgnoreCase("a0paper")) return MediaSizeName.ISO_A0;
    if(paper.equalsIgnoreCase("a1") || paper.equalsIgnoreCase("a1paper")) return MediaSizeName.ISO_A1;
    if(paper.equalsIgnoreCase("a2") || paper.equalsIgnoreCase("a2paper")) return MediaSizeName.ISO_A2;
    if(paper.equalsIgnoreCase("a3") || paper.equalsIgnoreCase("a3paper")) return MediaSizeName.ISO_A3;
    if(paper.equalsIgnoreCase("a4") || paper.equalsIgnoreCase("a4paper")) return MediaSizeName.ISO_A4;
    if(paper.equalsIgnoreCase("a5") || paper.equalsIgnoreCase("a5paper")) return MediaSizeName.ISO_A5;
    if(paper.equalsIgnoreCase("a6") || paper.equalsIgnoreCase("a6paper")) return MediaSizeName.ISO_A6;
    if(paper.equalsIgnoreCase("a7") || paper.equalsIgnoreCase("a7paper")) return MediaSizeName.ISO_A7;
    if(paper.equalsIgnoreCase("a8") || paper.equalsIgnoreCase("a8paper")) return MediaSizeName.ISO_A8;
    if(paper.equalsIgnoreCase("a9") || paper.equalsIgnoreCase("a9paper")) return MediaSizeName.ISO_A9;
    if(paper.equalsIgnoreCase("a10") || paper.equalsIgnoreCase("a10paper")) return MediaSizeName.ISO_A10;
    if(paper.equalsIgnoreCase("b0") || paper.equalsIgnoreCase("b0paper")) return MediaSizeName.ISO_B0;
    if(paper.equalsIgnoreCase("b1") || paper.equalsIgnoreCase("b1paper")) return MediaSizeName.ISO_B1;
    if(paper.equalsIgnoreCase("b2") || paper.equalsIgnoreCase("b2paper")) return MediaSizeName.ISO_B2;
    if(paper.equalsIgnoreCase("b3") || paper.equalsIgnoreCase("b3paper")) return MediaSizeName.ISO_B3;
    if(paper.equalsIgnoreCase("b4") || paper.equalsIgnoreCase("b4paper")) return MediaSizeName.ISO_B4;
    if(paper.equalsIgnoreCase("b5") || paper.equalsIgnoreCase("b5paper")) return MediaSizeName.ISO_B5;
    if(paper.equalsIgnoreCase("b6") || paper.equalsIgnoreCase("b6paper")) return MediaSizeName.ISO_B6;
    if(paper.equalsIgnoreCase("b7") || paper.equalsIgnoreCase("b7paper")) return MediaSizeName.ISO_B7;
    if(paper.equalsIgnoreCase("b8") || paper.equalsIgnoreCase("b8paper")) return MediaSizeName.ISO_B8;
    if(paper.equalsIgnoreCase("b9") || paper.equalsIgnoreCase("b9paper")) return MediaSizeName.ISO_B9;
    if(paper.equalsIgnoreCase("b10") || paper.equalsIgnoreCase("b10paper")) return MediaSizeName.ISO_B10;
    if(paper.equalsIgnoreCase("c0") || paper.equalsIgnoreCase("c0paper")) return MediaSizeName.ISO_C0;
    if(paper.equalsIgnoreCase("c1") || paper.equalsIgnoreCase("c1paper")) return MediaSizeName.ISO_C1;
    if(paper.equalsIgnoreCase("c2") || paper.equalsIgnoreCase("c2paper")) return MediaSizeName.ISO_C2;
    if(paper.equalsIgnoreCase("c3") || paper.equalsIgnoreCase("c3paper")) return MediaSizeName.ISO_C3;
    if(paper.equalsIgnoreCase("c4") || paper.equalsIgnoreCase("c4paper")) return MediaSizeName.ISO_C4;
    if(paper.equalsIgnoreCase("c5") || paper.equalsIgnoreCase("c5paper")) return MediaSizeName.ISO_C5;
    if(paper.equalsIgnoreCase("c6") || paper.equalsIgnoreCase("c6paper")) return MediaSizeName.ISO_C6;
    if(paper.equalsIgnoreCase("letter")) return MediaSizeName.NA_LETTER;
    if(paper.equalsIgnoreCase("legal")) return MediaSizeName.NA_LEGAL;
    
    //otherwise, do a case insensitive match on the variable name
    return Arrays.stream(MediaSizeName.class.getFields())
    .filter(f -> {
      int m = f.getModifiers();
      return Modifier.isStatic(m) && Modifier.isFinal(m) && Modifier.isPublic(m)
          && f.getType().equals(MediaSizeName.class);
    })
    .filter(f -> f.getName().equalsIgnoreCase(paper))
    .flatMap(f -> { //Flatmap to allow empty return in case of an error
      try {
        MediaSizeName msn = (MediaSizeName) f.get(null);
        return Stream.of(msn);
      } catch(IllegalAccessException | ClassCastException e) {
        return Stream.empty();
      }
    })
    .findAny().orElseThrow(() -> new OptionException("Value " + paper + " is not recognized as a paper size", OptionException.EC_INVALID_OPTION_VALUE));
  }
  
  
  private static int parsePosInt(String command, String value, boolean allowZero) throws OptionException {
    try {
      int i = Integer.parseInt(value);
      if(i < 0 || (!allowZero && i == 0)) 
        throw new OptionException("Option '" + command + "' requires a positive integer number as value, not '" + i + "'", OptionException.EC_INVALID_OPTION_VALUE);
      else return i;
    } catch(NumberFormatException e) {
      throw new OptionException("Option '" + command + "' requires an integer as value, not '" + value + "'", e, OptionException.EC_INVALID_OPTION_VALUE);
    }
  }
  
  private static double parsePosDouble(String command, String value, boolean allowZero) throws OptionException {
    try {
      double d = Double.parseDouble(value);
      if(d < 0 || (!allowZero && d == 0)) 
        throw new OptionException("Option '" + command + "' requires a positive number as value, not '" + d + "'", OptionException.EC_INVALID_OPTION_VALUE);
      else return d;
    } catch(NumberFormatException e) {
      throw new OptionException("Option '" + command + "' requires a number as value, not '" + value + "'", e, OptionException.EC_INVALID_OPTION_VALUE);
    }
  }
  
  private static int parsePosInt(String command, List<String> values, boolean allowZero) throws OptionException {
    if(values.size() != 1) throw new OptionException("Option '" + command + "' requires exactly one value", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
    return parsePosInt(command, values.get(0), allowZero);
  }
  
  private static Path parsePath(String command, List<String> values, boolean hasToExist) throws OptionException {
    if(values.size() != 1) throw new OptionException("Option '" + command + "' requires exactly one value", OptionException.EC_WRONG_OPTION_VALUE_COUNT);
    Path path = Paths.get(values.get(0)).toAbsolutePath();
    if(hasToExist) {
      if(!Files.exists(path)) throw new OptionException("File '" + path.toString() + "' does not exist", OptionException.EC_FILE_NOT_FOUND);
    }
    return path;
  }
  
  private boolean hasExportRelatedOption() {
    return exportDest != null || exportExt != null || blackwhite
        || exportFormat != null || imageSize != null || paperName != null
        || physicalSizeUnit != null || physicalSize != null || transparent;
  }
  
  private boolean doMultiExport() {
    return qgrafDiagramIndex == QgrafOutputFile.LOAD_ALL_INDEX_VALUE 
        && importQgraf != null
        && exportDest != null;
  }
  
  private String getExportFormat() {
    if(exportExt != null) return exportExt;
    if(exportDest != null) return ExportManager.getFileExtension(exportDest);
    return null;
  }
  
  public static OptionParser parse(String[] args) throws OptionException {
    OptionParser ap = new OptionParser(args);
    ap.doParse();
    return ap;
  }
}
