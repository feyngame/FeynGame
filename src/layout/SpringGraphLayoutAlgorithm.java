package layout;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jgrapht.Graph;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;

import qgraf.QgrafDiagram.GraphVertex;
import ui.Frame;
import ui.Settings;

/**
 * A spring-based force directed graph drawing algorithm.
 * Implementing subclasses must provide the spring forces.
 * 
 * @author Lars Bündgen
 */
public abstract class SpringGraphLayoutAlgorithm extends SimpleGraphLayoutAlgorithm {
  private static final Logger LOGGER = Frame.getLogger(SpringGraphLayoutAlgorithm.class);
  
  protected static final Random RANDOM = new Random(0);
  protected static final double DISPLACEMENT_SCALE = 0.01;
  
  protected int iterationCount;
  protected double initialTemp;
  protected double lengthScale;
  protected TemperatureModels temperature = TemperatureModels.LINEAR;
  
  private final boolean enforceDrawArea;
  
  protected SpringGraphLayoutAlgorithm(String name, String description, boolean enforceDrawArea) {
    super(name, description);
    this.enforceDrawArea = enforceDrawArea;
  }

  @Override
  public List<JComponent> createOptions(Consumer<Runnable> onFocusGained) {
    List<JComponent> list = super.createOptions(onFocusGained);
    JPanel p = new JPanel(new GridBagLayout());
    p.setBorder(BorderFactory.createTitledBorder("Spring layout simulation"));

    p.add(new JLabel("Iteration count:"), Settings.gbc(0, 0, 1, GridBagConstraints.HORIZONTAL));
    p.add(Settings.createIntegerInput(d -> d > 0, v -> iterationCount = v,
        () -> iterationCount, onFocusGained, 150), Settings.gbc(1, 0, 1, GridBagConstraints.HORIZONTAL));
    
    p.add(new JLabel("Step size:"), Settings.gbc(0, 1, 1, GridBagConstraints.HORIZONTAL));
    p.add(Settings.createDoubleInput(d -> d > 0.0, v -> initialTemp = v,
        () -> initialTemp, onFocusGained, 150), Settings.gbc(1, 1, 1, GridBagConstraints.HORIZONTAL));
    
    p.add(new JLabel("Length parameter:"), Settings.gbc(0, 2, 1, GridBagConstraints.HORIZONTAL));
    p.add(Settings.createDoubleInput(d -> d > 0.0, v ->  {Frame.frame.edgeLengthButtonGroup.clearSelection(); lengthScale = v;},
        () -> lengthScale, onFocusGained, 150), Settings.gbc(1, 2, 1, GridBagConstraints.HORIZONTAL));
    
    list.add(p);
    return list;
  }

  /**
   * Calculates a small random displacement vector that will be applied to a vertex if it is exactly on top of another vertex
   * @return
   */
  protected Vector2D calculateDisplacement(double charLen) {
    double t = RANDOM.nextDouble()*2.0*Math.PI;
    double dx =  DISPLACEMENT_SCALE * charLen * Math.cos(t);
    double dy =  DISPLACEMENT_SCALE * charLen * Math.sin(t);
    return new Vector2D(dx, dy);
  }

  /**
   * Calculate the force between two vertices, depending on whether they are connected by an edge.
   * The resulting number is the mangitude of the force; positive values are attractive. 
   * @param distance The distance between the vertices
   * @param idealLength The ideal length of an edge
   * @param connected Whether the two vertices are connected
   * @return The magnitude of the force between the vertices
   */
  public abstract double calculateVertexForce(double distance, double idealLength, boolean connected);
  
  /**
   * Calculate an external force that is applied to all vertices.
   * @param vertexType The vertex for which the force is calculated
   * @param location The position of the vertex
   * @return The external force
   */
  public abstract Vector2D calculateExternalForce(GraphVertex vertexType, Point2D location);
  
  /**
   * Called every iteration step before the vertices are moved.
   * @param graph The graph.
   * @param model The model in its current state.
   * @param n The index of the iteration step.
   */
  public abstract void onIteration(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model, int n);
  
  /**
   * Called after the last iteration step has finished
   * @param graph The graph.
   * @param model The model in its current state.
   */
  public abstract void onFinished(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model);
  
  /**
   * Called before the first iteration step begins.
   * @param graph The graph.
   * @param model The model in its current state.
   */
  public abstract void onStarted(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model);

  protected Vector2D calculateForce(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model,
      GraphVertex vertex, double idealLength, double charLength) {
    if(graph.getType().isDirected()) throw new IllegalArgumentException("Directed graph");
    if(!graph.getType().isSimple()) throw new IllegalArgumentException("Not a simple graph");
    
    Point2D start = model.get(vertex);
    Vector2D totalForce = new Vector2D();
    for(GraphVertex v : graph.vertexSet()) {
      if(v == vertex) continue;
      
      Point2D end = model.get(v);
      Vector2D diff = Vector2D.difference(start, end);
      
      if(diff.length() < DISPLACEMENT_SCALE * charLength) { //If vertices are too close together
        end = calculateDisplacement(charLength).movePoint(end, 1);
        diff = Vector2D.difference(start, end);
      }
      
      GraphMultiEdge edge = graph.getEdge(vertex, v);
      
      double ideal = (strongerMultiEdges && edge  != null) ? idealLength /(double)edge.getEdgeCount() : idealLength; 
      double forceLength = calculateVertexForce(diff.length(), ideal, edge != null);
      if(edge != null && strongerMultiEdges) forceLength *= edge.getEdgeCount(); 
      Vector2D force = Vector2D.aligned(forceLength, diff);
      force.fixNaN(); //In case the algorithm divides by 0, vertex will just remain where it is
      totalForce.add(force);
    }
    
    Vector2D e = calculateExternalForce(vertex, start);
    e.scale(charLength);
    totalForce.add(e);
    
    totalForce.fixNaN();
    return totalForce;
  }
  
  @Override
  public final void simpleLayout(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model) {
    
    double area = model.getDrawableArea().getWidth() * model.getDrawableArea().getHeight();
    double vc = graph.vertexSet().size();
    double charSize = Math.sqrt(area); //characteristic length that does not depend on lengthScale or graph
    double idealLength = lengthScale * Math.sqrt(area / vc);
    
    //Also fix NaNs on incoming positions
    for(Map.Entry<GraphVertex, Point2D> vert : new HashMap<>(model.collect()).entrySet()) {
      if(Double.isNaN(vert.getValue().getX())) {
        LOGGER.fine("x was NaN");
        model.put(vert.getKey(), new Point2D(0, vert.getValue().getY()));
      }
      
      if(Double.isNaN(vert.getValue().getY())) {
        LOGGER.fine("y was NaN");
        model.put(vert.getKey(), new Point2D(vert.getValue().getX(), 0));
      }
      
    }
    
    onStarted(graph, model);
    //do N iterations
    final Map<GraphVertex, Vector2D> forceMap = new HashMap<>();
    for(int i = 0; i < iterationCount; i++) {
      onIteration(graph, model, i);
      
      forceMap.clear();
      //Calculate all forces
      for(GraphVertex v : graph.vertexSet()) {
        forceMap.put(v, calculateForce(graph, model, v, idealLength, charSize));
      }
      
      double temperatureStep = temperature.temperature(i, iterationCount) * initialTemp * charSize; 
      
      //Then apply the forces
      for(GraphVertex v : graph.vertexSet()) {
        Vector2D force = forceMap.get(v);
        Point2D oldPoint = model.get(v);
        double len = force.length();
        
        if(len == 0) continue; //If no force, don't move
        
        double fixedDisp = Math.min(len, temperatureStep); //Forces have unit length! no scaling necessary
        Point2D newPoint = force.movePoint(oldPoint, fixedDisp / len);
        
        if(enforceDrawArea) {
          double x = Math.min(model.getDrawableArea().getMaxX(), Math.max(model.getDrawableArea().getMinX(), newPoint.getX()));
          double y = Math.min(model.getDrawableArea().getMaxY(), Math.max(model.getDrawableArea().getMinY(), newPoint.getY()));
          model.put(v, new Point2D(x, y));
        } else {
          model.put(v, newPoint);
        }
      }
    }
    onFinished(graph, model);
  }
  
  @Override
  public boolean requiresInitialLayout() {
    return true;
  }
  
}
