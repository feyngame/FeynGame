package layout;

import java.util.Objects;
import java.util.logging.Logger;

import org.jgrapht.alg.drawing.model.Point2D;

import ui.Frame;

/**
 * Class to represent a 2D vector with two {@code double} precision cartesian components.
 * 
 * @author Lars Bündgen
 */
public final class Vector2D {
  private static final Logger LOGGER = Frame.getLogger(Vector2D.class);

  /**
   * The x-coordinate of this vector.
   */
  public double x;
  /**
   * The y-coordinate of this vector.
   */
  public double y;

  /**
   * Create a new vector with components {@code (0, 0)}.
   */
  public Vector2D() {
    this.x = 0;
    this.y = 0;
  }

  /**
   * Creates a new vector.
   * @param x The value of the x-coordinate.
   * @param y The value of the y-coordinate.
   */
  public Vector2D(double x, double y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Copy constructor. Creates a new vector with the same components as the parameter.
   * @param vector The vector to copy.
   */
  public Vector2D(Vector2D vector) {
    this.x = vector.x;
    this.y = vector.y;
  }

  /**
   * Returns the length of the vector.
   * @return The length of the vector.
   */
  public double length() {
    return Math.sqrt(x*x + y*y);
  }

  /**
   * Returns the square of the length of the vector.
   * @return The square of the length of the vector.
   */
  public double lengthSq() {
    return x*x + y*y;
  }

  /**
   * Add the other vector to this vector. The vector components are added. The other vector's components are not modified.
   * @param other The vector to add to this vector.
   */
  public void add(Vector2D other) {
    this.x += other.x;
    this.y += other.y;
  }
  
  /**
   * Scales this vector by a factor. The vector components are multiplied by the factor.
   * @param factor The factor to scale by.
   */
  public void scale(double factor) {
    this.x *= factor;
    this.y *= factor;
  }

  /**
   * Translates a point by moving it with the direction and magnitude of this vector times a scale factor.
   * @param initial The initial position of the point. This object is not modified.
   * @param scale The scale parameter that is applied to the moved distance.
   * @return The translated point.
   */
  public Point2D movePoint(Point2D initial, double scale) {
    return new Point2D(initial.getX() + scale*x, initial.getY() + scale*y);
  }

  /**
   * Creates a vector with direction and magnitude.
   * @param size The length of the vector.
   * @param direction The direction of the vector. Is not required to be a unit vector.
   * @return A vector with the desired length and direction.
   * @throws IllegalArgumentException If the direction vector has length zero.
   */
  public static Vector2D aligned(double size, Vector2D direction) {
    if(direction.lengthSq() == 0) return new Vector2D(0, 0);
    double scale = size / direction.length();
    return new Vector2D(direction.x * scale, direction.y * scale);
  }

  /**
   * Creates a vector that points from the first point to the second.
   * @param start The first point.
   * @param end The second point.
   * @return A vector that points from {@code start} to {@code end}.
   */
  public static Vector2D difference(Point2D start, Point2D end) {
    return new Vector2D(end.getX() - start.getX(), end.getY() - start.getY());
  }
  
  /**
   * Calculates the distance between two points.
   * @param start The first point.
   * @param end The second point.
   * @return The distance between the two points.
   */
  public static double distance(Point2D start, Point2D end) {
    double x = end.getX() - start.getX();
    double y = end.getY() - start.getY();
    return Math.sqrt(x*x + y*y);
  }

  /**
   * Sets any component that is not finite to zero and logs an error message.
   */
  public void fixNaN() {
    if(!Double.isFinite(x)) {
      LOGGER.fine("x-component was NaN");
      x = 0;
    }
    if(!Double.isFinite(y)) {
      LOGGER.fine("y-component was NaN");
      y = 0;
    }
  }

  @Override
  public String toString() {
    return "[" + x + ", " + y + "]";
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Vector2D)) {
      return false;
    }
    Vector2D other = (Vector2D) obj;
    return Double.doubleToLongBits(x) == Double.doubleToLongBits(other.x)
        && Double.doubleToLongBits(y) == Double.doubleToLongBits(other.y);
  }
  
}