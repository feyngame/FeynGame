package layout;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.jgrapht.Graph;
import org.jgrapht.alg.drawing.LayoutAlgorithm2D;
import org.jgrapht.alg.drawing.model.Box2D;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;

import qgraf.ModelData;
import qgraf.QgrafDiagram.GraphEdge;
import qgraf.QgrafDiagram.GraphVertex;
import resources.Java8Support;
import ui.Frame;
import ui.Settings;

/**
 * A graph layout algorithm can be applied to a graph and calculates a position for every vertex in the graph.
 * Some layout algorithm require an initial vertex layout to be present.
 * <p>
 * This class also contains many static utility and configuration methods that relate to the graph layout process.
 * 
 * @author Lars Bündgen
 */
public abstract class GraphLayoutAlgorithm implements LayoutAlgorithm2D<GraphVertex, GraphEdge> {
  private static final Logger LOGGER = Frame.getLogger(GraphLayoutAlgorithm.class);
  
  private static final SpringGraphLayoutAlgorithm INSTANCE 
    = new FruchtermanReingoldSpringLayoutAlgorithm("Spring layout", "Spring layout"); //Keep this as a fallback
  private static final GraphLayoutAlgorithm EXT_INSTANCE_DISTANCE 
    = new ExternalLineDistanceLayoutAlgorithm("New external layout", "New external layout", false, false);
  private static final GraphLayoutAlgorithm EXT_INSTANCE_SPRING 
  = new ExternalLineSpringLayoutAlgorithm("External layout", "External layout", false, false);
  private static final GraphLayoutAlgorithm EXT_INSTANCE_DISTANCE_PRE 
  = new ExternalLineDistanceLayoutAlgorithm("New external layout", "New external layout", true, false);
  private static final GraphLayoutAlgorithm EXT_INSTANCE_SPRING_PRE 
  = new ExternalLineSpringLayoutAlgorithm("External layout", "External layout", true, false);
  private static final GraphLayoutAlgorithm EXT_ORDERED
  = new ExternalOrderLayoutAlgorithm("External layout (ordered)", "External layout (ordered)");
  
  private static final List<GraphLayoutAlgorithm> presets = new ArrayList<>();
  private static int selectedPresetIndex;
  
  /**
   * Returns the layout algorithm that should be applied to graphs by default.
   * @return The layout algorithm that should be applied to graphs by default.
   */
  public static GraphLayoutAlgorithm getLayoutAlgorithm() {
    if(selectedPresetIndex >= 0 && selectedPresetIndex < presets.size()) {
      return presets.get(selectedPresetIndex);
    } else {
      return INSTANCE;
    }
  }
  
  public static Collection<GraphLayoutAlgorithm> getLayoutAlgorithms() {
    return presets;
  }
  
  public static void resetLayoutPresets() {
    presets.clear();
    presets.addAll(getDefaultLayoutPresets());
    updatePresetMenus();
  }
  
  public static void setSelectedPresetIndex(int index) {
    if(index >= 0 && index < presets.size()) {
      selectedPresetIndex = index;
      LOGGER.finest("Switched selected preset index to " + index);
    } else {
      LOGGER.fine("Tried to set invalid preset index " + index + " (" + presets.size() + " presets available)");
    }
    updatePresetMenus();
  }
  
  public static Collection<GraphLayoutAlgorithm> getDefaultLayoutPresets() {
    FruchtermanReingoldSpringLayoutAlgorithm normalEdge = new FruchtermanReingoldSpringLayoutAlgorithm("Default", "Spring layout with default parameters");
    normalEdge.lengthScale = 0.4;
    FruchtermanReingoldSpringLayoutAlgorithm shortEdge  = new FruchtermanReingoldSpringLayoutAlgorithm("Shorter lines", "Spring layout with shorter edge length");
    shortEdge.lengthScale = 0.1;
    FruchtermanReingoldSpringLayoutAlgorithm longEdge   = new FruchtermanReingoldSpringLayoutAlgorithm("Longer lines", "Spring layout with longer edge length");
    longEdge.lengthScale = 0.7;
    return Java8Support.listOf(normalEdge, shortEdge, longEdge);
  }

  public static void updatePresetMenus() {
    if(presets.isEmpty()) {
      presets.addAll(getDefaultLayoutPresets());
      selectedPresetIndex = 0;
    } else if(selectedPresetIndex > presets.size() - 1) {
      selectedPresetIndex = presets.size() - 1;
    }
    Frame.frame.updateLayoutPresetsMenu(selectedPresetIndex, presets);
    
    if(Frame.frame.settingsFrame != null) {
      Frame.frame.settingsFrame.updatePresets();
    }
  }
  
  public static void addNewPreset(String name) {
    GraphLayoutAlgorithm np = new FruchtermanReingoldSpringLayoutAlgorithm(name, "Spring layout with custom parameters");
    presets.add(np);
    selectedPresetIndex = presets.size() - 1;
    updatePresetMenus();
  }
  
  public static void removeCurrentPreset() {
    if(presets.size() <= 1) return;
    presets.remove(presets.get(selectedPresetIndex));
    if(selectedPresetIndex >= presets.size())  selectedPresetIndex = 0;
    updatePresetMenus();
  }
  
  public static int getSelectedPresetIndex() {
    return selectedPresetIndex;
  }
  
  /**
   * 0 is keep
   * 1 is reorder spring
   * 2 is reorder distance
   * 3 is reorder spring fix out
   * 4 is reorder distance fix out
   */
  private static int externalAlgorithm = 0;
  
  
  /**
   * Returns the graph layout algorithm that should be applied to calculate the vertical order of external legs.
   * @return The graph layout algorithm that should be applied to calculate the vertical order of external legs.
   */
  public static GraphLayoutAlgorithm getPreLayoutAlgorithm() {
    switch (externalAlgorithm) {
    case 0:
      return EXT_ORDERED;
    case 1:
      return EXT_INSTANCE_SPRING;
    case 2:
      return EXT_INSTANCE_DISTANCE;
    case 3:
      return EXT_INSTANCE_SPRING_PRE;
    case 4:
      return EXT_INSTANCE_DISTANCE_PRE;
    default:
      return null;
    }
  }
  
  public static GraphLayoutAlgorithm getPostLayoutAlgorithm() {
    switch (externalAlgorithm) {
    case 3:
    case 4:
      return EXT_ORDERED;
    default:
      return null;
    }
  }
  
  public static int getSelectedLayoutAlgorithm() {
    return externalAlgorithm;
  }
  
  public static void setSelectedLayoutAlgorthm(int value) {
    if(value < 0 || value > 4) {
      LOGGER.warning("invalid value for layout algorithm");
      externalAlgorithm = 0;
    } else {
      externalAlgorithm = value;
    }
  }
  
  //These are dummy items, they are used to hold the state when in inFin mode
  //They are replaced with #putCheckboxes in drawing mode
//  private static JCheckBoxMenuItem rectangle = new JCheckBoxMenuItem(); 
//  private static JCheckBoxMenuItem reorder = new JCheckBoxMenuItem();
  private static JRadioButton currentHorizontalButton;
  private static boolean flip;
  private static boolean distAlg;
  
  /**
   * Resets the diagram orientation (vertical vs horizontal time axis) to the default value,
   * which is time increasing from left to right.
   */
  public static void setDefaultOrientation() {
    flip = false;
    currentHorizontalButton.setSelected(true);
  }

  /**
   * Set whether to use the graph-distance based algotithm for determining external leg order.
   * @param value The new value. {@code true} will use the graph-distance algorithm, {@code false}
   * will use the the spring layout based algorithm.
   */
  public static void useDistanceAlgorithm(boolean value) {
    distAlg = value;
  }
  
  /**
   * Returns {@code true} if the time axis goes bottom to top, {@code false} if it goes left to right.
   * @return {@code true} if the time axis goes bottom to top, {@code false} if it goes left to right.
   */
  public static boolean flipDiagramOrientation() {
    return flip;
  }
  
//  private static boolean keepLineOrder = true;
//  
//  public static boolean keepLineOrder() {
//    return keepLineOrder;
//  }
//  
//  public static void keepLineOrder(boolean value) {
//    keepLineOrder = value;
//  }
  
  private static boolean gridAutoLayout = true;
  
  public static boolean gridAutoLayout() {
    return gridAutoLayout;
  }
  
  public static void gridAutoLayout(boolean value) {
    gridAutoLayout = value;
  }
  
  /**
   * Returns whether the layout rectangle should be used for layout calculation.
   * @return whether the layout rectangle should be used for layout calculation.
   */
  public static boolean useRectangleForBounds() {
    return Frame.frame.getDrawPane().mBB;
  }

  public static String saveStaticParameters() {
    return String.join(";", 
        Boolean.toString(distAlg),
        Boolean.toString(flip));
  }
  
  public static void loadStaticParameters(String value) {
    String[] values = value.split(";");
    if(values.length != 2) return; //Fall back to defaults
    distAlg = Boolean.parseBoolean(values[0]);
    flip = Boolean.parseBoolean(values[1]);
  }
  
  /**
   * @param presetsString The first element is the name of the config option; NOT a preset string
   */
  public static void loadPresets(String[] presetsString) {
    presets.clear();
    if(presetsString.length == 1) { //NO preset, only the name of the settings option
      presets.addAll(getDefaultLayoutPresets());
    } else {
      for(int i = 1; i < presetsString.length; i++) {
        GraphLayoutAlgorithm gla = new FruchtermanReingoldSpringLayoutAlgorithm("Preset " + i, "Spring layout algorithm for preset " + i);
        gla.loadParameters(presetsString[i]);
        presets.add(gla);
      }
    }
  }
  
  public static String[] savePresets() {
    return presets.stream()
      .map(GraphLayoutAlgorithm::saveParameters)
      .toArray(String[]::new);
  }
  
  /**
   * Scales and translates all points in a layout model from one box to another box. The layout model in the parameter will
   * contain the new vertex positions after this method returns.
   * <p>
   * The points are modified by the affine tranformation (translation and scaling only) that maps the old box
   * onto the new box. This means that points that start outside the old box they will end up outside the new box as well.
   * @param <T> The vertex type in the layout model.
   * @param model The layout model.
   * @param oldBox The old box that the old posiitons are relative to.
   * @param newBox The new box that the positions will be scaled and translated to.
   * @throws NullPointerException If any parameter is {@code null}.
   * @see #scaledTranslate(Point2D, Box2D, Box2D) Single point version of this method.
   */
  public static <T> void scaledTranslate(LayoutModel2D<T> model, Box2D oldBox, Box2D newBox) {
    //Check these to throw even if the model is empty
    Objects.requireNonNull(oldBox);
    Objects.requireNonNull(newBox);
    
    for(T v : model.collect().keySet()) {
      Point2D oldPos = model.get(v);
      Point2D newPos = scaledTranslate(oldPos, oldBox, newBox);
      model.put(v, newPos);
    }
  }
  
  /**
   * Scales and translates a point from one box to another box.
   * <p>
   * The point is modified by the affine tranformation (translation and scaling only) that maps the old box
   * onto the new box.
   * @param old The old point.
   * @param oldBox The old box that the old posiitons are relative to.
   * @param newBox The new box that the positions will be scaled and translated to.
   * @return The translated point.
   * @throws NullPointerException If any parameter is {@code null}.
   * @see #scaledTranslate(Point2D, Box2D, Box2D) Single point version of this method.
   */
  public static Point2D scaledTranslate(Point2D old, Box2D oldBox, Box2D newBox) {
    double wscale = newBox.getWidth() / oldBox.getWidth();
    double hscale = newBox.getHeight() / oldBox.getHeight();
    double offX = newBox.getMinX() - wscale * oldBox.getMinX();
    double offY = newBox.getMinY() - hscale * oldBox.getMinY();
    
    return new Point2D(wscale * old.getX() + offX, hscale * old.getY() + offY);
  }
  
  public static List<JComponent> createGeneralOptions(Consumer<Runnable> onFocusGained) {
    List<JComponent> list = new ArrayList<>();

    JPanel general = new JPanel(new GridBagLayout());
    general.setBorder(BorderFactory.createTitledBorder("General options"));
    
//    general.add(new JLabel("Resize mode:"), Settings.gbc(0, 0, 1, GridBagConstraints.HORIZONTAL));
//    
//    JComboBox<GraphScaleMode> scaleModeBox = new JComboBox<>(GraphScaleMode.displayValues());
//    scaleModeBox.setPreferredSize(new Dimension(120, scaleModeBox.getPreferredSize().height));
//    scaleModeBox.addActionListener(e -> {
//      GraphScaleMode.setScaleMode((GraphScaleMode) scaleModeBox.getSelectedItem());
//    });
//    onFocusGained.accept(() -> scaleModeBox.setSelectedItem(GraphScaleMode.getScaleMode()));
//    
//    general.add(scaleModeBox, Settings.gbc(1, 0, 1, GridBagConstraints.HORIZONTAL));
//    
    general.add(new JLabel("Border size:"), Settings.gbc(0, 0, 1, GridBagConstraints.HORIZONTAL));
    general.add(Settings.createDoubleInput(d -> d >= 0.0 && d < 0.5, GraphLayoutAlgorithm.getLayoutAlgorithm()::setBorderSize,
        GraphLayoutAlgorithm.getLayoutAlgorithm()::getBorderSize, onFocusGained, 150), Settings.gbc(1, 0, 1, GridBagConstraints.HORIZONTAL));
    
//    general.add(Settings.createBooleanInput("Always randomize layout", GraphLayoutAlgorithm::alwaysRandomizeLayout,
//        GraphLayoutAlgorithm::alwaysRandomizeLayout, onFocusGained, 150), Settings.gbc(0, 1, 2, GridBagConstraints.HORIZONTAL));
//    general.add(Settings.createBooleanInput("Reorder exteral lines", GraphLayoutAlgorithm::alwaysReorderExternals,
//        GraphLayoutAlgorithm::alwaysReorderExternals, onFocusGained, 150), Settings.gbc(0, 2, 2, GridBagConstraints.HORIZONTAL));
//    general.add(Settings.createBooleanInput("Use rectangle as bounds", GraphLayoutAlgorithm::useRectangleForBounds,
//        GraphLayoutAlgorithm::useRectangleForBounds, onFocusGained, 150), Settings.gbc(0, 3, 2, GridBagConstraints.HORIZONTAL));
    
    ButtonGroup bg = new ButtonGroup();
    JRadioButton normal  = new JRadioButton("Horizontal time axis", !flip);
    JRadioButton flipped = new JRadioButton("Vertical time axis", flip);
    bg.add(normal);
    bg.add(flipped);
    
    normal.addActionListener(e -> flip = !normal.isSelected());
    flipped.addActionListener(e -> flip = flipped.isSelected());
    
//    general.add(new JLabel("Select orientation of time axis"),
//        Settings.gbc(0, 4, 2, GridBagConstraints.HORIZONTAL));
    general.add(normal, Settings.gbc(0, 4, 1, GridBagConstraints.HORIZONTAL));
    general.add(flipped, Settings.gbc(1, 4, 1, GridBagConstraints.HORIZONTAL));
    currentHorizontalButton = normal;
 
    JComboBox<String> box = new JComboBox<>(new String[] {"Spring-Based", "Distance-Based"});
    box.setSelectedIndex(distAlg ? 1 : 0);
    box.setEditable(false);
    box.addActionListener(e -> {
      GraphLayoutAlgorithm.useDistanceAlgorithm(box.getSelectedIndex() == 1);
    });
    
    general.add(new JLabel("External vertex order:"), Settings.gbc(0, 5, 1, GridBagConstraints.HORIZONTAL));
    general.add(box, Settings.gbc(1, 5, 1, GridBagConstraints.HORIZONTAL));
    
    list.add(general);
    
    JPanel unknown = new JPanel(new GridBagLayout());
    unknown.setBorder(BorderFactory.createTitledBorder("Unknown particles"));
    
    unknown.add(Settings.createBooleanInput("Create dashed lines for unknown particles", ModelData::setDashed,
        ModelData::getDashed, onFocusGained, 250), Settings.gbc(0, 1, 2, GridBagConstraints.HORIZONTAL));
    
    list.add(unknown);
    return list;
  }
  
  
  protected String name;
  private final String description;
  protected double relativeBorder;
  
  protected GraphLayoutAlgorithm(String name, String description) {
    this.name = name;
    this.description = description;
  }
  
  /**
   * Returns {@code true} if an existing vertex layout, called the initial layout, must be present before this layout
   * algorithm can be applied. Returns {@code false} if an initial layout is not required. 
   * @return {@code true} if an existing vertex layout must be present before this layout
   * algorithm can be applied, {@code false} if an initial layout is not required. 
   */
  public abstract boolean requiresInitialLayout();
  
  @Override
  public abstract void layout(Graph<GraphVertex, GraphEdge> graph, LayoutModel2D<GraphVertex> model);
  
  /**
   * Creates the components that should be displayed in the layout settings window.
   * <p>
   * The elements in the returned list are arranged vertically in the layout window. It is expected that each item
   * is a {@link JPanel} with a titled border that gives a category name to the components in the panel.
   * <p>
   * If this method is overridden in a subclass, this implementation should call the super implementation and then append
   * to the returned list.
   * @param onFocusGained A destination for tasks that should be run whenever the settings window regains focus.
   * @return A list of components that will be added to the layout window. Must be mutable.
   */
  public List<JComponent> createOptions(Consumer<Runnable> onFocusGained) {
    return new ArrayList<>();
  }
  
  /**
   * A displayable name of this layout algorithm.
   * @return The name of this algorithm.
   */
  public final String getName() {
    return name;
  }
  
  /**
   * A short description of this layout algorithm.
   * @return The description for this algorithm.
   */
  public final String getDescription() {
    return description;
  }
  
  /**
   * Returns the relative size of the border around the diagram, as a fraction of the width or height.
   * @return The relative size of the border around the diagram.
   */
  public double getBorderSize() {
    return relativeBorder;
  }
  
  /**
   * Sets the relative size of the border around the diagram, as a fraction of the width or height.
   * @param value The new value. It should be in the range {@code [0, 0.5)}, but this is not enforced by this method.
   */
  public void setBorderSize(double value) {
    relativeBorder = value;
  }
  
  /**
   * Reset all parameters of this graph layout algorithm to their default values.
   * The default values should not be all zeroes; they should be chosen such that the algotithm
   * works well in most cases.
   */
  public abstract void setDefaultParameters();
  
  /**
   * Load the parameters of this layout algorithm from a string.
   * @param data The encoded parameter string.
   */
  public abstract void loadParameters(String data);
  
  /**
   * Returns a string that encodes the parameters of this layout algorithm.
   * @return A string that encodes the parameters of this layout algorithm.
   */
  public abstract String saveParameters();
  
  public final String toString() {
    return name;
  }
  
  /**
   * Calculates the smallest box that contains all points in the layout model. 
   * The bounding box in the model has no effect on the result.
   * <p>
   * If the height or width of the box is smaller than a threshold value, they will be set to some arbitrary
   * finite value such that the points are located at the center of the box. This means that the returned box will never have
   * a width or height of zero. 
   * @param model The layout model.
   * @return The bounding box for this model.
   * @throws NullPointerException If the parameter is {@code null}.
   * @throws IllegalArgumentException If the layout model is empty.
   */
  protected static Box2D getCurrentModelBounds(LayoutModel2D<GraphVertex> model) {
    if(!model.iterator().hasNext()) throw new IllegalArgumentException(); //empty
    
    double minX = Double.MAX_VALUE;
    double maxX = Double.MIN_VALUE;
    double minY = Double.MAX_VALUE;
    double maxY = Double.MIN_VALUE;
    for(Map.Entry<GraphVertex, Point2D> entry : model) {
      minX = Math.min(minX, entry.getValue().getX());
      maxX = Math.max(maxX, entry.getValue().getX());
      minY = Math.min(minY, entry.getValue().getY());
      maxY = Math.max(maxY, entry.getValue().getY());
    }
    
    double width = maxX - minX;
    double height = maxY - minY;
    
    if(width < 0.001) { //Near zero
      width = 10;
      minX -= 5;
    }
    
    if(height < 0.001) {
      height = 10;
      minY -= 5;
    }
    
    return new Box2D(minX,  minY, width, height);
  }

  /**
   * Creates a smaller box that fits within the old box while leaving the specified border around the new box.
   * @param old The old, outer box.
   * @param relativeBorder The size of the border, as a fraction of the width and height of the old box.
   * @return The new, inner box.
   * @throws IllegalArgumentException If {@code relativeBorder} is not in the range {@code [0, 0.5)}.
   * @throws NullPointerException If {@code old} is {@code null}.
   */ 
  public static Box2D boxWithinBorder(Box2D old, double relativeBorder) {
    if(relativeBorder >= 0.5 || relativeBorder < 0.0) throw new IllegalArgumentException();
    double rx = relativeBorder * old.getWidth();
    double ry = relativeBorder * old.getHeight();
    
    return new Box2D(old.getMinX() + rx, old.getMinY() + ry, old.getWidth() - 2*rx, old.getHeight() - 2*ry);
  }
  
  /**
   * Moves all vertices from the current bounding box to the draw area of the model by applying
   * a scaled translation as  described in {@link #scaledTranslate(LayoutModel2D, Box2D, Box2D)}.
   * The new vertex positions will be stored in the same model.
   * @param model The model to modify.
   */
  public static void moveVerticesToDrawArea(LayoutModel2D<GraphVertex> model) {
    Box2D oldBox = getCurrentModelBounds(model);
    Box2D newBox = model.getDrawableArea();
    scaledTranslate(model, oldBox, newBox);
  }
  
}
