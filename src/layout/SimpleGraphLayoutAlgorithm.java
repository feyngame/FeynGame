package layout;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import qgraf.QgrafDiagram.GraphEdge;
import qgraf.QgrafDiagram.GraphVertex;
import ui.Settings;

/**
 * Allows easy implementation of layout algorithms that operate on simple graphs (without multi-edges or self-loops).
 * 
 * @author Lars Bündgen
 */
public abstract class SimpleGraphLayoutAlgorithm extends GraphLayoutAlgorithm {

  /**
   * Stores a {@link GraphEdge} and a {@code boolean} value. 
   * @author Lars Bündgen
   */
  protected static class GraphDirectedEdge implements Comparable<GraphDirectedEdge> {
    private final GraphEdge edge;
    private final boolean invert;
    
    public GraphDirectedEdge(GraphEdge edge, boolean invert) {
      this.edge = Objects.requireNonNull(edge);
      this.invert = invert;
    }

    @Override
    public int compareTo(GraphDirectedEdge o) {
      //Compare by id first
      String  id =   invert ?   edge.getDualIdentifier() :   edge.getIdentifier();
      String oid = o.invert ? o.edge.getDualIdentifier() : o.edge.getIdentifier();
      int c = id.compareTo(oid);
      return c == 0 ? Boolean.compare(invert, o.invert) : c;
    }
  }
  
  /**
   * Represents one or more edges in the original graph as a single edge in the simple graph.
   * 
   * @author Lars Bündgen
   */
  protected static class GraphMultiEdge extends DefaultEdge {
    private static final long serialVersionUID = 2261452545801524351L;
    
    private final GraphDirectedEdge[] edges;
    
    /**
     * Creates a new instance, possibly by copying all edges from an existing instance and appending one.
     * @param old The old instance. May be {@code null}.
     * @param edge The new edge to add to the multi-edge.
     * @param invert Whether the orientation of the new edge is reversed compared to the other ones.
     * @throws NullPointerException If {@code edge} is {@code null}.
     */
    public GraphMultiEdge(GraphMultiEdge old, GraphEdge edge, boolean invert) {
      if(old == null) {
        this.edges = new GraphDirectedEdge[] { new GraphDirectedEdge(edge, invert) };
      } else {
        this.edges = Arrays.copyOf(old.edges, old.edges.length + 1);
        this.edges[old.edges.length] = new GraphDirectedEdge(edge, invert);
      }
    }
    
    /**
     * Returns the number of edges in the original graph that this edge represents.
     * @return The number of edges in the original graph that this edge represents.
     */
    public int getEdgeCount() {
      return edges.length;
    }
  }
  
  protected double selfLoopRelativeHeight;
  protected double multiEdgeRelativeHeight;
  protected boolean strongerMultiEdges;
  
  protected SimpleGraphLayoutAlgorithm(String name, String description) {
    super(name, description);
  }

  @Override
  public final void layout(Graph<GraphVertex, GraphEdge> graph, LayoutModel2D<GraphVertex> model) {
    Graph<GraphVertex, GraphMultiEdge> simple = new SimpleGraph<>(GraphMultiEdge.class);
    Map<GraphVertex, GraphMultiEdge> selfLoops = new HashMap<>();
    
    for(GraphVertex v : graph.vertexSet()) {
      simple.addVertex(v);
    }
    
    for(GraphEdge e : graph.edgeSet()) {
      GraphVertex v1 = graph.getEdgeSource(e);
      GraphVertex v2 = graph.getEdgeTarget(e);
      
      if(v1 == v2) { //self loop
        GraphMultiEdge me = selfLoops.get(v1);
        selfLoops.put(v1, new GraphMultiEdge(me, e, false));
      } else {
        GraphMultiEdge me = simple.getEdge(v1, v2); //simpleGraph is undirected, so order does not matter here
        if(me == null) {
          simple.addEdge(v1, v2, new GraphMultiEdge(me, e, e.getStyle().isReversed()));
        } else {
          GraphVertex start = simple.getEdgeSource(me);
          GraphVertex end = simple.getEdgeTarget(me);
          simple.removeEdge(me);
          simple.addEdge(start, end, new GraphMultiEdge(me, e, (v1 != start) ^ e.getStyle().isReversed()));
        }
      }
    }
    
    //Do the layout with the simple graph
    simpleLayout(simple, model);
    
    //Then fix the multi edges
    for(GraphMultiEdge gme : simple.edgeSet()) {
      if(gme.getEdgeCount() > 1) {
        Arrays.sort(gme.edges); //stable order of multi edges
        double gmeLength = Vector2D.distance(model.get(simple.getEdgeSource(gme)), model.get(simple.getEdgeTarget(gme)));
        
        for(int i = 0; i < gme.getEdgeCount(); i++) {
          double centeredHeight = (2.0 * (double)i) / ((double)gme.getEdgeCount() - 1.0) - 1.0;
          if(gme.edges[i].invert) centeredHeight *= -1;  
          double height = centeredHeight * multiEdgeRelativeHeight * gmeLength;
          gme.edges[i].edge.getStyle().setLineHeight(height);
        }
      } else if(gme.getEdgeCount() == 1) {
        gme.edges[0].edge.getStyle().setLineHeight(0);
      }
    }
    
    double area = model.getDrawableArea().getWidth() * model.getDrawableArea().getHeight();
    double charSize = Math.sqrt(area);
    
    //Fix the self loops
    for(GraphMultiEdge gme : selfLoops.values()) {
      if(gme.getEdgeCount() == 1) { //single loop
        GraphEdge e = gme.edges[0].edge;
        GraphVertex v = graph.getEdgeSource(e); //equals target
        
        //PLUS TODO stable sort of multi edges! by identifier, then by invert
        
        if(graph.degreeOf(v) == 3) {
          //Only one other, place opposite
          Set<GraphEdge> edges = new HashSet<>(graph.edgesOf(v));
          edges.remove(e);
          GraphEdge other = edges.iterator().next();
          GraphVertex farVertex = Graphs.getOppositeVertex(graph, other, v);
          Point2D p1 = model.get(v);
          Point2D p2 = model.get(farVertex);
          double angle = Math.atan2(p1.getY() - p2.getY(), p1.getX() - p2.getX()) - (Math.PI / 2.0);
          e.getStyle().setLoopAngle(angle); //TODO -angle?
        } else {
          e.getStyle().setLoopAngle(0);
        }
        e.getStyle().setLineHeight(selfLoopRelativeHeight * charSize);
      } else { //more than one self loop -> degree 5 vertex? -> place loops in whereever
        for(int i = 0; i < gme.getEdgeCount(); i++) {
          gme.edges[i].edge.getStyle().setLoopAngle(2.0 * Math.PI / (double)gme.getEdgeCount() * i);
          gme.edges[i].edge.getStyle().setLineHeight(selfLoopRelativeHeight * charSize); 
        }
      }
    }
  }

  @Override
  public List<JComponent> createOptions(Consumer<Runnable> onFocusGained) {
    List<JComponent> list = super.createOptions(onFocusGained);
    JPanel p = new JPanel(new GridBagLayout());
    p.setBorder(BorderFactory.createTitledBorder("Multi-edges and self-loops"));
    
    p.add(new JLabel("Self-loop radius scale:"), Settings.gbc(0, 0, 1, GridBagConstraints.HORIZONTAL));
    p.add(Settings.createDoubleInput(d -> d > 0.0, v -> selfLoopRelativeHeight = v,
        () -> selfLoopRelativeHeight, onFocusGained, 150), Settings.gbc(1, 0, 1, GridBagConstraints.HORIZONTAL));
    
    p.add(new JLabel("Multi-edge curve scale:"), Settings.gbc(0, 1, 1, GridBagConstraints.HORIZONTAL));
    p.add(Settings.createDoubleInput(d -> d >= 0.0, v -> multiEdgeRelativeHeight = v,
        () -> multiEdgeRelativeHeight, onFocusGained, 150), Settings.gbc(1, 1, 1, GridBagConstraints.HORIZONTAL));
    
    p.add(Settings.createBooleanInput("Stronger springs for multi-edges", v -> strongerMultiEdges = v, () -> strongerMultiEdges,
        onFocusGained, 150), Settings.gbc(0, 2, 2, GridBagConstraints.HORIZONTAL));
    
    list.add(p);
    return list;
  }

  /**
   * Layout a graph, which is guaranteed to be a simple graph.
   * @param graph The simple graph.
   * @param model The layout model that stores vertex position.
   */
  public abstract void simpleLayout(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model);
  
}
