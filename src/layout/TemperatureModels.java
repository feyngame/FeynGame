package layout;

import org.jgrapht.alg.drawing.FRLayoutAlgorithm2D.TemperatureModel;

/**
 * This class defines all {@link TemperatureModel}s that FeynGame knows about.
 * 
 * @author Lars Bündgen
 */
public enum TemperatureModels implements TemperatureModel {
  /**
   * A model with a constant temperature of {@code 0.05}.
   */
  CONSTANT() {
    @Override
    public double temperature(int n, int N) {
      return 0.05;
    }
  },
  /**
   * A model where the temperature decreases exponentially by a factor of {@code 0.9} each step.
   */
  EXPONENTIAL() {
    @Override
    public double temperature(int n, int N) {
      return Math.pow(0.9, n);
    }
  },
  /**
   * A model where the temperature decreases linearly from {@code 1} to {@code 0}.
   */
  LINEAR() {
    @Override
    public double temperature(int n, int N) {
      return 1 - ((double) n / (double)N );
    }
  };
}
