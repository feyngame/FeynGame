package layout;

/**
 * This exception type is thrown when the graph layout algotithm can not be successfully applied to a graph.
 * 
 * @author Lars Bündgen
 */
public class GraphLayoutException extends Exception {
  private static final long serialVersionUID = -3656942083799822827L;

  /**
   * Creates a new instance with an exception message.
   * @param message The exception message.
   */
  public GraphLayoutException(String message) {
    super(message);
  }

}
