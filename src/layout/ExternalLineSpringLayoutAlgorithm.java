package layout;

import org.jgrapht.Graph;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;

import qgraf.QgrafDiagram.GraphVertex;
import qgraf.QgrafDiagram.VertexType;

/**
 * A modified spring layout algorithm that is used to only arrange external lines.
 * 
 * @author Lars Bündgen
 */
public class ExternalLineSpringLayoutAlgorithm extends SpringGraphLayoutAlgorithm {

  private static final double FORCE_SCALE = 0.1;
  private static final double FORCE_STRENGTH = 1.0;
  
  private final boolean sortInLegs;
  private final boolean sortOutLegs;
  
  protected ExternalLineSpringLayoutAlgorithm(String name, String description, boolean sortInLegs, boolean sortOutLegs) {
    super(name, description, false);
    this.sortInLegs = sortInLegs;
    this.sortOutLegs = sortOutLegs;
    setDefaultParameters();
  }
  
  protected double attractiveForce(double distance, double length) {
    return + distance*distance / length;
  }
  
  protected double repulsiveForce(double distance, double length) {
    return - length * length / distance;
  }

  @Override
  public double calculateVertexForce(double l, double k, boolean c) {
    if(c) {
      return attractiveForce(l, k) + repulsiveForce(l, k);
    } else {
      return repulsiveForce(l, k) * FORCE_SCALE; //weaker repulsive forces
    }
  }
  
  @Override
  protected Vector2D calculateForce(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model,
      GraphVertex vertex, double idealLength, double charLength) {
    if(graph.getType().isDirected()) throw new IllegalArgumentException("Directed graph");
    if(!graph.getType().isSimple()) throw new IllegalArgumentException("Not a simple graph");
    
    Point2D start = model.get(vertex);
    Vector2D totalForce = new Vector2D();

    for(GraphVertex v : graph.vertexSet()) {
      if(v == vertex) continue;
      if(v.getType().isExternal() && vertex.getType() == v.getType()) continue; //same externals ignore each other
      
      Point2D end = model.get(v);
      Vector2D diff = Vector2D.difference(start, end);
      
      if(diff.length() < DISPLACEMENT_SCALE * charLength) { //If vertices are too close together
        end = calculateDisplacement(charLength).movePoint(end, 1);
        diff = Vector2D.difference(start, end);
      }
      
      GraphMultiEdge edge = graph.getEdge(vertex, v);
      
      double ideal = (strongerMultiEdges && edge  != null) ? idealLength /(double)edge.getEdgeCount() : idealLength; 
      double forceLength = calculateVertexForce(diff.length(), ideal, edge != null);
      if(edge != null && strongerMultiEdges) forceLength *= edge.getEdgeCount(); //TODO scale
      Vector2D force = Vector2D.aligned(forceLength, diff);
      force.fixNaN(); //In case the algorithm divides by 0, vertex will just remain where it is
      totalForce.add(force);
    }
    
    Vector2D e = calculateExternalForce(vertex, start);
    e.scale(charLength);
    totalForce.add(e);
    
    totalForce.fixNaN();
    return totalForce;
  }

  @Override
  public Vector2D calculateExternalForce(GraphVertex vertexType, Point2D location) {
    if(vertexType.getType() == VertexType.EXTERNAL_IN) {
      return new Vector2D(-FORCE_STRENGTH, 0);
    } else if(vertexType.getType() == VertexType.EXTERNAL_OUT) {
      return new Vector2D(FORCE_STRENGTH, 0);
    } else {
      return new Vector2D();
    }
  }

  @Override
  public void onIteration(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model, int n) {}
  @Override
  public void onFinished(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model) {}
  @Override
  public void onStarted(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model) {
    if(sortInLegs ) ExternalOrderLayoutAlgorithm.layoutInVertices (graph, model, true);
    if(sortOutLegs) ExternalOrderLayoutAlgorithm.layoutOutVertices(graph, model, true);
    //This will set these vertices to fixed positions, so the algorithm won't move them
  }

  @Override
  public void setDefaultParameters() {
    initialTemp = 0.1;
    iterationCount = 100;
    strongerMultiEdges = false;
    relativeBorder = 0;
    multiEdgeRelativeHeight = 1;
    selfLoopRelativeHeight = 1;
    lengthScale = 0.5;
    temperature = TemperatureModels.CONSTANT;
  }

  @Override
  public void loadParameters(String data) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String saveParameters() {
    throw new UnsupportedOperationException();
  }

}
