package layout;

import qgraf.QgrafDiagram;

/**
 * Exception that is thrown when the Feynman diagram in the drawing area cannot be converted to a
 * {@link QgrafDiagram} because of its structure.
 * 
 * @author Lars Bündgen
 */
public class FeynGameDiagramStructureException extends Exception {
  private static final long serialVersionUID = -1647145390479362148L;

  /**
   * Creates a new instance with an exception message.
   * @param message The exception message.
   */
  public FeynGameDiagramStructureException(String message) {
    super(message);
  }

}
