package layout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.drawing.model.Box2D;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;
import org.jgrapht.alg.interfaces.ManyToManyShortestPathsAlgorithm.ManyToManyShortestPaths;
import org.jgrapht.alg.shortestpath.DijkstraManyToManyShortestPaths;
import org.jgrapht.graph.AsUndirectedGraph;

import qgraf.QgrafDiagram.GraphEdge;
import qgraf.QgrafDiagram.GraphVertex;
import qgraf.QgrafDiagram.VertexType;
import resources.Java8Support;

/**
 * A layout algorithm that arranges external lines based on the graph distance of the external vertices.
 * 
 * @author Lars Bündgen
 */
public class ExternalLineDistanceLayoutAlgorithm extends GraphLayoutAlgorithm {

  private final boolean sortInLegs;
  private final boolean sortOutLegs;
  
  protected ExternalLineDistanceLayoutAlgorithm(String name, String description, boolean sortInLegs, boolean sortOutLegs) {
    super(name, description);
    this.sortInLegs = sortInLegs;
    this.sortOutLegs = sortOutLegs;
  }

  private static GraphEdge getOnlyEdge(Graph<GraphVertex, GraphEdge> graph, GraphVertex vertex) {
    return graph.edgesOf(vertex).stream().findAny().orElseThrow(NoSuchElementException::new);
  }
  
  
  private static Comparator<GraphVertex> vertexByEdgeComparator(Graph<GraphVertex, GraphEdge> graph) {
    return new Comparator<GraphVertex>() {
      @Override
      public int compare(GraphVertex o1, GraphVertex o2) {
        GraphEdge e1 = getOnlyEdge(graph, o1);
        GraphEdge e2 = getOnlyEdge(graph, o2);
        return e1.compareTo(e2);
      }
    };
     
  }
  
  private List<GraphVertex> putIncomingVertices(Graph<GraphVertex, GraphEdge> graph, LayoutModel2D<GraphVertex> model) {
    Box2D box = model.getDrawableArea();
    
    List<GraphVertex> incoming = graph.vertexSet().stream()
        .filter(g -> g.getType() == VertexType.EXTERNAL_IN)
        .collect(Collectors.toCollection(ArrayList::new));
    //use this instead of toList to ensure that the list is mutable for Collections.sort()
    
    //For 1 and 2 incoming vertices, the order does not matter
    if(incoming.size() == 0) {
      return Java8Support.listOf();
    }else if(incoming.size() == 1) {
      GraphVertex v = incoming.get(0);
      model.put(v, new Point2D(box.getMinX(), (box.getMinY() + box.getMaxY()) / 2.0));
      return Java8Support.listOf(v);
    } else if(incoming.size() == 2) {
      GraphVertex v1 = incoming.get(0);
      GraphVertex v2 = incoming.get(1);
      
      if(vertexByEdgeComparator(graph).compare(v1, v2) > 0) {
        model.put(v1, new Point2D(box.getMinX(), box.getMinY()));
        model.put(v2, new Point2D(box.getMinX(), box.getMaxY()));
        return Java8Support.listOf(v1, v2);
      } else {
        model.put(v1, new Point2D(box.getMinX(), box.getMaxY()));
        model.put(v2, new Point2D(box.getMinX(), box.getMinY()));
        return Java8Support.listOf(v2, v1);
      }
    } else { //For three vertices, two might be closer to each other than the other
      Set<GraphVertex> set = new HashSet<>(incoming);
      DijkstraManyToManyShortestPaths<GraphVertex, GraphEdge> dij = 
          new DijkstraManyToManyShortestPaths<>(new AsUndirectedGraph<>(graph));
      ManyToManyShortestPaths<GraphVertex, GraphEdge> result = dij.getManyToManyPaths(set, set);

      //Presort the list, so the choice of start and end is stable
      Collections.sort(incoming, vertexByEdgeComparator(graph));
      
      //These are all the paths
      List<GraphPath<GraphVertex, GraphEdge>> paths = new ArrayList<>();
      for(int i = 0; i < incoming.size() - 1; i++) {
        for(int j = i + 1; j < incoming.size(); j++) {
          GraphVertex start = incoming.get(i);
          GraphVertex end = incoming.get(j);
          GraphPath<GraphVertex, GraphEdge> path = result.getPath(start, end);
          paths.add(path);
        }
      }
      
      //Sort by shortest path first
      Collections.sort(paths, Comparator.comparing(GraphPath::getLength));
      
      List<GraphVertex> orderedIncoming = new ArrayList<>();
      //Add the first path:
      GraphPath<GraphVertex, GraphEdge> p0 = paths.remove(0);
      orderedIncoming.add(p0.getStartVertex());
      orderedIncoming.add(p0.getEndVertex());
      
      while(!paths.isEmpty()) {
        //Find the shortest path in list
        GraphPath<GraphVertex, GraphEdge> p = paths.remove(0);
        if(orderedIncoming.contains(p.getStartVertex()) && orderedIncoming.contains(p.getEndVertex())) {
          continue;
        } else if(orderedIncoming.contains(p.getStartVertex())) {
          //add the end vertex nearest to the start vertex
          int i = orderedIncoming.indexOf(p.getStartVertex());
          
          //add on the side that is closer
          if(i >= orderedIncoming.size()) {
            orderedIncoming.add(p.getEndVertex());
          } else {
            orderedIncoming.add(0, p.getEndVertex());
          }
          
        } else if(orderedIncoming.contains(p.getEndVertex())) {
          int i = orderedIncoming.indexOf(p.getEndVertex());
          
          //add on the side that is closer
          if(i >= orderedIncoming.size()) {
            orderedIncoming.add(p.getStartVertex());
          } else {
            orderedIncoming.add(0, p.getStartVertex());
          }
          
        } else { //add both below
          orderedIncoming.add(p.getStartVertex());
          orderedIncoming.add(p.getEndVertex());
        }
      }
      
      //All are in order, set positions
      assert orderedIncoming.size() == incoming.size();
      
      double div = model.getDrawableArea().getHeight() / (double)(orderedIncoming.size() - 1);
      for(int i = 0; i < orderedIncoming.size(); i++) {
        GraphVertex v = orderedIncoming.get(i);
        model.put(v, new Point2D(box.getMinX(), box.getMinY() + i*div));
      }
      
      return orderedIncoming;
    }
  }
  
  private void putOutgoingVertices(Graph<GraphVertex, GraphEdge> graph, LayoutModel2D<GraphVertex> model,
      List<GraphVertex> orderedIncoming) {
    Box2D box = model.getDrawableArea();
    
    List<GraphVertex> outgoing = graph.vertexSet().stream()
        .filter(g -> g.getType() == VertexType.EXTERNAL_OUT)
        .collect(Collectors.toCollection(ArrayList::new));
    
    if(outgoing.size() == 0) {
      return;
    } else if(outgoing.size() == 1) {
      GraphVertex v = outgoing.get(0);
      model.put(v, new Point2D(box.getMaxX(), (box.getMinY() + box.getMaxY()) / 2.0));
    } else if(outgoing.size() == 2){ //choose whichever one is closer to the upper incoming one
      Set<GraphVertex> inSet = new HashSet<>(orderedIncoming);
      Set<GraphVertex> outSet = new HashSet<>(outgoing);
      
      //ensure stable sort
      Collections.sort(outgoing, vertexByEdgeComparator(graph));
      
      DijkstraManyToManyShortestPaths<GraphVertex, GraphEdge> dij = 
          new DijkstraManyToManyShortestPaths<>(new AsUndirectedGraph<>(graph));
      ManyToManyShortestPaths<GraphVertex, GraphEdge> result = dij.getManyToManyPaths(inSet, outSet);
      GraphVertex topIn = orderedIncoming.isEmpty() ? null : orderedIncoming.get(0);
      GraphVertex v1 = outgoing.get(0);
      GraphVertex v2 = outgoing.get(1);
      
      if(topIn != null && result.getPath(topIn, v1).getLength() <
          result.getPath(topIn, v2).getLength()) { //if v1 top
        model.put(v1, new Point2D(box.getMinX(), box.getMinY()));
        model.put(v2, new Point2D(box.getMinX(), box.getMaxY()));
      } else {
        model.put(v1, new Point2D(box.getMinX(), box.getMaxY()));
        model.put(v2, new Point2D(box.getMinX(), box.getMinY()));
      }
    } else { //3 or more outgoing:
      //Similar to incoming legs: add closest pairs, but decide start or end 
      //depending on the distance to the top node on the other side.
      GraphVertex topIn = orderedIncoming.isEmpty() ? null : orderedIncoming.get(0);
      GraphVertex bottomIn = orderedIncoming.isEmpty() ? null : orderedIncoming.get(orderedIncoming.size() - 1);
      
      Set<GraphVertex> inSet = new HashSet<>(orderedIncoming);
      Set<GraphVertex> outSet = new HashSet<>(outgoing);
      DijkstraManyToManyShortestPaths<GraphVertex, GraphEdge> dij = 
          new DijkstraManyToManyShortestPaths<>(new AsUndirectedGraph<>(graph));
      ManyToManyShortestPaths<GraphVertex, GraphEdge> result = dij.getManyToManyPaths(outSet, outSet);
      DijkstraManyToManyShortestPaths<GraphVertex, GraphEdge> dij2 = 
          new DijkstraManyToManyShortestPaths<>(new AsUndirectedGraph<>(graph));
      ManyToManyShortestPaths<GraphVertex, GraphEdge> result2 = dij2.getManyToManyPaths(inSet, outSet);
      
      //Presort the list, so the choice of start and end is stable
      Collections.sort(outgoing, vertexByEdgeComparator(graph));
      
      //These are all the paths
      List<GraphPath<GraphVertex, GraphEdge>> paths = new ArrayList<>();
      for(int i = 0; i < outgoing.size() - 1; i++) {
        for(int j = i + 1; j < outgoing.size(); j++) {
          GraphVertex start = outgoing.get(i);
          GraphVertex end = outgoing.get(j);
          GraphPath<GraphVertex, GraphEdge> path = result.getPath(start, end);
          paths.add(path);
        }
      }
      
      //Sort by shortest path first
      Collections.sort(paths, Comparator.comparing(GraphPath::getLength));
      
      List<GraphVertex> orderedOutgoing = new ArrayList<>();
      //Add the first path:
      GraphPath<GraphVertex, GraphEdge> p0 = paths.remove(0);
      //TODO first path has to be sorted by what is closer to top!
      if(topIn != null && result2.getPath(topIn, p0.getStartVertex()).getLength() <
          result2.getPath(topIn, p0.getEndVertex()).getLength()) {
        orderedOutgoing.add(p0.getStartVertex());
        orderedOutgoing.add(p0.getEndVertex());
      } else {
        orderedOutgoing.add(p0.getEndVertex());
        orderedOutgoing.add(p0.getStartVertex());
      }
      
      
      while(!paths.isEmpty()) {
        //Find the shortest path in list
        GraphPath<GraphVertex, GraphEdge> p = paths.remove(0);
        if(orderedOutgoing.contains(p.getStartVertex()) && orderedOutgoing.contains(p.getEndVertex())) {
          continue;
        } if(orderedOutgoing.contains(p.getStartVertex())) {
          //Check whether it is nearer to the top or bottom of the incoming ones
          int topDistance = topIn == null ? -1 : result2.getPath(topIn, p.getEndVertex()).getLength();
          int bottomDistance = bottomIn == null ? -1 : result2.getPath(bottomIn, p.getEndVertex()).getLength(); 
          if(topDistance < bottomDistance) {
            orderedOutgoing.add(0, p.getEndVertex());
          } else if(topDistance > bottomDistance){
            orderedOutgoing.add(p.getEndVertex());
          } else {
            int i = orderedOutgoing.indexOf(p.getStartVertex());
            //add on the side that is closer
            if(i >= orderedOutgoing.size()) {
              orderedOutgoing.add(p.getEndVertex());
            } else {
              orderedOutgoing.add(0, p.getEndVertex());
            }
          }
          
        } else if(orderedOutgoing.contains(p.getEndVertex())) {
          int topDistance = topIn == null ? -1 : result2.getPath(topIn, p.getStartVertex()).getLength();
          int bottomDistance = bottomIn == null ? -1 : result2.getPath(bottomIn, p.getStartVertex()).getLength();
          if(topDistance < bottomDistance) {
            orderedOutgoing.add(0, p.getStartVertex());
          } else if(topDistance > bottomDistance) {
            orderedOutgoing.add(p.getStartVertex());
          } else {
            int i = orderedOutgoing.indexOf(p.getEndVertex());
            //add on the side that is closer
            if(i >= orderedOutgoing.size()) {
              orderedOutgoing.add(p.getStartVertex());
            } else {
              orderedOutgoing.add(0, p.getStartVertex());
            }
          }
        } else { //add both below
          
          if(topIn != null && bottomIn != null && result2.getPath(topIn, p.getEndVertex()).getLength() < 
              result2.getPath(bottomIn, p.getEndVertex()).getLength()) {
            orderedOutgoing.add(0, p.getStartVertex());
            orderedOutgoing.add(0, p.getEndVertex());
          } else {
            orderedOutgoing.add(p.getStartVertex());
            orderedOutgoing.add(p.getEndVertex());
          }
        }
      }
      
      //All are in order, set positions
      assert orderedOutgoing.size() == outgoing.size();
      
      double div = model.getDrawableArea().getHeight() / (double)(orderedOutgoing.size() - 1);
      for(int i = 0; i < orderedOutgoing.size(); i++) {
        GraphVertex v = orderedOutgoing.get(i);
        model.put(v, new Point2D(box.getMaxX(), box.getMinY() + i*div));
      }
      
    }
  }
  
  @Override
  public void layout(Graph<GraphVertex, GraphEdge> graph, LayoutModel2D<GraphVertex> model) {
    List<GraphVertex> list;
    if(sortInLegs) {
      list = ExternalOrderLayoutAlgorithm.layoutInVertices(graph, model, true); 
    } else {
      list = putIncomingVertices(graph, model);
    }
    
    if(sortOutLegs) {
      ExternalOrderLayoutAlgorithm.layoutOutVertices(graph, model, true);
    } else {
      putOutgoingVertices(graph, model, list);
    }
  }


  @Override
  public boolean requiresInitialLayout() {
    return false;
  }


  @Override
  public void setDefaultParameters() {
  }


  @Override
  public void loadParameters(String data) {
    throw new UnsupportedOperationException();
  }


  @Override
  public String saveParameters() {
    throw new UnsupportedOperationException();
  }

}
