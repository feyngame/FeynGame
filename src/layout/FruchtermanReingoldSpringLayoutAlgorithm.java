package layout;

import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.jgrapht.Graph;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;

import qgraf.QgrafDiagram.GraphVertex;
import qgraf.QgrafDiagram.VertexType;
import ui.Frame;

/**
 * An implementation of a spring layout algorithm based on "Graph Drawing by Force–Directed Placement" by 
 * Thomas M. J. Fruchterman and Edward M. Reingold.
 * 
 * @author Lars Bündgen
 */
public class FruchtermanReingoldSpringLayoutAlgorithm extends SpringGraphLayoutAlgorithm {
  private static final Logger LOGGER = Frame.getLogger(FruchtermanReingoldSpringLayoutAlgorithm.class);
  
  protected FruchtermanReingoldSpringLayoutAlgorithm(String name, String description) {
    super(name, description, true);
    setDefaultParameters();
  }
  
  protected double calculateLengthParameter(double width, double height, int vertices) {
    return lengthScale * Math.sqrt(width * height / (double)vertices);
  }
  
  protected double attractiveForce(double distance, double length) {
    return + distance*distance / length;
  }
  
  protected double repulsiveForce(double distance, double length) {
    return - length * length / distance;
  }
  
  @Override
  public double calculateVertexForce(double l, double k, boolean c) {
    if(c) {
      return attractiveForce(l, k) + repulsiveForce(l, k);
    } else {
      return repulsiveForce(l, k);
    }
  }

  @Override
  public void setDefaultParameters() {
    initialTemp = 0.04;
    iterationCount = 200;
    strongerMultiEdges = false;
    relativeBorder = 0.1;
    multiEdgeRelativeHeight = 0.15;
    selfLoopRelativeHeight = 0.15;
    lengthScale = 0.4;
    temperature = TemperatureModels.LINEAR;
  }
  
  

  @Override
  public Vector2D calculateExternalForce(GraphVertex vertexType, Point2D location) {
    return new Vector2D();
  }

  @Override
  public void onIteration(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model, int N) {}
  
  /**
   * Moves all external vertices of the graph to the left and right edge of the drawable area of the model.
   * The vertices will be ordered by the y-coordinate in the layout and have equal spacing between them.
   * @param graph The graph 
   * @param model The current layout of the graph
   */
  static void fixExternalsToBounds(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model) {
    
    List<GraphVertex> incoming = graph.vertexSet().stream()
        .filter(g -> g.getType() == VertexType.EXTERNAL_IN)
        .sorted(Comparator.comparing(g -> model.get(g).getY()))
        .collect(Collectors.toList());
    
    if(!incoming.isEmpty()) {
      double extraOffset = incoming.size() == 1 ? model.getDrawableArea().getHeight() / 2.0 : 0.0;
      double delta = incoming.size() == 1 ? 0.0 : model.getDrawableArea().getHeight() / (incoming.size() - 1.0);
      double x = model.getDrawableArea().getMinX();
      double y = model.getDrawableArea().getMinY();
      
      for(int i = 0; i < incoming.size(); i++) {
        GraphVertex v = incoming.get(i);
        model.setFixed(v, false);
        model.put(v, new Point2D(x, y + i*delta + extraOffset));
        model.setFixed(v, true);
      }
    }
    
    List<GraphVertex> outgoing = graph.vertexSet().stream()
        .filter(g -> g.getType() == VertexType.EXTERNAL_OUT)
        .sorted(Comparator.comparing(g -> model.get(g).getY()))
        .collect(Collectors.toList());
    
    if(!outgoing.isEmpty()) {
      double extraOffset = outgoing.size() == 1 ? model.getDrawableArea().getHeight() / 2.0 : 0.0;
      double delta = outgoing.size() == 1 ? 0.0 : model.getDrawableArea().getHeight() / (outgoing.size() - 1.0);
      double x = model.getDrawableArea().getMaxX();
      double y = model.getDrawableArea().getMinY();
      
      for(int i = 0; i < outgoing.size(); i++) {
        GraphVertex v = outgoing.get(i);
        model.setFixed(v, false);
        model.put(v, new Point2D(x, y + i*delta + extraOffset));
        model.setFixed(v, true);
      }
    }
  }

  @Override
  public void onFinished(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model) {}
  
  @Override
  public void onStarted(Graph<GraphVertex, GraphMultiEdge> graph, LayoutModel2D<GraphVertex> model) {
    for(Point2D p : model.collect().values()) {
      if(!Double.isFinite(p.getX())) {
        throw new IllegalStateException();
      }
      if(!Double.isFinite(p.getY())) {
        throw new IllegalStateException();
      }
    }
    fixExternalsToBounds(graph, model);
  }

  @Override
  public void loadParameters(String data) {
    String[] parts = data.split(";");
    if(parts.length != 9) {
      LOGGER.warning("cannot parse layout parameters");
      setDefaultParameters();
      return;
    }
    
    try {
      initialTemp = Double.parseDouble(parts[0]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Invalid value for stepSize");
      initialTemp = 0.04;
    }
    
    try {
      iterationCount = Integer.parseInt(parts[1]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Invalid value for iterationCount");
      iterationCount = 300;
    }
    
    try {
      strongerMultiEdges = Boolean.parseBoolean(parts[2]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Invalid value for strongerMultiEdges");
      strongerMultiEdges = false;
    }
    
    try {
      relativeBorder = Double.parseDouble(parts[3]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Invalid value for relativeBorder");
      relativeBorder = 0.1;
    }
    
    try {
      multiEdgeRelativeHeight = Double.parseDouble(parts[4]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Invalid value for multiEdgeRelativeHeight");
      multiEdgeRelativeHeight = 20;
    }
    
    try {
      selfLoopRelativeHeight = Double.parseDouble(parts[5]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Invalid value for selfLoopRelativeHeight");
      selfLoopRelativeHeight = 15;
    }
    
    try {
      lengthScale = Double.parseDouble(parts[6]);
    } catch (NumberFormatException e) {
      LOGGER.warning("Invalid value for lengthScale");
      lengthScale = 1.0;
    }
    
    try {
      temperature = TemperatureModels.valueOf(parts[7]);
    } catch (IllegalArgumentException e) {
      LOGGER.warning("Invalid value for temperature");
      temperature = TemperatureModels.LINEAR;
    }
    
    name = parts[8];
  }

  @Override
  public String saveParameters() {
    return String.join(";", 
        Double.toString(initialTemp),
        Integer.toString(iterationCount),
        Boolean.toString(strongerMultiEdges),
        Double.toString(relativeBorder),
        Double.toString(multiEdgeRelativeHeight),
        Double.toString(selfLoopRelativeHeight),
        Double.toString(lengthScale),
        temperature.name(),
        name);
  }
  
  
  
}
