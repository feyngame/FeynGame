package layout;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.jgrapht.Graph;
import org.jgrapht.alg.drawing.model.Box2D;
import org.jgrapht.alg.drawing.model.LayoutModel2D;
import org.jgrapht.alg.drawing.model.Point2D;

import qgraf.QgrafDiagram.GraphEdge;
import qgraf.QgrafDiagram.GraphVertex;
import qgraf.QgrafDiagram.VertexType;

public final class ExternalOrderLayoutAlgorithm extends GraphLayoutAlgorithm {

  protected ExternalOrderLayoutAlgorithm(String name, String description) {
    super(name, description);
  }

  public static List<GraphVertex> layoutInVertices(Graph<GraphVertex, ?> graph, LayoutModel2D<GraphVertex> model, boolean fix)  {
    List<GraphVertex> inVertices = graph.vertexSet().stream()
        .filter(v -> v.getType() == VertexType.EXTERNAL_IN)
        .sorted(Comparator.comparingInt(GraphVertex::getExternalIndex))
        .collect(Collectors.toCollection(ArrayList::new));
    Box2D box = model.getDrawableArea();
    
    if(inVertices.size() == 1) {
      GraphVertex v = inVertices.get(0);
      model.put(v, new Point2D(box.getMinX(), (box.getMinY() + box.getMaxY()) / 2.0));
    } else if(inVertices.size() > 1) {
      double div = model.getDrawableArea().getHeight() / (double)(inVertices.size() - 1);
      for(int i = 0; i < inVertices.size(); i++) {
        GraphVertex v = inVertices.get(i);
        model.setFixed(v, false);
        model.put(v, new Point2D(box.getMinX(), box.getMinY() + i*div));
        model.setFixed(v, fix);
      }
    }
    
    return inVertices;
  }
  
  public static List<GraphVertex> layoutOutVertices(Graph<GraphVertex, ?> graph, LayoutModel2D<GraphVertex> model, boolean fix)  {
    List<GraphVertex> outVertices = graph.vertexSet().stream()
        .filter(v -> v.getType() == VertexType.EXTERNAL_OUT)
        .sorted(Comparator.comparingInt(GraphVertex::getExternalIndex))
        .collect(Collectors.toCollection(ArrayList::new));
    Box2D box = model.getDrawableArea();
    
    if(outVertices.size() == 1) {
      GraphVertex v = outVertices.get(0);
      model.put(v, new Point2D(box.getMaxX(), (box.getMinY() + box.getMaxY()) / 2.0));
    } else if(outVertices.size() > 1) {
      double div = model.getDrawableArea().getHeight() / (double)(outVertices.size() - 1);
      for(int i = 0; i < outVertices.size(); i++) {
        GraphVertex v = outVertices.get(i);
        model.setFixed(v, false);
        model.put(v, new Point2D(box.getMaxX(), box.getMinY() + i*div));
        model.setFixed(v, fix);
      }
    }
    
    return outVertices;
  }

  @Override
  public boolean requiresInitialLayout() {
    return false;
  }

  @Override
  public void layout(Graph<GraphVertex, GraphEdge> graph, LayoutModel2D<GraphVertex> model) {
    layoutInVertices(graph, model, false);
    layoutOutVertices(graph, model, false);
  }

  @Override
  public void setDefaultParameters() {}

  @Override
  public void loadParameters(String data) {}

  @Override
  public String saveParameters() {
    return "";
  }
}
