//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.jgrapht.Graph;
import org.jgrapht.alg.cycle.QueueBFSFundamentalCycleBasis;
import org.jgrapht.graph.DirectedPseudograph;
import org.jgrapht.traverse.DepthFirstIterator;
import org.scilab.forge.jlatexmath.ParseException;

import game.FeynLine;
import game.LineConfig;
import game.Point2D;
import ui.Frame;

/**
 * Class representing the abstract topology of a Feynman diagram and the corresponding Feynman rules
 */
public class FeynmanDiagram {
  private static final Logger LOGGER = Frame.getLogger(FeynmanDiagram.class);

    private final Graph<Point2D, FeynLine> diagram;
    private final Graph<Point2D, FeynLine> acDiagram;
    private final Set<List<FeynLine>> loops;
    private final Set<List<FeynLine>> acLoops;
    private final Map<FeynLine, FeynmanParticle> particles;
    private final Set<FeynmanRule> feynmanRules;

    /**
     * Constructs a Feynman diagram from the given list of lines.
     *
     * @param lines corresponding to the diagram
     */
    public FeynmanDiagram(List<FeynLine> lines) {
        diagram = new DirectedPseudograph<>(FeynLine.class);
        acDiagram = new DirectedPseudograph<>(FeynLine.class);
        for (FeynLine line : lines) {
            diagram.addVertex(line.getStart());
            diagram.addVertex(line.getEnd());
            diagram.addEdge(line.getStart(), line.getEnd(), line);
            if (LineConfig.feynmanRuleTemplates.isAnticommuting(line.lineConfig.identifier)) {
                acDiagram.addVertex(line.getStart());
                acDiagram.addVertex(line.getEnd());
                acDiagram.addEdge(line.getStart(), line.getEnd(), line);
            }
        }
        QueueBFSFundamentalCycleBasis<Point2D, FeynLine> cycleBasis = new QueueBFSFundamentalCycleBasis<>(diagram);
        loops = cycleBasis.getCycleBasis().getCycles();
        QueueBFSFundamentalCycleBasis<Point2D, FeynLine> acCycleBasis = new QueueBFSFundamentalCycleBasis<>(acDiagram);
        acLoops = acCycleBasis.getCycleBasis().getCycles();
        particles = new HashMap<>();
        feynmanRules = new LinkedHashSet<>();
        generateFeynmanRules();
    }

    private FeynmanParticle convertLineToParticle(FeynLine line) {
        FeynmanParticle particle;
        Point2D start = line.getStart();
        Point2D end = line.getEnd();
        if (diagram.degreeOf(start) == 1 && diagram.degreeOf(end) > 1) { // external
            if (start.isLeftOf(end)) { // incoming particle
                particle = new ExternalParticle(line, ExternalParticle.Type.INCOMING_PARTICLE);
            } else { // outgoing antiparticle
                particle = new ExternalParticle(line, ExternalParticle.Type.OUTGOING_ANTIPARTICLE);
            }
        } else if (diagram.degreeOf(start) > 1 && diagram.degreeOf(end) == 1) { // external
            if (start.isLeftOf(end)) { // outgoing particle
                particle = new ExternalParticle(line, ExternalParticle.Type.OUTGOING_PARTICLE);
            } else { // incoming antiparticle
                particle = new ExternalParticle(line, ExternalParticle.Type.INCOMING_ANTIPARTICLE);
            }
        } else { // internal
            particle = new FeynmanPropagator(line);
        }
        return particle;
    }

    private void generateFeynmanRules() {
        Index.resetCounter();
        FeynmanVertex.resetCounter();
        if (diagram.edgeSet().size() == 1) { // single propagator
            FeynLine line = diagram.edgeSet().iterator().next();
            FeynmanParticle propagator = convertLineToParticle(line);
            propagator.setMomentum("p");
            particles.put(line, propagator);
            feynmanRules.add(propagator);
            return;
        }
        Set<FeynmanRule> feynmanRulesInternal = new LinkedHashSet<>();
        Set<FeynmanRule> tempFeynmanRulesInternal = new LinkedHashSet<>();
        Set<ExternalParticle> feynmanRulesIncoming = new LinkedHashSet<>();
        Set<ExternalParticle> feynmanRulesOutgoing = new LinkedHashSet<>();
        Iterator<Point2D> iterator = new DepthFirstIterator<>(diagram);
        while (iterator.hasNext()) {
            Point2D vertex = iterator.next();
            if (diagram.degreeOf(vertex) > 1) {
                Set<FeynmanParticle> incomingVertexParticles = new HashSet<>();
                for (FeynLine line : diagram.incomingEdgesOf(vertex)) {
                    FeynmanParticle particle = particles.computeIfAbsent(line, this::convertLineToParticle);
                    incomingVertexParticles.add(particle);
                    if (particle instanceof FeynmanPropagator) {
                        feynmanRulesInternal.add(particle);
                    } else if (particle instanceof ExternalParticle) {
                        ExternalParticle externalParticle = (ExternalParticle) particle;
                        if (externalParticle.isIncoming()) feynmanRulesIncoming.add(externalParticle);
                        if (externalParticle.isOutgoing()) feynmanRulesOutgoing.add(externalParticle);
                    }
                }
                Set<FeynmanParticle> outgoingVertexParticles = new HashSet<>();
                for (FeynLine line : diagram.outgoingEdgesOf(vertex)) {
                    FeynmanParticle particle = particles.computeIfAbsent(line, this::convertLineToParticle);
                    outgoingVertexParticles.add(particle);
                    if (particle instanceof FeynmanPropagator) {
                        tempFeynmanRulesInternal.add(particle);
                    } else if (particle instanceof ExternalParticle) {
                        ExternalParticle externalParticle = (ExternalParticle) particle;
                        if (externalParticle.isIncoming()) feynmanRulesIncoming.add(externalParticle);
                        if (externalParticle.isOutgoing()) feynmanRulesOutgoing.add(externalParticle);
                    }
                }
                feynmanRulesInternal.add(new FeynmanVertex(incomingVertexParticles, outgoingVertexParticles));
                feynmanRulesInternal.addAll(tempFeynmanRulesInternal);
            }
        }
        momentumAssignment(feynmanRulesIncoming, feynmanRulesOutgoing);
        feynmanRules.addAll(feynmanRulesIncoming);
        feynmanRules.addAll(feynmanRulesInternal);
        feynmanRules.addAll(feynmanRulesOutgoing);
    }

    private void momentumAssignment(Set<ExternalParticle> feynmanRulesIncoming, Set<ExternalParticle> feynmanRulesOutgoing) {
        Set<FeynmanParticle> momenta = new HashSet<>();
        int incomingIndex = 1;
        for (ExternalParticle incomingParticle : feynmanRulesIncoming) {
            if (momenta.size() < feynmanRulesIncoming.size() + feynmanRulesOutgoing.size() - 1) {
                FeynLine incomingLine = incomingParticle.getLine();
                if (incomingLine.isMom()) {
                    int multiplicity = (incomingLine.momArrow.invert) ? -1 : 1;
                    if (incomingLine.momArrow.showDesc && !incomingLine.momArrow.description.chars().allMatch(Character::isWhitespace)) {
                        String momentumLabel = incomingLine.momArrow.description.trim();
                        if (momentumLabel.matches("^\\+?[^\\d\\+\\-].*")) {
                            momentumLabel = momentumLabel.replaceAll("^\\+", "").trim();
                            incomingParticle.setMomentum(momentumLabel, multiplicity);
                        } else if (momentumLabel.matches("^\\-[^\\d\\+\\-].*")) {
                            momentumLabel = momentumLabel.replaceAll("^\\-", "").trim();
                            incomingParticle.setMomentum(momentumLabel, -multiplicity);
                        } else {
                            incomingParticle.setMomentum("p_{" + (incomingIndex++) + "}", multiplicity);
                        }
                    } else {
                        incomingParticle.setMomentum("p_{" + (incomingIndex++) + "}", multiplicity);
                    }
                } else {
                    incomingParticle.setMomentum("p_{" + (incomingIndex++) + "}");
                }
                momenta.add(incomingParticle);
            }
        }
        int outgoingIndex = 1;
        for (ExternalParticle outgoingParticle : feynmanRulesOutgoing) {
            if (momenta.size() < feynmanRulesIncoming.size() + feynmanRulesOutgoing.size() - 1) {
                FeynLine outgoingLine = outgoingParticle.getLine();
                if (outgoingLine.isMom()) {
                    int multiplicity = (outgoingLine.momArrow.invert) ? -1 : 1;
                    if (outgoingLine.momArrow.showDesc && !outgoingLine.momArrow.description.chars().allMatch(Character::isWhitespace)) {
                        String momentumLabel = outgoingLine.momArrow.description.trim();
                        if (momentumLabel.matches("^\\+?[^\\d\\+\\-].*")) {
                            momentumLabel = momentumLabel.replaceAll("^\\+", "").trim();
                            outgoingParticle.setMomentum(momentumLabel, multiplicity);
                        } else if (momentumLabel.matches("^\\-[^\\d\\+\\-].*")) {
                            momentumLabel = momentumLabel.replaceAll("^\\-", "").trim();
                            outgoingParticle.setMomentum(momentumLabel, -multiplicity);
                        } else {
                            outgoingParticle.setMomentum("q_{" + (outgoingIndex++) + "}", multiplicity);
                        }
                    } else {
                        outgoingParticle.setMomentum("q_{" + (outgoingIndex++) + "}", multiplicity);
                    }
                } else {
                    outgoingParticle.setMomentum("q_{" + (outgoingIndex++) + "}");
                }
                momenta.add(outgoingParticle);
            }
        }
        int loopIndex = 1;
        for (List<FeynLine> loop : loops) {
            Set<List<FeynLine>> remaingLoops = new HashSet<>(loops);
            List<FeynLine> uniqueLoop = new ArrayList<>(loop);
            remaingLoops.remove(loop);
            remaingLoops.forEach(uniqueLoop::removeAll);
            if (uniqueLoop.isEmpty()) {
                LOGGER.warning("fully overlapping loops are not supported yet");
                return;
            }
            FeynmanParticle loopParticle = particles.get(uniqueLoop.get(0));
            loopParticle.setMomentum("k_{" + (loopIndex++) + "}");
            momenta.add(loopParticle);
        }
        int iteration = 0;
        Set<FeynLine> lines = diagram.edgeSet();
        while (momenta.size() < lines.size()) {
            for (FeynLine line : lines) {
                FeynmanParticle particle = particles.get(line);
                if (!momenta.contains(particle)) {
                    Point2D sourceVertex = diagram.getEdgeSource(line);
                    ParticleMomentum momentum = momentumConservation(sourceVertex, line);
                    if (momentum != null) {
                        particle.setMomentum(momentum);
                        momenta.add(particle);
                        continue;
                    }
                    Point2D targetVertex = diagram.getEdgeTarget(line);
                    momentum = momentumConservation(targetVertex, line);
                    if (momentum != null) {
                        particle.setMomentum(momentum);
                        momenta.add(particle);
                        continue;
                    }
                }
            }
            if (++iteration >= lines.size()) {
              LOGGER.warning("Cannot assign momenta"); 
              return;
            }
        }
    }

    private ParticleMomentum momentumConservation(Point2D vertex, FeynLine line) {
        if (diagram.degreeOf(vertex) <= 1) return null;
        ParticleMomentum momentum = new ParticleMomentum();
        Set<FeynLine> incomingEdges = diagram.incomingEdgesOf(vertex);
        Set<FeynLine> outgoingEdges = diagram.outgoingEdgesOf(vertex);
        for (FeynLine incomingLine : incomingEdges) {
            if (incomingLine != line) {
                ParticleMomentum otherMomentum = particles.get(incomingLine).getMomentum();
                if (otherMomentum == null) return null;
                if (incomingEdges.contains(line)) {
                    momentum.subtractMomentum(otherMomentum);
                } else if (outgoingEdges.contains(line)) {
                    momentum.addMomentum(otherMomentum);
                }
            }
        }
        for (FeynLine outgoingLine : outgoingEdges) {
            if (outgoingLine != line) {
                ParticleMomentum otherMomentum = particles.get(outgoingLine).getMomentum();
                if (otherMomentum == null) return null;
                if (incomingEdges.contains(line)) {
                    momentum.addMomentum(otherMomentum);
                } else if (outgoingEdges.contains(line)) {
                    momentum.subtractMomentum(otherMomentum);
                }
            }
        }
        return momentum;
    }

    /**
     * {@return the {@code LaTeX} representation of the amplitude of the Feynman diagram}
     * The Feynman rules can be specified in the model file.
     */
    public String parseAmplitude() throws ParseException {
        StringBuilder sign = new StringBuilder();
        int numberAcLoops = acLoops.size();
        if (numberAcLoops == 1) {
            sign.append("-");
        } else if (numberAcLoops > 1) {
            sign.append("(-1)^");
            sign.append(acLoops.size());
        }

        StringBuilder vertexFactors = new StringBuilder();
        for (Map.Entry<String, Integer> factor : FeynmanVertex.vertexFactors.entrySet()) {
            if (!factor.getKey().chars().allMatch(Character::isWhitespace)) {
                vertexFactors.append(factor.getKey());
                int power = factor.getValue();
                if (power != 1) {
                    vertexFactors.append("^");
                    vertexFactors.append(power);
                }
                vertexFactors.append("\\,");
            }
        }

        StringBuilder integral = new StringBuilder();
        int numberLoops = loops.size();
        if (numberLoops >= 1) {
            integral.append("\\int");
            for (int i = 1; i <= numberLoops; ++i) {
                integral.append("\\frac{\\mathrm{d}^4k_");
                integral.append(i);
                integral.append("}{(2\\pi)^4}");
            }
        }

        StringBuilder rules = new StringBuilder();
        for (FeynmanRule feynmanRule : feynmanRules) {
            if (!feynmanRule.toLaTeX().chars().allMatch(Character::isWhitespace)) {
                rules.append(feynmanRule.toLaTeX());
                rules.append("\\,");
            }
        }

	if (rules.length() == 0) {
	    throw new ParseException("No Feynman rules found for this diagram");
	}
        StringBuilder amplitude = new StringBuilder();
        amplitude.append(sign);
        amplitude.append(vertexFactors);
        amplitude.append(integral);
        amplitude.append(rules);
        return amplitude.toString();
    }

    /**
     * Distributes the momenta to every line of the diagram using momentum conservation at every vertex.
     */
    public void distributeMomenta() {
        for (Map.Entry<FeynLine, FeynmanParticle> particle : particles.entrySet()) {
            particle.getKey().showMom();
            int multiplicity = (particle.getKey().momArrow.invert) ? -1 : 1;
            particle.getKey().momArrow.showDesc = true;
            particle.getKey().momArrow.description = particle.getValue().getMomentum().toLaTeX(multiplicity);
        }
    }
}
