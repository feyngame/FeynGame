//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.*;

import game.FeynLine;

/**
 * Abstract class representing a particle in a Feynman diagram.
 */
public abstract class FeynmanParticle implements FeynmanRule {

    private ParticleMomentum momentum;
    private final FeynLine line;

    protected FeynmanParticle(FeynLine line) {
        this.line = line;
    }

    /**
     * Sets the momentum of the particle to the given ({@code Latex}-)string.
     *
     * @param momentumLabel represents the four-momentum
     */
    public void setMomentum(String momentumLabel) {
        momentum = new ParticleMomentum(momentumLabel);
    }

    /**
     * Sets the momentum of the particle to a multiple of the given ({@code Latex}-)string.
     *
     * @param momentumLabel represents the four-momentum
     * @param multiplicity integer prefactor
     */
    public void setMomentum(String momentumLabel, int multiplicity) {
        momentum = new ParticleMomentum(momentumLabel, multiplicity);
    }

    /**
     * Sets the momentum of the particle to the given four-momentum.
     *
     * @param momentum the four-momentum
     */
    public void setMomentum(ParticleMomentum momentum) {
        this.momentum = momentum;
    }

    /**
     * {@return the four-momentum of the particle}
     */
    public ParticleMomentum getMomentum() {
        return momentum;
    }

    /**
     * {@return the label of the particle as given in the {@link game.LineConfig}}
     */
    public String getLabel() {
        return line.lineConfig.description;
    }

    /**
     * {@return the identifier at the start/end of the line}
     * For bosonic particles both identifiers should be equal.
     *
     * @param start whether to return the identifier at the start of the line
     */
    public String getIdentifier(boolean start) {
        if (start)
            return line.lineConfig.identifier.getKey();
        return line.lineConfig.identifier.getValue();
    }

    /**
     * {@return the {@link game.FeynLine} corresponding to the particle}
     */
    public FeynLine getLine() {
        return line;
    }

    /**
     * {@return a nested list of indices at the start of the line as defined in the model file}
     * The indices of the particle are specified by the {@code indices} keyword in the model file.
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public abstract List<List<Index>> getStartIndices();

    /**
     * {@return a nested list of indices at the end of the line as defined in the model file}
     * The indices of the particle are specified by the {@code indices} keyword in the model file.
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public abstract List<List<Index>> getEndIndices();
}
