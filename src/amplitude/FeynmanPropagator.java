//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.*;
import java.util.regex.Matcher;

import org.scilab.forge.jlatexmath.ParseException;

import game.FeynLine;
import game.LineConfig;

/**
 * Class representing an internal particle in a Feynman diagram.
 */
public class FeynmanPropagator extends FeynmanParticle {

    private final List<List<Index>> indicesStart;
    private final List<List<Index>> indicesEnd;
    private final String propagatorTemplate;

    /**
     * Constructs an internal particle from the given line.
     *
     * @param line corresponding to an internal particle
     */
    public FeynmanPropagator(FeynLine line) {
        super(line);
        List<Integer> indexRepresentations = LineConfig.feynmanRuleTemplates.getIndexRepresentation(line.lineConfig.identifier);
        indicesStart = new ArrayList<>(indexRepresentations.size());
        indicesEnd = new ArrayList<>(indexRepresentations.size());
        for (int i = 0; i < indexRepresentations.size(); i++) {
            indicesStart.add(new ArrayList<>(indexRepresentations.get(i)));
            indicesEnd.add(new ArrayList<>(indexRepresentations.get(i)));
            for (int j = 1; j <= indexRepresentations.get(i); j++) {
                indicesStart.get(i).add(new Index(i));
                indicesEnd.get(i).add(new Index(i));
            }
        }
        propagatorTemplate = LineConfig.feynmanRuleTemplates.getPropagatorTemplate(line.lineConfig.identifier);
    }

    public List<List<Index>> getStartIndices() {
        return indicesStart;
    }

    public List<List<Index>> getEndIndices() {
        return indicesEnd;
    }

    public String defaultRepresentation() {
        StringBuilder indexString = new StringBuilder();
        Iterator<List<Index>> itIndexTypesIn = getStartIndices().iterator();
        Iterator<List<Index>> itIndexTypesOut = getEndIndices().iterator();
        while (itIndexTypesIn.hasNext() && itIndexTypesOut.hasNext()) {
            StringBuilder indexGroupIn = new StringBuilder();
            StringBuilder indexGroupOut = new StringBuilder();
            Iterator<Index> itIndicesIn = itIndexTypesIn.next().iterator();
            Iterator<Index> itIndicesOut = itIndexTypesOut.next().iterator();
            while (itIndicesIn.hasNext() && itIndicesOut.hasNext()) {
                indexGroupIn.append(itIndicesIn.next().toLaTeX());
                indexGroupOut.append(itIndicesOut.next().toLaTeX());
            }
            indexString.append(indexGroupIn);
            indexString.append(indexGroupOut);
            if (itIndexTypesIn.hasNext() && itIndexTypesOut.hasNext() && !indexGroupIn.toString().isEmpty() && !indexGroupOut.toString().isEmpty())
                indexString.append(" , ");
        }
        return "D_{" + indexString + "}^{(" + getLabel() + ")}(" + getMomentum().toLaTeX() + ")";
    }

    public String toLaTeX() throws ParseException {
        if (propagatorTemplate == null || propagatorTemplate.chars().allMatch(Character::isWhitespace))
            return "";
        Matcher mtList = substitutePattern.matcher(propagatorTemplate);
        String propagatorString = propagatorTemplate;
        while (mtList.find()) {
            String substitute = mtList.group(1);
            String[] substituteList = substitute.split("\\.");
            String substitution = "";
            try {
                if (substitute.matches("^default$")) {
                    substitution = defaultRepresentation();
                } else if (substitute.matches("^p$")) {
                    substitution = getMomentum().toLaTeX();
                } else if (substitute.matches("^p\\.slash$")) {
                    substitution = getMomentum().toLaTeX(true);
                } else if (substitute.matches("^p\\.vector$")) {
                    substitution = getMomentum().toLaTeX(1, false, true);
                } else if (substitute.matches("^-p$")) {
                    substitution = getMomentum().toLaTeX(-1);
                } else if (substitute.matches("^-p\\.slash$")) {
                    substitution = getMomentum().toLaTeX(-1, true);
                } else if (substitute.matches("^-p\\.vector$")) {
                    substitution = getMomentum().toLaTeX(-1, false, true);
                } else if (substitute.matches("^idxS\\.[a-z]+$")) {
                    int indexType = Index.getIndexType(substituteList[1]);
                    substitution = getStartIndices().get(indexType).get(0).toLaTeX();
                } else if (substitute.matches("^idxE\\.[a-z]+$")) {
                    int indexType = Index.getIndexType(substituteList[1]);
                    substitution = getEndIndices().get(indexType).get(0).toLaTeX();
                } else if (substitute.matches("^idxS\\.[a-z]+\\.\\d+$")) {
                    int indexType = Index.getIndexType(substituteList[1]);
                    int indexNumber = Integer.parseInt(substituteList[2]) - 1;
                    substitution = getStartIndices().get(indexType).get(indexNumber).toLaTeX();
                } else if (substitute.matches("^idxE\\.[a-z]+\\.\\d+$")) {
                    int indexType = Index.getIndexType(substituteList[1]);
                    int indexNumber = Integer.parseInt(substituteList[2]) - 1;
                    substitution = getEndIndices().get(indexType).get(indexNumber).toLaTeX();
                } else if (substitute.matches("^idxS\\.\\d+$")) {
                    int indexType = Integer.parseInt(substituteList[1]) + 3;
                    substitution = getStartIndices().get(indexType).get(0).toLaTeX();
                } else if (substitute.matches("^idxE\\.\\d+$")) {
                    int indexType = Integer.parseInt(substituteList[1]) + 3;
                    substitution = getEndIndices().get(indexType).get(0).toLaTeX();
                } else if (substitute.matches("^idxS\\.\\d+\\.\\d+$")) {
                    int indexType = Integer.parseInt(substituteList[1]) + 3;
                    int indexNumber = Integer.parseInt(substituteList[2]) - 1;
                    substitution = getStartIndices().get(indexType).get(indexNumber).toLaTeX();
                } else if (substitute.matches("^idxE\\.\\d+\\.\\d+$")) {
                    int indexType = Integer.parseInt(substituteList[1]) + 3;
                    int indexNumber = Integer.parseInt(substituteList[2]) - 1;
                    substitution = getEndIndices().get(indexType).get(indexNumber).toLaTeX();
                } else if (substitute.matches("^m")) {
                    substitution = "m_{" + getLabel() + "}";
                } else {
		    throw new ParseException("invalid pattern: " + substitute);
                }
            } catch (IndexOutOfBoundsException e) {
		throw new ParseException("cannot substitute: " + substitute);
            } catch (NumberFormatException e) {
                System.err.println("this should not happen");
            }
            propagatorString = propagatorString.replaceFirst(substituteRegex, Matcher.quoteReplacement(substitution));
        }
        return propagatorString;
    }
}
