//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.*;
import java.util.regex.Matcher;

import org.scilab.forge.jlatexmath.ParseException;

import game.LineConfig;

/**
 * Class representing a vertex in a Feynman diagram.
 */
public class FeynmanVertex implements FeynmanRule {

    /**
     * The overall vertex factors of the Feynman diagram.
     */
    public static Map<String, Integer> vertexFactors = new HashMap<>();

    private final Set<FeynmanParticle> incomingParticles;
    private final Set<FeynmanParticle> outgoingParticles;
    private final Map<String, List<FeynmanParticle>> vertexParticles;
    private final String vertexTemplate;

    /**
     * Constructs a vertex from the incoming and outgoing particles.
     *
     * @param incomingVertexParticles set of the incoming particles
     * @param outgoingVertexParticles set of the outgoing particles
     */
    public FeynmanVertex(Set<FeynmanParticle> incomingVertexParticles, Set<FeynmanParticle> outgoingVertexParticles) {
        this.incomingParticles = incomingVertexParticles;
        this.outgoingParticles = outgoingVertexParticles;
        vertexParticles = new HashMap<>();
        Map<String, Integer> vertexType = new HashMap<>();
        Iterator<FeynmanParticle> itParticles = incomingParticles.iterator();
        while (itParticles.hasNext()) {
            FeynmanParticle particle = itParticles.next();
            String identifier = particle.getIdentifier(false);
            vertexType.merge(identifier, 1, Integer::sum);
            vertexParticles.merge(identifier, new ArrayList<FeynmanParticle>(Collections.singleton(particle)), FeynmanVertex::mergeLists);
        }
        itParticles = outgoingParticles.iterator();
        while (itParticles.hasNext()) {
            FeynmanParticle particle = itParticles.next();
            String identifier = particle.getIdentifier(true);
            vertexType.merge(identifier, 1, Integer::sum);
            vertexParticles.merge(identifier, new ArrayList<FeynmanParticle>(Collections.singleton(particle)), FeynmanVertex::mergeLists);
        }
        vertexTemplate = LineConfig.feynmanRuleTemplates.getVertexTemplate(vertexType);
        String vertexFactor = LineConfig.feynmanRuleTemplates.getVertexFactor(vertexType);
        if (vertexFactor != null && !vertexFactor.chars().allMatch(Character::isWhitespace)) {
            String[] vertexFactorParts = vertexFactor.split("\\^");
            String identifier = vertexFactorParts[0].trim();
            int power = 1;
            if (vertexFactorParts.length == 2) {
                try {
                    power = Integer.parseInt(vertexFactorParts[1]);
                } catch (NumberFormatException e) {
                    System.err.println("undefined power:" + vertexFactorParts[1]);
                }
            }
            vertexFactors.merge(identifier, power, Integer::sum);
        }
    }

    public String defaultRepresentation() {
        StringBuilder interactingParticles = new StringBuilder();
        StringBuilder momentumString = new StringBuilder();
        StringBuilder indexString = new StringBuilder();
        Iterator<FeynmanParticle> itParticles = incomingParticles.iterator();
        while (itParticles.hasNext()) {
            FeynmanParticle particle = itParticles.next();
            interactingParticles.append(particle.getLabel());
            interactingParticles.append(" ");
            momentumString.append(particle.getMomentum().toLaTeX());
            if (itParticles.hasNext())
                momentumString.append(" , ");
            StringBuilder particleIndex = new StringBuilder();
            List<List<Index>> indices = particle.getEndIndices();
            Iterator<List<Index>> itIndexTypes = indices.iterator();
            while (itIndexTypes.hasNext()) {
                StringBuilder indexGroup = new StringBuilder();
                for (Index index : itIndexTypes.next()) {
                    indexGroup.append(index.toLaTeX());
                }
                particleIndex.append(indexGroup);
                if (itIndexTypes.hasNext() && !indexGroup.toString().isEmpty())
                    particleIndex.append(" , ");
            }
            indexString.append(particleIndex);
            if (itParticles.hasNext() && !particleIndex.toString().isEmpty())
                indexString.append(" ;\\, ");
        }
        if (!incomingParticles.isEmpty() && !outgoingParticles.isEmpty()) {
            indexString.append(" ;\\, ");
            momentumString.append(" , ");
        }
        itParticles = outgoingParticles.iterator();
        while (itParticles.hasNext()) {
            FeynmanParticle particle = itParticles.next();
            interactingParticles.append(particle.getLabel());
            interactingParticles.append(" ");
            momentumString.append(particle.getMomentum().toLaTeX(-1));
            if (itParticles.hasNext())
                momentumString.append(" , ");
            StringBuilder particleIndex = new StringBuilder();
            List<List<Index>> indices = particle.getStartIndices();
            Iterator<List<Index>> itIndexTypes = indices.iterator();
            while (itIndexTypes.hasNext()) {
                StringBuilder indexGroup = new StringBuilder();
                for (Index index : itIndexTypes.next()) {
                    indexGroup.append(index.toLaTeX());
                }
                particleIndex.append(indexGroup);
                if (itIndexTypes.hasNext() && !indexGroup.toString().isEmpty())
                    particleIndex.append(" , ");
            }
            indexString.append(particleIndex);
            if (itParticles.hasNext() && !particleIndex.toString().isEmpty())
                indexString.append(" ;\\, ");
        }
        return "V_{(" + interactingParticles + ")}^{" + indexString + "}(" + momentumString + ")";
    }

    public String toLaTeX() throws ParseException {
        if (vertexTemplate == null || vertexTemplate.chars().allMatch(Character::isWhitespace))
            return "";
        Matcher mtList = substitutePattern.matcher(vertexTemplate);
        String vertexString = vertexTemplate;
        while (mtList.find()) {
            String substitute = mtList.group(1);
            String[] substituteList = substitute.split("\\.");
            String substitution = "";
            try {
                if (substitute.matches("^default$")) {
                    substitution = defaultRepresentation();
                } else if (substitute.matches("^\\w+$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    substitution = vertexParticle.getLabel();
                } else if (substitute.matches("^\\w+\\.p$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(sign);
                } else if (substitute.matches("^\\w+\\.\\d+\\.p$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(sign);
                } else if (substitute.matches("^\\w+\\.p\\.slash$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(sign, true);
                } else if (substitute.matches("^\\w+\\.\\d+\\.p\\.slash$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(sign, true);
                } else if (substitute.matches("^\\w+\\.p\\.vector$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(sign, false, true);
                } else if (substitute.matches("^\\w+\\.\\d+\\.p\\.vector$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(sign, false, true);
                } else if (substitute.matches("^\\w+\\.-p$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(-sign);
                } else if (substitute.matches("^\\w+\\.\\d+\\.-p$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(-sign);
                } else if (substitute.matches("^\\w+\\.-p\\.slash$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(-sign, true);
                } else if (substitute.matches("^\\w+\\.\\d+\\.-p\\.slash$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(-sign, true);
                } else if (substitute.matches("^\\w+\\.-p\\.vector$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(-sign, false, true);
                } else if (substitute.matches("^\\w+\\.\\d+\\.-p\\.vector$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int sign = (outgoingParticles.contains(vertexParticle)) ? -1 : 1;
                    substitution = vertexParticle.getMomentum().toLaTeX(-sign, false, true);
                } else if (substitute.matches("^\\w+\\.idx\\.[a-z]+$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int indexType = Index.getIndexType(substituteList[2]);
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(0).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(0).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.\\d+\\.idx\\.[a-z]+$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int indexType = Index.getIndexType(substituteList[3]);
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(0).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(0).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.idx\\.[a-z]+\\.\\d+$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int indexType = Index.getIndexType(substituteList[2]);
                    int indexNumber = Integer.parseInt(substituteList[3]) - 1;
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.\\d+\\.idx\\.[a-z]+\\.\\d+$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int indexType = Index.getIndexType(substituteList[3]);
                    int indexNumber = Integer.parseInt(substituteList[4]) - 1;
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.idx\\.\\d+$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int indexType = Integer.parseInt(substituteList[2]) + 3;
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(0).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(0).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.\\d+\\.idx\\.\\d+$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int indexType = Integer.parseInt(substituteList[3]) + 3;
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(0).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(0).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.idx\\.\\d+\\.\\d+$")) {
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(0);
                    int indexType = Integer.parseInt(substituteList[2]) + 3;
                    int indexNumber = Integer.parseInt(substituteList[3]) - 1;
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.\\d+\\.idx\\.\\d+\\.\\d+$")) {
                    int particleNumber = Integer.parseInt(substituteList[1]) - 1;
                    FeynmanParticle vertexParticle = vertexParticles.get(substituteList[0]).get(particleNumber);
                    int indexType = Integer.parseInt(substituteList[3]) + 3;
                    int indexNumber = Integer.parseInt(substituteList[4]) - 1;
                    if (incomingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getEndIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                    if (outgoingParticles.contains(vertexParticle)) {
                        substitution = vertexParticle.getStartIndices().get(indexType).get(indexNumber).toLaTeX();
                    }
                } else if (substitute.matches("^\\w+\\.m$")) {
                    substitution = "m_{" + vertexParticles.get(substituteList[0]).get(0).getLabel() + "}";
                } else {
		   throw new ParseException("invalid pattern: " + substitute);
                }
            } catch (IndexOutOfBoundsException | NullPointerException e) {
                throw new ParseException("cannot substitute: " + substitute);
            } catch (NumberFormatException e) {
                System.err.println("this should not happen");
            }
            vertexString = vertexString.replaceFirst(substituteRegex, Matcher.quoteReplacement(substitution));
        }
        return vertexString;
    }

    /**
    * Helper function to join two lists.
    *
    * @param <T> the type of elements in this list
    * @param list1 first list
    * @param list2 second list
    *
    * @return the joined list of {@code list1 + list2}
    */
    public static<T> List<T> mergeLists(List<T> list1, List<T> list2) {
        List<T> list = new ArrayList<>(list1);
        list.addAll(list2);
        return list;
    }

    /**
    * Clears the overall vertex factors.
    */
    public static void resetCounter() {
        vertexFactors.clear();
    }
}
