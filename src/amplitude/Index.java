//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.*;

/**
 * Class representing a field index.
 */
public class Index {

    private static final Map<Integer, Integer> indexCounter = new HashMap<>();

    /**
    * add symbols to the list of indices as desired
    * each index is equipped with a subscript
    * the subscript will be incremented automatically if more indices are required than are given in the list
    * should not be empty
    */

    // symbols representing Lorentz indices
    private static final String[] lorentzIndices = {"\\mu", "\\nu", "\\rho", "\\sigma"};
    // symbols representing Dirac indices
    private static final String[] diracIndices = {"\\alpha", "\\beta", "\\gamma", "\\delta"};
    // symbols representing adjoint color indices
    private static final String[] colorAdjointIndices = {"a", "b", "c", "d", "e", "f"};
    // symbols representing fundamental color indices
    private static final String[] colorFundamentalIndices = {"i", "j", "k", "l", "m", "n"};
    // symbols representing auxiliary indices (different types are distinguished by their superscript)
    private static final String[] auxiliaryIndices = {"p", "q", "r", "s", "t"};

    /**
    * add keywords to the pattern strings as desired (may only contain lower case letters [a-z])
    * should not be empty
    */

    // substitution keys for Lorentz indices
    private static final String lorentzPattern = "^(l|lz|lorentz)$";
    // substitution keys for Dirac indices
    private static final String diracPattern = "^(d|di|dirac)$";
    // substitution keys for adjoint color indices
    private static final String colorAdjointPattern = "^(ca|cadj|coloradjoint)$";
    // substitution keys for fundamental color indices
    private static final String colorFundamentalPattern = "^(cf|cfun|colorfundamental)$";

    /**
    * Clears the indices.
    */
    public static void resetCounter() {
        indexCounter.clear();
    }

    /**
    * {@return the integer representing the index type corresponding to the identifier}
    * 0  : Lorentz index,
    * 1  : Dirac index,
    * 2  : color index (adjoint representation),
    * 3  : color index (fundamental representation),
    * -1 : no match.
    *
    * @param identifier specifying the index type
    */
    public static int getIndexType(String identifier) {
        if (identifier.matches(lorentzPattern))
            return 0;
        if (identifier.matches(diracPattern))
            return 1;
        if (identifier.matches(colorAdjointPattern))
            return 2;
        if (identifier.matches(colorFundamentalPattern))
            return 3;
        return -1;
    }

    /**
    * integer representing the type of the index.
    * 0  : Lorentz index,
    * 1  : Dirac index,
    * 2  : color index (adjoint representation),
    * 3  : color index (fundamental representation),
    * 4+ : auxiliary index.
    */
    private final int indexType;
    private final int counter;

    /**
    * Constructs an index of the given type.
    *
    * @param indexType integer specifies the type of the index
    */
    public Index(int indexType) {
        this.indexType = indexType;
        counter = indexCounter.merge(indexType, 1, Integer::sum) - 1;
    }

    /**
     * {@return the {@code LaTeX} representation of the index as defined in the model file}
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public String toLaTeX() {
        String latexString;
        int selection;
        switch (indexType) {
            case 0:
                selection = counter % lorentzIndices.length;
                latexString = lorentzIndices[selection] + "_{" + (counter/lorentzIndices.length + 1) + "}";
                break;
            case 1:
                selection = counter % diracIndices.length;
                latexString =  diracIndices[selection] + "_{" + (counter/diracIndices.length + 1) + "}";
                break;
            case 2:
                selection = counter % colorAdjointIndices.length;
                latexString = colorAdjointIndices[selection] + "_{" + (counter/colorAdjointIndices.length + 1) + "}";
                break;
            case 3:
                selection = counter % colorFundamentalIndices.length;
                latexString = colorFundamentalIndices[selection] + "_{" + (counter/colorFundamentalIndices.length + 1) + "}";
                break;
            default:
                selection = counter % auxiliaryIndices.length;
                latexString = auxiliaryIndices[selection] + "_{" + (counter/auxiliaryIndices.length + 1) + "}^{(" + (indexType - 3) + ")}";
        }
        return " " + latexString + " ";
    }
}
