//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.*;
import java.util.regex.Matcher;

import org.scilab.forge.jlatexmath.ParseException;

import game.FeynLine;
import game.LineConfig;

/**
 * Class representing an external particle in a Feynman diagram.
 */
public class ExternalParticle extends FeynmanParticle {

    /**
     * Types of the external particles.
     */
    enum Type {
        /**
         * Incoming particle
         */
        INCOMING_PARTICLE,
        /**
         * Incoming anti-particle
         */
        INCOMING_ANTIPARTICLE,
        /**
         * Outgoing particle
         */
        OUTGOING_PARTICLE,
        /**
         * Outgoing anti-particle
         */
        OUTGOING_ANTIPARTICLE
    }

    private final Type type;
    private final List<List<Index>> indices;
    private final String[] externalParticleTemplate;

    /**
     * Constructs an external particle from the given line and type.
     *
     * @param line corresponding to an external particle
     * @param type defines whether the (anti-)particle is incoming or outgoing
     */
    public ExternalParticle(FeynLine line, Type type) {
        super(line);
        this.type = type;
        List<Integer> indexRepresentations = LineConfig.feynmanRuleTemplates.getIndexRepresentation(line.lineConfig.identifier);
        indices = new ArrayList<>(indexRepresentations.size());
        for (int i = 0; i < indexRepresentations.size(); i++) {
            indices.add(new ArrayList<>(indexRepresentations.get(i)));
            for (int j = 1; j <= indexRepresentations.get(i); j++) {
                indices.get(i).add(new Index(i));
            }
        }
        externalParticleTemplate = LineConfig.feynmanRuleTemplates.getExternalParticleTemplate(line.lineConfig.identifier);
    }

    /**
     * {@return {@code true} if the external (anti-)particle is incoming}
     */
    public boolean isIncoming() {
        return type == Type.INCOMING_PARTICLE || type == Type.INCOMING_ANTIPARTICLE;
    }

    /**
     * {@return {@code true} if the external (anti-)particle is outgoing}
     */
    public boolean isOutgoing() {
        return type == Type.OUTGOING_PARTICLE || type == Type.OUTGOING_ANTIPARTICLE;
    }

    /**
     * {@return {@code true} if the external particle is an ordinary particle}
     */
    public boolean isParticle() {
        return type == Type.INCOMING_PARTICLE || type == Type.OUTGOING_PARTICLE;
    }

    /**
     * {@return {@code true} if the external particle is an anti-particle}
     */
    public boolean isAntiparticle() {
        return type == Type.INCOMING_ANTIPARTICLE || type == Type.OUTGOING_ANTIPARTICLE;
    }

    /**
     * {@return a nested list of the indices of the external (anti-)particle as defined in the model file}
     * The indices of the particle are specified by the {@code indices} keyword in the model file.
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public List<List<Index>> getIndices() {
        return indices;
    }

    /**
     * {@return a nested list of the indices of the external (anti-)particle} Same as {@code getIndices()}.
     *
     * @see ExternalParticle#getIndices()
     */
    public List<List<Index>> getStartIndices() {
        return getIndices();
    }

    /**
     * {@return a nested list of the indices of the external (anti-)particle} Same as {@code getIndices()}.
     *
     * @see ExternalParticle#getIndices()
     */
    public List<List<Index>> getEndIndices() {
        return getIndices();
    }

    public String defaultRepresentation() {
        StringBuilder indexString = new StringBuilder();
        Iterator<List<Index>> itIndexTypes = getIndices().iterator();
        while (itIndexTypes.hasNext()) {
            StringBuilder indexGroup = new StringBuilder();
            for (Index index : itIndexTypes.next()) {
                indexGroup.append(index.toLaTeX());
            }
            indexString.append(indexGroup);
            if (itIndexTypes.hasNext() && !indexGroup.toString().isEmpty())
                indexString.append(" , ");
        }
        String externalLabel = "";
        String externalInOut = "";
        int momentumSign = 1;
        switch (type) {
            case INCOMING_PARTICLE:
                externalLabel = "D";
                externalInOut = ",in";
                break;
            case INCOMING_ANTIPARTICLE:
                externalLabel = "\\bar{D}";
                externalInOut = ",in";
                momentumSign = -1;
                break;
            case OUTGOING_PARTICLE:
                externalLabel = "D";
                externalInOut = ",out";
                break;
            case OUTGOING_ANTIPARTICLE:
                externalLabel = "\\bar{D}";
                externalInOut = ",out";
                momentumSign = -1;
                break;
        }
        return externalLabel + "_{" + indexString + "}^{(" + getLabel() + externalInOut + ")}(" + getMomentum().toLaTeX(momentumSign) + ")";
    }

    public String toLaTeX() throws ParseException {
        String externalTemplate = "";
        int momentumSign = 1;
        switch (type) {
            case INCOMING_PARTICLE:
                externalTemplate = externalParticleTemplate[0];
                break;
            case INCOMING_ANTIPARTICLE:
                if (externalParticleTemplate[2] == null || externalParticleTemplate[2].chars().allMatch(Character::isWhitespace))
                    externalTemplate = externalParticleTemplate[0];
                else
                    externalTemplate = externalParticleTemplate[2];
                momentumSign = -1;
                break;
            case OUTGOING_PARTICLE:
                externalTemplate = externalParticleTemplate[1];
                break;
            case OUTGOING_ANTIPARTICLE:
                if (externalParticleTemplate[3] == null || externalParticleTemplate[3].chars().allMatch(Character::isWhitespace))
                    externalTemplate = externalParticleTemplate[1];
                else
                    externalTemplate = externalParticleTemplate[3];
                momentumSign = -1;
                break;
        }
        if (externalTemplate == null || externalTemplate.chars().allMatch(Character::isWhitespace))
            return "";
        Matcher mtList = substitutePattern.matcher(externalTemplate);
        String externalString = externalTemplate;
        while (mtList.find()) {
            String substitute = mtList.group(1);
            String[] substituteList = substitute.split("\\.");
            String substitution = "";
            try {
                if (substitute.matches("^default$")) {
                    substitution = defaultRepresentation();
                } else if (substitute.matches("^p$")) {
                    substitution = getMomentum().toLaTeX(momentumSign);
                } else if (substitute.matches("^p\\.slash$")) {
                    substitution = getMomentum().toLaTeX(momentumSign, true);
                } else if (substitute.matches("^p\\.vector$")) {
                    substitution = getMomentum().toLaTeX(momentumSign, false, true);
                } else if (substitute.matches("^-p$")) {
                    substitution = getMomentum().toLaTeX(-momentumSign);
                } else if (substitute.matches("^-p\\.slash$")) {
                    substitution = getMomentum().toLaTeX(-momentumSign, true);
                } else if (substitute.matches("^-p\\.vector$")) {
                    substitution = getMomentum().toLaTeX(-momentumSign, false, true);
                } else if (substitute.matches("^idx\\.[a-z]+$")) {
                    int indexType = Index.getIndexType(substituteList[1]);
                    substitution = getIndices().get(indexType).get(0).toLaTeX();
                } else if (substitute.matches("^idx\\.[a-z]+\\.\\d+$")) {
                    int indexType = Index.getIndexType(substituteList[1]);
                    int indexNumber = Integer.parseInt(substituteList[2]) - 1;
                    substitution = getIndices().get(indexType).get(indexNumber).toLaTeX();
                } else if (substitute.matches("^idx\\.\\d+$")) {
                    int indexType = Integer.parseInt(substituteList[1]) + 3;
                    substitution = getIndices().get(indexType).get(0).toLaTeX();
                } else if (substitute.matches("^idx\\.\\d+\\.\\d+$")) {
                    int indexType = Integer.parseInt(substituteList[1]) + 3;
                    int indexNumber = Integer.parseInt(substituteList[2]) - 1;
                    substitution = getIndices().get(indexType).get(indexNumber).toLaTeX();
                } else if (substitute.matches("^m")) {
                    substitution = "m_{" + getLabel() + "}";
                } else {
		    throw new ParseException("invalid pattern: " + substitute);
                }
            } catch (IndexOutOfBoundsException e) {
		throw new ParseException("cannot substitute: " + substitute);
            } catch (NumberFormatException e) {
                System.err.println("this should not happen");
            }
            externalString = externalString.replaceFirst(substituteRegex, Matcher.quoteReplacement(substitution));
        }
        return externalString;
    }
}
