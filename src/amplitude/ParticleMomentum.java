//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.*;

/**
 * Class representing the four-momentum of a particle.
 */
public class ParticleMomentum {

    private final Map<String, Integer> momenta;

    /**
     * Constructs a zero four-momentum.
     */
    public ParticleMomentum() {
        momenta = new HashMap<>();
    }

    /**
     * Constructs a four-momentum of the given ({@code Latex}-)string.
     *
     * @param momentumLabel represents the four-momentum
     */
    public ParticleMomentum(String momentumLabel) {
        this(momentumLabel, 1);
    }

    /**
     * Constructs a multiple of the four-momentum of the given ({@code Latex}-)string.
     *
     * @param momentumLabel represents the four-momentum
     * @param multiplicity integer prefactor
     */
    public ParticleMomentum(String momentumLabel, int multiplicity) {
        momenta = new HashMap<>();
        momenta.put(momentumLabel, multiplicity);
    }

    /**
     * {@return a copy of the four-momentum}
     */
    public Map<String, Integer> getMomenta() {
        return new HashMap<>(momenta);
    }

    /**
     * Adds a four-momentum of the given ({@code Latex}-)string to the existing four-momentum.
     *
     * @param momentumLabel represents the four-momentum to add
     */
    public void addMomentum(String momentumLabel) {
        momenta.merge(momentumLabel, 1, Integer::sum);
    }

    /**
     * Adds a given four-momentum to the existing four-momentum.
     *
     * @param otherMomentum four-momentum to add
     */
    public void addMomentum(ParticleMomentum otherMomentum) {
        otherMomentum.getMomenta().forEach((k, v) -> momenta.merge(k, v, Integer::sum));
    }

    /**
     * Subtracts a four-momentum of the given ({@code Latex}-)string from the existing four-momentum.
     *
     * @param momentumLabel represents the four-momentum to subtract
     */
    public void subtractMomentum(String momentumLabel) {
        momenta.merge(momentumLabel, -1, Integer::sum);
    }

    /**
     * Subtracts a given four-momentum from the existing four-momentum.
     *
     * @param otherMomentum four-momentum to subtract
     */
    public void subtractMomentum(ParticleMomentum otherMomentum) {
        otherMomentum.getMomenta().forEach((k, v) -> momenta.merge(k, -v, Integer::sum));
    }

    /**
     * {@return the number of different labels that make up the total four-momentum}
     */
    public int momentumLength() {
        int counter = 0;
        for (int multiplicity : momenta.values()) {
            if (multiplicity != 0)
                ++counter;
        }
        return counter;
    }

    /**
     * {@return the {@code LaTeX} representation of the momentum as defined in the model file}
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public String toLaTeX() {
        return toLaTeX(1, false, false);
    }

    /**
     * {@return the multiplicity times the {@code LaTeX} representation of the momentum as defined in the model file}
     *
     * @param multiplicity overall factor to be multiplied with the momentum
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public String toLaTeX(int multiplicity) {
        return toLaTeX(multiplicity, false, false);
    }

    /**
     * {@return the (possibly slashed) {@code LaTeX} representation of the momentum as defined in the model file}
     *
     * @param slashed whether or not the momentum should be slashed
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public String toLaTeX(boolean slashed) {
        return toLaTeX(1, slashed, false);
    }

    /**
     * {@return the multiplicity times the (possibly slashed) {@code LaTeX} representation of the momentum as defined in the model file}
     *
     * @param multiplicity overall factor to be multiplied with the momentum
     * @param slashed whether or not the momentum should be slashed
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public String toLaTeX(int multiplicity, boolean slashed) {
        return toLaTeX(multiplicity, slashed, false);
    }

    /**
     * {@return the multiplicity times the (possibly slashed) {@code LaTeX} representation of the (possibly three-)momentum as defined in the model file}
     * The momentum can either be slashed or a three-momentum but not both.
     *
     * @param multiplicity overall factor to be multiplied with the momentum
     * @param slashed whether or not the momentum should be slashed
     * @param vector whether or not a vector arrow should be added to the momentum
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    public String toLaTeX(int multiplicity, boolean slashed, boolean vector) {
        StringBuilder momentumString = new StringBuilder();
        for (Map.Entry<String, Integer> momentum : momenta.entrySet()) {
            StringBuilder momentumLabel = new StringBuilder(momentum.getKey());
            if (slashed) {
                momentumLabel.insert(1,"\\!\\!\\!/}");
                momentumLabel.insert(0,"{");
            } else if (vector) {
                momentumLabel.insert(1,"}");
                momentumLabel.insert(0,"\\vec{");
            }
            int momentumMultiplicity = multiplicity * momentum.getValue();
            if (momentumMultiplicity > 0) {
                if (!momentumString.toString().isEmpty())
                    momentumString.append(" + ");
                if (momentumMultiplicity != 1)
                    momentumString.append(momentumMultiplicity);
                momentumString.append(momentumLabel);
            } else if (momentumMultiplicity < 0) {
                momentumString.append(" - ");
                if (momentumMultiplicity != -1)
                    momentumString.append(-momentumMultiplicity);
                momentumString.append(momentumLabel);
            }
        }
        String finalMomentumString = momentumString.toString();
        if (finalMomentumString.chars().allMatch(Character::isWhitespace))
            return "0";
        if (momentumLength() > 1 || finalMomentumString.trim().startsWith("-") || finalMomentumString.trim().startsWith("+"))
            return " ( " + finalMomentumString + " ) ";
        return finalMomentumString;
    }
}
