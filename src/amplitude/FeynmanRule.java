//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.regex.Pattern;

/**
 * Interface for objects that correspond to Feynman rules in the amplitude.
 */
public interface FeynmanRule {

    /**
     * The regular expression used to find the substitution patterns.
     */
    String substituteRegex = "<([^>]+)>";

    /**
     * The compiled pattern used to substitute the placeholders.
     */
    Pattern substitutePattern = Pattern.compile(substituteRegex);

    /**
     * {@return a default {@code LaTeX} representation of the Feynman rule}
     */
    String defaultRepresentation();

    /**
     * {@return the {@code LaTeX} representation of the Feynman rule as defined in the model file}
     *
     * @see <a href="file:../../FeynGameJava/template/models/amplitude.model" target="_top">models/amplitude.model</a>
     */
    String toLaTeX();
}
