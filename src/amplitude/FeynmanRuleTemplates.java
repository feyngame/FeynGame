//=======================================================================================
//  FeynGame - a tool for easily creating Feynman diagrams
//  Copyright (C) 2019-2023 Robert Harlander, Sven Yannick Klein,
//  Maximilian Lipp and Magnus Schaaf
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//=======================================================================================

package amplitude;

import java.util.*;

/**
 * Class used to store the Feynman rule templates given in the model file.
 */
public class FeynmanRuleTemplates {

    private final Map<Map.Entry<String, String>, List<Integer>> indexRepresentations;
    private final Map<Map.Entry<String, String>, String> propagatorTemplates;
    private final Map<Map.Entry<String, String>, String[]> externalParticleTemplates;
    private final Map<Map.Entry<String, String>, Boolean> anticommuting;
    private final List<Map.Entry<Map<String, Integer>, String>> vertexTemplates;
    private final List<Map.Entry<Map<String, Integer>, String>> vertexFactors;

    /**
     * Constructs an empty set of templates.
     */
    public FeynmanRuleTemplates() {
        indexRepresentations = new HashMap<>();
        propagatorTemplates = new HashMap<>();
        externalParticleTemplates = new HashMap<>();
        anticommuting = new HashMap<>();
        vertexTemplates = new ArrayList<>();
        vertexFactors = new ArrayList<>();
    }
    /**
     * Copy constructor.
     *
     * @param feynmanRuleTemplates that should be copied
     */
    public FeynmanRuleTemplates(FeynmanRuleTemplates feynmanRuleTemplates) {
        indexRepresentations = new HashMap<>();
        for (Map.Entry<Map.Entry<String, String>, List<Integer>> indexRepresentationEntry : feynmanRuleTemplates.indexRepresentations.entrySet()) {
            Map.Entry<String, String> identifier = indexRepresentationEntry.getKey();
            List<Integer> indexRepresentation = indexRepresentationEntry.getValue();
            indexRepresentations.put(new AbstractMap.SimpleEntry<>(identifier.getKey(), identifier.getValue()), new ArrayList<Integer>(indexRepresentation));
        }
        propagatorTemplates = new HashMap<>();
        for (Map.Entry<Map.Entry<String, String>, String> propagatorTemplateEntry : feynmanRuleTemplates.propagatorTemplates.entrySet()) {
            Map.Entry<String, String> identifier = propagatorTemplateEntry.getKey();
            String propagatorTemplate = propagatorTemplateEntry.getValue();
            propagatorTemplates.put(new AbstractMap.SimpleEntry<>(identifier.getKey(), identifier.getValue()), propagatorTemplate);
        }
        externalParticleTemplates = new HashMap<>();
        for (Map.Entry<Map.Entry<String, String>, String[]> externalParticleTemplateEntry : feynmanRuleTemplates.externalParticleTemplates.entrySet()) {
            Map.Entry<String, String> identifier = externalParticleTemplateEntry.getKey();
            String[] externalParticleTemplate = externalParticleTemplateEntry.getValue();
            externalParticleTemplates.put(new AbstractMap.SimpleEntry<>(identifier.getKey(), identifier.getValue()), externalParticleTemplate.clone());
        }
        anticommuting = new HashMap<>();
        for (Map.Entry<Map.Entry<String, String>, Boolean> anticommutingEntry : feynmanRuleTemplates.anticommuting.entrySet()) {
            Map.Entry<String, String> identifier = anticommutingEntry.getKey();
            boolean isAnticommuting = anticommutingEntry.getValue();
            anticommuting.put(new AbstractMap.SimpleEntry<>(identifier.getKey(), identifier.getValue()), isAnticommuting);
        }
        vertexTemplates = new ArrayList<>();
        for (Map.Entry<Map<String, Integer>, String> vertexTemplateEntry : feynmanRuleTemplates.vertexTemplates) {
            Map<String, Integer> vertex = new HashMap<>();
            for (Map.Entry<String, Integer> particle : vertexTemplateEntry.getKey().entrySet()) {
                vertex.put(particle.getKey(), particle.getValue());
            }
            vertexTemplates.add(new AbstractMap.SimpleEntry<>(vertex, vertexTemplateEntry.getValue()));
        }
        vertexFactors = new ArrayList<>();
        for (Map.Entry<Map<String, Integer>, String> vertexFactorEntry : feynmanRuleTemplates.vertexFactors) {
            Map<String, Integer> vertex = new HashMap<>();
            for (Map.Entry<String, Integer> particle : vertexFactorEntry.getKey().entrySet()) {
                vertex.put(particle.getKey(), particle.getValue());
            }
            vertexFactors.add(new AbstractMap.SimpleEntry<>(vertex, vertexFactorEntry.getValue()));
        }
    }

    /**
     * Clears all templates.
     */
    public void clear() {
        indexRepresentations.clear();
        propagatorTemplates.clear();
        externalParticleTemplates.clear();
        anticommuting.clear();
        vertexTemplates.clear();
        vertexFactors.clear();
    }

    /**
     * Adds the index representation of a given particle.
     *
     * @param particle pair of strings that identify the particle uniquely
     * @param indexRepresentation specifying the indices of the particle
     */
    public void addIndexRepresentation(Map.Entry<String, String> particle, List<Integer> indexRepresentation) {
        indexRepresentations.put(particle, indexRepresentation);
    }

    /**
     * {@return the representation specifying the indices of the particle}
     *
     * @param particle pair of strings that identify the particle uniquely
     */
    public List<Integer> getIndexRepresentation(Map.Entry<String, String> particle) {
        return indexRepresentations.getOrDefault(particle, new ArrayList<Integer>());
    }

    /**
     * Adds the propagator template of a given particle.
     *
     * @param particle pair of strings that identify the particle uniquely
     * @param propagatorTemplate ({@code LaTeX})-string representing the propagator
     */
    public void addPropagatorTemplate(Map.Entry<String, String> particle, String propagatorTemplate) {
        propagatorTemplates.put(particle, propagatorTemplate);
    }

    /**
     * {@return the ({@code LaTeX})-string representing the propagator of the particle}
     *
     * @param particle pair of strings that identify the particle uniquely
     */
    public String getPropagatorTemplate(Map.Entry<String, String> particle) {
        return propagatorTemplates.get(particle);
    }

    /**
     * Adds the external particle template of a given particle.
     *
     * @param particle pair of strings that identify the particle uniquely
     * @param externalParticleTemplate array of ({@code LaTeX})-strings representing the Feynman rules of the external particle
     */
    public void addExternalParticleTemplate(Map.Entry<String, String> particle, String[] externalParticleTemplate) {
        externalParticleTemplates.put(particle, externalParticleTemplate);
    }

    /**
     * {@return the array of ({@code LaTeX})-strings representing the Feynman rules of the external particle}
     *
     * @param particle pair of strings that identify the particle uniquely
     */
    public String[] getExternalParticleTemplate(Map.Entry<String, String> particle) {
        return externalParticleTemplates.getOrDefault(particle, new String[4]);
    }

    /**
     * Set the commutation properties of a given particle.
     *
     * @param particle pair of strings that identify the particle uniquely
     * @param isAnticommuting whether or not the particle anti-commutes with its kind
     */
    public void setAnticommuting(Map.Entry<String, String> particle, boolean isAnticommuting) {
        anticommuting.put(particle, isAnticommuting);
    }

    /**
     * {@return whether or not the particle anti-commutes with its kind}
     *
     * @param particle pair of strings that identify the particle uniquely
     */
    public boolean isAnticommuting(Map.Entry<String, String> particle) {
        return anticommuting.getOrDefault(particle, false);
    }

    /**
     * Adds the vertex template of a given vertex.
     *
     * @param vertex map of strings and integers that identify the vertex uniquely
     * @param vertexTemplate ({@code LaTeX})-string representing the Feynman rule of the vertex
     */
    public void addVertexTemplate(Map<String, Integer> vertex, String vertexTemplate) {
        vertexTemplates.add(new AbstractMap.SimpleEntry<>(vertex, vertexTemplate));
    }

    /**
     * {@return the ({@code LaTeX})-string representing the Feynman rule of the vertex}
     *
     * @param vertex map of strings and integers that identify the vertex uniquely
     */
    public String getVertexTemplate(Map<String, Integer> vertex) {
        for (Map.Entry<Map<String, Integer>, String> rule : vertexTemplates) {
            if (rule.getKey().equals(vertex)) {
                if (rule.getValue() == null || rule.getValue().chars().allMatch(Character::isWhitespace))
                    return null;
                return rule.getValue();
            }
        }
        return null;
    }

    /**
     * Adds the vertex factor of a given vertex.
     *
     * @param vertex map of strings and integers that identify the vertex uniquely
     * @param vertexFactor ({@code LaTeX})-string representing an overall factor of the Feynman rule of the vertex
     */
    public void addVertexFactor(Map<String, Integer> vertex, String vertexFactor) {
        vertexFactors.add(new AbstractMap.SimpleEntry<>(vertex, vertexFactor));
    }

    /**
     * {@return the ({@code LaTeX})-string representing an overall factor of the Feynman rule of the vertex}
     *
     * @param vertex map of strings and integers that identify the vertex uniquely
     */
    public String getVertexFactor(Map<String, Integer> vertex) {
        for (Map.Entry<Map<String, Integer>, String> rule : vertexFactors) {
            if (rule.getKey().equals(vertex)) {
                if (rule.getValue() == null || rule.getValue().chars().allMatch(Character::isWhitespace))
                    return null;
                return rule.getValue();
            }
        }
        return null;
    }

}
