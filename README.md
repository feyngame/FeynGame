<h1><tt>FeynGame</tt></h1>

<p><tt>FeynGame</tt> is a Java-based graphical tool for drawing Feynman diagrams.
It is based on models, consisting of particles
(lines) and (optionally) vertices, each of which can be given their
individual properties (line style, color, arrows, label, etc.). The
diagrams can be exported in any standard image format, or as
EPS. Aside from its plain graphical aspect, the goal of <tt>FeynGame</tt> is
also educative, as it can check a Feynman diagrams validity. This
provides the basis to play games with diagrams, for example. As of now a game where a given set of initial and final states
must be connected through a Feynman diagram within a given interaction
model is available.</p>

<p>A pre-compiled version is available <a href="http://www.robert-harlander.de/software/feyngame">here</a>.

<p>Please refer to these papers when using <tt>FeynGame</tt>:
<ul>
<li>R. Harlander, S.Y. Klein, M. Schaaf, <i>FeynGame 2.1 &ndash; Feynman diagrams made easy</i>,
<a href="https://doi.org/10.22323/1.449.0657">PoS EPS-HEP2023 (2024) 657</a>,
<a href="https://arxiv.org/abs/2401.12778">[arXiv:2401.12778]</a></li>
<li>R. Harlander, S.Y. Klein, M. Lipp, <i>FeynGame</i>,
<a href="https://dx.doi.org/10.1016/j.cpc.2020.107465">Comput. Phys. Commun. 256 (2020) 107465</a>,
<a href="https://arxiv.org/abs/2003.00896">[arXiv:2003.00896]</a></li>
</ul>

<h3>Third party libraries</h3>

<tt>FeynGame</tt> uses the following third party libraries:

<ul>
<li><a href="https://github.com/opencollab/jlatexmath">JLaTeXMath</a> (License: <a href="https://opensource.org/licenses/gpl-2.0.php">GPL-2.0</a>)</li>
</ul>

<ul>
<li><a href="https://jgrapht.org/">JGraphT</a> (License: <a href="https://jgrapht.org/#ack">LGPL-2.1-or-later</a>)</li>
</ul>

<ul>
<li><a href="http://www.jheaps.org/">JHeaps</a> (License: <a href="http://www.jheaps.org/about/">Apache-2.0</a>)</li>
</ul>

<h3>Implemented algorithms</h3>

<tt>FeynGame</tt> implements the following third party algorithms:

<ul>
<li>an algorithm to determine the distance between an ellipse and a point by
<a href="https://www.geometrictools.com/Documentation/DistancePointEllipseEllipsoid.pdf">David Eberly</a> (License: <a href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>)</li>
</ul>

<h2>Requirements</h2>

<ul><li><a href="https://www.oracle.com/technetwork/java/javase/overview/index.html">Java Development Kit</a> >= 8</li></ul>

<h2>Installation</h2>

<h3>Linux/MacOs</h3>

<p>On Linux and MacOS you can use the <a href="buildfile">"buildfile" bash script</a> to compile the source files once they are downloaded. The script compiles the source files and compresses them into a JAR file in the <code>bin</code> directory. It also creates an executable for FeynGame in the same directory. The names of both the JAR and the executable are 'FeynGame-' appended by the Version of Java used to compile the code. The JAR then has '.jar' as a file ending. On MacOS additionally a '.app' Package is created.</p>

<p>The Java target release can be specified by passing it to <code>buildfile</code> with the -t flag. For example,</p>

<pre><code class="language-bash">./buildfile -t 8</code></pre>

should produce code compatible with Java-8. This option should only be used if the default compilation fails.

<p>Calling <code>buildfile</code> with the -j flag will produce a documentation with Javadoc. The documentation will be stored in	<code>Doc/</code>:</p>

<pre><code class="language-bash">./buildfile -j # create documentation</code></pre>

<p>FeynGame can also be installed via the -i flag followed by the installation prefix:</p>

<pre><code class="language-bash">./buildfile -i $prefix # install to $prefix</code></pre>

<p>The executable is copied to <code>$prefix/bin/</code> and a symbolic link to <code>$prefix/bin/feyngame</code> is created. The licenses are copied to <code>$prefix/share/licenses/FeynGame</code> and the JAR file to <code>$prefix/share/java/</code>. On MacOs, the '.app' Package is copied to <code>$prefix/Applications/</code>. Furthermore the aliases "global", "local", and "user" can be used. The former sets the prefix to "/" while the latter two use the users home directory as installation prefix. When the prefix is "global" or "/" on MacOS, the .app Package gets installed to <code>/Applications/</code> while the other files use the prefix <code>/usr/local</code>.



<h3>Windows</h3>

<p>On Windows you can use the <a href="buildfile.cmd">"buildfile.cmd" batch script</a>. It has the same functionality as the bash script above with the same flags that one can pass. It does not create an executable but only the JAR though.</p>

<h2>Executing the application</h2>

<p>To run <code>FeynGame</code>, one opens a console window in the folder <code>bin</code>, and executes the command</p>

<pre><code class="language-bash">./FeynGame-$java_version</code></pre>

<p>If this fails for some reason or you are running Windows, you can replace this call (here and below) by</p>

<pre><code class="language-bash">java -jar FeynGame-$java_version.jar</code></pre>

<p>In order to start <code>FeynGame</code> directly in drawing mode, one has to specify a model file:</p>

<pre><code class="language-bash">./FeynGame-$java_version $path_to_model_file</code></pre>

<p>A number of pre-defined models are provided in <code><a href="examples/models">expamples/models</a></code>.
To run <code>FeynGame</code> with the Standard Model, for example, one thus can run</p>

<pre><code class="language-bash">./FeynGame-$java_version ../expamples/models/standard.model</code></pre>

from the <code>bin</code> directory.
<p>A generic template model file is given in <code><a href="examples/models/template.model">examples/models/template.model</a></code>.
It exemplifies the main features of a model file.</p>

<p>FeynGame can also be started by specifying a diagram (".fg") file in the command line:</p>

<pre><code class="language-bash">./FeynGame-$java_version $path_to_diagram_file</code></pre>

<p>It is also possible to specify a model and a diagram file at the same time.</p>

<p>In order to start <code>FeynGame</code> directly in the InFin game mode, one has to specify a level file:<\p>

<pre><code class="language-bash">./FeynGame-$java_version $path_to_level_file</code></pre>

<p>One can also specify the maximum time for the game in seconds as follows:<\p>

<pre><code class="language-bash">./FeynGame-$java_version $time_interval $path_to_level_file</code></pre>

<p>To directly export a diagram saved as a ".fg" file to a desired file format one has to specify a diagram file and the file that should be exported to:</p>

<pre><code class="language-bash">./FeynGame-$java_version $path_to_exported_file $path_to_diagram_file</code></pre>

<p>To show all available command line options, use the "-h" or "--help" options.

<h3>Portability</h3>

Once FeynGame is built, the directory <code>bin</code> can be moved freely
on a single machine as well as in between different machines and executed with the commands above as long as the Java Runtime Environment is installed. Of course also the executable jar <code>bin/FeynGame.jar</code> can be moved and be executed without the here given model files.
