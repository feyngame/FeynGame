@echo off

Pushd "%~dp0"

SET Target=0
SET doc=0
SET gen=0
SET debug=0

:processargs
SET ARG=%1
IF DEFINED ARG (
  IF "%ARG%"=="-t" (
    SHIFT
    SET Target=%2
  )
  IF "%ARG%"=="-j" (
    SET doc=1
  )
  IF "%ARG%"=="-g" (
    SET gen=1
  )
  IF "%ARG%"=="-d" (
    SET debug=1
  )
  IF "%ARG%"=="-h" (
    ECHO This is a batch script to build FeynGame.
    ECHO Options:
    ECHO   -t    Set the target Java version.
    ECHO   -j    Generate documentation with JavaDoc
    ECHO   -g    Also build the Level Generator
    ECHO   -d    Debug mode
    ECHO   -h    Show this help
    EXIT
  )
  IF NOT "%ARG%"=="-t" (
    IF NOT "%ARG%"=="-j" (
      IF NOT "%ARG%"=="-g" (
        IF NOT "%ARG%"=="-d" (
          IF NOT "%ARG%"=="-h" (
            ECHO Invalid Option: %ARG%
            EXIT
          )
        )
      )
    )
  )
  SHIFT
  GOTO processargs
)

IF "%debug%"=="1" (
  ECHO Using debug mode.
)

IF "%Target%"=="0" (
  ECHO Building the Classes ...
  cd src\
  IF "%debug%"=="1" (
    javac -Xlint:all -Xmaxwarns 300 -deprecation -g -verbose -cp ..\lib\*;. -d ..\build FeynGame.java
  ) ELSE (
    javac -Xlint:none -nowarn -cp ..\lib\*;. -d ..\build FeynGame.java
  )
) ELSE (
  ECHO Building with Java Version %Target%
  ECHO Building the Classes ...
  cd src\
  IF "%debug%"=="1" (
    javac -cp ../lib/*;. --release %Target% -Xlint:all -Xmaxwarns 300 -deprecation -g -verbose -d ..\build FeynGame.java
  ) ELSE (
    javac -cp ../lib/*;. --release %Target% -nowarn -Xlint:none -d ..\build FeynGame.java
  )
)

cd ..
ECHO Copying needed files ...
md build\resources\
md build\javafx\

robocopy src\resources\ build\resources /e /xf GetResources.java

robocopy src\javafx\ build\javafx /e /xf JavaFXToJavaVectorGraphic.java

copy src\version.txt build\

ECHO Copying required dependencies ...
cd build\
jar -xf ..\lib\jlatexmath-1.0.7.jar org
jar -xf ..\lib\jheaps-0.11.jar org
jar -xf ..\lib\jgrapht-core-1.4.0.jar org
cd ..\

ECHO Creating needed directories and files ...
md bin
copy NUL bin\FeynGame.jar
ECHO Compressing the built program ...

IF "%debug%"=="1" (
  jar -cvmf src\META-INF\MANIFEST.MF bin\FeynGame.jar -C build\ FeynGame.class -C build\ OptionException.class -C build\ OptionParser.class -C build\ OptionParser$1.class -C build\ ui -C build\ game -C build\ amplitude -C build\ version.txt -C build\ resources -C build\ javafx -C build/ org -C build\ export -C build/ qgraf -C build/ layout
) ELSE (
  jar -cmf src\META-INF\MANIFEST.MF bin\FeynGame.jar -C build\ FeynGame.class -C build\ OptionException.class -C build\ OptionParser.class -C build\ OptionParser$1.class -C build\ ui -C build\ game -C build\ amplitude -C build\ version.txt -C build\ resources -C build\ javafx -C build/ org -C build\ export -C build/ qgraf -C build/ layout
)


IF "%doc%"=="1" (
  ECHO Generating Javadoc ...
  IF "%debug%"=="1" (
    javadoc -Xdocline:all -cp lib\*;. -verbose src\FeynGame.java -sourcepath src\ ui game amplitude resources javafx export -public -private -d Doc
  ) ELSE (
    javadoc -Xdoclint:none -cp lib\*;. -quiet src\FeynGame.java -sourcepath src\ ui game amplitude resources javafx export -public -private -d Doc
  )
)

IF "%gen%"=="1" (
  ECHO Building Level Generator ...
  cd src\levelgenerator
  IF "%Target%"=="0" (
    IF "%debug%"=="1" (
      javac -Xlint:all -Xmaxwarns 300 -deprecation -g -verbose -d ..\..\build\levelgenerator LevelGenerator.java
    ) ELSE (
      javac -Xlint:none -nowarn -d ..\..\build\levelgenerator LevelGenerator.java
    )
  ) ELSE (
    IF "%debug%"=="1" (
      javac --release %Target% -Xlint:all -Xmaxwarns 300 -deprecation -g -verbose -d ..\..\build\levelgenerator LevelGenerator.java
    ) ELSE (
      javac --release %Target% -Xlint:none -nowarn -d ..\..\build\levelgenerator LevelGenerator.java
    )
  )
  cd ..\..
  copy NUL bin\LevelGenerator.jar
  IF "%debug%"=="1" (
    jar -cvmf src\META-INF\MANIFEST2.MF bin\LevelGenerator.jar -C build\levelgenerator LevelGenerator.class -C build levelgenerator
  ) ELSE (
    jar -cmf src\META-INF\MANIFEST2.MF bin\LevelGenerator.jar -C build\levelgenerator LevelGenerator.class -C build levelgenerator
  )
)
