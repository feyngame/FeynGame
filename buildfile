#!/bin/bash
#
# -t: set java version (if required)
# -i: set installation path
#

cd "$(dirname "$0")"

while getopts "t:i:jgdh" opt; do
  case $opt in
  t)
    Target=$OPTARG >&2
    ;;
  i)
    Prefix=$OPTARG
    ;;
  j)
    Javadoc=1
    ;;
  g)
    LevelGenerator=1
    ;;
  d)
    Debug=1
    ;;
  h)
    echo "This is a bash script to build FeynGame."
    echo "Options:"
    echo "  -t    Set the target Java version"
    echo "  -i    Install files under the specified prefix"
    echo "  -j    Generate documentation with JavaDoc"
    echo "  -g    Also build the Level Generator"
    echo "  -d    Debug mode"
    echo "  -h    Show this help"
    exit 1
    ;;
  \?)
    echo "Invalid option: -$OPTARG" >&2
    exit 1
    ;;
  esac
done


if [[ "${Prefix}" == "global" ]]; then
  Prefix="/"
fi
if [[ "${Prefix}" == "user" ]]; then
  Prefix="${HOME}"
fi
if [[ "${Prefix}" == "local" ]]; then
  Prefix="${HOME}/.local"
fi

if [[ -n "$Prefix" ]]; then
  Prefix=$(realpath $Prefix)
fi

JavaMajorVersion=$(java -version 2>&1 | grep "version" | head -1 | cut -d'"' -f2 | sed '/^1\./s///' | cut -d'.' -f1)
date_last_commit=$(git log -1 --date=format:"%d.%m.%Y %H:%M:%S" --format="%ad")

version=$(cat ./src/version.txt)

if [[ $date_last_commit == *"fatal"* ]]; then
  date_last_commit=""
  echo -e "Building FeynGame version $version"
else
  echo -e "Building FeynGame version $version (last change: $date_last_commit)"
fi

if [[ -n "${Debug}" ]]; then
  echo "Using debug mode."
  sed 's/public static boolean DEBUG_MODE = false/public static boolean DEBUG_MODE = true/g' ./src/ui/Frame.java &> ./Frame.java
  mv ./Frame.java ./src/ui/Frame.java
else
  sed 's/public static boolean DEBUG_MODE = true/public static boolean DEBUG_MODE = false/g' ./src/ui/Frame.java &> ./Frame.java
  mv ./Frame.java ./src/ui/Frame.java
fi

(
cd src/
if [[ -n "${Target}" ]]; then
  echo -e "Building with Java Version $Target"
  # patch
  if [[ "${Target}" -lt 9 ]]; then
    echo -e "Patching..."
    patch -p1 < ../java8.patch
  fi
  echo -e "Building the Classes ..."
  if [[ -n "${Debug}" ]]; then
    javac -Xlint:all -Xmaxwarns 300 -deprecation -g -verbose -cp ../lib/*:. --release "$Target" -d ../build FeynGame.java
  else
    javac -Xlint:none -nowarn -cp ../lib/*:. --release "$Target" -d ../build FeynGame.java
  fi
  # revert patch
  if [[ "${Target}" -lt 9 ]]; then
    echo -e "Reverting patch..."
    patch -p1 -R < ../java8.patch
  fi
else
  # patch
  if [[ "${JavaMajorVersion}" -lt 9 ]]; then
    echo -e "Patching..."
    patch -p1 < ../java8.patch
  fi
  echo -e "Building the Classes ..."
  if [[ -n "${Debug}" ]]; then
    javac -Xlint:all -Xmaxwarns 300 -deprecation -g -verbose -cp ../lib/*:. -d ../build FeynGame.java
  else
    javac -Xlint:none -nowarn -cp ../lib/*:. -d ../build FeynGame.java
  fi
  # revert patch
  if [[ "${JavaMajorVersion}" -lt 9 ]]; then
    echo -e "Reverting patch..."
    patch -p1 -R < ../java8.patch
  fi
fi
)

echo -e "Copying needed files ..."
mkdir -p build/resources/
mkdir -p build/javafx/

cp -rf src/resources build/
cp -rf src/javafx build/
rm build/resources/GetResources.java
rm build/javafx/JavaFXToJavaVectorGraphic.java
echo -e "$version | $date_last_commit" > build/version.txt

echo -e "Copying required dependencies ..."
(
cd build/
jar -xf ../lib/jlatexmath-1.0.7.jar org
jar -xf ../lib/jheaps-0.11.jar org
jar -xf ../lib/jgrapht-core-1.4.0.jar org
)

echo -e "Creating needed directories and files ..."
mkdir -p bin
if [[ -n "${Target}" ]]; then
  FeynGameJar="FeynGame-$Target.jar"
  FeynGameExe="FeynGame-$Target"
else
  FeynGameJar="FeynGame-$JavaMajorVersion.jar"
  FeynGameExe="FeynGame-$JavaMajorVersion"
fi
touch bin/"$FeynGameJar"

echo -e "Compressing the built program ..."

if [[ -n "${Debug}" ]]; then
  jar -cvmf src/META-INF/MANIFEST.MF bin/"$FeynGameJar" -C build/ FeynGame.class -C build/ OptionException.class -C build/ OptionParser.class -C build/ 'OptionParser$1.class' -C build/ ui -C build/ game -C build/ amplitude -C build/ version.txt -C build/ resources -C build/ javafx -C build/ org -C build/ export -C build/ qgraf -C build/ layout
else
  jar -cmf src/META-INF/MANIFEST.MF bin/"$FeynGameJar" -C build/ FeynGame.class -C build/ OptionException.class -C build/ OptionParser.class -C build/ 'OptionParser$1.class' -C build/ ui -C build/ game -C build/ amplitude -C build/ version.txt -C build/ resources -C build/ javafx -C build/ org -C build/ export -C build/ qgraf -C build/ layout
fi

echo '#!/usr/bin/env -S java -jar' > bin/"$FeynGameExe"
cat bin/"$FeynGameJar" >> bin/"$FeynGameExe"
chmod +x bin/"$FeynGameExe"

if [[ -n "${Javadoc}" ]]; then
  echo -e "Generating Javadoc ..."
  if [[ -n "${Debug}" ]]; then
    javadoc -Xdoclint:all -cp lib/*:. -verbose src/FeynGame.java -sourcepath src/ ui game amplitude resources javafx export -public -private -d Doc
  else
    javadoc -Xdoclint:none -cp lib/*:. -quiet src/FeynGame.java -sourcepath src/ ui game amplitude resources javafx  export -public -private -d Doc
  fi
fi

if [[ -n "${LevelGenerator}" ]]; then
  echo -e "Building Level Generator ..."
  cd src/levelgenerator
  if [[ -n "${Target}" ]]; then
    if [[ -n "${Debug}" ]]; then
      javac -Xlint:all  -Xmaxwarns:300 -deprecation -g -verbose --release "$Target" -d ../../build/levelgenerator LevelGenerator.java
    else
      javac -Xlint:none -nowarn --release "$Target" -d ../../build/levelgenerator LevelGenerator.java
    fi
  else
    if [[ -n "${Debug}" ]]; then
      javac -Xlint:all -Xmaxwarns 300 -deprecation -g -verbose -d ../../build/levelgenerator LevelGenerator.java
    else
      javac -Xlint:none -nowarn -d ../../build/levelgenerator LevelGenerator.java
    fi
  fi
  cd ../..
  touch bin/LevelGenerator.jar
  if [[ -n "${Debug}" ]]; then
    jar -cvmf src/META-INF/MANIFEST2.MF bin/LevelGenerator.jar -C build/levelgenerator LevelGenerator.class -C build levelgenerator
  else
    jar -cmf src/META-INF/MANIFEST2.MF bin/LevelGenerator.jar -C build/levelgenerator LevelGenerator.class -C build levelgenerator
  fi
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
  rm -rf bin/FeynGame.app
  mkdir -p bin/FeynGame.app
  mkdir -p bin/FeynGame.app/Contents
  mkdir -p bin/FeynGame.app/Contents/MacOS
  mkdir -p bin/FeynGame.app/Contents/Resources
  cp bin/"$FeynGameJar" bin/FeynGame.app/Contents/Resources/FeynGame.jar
  cp src/resources/fglogo.icns bin/FeynGame.app/Contents/Resources/

  touch bin/FeynGame.app/Contents/MacOS/feyngame
  chmod +x bin/FeynGame.app/Contents/MacOS/feyngame
  printf '#!/usr/bin/env bash\n' >> bin/FeynGame.app/Contents/MacOS/feyngame
  printf 'SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )\n' >> bin/FeynGame.app/Contents/MacOS/feyngame
  printf 'cd $SCRIPT_DIR\n' >> bin/FeynGame.app/Contents/MacOS/feyngame
  printf 'java -jar ../Resources/FeynGame.jar\n' >> bin/FeynGame.app/Contents/MacOS/feyngame

  touch bin/FeynGame.app/Contents/info.plist
  printf '<?xml version="1.0" encoding="UTF-8"?>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<plist version="1.0">\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<dict>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundleDevelopmentRegion</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>English</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundleExecutable</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>feyngame</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundleIconFile</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>fglogo</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundleInfoDictionaryVersion</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>6.0</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundlePackageType</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>APPL</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundleShortVersionString</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>' >> bin/FeynGame.app/Contents/info.plist
  cat src/version.txt | grep "." >> bin/FeynGame.app/Contents/info.plist
  printf '</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundleSignature</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>xmmd</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>CFBundleVersion</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>' >> bin/FeynGame.app/Contents/info.plist
  cat src/version.txt | grep "." >> bin/FeynGame.app/Contents/info.plist
  printf '</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '<key>NSAppleScriptEnabled</key>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '<string>NO</string>\n' >> bin/FeynGame.app/Contents/info.plist

  printf '</dict>\n' >> bin/FeynGame.app/Contents/info.plist
  printf '</plist>\n' >> bin/FeynGame.app/Contents/info.plist

  if [[ -n "${Prefix}" ]]; then
    echo -e "Installing FeynGame ..."
    echo -e "Prefix: $Prefix"

    install -d $Prefix/Applications/ &> /dev/null
    cp -r bin/FeynGame.app ${Prefix}/Applications/

    if [[ "${Prefix}" == "/" ]]; then
      Prefix=/usr/local
    fi

    install -d $Prefix/share/licenses/FeynGame/
    install -m644 LICENSE "$Prefix/share/licenses/FeynGame/LICENSE"
    install -d $Prefix/share/licenses/FeynGame/3rd-party-licenses
    install -m644 3rd-party-licenses/jgrapht-license.txt "$Prefix/share/licenses/FeynGame/3rd-party-licenses/"
    install -m644 3rd-party-licenses/jheaps-license.txt "$Prefix/share/licenses/FeynGame/3rd-party-licenses/"
    install -m644 3rd-party-licenses/jlatexmath-license.txt "$Prefix/share/licenses/FeynGame/3rd-party-licenses/"

    install -d $Prefix/bin
    install -m755 "bin/$FeynGameExe" "$Prefix/bin/$FeynGameExe"
    (cd "$Prefix/bin/"
      ln -sf $FeynGameExe feyngame
    )
    install -d $Prefix/share/java
    install -m644 "bin/$FeynGameJar" "$Prefix/share/java/$FeynGameJar"

  fi
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
  if [[ -n "${Prefix}" ]]; then
    echo -e "Installing FeynGame ..."
    echo -e "Prefix: $Prefix"

    install -Dm644 LICENSE "$Prefix/share/licenses/FeynGame/LICENSE"
    install -m644 -Dt "$Prefix/share/licenses/FeynGame/3rd-party-licenses" 3rd-party-licenses/*

    install -Dm755 "bin/$FeynGameExe" "$Prefix/bin/$FeynGameExe"
    (cd "$Prefix/bin/"
      ln -sf $FeynGameExe feyngame
    )
    install -Dm644 "bin/$FeynGameJar" "$Prefix/share/java/$FeynGameJar"

    install -Dm644 src/resources/fglogo.png "$Prefix/share/pixmaps/fglogo.png"

    desktop-file-install -m 644 \
      --set-key=Exec --set-value="$Prefix/bin/feyngame %U" \
      --set-icon="$Prefix/share/pixmaps/fglogo.png" \
      --dir="$Prefix/share/applications" "src/feyngame.desktop"
  fi
fi
