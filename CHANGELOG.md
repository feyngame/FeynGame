<tt>FeynGame</tt> 3.0.0
=============

New features
------------

* There is now a menu with recently opened files

* One can now import qgraf output files: the information in the file gets converted to a FeynGame Feynman-diagram

* When choosing a file to open, in the file chooser a preview for the diagram in the selected .fg file is shown

* Increased logging capabilities: The most recent log output is shown in the window, also messages now have severity information. All messages get written to a logfile

* New options for arrows

* Arrows can now be drawn for all styles of lines

* The non-straight lines have now the option to add a small straight section to their ends

* Apply a model to an already drawn diagram, this changes the styles of the objects to the ones of the model

* It is now possible to install FeynGame using the buildfile, it can now also create a .app MacOs application

Changes
-------

* Export has been completely reworked and now allows for the export of multiple diagrams at the same time

* Improved Look-and-Feel Selection

* Changed how SPIRAL lines are drawn

* The grid now features highlighted points that form a larger grid

* Momenta can now be distributed along incorrect but connected diagrams

* Export to pdf is now possible as long as ps2pdf is installed

* The EditFrame now remembers its position and is opened at this position the next time it is invoked

* Unified how filechoosers look and work

* Help pages are updated

* The dimension of the canvas stays constant between sessions and when opening a new model

* The LaTeX font size can now be changed

Bug Fixes
---------

* Labels can now be properly edited with multiple objects selected


<tt>FeynGame</tt> 2.1.0
=============

New features
------------

* For the game mode one can assign Labels for incoming/outgoing particles separately

* In the game mode if a particle is not anticommuting but has different identifiers for the end and the start of the line, it is drawn with a momentum arrow indicating the charge flow, if not an arrow is specified to be present

* For Unix systems an executable is compiled such that one does not have to prepend java -jar in front of the filename

* For LaTeX labels, a default font size can be set

* New options for styling arrows: Height and length can be changed independently and a dent can be added

* Added the new LineTypes SSPIRAL and SWAVE to be able to create Gluinos, Photinos, ...

* The dimension of the canvas gets saved in between sessions

* Added the possibility to set a manual bounding box for exporting

* Installation is now possible with the buildfile

Changes
-------

* When the type of a line is changed via the right-click menu and the label parameters are the default ones for the old line type, they get set to the defaults of the new line type

* Tiles are now arranged according to the vertical/horizontal space that is available so that the space is used more efficiently in the case of the main frame and the edit frame and all tiles are always visible in the right-click menu

* Changed the design of the Tiles slightly

* The fonts in the welcome screen are now more in line with the fonts in the rest of the program

* When adding a new line to a model, the identifiers of these new lines are three character strings (first identifier is lower case and second identifier is upper case of first identifier)

* Feynman/amplitude rules are exported when exporting a model

* When removing a line with associated rules, it is asked whether to only remove the line, the line and the associated rules, or do nothing

* Some warning for the amplitude generator

* When removing a vertex style, its identifier gets removed from the associated vertex rules

* Tooltips are disabled by default, can be enabled with T

* When changing the line type of a line and its label is at the default setting, the default of the new line type is applied for the label

* On MacOs the icon is set to the FeynGame logo

* In the right click menu a default vertex is added to the 'Change Vertex' submenu if no vertex is given in the model

* In the EditFrame, the tiles are not shown any more for shapes

* The grid is now drawn without anti-aliasing, increasing performance

* When changing the pattern via the EditFrame, a default scaling is applied

* Added menu entries for zooming

* In dealing with files, a default filename is provided

* The default model was changed

* changed the directory structure

Bug fixes
---------

* In the game mode, the right-click menu and undo/redo are deactivated

* Fixed some keyboard shortcuts

* Fixed bug where "qgraf.dat" could not be deleted in the level generator

* Undo/redo lists and fixEnd/fixStart properties of the lines were not copied correctly when copying the panel

* Vertex rules with keywords other than "type" lead to errors in the LevelGenerator

* Labels are now editable via multiedit

* Highlighting works if shift is pressed

* Rounding errors in drawing of the canvas are alleviated

* When changing a line in the model to another line in the model via the tile in the EditFrame, its identifiers are not changed. This otherwise leads to inconsistencies in the model.

* Many minor bugs

<tt>FeynGame</tt> 2.0.0
=============

New features
------------

* Added the option to open multiple models at once via tabs. One can add additional models by clicking the + button next to the last tab. One can also convert the content of the frame to a model via <b>File > Convert canvas to model</b>. It is also possible to merge models via <b>File > Merge models</b>.

* Arrows can be toggled for all line types. The line types "FERMION" and "SCALAR" are therefore the same and "SCALAR" is omitted. An arrow is automatically added to a line configuration if the respective definition in the model file has two different identifiers for the end and start of the line. For clarity the names of the line types was changed:
    * FERMION -> PLAIN
    * GLUON -> SPIRAL
    * PHOTON -> WAVE

  The old names can still be used in model files but the names of the line types that are shown in the EditFrame get converted to their respective new names.

* Added momentum arrows

* Added "Shapes"

* Labels are now interpreted with LaTeX typesetting via the library [JLaTeXMath](https://github.com/opencollab/jlatexmath). For the labels to not be interpreted as LaTeX one has to include "\<html\>" in the label

* Vertices (connecting lines, not free floating) have now a flag "autoremove" which is per default "True". If "autoremove" is "True" the vertices behave as before. If it is "False" they do not get automatically removed e.g. two lines connected via such a vertex will not be combined anymore where they would previously have done so, removing the Vertex. If one "deletes" such a vertex and it is connected to nothing or only one line, it gets removed. If it is connected to two lines that can be combined, the vertex gets removed and the lines get combined. If the vertex is not removed, the "autoremove" flag gets set to "True". The flag can be toggled by shift-clicking the vertex or via the EditFrame. Vertices added by right-click -> Cut here start with "autoremove = False"

* Multiple objects can be edited at the same time: When an object is selected, one can select additional objects of the same type by shift-clicking them. Then all the selected objects styles can be edited simultaneously. Another option is to select a tile at the bottom. One can now perform edits on the object associated with the tile in the same way one would do for any other object. In doing so one also edits all objects that have the same style as the object of the selected tile.

* An option to print with a fixed maximum canvas size was added to the print menu. When this menu is opened the area that is printed with the "automatic boundaries" is indicated. After printing, the window that displays the LaTeX includegraphics parameters also shows the parameters to trim the PDF document to the "automatic boundaries".

* A level generator is compiled when giving the <b>-g</b> flag to the buildfiles. The executable jar file <b>Levelgenerator.jar</b> is located in the same directory as the FeynGame executable jar. The Levelgenerator can be used to generate level files for the InFin game mode for a given model file.

* One can now export a diagram to Postscript and Encapsulated Postscript. Export (also to image formats) is now possible directly from the command line.

* Zoom and disposition is now possible for the diagram with Shift-dragging the canvas and using the up and down keys.

* New command line options when starting.

* When model file supports it, the expression for the matrix element can be viewed for correct Feynman diagrams after checking for errors in the diagram.

* If no model file is specified on startup, the *drawing mode* loads a default model containing the **Standard Model** propagators and interactions as well as the corresponding Feynman rules (see `FeynGameJava/template/models/default.model`).

* If no level file is specified on startup, the *in out mode* loads a default level (see `FeynGameJava/template/levels/default.if`).

* If in a level file no explicit model is given, the default model will be loaded.

Changes
-------

* Changed how arrows are drawn: The parameter "arrowSize" now corresponds directly to the length in pixels of the base of the triangle that the arrow consists of.

* Vertices can have shapes "triangle" and "square"

* Toasts are not shown any more

* Changed how arrows are drawn: The parameter "arrowSize" now corresponds directly to the length in pixels of the base of the triangle that the arrow consists of

* In the EditFrame for vertices the Option "Select Pattern" changed to "No Pattern" and, if selected, removes the pattern from the vertex

* Labels get drawn above their respective parent objects

* The options in the "Print" dialogue get saved in preferences

* The "New" option in the "File" menu now lets FeynGame forget the lastly opened/saved ".fg" diagram save file

* All angles are displayed in terms of deg rather than rad

* New windows should be opened in the center of the screen

* Images and JavaFX files get saved inside the ".fg" file. Therefore images copied into <tt>FeynGame</tt> are not saved in the "images" folder any more

* "Edit -> Delete History" now also deletes all non used JavaFX files and Images from Memory

* When a vertex is selected and this vertex is not part of a floating object "Edit -> Delete selected object" does not do anything

* The right click menu does not show "Add floating object" if there is no floating object that can be added, the sub menu of "Add floating Object" also includes floating images; The same holds for the "Cut here" menu: It only appears if there is a vertex preset

* When a pattern is displayed, the back- and foreground colors can be set via the color of the line and the filling (shapes) and the color of the border and the filling (vertices), respectively

* The filling of vertices and shapes can be scaled

* The defaults for the labels of lines are now set separately for the different line types

* The length of the history that gets saved can be changed via <b>Edit > Steps to keep saved in history</b>

* When editing labels in the EditFrame one now does not have to hit <b>Enter</b> for the change to take effect.

* When checking for errors by pressing <b>F</b> or via <b> View -> Check for errors </b> the vertices where errors occurred are marked

* Exiting FeynGame is now mapped to cmd/ctrl + Q

* Slight changes in the behaviour of the up and down keys

* The title of the main window now contains the name of the currently opened file

* Slight reworks in the buildfiles.

Bug fixes
---------
* When loading a diagram from a file the actual file was not remembered by FeynGame. This lead to saving diagrams to a wrong file via the "Save" button

* Fixed bug where the calculations of the bounding box of labels were false

* Lines with the same curvature can only be combined automatically when the center of the circle they are parts of are the same point

* several minor bugs and crashes

* Vertex detection in "Check for errors" improved.

<tt>FeynGame</tt> 1.0.1
=============

Changes
-------
* game.model -> template.model, it is now referred to as template rather than example

* Added new model and level files as well as the FeynGame file for the logo

* Reorganized the build directory

* Addition of bash script ("feyngame") that executes FeynGame with the proper dock icon on MacOs

* Removed HelpPages from InFin game mode

* Removed a useless and empty console output in clipping

* Changed default settings for gamemode


Bug fixes
-------
* Focus is now returned to the main frame when a toast has opened

* Fixed crashes because invert, double, etc. line modifier key was pressed without any suited object present

* Removed non ASCII characters

* Clipping of height to zero when using mouse wheel is now also present in game mode


<tt>FeynGame</tt> 1.0.0
=============

This is the initial release of <tt>FeynGame</tt>. <tt>FeynGame</tt> is a tool to easily draw Feynman diagrams. It also incorporates an educational game where the initial and final states of a Feynman diagram are given and the player has to build a correct and connected Feynman diagram out of that.
